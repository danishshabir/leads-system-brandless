<?php
Class Model_notification extends Base_Model
{
	public function __construct()
	{
		parent::__construct("notifications");
		
	}
	
		
	public function getAllNotifications()
	{
		
		$this->db->select("l.trackid, n.*");
		$this->db->from("notifications n, leads l");
		$this->db->where("n.lead_id=l.id");
		$this->db->where("l.duplicate_of=0");

		$this->db->where("((YEAR(n.created_at) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH)AND MONTH(n.created_at) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ) or (MONTH(n.created_at) = MONTH(CURRENT_DATE)))");
		
		$this->db->where('n.comments != "Auto archived by system." and n.comments != "Lead is Delayed" and n.comments not like "%Lead received from%" and n.comments not like "%Lead deleted%" and n.comments not like "%Upcoming scheduled action for lead%" and n.comments not like "%New lead created%"');

		$this->db->order_by('id','desc');
		
		$result = $this->db->get();				
		
		//echo $this->db->last_query(); exit();
		
		return $result->result();
	}
    
	
	
}