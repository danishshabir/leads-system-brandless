<?php
Class Model_report_bk extends Base_Model
{
	public function __construct()
	{
		parent::__construct("leads");
		
	}
	public function getAllStatus($user_role, $user_id,$check_for_fetching_leads='')
	{
		$this->db->select('count(*) as lead_count,status');
        $this->db->from('leads');
		switch($check_for_fetching_leads)
		{
			case 'assign':
			$this->db->where('leads.assign_to',$user_id);
			break;
			case 'creator':
			$this->db->where('leads.created_by',$user_id);
			break;
			case 'manager':
			$this->db->where('`leads.assign_to` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' ) OR `leads.created_by` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' )');
			break;
			
		}
		$this->db->group_by('status'); 
		$query=$this->db->get();
		// echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	public function getCountByStatus($status)
	{
		$this->db->select('*');
        $this->db->from('leads');
		if($status != '')
		{
			$this->db->where('status', $status);
		}
        $query=$this->db->get();
		
		if($query->num_rows() > 0)
		{
        	return $query->num_rows();
		}
		else
		{
			return '0';
		}
	}
	public function getAllLeads($arr_date= array(),$user_role,$user_id,$branch_id = '',$category_id = '')
	{
		//echo $category_id;
		//exit();
		$this->db->select('DATE( created_at ) as created_at , COUNT( * ) as count_leads ');
        $this->db->from('leads');
        if(!empty($arr_date))
        {
          $this->db->where('DATE(created_at) >= "'.date('Y-m-d',strtotime($arr_date['date_to'])).'" AND DATE(created_at) <= "'.date('Y-m-d',strtotime($arr_date['date_from'])).'" ');  
        }else
        {
          $this->db->where('created_at >= DATE_SUB( NOW( ) , INTERVAL 14 DAY)');  
        }
		if($branch_id != '')
		{
			$this->db->where_in('branch_id',$branch_id);
		}
		if($category_id != '')
		{
			$this->db->where_in('category_id',$category_id);
		}	
		
		$this->db->group_by('DATE( created_at )');
		
        if($user_role != 1)
        {
            $this->db->where('assign_to',$user_id);
        }  
        $query=$this->db->get();
		//echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
    
   public function getAllLeadsOfYear($arr_date= array(),$user_role,$user_id,$branch_id= '')
	{
		//echo $branch_id;
		//exit();
		$this->db->select('DATE( created_at ) as created_at, COUNT( * ) as count_leads ');
        $this->db->from('leads');
        $this->db->where('YEAR(created_at) = "'.date('Y').'" AND MONTH(created_at) BETWEEN 1 AND 12');  
        
		if($branch_id != '')
		{
			$this->db->where_in('branch_id',$branch_id);
		}
		
		$this->db->group_by('MONTH( created_at )');
		
        if($user_role != 1)
        {
            $this->db->where('assign_to',$user_id);
        }  
        $query=$this->db->get();
		//echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
    
	/*public function getLeadsByCategories($user_role, $user_id,$branch_id = '',$category_id= '')
	{
		$this->db->select('categories.title as cat_title, COUNT( * ) as cat_count ');
        $this->db->from('leads');
		$this->db->join('categories', 'leads.category_id = categories.id');
		$this->db->where("STATUS !=  'Approved and Archived'");
		if($user_role != 1)
        {
           $this->db->where('leads.assign_to',$user_id); 
        }
		
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		}
		if($category_id != '')
		{
			$this->db->where_in('leads.category_id',$category_id);
		}	
		
        $this->db->group_by('category_id');
		
        $query=$this->db->get();
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}*/
	public function getLeadsByComplaint($user_role, $user_id,$branch_id = '',$category_id= '')
	{
		$this->db->select('LEFT(`trackid` , 3) as comp_type, COUNT(*) as comp_count');
        $this->db->from('leads,categories');
		//$this->db->where("trackid LIKE '%NCT%' OR trackid LIKE '%UCT%' OR trackid LIKE '%SCT%' OR trackid LIKE '%PCT%' OR trackid LIKE '%CCT%'");
		$this->db->where('leads.category_id = categories.id');
		$this->db->where('categories.lead_type_tag','CT');
		if($user_role != 1)
        {
            $this->db->where('leads.assign_to',$user_id);
        }
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		}
		if($category_id != '')
		{
			$this->db->where_in('leads.category_id',$category_id);
			
		}
		$this->db->where('leads.status !=','Closed');
		$this->db->where('leads.status !=', 'Approved and Archived');
        $this->db->group_by('comp_type');
		
        $query=$this->db->get();
	//	echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
	public function getSatisfaction($user_role, $user_id,$branch_id = '',$category_id = '',$check_for_fetching_leads = '')
	{
		
     
		$this->db->select('DATE(survey_results.created_at) AS date_lead, AVG( survey_results.answer ) as answers_count');
        $this->db->from('survey_results');
		$this->db->from('leads');
        $this->db->from('surveys');
        $this->db->join('questions', 'survey_results.survey_id = questions.survey_id AND survey_results.question_id = questions.id AND surveys.category_id = leads.category_id ');
		$this->db->where("questions.answer =  'Rating'");
		
   
		if($user_id!==-1) //-1 is for overall
		{
			switch($check_for_fetching_leads)
			{
				case 'assign':
				$this->db->where('leads.assign_to',$user_id);
				break;
				case 'creator':
				$this->db->where('leads.created_by',$user_id);
				break;
				case 'manager':
				$this->db->where('`leads.assign_to` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' ) OR `leads.created_by` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' )');
				break;
				
			}
		}
		
		
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		} 
		if($category_id != '')
		{
			$this->db->where("surveys.category_id",$category_id);
		}
		
		$this->db->group_by('DATE(survey_results.created_at)');
		
        $query=$this->db->get();
        
		//echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}


	public function getSatisfaction1($user_id,$assignedOrCreator="Assigned")
	{
		$this->db->select("date(sr.created_at) as perday, avg(sr.answer) as average");
        $this->db->from("leads l, survey_results sr");
		$this->db->where("
		l.id=sr.lead_id 
		and l.survey_response=1 		 
		and sr.answer REGEXP '^[0-9]+$' 
		");

		if($user_id)
		{
			if($assignedOrCreator==="Assigned")
				$this->db->where("l.assign_to='".$user_id."'");
			elseif($assignedOrCreator==="Creator")
				$this->db->where("l.created_by='".$user_id."'");
		}

		$this->db->group_by('DATE(sr.created_at)');
		$this->db->order_by('sr.created_at','ASC');
		

		$query=$this->db->get();

		//echo $this->db->last_query(); exit();

		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return null;
		}
		
	}
	
	public function getCategoryOfSurvey()
	{
		$this->db->select('category_id');
		$this->db->from('surveys');
		$this->db->limit(1);
	    $query=$this->db->get();
		if($query->num_rows() > 0)
		{
        	 $result = $query->result();
		     return $result[0]->category_id;
		}
		else
		{
			return false;
		}
	}
	
	public function getLeadsByRateCategories($user_role, $user_id,$branch_id = '',$category_id = '')
	{
		//$this->db->select('categories.title as rate_title, AVG(TIME_TO_SEC(TIMEDIFF(leads.close_time, leads.created_at))/60) as rate_avg');
		$this->db->select('users.full_name as rate_title, AVG(TIME_TO_SEC(TIMEDIFF(leads.close_time, leads.created_at))/60) as rate_avg');
        $this->db->from('leads');
		//$this->db->join('categories', 'leads.category_id = categories.id');
		$this->db->join('users', 'leads.assign_to = users.id');
		$this->db->where("leads.status =  'Closed'");
		$this->db->where('leads.status !=', 'Approved and Archived');
	    if($user_role != 1)
        {
            $this->db->where('leads.assign_to',$user_id);
            
        }
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		}
	    if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		
    	$this->db->group_by('leads.assign_to');
		
        $query=$this->db->get();
		//echo $this->db->last_query();exit();
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
	public function getLeadsByCategory($user_role, $user_id, $branch_id = '',$category_id='',$check_for_fetching_leads='')
	{
		$this->db->select('LEFT(`trackid` , 3) as cat_type, COUNT(*) as cat_count');
        $this->db->from('leads,categories');
		//$this->db->where("trackid LIKE '%NTD%' OR trackid LIKE '%NSE%' OR trackid LIKE '%NGE%' OR trackid LIKE '%USE%' OR trackid LIKE '%UGE%' OR trackid LIKE '%MGE%' OR trackid LIKE '%CGE%' OR trackid LIKE '%SSE%'");
		$this->db->where('leads.category_id = categories.id');
        switch($check_for_fetching_leads)
		{
			case 'assign':
			$this->db->where('leads.assign_to',$user_id);
			break;
			case 'creator':
			$this->db->where('leads.created_by',$user_id);
			break;
			case 'manager':
			$this->db->where('`leads.assign_to` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' ) OR `leads.created_by` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' )');
			break;
			
		}
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		}
		if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		$this->db->where('leads.status !=','Closed');
		$this->db->where('leads.status !=', 'Approved and Archived');
		
        $this->db->group_by('cat_type');
		
        $query=$this->db->get();
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
	public function getVehicleType($user_role, $user_id,$branch_id = '', $category_id='')
	{
		$this->db->select('vehicles.name as vehicle_name, COUNT( * ) as vehicle_count');
        $this->db->from('leads');
		$this->db->join('vehicles', 'FIND_IN_SET( vehicles.id, leads.vehicle ) ');
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		} 
		
		if($user_role != 1)
        {
            $this->db->where('assign_to',$user_id);
        }
		if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		
		$this->db->group_by('vehicles.name');
        $query=$this->db->get();
		//echo $this->db->last_query();
		//exit();
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
	public function getLeadsByCity($user_role, $user_id,$branch_id = '', $category_id='')
	{ 
		$this->db->select('cities.`title` as `city_name`, COUNT( * ) as city_count');
        $this->db->from('leads');
		$this->db->join('cities', 'cities.id= leads.city_id');
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		} 
		
		if($user_role != 1)
        {
            $this->db->where('assign_to',$user_id);
        }
		if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		$this->db->where('leads.status !=','Closed');
		$this->db->where('leads.status !=', 'Approved and Archived');
		$this->db->group_by('cities.title');
        $query=$this->db->get();
		
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
	public function getLeadsByUser($user_role, $user_id,$branch_id = '', $category_id = '')
	{ 
		$this->db->select('users.full_name as `user_name`, COUNT( * ) as user_count');
        $this->db->from('leads');
		$this->db->join('users', 'users.id= leads.assign_to');
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		} 
		
		if($user_role != 1)
        {
            $this->db->where('assign_to',$user_id);
        }
		
		if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		$this->db->where('leads.status !=','Closed');
		$this->db->where('leads.status !=', 'Approved and Archived');
		$this->db->group_by('users.full_name');
		
        $query=$this->db->get();
		//echo $this->db->last_query();
		//exit();
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
	public function totalCountLeads($user_role, $user_id,$branch_id = '',$category_id = '',$not_count=true)
	{ 
		$this->db->select('COUNT( * ) as lead_count');
        $this->db->from('leads');
		//$this->db->join('users', 'users.id= leads.assign_to');
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		} 
		
		if($user_role != 1)
        {
            $this->db->where('assign_to',$user_id);
        }
		
		if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		if($not_count)
		{
			$this->db->where('leads.status !=', 'Closed');
			$this->db->where('leads.status !=', 'Approved and Archived');
		}
		
		//$this->db->group_by('leads.id');
        $query=$this->db->get();
		//echo $this->db->last_query();
		//exit();
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	/*public function getLeadsByCategoryDir($user_role, $user_id,$branch_id = '',$category_id = '')
	{
		$this->db->select('LEFT(`trackid` , 3) as dir_type, COUNT(*) as dir_count');
        $this->db->from('leads');
		$this->db->where("trackid LIKE '%NCT%' OR trackid LIKE '%UCT%'");
		if($user_role != 1)
        {
            $this->db->where('assign_to',$user_id);
        }
		if($branch_id != '')
		{
			$this->db->where_in('branch_id',$branch_id);
			
		}
	    if($category_id != '')
		{
			$this->db->where("category_id",$category_id);
		} 
        $this->db->group_by('dir_type');
		
        $query=$this->db->get();
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}*/
	
	
	public function getLeadsByRegion($user_role, $user_id,$branch_id = '',$category_id = '',$check_for_fetching_leads='')
	{
		$this->db->select('cities.title as region_title, COUNT( * ) as region_count');
        $this->db->from('leads');
		$this->db->join('cities', 'leads.city_id = cities.id');
		$this->db->where("STATUS !=  'Approved and Archived' AND leads.city_id = cities.id");
		switch($check_for_fetching_leads)
		{
			case 'assign':
			$this->db->where('leads.assign_to',$user_id);
			break;
			case 'creator':
			$this->db->where('leads.created_by',$user_id);
			break;
			case 'manager':
			$this->db->where('`leads.assign_to` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' ) OR `leads.created_by` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' )');
			break;
			
		}
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		}
		if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		$this->db->where('leads.status !=','Closed');
		$this->db->where('leads.status !=', 'Approved and Archived');
        $this->db->order_by('cities.id','ASC');
		$this->db->group_by('cities.title');
		
		
        $query=$this->db->get();
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
	public function getLeadsByBranch($user_role,$branch_id,$branch_ids = '',$category_id= '')
	{
		$this->db->select('branches.title as branch_title, COUNT( * ) as branch_count');
        $this->db->from('leads');
		$this->db->join('branches', 'leads.branch_id = branches.id');
		$this->db->where("STATUS !=  'Approved and Archived' AND leads.branch_id = branches.id");
		if($branch_id == ''){
		if($user_role != 1)
        {
            $this->db->where('leads.branch_id',$branch_id);
        }
	    }else{
			$this->db->where_in('leads.branch_id',$branch_ids);
			
		} 
		if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		$this->db->where('leads.status !=','Closed');
		$this->db->where('leads.status !=', 'Approved and Archived');
        $this->db->group_by('branches.title');
		
        $query=$this->db->get();
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
}