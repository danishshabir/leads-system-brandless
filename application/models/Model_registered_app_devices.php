<?php
Class Model_registered_app_devices extends Base_Model
{
	public function __construct()
	{
		parent::__construct("registered_app_devices");
		
	}

	public function count_no_of_devices_with_key($key)
	{
		$this->db->select('*');
		$this->db->from('registered_app_devices');
		$this->db->where('key', $key);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$result = $query->num_rows();
			return $result;
		} else {
			return '0';
		}
	}
		
	
}