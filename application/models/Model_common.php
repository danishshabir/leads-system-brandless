<?php
Class Model_common extends Base_Model
{

	public function __construct()
	{
		//parent::__construct("leads");
		
	}


	public function getUserDep($uId)
	{
		$this->db->select("sdu.struc_dep_id");
		$this->db->from("struc_dep_users sdu");
		$this->db->where("sdu.user_id='".$uId."' and sdu.struc_dep_id in (52,53,55,56,57)");	
			
		$query=$this->db->get();

		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row->struc_dep_id;
		}

		return 0;
	}
	
	public function getManagers()
	{
		//$manual_query = "SELECT sd.id as dep_id, u.id, u.full_name as name, sd.title as dep_name FROM `structure_departments` sd, users u where u.id=sd.manager_id and sd.id in (56, 57, 55, 52, 53, 54) group by u.id order by u.full_name";
		$manual_query = "SELECT sd.id as dep_id, u.id, u.full_name as name, sd.title as dep_name FROM `structure_departments` sd, users u where u.id=sd.manager_id order by u.full_name";
		
		$query = $this->db->query($manual_query);
			
		return $query->result();	
	}

	
}