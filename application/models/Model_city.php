<?php
Class Model_city extends Base_Model
{
	public function __construct()
	{
		parent::__construct("cities");
		
	}
	

	public function getCities()
	{
		$this->db->select('*');
		$this->db->from("cities");		
		$this->db->where('id != 0');
		$this->db->order_by('title');
		
		$query = $this->db->get();
		
		//echo $this->db->last_query(); exit;
        $result = $query->result_array();
		
		if (!empty($result)) {
			   return $result;
			}else{
			   return false;

		} 
	}
	
	
}