<?php
Class Model_vehicle extends Base_Model
{
	public function __construct()
	{
		parent::__construct("vehicles");
		
	}
	
	
	public function getAllSorted()
	{
		$this->db->select('*');
		$this->db->from('vehicles');
		//$this->db->order_by('sort','asc');
		$query = $this->db->get();
		
		
		if (!empty($query->result())) {
			   return $query->result();
			}else{
			   return false;

			}   
		
	}
	
	
}