<?php
Class Model_vehicle_specific_name extends Base_Model
{
	public function __construct()
	{
		parent::__construct("vehicle_specific_names");
		
	}

	public function deleteByGeneralId($general_v_id)
	{
		$this->db->delete($this->table, array('vehicle_id' => $general_v_id)); 		
	}
	
}