<?php
Class Model_survey_result extends Base_Model
{
	public function __construct()
	{
		parent::__construct("survey_results");
		
	}
    
    public function getSurveyResult($survey_id)
    {
        $query = $this->db->query("SELECT questions.id, AVG( survey_results.answer ) as average FROM  `survey_results` , questions WHERE survey_results.survey_id = questions.survey_id AND questions.answer =  'Rating' AND survey_results.question_id = questions.id AND survey_results.survey_id =".$survey_id." group by questions.id");
    
        $result = $query->result();
       
        if(!empty($result))
        {
            return $result;
        }else
        {
            return false;
        }
    }
    
    public function getTotalAvg($survey_id)
    {
        //$query = $this->db->query("SELECT AVG( survey_results.answer ) as average FROM  `survey_results` , questions WHERE survey_results.survey_id = questions.survey_id AND questions.answer =  'Rating' AND survey_results.question_id = questions.id AND survey_results.survey_id =".$survey_id." ");

		$query = $this->db->query("SELECT AVG( survey_results.answer_val ) as average FROM  `survey_results` WHERE survey_results.survey_id =".$survey_id." ");
    
        $result = $query->result();
       //echo $this->db->last_query();exit();
        if(!empty($result))
        {
            return $result;
        }else
        {
            return false;
        }
    }
    public function getTotalAvgCat($survey_id,$cat_id)
    {
        $query = $this->db->query("SELECT AVG( survey_results.answer ) as average FROM  `survey_results` , questions,surveys WHERE surveys.id = survey_results.survey_id AND survey_results.survey_id = questions.survey_id AND questions.answer =  'Rating' AND survey_results.question_id = questions.id AND surveys.category_id =".$cat_id." ");
    
        $result = $query->result();
       
        if(!empty($result))
        {
            return $result;
        }else
        {
            return false;
        }
    } 
    
    public function getTotalAvgLead($survey_id,$lead_id)
    {
        //$query = $this->db->query("SELECT AVG( survey_results.answer ) as average FROM  `survey_results` , questions WHERE survey_results.survey_id = questions.survey_id AND questions.answer =  'Rating' AND survey_results.question_id = questions.id AND survey_results.survey_id =".$survey_id." AND survey_results.lead_id =".$lead_id."  ");

		$query = $this->db->query("SELECT AVG( survey_results.answer_val ) as average FROM  `survey_results` WHERE survey_results.survey_id =".$survey_id." AND survey_results.lead_id =".$lead_id."  ");
    
        $result = $query->result();
       
        if(!empty($result))
        {
            return $result;
        }else
        {
            return false;
        }
    }

	// it is already check from calling side that lead has a response
	public function getAvgBySurvAndLeadId($survey_id,$lead_id)
    {
        //$query = $this->db->query("SELECT AVG( survey_results.answer ) as average FROM  `survey_results` , questions WHERE survey_results.survey_id = questions.survey_id AND questions.answer =  'Rating' AND survey_results.question_id = questions.id AND survey_results.survey_id =".$survey_id." AND survey_results.lead_id =".$lead_id."  ");

		$query = $this->db->query("SELECT AVG( survey_results.answer_val ) as average FROM  `survey_results` WHERE survey_results.survey_id =".$survey_id." AND survey_results.lead_id =".$lead_id."  ");
    
        $result = $query->result();
       
        if(!empty($result))
        {
            return $result;
        }else
        {
            return false;
        }
    }

	public function getAvgByBranchId($branch_id)
    {
        //$query = $this->db->query("SELECT AVG( survey_results.answer ) as average FROM  `survey_results` , questions WHERE survey_results.survey_id = questions.survey_id AND questions.answer =  'Rating' AND survey_results.question_id = questions.id AND survey_results.survey_id =".$survey_id." AND survey_results.lead_id =".$lead_id."  ");

		$query = $this->db->query("SELECT AVG( sr.answer_val ) as average FROM  survey_results sr, leads l WHERE l.id=sr.lead_id and l.branch_id='".$branch_id."' and l.survey_response='1'");
    
        $result = $query->result();
       
        if(!empty($result))
        {
            return $result;
        }else
        {
            return false;
        }
    }

	public function getAvgAllBranchs()
    {
        //$query = $this->db->query("SELECT AVG( survey_results.answer ) as average FROM  `survey_results` , questions WHERE survey_results.survey_id = questions.survey_id AND questions.answer =  'Rating' AND survey_results.question_id = questions.id AND survey_results.survey_id =".$survey_id." AND survey_results.lead_id =".$lead_id."  ");

		$query = $this->db->query("SELECT AVG( sr.answer_val ) as average FROM  survey_results sr, leads l WHERE l.id=sr.lead_id and l.survey_response='1'");
    
        $result = $query->result();
       
        if(!empty($result))
        {
            return $result;
        }else
        {
            return false;
        }
    }
       
    public function getleadSurveyResponse($survey_id,$lead_id)
    {
        //$query = $this->db->query("SELECT questions.*, survey_results.answer as user_answer from questions ,survey_results where survey_results.question_id = questions.id AND questions.survey_id = ".$survey_id." AND survey_results.lead_id = ".$lead_id." order by questions.order_by asc");

		$query = $this->db->query("SELECT questions.*, survey_results.answer as user_answer, survey_results.created_at as sur_submitted from questions ,survey_results where survey_results.question_id = questions.id AND questions.survey_id = ".$survey_id." AND survey_results.lead_id = ".$lead_id."");
    
        $result = $query->result();
       
        if(!empty($result))
        {
            return $result;
        }else
        {
            return false;
        }
    }
    
	//same function from reports model except this is for single user
	public function getUserAvgScore($user_id)
	{
		$this->db->select("u.full_name, u.id as userid, avg( sr.answer_val ) as avgval");
		$this->db->from("leads l, users u, survey_results sr");
		$this->db->where("u.id=l.assign_to and l.survey_response!=0 and sr.lead_id=l.id");
		$this->db->where('l.duplicate_of', '0');
		$this->db->where('l.assign_to', $user_id);

		$this->db->group_by('l.assign_to'); 
		$this->db->order_by('avgval'); 	
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $result = $query->row();
		}

		return null;
	}	

	
}