<?php

Class Model_structure_departments extends Base_Model

{

	public function __construct()

	{

		parent::__construct("structure_departments");

		

	}

		
	public function getUserManagingDeps($user_id)
	{
		$this->db->select('*');
		$this->db->from('structure_departments');
		$this->db->where('manager_id',$user_id);
		$query = $this->db->get();
		$result = $query->result();
		
		if (!empty($result)) {
		   return $result;
		}else{
		   return false;
		} 
	}
	
	

	

}