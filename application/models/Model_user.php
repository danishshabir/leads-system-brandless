<?php
Class Model_user extends Base_Model
{
	public function __construct()
	{
		parent::__construct("users");
		
	}
	
	public function getAllUsersWithRoles($user_id = '')
	{
		//$this->db->select('users.*,user_roles.title as user_type,branches.title as branch_title,categories.title as depart_title');
		$this->db->select('users.*,user_roles.title as user_type,branches.title as branch_title');
	 	$this->db->from('users');
		$this->db->join('user_roles', 'users.role_id = user_roles.id' );
		$this->db->join('branches', 'users.branch_id = branches.id' );
		//$this->db->join('categories', 'users.depart_id = categories.id' );
		if($user_id != ''){
		$this->db->where('users.id',$user_id);			
		}
		$this->db->where('users.id != ',1);	//main admin id is 1
		$query = $this->db->get();
        $results = $query->result();
		if(!empty($results)){
			return $results;
		}else{
			return false;
		}
	}

	public function getAllUsersExcMe($user_id)
	{
		//$this->db->select('users.*,user_roles.title as user_type,branches.title as branch_title,categories.title as depart_title');
		$this->db->select('*');
	 	$this->db->from('users');		
		$this->db->where('id != ',$user_id);
		$this->db->order_by('full_name','asc');
		$query = $this->db->get();
        $results = $query->result();
		if(!empty($results)){
			return $results;
		}else{
			return false;
		}
	}

	public function getUsersForTagging($notUsers, $text)
	{
		//$this->db->select('users.*,user_roles.title as user_type,branches.title as branch_title,categories.title as depart_title');
		$this->db->select('*');
	 	$this->db->from('users');		
		$this->db->where_not_in('id',$notUsers);
		$this->db->like('full_name',$text);
		$query = $this->db->get();
        $results = $query->result();
		if(!empty($results)){
			return $results;
		}else{
			return false;
		}
	}

	
	
   /* users are no more linked with category
   public function getMembersByCategory($category_id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('is_team_lead',0);
        $this->db->where('is_branch_manager',0);
        $this->db->where('depart_id',$category_id);
        $query = $this->db->get();
        $results = $query->result();
		if(!empty($results)){
			return $results;
		}else{
			return false;
		}
    }*/

	public function getInboxAssignees()
	{

		$this->db->select('u.id');
        $this->db->from('users u, rights r');
        $this->db->where("r.section_id=15 and r.read=1 and u.role_id=r.role_id");
		//$this->db->limit(1);

        $query = $this->db->get();
        $results = $query->result_array();

		//echo $this->db->last_query(); exit();

		if(!empty($results)){
			return $results;
		}else{
			return false;
		}

	}

	public function getUserIdIfSection($userId, $secId) //Currenlty this funciton checks only who have read access.
	{

		$this->db->select('u.id');
        $this->db->from('users u, rights r');
        $this->db->where("r.section_id=".$secId." and u.id=".$userId." and r.read=1 and u.role_id=r.role_id");
		//$this->db->limit(1);

        $query = $this->db->get();
        $results = $query->result_array();

		//echo $this->db->last_query(); exit();

		if(!empty($results)){
			return $results;
		}else{
			return false;
		}

	}

	public function getUsersIn($idsArr)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where_in('id', $idsArr);
		$this->db->order_by('full_name','asc');
		$query = $this->db->get();
		$result = $query->result();

		if (!empty($result)) {
			return $result;
		}else{
			return false;
		} 
	}
	
	
}