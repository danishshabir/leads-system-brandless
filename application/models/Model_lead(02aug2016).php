<?php
Class Model_lead extends Base_Model
{
	public function __construct()
	{
		parent::__construct("leads");
		
	}
	

	public function getAllSortedByPaged($pageNumber,$dashboard=0, $applyLimit=1, $users, $keyWord, $checkLoggedInUserInboxRole, $applyFilter, $category_id, $status)
	{

		$mainQry = "";
		$sortType = "";
		$orderBy = "";	

		$loggedInUserID = $this->session->userdata['user']['id'];
		

		if( rights(35,'read') ) //if the user have the role of view all leads
		{
			$whereQuery = "select l.* from leads l";
		}
		else
		{
			$whereQuery = "select l.* from leads l where assign_to = '".$loggedInUserID."' OR created_by = '".$loggedInUserID."' ";

			$whereQuery .= " union select l.* from leads l where id in (select lead_id from lead_tags where user_id='".$loggedInUserID."' and active=1)  ";

			if($checkLoggedInUserInboxRole) //check if the logged in user has the inbox role
			{
				$whereQuery .= " union select l.* from leads l where created_by = '0' ";
			}

			// for manager it will also see leads of the users under him (From structure)
			if($users)
			{	
				$whereQuery .= " union select l.* from leads l where assign_to in (".$users.") OR created_by in (".$users.")";
			}
		}


		$mainQry = "select temp1.* from (select * from (".$whereQuery.") temp where status !='Approved and Archived' order by created_at desc) temp1 ";		
		
		$AscDesc = $this->input->get('AscDesc');

		if($this->input->get('sortType')!='')
		{
			$sortType = $this->input->get('sortType');

			if($this->input->get('sortType')=='branch') 
			{

				$mainQry .= " left join branches b on temp1.branch_id=b.id";						
				$orderBy  = " order by b.title ".$AscDesc;

			}
			elseif($this->input->get('sortType')=='source')
			{
				if(!$checkLoggedInUserInboxRole)
				{
					$mainQry .= " left join users u on temp1.created_by=u.id
					left join sources s on temp1.manual_source=s.id";							
					$orderBy  = " order by u.full_name, s.title ".$AscDesc;
				}

				if($checkLoggedInUserInboxRole)
				{												
					$orderBy = " order by form_type ".$AscDesc;
				}

			}
			elseif($this->input->get('sortType')=='assignto')
			{
				$mainQry .= " left join users u on temp1.assign_to=u.id";						
				$orderBy  = " order by u.full_name ".$AscDesc;
			}
			elseif($this->input->get('sortType')=='duedate')
			{										
				$orderBy  = " order by due_date ".$AscDesc;
			}
			
		}
		else
		{			
				
			$orderBy = " order by created_at desc";
		}

		//search
		$keyWord = trim($keyWord);
		$keyWord = strtolower($keyWord);

		if($keyWord!="" || $applyFilter)
		{
			$mainQry .= " where 1=1 ";
		}

		if($keyWord!="")
		{
			$mainQry .= " 
			  AND 
			  (
				  LOWER(temp1.trackid) like '%".$keyWord ."%' 
				  OR LOWER(temp1.title) like '%".$keyWord ."%' 
				  OR LOWER(temp1.first_name) like '%".$keyWord ."%' 
				  OR LOWER(temp1.surname) like '%".$keyWord ."%' 
				  OR LOWER(temp1.email) like '%".$keyWord ."%' 
				  OR temp1.mobile like '%".$keyWord."%'
			  )
			 ";
		}

		//when filter is applied then it will be from default listing state and no other state is maintained?
		if($applyFilter)
		{
			if($category_id!='All')
			{
				$mainQry .= " AND temp1.category_id = '".$category_id."' ";
			}					
			
			if($status!='All')
			{	//lead status
				if($status==='Assigned Plus')
				{
					$mainQry .= " AND temp1.assign_to != 0";
				}
				else
				{
					$mainQry .= " AND temp1.status = '".$status."' ";
				}
			}			

		}


		$mainQry .= " group by email ";

		$mainQry .= $orderBy;

		//========
		$limit = 10; //10 per page
		if($pageNumber>1)
			$start = (((int)$pageNumber-1)*$limit);
		else
			$start = 0;

		if($applyLimit)
			$mainQry .= " LIMIT ".$limit." OFFSET ".$start."";
		//==========


		//echo $mainQry; exit();

		$query = $this->db->query($mainQry);
				
		
		$result = $query->result();
		
		
		if (!empty($result)) {
			   return $result;
			}else{
			   return false;

		} 
	}
	
	public function getLeadsWithNotifications($check = '')
	{

		//managers will not see the notifications. Only creator or assignee will see

		$this->db->start_cache();
		$checkLoggedInUserInboxRole = checkLoggedInUserInboxRole();
		$this->db->stop_cache();
		$this->db->flush_cache();		


		$notificationQry = "
		select notifications.*, leads.trackid, leads.title, leads.first_name, leads.surname, leads.vehicle_specific_names_id from notifications, leads where 
			leads.id=notifications.lead_id 
			AND 
			(
				(
					IF(`leads`.`created_by` = '".$this->session->userdata['user']['id']."', `creater_notification_read`, `assignee_notification_read`) = '1'	
				)
				AND 
				(
					leads.created_by='".$this->session->userdata['user']['id']."' 
					OR leads.assign_to='".$this->session->userdata['user']['id']."'
				)
			)
		
		";


		$notificationQry .= "
		UNION
		select notifications.*, leads.trackid, leads.title, leads.first_name, leads.surname, leads.vehicle_specific_names_id from notifications, leads where 
			leads.id=notifications.lead_id 
			AND 
			(
				(
					tagged_user_notification_read = 1
				)
				AND 
				(
					notifications.tagged_user_id='".$this->session->userdata['user']['id']."'
				)
			)
		
		";

		if($checkLoggedInUserInboxRole)
		{
			$notificationQry.= "
				UNION
				select notifications.*, leads.trackid, leads.title, leads.first_name, leads.surname, leads.vehicle_specific_names_id from notifications, leads where 
					leads.id=notifications.lead_id 
					AND 
					(
						(
							inbox_notification_read  = 1
						)
						AND 
						(
							leads.created_by='0'
						)
					)
				
				";
		}

		

		$notificationQry = "select * from (".$notificationQry.") temp order by created_at desc";

		if($check == 'header')
		{
			$notificationQry .= " limit 4";
		}

		$query = $this->db->query($notificationQry);

		$result = $query->result();		
				
		if (!empty($result)) {			 
			return $result;
		}else{			
		   return false;
		}
	  
	}

	public function checkLeadHasUnreadNotifications($lead_id)
	{ 

		//managers will not see the notifications. Only creator or assignee will see

		$leadRec = "";
		$checkLoggedInUserInboxRole = "";

		$this->db->start_cache();
			$this->db->select('*');
			$this->db->from('leads');
			$this->db->where("
			(
				leads.created_by='".$this->session->userdata['user']['id']."'  
				OR leads.assign_to='".$this->session->userdata['user']['id']."'
			)
			AND
			leads.id = '".$lead_id."'			
			");

			$leadQuery = $this->db->get();
			$leadRec = $leadQuery->result();
		$this->db->stop_cache();
		$this->db->flush_cache();

		$this->db->start_cache();
			$checkLoggedInUserInboxRole = checkLoggedInUserInboxRole();
		$this->db->stop_cache();
		$this->db->flush_cache();


		$this->db->select('notifications.*, leads.trackid');
		$this->db->from('notifications, leads');
		$this->db->where("
		(
		leads.id = '".$lead_id."'
		AND
		leads.id=notifications.lead_id 
		)
		");		
		
		//now check creator or assignee
		
		if($leadRec && is_array($leadRec) && $leadRec[0]->created_by === $this->session->userdata['user']['id'])
		{
			$this->db->where("creater_notification_read","1");
		}
		elseif($leadRec && is_array($leadRec) && $leadRec[0]->assign_to === $this->session->userdata['user']['id'])
		{
			$this->db->where("assignee_notification_read","1");
		}									
		elseif($checkLoggedInUserInboxRole)
		{

			$this->db->where("
			(
			inbox_notification_read = '1'
			AND
			notifications.comments not like '%Lead received from%'
			)
			");

		}
		else
		{	
			//tagged
			$this->db->where("(tagged_user_notification_read = '1' AND tagged_user_id='".$this->session->userdata['user']['id']."')");
		}
			
		$query = $this->db->get();	

		//echo $this->db->last_query(); exit();
					
		if (!empty($query->result())) {
			
			   return $query->result();
		}
		else
		{
			return false;
		}
		
	  
	  
	}
	
	public function getSubLeads($email,$lead_id,$limit=0,$assignee=0)
	{
		$this->db->select('*');
		$this->db->from('leads');
		//$this->db->where('new_lead_with_same_email',0);
		$this->db->where('email',$email);
		$this->db->where('id !=',$lead_id);


		/*if($assignee>0)
			$this->db->where('assign_to',$assignee);


		if($limit!=0)
		{
			$this->db->limit($limit);
		}*/

		$this->db->order_by('created_at','desc');
		
		$query = $this->db->get();
		
		//echo $this->db->last_query();exit();
		
		
		if (!empty($query->result())) {
			   return $query->result();
			}else{
			   return false;

			} 
	}

	
	public function getSingleLead($id)
	{
		$this->db->select('*');
		$this->db->from('leads');		
		$this->db->where('id',$id);
		$query = $this->db->get();
		
		
		if (!empty($query->result())) {
			   return $query->result();
			}else{
			   return false;

			} 
	}

	public function getLastLeadId()
	{
		$this->db->select('id');
		$this->db->from('leads');		
		$this->db->order_by('id','desc');
		$this->db->limit(1);
		$query = $this->db->get();
			
		if (!empty($query->result())) 
		{
			$row = $query->result();
			return $row[0]->id;

		}else
		{
			return 0;
		} 
	}

	/*public function searchLeads($keyWord, $users)
	{

		$this->db->start_cache();
		$checkLoggedInUserInboxRole = checkLoggedInUserInboxRole();
		$this->db->stop_cache();
		$this->db->flush_cache();
	

		$this->db->select('*');
		$this->db->from('leads');
			
		$whereQuery = "(";

		$whereQuery .= "		
		assign_to = '".$this->session->userdata['user']['id']."'
		OR created_by = '".$this->session->userdata['user']['id']."'
		";

		if($checkLoggedInUserInboxRole) //check if the logged in user has the inbox role
		{ 
			$whereQuery .= "OR created_by = '0'";
		}

		if($users)
		{							
			$whereQuery .="					
			OR assign_to in (".$users.") 
			OR created_by in (".$users.") 
			";
		}

		$whereQuery .= ")";
		
		$this->db->where("
		(				 
		  ".$whereQuery."
		) 
		AND 
		(
		  trackid like '%".$keyWord ."%' 
		  OR title like '%".$keyWord ."%' 
		  OR first_name like '%".$keyWord ."%' 
		  OR surname like '%".$keyWord ."%' 
		  OR email like '%".$keyWord ."%' 
		  OR mobile like '%".$keyWord."%'
		 )
		 
		 ");
		

		$this->db->order_by('created_at','desc');

		$query = $this->db->get();		

		//echo $this->db->last_query();exit();
		
		
		if (!empty($query->result())) {
			
			   return $query->result();
		}else{
			
		   return false;

		} 
	  
	  
	}*/

	//this function is not used anymore may be?
	public function getFilteredLeads($fields, $users)
	{		


		$this->db->start_cache();
		$checkLoggedInUserInboxRole = checkLoggedInUserInboxRole();
		$this->db->stop_cache();
		$this->db->flush_cache();		

	
		$whereQuery = "(";

		$whereQuery .= "		
		assign_to = '".$this->session->userdata['user']['id']."'
		OR created_by = '".$this->session->userdata['user']['id']."'
		";

		if($checkLoggedInUserInboxRole) //check if the logged in user has the inbox role
		{ 
			$whereQuery .= "OR created_by = '0'";
		}

		if($users)
		{							
			$whereQuery .="					
			OR assign_to in (".$users.") 
			OR created_by in (".$users.") 
			";
		}

		$whereQuery .= ")";
		
		$this->db->where($whereQuery);
		

		$this->db->order_by('created_at','desc');
	
		$result = $this->db->get_where($this->table,$fields);	
		
		//echo $this->db->last_query();exit();
		
		
		if($result->num_rows() > 0)
		{			
		    return $result->result();
		}
		
		else
		{
			return false;
		} 
	}

	
	public function getLeadsWithIdsArr($idsArr)
	{

		$this->db->select('*');
		$this->db->from('leads');
		$this->db->where_in('id', $idsArr);
		$query = $this->db->get();
		$result = $query->result();
		
		if (!empty($result)) {
		   return $result;
		}else{
		   return false;
		} 

	}
	
	public function updateLatestLeadByEmail($email)
	{

		$where['email'] = $email;		
		$data['new_lead_with_same_email'] = 1; //this updated will become on top and others with same email will become as nested.

		$this->db->order_by('updated_at','desc');
		$this->db->limit(1);
		$this->db->update($this->table,$data,$where);


	}
	
	public function getLeadsToUpdateArchive()
	{
		$query = $this->db->query("SELECT *  FROM `leads` WHERE status='Closed' and DATE(close_time) < DATE_SUB('".Date("Y-m-d")."', INTERVAL 1 MONTH)");
		/*$this->db->select('*');
		$this->db->where('status','Closed');
		$this->db->where("close_time <","DATE_SUB('".Date("Y-m-d")."', INTERVAL 1 MONTH)");
		$query = $this->db->get();*/
		//echo $this->db->last_query();exit();
		$result = $query->result();
		
		if (!empty($result)) {
			 
			   return $result;
		}else{
			
		   return false;

		} 
	}

	public function getAllLateLeads()
	{

		$this->db->select('*');
		$this->db->from('leads');

		$this->db->where('late_lead_generated =','0');		
		$this->db->where('status !=','Closed');
		$this->db->where('status !=','Approved and Archived');
		$this->db->where('due_date <',date('Y-m-d H:i:s'));
		$query = $this->db->get();
		
		//echo $this->db->last_query(); exit();

		$result = $query->result();
		
		if (!empty($result)) {
			   return $result;
			}else{
			   return false;

			} 
	}
	
	
}