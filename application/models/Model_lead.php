<?php
Class Model_lead extends Base_Model
{
	public function __construct()
	{
		parent::__construct("leads");
		
	}
	

	public function getAllSortedByPaged($pageNumber,$dashboard=0, $applyLimit=1, $users, $keyWord, $checkLoggedInUserInboxRole, $applyFilter, $category_id, $status, $branchIdOfLoggedInManager, $branchIdOfLoggedInSubMng, $sepcialData = array())
	{
		$mainQry = "";
		$sortType = "";
		$orderBy = "";	
		$notificationsSortQ = false;

		$loggedInUserID = $this->session->userdata['user']['id'];
		// set variable to view on off all leads
		if(isset($this->session->userdata['viewLeadStatus'])){
			if($this->session->userdata['viewLeadStatus'] == 1){
				$viewLeadStatus = 1;
			}else{
				$viewLeadStatus = 0;
			}
		}else{
			$viewLeadStatus = 0;
		}
		//==========
		
		if( rights(36,'read') && $keyWord!="") //if the user have the role of searh any lead
		{
			$whereQuery = "select l.* from leads l";
		}
		elseif( rights(35,'read') && $viewLeadStatus == 0) //if the user have the role of view all leads, also check if the view all is on/off
		//elseif( rights(35,'read')) //if the user have the role of view all leads, also check if the view all is on/off
		{
			$whereQuery = "select l.* from leads l";
		}
		
		else
		{
			
			$whereQuery = "select l.* from leads l where assign_to = '".$loggedInUserID."' OR created_by = '".$loggedInUserID."' OR updated_by = '".$loggedInUserID."' ";

			$whereQuery .= " union select l.* from leads l where id in (select lead_id from lead_tags where user_id='".$loggedInUserID."' and active=1)  ";

			if($checkLoggedInUserInboxRole && $viewLeadStatus == 0) //check if the logged in user has the inbox role, also check if the view all is on/off
			//if($checkLoggedInUserInboxRole) //check if the logged in user has the inbox role, also check if the view all is on/off
			{
				$whereQuery .= " union select l.* from leads l where created_by = '0' ";
			}

			// for manager it will also see leads of the users under him (From structure)
			if($users)
			{	
				$whereQuery .= " union select l.* from leads l where assign_to in (".$users.") OR created_by in (".$users.") OR updated_by in (".$users.") ";
			}			

			if($branchIdOfLoggedInManager)
			{
				if($branchIdOfLoggedInManager==7)
				{
					$whereQuery .= " union select l.* from leads l where (l.branch_id ='7' || l.branch_id ='22')  ";
				}
				elseif($branchIdOfLoggedInManager==1)
				{
					$whereQuery .= " union select l.* from leads l where (l.branch_id ='1' || l.branch_id ='51')  ";
				}
				elseif($branchIdOfLoggedInManager==33)
				{
					$whereQuery .= " union select l.* from leads l where (l.branch_id ='33' || l.branch_id ='52')  ";
				}
				else
				{
					$whereQuery .= " union select l.* from leads l where l.branch_id ='".$branchIdOfLoggedInManager."'  ";
				}
			}
			elseif($branchIdOfLoggedInSubMng)
			{
				$whereQuery .= " union select l.* from leads l where l.branch_id ='".$branchIdOfLoggedInSubMng."'  ";

				//(l.code!='' and l.category_id='6') from mobile app test drive request should also go to managers display by default.
			}

		}
		
		if($this->session->userdata['user']['role_id']==="73") { //data center role 
			$whereQuery .= " union select l.* from leads l where l.is_data_center ='1' and survey_response=0 and date(l.created_at) >= '2016-12-01' ";
		}


		if($applyFilter && $status==="Approved and Archived")
		{
		
			$mainQry = "select temp1.* from (select * from (".$whereQuery.") temp where status !='Duplicated' order by created_at desc) temp1 ";
		}
		else
		{
			if($keyWord!="")
				$mainQry = "select temp1.* from (select * from (".$whereQuery.") temp where status !='Duplicated' order by created_at desc) temp1 ";
			else
			{
				if($this->session->userdata['user']['role_id']==="73" || strpos($status,"SurveyDC")!==false) {
					$mainQry = "select temp1.* from (select * from (".$whereQuery.") temp where ((status ='Approved and Archived' and is_data_center ='1' and survey_response=0) || (status !='Approved and Archived' and is_data_center ='1' and survey_response=0) || ((status !='Approved and Archived') and is_data_center ='0')) and status !='Duplicated' order by created_at  desc) temp1 ";
				}
				else{
					$mainQry = "select temp1.* from (select * from (".$whereQuery.") temp where status !='Approved and Archived' and status !='Duplicated' order by created_at  desc) temp1 ";
				}
			}
		}
		
		if($dashboard)	
		{
			//testing query with hardcoded id
			//$mainQry .= " inner join leads_messages lm on temp1.id=lm.id inner join messages m on lm.message_id=m.id where ( (lm.created_by=(select created_by from leads_messages where lead_id=temp1.id order by id desc limit 1) and m.self_show=1) or (temp1.assign_to='136' and lm.created_by!=(select created_by from leads_messages where lead_id=temp1.id order by id desc limit 1) and m.show_on_assignee_dashboard=1) or (temp1.created_by='136' and lm.created_by!=(select created_by from leads_messages where lead_id=temp1.id order by id desc limit 1) and m.show_on_creator_dashboard=1) ) ";	
			//if($branchIdOfLoggedInManager or $branchIdOfLoggedInSubMng or rights(35,'read'))
			//{
				//$mainQry .=	" where (temp1.status='Finished' or temp1.due_date <= \"".date('Y-m-d H:i:s')."\") ";
				$mainQry .=	" where temp1.id not in (select lead_id from leads_messages lm where lm.message_id not in (16,19) ) ";
			//}
			//else
			//{
				//$mainQry .=	" where (temp1.due_date <= \"".date('Y-m-d H:i:s')."\") ";
			//}
		}
		
		$AscDesc = $this->input->get('AscDesc');

		if($this->input->get('sortType')!='')
		{
			$sortType = $this->input->get('sortType');

			if($this->input->get('sortType')=='branch') 
			{

				$mainQry .= " left join branches b on temp1.branch_id=b.id";						
				$orderBy  = " order by b.title ".$AscDesc;

			}
			elseif($this->input->get('sortType')=='event') 
			{

				$mainQry .= " left join event e on temp1.event_id=e.id";						
				$orderBy  = " order by e.title ".$AscDesc;

			}
			elseif($this->input->get('sortType')=='source')
			{
				if(!$checkLoggedInUserInboxRole)
				{
					$mainQry .= " left join users u on temp1.created_by=u.id
					left join sources s on temp1.manual_source=s.id";							
					$orderBy  = " order by u.full_name, s.title ".$AscDesc;
				}

				if($checkLoggedInUserInboxRole)
				{												
					$orderBy = " order by form_type ".$AscDesc;
				}

			}
			elseif($this->input->get('sortType')=='assignto')
			{
				$mainQry .= " left join users u on temp1.assign_to=u.id";						
				$orderBy  = " order by u.full_name ".$AscDesc;
			}
			elseif($this->input->get('sortType')=='duedate')
			{										
				//$orderBy  = " order by due_date ".$AscDesc;
				$orderBy  = " order by created_at ".$AscDesc;
			}
			elseif($this->input->get('sortType')=='ntf')
			{										
				//$orderBy  = " order by due_date ".$AscDesc;
				//$mainQry .= " left join notifications nt on temp1.id=nt.lead_id ";	

				//temp1.created_by === $this->session->userdata['user']['id'] creater_notification_read
				//temp1.assign_to === $this->session->userdata['user']['id'] assignee_notification_read					
				//nt.tagged_user_id=$this->session->userdata['user']['id'] tagged_user_notification_read
				
				//$orderBy  = " order by nt.id ".$AscDesc;
				$notificationsSortQ = true;
			}
			
		}
		else
		{			
			if($dashboard)	
				//$orderBy = " order by created_at asc";
				$orderBy = " order by created_at asc";
			else
				$orderBy = " order by created_at desc";
		}

		//search
		$keyWord = trim($keyWord);
		$keyWord = strtolower($keyWord);

		if($keyWord!="" || $applyFilter)
		{
			$mainQry .= " where 1=1 ";
		}

		if($keyWord!="")
		{
			$mainQry .= " 
			  AND 
			  (
				  LOWER(temp1.trackid) like '%".$keyWord ."%' 
				  OR LOWER(temp1.title) like '%".$keyWord ."%' 
				  OR LOWER(temp1.first_name) like '%".$keyWord ."%' 
				  OR LOWER(temp1.surname) like '%".$keyWord ."%' 
				  OR LOWER(temp1.email) like '%".$keyWord ."%' 
				  OR temp1.mobile like '%".$keyWord."%'
			  )
			 ";
		}

		//when filter is applied then it will be from default listing state and no other state is maintained?
		$apply_default_filter = true;
		if($applyFilter)
		{
			if($category_id!='All')
			{
				$mainQry .= " AND temp1.category_id = '".$category_id."' ";
			}					
			
			if($status!='All')
			{	//lead status
				if($status==='Assigned Plus')
				{
					$mainQry .= " AND temp1.assign_to != 0";
					$apply_default_filter = false;
				}
				//elseif($status==='Delayed')
				
				//===================================
				$dueDateQ = "";
				$dueDateApplyOr = false;				
				if(strpos($status,"Delayed")!==false)
				{	
					$dueDateQ .= " (temp1.due_date < '".date('Y-m-d H:i:s')."' AND temp1.status != 'Closed' AND temp1.status != 'Finished' AND temp1.status != 'Approved and Archived' AND temp1.status != 'Duplicated') ";
					$apply_default_filter = false;
					$dueDateApplyOr = true;
				}
				
				if(strpos($status,"SurveyDC")!==false)
				{
					if($dueDateApplyOr)
						$dueDateQ .= " OR (temp1.is_data_center ='1' and temp1.survey_response=0) ";
					else
						$dueDateQ .= " (temp1.is_data_center ='1' and temp1.survey_response=0) ";
					
					
					$apply_default_filter = false;
					$dueDateApplyOr = true;
				}
				
				
				//elseif($status==='Upcomming')
				if(strpos($status,"Upcomming")!==false)
				{
					if($dueDateApplyOr)
						$dueDateQ .= " OR (temp1.due_date > '".date('Y-m-d H:i:s')."' AND temp1.status != 'Closed' AND temp1.status != 'Finished' AND temp1.status != 'Approved and Archived' AND temp1.status != 'Duplicated') ";
					else
						$dueDateQ .= " (temp1.due_date > '".date('Y-m-d H:i:s')."' AND temp1.status != 'Closed' AND temp1.status != 'Finished' AND temp1.status != 'Approved and Archived' AND temp1.status != 'Duplicated') ";

					//$orderBy  = " order by due_date asc";
					$apply_default_filter = false;
					$dueDateApplyOr = true;
				}
				if(strpos($status,"Finished")!==false)
				{
					if($dueDateApplyOr)
						$dueDateQ .= " OR temp1.status = 'Finished' ";
					else
						$dueDateQ .= " temp1.status = 'Finished' ";
					$apply_default_filter = false;
					$dueDateApplyOr = true;
				}
				if(strpos($status,"Closed")!==false)
				{
					if($dueDateApplyOr)
						$dueDateQ .= " OR temp1.status = 'Closed' ";
					else
						$dueDateQ .= " temp1.status = 'Closed' ";
					$apply_default_filter = false;
				}

				if($dueDateQ)
				$mainQry .= " AND ( ".$dueDateQ." )";
				//===================================


				if(strpos($status,"branch-")!==false)
				{
					$statusBranch = explode("||",$status)[0];
					$branchArr = explode("-",$statusBranch);
					if($branchArr[1]>0)
					{
						$mainQry .= " AND temp1.branch_id in (";
							for($asf=1; $asf<count($branchArr); $asf++)
							{	if($asf>1) $mainQry .= ",";
								$mainQry .= "'".$branchArr[$asf]."' ";
							}
						$mainQry .= ")";
					}
					else
					{
						$mainQry .= " AND temp1.branch_id = '0' ";
					}
					$apply_default_filter = false;
				}

				if(strpos($status,"event-")!==false)
				{
					$statusEvent = explode("||",$status)[4];
					$eventArr = explode("-",$statusEvent);
					if($eventArr[1]>0)
					{
						$mainQry .= " AND temp1.event_id in (";
							for($asf=1; $asf<count($eventArr); $asf++)
							{	if($asf>1) $mainQry .= ",";
								$mainQry .= "'".$eventArr[$asf]."' ";
							}
						$mainQry .= ")";
					}
					else
					{
						$mainQry .= " AND temp1.event_id = '0' ";
					}
					$apply_default_filter = false;
				}
				//=====================

				if(strpos($status,"assignto-")!==false)
				{
					$statusAssign = explode("||",$status)[2];
					$statusArr = explode("-",$statusAssign);
					if($statusArr[1]>0)
					{
						$mainQry .= " AND temp1.assign_to in (";
							for($asf=1; $asf<count($statusArr); $asf++)
							{	if($asf>1) $mainQry .= ",";
								$mainQry .= "'".$statusArr[$asf]."' ";
							}
						$mainQry .= ")";
					}
					else
					{
						$mainQry .= " AND temp1.assign_to = '0' ";
					}
					$apply_default_filter = false;
				}




				//================
				if(strpos($status,"source::")!==false)
				{
					$statusSource = explode("||",$status)[1];
					$statusSourceArr = explode("::",$statusSource);
					//$statusSourceArrToStr = implode(",",$statusSourceArr);
					
					$sourceQ = "";
					$sourceApplyOr = false;																				
						
					if(in_array("Test Drive",$statusSourceArr) || in_array("Contact us",$statusSourceArr) || in_array("Call Back",$statusSourceArr) || in_array("Contact us",$statusSourceArr) || in_array("Test Drive",$statusSourceArr))
					{
						$sourceQ .= " (temp1.form_type in (";
						$addComma = false;
						for($asf=1; $asf<count($statusSourceArr); $asf++)
						{	
							if(strpos($statusSourceArr[$asf],"Test Drive")!==false || strpos($statusSourceArr[$asf],"Contact us")!==false || strpos($statusSourceArr[$asf],"Call Back")!==false || strpos($statusSourceArr[$asf],"Contact us")!==false || strpos($statusSourceArr[$asf],"Test Drive")!==false)
							{
								if($addComma) $sourceQ .= ",";
								$sourceQ .= "'".$statusSourceArr[$asf]."' ";
								$addComma = true;
							}

						}
						$sourceQ .= ") and manual_source='') ";


						$apply_default_filter = false;
						$sourceApplyOr = true;
					}


					$isManualSourceExists = false;					
					foreach($statusSourceArr as $sS)
					{
						if($sS!="source")
						{
							if(isset($sepcialData['manual_source']) && strpos($sepcialData['manual_source'], $sS)!==false)						
							{
								$isManualSourceExists = true;
								break;
							}
						}
					}					

					if($isManualSourceExists)
					{					
						
						if($sourceApplyOr) $sourceQ .= " OR ";
						
						$sourceQ .= " (temp1.manual_source in (";
						$addComma = false;
						for($asf=1; $asf<count($statusSourceArr); $asf++)
						{	
							if(isset($sepcialData['manual_source']) && strpos($statusSourceArr[$asf], "manual_source")!==false)
							{
								if($addComma) $sourceQ .= ",";
								$sourceQ .= "'".str_replace("manual_source","",$statusSourceArr[$asf])."' ";
								$addComma = true;
							}

						}
						$sourceQ .= ") and form_type='') ";
						
						$apply_default_filter = false;
						$sourceApplyOr = true;
					}

					
					
					
					//==
					$isCreatedByExists = false;					
					foreach($statusSourceArr as $sS)
					{							
							if(strpos($sS,"created_by")!==false)
							{
								$isCreatedByExists = true;
								break;
							}
					}					
					if($isCreatedByExists)
					{
						if($sourceApplyOr) $sourceQ .= " OR ";
						
						$sourceQ .= " (temp1.created_by in (";
							for($asf=1; $asf<count($statusSourceArr); $asf++)
							{	
								if(strpos($statusSourceArr[$asf], "created_by")!==false)
								{
									if($asf>1) $sourceQ .= ",";
									$sourceQ .= "'".str_replace("created_by","",$statusSourceArr[$asf])."' ";
								}
							}
						$sourceQ .= ") and form_type='' and manual_source='') ";
						
						$apply_default_filter = false;
						$sourceApplyOr = true;
					}
					

					if($sourceQ)
					$mainQry .= " AND ( ".$sourceQ." )";
				}
				//=====================================



				if($apply_default_filter)
				{
					$mainQry .= " AND temp1.status = '".$status."' ";
				}
			}			

		}


		//old statment // $mainQry .= " group by email ";
		
		//new statement 
		$mainQry .= " group by
		(CASE 
			WHEN (temp1.email = 'na@na.com') 
			THEN
				temp1.mobile 
			ELSE
				temp1.email
		END) ";

		$mainQry .= $orderBy;

		if(isset($sepcialData['countOnly']) && $sepcialData['countOnly'] == 1)
		{
			
		}
		else
		{
			//========
			$limit = 10; //10 per page
			if($dashboard){
			    $limit = 30;
			}
			
			if($pageNumber>1)
				$start = (((int)$pageNumber-1)*$limit);
			else
				$start = 0;
	
			if($applyLimit)
				$mainQry .= " LIMIT ".$limit." OFFSET ".$start."";
			//==========
		}

		$mainFinalQry = $mainQry;

		if(isset($sepcialData['countOnly']) && $sepcialData['countOnly'] == 1)
		{
			$mainFinalQry = "select count(*) as count from (".$mainQry.") count_table";
		}
		
		if(isset($sepcialData['statusCounter']) && $sepcialData['statusCounter'] == 1)
		{
			$mainFinalQry = "select 'Finished' as status, count(count_table.id) as status_count from (".$mainQry.") count_table where count_table.status in ('Finished')";						
		}
		
		if(isset($sepcialData['statusCounter2']) && $sepcialData['statusCounter2'] == 1)
		{
			$mainFinalQry = "select 'Closed' as status, count(count_table.id) as status_count from (".$mainQry.") count_table where count_table.status in ('Closed')";						
		}
		
		if(isset($sepcialData['delayedCounter']) && $sepcialData['delayedCounter'] == 1)
		{
			$mainFinalQry = "select count(*) as delayed_count from (".$mainQry.") count_table where count_table.due_date <= '".date("Y-m-d H:i:s")."' and count_table.duplicate_of=0 and count_table.status not in(\"Finished\",\"Closed\",\"Approved and Archived\")";						
		}
		
		if(isset($sepcialData['upComingCounter']) && $sepcialData['upComingCounter'] == 1)
		{
			$mainFinalQry = "select count(*) as up_count from (".$mainQry.") count_table where count_table.due_date >= '".date("Y-m-d H:i:s")."' and count_table.duplicate_of=0 and count_table.status not in(\"Finished\",\"Closed\",\"Approved and Archived\")";						
		}
		

		//echo $mainFinalQry; exit();
		if($notificationsSortQ)
		{
			
			//$query = $this->db->query($mainFinalQry);
			$result = $this->getLeadsWithNotifications('',1);
			
		}else
		{

			$query = $this->db->query($mainFinalQry);
			$result = $query->result();
		}
				
		
		
		
		
		if (!empty($result)) {
			   return $result;
			}else{
			   return false;

		} 
	}
	
	//allNotificationsRead() is almost same as the following logics
	public function getLeadsWithNotifications($check = '', $sort=0)
	{

		$this->db->start_cache();
		$checkLoggedInUserInboxRole = checkLoggedInUserInboxRole();
		$this->db->stop_cache();
		$this->db->flush_cache();		

		$slc = "notifications.*, leads.id as leadid, leads.trackid, leads.title, leads.first_name, leads.surname, leads.vehicle_specific_names_id, leads.status";
		if($sort)
		{
			$slc = "leads.*";
		}
		

		$notificationQry = "
		select ".$slc." from notifications, leads where 
			leads.id=notifications.lead_id 
			AND 
			(
				(
					IF(`leads`.`created_by` = '".$this->session->userdata['user']['id']."', `creater_notification_read`, `assignee_notification_read`) = '1'	
				)
				AND 
				(
					leads.created_by='".$this->session->userdata['user']['id']."' 
					OR leads.assign_to='".$this->session->userdata['user']['id']."'
				)
			)
		
		";


		$notificationQry .= "
		UNION
		select ".$slc." from notifications, leads where 
			leads.id=notifications.lead_id 
			AND 
			(
				(
					tagged_user_notification_read = 1
				)
				AND 
				(
					notifications.tagged_user_id='".$this->session->userdata['user']['id']."'
				)
			)
		
		";

		if($checkLoggedInUserInboxRole)
		{
			$notificationQry.= "
				UNION
				select ".$slc." from notifications, leads where 
					leads.id=notifications.lead_id 
					AND 
					(
						(
							inbox_notification_read  = 1
						)
						AND 
						(
							leads.created_by='0'
						)
					)					
				
				";
				
				if($sort)
				{
					$notificationQry.= " and notifications.comments not like '%Lead received from%'";
				}
		}

		

		$notificationQry = "select * from (".$notificationQry.") temp where temp.status !='Approved and Archived' and temp.status !='Duplicated' order by created_at desc";

		if($check == 'header')
		{
			$notificationQry .= " limit 4";
		}
		//echo $notificationQry; exit;
		$query = $this->db->query($notificationQry);
		
		$result = $query->result();		
				
		if (!empty($result)) {			 
			return $result;
		}else{			
		   return false;
		}
	}
	//getLeadsWithNotifications() is almost same as the following logics
	public function allNotificationsRead()
	{
		//echo $id; exit;
		$this->db->start_cache();
		$checkLoggedInUserInboxRole = checkLoggedInUserInboxRole();
		$this->db->stop_cache();
		$this->db->flush_cache();		
		
		$notificationQry = "update notifications set assignee_notification_read=0, creater_notification_read=0,inbox_notification_read=0, tagged_user_notification_read=0 
where notifications.id 
IN
(select id from (select notifications.*, leads.id as leadid, leads.trackid, leads.title, leads.first_name, leads.surname, leads.vehicle_specific_names_id from notifications, leads where 
			leads.id=notifications.lead_id 
			AND 
			(
				(
					IF(`leads`.`created_by` = '".$this->session->userdata['user']['id']."', `creater_notification_read`, `assignee_notification_read`) = '1'	
				)
				AND 
				(
					leads.created_by='".$this->session->userdata['user']['id']."' 
					OR leads.assign_to='".$this->session->userdata['user']['id']."'
				)
			)
		";
		$notificationQry .= "
		UNION
		select notifications.*, leads.id as leadid, leads.trackid, leads.title, leads.first_name, leads.surname, leads.vehicle_specific_names_id from notifications, leads where 
			leads.id=notifications.lead_id 
			AND 
			(
				(
					tagged_user_notification_read = 1
				)
				AND 
				(
					notifications.tagged_user_id='".$this->session->userdata['user']['id']."'
				)
			)
		";
		if($checkLoggedInUserInboxRole)
		{
			$notificationQry.= "
				UNION
				select notifications.*, leads.id as leadid, leads.trackid, leads.title, leads.first_name, leads.surname, leads.vehicle_specific_names_id from notifications, leads where 
					leads.id=notifications.lead_id 
					AND 
					(
						(
							inbox_notification_read  = 1
						)
						AND 
						(
							leads.created_by='0'
						)
					)
					
				";
		}
		$notificationQry.= ") temp )";
		$query = $this->db->query($notificationQry);
		return $query;
	}

	public function checkLeadHasUnreadNotifications($lead_id)
	{ 

		//managers will not see the notifications. Only creator or assignee will see

		$leadRec = "";
		$checkLoggedInUserInboxRole = "";

		$this->db->start_cache();
			$this->db->select('*');
			$this->db->from('leads');
			$this->db->where("
			(
				leads.created_by='".$this->session->userdata['user']['id']."'  
				OR leads.assign_to='".$this->session->userdata['user']['id']."'
			)
			AND
			leads.id = '".$lead_id."'			
			");

			$leadQuery = $this->db->get();
			$leadRec = $leadQuery->result();
		$this->db->stop_cache();
		$this->db->flush_cache();

		$this->db->start_cache();
			$checkLoggedInUserInboxRole = checkLoggedInUserInboxRole();
		$this->db->stop_cache();
		$this->db->flush_cache();


		$this->db->select('notifications.*, leads.trackid');
		$this->db->from('notifications, leads');
		$this->db->where("
		(
		leads.id = '".$lead_id."'
		AND
		leads.id=notifications.lead_id 
		)
		");		
		
		//now check creator or assignee
		
		if($leadRec && is_array($leadRec) && $leadRec[0]->created_by === $this->session->userdata['user']['id'])
		{
			$this->db->where("creater_notification_read","1");
		}
		elseif($leadRec && is_array($leadRec) && $leadRec[0]->assign_to === $this->session->userdata['user']['id'])
		{
			$this->db->where("assignee_notification_read","1");
		}									
		elseif($checkLoggedInUserInboxRole)
		{

			$this->db->where("
			(
			inbox_notification_read = '1'
			AND
			notifications.comments not like '%Lead received from%'
			)
			");

		}
		else
		{	
			//tagged
			$this->db->where("(tagged_user_notification_read = '1' AND tagged_user_id='".$this->session->userdata['user']['id']."')");
		}
			
		$query = $this->db->get();	

		//echo $this->db->last_query(); exit();
					
		if (!empty($query->result())) {
			
			   return $query->result();
		}
		else
		{
			return false;
		}
		
	  
	  
	}
	
	//old 12 jan 2017 public function getSubLeads($email,$lead_id,$limit=0,$assignee=0)
	public function getSubLeads($email,$mobile,$lead_id,$limit=0,$assignee=0)
	{
		$this->db->select('*');
		$this->db->from('leads');
		//$this->db->where('new_lead_with_same_email',0);
		
		//old 12 dec 2017 $this->db->where('email',$email);
		//New
		if(strtolower($email)=="na@na.com"){
			$this->db->where('mobile',$mobile);
			$this->db->where('email',"na@na.com");
		}
		else{
			$this->db->where('email',$email);
		}
			
		$this->db->where('id !=',$lead_id);
		//$this->db->where('status !=','Approved and Archived');


		/*if($assignee>0)
			$this->db->where('assign_to',$assignee);


		if($limit!=0)
		{
			$this->db->limit($limit);
		}*/

		$this->db->order_by('created_at','desc');
		
		$query = $this->db->get();
		
		//echo $this->db->last_query();exit();
		
		
		if (!empty($query->result())) {
			   return $query->result();
			}else{
			   return false;

			} 
	}

	public function getSubLeads1($email,$lead_id,$limit=0,$assignee=0)
	{
		$this->db->select('*');
		$this->db->from('leads');
		//$this->db->where('new_lead_with_same_email',0);
		$this->db->where('email',$email);
		$this->db->where('id !=',$lead_id);
		$this->db->where('status !=','Approved and Archived');
		$this->db->where('status !=','Duplicated');


		/*if($assignee>0)
			$this->db->where('assign_to',$assignee);


		if($limit!=0)
		{
			$this->db->limit($limit);
		}*/

		$this->db->order_by('created_at','desc');
		
		$query = $this->db->get();
		
		//echo $this->db->last_query();exit();
		
		
		if (!empty($query->result())) {
			   return $query->result();
			}else{
			   return false;

			} 
	}

	
	public function getSingleLead($id)
	{
		$this->db->select('*');
		$this->db->from('leads');		
		$this->db->where('id',$id);
		$query = $this->db->get();
		
		
		if (!empty($query->result())) {
			   return $query->result();
			}else{
			   return false;

			} 
	}

	public function getLastLeadId()
	{
		$this->db->select('id');
		$this->db->from('leads');		
		$this->db->order_by('id','desc');
		$this->db->limit(1);
		$query = $this->db->get();
			
		if (!empty($query->result())) 
		{
			$row = $query->result();
			return $row[0]->id;

		}else
		{
			return 0;
		} 
	}

	/*public function searchLeads($keyWord, $users)
	{

		$this->db->start_cache();
		$checkLoggedInUserInboxRole = checkLoggedInUserInboxRole();
		$this->db->stop_cache();
		$this->db->flush_cache();
	

		$this->db->select('*');
		$this->db->from('leads');
			
		$whereQuery = "(";

		$whereQuery .= "		
		assign_to = '".$this->session->userdata['user']['id']."'
		OR created_by = '".$this->session->userdata['user']['id']."'
		";

		if($checkLoggedInUserInboxRole) //check if the logged in user has the inbox role
		{ 
			$whereQuery .= "OR created_by = '0'";
		}

		if($users)
		{							
			$whereQuery .="					
			OR assign_to in (".$users.") 
			OR created_by in (".$users.") 
			";
		}

		$whereQuery .= ")";
		
		$this->db->where("
		(				 
		  ".$whereQuery."
		) 
		AND 
		(
		  trackid like '%".$keyWord ."%' 
		  OR title like '%".$keyWord ."%' 
		  OR first_name like '%".$keyWord ."%' 
		  OR surname like '%".$keyWord ."%' 
		  OR email like '%".$keyWord ."%' 
		  OR mobile like '%".$keyWord."%'
		 )
		 
		 ");
		

		$this->db->order_by('created_at','desc');

		$query = $this->db->get();		

		//echo $this->db->last_query();exit();
		
		
		if (!empty($query->result())) {
			
			   return $query->result();
		}else{
			
		   return false;

		} 
	  
	  
	}*/

	//this function is not used anymore may be?
	public function getFilteredLeads($fields, $users)
	{		


		$this->db->start_cache();
		$checkLoggedInUserInboxRole = checkLoggedInUserInboxRole();
		$this->db->stop_cache();
		$this->db->flush_cache();		

	
		$whereQuery = "(";

		$whereQuery .= "		
		assign_to = '".$this->session->userdata['user']['id']."'
		OR created_by = '".$this->session->userdata['user']['id']."'
		";

		if($checkLoggedInUserInboxRole) //check if the logged in user has the inbox role
		{ 
			$whereQuery .= "OR created_by = '0'";
		}

		if($users)
		{							
			$whereQuery .="					
			OR assign_to in (".$users.") 
			OR created_by in (".$users.") 
			";
		}

		$whereQuery .= ")";
		
		$this->db->where($whereQuery);
		

		$this->db->order_by('created_at','desc');
	
		$result = $this->db->get_where($this->table,$fields);	
		
		//echo $this->db->last_query();exit();
		
		
		if($result->num_rows() > 0)
		{			
		    return $result->result();
		}
		
		else
		{
			return false;
		} 
	}

	
	public function getLeadsWithIdsArr($idsArr)
	{

		$this->db->select('*');
		$this->db->from('leads');
		$this->db->where_in('id', $idsArr);
		$query = $this->db->get();
		$result = $query->result();
		
		if (!empty($result)) {
		   return $result;
		}else{
		   return false;
		} 

	}
	
	public function updateLatestLeadByEmail($email)
	{

		$where['email'] = $email;		
		$data['new_lead_with_same_email'] = 1; //this updated will become on top and others with same email will become as nested.

		$this->db->order_by('updated_at','desc');
		$this->db->limit(1);
		$this->db->update($this->table,$data,$where);


	}
	
	public function getLeadsToUpdateArchive()
	{
		//$query = $this->db->query("SELECT *  FROM `leads` WHERE status='Closed' and DATE(close_time) < DATE_SUB('".Date("Y-m-d")."', INTERVAL 1 MONTH)");
		$query = $this->db->query("SELECT *  FROM `leads` WHERE status='Closed' and DATE(close_time) < '".Date("Y-m-d")."'");
		/*$this->db->select('*');
		$this->db->where('status','Closed');
		$this->db->where("close_time <","DATE_SUB('".Date("Y-m-d")."', INTERVAL 1 MONTH)");
		$query = $this->db->get();*/
		//echo $this->db->last_query();exit();
		$result = $query->result();
		
		if (!empty($result)) {
			 
			   return $result;
		}else{
			
		   return false;

		} 
	}

	public function getAllLateLeads()
	{

		$this->db->select('*');
		$this->db->from('leads');

		$this->db->where('late_lead_generated =','0');		
		$this->db->where('status !=','Closed');
		$this->db->where('status !=','Finished');
		$this->db->where('status !=','Duplicated');
		$this->db->where('status !=','No Response');
		$this->db->where('status !=','Approved and Archived');
		$this->db->where('due_date <',date('Y-m-d H:i:s'));
		$query = $this->db->get();
		
		//echo $this->db->last_query(); exit();

		$result = $query->result();
		
		if (!empty($result)) {
			   return $result;
			}else{
			   return false;

			} 
	}

	public function checkIfCustomerInteracted($lead_id)
	{
		$this->db->select('*');
		$this->db->from('leads_messages');

		$this->db->where('lead_id',$lead_id);		
		//$this->db->where('message_id in (3,5,8,9,15)');	
		$this->db->where('message_id in (3,5,8,9)');
		$query = $this->db->get();
		
		//echo $this->db->last_query(); exit();

		$result = $query->result();
		
		if (!empty($result)) {
			   return "1";
		}else{
			   return "0";

		} 

	}
	
	//12 Nov 2016 - return leads for which survey is complete but notification is not sent yet
	public function getCompletedSurveyLeadsForNotification()
	{
		$this->db->select('*');
		$this->db->from('leads');		
		$this->db->where('survey_response',1);
		$this->db->where('survey_notification',0);
		$query = $this->db->get();
		
		
		if (!empty($query->result())) {
			   return $query->result();
			}else{
			   return false;

			} 
	}

	public function getBranchDDB($users="")
	{
		$this->db->select('b.id as bid, b.title as branch_name, count(l.id) as branch_count');
		$this->db->from('leads l, branches b');		
		$this->db->where('`l`.`branch_id` = `b`.`id` and l.duplicate_of=0 and l.status!="Approved and Archived"');
		
		if($users)
		$this->db->where('l.assign_to in ('.$users.')');		
		
		$this->db->group_by('l.branch_id');
		$this->db->order_by('b.title');
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->result())) {
		   return $query->result();
		}else{
		   return false;
		} 
		
	}
	
	public function getNoBranchCount($users="")
	{
		$this->db->select('count(l.id) as nobranch_count');
		$this->db->from('leads l');		
		$this->db->where('l.branch_id=0 and l.duplicate_of=0 and l.status!="Approved and Archived"');
	
	if($users)
		$this->db->where('l.assign_to in ('.$users.')');	
	
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->row())) {
		   return $query->row();
		}else{
		   return false;
		} 
		
	}
	
	
	public function getSourceDDB($branch_id_for_filter=0)
	{
		$this->db->select('ms.id as msid, ms.title as msource_name, count(l.id) as msource_count');
		$this->db->from('leads l, sources ms');		
		//l.form_type="" there are less than 10 such buggy leads that were in the start of this project so have this check.
		//other wise either l.form_type="" or l.manual_source one of these have value, both can't
		$this->db->where('`l`.`manual_source` = `ms`.`id` and l.form_type="" and l.duplicate_of=0 and l.status!="Approved and Archived"');
		if($branch_id_for_filter) $this->db->where('l.branch_id',$branch_id_for_filter);
		$this->db->group_by('ms.id');
		$this->db->order_by('ms.title');
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->result())) {
		   return $query->result();
		}else{
		   return false;
		} 
		
	}
	
	public function getFormTypeDDB($branch_id_for_filter=0)
	{
		$this->db->select('l.form_type, count(l.id) as form_type_count');
		$this->db->from('leads l');			
		$this->db->where('l.manual_source="" and l.form_type!="" and l.duplicate_of=0 and l.status!="Approved and Archived"');
		if($branch_id_for_filter) $this->db->where('l.branch_id',$branch_id_for_filter);
		$this->db->group_by('l.form_type');
		$this->db->order_by('l.form_type');
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->result())) {
		   return $query->result();
		}else{
		   return false;
		} 
		
	}
	
	public function getEventDDB()
	{
		$this->db->select('e.id as eid, e.title as event_name, count(l.id) as event_count');
		$this->db->from('leads l, event e');		
		$this->db->where('`l`.`event_id` = `e`.`id` and l.duplicate_of=0 and l.status!="Approved and Archived"');
		$this->db->group_by('e.id');
		$this->db->order_by('e.title');
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->result())) {
		   return $query->result();
		}else{
		   return false;
		} 
		
	}
	
		
	public function getAssignToDDB($branch_id_for_filter=0,$users)
	{	
		$viewAll = false;
		if(rights(35,'read')) $viewAll = true;
		
		//echo $branch_id_for_filter; exit;
		$this->db->select('u.id as uid, u.full_name, count(l.id) as count');
		$this->db->from('leads l, users u');			
		$this->db->where('l.assign_to=u.id and l.duplicate_of=0 and l.status!="Approved and Archived"');
		
		// set variable to view on off all leads
		if(isset($this->session->userdata['viewLeadStatus'])){
			if($this->session->userdata['viewLeadStatus'] == 1){
				$viewLeadStatus = 1;
			}else{
				$viewLeadStatus = 0;
			}
		}else{
			$viewLeadStatus = 0;
		}
		//==========
		
		if($viewAll  && $viewLeadStatus == 0) //if the user have the role of view all leads, also check if the view all is on/off
		{
			
		}
		elseif($users){
			$this->db->where('l.assign_to in ('.$users.')');
		}
		
		//if($branch_id_for_filter) $this->db->where('l.branch_id',$branch_id_for_filter);
		
		$this->db->group_by('u.id');
		$this->db->order_by('u.full_name');
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->result())) {
		   return $query->result();
		}else{
		   return false;
		} 
		
	}
	
	public function getNoAssignToCount($branch_id_for_filter=0)
	{
		$this->db->select('count(l.id) as noassign_count');
		$this->db->from('leads l');			
		$this->db->where('l.assign_to=0 and l.duplicate_of=0 and l.status!="Approved and Archived"');
		if($branch_id_for_filter) $this->db->where('l.branch_id',$branch_id_for_filter);
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->row())) {
		   return $query->row();
		}else{
		   return false;
		} 
		
	}
	
	public function getStatusCounter($branch_id_for_filter=0,$simple_user_id_for_filter=0)
	{
		$this->db->select('l.status, count(l.id) as status_count');
		$this->db->from('leads l');			
		$this->db->where('l.status in ("Finished","Closed") and l.duplicate_of=0 and l.status!="Approved and Archived"');
		if($branch_id_for_filter) $this->db->where('l.branch_id',$branch_id_for_filter);
		elseif($simple_user_id_for_filter) $this->db->where('(l.assign_to="'.$simple_user_id_for_filter.'" or l.created_by="'.$simple_user_id_for_filter.'")');
		$this->db->group_by('l.status');
		$this->db->order_by('l.status');
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->result())) {
		   return $query->result();
		}else{
		   return false;
		} 
		
	}
	
	public function getDelayedCounter($branch_id_for_filter=0,$simple_user_id_for_filter=0)
	{
		$this->db->select('count(l.id) as delayed_count');
		$this->db->from('leads l');			
		$this->db->where('l.due_date <= "'.date("Y-m-d H:i:s").'" and l.duplicate_of=0 and l.status not in("Finished","Closed","Approved and Archived")');
		if($branch_id_for_filter) $this->db->where('l.branch_id',$branch_id_for_filter);
		elseif($simple_user_id_for_filter) $this->db->where('(l.assign_to="'.$simple_user_id_for_filter.'" or l.created_by="'.$simple_user_id_for_filter.'")');
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->row())) {
		   return $query->row();
		}else{
		   return false;
		} 
		
	}
	
	public function getUpComingCounter($branch_id_for_filter=0,$simple_user_id_for_filter=0)
	{
		$this->db->select('count(l.id) as up_count');
		$this->db->from('leads l');			
		$this->db->where('l.due_date >= "'.date("Y-m-d H:i:s").'" and l.duplicate_of=0 and l.status not in("Finished","Closed","Approved and Archived")');
		if($branch_id_for_filter) $this->db->where('l.branch_id',$branch_id_for_filter);
		elseif($simple_user_id_for_filter) $this->db->where('(l.assign_to="'.$simple_user_id_for_filter.'" or l.created_by="'.$simple_user_id_for_filter.'")');
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->row())) {
		   return $query->row();
		}else{
		   return false;
		} 
		
	}
	
	
	
	public function getSurveyDCCounter()
	{
		$this->db->select('count(*) as sur_count');
		$this->db->from('leads l');			
		$this->db->where('l.is_data_center ="1" and l.survey_response=0 and l.duplicate_of=0 and date(l.created_at) >= "2016-12-01"');		
		
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->row())) {
		   return $query->row();
		}else{
		   return false;
		} 
		
	}
	
	
	
	public function getTotalLeadsCountForPaging($branch_id_for_filter=0,$simple_user_id_for_filter=0)
	{
		$this->db->select('count(l.id) as count');
		$this->db->from('leads l');			
		$this->db->where('l.new_lead_with_same_email=1 and l.duplicate_of=0 and l.status!="Approved and Archived"');
		if($branch_id_for_filter) $this->db->where('l.branch_id',$branch_id_for_filter);
		elseif($simple_user_id_for_filter) $this->db->where('(l.assign_to="'.$simple_user_id_for_filter.'" or l.created_by="'.$simple_user_id_for_filter.'")');
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->row())) {
		   $row = $query->row();
		   return $row->count;
		}else{
		   return 0;
		} 
		
	}
	
	public function getActiveLeadsCount($branch_id_for_filter=0,$simple_user_id_for_filter=0)
	{
		$this->db->select('count(l.id) as count');
		$this->db->from('leads l');			
		$this->db->where('l.duplicate_of=0 and l.status not in ("Closed","Approved and Archived")');
		if($branch_id_for_filter) $this->db->where('l.branch_id',$branch_id_for_filter);
		elseif($simple_user_id_for_filter) $this->db->where('(l.assign_to="'.$simple_user_id_for_filter.'" or l.created_by="'.$simple_user_id_for_filter.'")');
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->row())) {
		   $row = $query->row();
		   return $row->count;
		}else{
		   return 0;
		} 
		
	}
	
	public function getUnAssignedCount($branch_id_for_filter=0)
	{
		$this->db->select('count(l.id) as count');
		$this->db->from('leads l');			
		$this->db->where('l.assign_to=0 and l.duplicate_of=0 and l.status not in ("Closed","Approved and Archived")');
		if($branch_id_for_filter) $this->db->where('l.branch_id',$branch_id_for_filter);		
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->row())) {
		   $row = $query->row();
		   return $row->count;
		}else{
		   return 0;
		} 
		
	}
	
	public function getDepUsers($uid)
	{
		
		$this->db->select("GROUP_CONCAT(user_id) as users");
		$this->db->from("struc_dep_users");
		$this->db->where("struc_dep_id=(select struc_dep_id from struc_dep_users where user_id='".$uid."' limit 1)");		
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->row())) {
		   $row = $query->row();
		   return $row->users; //comma sep
		}else{
		   return 0;
		} 
		
	}
	
	public function getDepAvgScore($users)
	{
		if($users)
		{
			$this->db->select("avg( sr.answer_val ) as avgval");
			$this->db->from("leads l, survey_results sr");
			$this->db->where("l.survey_response!=0 and sr.lead_id=l.id and l.assign_to in (".$users.")");
			$this->db->where('l.duplicate_of', '0');		
			$query = $this->db->get();
			//echo $this->db->last_query(); exit();
			if (!empty($query->row())) {
			   $row = $query->row();
			   return round($row->avgval,2);
			}else{
			   return 0;
			} 
		}
		return 0;
		
	}
	
	public function getAllScore()
	{
		$this->db->select("avg( sr.answer_val ) as avgval");
		$this->db->from("leads l, survey_results sr");
		$this->db->where("l.survey_response!=0 and sr.lead_id=l.id");
		$this->db->where('l.duplicate_of', '0');		
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->row())) {
		   $row = $query->row();
		   return round($row->avgval,2);
		}else{
		   return 0;
		} 
		
	}
	
	public function getUserAvgScore($uid)
	{
		$this->db->select("avg( sr.answer_val ) as avgval");
		$this->db->from("leads l, survey_results sr");
		$this->db->where("l.survey_response!=0 and sr.lead_id=l.id and l.assign_to='".$uid."'");
		$this->db->where('l.duplicate_of', '0');		
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->row())) {
		   $row = $query->row();
		   return round($row->avgval,2);
		}else{
		   return 0;
		} 
		
	}
	
	//leads that are assigned and delayed.
	public function getAssignedDelayedCounter($branch_id_for_filter=0,$simple_user_id_for_filter=0)
	{
		$this->db->select('count(l.id) as delayed_count');
		$this->db->from('leads l');			
		$this->db->where('l.assign_to!=0 and l.due_date <= "'.date("Y-m-d H:i:s").'" and l.duplicate_of=0 and l.status not in("Finished","Closed","Approved and Archived")');
		if($branch_id_for_filter) $this->db->where('l.branch_id',$branch_id_for_filter);
		elseif($simple_user_id_for_filter) $this->db->where('(l.assign_to="'.$simple_user_id_for_filter.'" or l.created_by="'.$simple_user_id_for_filter.'")');
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		if (!empty($query->row())) {
		   return $query->row();
		}else{
		   return 0;
		} 
		
	}
	
	//leads that are assigned and they are not delayed yet but user also have not taken any action after assigned.
	public function getAssignedNoActionCounter($branch_id_for_filter=0,$simple_user_id_for_filter=0)
	{
				
		$manual_query = "select count(l.id) as noaction_count from leads l, leads_messages lm where l.id=lm.lead_id and lm.message_id=19 and lm.lead_id not in (select lead_id from leads_messages where message_id!=19) and l.status not in (\"Closed\",\"Approved and Archived\") and l.duplicate_of=0 and l.due_date >= \"".date("Y-m-d H:i:s")."\"";
		
		if($branch_id_for_filter) $manual_query .= ' and l.branch_id="'.$branch_id_for_filter.'"';
		elseif($simple_user_id_for_filter) $manual_query .= ' and (l.assign_to="'.$simple_user_id_for_filter.'" or l.created_by="'.$simple_user_id_for_filter.'")';
		
		$query = $this->db->query($manual_query);
		
		if (!empty($query->row())) {
		   return $query->row();
		}else{
		   return 0;
		} 
		
		
	}
	
	public function getUsersLeadCount($users)
	{
		if($users)
		{
			$manual_query = "select u.full_name, count(l.id) as assign_count, u.pie_color as color from leads l, users u where l.status not in (\"Closed\",\"Approved and Archived\") and l.duplicate_of=0 and u.id=l.assign_to and l.assign_to in (".$users.") group by u.id";
			
			
			$query = $this->db->query($manual_query);
			
			if (!empty($query->result())) {
			   return $query->result();
			}else{
			   return false;
			} 
		}
		return false;
		
		
	}
	
	public function getUsersVehiclesCount($uid)
	{
				
		$manual_query = "SELECT `v`.`id`, `v`.`name`, v.short_name, v.bubble_graph_color as color, count(lm.id) as scheduled_count FROM `leads_messages` `lm`, `vehicle_specific_names` `vs`, `vehicles` `v`, `leads` `l` WHERE `lm`.`message_id` = 5 and `lm`.`vehicle_specific_names_id` != '' and FIND_IN_SET (vs.id,lm.vehicle_specific_names_id) >0 and `v`.`id` = `vs`.`vehicle_id` and `l`.`id` = `lm`.`lead_id` AND `lm`.`created_by` = '".$uid."' AND `l`.`duplicate_of` = '0' and l.status not in (\"Finished\",\"Closed\",\"Approved and Archived\") GROUP BY `v`.`id`";		
		
		$query = $this->db->query($manual_query);
		
		if (!empty($query->result())) {
		   return $query->result();
		}else{
		   return false;
		} 
		
		
	}
	
	
	public function checkNoAction($leadIdsCommaSep)
	{		
		$manual_query = "SELECT group_concat(l.id) as commaIds from leads l where l.id in (".$leadIdsCommaSep.") and l.id not in (select lead_id from leads_messages lm where lm.message_id not in (16,19) and lm.lead_id in (".$leadIdsCommaSep.") )";		
		
		$query = $this->db->query($manual_query);
		
		if (!empty($query->result())) {
		   return $query->result_array();
		}else{
		   return array();
		} 
		
	}
	
}