<?php
Class Model_source extends Base_Model
{
	public function __construct()
	{
		parent::__construct("sources");
		
	}	

	public function getIds()
	{

		$this->db->select('id');
		$this->db->from('sources');

		$query = $this->db->get();
		
		$result = $query->result_array();
		//print_r($result); exit();
		//echo implode(',', $result); exit();

		$concatinatedIds = "";
		foreach($result as $key=>$value)
		{
			//print_r($value); exit();
			if($concatinatedIds) $concatinatedIds .= ",";
			$concatinatedIds .= 'manual_source'.$value['id'];
		}

		//echo $this->db->last_query(); exit();

		return $concatinatedIds;
			
	}
	
}