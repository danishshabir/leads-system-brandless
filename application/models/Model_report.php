<?php
Class Model_report extends Base_Model
{

public function __construct()
{
	parent::__construct("leads");
	
}


public function getTimeToMakeCallActionEachUserAvg($filterData, $userIds=0)
{
	$this->db->select("u.full_name, u.pie_color, u.id as userid, avg(TIMESTAMPDIFF(SECOND, l.assigned_at, first_call_time)) as avgval");
	$this->db->from("leads l, users u, leads_messages lm");
	$this->db->where("u.id=lm.created_by and lm.message_id in (8,9,10) and l.id=lm.lead_id and first_call_time!='0000-00-00 00:00:00' and assigned_at!='0000-00-00 00:00:00' and first_call_time > assigned_at and lm.created_by=l.assign_to");
	$this->db->where('l.duplicate_of', '0');
	
	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("lm.created_by in(".$filterData['depUsers'].")");	
	if($userIds) $this->db->where("lm.created_by in(".$userIds.")"); //for redar graphs 
	
	
	if(
	(!isset($filterData['date_from']) || (isset($filterData['date_from']) && $filterData['date_from']==""))
	&& (!isset($filterData['date_from']) || (isset($filterData['date_from']) && $filterData['date_from']==""))
	)
	{		
		$first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
		$last_day_this_month  = date('Y-m-t');

		$filterData['date_from'] = $first_day_this_month;
		$filterData['date_to'] = $last_day_this_month;
	}
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
	
	if(!$userIds)
	{
		$this->db->group_by('lm.created_by'); 
		$this->db->order_by('avgval'); 	
	}
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

/*public function getTimeToMakeCallActionEachUserMAvg($filterData)
{
	$manual_query = "select full_name, pie_color, userid, (sum(avgval))/count(userid) as avgval from ( SELECT u.full_name, u.pie_color, u.id as userid, avg(TIMESTAMPDIFF(SECOND, l.assigned_at, first_call_time)) as avgval FROM leads l, users u, leads_messages lm WHERE u.id = lm.created_by and lm.message_id in (8,9,10) and l.id = lm.lead_id and first_call_time != '0000-00-00 00:00:00' and assigned_at != '0000-00-00 00:00:00' and first_call_time > assigned_at and lm.created_by = l.assign_to AND l.duplicate_of = '0' "; 
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";
	
	$manual_query .= " GROUP BY YEAR(l.created_at), MONTH(l.created_at), lm.created_by ORDER BY u.id ) t1 group by t1.userid order by avgval";
	
	$query = $this->db->query($manual_query);
	
	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}*/

public function getTimeToFinishActionEachUserAvg($filterData, $userIds=0)
{
	$this->db->select("u.full_name, u.pie_color, u.id as userid, avg(TIMESTAMPDIFF(SECOND, l.assigned_at, finished_time)) as avgval");
	$this->db->from("leads l, users u, leads_messages lm");
	$this->db->where("u.id=lm.created_by and lm.message_id=7 and l.id=lm.lead_id and finished_time!='0000-00-00 00:00:00' and assigned_at!='0000-00-00 00:00:00' and finished_time > assigned_at and lm.created_by=l.assign_to");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('(l.status="Finished" or l.status="Closed" or l.status="Approved and Archived")');

	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("lm.created_by in(".$filterData['depUsers'].")");
	if($userIds) $this->db->where("lm.created_by in(".$userIds.")"); //for redar graphs

	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
	
	if(!$userIds)
	{
		$this->db->group_by('lm.created_by'); 
		$this->db->order_by('avgval'); 	
	}
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getTimeToCloseActionEachUserAvg($filterData)
{
	$this->db->select("u.full_name, u.pie_color, u.id as userid, avg(TIMESTAMPDIFF(SECOND, l.created_at, close_time)) as avgval");
	$this->db->from("leads l, users u, leads_messages lm");
	$this->db->where("u.id=lm.created_by and lm.message_id=11 and l.id=lm.lead_id and close_time!='0000-00-00 00:00:00'");
	$this->db->where('l.duplicate_of', '0');

	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("lm.created_by in(".$filterData['depUsers'].")");

	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	$this->db->group_by('lm.created_by'); 
	$this->db->order_by('avgval'); 	
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}


public function getNumOfDisapprovedActionEachUser($filterData, $userIds=0)
{
	$this->db->select("u.full_name, u.id as userid, count(u.id) as num");
	$this->db->from("leads l, users u, leads_messages lm");
	$this->db->where("u.id=l.assign_to and l.id=lm.lead_id and lm.message_id=14");
	$this->db->where('l.duplicate_of', '0');

	if($userIds) $this->db->where("u.id in(".$userIds.")"); //for redar graphs	
	else if(isset($filterData['user_id']) && $filterData['user_id']) $this->db->where("u.id", $filterData['user_id']);
	else if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("u.id in(".$filterData['depUsers'].")");
	
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.finished_time) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.finished_time) <=', $filterData['date_to']);
	
	if(!$userIds)
	{
		$this->db->group_by('u.id'); 
		$this->db->order_by('num','desc'); 	
	}
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getNumOfSuccessfullCallActionEachUser($filterData)
{
	$this->db->select("u.full_name, u.id as userid, count(u.id) as num");
	$this->db->from("users u, leads_messages lm, leads l");
	$this->db->where("u.id=lm.created_by and lm.message_id=9 and l.id=lm.lead_id");	
	$this->db->where('l.duplicate_of', '0');

	if(isset($filterData['user_id']) && $filterData['user_id']) $this->db->where("u.id", $filterData['user_id']);
	else if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("u.id in(".$filterData['depUsers'].")");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$this->db->group_by('u.id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getNumOfScheduledCallActionEachUser($filterData)
{
	$this->db->select("u.full_name, u.id as userid, count(u.id) as num");
	$this->db->from("users u, leads_messages lm, leads l");
	$this->db->where("u.id=lm.created_by and lm.message_id=8 and l.id=lm.lead_id");	
	$this->db->where('l.duplicate_of', '0'); 

	if(isset($filterData['user_id']) && $filterData['user_id']) $this->db->where("u.id", $filterData['user_id']);
	else if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("u.id in(".$filterData['depUsers'].")");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	$this->db->group_by('u.id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}


public function getNumOfNoAnsActionEachUser($filterData)
{
	$this->db->select("u.full_name, u.id as userid, count(u.id) as num");
	$this->db->from("users u, leads_messages lm, leads l");
	$this->db->where("u.id=lm.created_by and lm.message_id=10 and l.id=lm.lead_id");
	$this->db->where('l.duplicate_of', '0'); 

	if(isset($filterData['user_id']) && $filterData['user_id']) $this->db->where("u.id", $filterData['user_id']);
	else if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("u.id in(".$filterData['depUsers'].")");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	$this->db->group_by('u.id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getAllActionCountEachUser($filterData)
{
	$this->db->select("u.full_name, u.id as userid, lm.message_id, count(u.id) as num");
	$this->db->from("leads l, users u, leads_messages lm");
	$this->db->where("u.id=l.assign_to and l.id=lm.lead_id and (lm.message_id=10 or lm.message_id=9)");
	$this->db->where('l.duplicate_of', '0');
		
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	$this->db->group_by('u.full_name, u.id, lm.message_id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		//return $result = $query->result();
		$result = $query->result();
		
		$actionsCount = array();
		
		foreach($result as $row)
		{
			if($row->message_id == 9)
			{
				$actionsCount[$row->full_name]['successful_num'] = $row->num;
			}
			elseif($row->message_id == 10)
			{
				$actionsCount[$row->full_name]['no_answer_num'] = $row->num;
			}
		}

		foreach($actionsCount as $key1 => $row)
		{
			/*echo "<pre>";
			print_r($actionsCount[$key1]);
			print_r($row);
			*/
			
			$total_sum = 0;
			foreach($row as $childrow)		//summing up all values
			{
				$total_sum += $childrow;
			}
			foreach($row as $key2 =>$childrow)	//calculating percentages and saving them in array
			{
				/*
				echo "[" . $key1 ."][". $key2 . "]";
				echo "  ===";
				echo $childrow/$total_sum*100;
				*/
				if(!array_key_exists("successful_num", $actionsCount[$key1]))
				{
					$actionsCount[$key1]["successful_num"] = 0;
				}

				if(!array_key_exists("no_answer_num", $actionsCount[$key1]))
				{
					$actionsCount[$key1]["no_answer_num"] = 0;
				}
				
				if($actionsCount[$key1]["no_answer_num"] == 0 && $actionsCount[$key1]["successful_num"] == 0)
				{
					unset($actionsCount[$key1]);

				}

				
				if($key2 == "successful_num")
				{
					$actionsCount[$key1]["successful_per"] = $childrow/$total_sum*100;
				}
				else
				{
					$actionsCount[$key1]["no_answer_per"] = $childrow/$total_sum*100;
				}
			}
			
			/*			
			echo "<pre>";
			print_r($actionsCount[$key1]);
			exit();
			*/
		}
		
		
		return $actionsCount;
	}

	return null;
}



public function getAvgTimeOfNoAnsActionEachUser($filterData)
{
	$this->db->select("u.full_name, u.id as userid, count(u.id) as num, DATE_FORMAT(FROM_UNIXTIME(avg(UNIX_TIMESTAMP(TIME(lm.created_at)))),'%l:%i %p') as avgval");
	$this->db->from("users u, leads_messages lm, leads l");
	$this->db->where("u.id = lm.created_by and lm.message_id = 10 and l.id=lm.lead_id");	
	
	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("u.id in(".$filterData['depUsers'].")");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	$this->db->group_by('u.id'); 
	$this->db->order_by('avgval'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getAvgTimeOfSuccessfullCallActionEachUser($filterData)
{
	$this->db->select("u.full_name, u.id as userid, count(u.id) as num, DATE_FORMAT(FROM_UNIXTIME(avg(UNIX_TIMESTAMP(TIME(lm.created_at)))),'%l:%i %p') as avgval");
	$this->db->from("users u, leads_messages lm, leads l");
	$this->db->where("u.id = lm.created_by and lm.message_id = 9 and l.id=lm.lead_id");	
	$this->db->where('l.duplicate_of', '0');

	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("u.id in(".$filterData['depUsers'].")");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	$this->db->group_by('u.id'); 
	$this->db->order_by('avgval'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getHourTimeOfSuccessfullCallAction($filterData) 
{
	$this->db->select("count(u.id) as num, hour(lm.created_at) as timehour");
	$this->db->from("users u, leads_messages lm, leads l");
	$this->db->where("u.id = lm.created_by and lm.message_id = 9 and l.id=lm.lead_id");	
	$this->db->where('l.duplicate_of', '0');

	if(isset($filterData['user_id']) && $filterData['user_id']) $this->db->where("lm.created_by", $filterData['user_id']);
	else if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("lm.created_by in(".$filterData['depUsers'].")");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	$this->db->group_by('hour(lm.created_at)'); 
	$this->db->order_by('timehour');
	
	$query=$this->db->get();

	//echo $this->db->last_query(); exit(); 

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getNumOfClosedWithNoCallsActionEachUser($filterData)
{
	$innerQuery = "SELECT distinct lm.lead_id FROM leads_messages lm, leads l2 WHERE l2.id=lm.lead_id and l2.duplicate_of=0 and  lm.message_id in (8,9,10) ";
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$innerQuery .=" and date(lm.created_at) >= '".$filterData['date_from']."'";
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$innerQuery .=" and date(lm.created_at) <= '".$filterData['date_to']."'";

	$this->db->select("u.full_name, u.id as userid, count(l.assign_to) as num");
	$this->db->from("leads l, users u");
	$this->db->where("u.id=l.assign_to and l.status in('Closed','Approved and Archived') and l.assign_to!='0' and l.id not in (".$innerQuery.")");
	$this->db->where('l.duplicate_of', '0');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.close_time) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.close_time) <=', $filterData['date_to']);

	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("l.assign_to in(".$filterData['depUsers'].")");

	$this->db->group_by('l.assign_to'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

//New function for graph data -- 11. Leads closed with no calls 12. Leads closed with no answered calls 
public function getNumOfClosedLeadsWithNoCallAndNoAnswerAndEmail($filterData)
{
	$NoCallActionLeads = $this->getNumOfClosedWithNoCallsActionEachUser($filterData);
	$NoAnsweredLeads = $this->getNumOfClosedWithNoAnsweredEachUser($filterData);
	
	$actionsCount = array();
			
	if($NoCallActionLeads)
	foreach($NoCallActionLeads as $row)
	{
		$actionsCount[$row->full_name]['no_call']= $row->num;
	}

	if($NoAnsweredLeads)
	foreach($NoAnsweredLeads as $row)
	{
		$actionsCount[$row->full_name]['no_answered'] = $row->num;
	}
	
	foreach($actionsCount as $key =>$childrow)	//calculating percentages and saving them in array
	{
		if(!array_key_exists("no_call", $actionsCount[$key]))
		{
			$actionsCount[$key]["no_call"] = 0;
		}

		if(!array_key_exists("no_answered", $actionsCount[$key]))
		{
			$actionsCount[$key]["no_answered"] = 0;
		}

		if($actionsCount[$key]["no_call"] == 0 && $actionsCount[$key]["no_answered"] == 0)
		{
			unset($actionsCount[$key]);

		}
	}
	
	return $actionsCount;
	
	
}


public function getNumOfClosedWithNoCallsEmailsActionEachUser($filterData)
{
	$innerQuery = "SELECT distinct lm.lead_id FROM leads_messages lm, leads l2 WHERE l2.id=lm.lead_id and l2.duplicate_of=0 and lm.message_id in (2,8,9,10) ";
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$innerQuery .=" and date(lm.created_at) >= '".$filterData['date_from']."'";
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$innerQuery .=" and date(lm.created_at) <= '".$filterData['date_to']."'";

	$this->db->select("u.full_name, u.id as userid, count(l.assign_to) as num");
	$this->db->from("leads l, users u");
	$this->db->where("u.id=l.assign_to and l.status in('Closed','Approved and Archived') and l.assign_to!='0' and l.id not in (".$innerQuery.")");
	$this->db->where('l.duplicate_of', '0');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.close_time) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.close_time) <=', $filterData['date_to']);

	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("l.assign_to in(".$filterData['depUsers'].")");

	$this->db->group_by('l.assign_to'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}


public function getNumOfClosedWithNoAnsweredEachUser($filterData)
{
	$innerQuery = "select distinct le.id from leads le, leads_messages lem where le.id=lem.lead_id and le.duplicate_of=0  and (lem.message_id=8 or lem.message_id=9)";
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$innerQuery .=" and date(lem.created_at) >= '".$filterData['date_from']."'";
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$innerQuery .=" and date(lem.created_at) <= '".$filterData['date_to']."'";

	$this->db->select("u.full_name, u.id as userid, count(u.id) as num");
	$this->db->from("leads l, users u, leads_messages lm");
	$this->db->where("u.id=l.assign_to and l.id=lm.lead_id and lm.message_id=10");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.id NOT IN ('.$innerQuery.') ');
	$this->db->where('l.status', 'Approved and Archived');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.close_time) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.close_time) <=', $filterData['date_to']);

	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("u.id in(".$filterData['depUsers'].")");

	$this->db->group_by('u.id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getNumOfCarsEachUser($filterData)
{
	$users = array(); 
	$cars = array();
	$carsCompleted = array();

	$this->db->select("lm.created_by, lm.vehicle_specific_names_id, lm.test_drive_completed, lm.test_drive_type");
	$this->db->from("leads l,leads_messages lm");
	$this->db->where("l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!=''");
	$this->db->where('l.duplicate_of', '0');
	//$this->db->where('(lm.test_drive_completed=1 or lm.test_drive_type="Walk in")');
	//$this->db->where('(l.category_id=1 or l.category_id=34)');	
	
	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("lm.created_by in(".$filterData['depUsers'].")");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{
			$uId = $row->created_by;
			$car = $row->vehicle_specific_names_id;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			//$car = implode(",",$carsArr); //can be multiple in comma
			$car = $carsArr[0];
			//==			

			if(!in_array($uId, $users))
			{
				$users[] = $uId;
				$cars[$uId] = $car;
				if($row->test_drive_completed || $row->test_drive_type === "Walk in")
					$carsCompleted[$uId] = $car;
				else
					$carsCompleted[$uId] = ""; //to make the index
			}
			else
			{
				$cars[$uId] = $cars[$uId].','.$car;
				if($row->test_drive_completed || $row->test_drive_type === "Walk in")
					$carsCompleted[$uId] = $carsCompleted[$uId].','.$car;
			}
		}
	}
	
	return array($users,$cars,$carsCompleted);
}

public function getNumOfCarsEachBranch($filterData)
{
	$branchs = array();
	$cars = array();
	$carsCompleted = array();

	$this->db->select("l.branch_id, lm.vehicle_specific_names_id, lm.test_drive_completed, lm.test_drive_type");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where("l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!=''");
	//$this->db->where('(l.category_id=1 or l.category_id=34)');	
	$this->db->where('l.duplicate_of', '0');	
	
	$this->db->where("lm.created_by in(select user_id from struc_dep_users where struc_dep_id in (56, 57, 55, 52, 53, 54, 80))");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{
			$branch_id = $row->branch_id;
			$car = $row->vehicle_specific_names_id;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			//$car = implode(",",$carsArr); //can be multiple in comma
			$car = $carsArr[0];
			//==	

			if(!in_array($branch_id, $branchs))
			{
				$branchs[] = $branch_id;
				$cars[$branch_id] = $car;
				if($row->test_drive_completed || $row->test_drive_type === "Walk in")
					$carsCompleted[$branch_id] = $car;
				else
					$carsCompleted[$branch_id] = ""; //to make the index
			}
			else
			{
				$cars[$branch_id] = $cars[$branch_id].','.$car;
				if($row->test_drive_completed || $row->test_drive_type === "Walk in")
					$carsCompleted[$branch_id] = $carsCompleted[$branch_id].','.$car;
			}
		}
	}
	
	return array($branchs,$cars,$carsCompleted);
}

//it gives the general cars received from customer requests.
public function getNumOfAllCarsEachBranch($filterData)
{
	$branchs = array();
	$cars = array();	
	

	//$this->db->select("l.branch_id, l.vehicle");
	$this->db->select("l.branch_id, CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')) as vehicle");	
	
	$this->db->from("leads l");
	//$this->db->where("l.vehicle!=''");
	$this->db->where("(l.vehicle!='' or l.vehicle_specific_names_id!='')");	
	
	$this->db->where('l.duplicate_of', '0');
	
	$this->db->where("l.assign_to in(select user_id from struc_dep_users where struc_dep_id in (56, 57, 55, 52, 53, 54, 80))");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);

	//$this->db->where("l.created_at >=",'2016-09-01');
	//$this->db->where("l.created_at <=",'2016-09-30');
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{
			$branch_id = $row->branch_id;
			$car = $row->vehicle;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			$car = implode(",",$carsArr); //can be multiple in comma
			//==	

			if(!in_array($branch_id, $branchs))
			{
				$branchs[] = $branch_id;
				$cars[$branch_id] = $car;				
			}
			else
			{
				$cars[$branch_id] = $cars[$branch_id].','.$car;				
			}
		}
	}
	
	return array($branchs,$cars);
}

public function getAllCarsKSA($filterData)
{
	$requests = array();
	$cars = array();
	$carsCompleted = array();

	$this->db->select("lm.vehicle_specific_names_id, lm.test_drive_completed, lm.test_drive_type");
	$this->db->from("leads l,leads_messages lm");
	$this->db->where("l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!=''");
	//$this->db->where('(l.category_id=1 or l.category_id=34)');	
	$this->db->where('l.duplicate_of', '0');

	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{			
			$car = $row->vehicle_specific_names_id;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr); 
			//==	
			$carsArrTemp = array();
			$carsArrTemp[] = $carsArr[0];
			$carsArr = $carsArrTemp;
			
			foreach($carsArr as $cTD)
			{
				$cars[] = $cTD;

				if($row->test_drive_completed || $row->test_drive_type === "Walk in")
					$carsCompleted[] = $cTD;
			}

			
		}
	}

	//==

	//$this->db->select("l.vehicle");
	$this->db->select("CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')) as vehicle");	
	
	$this->db->from("leads l");
	//$this->db->where("l.vehicle!=''");
	$this->db->where("(l.vehicle!='' or l.vehicle_specific_names_id!='')");	
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	$this->db->where('l.duplicate_of', '0');
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{			
			$car = $row->vehicle;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);			
			
			foreach($carsArr as $cTD)
			{
				$requests[] = $cTD;
			}						
			
			
		}
	}



	
	return array($requests, $cars, $carsCompleted);
}

public function getNumOfCarsEachCity($filterData)
{
	$cities = array();
	$cars = array();
	$carsCompleted = array();

	$this->db->select("c.id, lm.vehicle_specific_names_id, lm.test_drive_completed, lm.test_drive_type");
	$this->db->from("leads l, leads_messages lm, branches b, cities c");
	$this->db->where("b.id = l.branch_id and c.id=b.city_id and l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!=''");
	//$this->db->where('(l.category_id=1 or l.category_id=34)');	
	$this->db->where('l.duplicate_of', '0');


	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{
			$city_id = $row->id;
			$car = $row->vehicle_specific_names_id;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			//$car = implode(",",$carsArr); //can be multiple in comma
			$car = $carsArr[0];
			//==	

			if(!in_array($city_id, $cities))
			{
				$cities[] = $city_id;
				$cars[$city_id] = $car;
				if($row->test_drive_completed || $row->test_drive_type === "Walk in")
					$carsCompleted[$city_id] = $car;
				else
					$carsCompleted[$city_id] = ""; //to make the index
			}
			else
			{
				$cars[$city_id] = $cars[$city_id].','.$car;
				if($row->test_drive_completed || $row->test_drive_type === "Walk in")
					$carsCompleted[$city_id] = $carsCompleted[$city_id].','.$car;
			}
		}
	}
	
	return array($cities,$cars,$carsCompleted);
}

//it gives the general cars received from customer requests.
public function getNumOfAllCarsEachCity($filterData)
{
	$cities = array();
	$cars = array();	

	//$this->db->select("c.id, l.vehicle");
	$this->db->select("c.id, CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')) as vehicle");	
	
	$this->db->from("leads l, branches b, cities c");
	$this->db->where("b.id = l.branch_id and c.id=b.city_id and (l.vehicle!='' or l.vehicle_specific_names_id!='')");
	//$this->db->where('(l.category_id=1 or l.category_id=34)');	
	$this->db->where('l.duplicate_of', '0');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{
			$city_id = $row->id;
			$car = $row->vehicle;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			$car = implode(",",$carsArr); //can be multiple in comma
			//==	

			if(!in_array($city_id, $cities))
			{
				$cities[] = $city_id;
				$cars[$city_id] = $car;				
			}
			else
			{
				$cars[$city_id] = $cars[$city_id].','.$car;				
			}
		}
	}
	
	return array($cities,$cars);
}


public function getNumOfSuccessDrivesEachBranch($filterData)
{
	$branchs = array();
	$cars = array();

	$this->db->select("l.branch_id, lm.vehicle_specific_names_id");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where("l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!='' and (lm.test_drive_completed=1 or lm.test_drive_type = 'Walk in')");
	//$this->db->where('(l.category_id=1 or l.category_id=34)');	
	$this->db->where('l.duplicate_of', '0');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{
			$branch_id = $row->branch_id;
			$car = $row->vehicle_specific_names_id;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			//$car = implode(",",$carsArr); //can be multiple in comma
			$car = $carsArr[0];
			//==	

			if(!in_array($branch_id, $branchs))
			{
				$branchs[] = $branch_id;
				$cars[$branch_id] = $car;
			}
			else
			{
				$cars[$branch_id] = $cars[$branch_id].','.$car;
			}
		}
	}
	
	return array($branchs,$cars);
}


public function getSuccessDrives($filterData) 
{
	$mainModels = array();
	$cars = array();

	$this->db->select("lm.vehicle_specific_names_id");
	$this->db->from("leads l,leads_messages lm");
	$this->db->where("l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!='' and (lm.test_drive_completed=1 or lm.test_drive_type = 'Walk in')");
	$this->db->where('l.duplicate_of', '0');
	//$this->db->where('(l.category_id=1 or l.category_id=34)');

	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{			
			$car = $row->vehicle_specific_names_id;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			//==	

			foreach($carsArr as $specCar)
			{


				$this->db->select("v.id");
				$this->db->from("vehicles v, vehicle_specific_names vs");
				$this->db->where("v.id=vs.vehicle_id");				
				$this->db->where("vs.id",$specCar);
				$query2=$this->db->get();
				$result2 = $query2->result();
				$generalCarId = $result2[0]->id; //general car id


				if(!in_array($generalCarId, $mainModels))
				{
					$mainModels[] = $generalCarId;
					$cars[$generalCarId] = $specCar;
				}
				else
				{
					$cars[$generalCarId] = $cars[$generalCarId].','.$specCar;
				}
				
			}

			
		}
	}
	//print_r($mainModels); exit();
	return array($mainModels,$cars);
}


public function getAllCarsKSAPerc($filterData) 
{
	$cars = array();
	$carsCompleted = array();

	$this->db->select("lm.vehicle_specific_names_id, lm.test_drive_completed, lm.test_drive_type");
	$this->db->from("leads l,leads_messages lm");
	$this->db->where("l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!=''");
	$this->db->where('l.duplicate_of', '0');	
	//$this->db->where('(l.category_id=1 or l.category_id=34)');	
		

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{			
			$car = $row->vehicle_specific_names_id;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			//==	

			/*foreach($carsArr as $cTD)
			{
				$cars[] = $cTD;

				if($row->test_drive_completed || $row->test_drive_type === 'Walk in')
					$carsCompleted[] = $cTD;
			}*/

			//now convert the specific to general
			for($ctgc=0; $ctgc<count($carsArr); $ctgc++)
			{
				$gId = $this->getGIdOfSpecific($carsArr[$ctgc]);
				$cars[] = $gId;

				if($row->test_drive_completed || $row->test_drive_type === 'Walk in')
					$carsCompleted[] = $gId;
			}

			
		}
	}
	
	return array($cars, $carsCompleted);
}

public function getCarsConversions($filterData) 
{
	/*$this->db->select("v.id, v.name, count(lm.id) as scheduled_count, sum(case when lm.test_drive_type='Walk in' or test_drive_completed=1 then 1 else 0 end) as completed_count");
	$this->db->from("leads_messages lm, vehicle_specific_names vs, vehicles v, leads l");
	$this->db->where("lm.message_id=5 and lm.vehicle_specific_names_id!='' and FIND_IN_SET (vs.id,lm.vehicle_specific_names_id)>0 and v.id=vs.vehicle_id and l.id=lm.lead_id");
	$this->db->where("lm.created_by = '".$filterData['user_id']."'");
	$this->db->where('l.duplicate_of', '0');

	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$this->db->group_by('v.name');*/
	
		
	/*$manual_query = "select  id1 as id, name1 as name, ifnull(requests_count, 0) as requests_count, ifnull(scheduled_count, 0) as scheduled_count, ifnull(completed_count, 0) as completed_count from (select * from (SELECT `v`.`id`, `v`.`name`, count(lm.id) as scheduled_count, sum(case when lm.test_drive_type='Walk in' or test_drive_completed=1 then 1 else 0 end) as completed_count 
	FROM `leads_messages` `lm`, `vehicle_specific_names` `vs`, `vehicles` `v`, `leads` `l` WHERE `lm`.`message_id` = 5 and `lm`.`vehicle_specific_names_id` != '' and FIND_IN_SET (vs.id,lm.vehicle_specific_names_id) >0 and `v`.`id` = `vs`.`vehicle_id` and `l`.`id` = `lm`.`lead_id` AND `lm`.`created_by` = '".$filterData['user_id']."' AND `l`.`duplicate_of` = '0' ";

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(l.assigned_at) >= '".$filterData['date_from']."'";
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(l.assigned_at) <= '".$filterData['date_to']."'";

	$manual_query .= " GROUP BY `v`.`name`) user_counts right outer join 
	(select * from
	(SELECT `v`.`id` as id1, `v`.`name` as name1, count(v.name) as requests_count  FROM `vehicles` `v`, `leads` `l` WHERE FIND_IN_SET (v.id,l.vehicle) > 0 and `l`.`assign_to` = '".$filterData['user_id']."' AND `l`.`duplicate_of` = '0' ";
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(l.assigned_at) >= '".$filterData['date_from']."'";
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(l.assigned_at) <= '".$filterData['date_to']."'";
	
	
	$manual_query .= " group by v.name) t1) total_requests
	on user_counts.id = total_requests.id1) combined_table";*/
	
	
	$manual_query = "select id1 as id, name1 as name, ifnull(requests_count, 0) as requests_count, ifnull(scheduled_count, 0) as scheduled_count, ifnull(completed_count, 0) as completed_count, ifnull(purchased_count, 0) as purchased_count from 

	(
		
	select * from (SELECT `v`.`id` as id1, `v`.`name` as name1, count(v.name) as requests_count FROM `vehicles` `v`, `leads` `l` WHERE FIND_IN_SET (v.id,(CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')))) > 0 and `l`.`assign_to` = '".$filterData['user_id']."' AND `l`.`duplicate_of` = '0'";


	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(l.assigned_at) >= '".$filterData['date_from']."'";
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(l.assigned_at) <= '".$filterData['date_to']."'";

	$manual_query .= " group by v.name) total_requests 
		
	left join 
		
	(select * from (SELECT `v`.`id`, `v`.`name`, count(lm.id) as scheduled_count, sum(case when lm.test_drive_type='Walk in' or test_drive_completed=1 then 1 else 0 end) as completed_count FROM `leads_messages` `lm`, `vehicle_specific_names` `vs`, `vehicles` `v`, `leads` `l` WHERE `lm`.`message_id` = 5 and `lm`.`vehicle_specific_names_id` != '' and FIND_IN_SET (vs.id,lm.vehicle_specific_names_id) >0 and `v`.`id` = `vs`.`vehicle_id` and `l`.`id` = `lm`.`lead_id` AND `lm`.`created_by` = '".$filterData['user_id']."' AND `l`.`duplicate_of` = '0'";

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";

	$manual_query .= " GROUP BY `v`.`name`) t1) user_counts     
	 
	on total_requests.id1 = user_counts.id
		
	left join 
		
	(select * from (SELECT `v`.`id` as id2, `v`.`name` as name2, count(l.id) as purchased_count FROM `leads_messages` `lm`, `vehicle_specific_names` `vs`, `vehicles` `v`, `leads` `l` WHERE `lm`.`message_id` = 4 and `lm`.`comments` like \"%Purchased a car%\" AND `v`.`id` = `vs`.`vehicle_id` and `vs`.`id` = `l`.`purchased_vehicle_id` and `l`.`id` = `lm`.`lead_id` and `l`.`tag_id` = 2 AND `lm`.`created_by` = '".$filterData['user_id']."' AND `l`.`duplicate_of` = '0'";
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";

	$manual_query .= " GROUP BY `v`.`name`) t2) user_counts2    
	 
	on total_requests.id1 = user_counts2.id2 
    
	) combined_table";
	
	
	//$query=$this->db->get();
	//echo $manual_query; exit();
	
	$query = $this->db->query($manual_query);
	
	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $query->result();		
	}
	
	return null;
}

public function getCarsConversions3($filterData) 
{
		
	$manual_query = "select id1 as id, ifnull(name1, (select v2.name from vehicles v2 where v2.id=combined_table.id2)) as name, ifnull(requests_count, 0) as requests_count, ifnull(purchased_count, 0) as purchased_count from 

	(
		
	select * from (SELECT `v`.`id` as id1, `v`.`name` as name1, count(v.name) as requests_count FROM `vehicles` `v`, `leads` `l` WHERE FIND_IN_SET (v.id,(CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')))) > 0 and `l`.`assign_to` = '".$filterData['user_id']."' AND `l`.`duplicate_of` = '0'";


	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(l.assigned_at) >= '".$filterData['date_from']."'";
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(l.assigned_at) <= '".$filterData['date_to']."'";

	$manual_query .= " group by v.name) total_requests 
		
	right join 
		
	(select * from (SELECT `v`.`id` as id2, `v`.`name` as name2, count(l.id) as purchased_count FROM `leads_messages` `lm`, `vehicle_specific_names` `vs`, `vehicles` `v`, `leads` `l` WHERE `lm`.`message_id` = 4 and `lm`.`comments` like \"%Purchased a car%\" AND `v`.`id` = `vs`.`vehicle_id` and `vs`.`id` = `l`.`purchased_vehicle_id` and `l`.`id` = `lm`.`lead_id` and `l`.`tag_id` = 2 AND `lm`.`created_by` = '".$filterData['user_id']."' AND `l`.`duplicate_of` = '0'";
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";

	$manual_query .= " GROUP BY `v`.`name`) t2) user_counts2    
	 
	on total_requests.id1 = user_counts2.id2 
    
	) combined_table";
	
	
	//$query=$this->db->get();
	//echo $manual_query; exit();
	
	$query = $this->db->query($manual_query);
	
	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $query->result();		
	}
	
	return null;
}

public function getCountUserLeads($filterData) 
{
	$this->db->select("count( l.id ) as num");
	$this->db->from("leads l");
	$this->db->where("l.assign_to",$filterData['user_id']);	
	$this->db->where('l.duplicate_of', '0');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
	
	$this->db->group_by('l.assign_to'); 
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->row();
		return $result->num;
		
	}
	
	return 0;

	

}

public function getCountUserScheduled($filterData) 
{
	$this->db->select("count( lm.id ) as num");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where('lm.lead_id = l.id and lm.message_id = 5');
	$this->db->where("lm.created_by",$filterData['user_id']);	
	$this->db->where('l.duplicate_of', '0');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
	
	$this->db->group_by('lm.created_by'); 
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->row();
		return $result->num;
		
	}
	
	return 0;

	

}

public function getCountUserCompleted($filterData) 
{
	$this->db->select("count( lm.id ) as num");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where('lm.lead_id = l.id and (lm.test_drive_type="Walk in" or test_drive_completed=1)');
	$this->db->where("lm.created_by",$filterData['user_id']);	
	$this->db->where('l.duplicate_of', '0');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
	
	$this->db->group_by('lm.created_by'); 
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->row();
		return $result->num;
		
	}
	
	return 0;

	

}

public function getCountUserPurchased($filterData) 
{
	$this->db->select("count( l.id ) as num");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where('l.tag_id', '2');
	$this->db->where('l.purchased_vehicle_id!="" and l.id=lm.lead_id and lm.message_id=4 and lm.comments like "%Purchased a car%"');
	$this->db->where("l.assign_to",$filterData['user_id']);	
	$this->db->where('l.duplicate_of', '0');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
	
	$this->db->group_by('l.assign_to'); 
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->row();
		return $result->num;
		
	}
	
	return 0;


	

}

public function getCountUserLost($filterData) 
{
	$this->db->select("count( l.id ) as num");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where('l.tag_id', '1');
	$this->db->where("l.assign_to",$filterData['user_id']);	
	$this->db->where('l.duplicate_of', '0');
	$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
	
	$this->db->group_by('l.assign_to'); 
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->row();
		return $result->num;
		
	}
	
	return 0;


	

}

public function getCarsConversions2($filterData) 
{
	$this->db->select("v.id, v.name, count(l.id) as purchased_count");
	$this->db->from("leads_messages lm, vehicle_specific_names vs, vehicles v, leads l");
	$this->db->where('lm.message_id=4 and lm.comments like "%Purchased a car%"'); //comment
	$this->db->where("v.id=vs.vehicle_id and vs.id=l.purchased_vehicle_id and l.id=lm.lead_id and l.tag_id=2");
	$this->db->where("lm.created_by = '".$filterData['user_id']."'");
	$this->db->where('l.duplicate_of', '0');

	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$this->db->group_by('v.name');
	
	$query=$this->db->get();
	
	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $query->result();		
	}
	
	return null;
}

public function getAllCarsPerSource($filterData)
{
	$cars = array();
	//$carsCategory = array();

	$this->db->select("lm.vehicle_specific_names_id, lm.test_drive_completed, lm.test_drive_type");
	$this->db->from("leads l,leads_messages lm");
	$this->db->where("l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!=''");
	$this->db->where('l.duplicate_of', '0');	
	//$this->db->where('(l.category_id=1 or l.category_id=34)');	
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{			
			$car = $row->vehicle_specific_names_id;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			//==	

			foreach($carsArr as $cTD)
			{
				$cars[] = $cTD;

				//if($row->test_drive_completed || $row->test_drive_type === 'Walk in')
				//$carsCategory[] = $cTD;
			}

			
		}
	}
	
	//return array($cars, $carsCategory);
	return array($cars);
}

public function getAllPurchCarsKSAPerc($filterData)
{
	$cars = array();
	$carsCompleted = array();
	$carsPurchased = array();

	$this->db->select("l.tag_id, lm.test_drive_completed, lm.vehicle_specific_names_id, lm.test_drive_type, l.purchased_vehicle_id");
	$this->db->from("leads l,leads_messages lm");
	$this->db->where("l.id=lm.lead_id");
	$this->db->where("lm.message_id=5 and lm.vehicle_specific_names_id!=''");
	$this->db->where('l.duplicate_of', '0');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{			
			$car = $row->vehicle_specific_names_id;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			//==	

			/*foreach($carsArr as $cTD)
			{
				$cars[] = $cTD;

				if($row->test_drive_completed || $row->test_drive_type === 'Walk in')
					$carsCompleted[] = $cTD;

				if($row->tag_id==="2")
					$carsPurchased[] = $cTD;
			}*/

			//now convert the specific to general
			for($ctgc=0; $ctgc<count($carsArr); $ctgc++)
			{
				$gId = $this->getGIdOfSpecific($carsArr[$ctgc]);
				$cars[] = $gId;

				if($row->test_drive_completed || $row->test_drive_type === 'Walk in')
					$carsCompleted[] = $gId;

				if($row->tag_id==="2" && $row->purchased_vehicle_id!="")
					$carsPurchased[] = $gId;
			}
			
		}
	}
	
	return array($cars, $carsCompleted, $carsPurchased); //scheduled/completed/purchased
}


public function getUserAvgScore($filterData, $userIds=0)
{
	$this->db->select("b.title, u.full_name, u.id as userid, avg( sr.answer_val ) as avgval");
	$this->db->from("leads l, users u, survey_results sr, branches b");
	$this->db->where("u.id=l.assign_to and l.survey_response!=0 and sr.lead_id=l.id and b.id = u.branch_id");
	$this->db->where('l.duplicate_of', '0');

	if($userIds) $this->db->where("u.id in(".$userIds.")"); //for redar graphs
	else if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("u.id in(".$filterData['depUsers'].")");
	

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}



public function getUserGraphAvgScore($filterData)
{
	$this->db->select("u.full_name, u.id as userid, avg( sr.answer_val ) as avgval");
	$this->db->from("leads l, users u, survey_results sr, branches b");
	$this->db->where("u.id=l.assign_to and l.survey_response!=0 and sr.lead_id=l.id");
	$this->db->where('l.duplicate_of', '0');

	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("u.id in(".$filterData['depUsers'].")");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);

	$this->db->group_by('u.full_name, u.id'); 
	$this->db->order_by('avgval','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result_array();
	}

	return null;
}

public function getUserAvgScore1($filterData)
{
	$this->db->select("u.full_name, u.id as userid, avg( sr.answer_val ) as avgval");
	$this->db->from("leads l, users u, survey_results sr");
	$this->db->where("u.id=l.assign_to and l.survey_response!=0 and sr.lead_id=l.id");
	$this->db->where('l.duplicate_of', '0');

	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("l.assign_to in(".$filterData['depUsers'].")");

	//don't apply filter here because this is helping outer function.
	//if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	//if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);

	$this->db->group_by('l.assign_to'); 
	//$this->db->order_by('avgval','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getUserAvgScore2($filterData)
{
	$this->db->select("u.full_name, u.id as userid, avg( sr.answer_val ) as avgval");
	$this->db->from("leads l, users u, survey_results sr");
	$this->db->where("u.id=l.assign_to and l.survey_response!=0 and sr.lead_id=l.id");
	$this->db->where('l.duplicate_of', '0');

	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("l.assign_to in(".$filterData['depUsers'].")");

	//don't apply filter here because this is helping outer function.
	//if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	//if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);

	$this->db->group_by('l.assign_to'); 
	$this->db->order_by('avgval','desc'); 
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getUserAvgScoreAsUidKey($filterData)
{
	$userScoreArr = array();

	$this->db->select("u.id as userid, avg( sr.answer_val ) as avgval");
	$this->db->from("leads l, users u, survey_results sr");
	$this->db->where("u.id=l.assign_to and l.survey_response!=0 and sr.lead_id=l.id");
	$this->db->where('l.duplicate_of', '0');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);

	$this->db->group_by('u.id'); 
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		$userScoreArr[$row->userid] = $row->avgval;
	}

	return $userScoreArr;
}

public function getUserGraphAvgScoreAsUidKey($filterData)
{
	$userScoreArr = array();

	$this->db->select("u.id as userid, avg( sr.answer_val ) as avgval");
	$this->db->from("leads l, users u, survey_results sr");
	$this->db->where("u.id=l.assign_to and l.survey_response!=0 and sr.lead_id=l.id and l.assign_to = '".$filterData['user_id']."'");
	$this->db->where('l.duplicate_of', '0');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);

	$this->db->group_by('u.id'); 
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		$userScoreArr[$row->userid] = $row->avgval;
	}

	return $userScoreArr;
}

public function getUserDepsAvgScore($depsOtherUsers,$filterData)
{
	$this->db->select("avg( sr.answer_val ) as avgval");
	$this->db->from("leads l, survey_results sr");
	$this->db->where("l.survey_response!=0 and sr.lead_id=l.id and l.assign_to in (".$depsOtherUsers.")");
	$this->db->where('l.duplicate_of', '0');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;	
	
}

public function getUserGraphDepsAvgScore($filterData)
{
	$this->db->select("avg( sr.answer_val ) as avgval");
	$this->db->from("leads l, survey_results sr");	
	$this->db->where('l.duplicate_of', '0');
	
	if(isset($filterData['depUsers']) && $filterData['depUsers']!="")
	$this->db->where("l.survey_response!=0 and sr.lead_id=l.id and l.assign_to in (".$filterData['depUsers'].")");
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;	
	
}

//Shahid - 3 Nov 2016 - new function to get depts average

public function getDepsAvgScore($filterData)
{
	$this->db->select("b.title, avg( sr.answer_val ) as avgval");
	$this->db->from("leads l, survey_results sr, branches b");
	$this->db->where("l.survey_response!=0 and sr.lead_id=l.id and l.branch_id = b.id");
	$this->db->where('l.duplicate_of', '0');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;	
	
}


public function getTotalAvgScore($filterData)
{
	$this->db->select("avg( sr.answer_val ) as avgval");
	$this->db->from("survey_results sr, leads l");
	$this->db->where("l.survey_response!=0 and sr.lead_id=l.id");
	$this->db->where('l.duplicate_of', '0');	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('sr.created_at >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('sr.created_at <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;
}

public function getTotalAvgScoreYearly($userIds,$filterData)
{
	$userYearlyAvg = array();
	if(count($userIds)>0)
	{
		foreach($userIds as $uId)
		{
			$this->db->select("DATE_FORMAT(sr.created_at,'%b %Y') as month, avg( sr.answer_val ) as avgval");
			$this->db->from("leads l, survey_results sr");
			$this->db->where("l.survey_response!=0 and sr.lead_id=l.id and l.assign_to = '".$uId."'");
			$this->db->where('l.duplicate_of', '0');
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);
			$this->db->group_by('YEAR(sr.created_at), MONTH(sr.created_at)'); 
			$this->db->order_by('avgval','desc'); 	

			
			$query=$this->db->get();

			//echo $this->db->last_query();exit();

			if($query->num_rows() > 0)
			{
				$result = $query->result();
				foreach($result as $row)
					$userYearlyAvg[$uId][$row->month] = $row->avgval;
			}

		}
	}

	return $userYearlyAvg;

	
}

public function getUGraphTotalAvgScoreYearly($filterData)
{
	$userYearlyAvg = array();
			
	$this->db->select("DATE_FORMAT(sr.created_at,'%b %Y') as month, avg( sr.answer_val ) as avgval");
	$this->db->from("leads l, survey_results sr");
	$this->db->where("l.survey_response!=0 and sr.lead_id=l.id and l.assign_to = '".$filterData['user_id']."'");
	$this->db->where('l.duplicate_of', '0');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);
	$this->db->group_by('YEAR(sr.created_at), MONTH(sr.created_at)'); 

	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();
	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
			$userYearlyAvg[$filterData['user_id']][$row->month] = $row->avgval;
	}
		
	return $userYearlyAvg;

	
}

public function getUGraphGradeAvgScoreYearly($filterData)
{
	$userYearlyAvg = array();
			
	$this->db->select("DATE_FORMAT(sr.created_at,'%b %Y') as month, sum( sr.answer_val ) as actual_score, sum(l.survey_response) as surveys_completed");
	$this->db->from("leads l, survey_results sr");
	$this->db->where("l.survey_response!=0 and sr.lead_id=l.id and l.assign_to = '".$filterData['user_id']."'");
	$this->db->where('l.duplicate_of', '0');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);
	$this->db->group_by('YEAR(sr.created_at), MONTH(sr.created_at)'); 

	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();
	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{
			$userYearlyAvg[$filterData['user_id']][$row->month]['actual_score'] = $row->actual_score;
			$userYearlyAvg[$filterData['user_id']][$row->month]['surveys_completed'] = $row->surveys_completed;
		}
	}
		
	return $userYearlyAvg;

	
}

public function getUGraphTotalLeadsYearly($filterData)
{
	$userYearlyAvg = array();

	/*$this->db->select("DATE_FORMAT(l.assigned_at,'%b %Y') as month, count( u.id ) as num");
	$this->db->from("leads l, users u");
	$this->db->where("u.id=l.assign_to and l.assign_to = '".$filterData['user_id']."'");
	$this->db->where('l.duplicate_of', '0');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
	
	$this->db->group_by('YEAR(l.assigned_at), MONTH(l.assigned_at)'); 

	$query=$this->db->get();*/
	
	$queryStr = "SELECT DATE_FORMAT(l.assigned_at, '%b %Y') as month, count( l.id ) as num FROM `leads` `l`, `users` `u` WHERE `u`.`id` = `l`.`assign_to` and `l`.`assign_to` = '".$filterData['user_id']."' AND `l`.`duplicate_of` = '0' GROUP BY YEAR(l.assigned_at), MONTH(l.assigned_at)";
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$queryStr .= " and date(l.assigned_at) >= '".$filterData['date_from']."'";
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$queryStr .= " and date(l.assigned_at) <= '".$filterData['date_to']."'";
	
	/*$queryStr .= "union SELECT DATE_FORMAT(l.assigned_at, '%b %Y') as month, count( u.id ) as num FROM `leads` `l`, `users` `u` WHERE `u`.`id` = `l`.`created_by` and `l`.`created_by` = '".$filterData['user_id']."' AND `l`.`assign_to` != '".$filterData['user_id']."' AND `l`.`duplicate_of` = '0' GROUP BY YEAR(l.created_at), MONTH(l.created_at)";
	
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$queryStr .= " and date(l.created_at) >= '".$filterData['date_from']."'";
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$queryStr .= " and date(l.created_at) <= '".$filterData['date_to']."'";*/ 
	
	//echo $queryStr;
	
	$query = $this->db->query($queryStr);

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{	
			if(isset($userYearlyAvg[$filterData['user_id']][$row->month]))
				$userYearlyAvg[$filterData['user_id']][$row->month] += $row->num;
			else
				$userYearlyAvg[$filterData['user_id']][$row->month] = $row->num;
		}
	}
	return $userYearlyAvg;

	
}

public function getUGraphTotalComLeadsYearly($filterData)
{
	$userYearlyAvg = array();

	$this->db->select("DATE_FORMAT(lm.created_at,'%b %Y') as month, count( lm.created_by ) as num");
	$this->db->from("leads_messages lm, leads l");
	$this->db->where("lm.created_by = '".$filterData['user_id']."' and lm.message_id=5 and lm.vehicle_specific_names_id!='' and l.id=lm.lead_id");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('(lm.test_drive_completed=1 or lm.test_drive_type="Walk in")');
	
	
	//$this->db->where('l.duplicate_of', '0');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	$this->db->group_by('YEAR(lm.created_at), MONTH(lm.created_at)'); 

	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
			$userYearlyAvg[$filterData['user_id']][$row->month] = $row->num;
	}
	return $userYearlyAvg;

	
}


public function getUserTLPM($filterData)
{
	$this->db->select("u.full_name, u.id as userid, count( u.id ) as num");
	$this->db->from("leads l, users u");
	$this->db->where("u.id=l.assign_to");
	$this->db->where('l.duplicate_of', '0');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);

	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("l.assign_to in(".$filterData['depUsers'].")");

	$this->db->group_by('l.assign_to'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getUserClosedTLPM($filterData)
{
	$this->db->select("u.full_name, u.id as userid, count( u.id ) as num");
	$this->db->from("leads l, users u, leads_messages lm");
	$this->db->where("u.id=lm.created_by and l.id=lm.lead_id");
	$this->db->where("lm.message_id","11");
	$this->db->where('l.duplicate_of', '0');
	//have the following check so if the lead was not diapproved after finished. Actually the finished is counted from the message_id=7 e.g finished messages.
	$this->db->where('(l.status="Closed" or l.status="Approved and Archived")');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.close_time) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.close_time) <=', $filterData['date_to']);

	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("u.id in(".$filterData['depUsers'].")");

	$this->db->group_by('u.id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getUserFinishedTLPM($filterData)
{
	$this->db->select("u.full_name, u.id as userid, count( u.id ) as num");
	$this->db->from("leads l, users u, leads_messages lm");
	$this->db->where("u.id=lm.created_by and l.id=lm.lead_id");
	$this->db->where("lm.message_id","7");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where("finished_time!='0000-00-00 00:00:00' and assigned_at!='0000-00-00 00:00:00' and finished_time > assigned_at and lm.created_by=l.assign_to");
	//have the following check so if the lead was not diapproved after finished. Actually the finished is counted from the message_id=7 e.g finished messages.
	$this->db->where('(l.status="Finished" or l.status="Closed" or l.status="Approved and Archived")');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.finished_time) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.finished_time) <=', $filterData['date_to']);

	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("u.id in(".$filterData['depUsers'].")");

	$this->db->group_by('u.id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}


public function getBranchClosedTLPM($filterData)
{
	$this->db->select("b.title, count( l.branch_id ) as num");
	$this->db->from("leads l, branches b, leads_messages lm");
	$this->db->where("b.id=l.branch_id and l.id=lm.lead_id AND b.id not in (1,7,17,18,20,22)");
	$this->db->where("lm.message_id","11");
	$this->db->where('l.duplicate_of', '0');
	//have the following check so if the lead was not diapproved after finished. Actually the finished is counted from the message_id=7 e.g finished messages.
	$this->db->where('(l.status="Closed" or l.status="Approved and Archived")');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.branch_id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getOrgStrucBranchClosedTLPM($filterData)
{
	$this->db->select("sd.title, count(lm.created_by) as num");
	$this->db->from("leads l, leads_messages lm, struc_dep_users sdu,structure_departments sd");	
	$this->db->where("l.id=lm.lead_id and `l`.`assign_to` in(select user_id from struc_dep_users where struc_dep_id in (56, 57, 55, 52, 53, 54, 80)) and l.assign_to=sdu.user_id and sd.id=sdu.struc_dep_id");
	$this->db->where("lm.message_id","11");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('(l.status="Closed" or l.status="Approved and Archived")');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.close_time) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.close_time) <=', $filterData['date_to']);
	$this->db->group_by('sdu.struc_dep_id');
	$this->db->order_by('sd.title');
	
	
	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getNoBranchClosedTLPM($filterData)
{
	$this->db->select("count( l.branch_id ) as num");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where("l.id=lm.lead_id");
	$this->db->where("lm.message_id","11");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.branch_id', '0');
	//have the following check so if the lead was not diapproved after finished. Actually the finished is counted from the message_id=7 e.g finished messages.
	$this->db->where('(l.status="Closed" or l.status="Approved and Archived")');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.branch_id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;
}

public function getBranchFinishedTLPM($filterData)
{
	$this->db->select("b.title, count( l.branch_id ) as num");
	$this->db->from("leads l, branches b, leads_messages lm");
	$this->db->where("b.id=l.branch_id and l.id=lm.lead_id AND b.id not in (1,7,17,18,20,22)");
	$this->db->where("lm.message_id","7");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where("finished_time!='0000-00-00 00:00:00' and assigned_at!='0000-00-00 00:00:00' and finished_time > assigned_at and lm.created_by=l.assign_to");
	//have the following check so if the lead was not diapproved after finished. Actually the finished is counted from the message_id=7 e.g finished messages.
	$this->db->where('(l.status="Finished" or l.status="Closed" or l.status="Approved and Archived")');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.finished_time) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.finished_time) <=', $filterData['date_to']);
	$this->db->group_by('l.branch_id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getOrgStrucBranchFinishedTLPM($filterData)
{
	$this->db->select("sd.title, count(lm.created_by) as num");
	$this->db->from("leads l, leads_messages lm, struc_dep_users sdu,structure_departments sd");	
	$this->db->where("l.id=lm.lead_id and `l`.`assign_to` in(select user_id from struc_dep_users where struc_dep_id in (56, 57, 55, 52, 53, 54, 80)) and l.assign_to=sdu.user_id and sd.id=sdu.struc_dep_id");
	$this->db->where("finished_time!='0000-00-00 00:00:00' and assigned_at!='0000-00-00 00:00:00' and finished_time > assigned_at and lm.created_by=l.assign_to");
	$this->db->where("lm.message_id","7");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('(l.status="Finished" or l.status="Closed" or l.status="Approved and Archived")');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.finished_time) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.finished_time) <=', $filterData['date_to']);
	$this->db->group_by('sdu.struc_dep_id');
	$this->db->order_by('sd.title');

	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();		
	}
	
	return null;
	
}

public function getNoBranchFinishedTLPM($filterData)
{
	$this->db->select("count( l.branch_id ) as num");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where("l.id=lm.lead_id");
	$this->db->where("lm.message_id","7");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.branch_id', '0');
	$this->db->where("finished_time!='0000-00-00 00:00:00' and assigned_at!='0000-00-00 00:00:00' and finished_time > assigned_at and lm.created_by=l.assign_to");
	//have the following check so if the lead was not diapproved after finished. Actually the finished is counted from the message_id=7 e.g finished messages.
	$this->db->where('(l.status="Finished" or l.status="Closed" or l.status="Approved and Archived")');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.finished_time) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.finished_time) <=', $filterData['date_to']);
	$this->db->group_by('l.branch_id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;
}

public function getUserTLPM1($filterData)
{
	$this->db->select("u.full_name, u.id as userid, count( u.id ) as num");
	$this->db->from("leads l, users u");
	$this->db->where("u.id=l.assign_to");
	$this->db->where('l.duplicate_of', '0');

	//don't apply filter here because this is helping outer function.
	//if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	//if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);

	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("l.assign_to in(".$filterData['depUsers'].")");

	$this->db->group_by('l.assign_to'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getTotalLeadsYearly($userIds,$filterData)
{
	$userYearlyAvg = array();
	if(count($userIds)>0)
	{
		foreach($userIds as $uId)
		{
			$this->db->select("DATE_FORMAT(l.assigned_at,'%b %Y') as month, count( u.id ) as num");
			$this->db->from("leads l, users u");
			$this->db->where("u.id=l.assign_to and l.assign_to = '".$uId."'");
			$this->db->where('l.duplicate_of', '0');
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
			$this->db->group_by('YEAR(l.assigned_at), MONTH(l.assigned_at)'); 

			
			$query=$this->db->get();

			//echo $this->db->last_query();exit();

			if($query->num_rows() > 0)
			{
				$result = $query->result();
				foreach($result as $row)
					$userYearlyAvg[$uId][$row->month] = $row->num;
			}

		}
	}

	return $userYearlyAvg;

	
}

public function getBranchTLPM($filterData)
{
	$this->db->select("b.title, b.id as branchid, count( b.id ) as num");
	$this->db->from("leads l, branches b");
	$this->db->where("b.id=l.branch_id AND b.id not in (1,7,17,18,20,22)");
	$this->db->where('l.duplicate_of', '0');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.branch_id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}


public function getBranchTLPM1($filterData)
{
	$this->db->select("b.title, b.id as branchid, count( b.id ) as num");
	$this->db->from("leads l, branches b");
	$this->db->where("b.id=l.branch_id");
	$this->db->where('l.duplicate_of', '0');

	//don't apply filter here because this is helping outer function.
	//if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	//if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);

	$this->db->group_by('l.branch_id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getTotalBranchLeadsYearly($branchIds,$filterData)
{
	$branchYearlyAvg = array();
	if(count($branchIds)>0)
	{
		foreach($branchIds as $bId)
		{
			$this->db->select("DATE_FORMAT(l.created_at,'%b %Y') as month, count( b.id ) as num");
			$this->db->from("leads l, branches b");
			$this->db->where("b.id=l.branch_id and l.branch_id = '".$bId."' AND b.id not in (1,7,17,18,20,22)");
			$this->db->where('l.duplicate_of', '0');
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
			$this->db->group_by('YEAR(l.created_at), MONTH(l.created_at)'); 

			
			$query=$this->db->get();

			//echo $this->db->last_query();exit();

			if($query->num_rows() > 0)
			{
				$result = $query->result();
				foreach($result as $row)
					$branchYearlyAvg[$bId][$row->month] = $row->num;
			}

		}
	}

	return $branchYearlyAvg;

	
}

public function getOrgStrucDepLeadsYearly($filterData)
{
	$this->db->select("DATE_FORMAT(l.assigned_at,'%b %Y') as month, 
	sum(case when sdu.struc_dep_id=54 then 1 else 0 end) as `Abha Showroom`,
	sum(case when sdu.struc_dep_id=80 then 1 else 0 end) as `Madina Showroom`,
	sum(case when sdu.struc_dep_id=53 then 1 else 0 end) as `Automall Showroom`,
	sum(case when sdu.struc_dep_id=57 then 1 else 0 end) as `Dammam Showroom`,
	sum(case when sdu.struc_dep_id=55 then 1 else 0 end) as `Khurais Showroom`,
	sum(case when sdu.struc_dep_id=52 then 1 else 0 end) as `Madina Road Showroom`,
	sum(case when sdu.struc_dep_id=56 then 1 else 0 end) as `Uroubah Showroom`,
	");
	$this->db->from("leads l,struc_dep_users sdu,structure_departments sd");
	$this->db->where("`l`.`assign_to` in(select user_id from struc_dep_users where struc_dep_id in (56, 57, 55, 52, 53, 54, 80)) and l.assign_to=sdu.user_id and sd.id=sdu.struc_dep_id");
	$this->db->where('l.duplicate_of', '0');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
	$this->db->group_by('YEAR(l.assigned_at), MONTH(l.assigned_at)'); 	

	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();		
	}
	
	return null;
	
}

public function getOrgStrucDepLeads($filterData)
{
	$this->db->select("sd.title, count(l.assign_to) as num");
	$this->db->from("leads l,struc_dep_users sdu,structure_departments sd");
	$this->db->where("`l`.`assign_to` in(select user_id from struc_dep_users where struc_dep_id in (56, 57, 55, 52, 53, 54, 80)) and l.assign_to=sdu.user_id and sd.id=sdu.struc_dep_id");
	$this->db->where('l.duplicate_of', '0');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
	$this->db->group_by('sdu.struc_dep_id');
	$this->db->order_by('sd.title');

	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();		
	}
	
	return null;
	
}


public function getNumberOfLeadsPerCategories($filterData)
{
	$this->db->select("c.title, c.id as category_id, count( c.id ) as num");
	$this->db->from("leads l, categories c");
	$this->db->where("c.id=l.category_id");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.category_id!=', '0');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.category_id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getUGraphLeadSource($filterData)
{
	/*$this->db->select("l.form_type as source, count( l.form_type ) as num");
	$this->db->from("leads l");	
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.manual_source', '');
	$this->db->where("l.assign_to = '".$filterData['user_id']."'");
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.form_type'); 
	//$this->db->order_by('num','desc');
	*/
	
	$queryStr = "SELECT l.form_type as source, count( l.form_type ) as num FROM leads l WHERE l.duplicate_of = '0' AND l.manual_source = '' AND l.assign_to = '".$filterData['user_id']."'";
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$queryStr .= " and date(l.assigned_at) >= '".$filterData['date_from']."'";
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$queryStr .= " and date(l.assigned_at) <= '".$filterData['date_to']."'";
	
	$queryStr .= " GROUP BY l.form_type";
	
	
	
	$queryStr .= " union SELECT s.title as source, count( l.manual_source ) as num FROM leads l, sources s WHERE l.manual_source=s.id and l.duplicate_of = '0' AND l.manual_source != 0 and l.form_type='' AND l.assign_to = '".$filterData['user_id']."'";
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$queryStr .= " and date(l.assigned_at) >= '".$filterData['date_from']."'";
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$queryStr .= " and date(l.assigned_at) <= '".$filterData['date_to']."'";
	
	$queryStr .= " GROUP BY l.manual_source";
	
	$query = $this->db->query($queryStr);
	
	//$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getUGraphLeadSource2($filterData)
{
	/*$this->db->select("l.form_type as source, count( l.form_type ) as num");
	$this->db->from("leads l");	
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.manual_source', '');
	$this->db->where("l.assign_to = '".$filterData['user_id']."'");
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.form_type'); 
	//$this->db->order_by('num','desc');
	*/
	
	$first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
	$last_day_this_month  = date('Y-m-t');

	$date_from = $first_day_this_month;
	$date_to = $last_day_this_month;
	
	$queryStr = "SELECT l.form_type as source, count( l.form_type ) as num FROM leads l WHERE l.duplicate_of = '0' AND l.manual_source = '' AND l.assign_to = '".$filterData['user_id']."'";
	
	$queryStr .= " and date(l.assigned_at) >= '".$date_from."'";
	$queryStr .= " and date(l.assigned_at) <= '".$date_to."'";
	
	$queryStr .= " GROUP BY l.form_type";
	
	
	
	$queryStr .= " union SELECT s.title as source, count( l.manual_source ) as num FROM leads l, sources s WHERE l.manual_source=s.id and l.duplicate_of = '0' AND l.manual_source != 0 and l.form_type='' AND l.assign_to = '".$filterData['user_id']."'";
	
	$queryStr .= " and date(l.assigned_at) >= '".$date_from."'";
	$queryStr .= " and date(l.assigned_at) <= '".$date_to."'";
	
	$queryStr .= " GROUP BY l.manual_source";
	
	//echo $queryStr; exit();
	
	$query = $this->db->query($queryStr);
	
	//$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}


public function getNumberOfCompletedTestDrivesPerCategories($filterData)
{
	/*$this->db->select("c.title, c.id as category_id, count( c.id ) as num");
	$this->db->from("leads l, categories c, leads_messages lm");
	$this->db->where("c.id=l.category_id");
	$this->db->where("lm.lead_id=l.id");
	$this->db->where('l.duplicate_of', '0');
	//$this->db->where('(l.category_id=1 or l.category_id=34)');
	$this->db->where("lm.message_id=5 and lm.vehicle_specific_names_id!=''");
	$this->db->where('(lm.test_drive_completed=1 or lm.test_drive_type="Walk in")');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);

	$this->db->group_by('l.category_id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;*/
	
	$this->db->select("CONCAT(form_type,'',IF(manual_source = '0', '', (select s.title from sources s where s.id=l.manual_source))) as title, count( * ) as num");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where("lm.lead_id=l.id");
	$this->db->where('l.duplicate_of', '0');
	//$this->db->where('(l.category_id=1 or l.category_id=34)');
	$this->db->where("lm.message_id=5 and lm.vehicle_specific_names_id!=''");
	$this->db->where('(lm.test_drive_completed=1 or lm.test_drive_type="Walk in")');
	
	$this->db->where('l.assign_to in(select user_id from struc_dep_users where struc_dep_id in (56, 57, 55, 52, 53, 54, 80))');

	// 6th July 2017
	//if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	//if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);

	$this->db->group_by('l.form_type, manual_source'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
	
}

/*public function getNumberOfCompletedTestDrivesPerWebsiteSource($filterData)
{
	$this->db->select("l.form_type as title, count( l.form_type ) as num");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where("lm.lead_id=l.id");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.category_id', '0');
	$this->db->where('(lm.test_drive_completed=1 or lm.test_drive_type="Walk in")');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.form_type'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}*/


public function getNumberOfFinishedPerCategories($filterData)
{
	$this->db->select("c.title, c.id as category_id, count( c.id ) as num");
	$this->db->from("leads l, categories c, leads_messages lm");
	$this->db->where("c.id=l.category_id");
	$this->db->where("lm.lead_id=l.id");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.category_id!=', '0');
	$this->db->where('lm.message_id','7'); //7 is finished
	$this->db->where("finished_time!='0000-00-00 00:00:00' and assigned_at!='0000-00-00 00:00:00' and finished_time > assigned_at and lm.created_by=l.assign_to");
	$this->db->where('(l.status="Finished" or l.status="Closed" or l.status="Approved and Archived")');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.finished_time) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.finished_time) <=', $filterData['date_to']);

	$this->db->group_by('l.category_id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

//following is for which the category is not assigned
public function getNumberOfFinishedPerWebsiteSource($filterData)
{
	$this->db->select("l.form_type as title, count( l.form_type ) as num");
	$this->db->from("leads l , leads_messages lm");	
	$this->db->where("lm.lead_id=l.id");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.category_id', '0');
	$this->db->where('lm.message_id','7'); //7 is finished
	$this->db->where("finished_time!='0000-00-00 00:00:00' and assigned_at!='0000-00-00 00:00:00' and finished_time > assigned_at and lm.created_by=l.assign_to");
	$this->db->where('(l.status="Finished" or l.status="Closed" or l.status="Approved and Archived")');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.finished_time) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.finished_time) <=', $filterData['date_to']);

	$this->db->group_by('l.form_type'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getNumberOfLostPerCategories($filterData)
{
	$this->db->select("c.title, c.id as category_id, count( c.id ) as num");
	$this->db->from("leads l, categories c, leads_messages lm");
	$this->db->where("c.id=l.category_id");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.category_id!=', '0');
	$this->db->where('l.tag_id', '1'); //1=lost
	$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	
	//$this->db->where('(l.status="Finished" or l.status="Closed" or l.status="Approved and Archived")');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.category_id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}


public function getNumberOfPurchPerCategories($filterData)
{
	/*$this->db->select("c.title, c.id as category_id, count( c.id ) as num");
	$this->db->from("leads l, categories c");
	$this->db->where("c.id=l.category_id");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.category_id!=', '0');
	$this->db->where('l.tag_id', '2'); //2=purchased
	//$this->db->where('(l.status="Finished" or l.status="Closed" or l.status="Approved and Archived")');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.category_id'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;*/
	
	$this->db->select("CONCAT(form_type,'',IF(manual_source = '0', '', (select s.title from sources s where s.id=l.manual_source))) as title, count( * ) as num");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.tag_id', '2'); //2=purchased
	$this->db->where('l.id=lm.lead_id and lm.message_id=4 and lm.comments like "%Purchased a car%" and l.purchased_vehicle_id!=""');
	//$this->db->where('(l.status="Finished" or l.status="Closed" or l.status="Approved and Archived")');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.form_type, manual_source');  
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
	
}

//following is for which the category is not assigned
public function getNumberOfLostPerWebsiteSource($filterData)
{
	$this->db->select("l.form_type as title, count( l.form_type ) as num");
	$this->db->from("leads l, leads_messages lm");	
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.category_id', '0');
	$this->db->where('l.tag_id', '1'); //1=lost
	$this->db->where('l.form_type!=', ''); 
	$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	
	//$this->db->where('(l.status="Finished" or l.status="Closed" or l.status="Approved and Archived")');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.form_type'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}


//following is for which the category is not assigned
public function getNumberOfPurchPerWebsiteSource($filterData)
{
	$this->db->select("l.form_type as title, count( l.form_type ) as num");
	$this->db->from("leads l");	
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.category_id', '0');
	$this->db->where('l.tag_id', '2'); //2=purchased
	$this->db->where('l.purchased_vehicle_id!=""');
	$this->db->where('l.form_type!=', ''); 
	//$this->db->where('(l.status="Finished" or l.status="Closed" or l.status="Approved and Archived")');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.form_type'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

/*
public function getNumberOfLeadsPerManualSource($filterData)
{
	$this->db->select("s.title, s.id as source_id, count( s.id ) as num");
	$this->db->from("leads l, sources s");
	$this->db->where("s.id=l.manual_source");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.category_id', '0');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.manual_source'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}*/

public function getNumberOfLeadsPerWebsiteSource($filterData)
{
	$this->db->select("form_type as title, count( l.id ) as num");
	$this->db->from("leads l");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.form_type!=', '');
	$this->db->where('l.category_id', '0');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.form_type'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getNumberOfViewedSurvey($filterData)
{
	$this->db->select("count(*) as num");
	$this->db->from("leads l");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.survey_viewed', '1');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;
}

public function getNumberOfSentSurvey($filterData)
{
	$this->db->select("count(*) as num");
	$this->db->from("leads l");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.survey_sent', '1');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;
}

public function getNumberOfCompletedSurvey($filterData)
{
	$this->db->select("count(*) as num");
	$this->db->from("leads l");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.survey_response', '1');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;
}

public function getNumberOfLeads($filterData)
{
	$this->db->select("count(*) as num");
	$this->db->from("leads l");
	$this->db->where('l.duplicate_of', '0');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;
}

public function getNumberOfTestDriveLeads($filterData)
{
	$this->db->select("count(*) as num");
	$this->db->from("leads l");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('(l.category_id=1 or l.category_id=34)'); //when online forms are received then cat1 is assignend and from manual or iPad the cat34
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;
}

public function getNumberOfCompletedTestDrivesLeads($filterData)
{
	$this->db->select("count(l.id) as num");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where("lm.lead_id=l.id");
	$this->db->where('l.duplicate_of', '0');
	//$this->db->where('(l.category_id=1 or l.category_id=34)'); //when online forms are received then cat1 is assignend and from manual or iPad the cat34
	$this->db->where('(lm.test_drive_completed=1 or lm.test_drive_type="Walk in")');

	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;
}

public function getCompTestDrivesBranches($filterData)  
{
	$this->db->select("sd.title, count(lm.id) as num");
	$this->db->from("leads l,leads_messages lm,struc_dep_users sdu,structure_departments sd");
	$this->db->where("`lm`.`created_by` in(select user_id from struc_dep_users where struc_dep_id in (56, 57, 55, 52, 53, 54, 80)) and l.assign_to=sdu.user_id and sd.id=sdu.struc_dep_id");
	$this->db->where("lm.lead_id=l.id");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('(lm.test_drive_completed=1 or lm.test_drive_type="Walk in")');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	$this->db->group_by('sdu.struc_dep_id'); 
	$this->db->order_by('sd.title'); 
	

	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getPurchTestDrivesBranches($filterData)  
{
	
	/*$this->db->select("sd.title, count(l.id) as num");
	$this->db->from("leads l, struc_dep_users sdu, structure_departments sd");
	$this->db->where("`l`.`assign_to` in(select user_id from struc_dep_users where struc_dep_id in (56, 57, 55, 52, 53, 54, 80)) and l.assign_to=sdu.user_id and sd.id=sdu.struc_dep_id");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.tag_id=2');
	$this->db->where('l.purchased_vehicle_id!=""');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	$this->db->group_by('sdu.struc_dep_id'); 
	$this->db->order_by('sd.title'); */
	
	$this->db->select("sd.title, count(l.id) as num");
	$this->db->from("leads l, leads_messages lm, struc_dep_users sdu, structure_departments sd");
	$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Purchased a car%' and `l`.`assign_to` in(select user_id from struc_dep_users where struc_dep_id in (56, 57, 55, 52, 53, 54, 80)) and l.assign_to=sdu.user_id and sd.id=sdu.struc_dep_id");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.tag_id=2');
	$this->db->where('l.purchased_vehicle_id!=""');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	$this->db->group_by('sdu.struc_dep_id'); 
	$this->db->order_by('sd.title'); 
	

	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getNumberOfPurchasedCarsLeads($filterData)
{
	$this->db->select("count(*) as num");
	$this->db->from("leads l");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.tag_id', '2');
	$this->db->where('l.purchased_vehicle_id!=""');
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;
}

public function getPurchasedSalesCons($filterData)
{
	$this->db->select("count(l.assign_to) as num, u.full_name");
	$this->db->from("leads l, users u, leads_messages lm");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.tag_id', '2');
	$this->db->where('l.purchased_vehicle_id!="" and l.id=lm.lead_id and lm.message_id=4 and lm.comments like "%Purchased a car%"');
	$this->db->where('u.id=l.assign_to');
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$this->db->group_by("l.assign_to");
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getTimeFromLeadCreationToPurchase($filterData)
{
	$this->db->select("avg(TIMESTAMPDIFF(SECOND, l.created_at, lm.created_at)) as avgval");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.tag_id', '2');
	$this->db->where('l.purchased_vehicle_id!=""');
	$this->db->where('l.id=lm.lead_id');
	$this->db->where('lm.message_id=4 and lm.comments like "%Purchased a car%"'); //comment

	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;
}

public function getTimeFromLeadCreationToVisit($filterData)
{
	//$this->db->select("avg(TIMESTAMPDIFF(SECOND, l.created_at, lm.created_at)) as avgval");
	$this->db->select("count(*) as num");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.id=lm.lead_id');
	$this->db->where('lm.message_id=3');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;
}

public function getTimeFromLeadCreationToLost($filterData)
{
	//$this->db->select("avg(TIMESTAMPDIFF(SECOND, l.created_at, lm.created_at)) as avgval");
	$this->db->select("count(*) as num");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.tag_id', '1');
	$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	


	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->row();
	}

	return null;
}

public function getBranchesAvgScore($filterData)
{
	/*$this->db->select("b.title, avg( sr.answer_val ) as avgval");
	$this->db->from("leads l, survey_results sr, branches b");
	$this->db->where("l.survey_response!=0 and sr.lead_id=l.id and l.branch_id=b.id");
	$this->db->where('l.duplicate_of', '0');
	$this->db->group_by("l.branch_id");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;	*/
	
	$this->db->select("sd.id, sd.title, avg( sr.answer_val ) as avgval");
	$this->db->from("struc_dep_users sdu,structure_departments sd, leads l, survey_results sr");
	$this->db->where("sdu.struc_dep_id in (56, 57, 55, 52, 53, 54, 80) and sd.id=sdu.struc_dep_id and l.survey_response=1 and sr.lead_id=l.id and l.assign_to=sdu.user_id");
	$this->db->where('l.duplicate_of', '0');
	$this->db->group_by("sdu.struc_dep_id");
	$this->db->order_by("avgval");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;	
	
}

public function getUserLostTLPM($filterData)
{
	$this->db->select("u.full_name, u.id as userid, count( u.id ) as num");
	$this->db->from("leads l, users u, leads_messages lm");
	$this->db->where("u.id=l.assign_to");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.tag_id', '1'); //1 lost
	$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);

	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("l.assign_to in(".$filterData['depUsers'].")");

	$this->db->group_by('l.assign_to'); 
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getAvgScorePerCategories($filterData)
{
	$this->db->select("c.title, c.id as category_id, avg( sr.answer_val ) as avgval");
	$this->db->from("leads l, categories c, survey_results sr");
	$this->db->where("c.id=l.category_id");
	$this->db->where("l.survey_response!=0 and sr.lead_id=l.id");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.category_id!=', '0');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);

	$this->db->group_by('l.category_id'); 
	$this->db->order_by('avgval','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

//following is for which the category is not assigned
public function getAvgScorePerWebsiteSource($filterData)
{

	$this->db->select("l.form_type as title, avg( sr.answer_val ) as avgval");
	$this->db->from("leads l, survey_results sr");
	$this->db->where("l.survey_response!=0 and sr.lead_id=l.id");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.category_id', '0');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);

	$this->db->group_by('l.form_type'); 
	$this->db->order_by('avgval','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}




public function getCitiesWithBranches()
{

	$this->db->select("c.*");
	$this->db->from("cities c, branches b");
	$this->db->where("c.id=b.city_id and c.id!=0");
	$this->db->group_by('c.id'); 
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getCarTypeEachUser($filterData)
{
	$users = array(); 
	$carsScheduled = array();
	$carsCompleted = array();

	$this->db->select("lm.created_by, lm.vehicle_specific_names_id, lm.test_drive_completed, lm.test_drive_type");
	$this->db->from("leads l,leads_messages lm");
	$this->db->where("l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!=''");
	$this->db->where('l.duplicate_of', '0');
	

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{
			$uId = $row->created_by;
			$car = $row->vehicle_specific_names_id;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			//now convert the specific to general
			for($ctgc=0; $ctgc<count($carsArr); $ctgc++)
			{
				 //$ctgc this is specific id. 
				$carsArr[$ctgc] = $this->getGIdOfSpecific($carsArr[$ctgc]);
			}
			//$car = implode(",",$carsArr); //can be multiple in comma
			$car = $carsArr[0];
			//==			

			if(!in_array($uId, $users))
			{
				$users[] = $uId;
				$carsScheduled[$uId] = $car;
				if($row->test_drive_completed || $row->test_drive_type === "Walk in")
					$carsCompleted[$uId] = $car;
				else
					$carsCompleted[$uId] = ""; //to make the index
			}
			else
			{
				$carsScheduled[$uId] = $carsScheduled[$uId].','.$car;
				if($row->test_drive_completed || $row->test_drive_type === "Walk in")
					$carsCompleted[$uId] = $carsCompleted[$uId].','.$car;
			}
		}
	}
	
	return array($users,$carsScheduled,$carsCompleted);
}

public function getGCarTypeEachUser($filterData)
{
	$users = array();
	$cars = array();	

	$this->db->select("l.assign_to, CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')) as vehicle");
	$this->db->from("leads l");
	$this->db->where("(l.vehicle!='' or l.vehicle_specific_names_id!='') and l.assign_to!='0'");
	//$this->db->where('(l.category_id=1 or l.category_id=34)');	
	$this->db->where('l.duplicate_of', '0');

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);

	//$this->db->where("l.created_at >=",'2016-09-01');
	//$this->db->where("l.created_at <=",'2016-09-30');
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{
			$user_id = $row->assign_to;
			$car = $row->vehicle;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			$car = implode(",",$carsArr); //can be multiple in comma
			//==	

			if(!in_array($user_id, $users))
			{
				$users[] = $user_id;
				$cars[$user_id] = $car;				
			}
			else
			{
				$cars[$user_id] = $cars[$user_id].','.$car;				
			}
		}
	}
	
	return array($users,$cars);
}

public function getGnameOfSpecific($specific_id)
{
	$this->db->select("v.name");
	$this->db->from("vehicle_specific_names vsn, vehicles v");
	$this->db->where("vsn.vehicle_id = v.id and vsn.id='".$specific_id."'");
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$row = $query->row();
		return $row->name;
	}
	else
	{
		return "";
	}

}

private function getGIdOfSpecific($specific_id)
{
	$this->db->select("v.id");
	$this->db->from("vehicle_specific_names vsn, vehicles v");
	$this->db->where("vsn.vehicle_id = v.id and vsn.id='".$specific_id."'");
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$row = $query->row();
		return $row->id;
	}
	else
	{
		return "";
	}

}

public function getCarTypes($filterData)
{
	//$users = array(); 
	$cars = array();
	$branches = array();
	//$carsScheduled = array();
	//$carsCompleted = array();

	$this->db->select("l.branch_id, lm.vehicle_specific_names_id, lm.test_drive_completed, lm.test_drive_type");
	$this->db->from("leads l,leads_messages lm");
	$this->db->where("l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!=''");
	$this->db->where('l.duplicate_of', '0');
	
	if(isset($filterData['user_id']) && $filterData['user_id']) $this->db->where("lm.created_by", $filterData['user_id']);
	else
	{
		$this->db->where("lm.created_by in(select user_id from struc_dep_users where struc_dep_id in (56, 57, 55, 52, 53, 54, 80))");
	}

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{
			$bId = $row->branch_id;
			$car = $row->vehicle_specific_names_id;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			//now convert the specific to general
			for($ctgc=0; $ctgc<count($carsArr); $ctgc++)
			{
				$gId = $this->getGIdOfSpecific($carsArr[$ctgc]);
				if(!in_array($gId,$cars))
				{
					$cars[] = $gId;
					$branches[$gId] = $bId;

				}
				else
				{
					$branches[$gId] = $branches[$gId].",".$bId;
				}
				
				break;
			}
			//$car = implode(",",$carsArr); //can be multiple in comma
			//==						

			/*if(!in_array($uId, $users))
			{
				$users[] = $uId;
				$carsScheduled[$uId] = $car;
				if($row->test_drive_completed || $row->test_drive_type === "Walk in")
					$carsCompleted[$uId] = $car;
				else
					$carsCompleted[$uId] = ""; //to make the index
			}
			else
			{
				$carsScheduled[$uId] = $carsScheduled[$uId].','.$car;
				if($row->test_drive_completed || $row->test_drive_type === "Walk in")
					$carsCompleted[$uId] = $carsCompleted[$uId].','.$car;
			}*/
		}
	}

	

	//print_r($branches);
	//echo "<br><br>";

/*	$gBranches = $gCarType[1];
	//print_r($gBranches); exit();
	foreach($gBranches as $gid=>$branch)
	{
		if(!array_key_exists($gid,$branches))
		{
			$branches[$gid] = $branch;
		}
		else
		{
			$branches[$gid] = $branch;
		}

	}

	//print_r($branches);

	//exit();
	*/
	

	return array($cars,$branches);
}

public function getCarsEachBranch($filterData)
{
	$branchs = array();
	$cars = array();
	$carsCompleted = array();

	$this->db->select("l.branch_id, lm.vehicle_specific_names_id, lm.test_drive_completed, lm.test_drive_type");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where("l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!=''");
	//$this->db->where('(l.category_id=1 or l.category_id=34)');	
	$this->db->where('l.duplicate_of', '0');	
	
	if(isset($filterData['user_id']) && $filterData['user_id']) $this->db->where("lm.created_by", $filterData['user_id']);
	else
	{
		$this->db->where("lm.created_by in(select user_id from struc_dep_users where struc_dep_id in (56, 57, 55, 52, 53, 54, 80))");
	}
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{
			$branch_id = $row->branch_id;
			$car = $row->vehicle_specific_names_id;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			//now convert the specific to general
			for($ctgc=0; $ctgc<count($carsArr); $ctgc++)
			{
				$carsArr[$ctgc] = $this->getGIdOfSpecific($carsArr[$ctgc]);
			}
			//$car = implode(",",$carsArr); //can be multiple in comma
			$car = $carsArr[0];
			//==	

			if(!in_array($branch_id, $branchs))
			{
				$branchs[] = $branch_id;
				$cars[$branch_id] = $car;
				if($row->test_drive_completed || $row->test_drive_type === "Walk in")
					$carsCompleted[$branch_id] = $car;
				else
					$carsCompleted[$branch_id] = ""; //to make the index
			}
			else
			{
				$cars[$branch_id] = $cars[$branch_id].','.$car;
				if($row->test_drive_completed || $row->test_drive_type === "Walk in")
					$carsCompleted[$branch_id] = $carsCompleted[$branch_id].','.$car;
			}
		}
	}
	
	return array($branchs,$cars,$carsCompleted);
}

public function getGCarTypeEachBranch($filterData)
{
	$branches = array();
	$cars = array();	

	//$this->db->select("l.branch_id, l.vehicle");
	$this->db->select("l.branch_id, CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')) as vehicle");
	
	$this->db->from("leads l");
	//$this->db->where("l.vehicle!=''");
	$this->db->where("(l.vehicle!='' or l.vehicle_specific_names_id!='')");	
	
	$this->db->where('l.duplicate_of', '0');

	if(isset($filterData['user_id']) && $filterData['user_id']) $this->db->where("l.assign_to", $filterData['user_id']);
	else
	{
		$this->db->where("l.assign_to in(select user_id from struc_dep_users where struc_dep_id in (56, 57, 55, 52, 53, 54, 80))");
	}
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);

	//$this->db->where("l.created_at >=",'2016-09-01');
	//$this->db->where("l.created_at <=",'2016-09-30');
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{
			$bId = $row->branch_id;
			$car = $row->vehicle;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			$car = implode(",",$carsArr); //can be multiple in comma
			//==	

			if(!in_array($bId, $branches))
			{
				$branches[] = $bId;
				$cars[$bId] = $car;				
			}
			else
			{
				$cars[$bId] = $cars[$bId].','.$car;				
			}
		}
	}
	
	return array($branches,$cars);
}

public function getGCarType($filterData)
{
	$cars = array();	
	$branches = array();
	
	//$this->db->select("l.branch_id, l.vehicle");
	$this->db->select("l.branch_id, CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')) as vehicle");	
	$this->db->from("leads l");
	//$this->db->where("l.vehicle!=''");
	$this->db->where("(l.vehicle!='' or l.vehicle_specific_names_id!='')");	
	$this->db->where('l.duplicate_of', '0');
	
	if(isset($filterData['user_id']) && $filterData['user_id']) $this->db->where("l.assign_to", $filterData['user_id']);
	else
	{
		$this->db->where("l.assign_to in(select user_id from struc_dep_users where struc_dep_id in (56, 57, 55, 52, 53, 54, 80))");
	}
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);

	//$this->db->where("l.created_at >=",'2016-09-01');
	//$this->db->where("l.created_at <=",'2016-09-30');
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{
			$bId = $row->branch_id;
			$car = $row->vehicle;			
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);

			for($ctgc=0; $ctgc<count($carsArr); $ctgc++)
			{
				if(isset($carsArr[$ctgc]))
				{
					$gId = $carsArr[$ctgc];
					if(!in_array($gId,$cars))
					{
						$cars[] = $gId;		
						$branches[$gId] = $bId;
					}
					else
					{
						$branches[$gId] = $branches[$gId].",".$bId;
					}
				}
			}
			
		}
	}
	return array($cars,$branches);
}

public function getUsersFromLeads($filterData)
{
	$this->db->select("u.full_name, u.id as userid");
	$this->db->from("leads l, users u");
	$this->db->where("u.id=l.assign_to");
	$this->db->where('l.duplicate_of', '0');

	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("l.assign_to in(".$filterData['depUsers'].")");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.assign_to');
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getUsersNumOfFinishedButNotClosedLeads($filterData)
{
	$this->db->select("u.full_name, u.id as userid, count( u.id ) as num");
	$this->db->from("leads l, users u");
	$this->db->where("u.id=l.assign_to");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('l.status="Finished"');

	if(isset($filterData['user_id']) && $filterData['user_id']) $this->db->where("l.assign_to", $filterData['user_id']);
	else if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("l.assign_to in(".$filterData['depUsers'].")");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	$this->db->group_by('l.assign_to');
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

public function getUsersNumOfClosedLeads($filterData)
{
	$this->db->select("u.full_name, u.id as userid, count( u.id ) as num");
	$this->db->from("leads l, users u");
	$this->db->where("u.id=l.assign_to");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where('(l.status="Closed" or l.status="Approved and Archived")');

	if(isset($filterData['user_id']) && $filterData['user_id']) $this->db->where("l.assign_to", $filterData['user_id']);
	else if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("l.assign_to in(".$filterData['depUsers'].")");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
	
	
	
	$this->db->where('l.duplicate_of', '0');
	
	$this->db->group_by('l.assign_to');
	$this->db->order_by('num','desc'); 	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}


public function getDepUsersSurveyScores($filterData)
{

	$depIds = array(52,53,55,56,57,54,80);//from org struc
	$depUsers = array();
	$depUsersScore = array();
	foreach($depIds as $depId)
	{
		$depUsers[$depId] = array();

		$this->db->select("sdu.user_id");
		$this->db->from("struc_dep_users sdu");
		$this->db->where("sdu.struc_dep_id='".$depId."'");				
		$query=$this->db->get();
		if($query->num_rows() > 0){	$depUsers[$depId] = $query->result_array(); }
	}

	foreach($depUsers as $depId=>$depUsersResArr)
	{
		foreach($depUsersResArr as $depUser)
		{
			//echo $depUser['user_id'];
			$userAvgScore = $this->getUserAvgScoreById($filterData,$depUser['user_id']);
			if($userAvgScore)
			{
				if(isset($depUsersScore[$depId]['totalDepAvg'])) 
					$depUsersScore[$depId]['totalDepAvg'] = $depUsersScore[$depId]['totalDepAvg']+round($userAvgScore[0]['avgval'],2);
				else
				{
					$depUsersScore[$depId]['totalDepAvg'] = round($userAvgScore[0]['avgval'],2);
					$depUsersScore[$depId]['depName'] = $this->getDepNameOrgStruc($depId);				
				}

				$depUsersScore[$depId][$depUser['user_id']] = $userAvgScore[0];
				
			}
		}
		//echo "<br>";
	}
	//echo "<pre>";
	//print_r($depUsersScore);
	//exit();
	
	return $depUsersScore;
	
}

public function getUserAvgScoreById($filterData,$user_id)
{
	$this->db->select("u.id as user_id, u.full_name, avg( sr.answer_val ) as `avgval`");
	$this->db->from("leads l, users u, survey_results sr, branches b");
	$this->db->where("u.id=l.assign_to and l.survey_response!=0 and sr.lead_id=l.id");
	$this->db->where('l.duplicate_of', '0');
	$this->db->where("l.assign_to=".$user_id."");
	$this->db->having("avgval != ''");
	
	if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("u.id in(".$filterData['depUsers'].")");

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		return $result = $query->result_array();
	}

	return null;
}

public function getDepUsersByDepId($depId)
{
	$this->db->select("u.id, u.full_name");
	$this->db->from("struc_dep_users sdu, users u");
	$this->db->where("sdu.struc_dep_id='".$depId."' and u.id=sdu.user_id");	
	$this->db->order_by('u.full_name','ASC');
		
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		return $result = $query->result();
	}

	return null;
}

/* moved to common model
public function getUserDep($uId)
{
	$this->db->select("sdu.struc_dep_id");
	$this->db->from("struc_dep_users sdu");
	$this->db->where("sdu.user_id='".$uId."' and sdu.struc_dep_id in (52,53,55,56,57)");	
		
	$query=$this->db->get();

	if($query->num_rows() > 0)
	{
		$row = $query->row();
		return $row->struc_dep_id;
	}

	return 0;
}
*/
public function getDepNameOrgStruc($dId)
{
	$this->db->select("title");
	$this->db->from("structure_departments");	
	$this->db->where("id=".$dId."");
	
	$query=$this->db->get();

	$res = $query->row();

	return $res->title;
	
}

public function getDepUsers()
{

	$depIds = array(52,53,55,56,57,54,80);//from org struc
	$depNames = array("Madina Road Showroom","Automall Showroom","Khurais Showroom","Uroubah Showroom","Dammam Showroom","Abha Showroom","Madina Showroom");
	$depUsers = array();
	foreach($depIds as $depId)
	{
		$depUsers[$depId] = array();

		$this->db->select("sdu.user_id, u.full_name");
		$this->db->from("struc_dep_users sdu, users u");
		$this->db->where("sdu.struc_dep_id='".$depId."' and u.id=sdu.user_id");	
		$this->db->order_by('u.full_name','ASC');
		$query=$this->db->get();
		if($query->num_rows() > 0){	$depUsers[$depId] = $query->result_array(); }
	}

	/*foreach($depUsers as $depId=>$depUsersResArr)
	{
		foreach($depUsersResArr as $depUser)
		{
			$depUser['user_id'];
			$depUser['full_name'];
			
		}
		
	}*/

	return array($depIds, $depNames, $depUsers);
	
}

public function getUserRedar($filterData)
{
	$firstCall = 0;
	$finishLeads= 0;
	$disapproved = 0;
	$score = 0;
	$testDrive = 0;
	$sales = 0;

	$userId = $filterData['user_id'];

	unset($filterData['user_id']);
	unset($filterData['depUsers']);

	if($userId)
	{
		$res = $this->getTimeToMakeCallActionEachUserAvg($filterData,$userId);
		if($res) $firstCall = secondsToTextDurInHours($res[0]->avgval); //hours (min at the end are ignored?)

		$res = $this->getTimeToFinishActionEachUserAvg($filterData,$userId);
		if($res) $finishLeads = secondsToTextDurInHours($res[0]->avgval); //hours (min at the end are ignored?)

		$res = $this->getNumOfDisapprovedActionEachUser($filterData,$userId);
		if($res) $disapproved = $res[0]->num;

		$res = $this->getUserAvgScore($filterData,$userId);
		if($res) $score = round($res[0]->avgval,2); //between 0-5

		
		$testDrive = $this->getCountOfCompletedTestByUser($userId,$filterData);
		$sales = $this->getNumberOfPurchPerUser($userId,$filterData);
	}

	return array('firstCall'=>$firstCall,'finishLeads'=>$finishLeads,'disapproved'=>$disapproved,'score'=>$score,'testDrive'=>$testDrive,'sales'=>$sales);

}

public function getDepRedar($filterData)
{
	$firstCall = 0;
	$finishLeads= 0;
	$disapproved = 0;
	$score = 0;
	$testDrive = 0;
	$sales = 0;

	$userIdsCommaSep = $filterData['depUsers'];

	unset($filterData['depUsers']);
	unset($filterData['user_id']);

	if($userIdsCommaSep)
	{

		$res = $this->getTimeToMakeCallActionEachUserAvg($filterData,$userIdsCommaSep);
		if($res) $firstCall = secondsToTextDurInHours($res[0]->avgval); //hours (min at the end are ignored?)		
			
		$res = $this->getTimeToFinishActionEachUserAvg($filterData,$userIdsCommaSep);
		if($res) $finishLeads = secondsToTextDurInHours($res[0]->avgval); //hours (min at the end are ignored?)

		$res = $this->getNumOfDisapprovedActionEachUser($filterData,$userIdsCommaSep);
		if($res) $disapproved = $res[0]->num;

		$res = $this->getUserAvgScore($filterData,$userIdsCommaSep);
		if($res) $score = round($res[0]->avgval,2); //between 0-5

		
		$testDrive = $this->getCountOfCompletedTestByUser($userIdsCommaSep,$filterData);
		$sales = $this->getNumberOfPurchPerUser($userIdsCommaSep,$filterData);
	}
	
	return array('firstCall'=>$firstCall,'finishLeads'=>$finishLeads,'disapproved'=>$disapproved,'score'=>$score,'testDrive'=>$testDrive,'sales'=>$sales);
	

}

public function getCountOfCompletedTestByUser($userId,$filterData)
{
	$count = 0;	

	$this->db->select("lm.created_by, lm.vehicle_specific_names_id, lm.test_drive_completed, lm.test_drive_type");
	$this->db->from("leads l,leads_messages lm");
	$this->db->where("l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!=''");
	$this->db->where('l.duplicate_of', '0');
	//$this->db->where('(lm.test_drive_completed=1 or lm.test_drive_type="Walk in")');
	//$this->db->where('(l.category_id=1 or l.category_id=34)');	
	
	if($userId) $this->db->where("lm.created_by in(".$userId.")"); //for redar graphs

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->result();
		foreach($result as $row)
		{
			$uId = $row->created_by;
			$car = $row->vehicle_specific_names_id;
			
			//check if same lead has same cars twice, then its a mistake.
			$carsArr = explode(",",$car);
			$carsArr = array_unique($carsArr);
			//$car = implode(",",$carsArr); //can be multiple in comma
			$car = $carsArr[0];
			//==			

			if($row->test_drive_completed || $row->test_drive_type === "Walk in")
				$count++;
			
			
		}
	}
	
	return $count;
}

public function getNumberOfPurchPerUser($userId,$filterData)
{
	$this->db->select("count( l.id ) as num");
	$this->db->from("leads l, leads_messages lm");
	$this->db->where('l.duplicate_of', '0');
	//$this->db->where('l.assign_to', $userId);
	$this->db->where('l.tag_id', '2'); //2=purchased
	$this->db->where('l.purchased_vehicle_id!=""');
	//$this->db->where('l.purchased_vehicle_id=vs.id');
	//if($userId) $this->db->where("l.assign_to in(".$userId.")"); //for redar graphs
	if($userId) $this->db->where("lm.created_by in(".$userId.")"); //for redar graphs	
	$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Purchased a car%'");	

	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
	if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);	
	
	$query=$this->db->get();

	//echo $this->db->last_query();exit();

	if($query->num_rows() > 0)
	{
		$result = $query->row();
		return $result->num;
	}

	return 0;
}
 
public function mktSheet()
{
    $manual_query = "SELECT DATE_FORMAT(l.created_at,'%b %Y') as Month, b.title as Branch, 
	sum(case when l.form_type=\"Test Drive\" then 1 else 0 end) as `Test Drive`,
	sum(case when l.form_type=\"Call Back\" then 1 else 0 end) as `Call Back`,
	sum(case when l.form_type=\"Contact us\" then 1 else 0 end) as `Contact us`,
	sum(case when l.manual_source=\"6\" then 1 else 0 end) as `ROI Call Center`
	FROM `leads` `l`,users u, branches b
	WHERE  l.assign_to=u.id and b.id=u.branch_id AND `l`.`duplicate_of` = '0' GROUP BY YEAR(l.created_at), MONTH(l.created_at), b.title ORDER BY YEAR(l.created_at), MONTH(l.created_at),b.title";

    return $this->db->query($manual_query);
}

public function leadsCountSheet()
{

	return $query=$this->db->query("select DATE_FORMAT(l.created_at,'%b %Y') as Month, b.title, count(*) from branches b left join leads l on b.id = l.branch_id group by DATE_FORMAT(l.created_at,'%b %Y'), b.title order by DATE_FORMAT(l.created_at,'%b %Y'), b.title
");
	
}

public function eventsCountSheet()
{
	/*$this->db->select("e.title as `Event MKT`, count(*) as Count,
	' ' as `Sources`,
	sum(case when l.http_referer like '%google%' then 1 else 0 end) as Google, 
	sum(case when (l.http_referer like '%t.co%' or l.http_referer like '%twitter%') then 1 else 0 end) as Twitter, 
	sum(case when l.http_referer like '%outlook.live%' then 1 else 0 end) as `Outlook.live` , 
	sum(case when l.http_referer like '%facebook%' then 1 else 0 end) as `Facebook` , 
	sum(case when l.http_referer like '%instagram%' then 1 else 0 end) as `Instagram` , 
	");
	$this->db->from("leads l, event e");
	$this->db->where("l.event_id=e.id");
	$this->db->where("e.id=15");
	$this->db->where('l.duplicate_of', '0');
	
	$this->db->group_by('l.event_id'); 	
	
	return $query=$this->db->get();*/
	
	
	/*$this->db->select("e.title as `Event MKT`, count(*) as Count");
	$this->db->from("leads l, event e");
	$this->db->where("l.event_id=e.id");
	$this->db->where('l.duplicate_of', '0');
	
	$this->db->group_by('l.event_id'); 	
	
	return $query=$this->db->get();*/
	
	
	$manual_query = 'select title as `MKT Activity`, count(id) as `Total Count`, count(td_id) as `Completed TD`, count(pur_id) 
					as `Purchased` from 
					(SELECT l.id, l.event_id, e.title from leads l, event e where l.event_id=e.id and l.duplicate_of=0) t1

					left join 
					(SELECT lm.lead_id as td_id from leads l, leads_messages lm, event e where l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!="" and (lm.test_drive_completed=1 or lm.test_drive_type="Walk in") and l.event_id=e.id and l.duplicate_of=0) t2
					on t1.id=t2.td_id

					left join
					(SELECT l.id as pur_id from leads l, event e where l.event_id=e.id and l.duplicate_of=0 and l.tag_id=2 and l.purchased_vehicle_id>0) t3
					on t1.id=t3.pur_id

					group by event_id';
	
	return $query=$this->db->query($manual_query);
	
	//echo $this->db->last_query();exit();
	
	
	
}


//Note: this report is based upon lead assigned date range. But if you will compare it with completed TD with other graphs that uses the lm that is actual test drive done date range. The count can be different.
//also this is based upon the sales consultants that are in organization structure for 
//Madina Road (Jeddah 1) Branch
//Automall Branch
//Khurais Branch
//Urouba Branch
//Dammam Branch
public function scTestDriveSheet()
{

	/*$manual_query = "SELECT sd.title as Branch, u.full_name as `SC Name`, group_concat(concat(' ',vsn.name,'|',CONCAT(form_type,'',IF(manual_source = '0', '', (select s.title from sources s where s.id=l.manual_source))))) as Vehicle_and_Source ,count(lm.id) as `Completed TD` FROM leads l, leads_messages lm, struc_dep_users sdu, structure_departments sd, users u, vehicle_specific_names vsn WHERE lm.created_by in(select user_id from struc_dep_users where struc_dep_id in (56, 57, 55, 52, 53)) and l.assign_to = sdu.user_id and sd.id = sdu.struc_dep_id AND lm.lead_id = l.id AND l.duplicate_of = '0' AND (lm.test_drive_completed = 1 or lm.test_drive_type = 'Walk in') and u.id=lm.created_by and lm.vehicle_specific_names_id=vsn.id ";
	
	if(isset($_GET['date_from']) && $_GET['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$_GET['date_from']."'";
	if(isset($_GET['date_to']) && $_GET['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";
	
	$manual_query .= " GROUP BY sdu.struc_dep_id,u.id ORDER BY sd.title,u.full_name";
	
	return $this->db->query($manual_query);*/
	
	//$first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
	//$last_day_this_month  = date('Y-m-t');
		
	//$_GET['date_from'] = $first_day_this_month;
	//$_GET['date_to'] = $last_day_this_month;
	
	//$_GET['date_from'] = "2017-03-01";
	//$_GET['date_to'] = "2017-03-31";
	
	
	$manual_query = "select title as Branch, full_name as `SC Name`,trackid as `Trackid`, Source , if(Requested!='', Requested, 'No car have been selected') as `Requested` , created_at as `Created at`, assigned_at as `Assigned at`, if(vsn is not null, vsn, '') as `TD Completed`, (select name from vehicle_specific_names vsn_2 where vsn_2.id=purchased_vehicle_id) as `Purchased Vehicle` from (
	(
	SELECT l.id, l.trackid, CONCAT(form_type,'',IF(manual_source = '0' || form_type!='', '', (select s.title from sources s where s.id=l.manual_source))) as Source ,b.title, u.full_name, concat(if(l.vehicle!='', v.name, ''),if(l.vehicle_specific_names_id!='', vsn.name, '')) as Requested, l.created_at, l.assigned_at, l.purchased_vehicle_id FROM leads l left join vehicles v on v.id=l.vehicle left join vehicle_specific_names vsn on vsn.id=l.vehicle_specific_names_id, branches b, users u WHERE l.assign_to=u.id and b.id = u.branch_id AND l.duplicate_of = '0' AND u.id=l.assign_to";
	
	if(isset($_POST['date_from']) && $_POST['date_from']!="")	$manual_query .= " and date(l.created_at) >= '".$_POST['date_from']."'";
	if(isset($_POST['date_to']) && $_POST['date_to']!="")	$manual_query .= " and date(l.created_at) <= '".$_POST['date_to']."'";

	$manual_query .= " ORDER BY b.title,u.full_name,l.created_at
	) 
		requests left join 
	(
	SELECT lm.lead_id, vsn.name as vsn FROM leads_messages lm, vehicle_specific_names vsn WHERE (lm.test_drive_completed = 1 or lm.test_drive_type = 'Walk in') and lm.vehicle_specific_names_id=vsn.id
	) completed

		on requests.id=completed.lead_id
		
	) ";
	
	
	
	return $this->db->query($manual_query);
	
			
}

//1st Dec 2016 onwards
public function walkinSheet1()
{
	//$manual_query = "SELECT b.title as Branch, sum(case when l.manual_source = 13 then 1 else 0 end) as `Walk-in`, sum(case when l.manual_source = 8 then 1 else 0 end) as `Inbound Call` FROM leads l, branches b, sources s WHERE (l.manual_source = 13 or l.manual_source = 8) and l.orignal_created_by > 0 and date(l.created_at) >= '2016-12-01' and l.duplicate_of=0 and l.branch_id=b.id and s.id=l.manual_source group by branch_id order by `Walk-in` desc";
    $manual_query = "SELECT b.title as Branch, sum(case when l.manual_source = 13 then 1 else 0 end) as `Walk-in`, sum(case when l.manual_source = 8 then 1 else 0 end) as `Inbound Call` FROM branches b left join leads l on l.branch_id=b.id left join sources s on s.id=l.manual_source group by b.title order by `Walk-in` desc, b.title";

	return $this->db->query($manual_query);
			
}

//1st Dec 2016 onwards
public function walkinSheet2($branchesResult)
{
	$bCountQ = "";
	foreach($branchesResult as $branch)
	{
		if($bCountQ!="") $bCountQ .= ",";
		$bCountQ .= "sum(case when l.branch_id=".$branch->id." then 1 else 0 end) as `".$branch->title." Walk-in` ";
	}	
	
	$manual_query = "SELECT vs.name as Vehicle, ".$bCountQ.", count(l.id) as Total FROM leads l, vehicle_specific_names vs, sources s WHERE l.manual_source = 13 and l.orignal_created_by > 0 and date(l.created_at) >= '2016-12-01' and l.duplicate_of=0 and l.vehicle_specific_names_id=vs.id and s.id=l.manual_source group by vs.name order by Total desc";
	
	//echo $manual_query; exit;
	
	return $this->db->query($manual_query);
			
}

//1st Dec 2016 onwards
public function walkinSheet3($branchesResult)
{
	$bCountQ = "";
	foreach($branchesResult as $branch)
	{
		if($bCountQ!="") $bCountQ .= ",";
		$bCountQ .= "sum(case when l.branch_id=".$branch->id." then 1 else 0 end) as `".$branch->title." Inbound` ";
	}	
	
	$manual_query = "SELECT vs.name as Vehicle, ".$bCountQ.", count(l.id) as Total FROM leads l, vehicle_specific_names vs, sources s WHERE l.manual_source = 8 and l.orignal_created_by > 0 and date(l.created_at) >= '2016-12-01' and l.duplicate_of=0 and l.vehicle_specific_names_id=vs.id and s.id=l.manual_source group by vs.name order by Total desc";
	
	return $this->db->query($manual_query);
			
}

//leads created by each sales consultant 1st Dec 2016 onwards
public function walkinSheet4()
{
	$manual_query = "SELECT b.title as `Branch`, u.full_name as `Sales Consultants`, sum(case when l.manual_source = 13 then 1 else 0 end) as `Walk-in`, sum(case when l.manual_source = 8 then 1 else 0 end) as `Inbound Call`, count(l.id) as Total FROM leads l, users u, branches b, sources s WHERE (l.manual_source = 13 or l.manual_source = 8) and l.orignal_created_by > 0 and date(l.created_at) >= '2016-12-01' and l.duplicate_of=0 and u.id=l.orignal_created_by and l.branch_id=b.id  and s.id=l.manual_source group by l.branch_id, l.orignal_created_by order by Branch, l.orignal_created_by";
	
	return $this->db->query($manual_query);
			
}

// 1st Dec 2016 onwards
public function walkinSheet5()
{
	$manual_query = "SELECT b.title as `Branch`, vs.name as `Vehicle`, sum(case when l.manual_source = 13 then 1 else 0 end) as `Walk-in`, sum(case when l.manual_source = 8 then 1 else 0 end) as `Inbound Call`, count(l.id) as Total FROM leads l, vehicle_specific_names vs, branches b, sources s WHERE (l.manual_source = 13 or l.manual_source = 8) and l.orignal_created_by > 0 and date(l.created_at) >= '2016-12-01' and l.duplicate_of=0 and l.vehicle_specific_names_id=vs.id and l.branch_id=b.id  and s.id=l.manual_source group by l.branch_id, vs.name order by Branch, vs.name";
	
	return $this->db->query($manual_query);
			
}

//============================================================================================
//==========following are old========== 

	public function getAllStatus($user_role, $user_id,$check_for_fetching_leads='')
	{
		$this->db->select('count(*) as lead_count,status');
        $this->db->from('leads');
		switch($check_for_fetching_leads)
		{
			case 'assign':
			$this->db->where('leads.assign_to',$user_id);
			break;
			case 'creator':
			$this->db->where('leads.created_by',$user_id);
			break;
			case 'manager':
			$this->db->where('`leads.assign_to` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' ) OR `leads.created_by` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' )');
			break;
			
		}
		$this->db->group_by('status'); 
		$query=$this->db->get();
		// echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	public function getCountByStatus($status)
	{
		$this->db->select('*');
        $this->db->from('leads');
		if($status != '')
		{
			$this->db->where('status', $status);
		}
        $query=$this->db->get();
		
		if($query->num_rows() > 0)
		{
        	return $query->num_rows();
		}
		else
		{
			return '0';
		}
	}
	public function getAllLeads($arr_date= array(),$user_role,$user_id,$branch_id= '',$category_id = '')
	{
		//echo $category_id;
		//exit();
		$this->db->select('DATE( created_at ) as created_at , COUNT( * ) as count_leads ');
        $this->db->from('leads');
        if(!empty($arr_date))
        {
          $this->db->where('DATE(created_at) >= "'.date('Y-m-d',strtotime($arr_date['date_to'])).'" AND DATE(created_at) <= "'.date('Y-m-d',strtotime($arr_date['date_from'])).'" ');  
        }else
        {
          $this->db->where('created_at >= DATE_SUB( NOW( ) , INTERVAL 14 DAY)');  
        }
		if($branch_id != '')
		{
			$this->db->where_in('branch_id',$branch_id);
		}
		if($category_id != '')
		{
			$this->db->where_in('category_id',$category_id);
		}	
		
		$this->db->group_by('DATE( created_at )');
		
        if($user_role != 1)
        {
            $this->db->where('assign_to',$user_id);
        }  
        $query=$this->db->get();
		//echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
    
   public function getAllLeadsOfYear($arr_date= array(),$user_role,$user_id,$branch_id= '')
	{
		//echo $branch_id;
		//exit();
		$this->db->select('DATE( created_at ) as created_at, COUNT( * ) as count_leads ');
        $this->db->from('leads');
        $this->db->where('YEAR(created_at) = "'.date('Y').'" AND MONTH(created_at) BETWEEN 1 AND 12');  
        
		if($branch_id != '')
		{
			$this->db->where_in('branch_id',$branch_id);
		}
		
		$this->db->group_by('YEAR(created_at), MONTH(created_at)');
		
        if($user_role != 1)
        {
            $this->db->where('assign_to',$user_id);
        }  
        $query=$this->db->get();
		//echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
    
	
	public function getLeadsByComplaint($user_role, $user_id,$branch_id = '',$category_id= '')
	{
		$this->db->select('LEFT(`trackid` , 3) as comp_type, COUNT(*) as comp_count');
        $this->db->from('leads,categories');
		//$this->db->where("trackid LIKE '%NCT%' OR trackid LIKE '%UCT%' OR trackid LIKE '%SCT%' OR trackid LIKE '%PCT%' OR trackid LIKE '%CCT%'");
		$this->db->where('leads.category_id = categories.id');
		$this->db->where('categories.lead_type_tag','CT');
		if($user_role != 1)
        {
            $this->db->where('leads.assign_to',$user_id);
        }
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		}
		if($category_id != '')
		{
			$this->db->where_in('leads.category_id',$category_id);
			
		}
		$this->db->where('leads.status !=','Closed');
		$this->db->where('leads.status !=', 'Approved and Archived');
        $this->db->group_by('comp_type');
		
        $query=$this->db->get();
	//	echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
	public function getSatisfaction($user_role, $user_id,$branch_id = '',$category_id = '',$check_for_fetching_leads = '')
	{
		
     
		$this->db->select('DATE(survey_results.created_at) AS date_lead, AVG( survey_results.answer ) as answers_count');
        $this->db->from('survey_results');
		$this->db->from('leads');
        $this->db->from('surveys');
        $this->db->join('questions', 'survey_results.survey_id = questions.survey_id AND survey_results.question_id = questions.id AND surveys.category_id = leads.category_id ');
		$this->db->where("questions.answer =  'Rating'");
		
   
		if($user_id!==-1) //-1 is for overall
		{
			switch($check_for_fetching_leads)
			{
				case 'assign':
				$this->db->where('leads.assign_to',$user_id);
				break;
				case 'creator':
				$this->db->where('leads.created_by',$user_id);
				break;
				case 'manager':
				$this->db->where('`leads.assign_to` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' ) OR `leads.created_by` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' )');
				break;
				
			}
		}
		
		
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		} 
		if($category_id != '')
		{
			$this->db->where("surveys.category_id",$category_id);
		}
		
		$this->db->group_by('DATE(survey_results.created_at)');
		
        $query=$this->db->get();
        
		//echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}


	public function getSatisfaction1($user_id,$assignedOrCreator="Assigned")
	{
		$this->db->select("date(sr.created_at) as perday, avg(sr.answer) as average");
        $this->db->from("leads l, survey_results sr");
		$this->db->where("
		l.id=sr.lead_id 
		and l.survey_response=1 		 
		and sr.answer REGEXP '^[0-9]+$' 
		");

		if($user_id)
		{
			if($assignedOrCreator==="Assigned")
				$this->db->where("l.assign_to='".$user_id."'");
			elseif($assignedOrCreator==="Creator")
				$this->db->where("l.created_by='".$user_id."'");
		}

		$this->db->group_by('DATE(sr.created_at)');
		$this->db->order_by('sr.created_at','ASC');
		

		$query=$this->db->get();

		//echo $this->db->last_query(); exit();

		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return null;
		}
		
	}
	
	public function getCategoryOfSurvey()
	{
		$this->db->select('category_id');
		$this->db->from('surveys');
		$this->db->limit(1);
	    $query=$this->db->get();
		if($query->num_rows() > 0)
		{
        	 $result = $query->result();
		     return $result[0]->category_id;
		}
		else
		{
			return false;
		}
	}
	
	public function getLeadsByRateCategories($user_role, $user_id,$branch_id = '',$category_id = '')
	{
		//$this->db->select('categories.title as rate_title, AVG(TIME_TO_SEC(TIMEDIFF(leads.close_time, leads.created_at))/60) as rate_avg');
		$this->db->select('users.full_name as rate_title, AVG(TIME_TO_SEC(TIMEDIFF(leads.close_time, leads.created_at))/60) as rate_avg');
        $this->db->from('leads');
		//$this->db->join('categories', 'leads.category_id = categories.id');
		$this->db->join('users', 'leads.assign_to = users.id');
		$this->db->where("leads.status =  'Closed'");
		$this->db->where('leads.status !=', 'Approved and Archived');
	    if($user_role != 1)
        {
            $this->db->where('leads.assign_to',$user_id);
            
        }
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		}
	    if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		
    	$this->db->group_by('leads.assign_to');
		
        $query=$this->db->get();
		//echo $this->db->last_query();exit();
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
	public function getLeadsByCategory($user_role, $user_id, $branch_id = '',$category_id='',$check_for_fetching_leads='')
	{
		$this->db->select('LEFT(`trackid` , 3) as cat_type, COUNT(*) as cat_count');
        $this->db->from('leads,categories');
		//$this->db->where("trackid LIKE '%NTD%' OR trackid LIKE '%NSE%' OR trackid LIKE '%NGE%' OR trackid LIKE '%USE%' OR trackid LIKE '%UGE%' OR trackid LIKE '%MGE%' OR trackid LIKE '%CGE%' OR trackid LIKE '%SSE%'");
		$this->db->where('leads.category_id = categories.id');
        switch($check_for_fetching_leads)
		{
			case 'assign':
			$this->db->where('leads.assign_to',$user_id);
			break;
			case 'creator':
			$this->db->where('leads.created_by',$user_id);
			break;
			case 'manager':
			$this->db->where('`leads.assign_to` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' ) OR `leads.created_by` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' )');
			break;
			
		}
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		}
		if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		$this->db->where('leads.status !=','Closed');
		$this->db->where('leads.status !=', 'Approved and Archived');
		
        $this->db->group_by('cat_type');
		
        $query=$this->db->get();
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
	public function getVehicleType($user_role, $user_id,$branch_id = '', $category_id='')
	{
		$this->db->select('vehicles.name as vehicle_name, COUNT( * ) as vehicle_count');
        $this->db->from('leads');
		$this->db->join('vehicles', 'FIND_IN_SET( vehicles.id, leads.vehicle ) ');
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		} 
		
		if($user_role != 1)
        {
            $this->db->where('assign_to',$user_id);
        }
		if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		
		$this->db->group_by('vehicles.name');
        $query=$this->db->get();
		//echo $this->db->last_query();
		//exit();
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
	public function getLeadsByCity($user_role, $user_id,$branch_id = '', $category_id='')
	{ 
		$this->db->select('cities.`title` as `city_name`, COUNT( * ) as city_count');
        $this->db->from('leads');
		$this->db->join('cities', 'cities.id= leads.city_id');
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		} 
		
		if($user_role != 1)
        {
            $this->db->where('assign_to',$user_id);
        }
		if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		$this->db->where('leads.status !=','Closed');
		$this->db->where('leads.status !=', 'Approved and Archived');
		$this->db->group_by('cities.title');
        $query=$this->db->get();
		
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
	public function getLeadsByUser($user_role, $user_id,$branch_id = '', $category_id = '')
	{ 
		$this->db->select('users.full_name as `user_name`, COUNT( * ) as user_count');
        $this->db->from('leads');
		$this->db->join('users', 'users.id= leads.assign_to');
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		} 
		
		if($user_role != 1)
        {
            $this->db->where('assign_to',$user_id);
        }
		
		if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		$this->db->where('leads.status !=','Closed');
		$this->db->where('leads.status !=', 'Approved and Archived');
		$this->db->group_by('users.full_name');
		
        $query=$this->db->get();
		//echo $this->db->last_query();
		//exit();
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
	public function totalCountLeads($user_role, $user_id,$branch_id = '',$category_id = '',$not_count=true)
	{ 
		$this->db->select('COUNT( * ) as lead_count');
        $this->db->from('leads');
		//$this->db->join('users', 'users.id= leads.assign_to');
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		} 
		
		if($user_role != 1)
        {
            $this->db->where('assign_to',$user_id);
        }
		
		if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		if($not_count)
		{
			$this->db->where('leads.status !=', 'Closed');
			$this->db->where('leads.status !=', 'Approved and Archived');
		}
		
		//$this->db->group_by('leads.id');
        $query=$this->db->get();
		//echo $this->db->last_query();
		//exit();
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
	
	
	public function getLeadsByRegion($user_role, $user_id,$branch_id = '',$category_id = '',$check_for_fetching_leads='')
	{
		$this->db->select('cities.title as region_title, COUNT( * ) as region_count');
        $this->db->from('leads');
		$this->db->join('cities', 'leads.city_id = cities.id');
		$this->db->where("STATUS !=  'Approved and Archived' AND leads.city_id = cities.id");
		switch($check_for_fetching_leads)
		{
			case 'assign':
			$this->db->where('leads.assign_to',$user_id);
			break;
			case 'creator':
			$this->db->where('leads.created_by',$user_id);
			break;
			case 'manager':
			$this->db->where('`leads.assign_to` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' ) OR `leads.created_by` IN (SELECT `id` FROM `users` where  parent_id = '.$user_id.' )');
			break;
			
		}
		if($branch_id != '')
		{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		}
		if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		$this->db->where('leads.status !=','Closed');
		$this->db->where('leads.status !=', 'Approved and Archived');
        $this->db->order_by('cities.id','ASC');
		$this->db->group_by('cities.title');
		
		
        $query=$this->db->get();
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}
	
	/*public function getLeadsByBranch($user_role, $branch_id,$branch_id = '',$category_id= '')
	{
		$this->db->select('branches.title as branch_title, COUNT( * ) as branch_count');
        $this->db->from('leads');
		$this->db->join('branches', 'leads.branch_id = branches.id');
		$this->db->where("STATUS !=  'Approved and Archived' AND leads.branch_id = branches.id");
		if($branch_id == ''){
		if($user_role != 1)
        {
            $this->db->where('leads.branch_id',$branch_id);
        }
	    }else{
			$this->db->where_in('leads.branch_id',$branch_id);
			
		} 
		if($category_id != '')
		{
			$this->db->where("leads.category_id",$category_id);
		}
		$this->db->where('leads.status !=','Closed');
		$this->db->where('leads.status !=', 'Approved and Archived');
        $this->db->group_by('branches.title');
		
        $query=$this->db->get();
		
		if($query->num_rows() > 0)
		{
        	return $query->result();
		}
		else
		{
			return '0';
		}
	}*/
	
}