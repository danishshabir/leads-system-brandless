<?php
Class Model_section extends Base_Model
{
	public function __construct()
	{
		parent::__construct("sections");
		
	}
	
	public function getAllSections()
	{

		$this->db->order_by('order','asc');

		$result = $this->db->get($this->table);
		
		return $result->result();
	}
	
	
}