<?php
Class Model_appointment extends Base_Model
{
    public function __construct()
    {
        parent::__construct("appointment");
    }
    public function get_appointments($array)
    {
        $this->db->select('*');
        $this->db->from('appointment');
        $this->db->where('Date(appointment_start_time) >=', $array['weekStart']);
        $this->db->where('Date(appointment_start_time) <=', $array['weekEnd']);
		$this->db->where('branch_id', $array['branchId']);
		$this->db->where('is_deleted', 0);
		if($array['page'] == 1) $this->db->where('adviser_id', $array['adviser_id']);
        $query = $this->db->get();
        if (!empty($query->result())) {
            return $query->result();
        } else {
            return false;
        }
    }
    public function get_appointment($array)
    {
        $this->db->select('*');
        $this->db->from('appointment');
        $this->db->where('Date(appointment_start_time)', $array['weekStart']);
		$this->db->where('branch_id', $array['branchId']);
		$this->db->where('is_deleted', 0);
		if($array['page'] == 2) $this->db->where('adviser_id', $array['adviser_id']);
        $query = $this->db->get();
        if (!empty($query->result())) {
            return $query->result();
        } else {
            return false;
        }
    }
	public function monthly_appointments($array)
    {
        $this->db->select('Date(appointment_start_time) as appointment_time, count(*) as total_count');
        $this->db->from('appointment');
        $this->db->where('Date(appointment_start_time) >=', $array['start_date']);
        $this->db->where('Date(appointment_start_time) <=', $array['end_date']);
		$this->db->where('branch_id', $array['branchId']);
		$this->db->where('is_deleted', 0);
		if(isset($array['type'])) $this->db->where('type', $array['type']);
		if($array['page'] == 1 || $array['page'] == 2) $this->db->where('adviser_id', $array['adviser_id']);
		$this->db->group_by('Date(appointment_start_time)');
        $query = $this->db->get();
        if (!empty($query->result())) {
            return $query->result();
        } else {
            return false;
        }
    }
    public function getAdviserUsersByBidFromOrgStruc($bid)
    {		
        if (!$bid) {
			return false; 
		}
		
        //$querystr = "select u.* from users u where id in (select user_id from struc_dep_users where struc_dep_id = (select id from structure_departments where bid='" . $bid . "')) and u.adviser_type in (0,1)";		
        
        $querystr = "select u.* from users u where u.branch_id = '" . $bid . "' and u.adviser_type in (0,1)";
        $query    = $this->db->query($querystr);
        $result   = $query->result();		
		
		//echo "<pre>"; print_r($result); die(); 
		
        if (!empty($result)) {
            return $result;
        } else {
            return false;
        }
    }
    public function getUserBIdFromOrgStruc($user_id)
    {
        $query = $this->db->query("SELECT bid FROM `structure_departments` WHERE id = (select sdu.struc_dep_id from struc_dep_users sdu where sdu.user_id='" . $user_id . "')");
        $row   = $query->row();
        if (!empty($row)) {
            return $row->bid;
        } else {
            return false;
        }
    }
    public function getAppointmentVehicles()
    {
        $query  = $this->db->query("SELECT t.id as type_id, t.name as type_name ,m.id as model_id, m.model_name, m.model_year FROM `appointment_vehicle_model` m, `appointment_vehicle_type` t  WHERE t.id=m.appnt_veh_type_id order by type_name, model_year");
        $result = $query->result();
        if (!empty($result)) {
            $darray = array();
            foreach ($result as $row) {
                $darray[$row->type_id]['type_name'] = $row->type_name;
				$darray[$row->type_id]['models'][] = $row;
            }
			return $darray;
        } else {
            return false;
        }
    }
    public function getAppointmentForReminder($data)
    {
        $this->db->select('appointment.name, appointment.mobile, appointment.email, appointment.appointment_start_time, appointment.appointment_end_time, branches.title as branchTitleEn, branches.branch_title_ar branchTitleAr');
        $this->db->from('appointment');
        $this->db->join('branches', 'branches.id = appointment.branch_id', 'inner');
        $this->db->where('Date(appointment_start_time)', $data);
        $this->db->where('is_deleted', 0);
        $query = $this->db->get();

        if (!empty($query->result())) {
            return $query->result();
        } else {
            return false;
        }
    }
}