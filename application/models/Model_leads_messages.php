<?php
Class Model_leads_messages extends Base_Model
{
	public function __construct()
	{
		parent::__construct("leads_messages");
		
	}
	

	public function getLastMessagePhoneStatus($lead_id)
	{
		$this->db->select('*');
		$this->db->from('leads_messages');
		$this->db->order_by('id','desc');
		$this->db->where('lead_id',$lead_id);
		$message_id_arr = array('8', '9', '10');
		$this->db->where_in('message_id', $message_id_arr);
		$this->db->limit(1);
		$query = $this->db->get();
			
		if (!empty($query->result())) 
		{
			$row = $query->result();
			return $row[0]->message_id;

		}else
		{
			return 0;
		} 
	}


	public function getMessagesByLead($fields,$howManyLatest)
	{

		if($howManyLatest === '-1')
		{
	
			$result = $this->db->get_where($this->table,$fields);
			
			//$this->db->last_query(); exit();
			
			if($result->num_rows() > 0)
			{		
				return $result->result();
			}
			
			else
			{
				return false;
			} 
		}
		else
		{

			//latest howManyLatest , it will fetch latest records for action block.

			$gmblQuery = "select * from (
				select * from leads_messages where lead_id = '".$fields['lead_id']."' order by id desc limit ".$howManyLatest."
			) tmp order by tmp.id asc";

			$result = $this->db->query($gmblQuery);

			return $result->result();


		}
	}

	public function checkCallOfActionCreated($lead_id)
	{

		$this->db->select('*');
		$this->db->from('leads_messages');
		$this->db->where('lead_id',$lead_id);
		$this->db->where('admin_created_action_type !=',"");
		$query = $this->db->get();
		$result = $query->result();
			
		if (!empty($result)) 
		{
			return true;

		}else
		{
			return false;
		} 
	}

	public function getfirstCallToCustomerDateTime($lead_id)
	{
		$this->db->select('*');
		$this->db->from('leads_messages');
		$this->db->limit(1);
		$messageIdsArr = array('8', '9', '10');
		$this->db->where_in("message_id",$messageIdsArr);
		$this->db->where("lead_id =",$lead_id);
		$query = $this->db->get();
		//echo $this->db->last_query(); exit();
		$result = $query->result();
		
		if (!empty($result)) {
		   return $result[0]->created_at;
		}else{
		   return false;
		} 
	}
	
	
}