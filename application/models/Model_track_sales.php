<?php
Class Model_track_sales extends Base_Model
{
	public function __construct()
	{
		parent::__construct("leads");
		
	}
	
	
	public function getSales($sold_year="",$sold_month="")
	{
		
		$q = "SELECT l.trackid, u.full_name as `phone_called_by`, lm.created_at as `called_at`,  vsn.name as `sold_vehicle`, (select u2.full_name from users u2 where u2.id=l.sold_by) as `sold_by` ,l.purchased_at FROM leads l, `leads_messages` lm, users u, vehicle_specific_names vsn where l.id=lm.lead_id and lm.created_by=u.id and lm.message_id = 9 and l.purchased_vehicle_id > 0 and vsn.id=l.purchased_vehicle_id  ";	
		$q .= " and l.purchased_at >= '2018-10-03 09:31:20' "; //this column was addd on 3rd oct 2018
		
		if($sold_year) $q .= " and Year(l.purchased_at)= ".$this->db->escape($sold_year)." ";
		if($sold_month) $q .= " and Month(l.purchased_at)= ".$this->db->escape($sold_month)." ";	
		$q .= " and lm.created_by='".$this->session->userdata['user']['id']."' ";
		
		$q .= " group by lm.lead_id ";
		
		$query = $this->db->query($q);
		$result = $query->result();
		
		if(!empty($query->result())){
			return $query->result();
		}else{
			return false;
		}
	
		
	}
	
	
	
}