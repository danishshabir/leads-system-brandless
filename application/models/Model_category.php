<?php
Class Model_category extends Base_Model
{
	public function __construct()
	{
		parent::__construct("categories");
		
	}
	
	public function getCategoryForInboxFetchingEmail($emailSubjectToSearchInCat)
	{
		$this->db->select('*');
		$this->db->from('categories');		
		$this->db->like('email_subject',$emailSubjectToSearchInCat);
		$this->db->order_by('id','desc');
		$this->db->limit(1);

		$query = $this->db->get();
		$result = $query->result();
		
		if (!empty($result)) {
			   return $result;
			}else{
			   return false;

		} 
	}
	public function getCategoryByLimit($page)
	{
		$this->db->select('*');
		$this->db->from('categories');		
		$this->db->limit(4,(($page-1)*4));

		$query = $this->db->get();
		//echo $this->db->last_query();exit();
        $result = $query->result();
		
		if (!empty($result)) {
			   return $result;
			}else{
			   return false;

		} 
	}
	
	
	public function getMultipleCategories($inData)
	{
		$this->db->select('id, concat(title ," - ",title_email_ar) as cat_name');
		//$this->db->select('id, title');
		$this->db->from("categories");		
		$this->db->where('id in ('.$inData.')');
		
		$query = $this->db->get();
		
		//echo $this->db->last_query(); exit;
        $result = $query->result_array();
		
		if (!empty($result)) {
			   return $result;
			}else{
			   return false;

		} 
	}
	
	public function getMultipleVehicles($inData)
	{
		$this->db->select('id,name');
		$this->db->from("vehicles");		
		$this->db->where('id in ('.$inData.')');
		
		$query = $this->db->get();
		
		//echo $this->db->last_query(); exit;
        $result = $query->result_array();
		
		if (!empty($result)) {
			   return $result;
			}else{
			   return false;

		} 
	}
	
	public function getMultipleBranches($inData)
	{
		
		$bid_city_arr = explode(",",$inData);
		$branches_ids_arr = array();
		
		foreach($bid_city_arr as $bidcid)
		{
			$bidcidArr = explode("|",$bidcid);			
			$branches_ids_arr[] = $bidcidArr[0];
		}
		
		$branches_ids_commasep = implode(",",$branches_ids_arr);
		
		//echo($branches_ids_commasep); exit();
		
		$this->db->select('concat(b.id,"|",c.id) as bid,concat(c.title," - ",b.title," ",b.branch_title_ar) as branch_name');
		$this->db->from("branches b, cities c");				
		$this->db->where('b.city_id = c.id and b.id in ('.$branches_ids_commasep.')');
		$this->db->order_by('c.title, b.title');
		
		$query = $this->db->get();
		
		//echo $this->db->last_query(); exit;
        $result = $query->result_array();
		
		if (!empty($result)) {
			   return $result;
			}else{
			   return false;

		} 
	}
	
}