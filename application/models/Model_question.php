<?php
Class Model_question extends Base_Model
{
	public function __construct()
	{
		parent::__construct("questions");
		
	}
    
    public function getMultipleRowsOrderby($survey_id)
	{
	   $this->db->select('*');
       $this->db->from('questions');
       $this->db->where('survey_id', $survey_id);
       $this->db->order_by('order_by','ASC');
       $query = $this->db->get();
       $result = $query->result();
       if(!empty($result))
       {
        return $result;
       }else
       {
        return false;
       }
		 
	}

	
}