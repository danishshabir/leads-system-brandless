<?php
class Central_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function save($tb, $data)
    {
        $this->db->set($data);
        $this->db->insert($tb);
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    public function update($tb, $data, $where, $id, $and_w = NULL, $and = NULL)
    {
        $this->db->where($where, $id);
        if (isset($and) and !empty($and)) {
            $this->db->where($and_w, $and);
        }
        $this->db->update($tb, $data);
        $this->db->last_query();
        if ($this->db->affected_rows() > 0) {
            return $id;
        } else {
            return false;
        }
    }
    public function del($tb, array $arr)
    {
        foreach ($arr as $where => $id) {
            $this->db->where($where, $id);
        }
        $this->db->delete($tb);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return false;
        }
    }
    public function first($tb, $where, $id, $and_w = NULL, $and = NULL)
    {
        if (!empty($where)) {
            $this->db->where($where, $id);
        }
        if (isset($and) and !empty($and)) {
            $this->db->where($and_w, $and);
        }
        $query  = $this->db->get($tb);
        $result = $query->row();
        if ($query->num_rows() > 0) {
            return $result;
        } else {
            return false;
        }
    }
    public function all_fetch($tb, $where = NULL, $id = NULL, $order = NULL)
    {
        if (!empty($where)) {
            $this->db->where($where, $id);
        }
        if (!empty($order)) {
            $this->db->order_by($order, "ASC");
        }
        $query  = $this->db->get($tb);
        $result = $query->result();
        if ($query->num_rows() > 0) {
            return $result;
        } else {
            return false;
        }
    }
    public function fetch_tutorial_videos($tb)
    {
        $this->db->where('active', 'On');
        $this->db->where('show_home', 'Off');
        $this->db->where('featured', 'Off');
        if (!empty($order)) {
            $this->db->order_by($order, "ASC");
        }
        $query  = $this->db->get($tb);
        $result = $query->result();
        if ($query->num_rows() > 0) {
            return $result;
        } else {
            return false;
        }
    }
    public function all_fetch_not_in($tb, $where = NULL, $id = NULL)
    {
        if (!empty($where)) {
            $this->db->where_not_in($where, $id);
        }
        $query  = $this->db->get($tb);
        $result = $query->result();
        if ($query->num_rows() > 0) {
            return $result;
        } else {
            return false;
        }
    }
    public function select_all_array($tb, array $arr)
    {
        foreach ($arr as $where => $id) {
            $this->db->where($where, $id);
        }
        $query  = $this->db->get($tb);
        $result = $query->result();
        if ($query->num_rows() > 0) {
            return $result;
        } else {
            return false;
        }
    }
    public function select_array($tb, array $arr)
    {
        foreach ($arr as $where => $id) {
            $this->db->where($where, $id);
        }
        $query  = $this->db->get($tb);
        $result = $query->row();
        if ($query->num_rows() > 0) {
            return $result;
        } else {
            return false;
        }
    }
    public function update_array($tb, $data, $arr)
    {
        foreach ($arr as $where => $id) {
            $this->db->where($where, $id);
        }
        $this->db->update($tb, $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function count_rows($tb, array $arr, array $arr2 = null)
    {
        foreach ($arr as $where => $id) {
            $this->db->where($where, $id);
        }
		if($arr2) {
			foreach ($arr2 as $where => $id) {
				$this->db->where($where.' !=', $id);
			}
		}
        $query = $this->db->get($tb);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return false;
        }
    }
    public function select_max_field($tb, array $arr, $field)
    {
        $this->db->select_max($field);
        foreach ($arr as $where => $id) {
            $this->db->where($where, $id);
        }
        $result = $this->db->get($tb)->row();
        return $result;
    }
    public function unique_row($tb, $where, $id, $and_w = NULL, $and = NULL, $and_other = NULL, $and_us = NULL)
    {
        if (!empty($where)) {
            $this->db->where($where, $id);
        }
        if (isset($and) and !empty($and)) {
            $this->db->where($and_w, $and);
        }
        if (isset($and_us) and !empty($and_us)) {
            $this->db->where($and_other, $and_us);
        }
        $query  = $this->db->get($tb);
        $result = $query->row();
        if ($query->num_rows() > 0) {
            return $result;
        } else {
            return false;
        }
    }
    public function countAppointmentByAdviserId($tb, $arr, $id=null)
    {
        if(!empty($id)) {
            $this->db->where('id !=', $id);
        }
        $this->db->where('is_deleted', 0);
        return $this->db->where($arr)->get($tb)->num_rows();
    }
}