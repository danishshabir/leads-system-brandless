<?php
Class Model_manager_report extends Base_Model
{

	public function __construct()
	{
			
	}

	//moved to common model
	/*
	public function getManagers()
	{
		$manual_query = "SELECT sd.id as dep_id, u.id, u.full_name as name, sd.title as dep_name FROM `structure_departments` sd, users u where u.id=sd.manager_id and sd.id in (56, 57, 55, 52, 53, 54) group by u.id order by u.full_name";
		
		$query = $this->db->query($manual_query);
			
		return $query->result();	
	}*/
	
	public function getAllDepUsersCommaSep($filterData)
	{
		//$filterData['dep_id']
		
		$manual_query = "SELECT GROUP_CONCAT(user_id) as commaSepUIds FROM `struc_dep_users` where struc_dep_id='".$filterData['dep_id']."'";
		
		$query = $this->db->query($manual_query);
			
		$row = $query->row();	
		
		return $row->commaSepUIds; 
	}
	
	public function getUsersRes($userIds)
	{
		//$filterData['dep_id']
		
		$manual_query = "SELECT u.* FROM users u where u.id in (".$userIds.")";
		
		$query = $this->db->query($manual_query);
		if($query->num_rows() > 0)
			return $query->result();		
		
		return null;
	}
	
	public function getTimeToMakeCallActionEachUserAvg($filterData)
	{
		/*$this->db->select("u.full_name, avg(TIMESTAMPDIFF(SECOND, l.assigned_at, first_call_time)) as avgval, count(lm.id) as num_leads");
		$this->db->from("leads l, users u, leads_messages lm");
		$this->db->where("u.id=lm.created_by and lm.message_id in (8,9,10) and l.id=lm.lead_id and first_call_time!='0000-00-00 00:00:00' and assigned_at!='0000-00-00 00:00:00' and first_call_time > assigned_at and lm.created_by=l.assign_to");
		$this->db->where("lm.created_by in(".$filterData['depUsers'].")");		
		$this->db->where('l.duplicate_of', '0');
				
		if(
		(!isset($filterData['date_from']) || (isset($filterData['date_from']) && $filterData['date_from']==""))
		&& (!isset($filterData['date_from']) || (isset($filterData['date_from']) && $filterData['date_from']==""))
		)
		{		
			$first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
			$last_day_this_month  = date('Y-m-t');

			$filterData['date_from'] = $first_day_this_month;
			$filterData['date_to'] = $last_day_this_month;
		}
				
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		
		$this->db->group_by('lm.created_by'); 
		$this->db->order_by('avgval'); 	
				
		$query=$this->db->get();*/
		
		if(
		(!isset($filterData['date_from']) || (isset($filterData['date_from']) && $filterData['date_from']==""))
		&& (!isset($filterData['date_from']) || (isset($filterData['date_from']) && $filterData['date_from']==""))
		)
		{		
			$first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
			$last_day_this_month  = date('Y-m-t');

			$filterData['date_from'] = $first_day_this_month;
			$filterData['date_to'] = $last_day_this_month;
		}
		
		/*$manual_query = "SELECT u.full_name, ifnull(avg(TIMESTAMPDIFF(SECOND, l.assigned_at, first_call_time)),0) as avgval, count(l.id) as num_leads FROM 
		users u left join leads_messages lm on u.id = lm.created_by and lm.message_id in (8,9,10) left join leads l on l.id = lm.lead_id and lm.created_by = l.assign_to and first_call_time != '0000-00-00 00:00:00' and assigned_at != '0000-00-00 00:00:00' "; 
		
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";

		$manual_query .= " and first_call_time > assigned_at AND l.duplicate_of = '0'
		where u.id in (".$filterData['depUsers'].")
		GROUP BY u.id ORDER BY avgval";*/
		
		
		$manual_query = "select full_name,avgval,num_leads  from (
    
    SELECT u.id as uid1, u.full_name, ifnull(avg(TIMESTAMPDIFF(SECOND, l.assigned_at, first_call_time)),0) as avgval FROM users u left join leads_messages lm on u.id = lm.created_by and lm.message_id in (8,9,10) left join leads l on l.id = lm.lead_id and lm.created_by = l.assign_to and first_call_time != '0000-00-00 00:00:00' and assigned_at != '0000-00-00 00:00:00' "; 
	
	if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(l.assigned_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(l.assigned_at) <= '".$filterData['date_to']."'";
	
	$manual_query .= " and date(l.assigned_at) >= '2017-02-01' and date(l.assigned_at) <= '2017-02-28' and first_call_time > assigned_at AND l.duplicate_of = '0' where u.id in (".$filterData['depUsers'].") GROUP BY u.id
    
    
    ) t1 left join (
        
        SELECT u.id as uid2, count(distinct l.id) as num_leads FROM users u left join leads_messages lm on u.id = lm.created_by and lm.message_id in (8,9,10) left join leads l on l.id = lm.lead_id and lm.created_by = l.assign_to and first_call_time != '0000-00-00 00:00:00' and assigned_at != '0000-00-00 00:00:00' ";

		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";

		$manual_query .= " and date(lm.created_at) >= '2017-02-01' and date(lm.created_at) <= '2017-02-28' and first_call_time > assigned_at AND l.duplicate_of = '0' where u.id in (".$filterData['depUsers'].") GROUP BY u.id
        
        
        ) t2 on t1.uid1=t2.uid2 order by avgval";
				
		
				
		$query = $this->db->query($manual_query);

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $result = $query->result();
		}

		return null;
	}
	
	public function getTimeToFinishActionEachUserAvg($filterData)
	{
		
		$innerQuery = " ( ";
		$innerQuery .= " select count(distinct l2.id) from leads l2, users u2, leads_messages lm2 where u2.id=lm2.created_by and l2.id=lm2.lead_id and lm2.message_id=7 and l2.duplicate_of=0 and l2.finished_time!='0000-00-00 00:00:00' and l2.assigned_at!='0000-00-00 00:00:00' and l2.finished_time > l2.assigned_at and lm2.created_by=l2.assign_to and (l2.status=\"Finished\" or l2.status=\"Closed\" or l2.status=\"Approved and Archived\") and l2.assign_to=u.id ";
		
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$innerQuery .=" and date(lm2.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$innerQuery .=" and date(lm2.created_at) <= '".$filterData['date_to']."'";
		
		$innerQuery .= " ) ";
		
		$innerQuery .=" as num_leads ";
		
		
		
		/*$this->db->select("u.full_name, avg(TIMESTAMPDIFF(SECOND, l.assigned_at, finished_time)) as avgval, ".$innerQuery."");
		$this->db->from("leads l, users u, leads_messages lm");
		$this->db->where("finished_time!='0000-00-00 00:00:00' and assigned_at!='0000-00-00 00:00:00' and finished_time > assigned_at");
		$this->db->where("lm.message_id=7");
		$this->db->where("u.id=lm.created_by and l.id=lm.lead_id and lm.created_by=l.assign_to");
		$this->db->where("l.assign_to in(".$filterData['depUsers'].")");	
		$this->db->where('l.duplicate_of', '0');
		$this->db->where('(l.status="Finished" or l.status="Closed" or l.status="Approved and Archived")');
	
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
		
		$this->db->group_by('lm.created_by'); 
		$this->db->order_by('avgval'); 	
		
		$query=$this->db->get();*/
		
		$manual_query = "select u.full_name, avg(TIMESTAMPDIFF(SECOND, l.assigned_at, finished_time)) as avgval, ".$innerQuery." FROM 
		users u left join leads_messages lm on u.id=lm.created_by and lm.message_id=7 left join leads l on l.id=lm.lead_id and lm.created_by=l.assign_to and finished_time!='0000-00-00 00:00:00' and assigned_at!='0000-00-00 00:00:00' and finished_time > assigned_at and (l.status='Finished' or l.status='Closed' or l.status='Approved and Archived') and l.duplicate_of = '0' "; 
		
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(l.assigned_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(l.assigned_at) <= '".$filterData['date_to']."'";

		$manual_query .= " where u.id in (".$filterData['depUsers'].")
		GROUP BY u.id ORDER BY avgval";
				
		//echo $manual_query; exit();
				
		$query = $this->db->query($manual_query);		

		if($query->num_rows() > 0)
		{
			return $result = $query->result();
		}

		return null;
	}
	
	public function getNumOfDisapprovedActionEachUser($filterData)
	{
		
		$manual_query = "select u.full_name, sum(case when lm.message_id=7 and lm.created_by=l.assign_to then 1 else 0 end) as finished, sum(case when lm.message_id=14 and lm.created_by=l.created_by then 1 else 0 end) as disapproved from leads l, users u, leads_messages lm where u.id=l.assign_to and l.id=lm.lead_id and l.duplicate_of=0 and u.id in(".$filterData['depUsers'].") ";
		
		$manual_query .= " and (l.status='Finished' or l.status='Closed' or l.status='Approved and Archived') ";
		
		/*if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and case when lm.message_id=7 then date(l.finished_time) else date(lm.created_at) end >= '".$filterData['date_from']."' ";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and case when lm.message_id=7 then date(l.finished_time) else date(lm.created_at) end <= '".$filterData['date_to']."' ";*/
		
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."' ";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."' ";
		
		$manual_query .= " group by u.id order by finished desc";		
		
		//echo $manual_query; exit();
		
		$query=$this->db->query($manual_query);
		
		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $result = $query->result();
		}

		return null;
		
	}
	
	public function getNumOfSuccessfullCallActionEachUser($filterData)
	{ 
	
		/*$manual_query = "SELECT `u`.`full_name`, count(lm.id) as success, ( select count(ll.id) from leads ll where ll.assign_to=l.assign_to and ll.duplicate_of=0 "; 
		
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(ll.assigned_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(ll.assigned_at) <= '".$filterData['date_to']."'";
		
		$manual_query .= ") as num_leads FROM `users` `u` left join `leads_messages` `lm` on `u`.`id` = `lm`.`created_by` and `lm`.`message_id` = 9 ";

		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";
		
		$manual_query .= " left join `leads` `l` on `l`.`id` = `lm`.`lead_id` WHERE u.id in(".$filterData['depUsers'].") GROUP BY `u`.`id` ORDER BY `success` DESC";
	
		$query=$this->db->query($manual_query);*/
		
		$manual_query = "SELECT `u`.`full_name`, count(lm.id) as success, ( select count(ll.id) from leads ll where ll.assign_to=l.assign_to and ll.duplicate_of=0 and (ll.status='Assigned' or ll.status='Finished' or ll.status='No Response' or ll.status='Disapproved')"; 
		
		//if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(ll.assigned_at) >= '".$filterData['date_from']."'";
		//if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(ll.assigned_at) <= '".$filterData['date_to']."'";
		
		$manual_query .= ") as num_leads FROM `users` `u` left join `leads` `l` on l.assign_to=u.id ";

		$manual_query .= " left join `leads_messages` `lm` on `l`.`id` = `lm`.`lead_id` and `u`.`id` = `lm`.`created_by` and `lm`.`message_id` = 9 ";

		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";
		
		$manual_query .= " WHERE u.id in(".$filterData['depUsers'].") GROUP BY `u`.`id` ORDER BY `success` DESC";
	
		$query=$this->db->query($manual_query);

		//echo $manual_query; exit();

		if($query->num_rows() > 0)
		{
			return $result = $query->result();
		}

		return null;
	}
	
	public function getBranchesAvgScore($filterData)
	{
			
		$this->db->select("sd.id, sd.title, avg( sr.answer_val ) as avgval");
		$this->db->from("struc_dep_users sdu,structure_departments sd, leads l, survey_results sr");
		$this->db->where("sdu.struc_dep_id in (56, 57, 55, 52, 53, 54) and sd.id=sdu.struc_dep_id and l.survey_response=1 and sr.lead_id=l.id and l.assign_to=sdu.user_id");
		$this->db->where('l.duplicate_of', '0');
		$this->db->group_by("sdu.struc_dep_id");
		$this->db->order_by("avgval");

		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $result = $query->result();
		}

		return null;	
		
	}
	
	public function getTotalAvgScore($filterData)
	{
		$this->db->select("avg( sr.answer_val ) as avgval");
		$this->db->from("survey_results sr, leads l");
		$this->db->where("l.survey_response!=0 and sr.lead_id=l.id");
		$this->db->where('l.duplicate_of', '0');	
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('sr.created_at >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('sr.created_at <=', $filterData['date_to']);
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $result = $query->row();
		}

		return null;
	}
	
	public function getAvgScoreYearly($filterData)
	{
		$yearlyAvg = array();
				
		$this->db->select("DATE_FORMAT(sr.created_at,'%b %Y') as month, avg( sr.answer_val ) as avgval");
		$this->db->from("leads l, survey_results sr");
		$this->db->where("l.survey_response!=0 and sr.lead_id=l.id");
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.assign_to in(".$filterData['depUsers'].")");
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);
		
		$this->db->group_by('YEAR(sr.created_at), MONTH(sr.created_at)'); 

		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			foreach($result as $row)
				$yearlyAvg[$row->month] = $row->avgval;
		}
			
		return $yearlyAvg;		

	}
	
	public function getAvgScore($filterData)
	{		
		$this->db->select("avg( sr.answer_val ) as avgval");
		$this->db->from("leads l, survey_results sr");
		$this->db->where("l.survey_response!=0 and sr.lead_id=l.id");
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.assign_to in(".$filterData['depUsers'].")");
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(sr.created_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(sr.created_at) <=', $filterData['date_to']);
				
		$query=$this->db->get();

		//echo $this->db->last_query();exit();
		if($query->num_rows() > 0)
		{
			return $result = $query->row();			
		}
			
		return 0;

	}
	
	//actually due to branch and department issue, currently we can count graphs based upon assigned leads.
	//so one a new leads arrives then it will not show in this graph until it is assigned.
	public function getDGraphTotalLeadsYearly($filterData)
	{
		$dYearlyAvg = array();
		
		$queryStr = "SELECT DATE_FORMAT(l.assigned_at, '%b %Y') as month, count( l.id ) as num FROM `leads` `l`, `users` `u` WHERE `u`.`id` = `l`.`assign_to` and `l`.`assign_to` in (".$filterData['depUsers'].") AND `l`.`duplicate_of` = '0' ";
		
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$queryStr .= " and date(l.assigned_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$queryStr .= " and date(l.assigned_at) <= '".$filterData['date_to']."'";		
		
		$queryStr .= " GROUP BY YEAR(l.assigned_at), MONTH(l.assigned_at)";
		
		//echo $queryStr;
		
		$query = $this->db->query($queryStr);

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			foreach($result as $row)
			{					
				$dYearlyAvg[$row->month] = $row->num;
			}
		}
		return $dYearlyAvg;

		
	}
	
	public function getDGraphTotalComLeadsYearly($filterData)
	{
		
		$dYearlyAvg = array();

		$this->db->select("DATE_FORMAT(lm.created_at,'%b %Y') as month, count( lm.created_by ) as num");
		$this->db->from("leads_messages lm, leads l");
		$this->db->where("l.id=lm.lead_id and lm.created_by in (".$filterData['depUsers'].") and lm.message_id=5 and lm.vehicle_specific_names_id!=''");
		$this->db->where('(lm.test_drive_completed=1 or lm.test_drive_type="Walk in")');
		
		
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
		$this->db->group_by('YEAR(lm.created_at), MONTH(lm.created_at)'); 

		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			foreach($result as $row)
				$dYearlyAvg[$row->month] = $row->num;
		}
		return $dYearlyAvg;

		
	}
	
	public function getUserTLeads($filterData)
	{
		$this->db->select("u.full_name, u.id as userid, count( u.id ) as num");
		$this->db->from("leads l, users u");
		$this->db->where("u.id=l.assign_to");
		$this->db->where("l.assign_to in(".$filterData['depUsers'].")");
		$this->db->where('l.duplicate_of', '0');
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);

		if(isset($filterData['depUsers']) && $filterData['depUsers']!="") $this->db->where("l.assign_to in(".$filterData['depUsers'].")");

		$this->db->group_by('l.assign_to'); 
		$this->db->order_by('num','desc'); 	
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $result = $query->result();
		}

		return null;
	}
	
	//actually due to branch and department issue, currently we can count graphs based upon assigned leads.
	//so one a new leads arrives then it will not show in this graph until it is assigned.
	public function getPerCatAndSourceCurrentMonth($filterData)
	{	
		if(
		(!isset($filterData['date_from']) || (isset($filterData['date_from']) && $filterData['date_from']==""))
		&& (!isset($filterData['date_from']) || (isset($filterData['date_from']) && $filterData['date_from']==""))
		)
		{		
			$first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
			$last_day_this_month  = date('Y-m-t');

			$filterData['date_from'] = $first_day_this_month;
			$filterData['date_to'] = $last_day_this_month;
		}
		
	
		$this->db->select("CONCAT(form_type,'',IF(manual_source = '0', '', (select s.title from sources s where s.id=l.manual_source))) as title, count( * ) as num");
		$this->db->from("leads l");
		$this->db->where("l.assign_to in(".$filterData['depUsers'].")");
		$this->db->where('l.duplicate_of', '0');		
	
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		

		$this->db->group_by('l.form_type, manual_source'); 
		$this->db->order_by('num','desc'); 	
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $result = $query->result();
		}

		return null;
		
	}
	
	//actually due to branch and department issue, currently we can count graphs based upon assigned leads.
	//so one a new leads arrives then it will not show in this graph until it is assigned.
	public function getPerCatAndSource($filterData)
	{	
		$this->db->select("CONCAT(form_type,'',IF(manual_source = '0', '', (select s.title from sources s where s.id=l.manual_source))) as title, count( * ) as num");
		$this->db->from("leads l");
		$this->db->where("l.assign_to in(".$filterData['depUsers'].")");
		$this->db->where('l.duplicate_of', '0');		

		// 6th July 2017
		//if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
		//if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);		
		
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);	

		$this->db->group_by('l.form_type, manual_source'); 
		$this->db->order_by('num','desc'); 	
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $result = $query->result();
		}

		return null;
		
	}
	
	
	//======Start conversions=====
	
	public function getCountDepLeads($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l");
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);			
		}
		
		
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepScheduled($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and lm.message_id = 5');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}


	public function getCountDepCompleted($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and (lm.test_drive_type="Walk in" or test_drive_completed=1)');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepPurchased($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '2');		
		$this->db->where('l.purchased_vehicle_id!=""');
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");	
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Purchased a car%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;	

	}

	public function getCountDepLost($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '1');		
		$this->db->where("l.assign_to in (".$filterData['depUsers'].")");	
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;

	}
	
	//====
	
	public function getCountDepLeads1($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l");
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");
		$this->db->where('l.manual_source', '6');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepScheduled1($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and lm.message_id = 5');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '6');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}


	public function getCountDepCompleted1($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and (lm.test_drive_type="Walk in" or test_drive_completed=1)');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '6');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepPurchased1($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '2');		
		$this->db->where('l.purchased_vehicle_id!=""');
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");	
		$this->db->where('l.manual_source', '6');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Purchased a car%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;	

	}

	public function getCountDepLost1($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '1');		
		$this->db->where("l.assign_to in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '6');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;

	}
	
	//====
	
	public function getCountDepLeads2($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l");
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");
		$this->db->where('l.manual_source', '5');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepScheduled2($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and lm.message_id = 5');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '5');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}


	public function getCountDepCompleted2($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and (lm.test_drive_type="Walk in" or test_drive_completed=1)');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '5');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepPurchased2($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '2');		
		$this->db->where('l.purchased_vehicle_id!=""');
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");	
		$this->db->where('l.manual_source', '5');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Purchased a car%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;	

	}

	public function getCountDepLost2($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '1');		
		$this->db->where("l.assign_to in (".$filterData['depUsers'].")");
		$this->db->where('l.manual_source', '5');		
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;

	}
	
	//====
	
	
	public function getCountDepLeads3($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l");
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");
		$this->db->where('l.manual_source', '8');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepScheduled3($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and lm.message_id = 5');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '8');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}


	public function getCountDepCompleted3($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and (lm.test_drive_type="Walk in" or test_drive_completed=1)');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '8');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepPurchased3($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '2');		
		$this->db->where('l.purchased_vehicle_id!=""');
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");	
		$this->db->where('l.manual_source', '8');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Purchased a car%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;	

	}

	public function getCountDepLost3($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '1');		
		$this->db->where("l.assign_to in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '8');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;

	}
	
	//====
	
	public function getCountDepLeads4($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l");
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");
		$this->db->where('l.manual_source', '9');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepScheduled4($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and lm.message_id = 5');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '9');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}


	public function getCountDepCompleted4($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and (lm.test_drive_type="Walk in" or test_drive_completed=1)');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '9');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepPurchased4($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '2');		
		$this->db->where('l.purchased_vehicle_id!=""');
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");	
		$this->db->where('l.manual_source', '9');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Purchased a car%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;	

	}

	public function getCountDepLost4($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '1');		
		$this->db->where("l.assign_to in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '9');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;

	}
	
	//===

	public function getCountDepLeads5($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l");
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");
		$this->db->where('l.manual_source', '10');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepScheduled5($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and lm.message_id = 5');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '10');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}


	public function getCountDepCompleted5($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and (lm.test_drive_type="Walk in" or test_drive_completed=1)');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '10');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepPurchased5($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '2');		
		$this->db->where('l.purchased_vehicle_id!=""');
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");	
		$this->db->where('l.manual_source', '10');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Purchased a car%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;	

	}

	public function getCountDepLost5($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '1');		
		$this->db->where("l.assign_to in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '10');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;

	}
	
	//====
	
		public function getCountDepLeads6($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l");
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");
		$this->db->where('l.manual_source', '11');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepScheduled6($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and lm.message_id = 5');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '11');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}


	public function getCountDepCompleted6($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and (lm.test_drive_type="Walk in" or test_drive_completed=1)');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '11');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepPurchased6($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '2');		
		$this->db->where('l.purchased_vehicle_id!=""');
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");	
		$this->db->where('l.manual_source', '11');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Purchased a car%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;	

	}

	public function getCountDepLost6($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '1');		
		$this->db->where("l.assign_to in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '11');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;

	}
	
	//===
	
		public function getCountDepLeads7($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l");
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");
		$this->db->where('l.manual_source', '12');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepScheduled7($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and lm.message_id = 5');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '12');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}


	public function getCountDepCompleted7($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and (lm.test_drive_type="Walk in" or test_drive_completed=1)');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '12');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepPurchased7($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '2');		
		$this->db->where('l.purchased_vehicle_id!=""');
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");	
		$this->db->where('l.manual_source', '12');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Purchased a car%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;	

	}

	public function getCountDepLost7($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '1');		
		$this->db->where("l.assign_to in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '12');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;

	}
	
	//====
	
		public function getCountDepLeads8($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l");
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");
		$this->db->where('l.manual_source', '13');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepScheduled8($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and lm.message_id = 5');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '13');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}


	public function getCountDepCompleted8($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and (lm.test_drive_type="Walk in" or test_drive_completed=1)');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '13');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepPurchased8($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '2');		
		$this->db->where('l.purchased_vehicle_id!=""');
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");	
		$this->db->where('l.manual_source', '13');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Purchased a car%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;	

	}

	public function getCountDepLost8($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '1');		
		$this->db->where("l.assign_to in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '13');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;

	}
	
	//====
	
		public function getCountDepLeads9($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l");
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");
		$this->db->where('l.manual_source', '15');		
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepScheduled9($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and lm.message_id = 5');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '15');		
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}


	public function getCountDepCompleted9($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and (lm.test_drive_type="Walk in" or test_drive_completed=1)');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '15');		
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepPurchased9($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '2');		
		$this->db->where('l.purchased_vehicle_id!=""');
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");	
		$this->db->where('l.manual_source', '15');		
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Purchased a car%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;	

	}

	public function getCountDepLost9($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '1');		
		$this->db->where("l.assign_to in (".$filterData['depUsers'].")");	
		$this->db->where('l.manual_source', '15');		
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;

	}
	
	//====
	
	public function getCountDepLeads10($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l");
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");		
		$this->db->where('l.form_type', 'Test Drive');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepScheduled10($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and lm.message_id = 5');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.form_type', 'Test Drive');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}


	public function getCountDepCompleted10($filterData) 
	{
		$this->db->select("count( lm.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('lm.lead_id = l.id and (lm.test_drive_type="Walk in" or test_drive_completed=1)');
		$this->db->where("lm.created_by in (".$filterData['depUsers'].")");	
		$this->db->where('l.form_type', 'Test Drive');
		$this->db->where('l.duplicate_of', '0');
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;
	}

	public function getCountDepPurchased10($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '2');		
		$this->db->where('l.purchased_vehicle_id!=""');
		$this->db->where("(l.assign_to in (".$filterData['depUsers']."))");	
		$this->db->where('l.form_type', 'Test Drive');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Purchased a car%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;	

	}

	public function getCountDepLost10($filterData) 
	{
		$this->db->select("count( l.id ) as num");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where('l.tag_id', '1');		
		$this->db->where("l.assign_to in (".$filterData['depUsers'].")");	
		$this->db->where('l.form_type', 'Test Drive');
		$this->db->where('l.duplicate_of', '0');
		$this->db->where("l.id=lm.lead_id and lm.message_id=4 and lm.comments like '%Lost.%'");	
		
		if(isset($filterData['conversion_date_range']) && $filterData['conversion_date_range']=="lead_created") 
		{ 
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.created_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.created_at) <=', $filterData['date_to']);
		
		}else
		{			
			if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
			if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		}
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->num;
			
		}
		
		return 0;

	}	
	
	//======End conversions=====

	public function getCarTypes($filterData)
	{
		//$users = array(); 
		$cars = array();
		$branches = array();
		//$carsScheduled = array();
		//$carsCompleted = array();

		$this->db->select("l.branch_id, lm.vehicle_specific_names_id, lm.test_drive_completed, lm.test_drive_type");
		$this->db->from("leads l,leads_messages lm");
		$this->db->where("l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!=''");
		$this->db->where('l.duplicate_of', '0');
		
		$this->db->where("l.assign_to in(".$filterData['depUsers'].")");

		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			foreach($result as $row)
			{
				$bId = $row->branch_id;
				$car = $row->vehicle_specific_names_id;
				
				//check if same lead has same cars twice, then its a mistake.
				$carsArr = explode(",",$car);
				$carsArr = array_unique($carsArr);
				//now convert the specific to general
				for($ctgc=0; $ctgc<count($carsArr); $ctgc++)
				{
					$gId = $this->getGIdOfSpecific($carsArr[$ctgc]);
					if(!in_array($gId,$cars))
					{
						$cars[] = $gId;
						$branches[$gId] = $bId;

					}
					else
					{
						$branches[$gId] = $branches[$gId].",".$bId;
					}
				}				
			}
		}

		return array($cars,$branches);
	}
	
	public function getCarsEachBranch($filterData)
	{
		$branchs = array();
		$cars = array();
		$carsCompleted = array();

		$this->db->select("l.branch_id, lm.vehicle_specific_names_id, lm.test_drive_completed, lm.test_drive_type");
		$this->db->from("leads l, leads_messages lm");
		$this->db->where("l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!=''");

		$this->db->where('l.duplicate_of', '0');	
		
		$this->db->where("l.assign_to in(".$filterData['depUsers'].")");
		
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
		
		$query=$this->db->get();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			foreach($result as $row)
			{
				$branch_id = $row->branch_id;
				$car = $row->vehicle_specific_names_id;
				
				//check if same lead has same cars twice, then its a mistake.
				$carsArr = explode(",",$car);
				$carsArr = array_unique($carsArr);
				//now convert the specific to general
				for($ctgc=0; $ctgc<count($carsArr); $ctgc++)
				{
					$carsArr[$ctgc] = $this->getGIdOfSpecific($carsArr[$ctgc]);
				}
				//$car = implode(",",$carsArr); //can be multiple in comma
				$car = $carsArr[0];
				//==	

				if(!in_array($branch_id, $branchs))
				{
					$branchs[] = $branch_id;
					$cars[$branch_id] = $car;
					if($row->test_drive_completed || $row->test_drive_type === "Walk in")
						$carsCompleted[$branch_id] = $car;
					else
						$carsCompleted[$branch_id] = ""; //to make the index
				}
				else
				{
					$cars[$branch_id] = $cars[$branch_id].','.$car;
					if($row->test_drive_completed || $row->test_drive_type === "Walk in")
						$carsCompleted[$branch_id] = $carsCompleted[$branch_id].','.$car;
				}
			}
		}
		
		return array($branchs,$cars,$carsCompleted);
	}
	
	public function getGCarTypeEachBranch($filterData)
	{
		$branches = array();
		$cars = array();	

		$this->db->select("l.branch_id, CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')) as vehicle");
		$this->db->from("leads l");
		$this->db->where("(l.vehicle!='' or l.vehicle_specific_names_id!='')");	
		$this->db->where('l.duplicate_of', '0');

		$this->db->where("l.assign_to in(".$filterData['depUsers'].")");
		
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			foreach($result as $row)
			{
				$bId = $row->branch_id;
				$car = $row->vehicle;
				
				//check if same lead has same cars twice, then its a mistake.
				$carsArr = explode(",",$car);
				$carsArr = array_unique($carsArr);
				$car = implode(",",$carsArr); //can be multiple in comma
				//==	

				if(!in_array($bId, $branches))
				{
					$branches[] = $bId;
					$cars[$bId] = $car;				
				}
				else
				{
					$cars[$bId] = $cars[$bId].','.$car;				
				}
			}
		}
		
		return array($branches,$cars);
	}
	
	public function getCarTypeEachUser($filterData)
	{
		$users = array(); 
		$carsScheduled = array();
		$carsCompleted = array();

		$this->db->select("lm.created_by, lm.vehicle_specific_names_id, lm.test_drive_completed, lm.test_drive_type");
		$this->db->from("leads l,leads_messages lm");
		$this->db->where("l.assign_to in(".$filterData['depUsers'].")");
		$this->db->where("l.id=lm.lead_id and lm.message_id=5 and lm.vehicle_specific_names_id!=''");
		$this->db->where('l.duplicate_of', '0');
		

		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			foreach($result as $row)
			{
				$uId = $row->created_by;
				$car = $row->vehicle_specific_names_id;
				
				//check if same lead has same cars twice, then its a mistake.
				$carsArr = explode(",",$car);
				$carsArr = array_unique($carsArr);
				//now convert the specific to general
				for($ctgc=0; $ctgc<count($carsArr); $ctgc++)
				{
					 //$ctgc this is specific id. 
					$carsArr[$ctgc] = $this->getGIdOfSpecific($carsArr[$ctgc]);
				}
				//$car = implode(",",$carsArr); //can be multiple in comma
				$car = $carsArr[0];
				//==			

				if(!in_array($uId, $users))
				{
					$users[] = $uId;
					$carsScheduled[$uId] = $car;
					if($row->test_drive_completed || $row->test_drive_type === "Walk in")
						$carsCompleted[$uId] = $car;
					else
						$carsCompleted[$uId] = ""; //to make the index
				}
				else
				{
					$carsScheduled[$uId] = $carsScheduled[$uId].','.$car;
					if($row->test_drive_completed || $row->test_drive_type === "Walk in")
						$carsCompleted[$uId] = $carsCompleted[$uId].','.$car;
				}
			}
		}
		
		return array($users,$carsScheduled,$carsCompleted);
	}
	
	public function getGCarTypeEachUser($filterData)
	{
		$users = array();
		$cars = array();	

		$this->db->select("l.assign_to, CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')) as vehicle");
		
		$this->db->from("leads l");
		$this->db->where("(l.vehicle!='' or l.vehicle_specific_names_id!='') and l.assign_to!='0'");
		$this->db->where("l.assign_to in(".$filterData['depUsers'].")");		
		$this->db->where('l.duplicate_of', '0');

		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			foreach($result as $row)
			{
				$user_id = $row->assign_to;
				$car = $row->vehicle;
				
				//check if same lead has same cars twice, then its a mistake.
				$carsArr = explode(",",$car);
				$carsArr = array_unique($carsArr);
				$car = implode(",",$carsArr); //can be multiple in comma
				//==	

				if(!in_array($user_id, $users))
				{
					$users[] = $user_id;
					$cars[$user_id] = $car;				
				}
				else
				{
					$cars[$user_id] = $cars[$user_id].','.$car;				
				}
			}
		}
		
		return array($users,$cars);
	}
	
	public function getCarsConversions3($filterData) 
	{
			
		$manual_query = "select id1 as id, ifnull(name1, (select v2.name from vehicles v2 where v2.id=combined_table.id2)) as name, ifnull(requests_count, 0) as requests_count, ifnull(purchased_count, 0) as purchased_count from 

		(
			
		select * from (SELECT `v`.`id` as id1, `v`.`name` as name1, count(v.name) as requests_count FROM `vehicles` `v`, `leads` `l` WHERE FIND_IN_SET (v.id,(CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')))) > 0 and `l`.`assign_to` in (".$filterData['depUsers'].") AND `l`.`duplicate_of` = '0'";


		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(l.assigned_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(l.assigned_at) <= '".$filterData['date_to']."'";

		$manual_query .= " group by v.name) total_requests 
			
		right join 
			
		(select * from (SELECT `v`.`id` as id2, `v`.`name` as name2, count(l.id) as purchased_count FROM `leads_messages` `lm`, `vehicle_specific_names` `vs`, `vehicles` `v`, `leads` `l` WHERE `lm`.`message_id` = 4 and `lm`.`comments` like \"%Purchased a car%\" AND `v`.`id` = `vs`.`vehicle_id` and `vs`.`id` = `l`.`purchased_vehicle_id` and `l`.`id` = `lm`.`lead_id` and `l`.`tag_id` = 2 AND `lm`.`created_by` in (".$filterData['depUsers'].") AND `l`.`duplicate_of` = '0'";
		
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";

		$manual_query .= " GROUP BY `v`.`name`) t2) user_counts2    
		 
		on total_requests.id1 = user_counts2.id2 
		
		) combined_table";
		
		
		//$query=$this->db->get();
		//echo $manual_query; exit();
		
		$query = $this->db->query($manual_query);
		
		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $query->result();		
		}
		
		return null;
	}
	
	public function getCarsConversions($filterData)  
	{
		/*$this->db->select("v.id, v.name, count(lm.id) as scheduled_count, sum(case when lm.test_drive_type='Walk in' or test_drive_completed=1 then 1 else 0 end) as completed_count");
		$this->db->from("leads_messages lm, vehicle_specific_names vs, vehicles v, leads l");
		$this->db->where("lm.message_id=5 and lm.vehicle_specific_names_id!='' and FIND_IN_SET (vs.id,lm.vehicle_specific_names_id)>0 and v.id=vs.vehicle_id and l.id=lm.lead_id");
		$this->db->where("lm.created_by = '".$filterData['user_id']."'");
		$this->db->where('l.duplicate_of', '0');

		
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(lm.created_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(lm.created_at) <=', $filterData['date_to']);
		
		$this->db->group_by('v.name');*/
		
			
		/*$manual_query = "select  id1 as id, name1 as name, ifnull(requests_count, 0) as requests_count, ifnull(scheduled_count, 0) as scheduled_count, ifnull(completed_count, 0) as completed_count from (select * from (SELECT `v`.`id`, `v`.`name`, count(lm.id) as scheduled_count, sum(case when lm.test_drive_type='Walk in' or test_drive_completed=1 then 1 else 0 end) as completed_count 
		FROM `leads_messages` `lm`, `vehicle_specific_names` `vs`, `vehicles` `v`, `leads` `l` WHERE `lm`.`message_id` = 5 and `lm`.`vehicle_specific_names_id` != '' and FIND_IN_SET (vs.id,lm.vehicle_specific_names_id) >0 and `v`.`id` = `vs`.`vehicle_id` and `l`.`id` = `lm`.`lead_id` AND `lm`.`created_by` = '".$filterData['user_id']."' AND `l`.`duplicate_of` = '0' ";

		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(l.assigned_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(l.assigned_at) <= '".$filterData['date_to']."'";

		$manual_query .= " GROUP BY `v`.`name`) user_counts right outer join 
		(select * from
		(SELECT `v`.`id` as id1, `v`.`name` as name1, count(v.name) as requests_count  FROM `vehicles` `v`, `leads` `l` WHERE FIND_IN_SET (v.id,l.vehicle) > 0 and `l`.`assign_to` = '".$filterData['user_id']."' AND `l`.`duplicate_of` = '0' ";
		
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(l.assigned_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(l.assigned_at) <= '".$filterData['date_to']."'";
		
		
		$manual_query .= " group by v.name) t1) total_requests
		on user_counts.id = total_requests.id1) combined_table";*/
		
		
		$manual_query = "select id1 as id, name1 as name, ifnull(requests_count, 0) as requests_count, ifnull(scheduled_count, 0) as scheduled_count, ifnull(completed_count, 0) as completed_count, ifnull(purchased_count, 0) as purchased_count from 

		(
			
		select * from (SELECT `v`.`id` as id1, `v`.`name` as name1, count(v.name) as requests_count FROM `vehicles` `v`, `leads` `l` WHERE FIND_IN_SET (v.id,(CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')))) > 0 and `l`.`assign_to` in (".$filterData['depUsers'].") AND `l`.`duplicate_of` = '0'";


		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(l.assigned_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(l.assigned_at) <= '".$filterData['date_to']."'";

		$manual_query .= " group by v.name) total_requests 
			
		left join 
			
		(select * from (SELECT `v`.`id`, `v`.`name`, count(lm.id) as scheduled_count, sum(case when lm.test_drive_type='Walk in' or test_drive_completed=1 then 1 else 0 end) as completed_count FROM `leads_messages` `lm`, `vehicle_specific_names` `vs`, `vehicles` `v`, `leads` `l` WHERE `lm`.`message_id` = 5 and `lm`.`vehicle_specific_names_id` != '' and FIND_IN_SET (vs.id,SUBSTRING_INDEX(lm.vehicle_specific_names_id, ',', 1)) >0 and `v`.`id` = `vs`.`vehicle_id` and `l`.`id` = `lm`.`lead_id` AND `lm`.`created_by` in (".$filterData['depUsers'].") AND `l`.`duplicate_of` = '0'";

		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";

		$manual_query .= " GROUP BY `v`.`name`) t1) user_counts     
		 
		on total_requests.id1 = user_counts.id
			
		left join 
			
		(select * from (SELECT `v`.`id` as id2, `v`.`name` as name2, count(l.id) as purchased_count FROM `leads_messages` `lm`, `vehicle_specific_names` `vs`, `vehicles` `v`, `leads` `l` WHERE `lm`.`message_id` = 4 and `lm`.`comments` like \"%Purchased a car%\" AND `v`.`id` = `vs`.`vehicle_id` and `vs`.`id` = `l`.`purchased_vehicle_id` and `l`.`id` = `lm`.`lead_id` and `l`.`tag_id` = 2 AND `lm`.`created_by` in (".$filterData['depUsers'].") AND `l`.`duplicate_of` = '0'";
		
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";

		$manual_query .= " GROUP BY `v`.`name`) t2) user_counts2    
		 
		on total_requests.id1 = user_counts2.id2 
		
		) combined_table";
		
		
		//$query=$this->db->get();
		//echo $manual_query; exit();
		
		$query = $this->db->query($manual_query);
		
		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $query->result();		
		}
		
		return null;
	}
	
	private function getGIdOfSpecific($specific_id)
	{
		$this->db->select("v.id");
		$this->db->from("vehicle_specific_names vsn, vehicles v");
		$this->db->where("vsn.vehicle_id = v.id and vsn.id='".$specific_id."'");
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row->id;
		}
		else
		{
			return "";
		}

	}
	
	public function getGCarType($filterData)
	{
		$cars = array();	
		$branches = array();

		$this->db->select("l.branch_id, CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')) as vehicle");	
		$this->db->from("leads l");
		$this->db->where("(l.vehicle!='' or l.vehicle_specific_names_id!='')");	
		////$this->db->where('(l.category_id=1 or l.category_id=34)');	
		$this->db->where('l.duplicate_of', '0');
		
		$this->db->where("l.assign_to in(".$filterData['depUsers'].")");

		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$this->db->where('date(l.assigned_at) >=', $filterData['date_from']);
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$this->db->where('date(l.assigned_at) <=', $filterData['date_to']);

		//$this->db->where("l.created_at >=",'2016-09-01');
		//$this->db->where("l.created_at <=",'2016-09-30');
		
		$query=$this->db->get();

		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			foreach($result as $row)
			{
				$bId = $row->branch_id;
				$car = $row->vehicle;			
				
				//check if same lead has same cars twice, then its a mistake.
				$carsArr = explode(",",$car);
				$carsArr = array_unique($carsArr);

				for($ctgc=0; $ctgc<count($carsArr); $ctgc++)
				{
					if(isset($carsArr[$ctgc]))
					{
						$gId = $carsArr[$ctgc];
						if(!in_array($gId,$cars))
						{
							$cars[] = $gId;		
							$branches[$gId] = $bId;
						}
						else
						{
							$branches[$gId] = $branches[$gId].",".$bId;
						}
					}
				}
				
			}
		}
		return array($cars,$branches);
	}
	
	
	public function get_car_sales_consultant_hierarchical($filterData)  
	{
		
		
		$manual_query = "select id1 as id, name1 as name, full_name as sc, ifnull(requests_count, 0) as requests_count from 

		(
			
		select * from (SELECT  u.full_name, `v`.`id` as id1, `v`.`name` as name1, count(v.name) as requests_count FROM `vehicles` `v`, `leads` `l`, users u WHERE u.id=l.assign_to and FIND_IN_SET (v.id,(CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')))) > 0 and `l`.`assign_to` in (".$filterData['depUsers'].") AND `l`.`duplicate_of` = '0'";


		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(l.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(l.created_at) <= '".$filterData['date_to']."'";

		$manual_query .= " group by u.id, v.name) total_requests 
					
		) combined_table order by id";
		
		
		//$query=$this->db->get();
		//echo $manual_query; exit();
		
		$query = $this->db->query($manual_query);
		
		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $query->result();		
		}
		
		return null;
	}
	
	public function get_car_sales_consultant_hierarchical2($filterData)  
	{
		
		
		$manual_query = "select id1 as id, name1 as name, full_name as sc, ifnull(completed_count, 0) as completed_count from 

		(
			
		select * from (SELECT u.full_name, `v`.`id` as id1, `v`.`name` as name1, sum(case when lm.test_drive_type='Walk in' or test_drive_completed=1 then 1 else 0 end) as completed_count FROM `leads_messages` `lm`, `vehicle_specific_names` `vs`, `vehicles` `v`, `leads` `l`, users u WHERE u.id=lm.created_by and `lm`.`message_id` = 5 and `lm`.`vehicle_specific_names_id` != '' and FIND_IN_SET (vs.id,SUBSTRING_INDEX(lm.vehicle_specific_names_id, ',', 1)) >0 and `v`.`id` = `vs`.`vehicle_id` and `l`.`id` = `lm`.`lead_id` AND `lm`.`created_by` in (".$filterData['depUsers'].") AND `l`.`duplicate_of` = '0'";


		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";

		$manual_query .= " group by u.id, v.name having completed_count>0) td_completed 
					
		) combined_table order by id";
		
		
		//$query=$this->db->get();
		//echo $manual_query; exit();
		
		$query = $this->db->query($manual_query);
		
		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $query->result();		
		}
		
		return null;
	}
	
	public function get_car_sales_consultant_hierarchical3($filterData)  
	{
		
		
		$manual_query = "select id1 as id, uid, name1 as name, full_name as sc, src, ifnull(requests_count, 0) as requests_count from 

		(
			
		select * from (SELECT u.id as uid, u.full_name, `v`.`id` as id1, `v`.`name` as name1, CONCAT(form_type,'',IF(manual_source = '0', '', (select s.title from sources s where s.id=l.manual_source))) as src, count(v.name) as requests_count FROM `vehicles` `v`, `leads` `l`, users u WHERE u.id=l.assign_to and FIND_IN_SET (v.id,(CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')))) > 0 and `l`.`assign_to` in (".$filterData['depUsers'].") AND `l`.`duplicate_of` = '0'";


		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(l.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(l.created_at) <= '".$filterData['date_to']."'";

		$manual_query .= " group by u.id, v.name, l.form_type, manual_source) req 
					
		) combined_table order by uid";
		
		
		//$query=$this->db->get();
		//echo $manual_query; exit();
		
		$query = $this->db->query($manual_query);
		
		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $query->result();		
		}
		
		return null;
	}	
	
	public function get_car_sales_consultant_hierarchical4($filterData)  
	{
		
		
		$manual_query = "select name,src, ifnull(completed_count, 0) as completed_count from 

		(
			
		select * from (SELECT `v`.`id`, `v`.`name`, CONCAT(form_type,'',IF(manual_source = '0', '', (select s.title from sources s where s.id=l.manual_source))) as src, sum(case when lm.test_drive_type='Walk in' or test_drive_completed=1 then 1 else 0 end) as completed_count FROM `leads_messages` `lm`, `vehicle_specific_names` `vs`, `vehicles` `v`, `leads` `l` WHERE `lm`.`message_id` = 5 and `lm`.`vehicle_specific_names_id` != '' and FIND_IN_SET (vs.id,SUBSTRING_INDEX(lm.vehicle_specific_names_id, ',', 1)) >0 and `v`.`id` = `vs`.`vehicle_id` and `l`.`id` = `lm`.`lead_id` AND `lm`.`created_by` in (".$filterData['depUsers'].") AND `l`.`duplicate_of` = '0' ";


		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";

		$manual_query .= " group by src, v.name having completed_count>0) req 
					
		) combined_table order by src";
		
		
		//$query=$this->db->get();
		//echo $manual_query; exit();
		
		$query = $this->db->query($manual_query);
		
		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $query->result();		
		}
		
		return null;
	}	
	
	public function get_car_sales_consultant_hierarchical5($filterData)  
	{
		
		
		$manual_query = "select name1 as name, src, ifnull(requests_count, 0) as requests_count from 

		(
			
		select * from (SELECT `v`.`id` as id1, `v`.`name` as name1, CONCAT(form_type,'',IF(manual_source = '0', '', (select s.title from sources s where s.id=l.manual_source))) as src, count(v.name) as requests_count FROM `vehicles` `v`, `leads` `l` WHERE FIND_IN_SET (v.id,(CONCAT(l.vehicle,if(l.vehicle_specific_names_id!='',(select group_concat(v2.id) from vehicles v2, vehicle_specific_names vsn2 where v2.id=vsn2.vehicle_id and FIND_IN_SET(vsn2.id, l.vehicle_specific_names_id)),'')))) > 0 and `l`.`assign_to` in (".$filterData['depUsers'].") AND `l`.`duplicate_of` = '0' ";


		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(l.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(l.created_at) <= '".$filterData['date_to']."'";

		$manual_query .= " group by src, v.name) req 
					
		) combined_table order by src";
		
		
		//$query=$this->db->get();
		//echo $manual_query; exit();
		
		$query = $this->db->query($manual_query);
		
		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $query->result();		
		}
		
		return null;
	}	
	
	public function get_car_sales_consultant_hierarchical6($filterData)  
	{
		
		
		$manual_query = "select uid, full_name, name2 as name, ifnull(purchased_count, 0) as purchased_count from 

		(
			
		select * from (SELECT u.id as uid, u.full_name,  `v`.`id` as id2, `v`.`name` as name2, count(l.id) as purchased_count FROM `leads_messages` `lm`, `vehicle_specific_names` `vs`, `vehicles` `v`, `leads` `l`, users u WHERE u.id=lm.created_by and `lm`.`message_id` = 4 and `lm`.`comments` like \"%Purchased a car%\" AND `v`.`id` = `vs`.`vehicle_id` and `vs`.`id` = `l`.`purchased_vehicle_id` and `l`.`id` = `lm`.`lead_id` and `l`.`tag_id` = 2 AND `lm`.`created_by` in (".$filterData['depUsers'].") AND `l`.`duplicate_of` = '0' ";


		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";

		$manual_query .= " group by u.id, `v`.`name`) req 
					
		) combined_table order by uid";
		
		
		//$query=$this->db->get();
		//echo $manual_query; exit();
		
		$query = $this->db->query($manual_query);
		
		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $query->result();		
		}
		
		return null;
	}	
	
	public function get_car_sales_consultant_hierarchical7($filterData)  
	{
		
		
		$manual_query = "select id2 as id, full_name, name2 as name, ifnull(purchased_count, 0) as purchased_count from 

		(
			
		select * from (SELECT u.id as uid, u.full_name,  `v`.`id` as id2, `v`.`name` as name2, count(l.id) as purchased_count FROM `leads_messages` `lm`, `vehicle_specific_names` `vs`, `vehicles` `v`, `leads` `l`, users u WHERE u.id=lm.created_by and `lm`.`message_id` = 4 and `lm`.`comments` like \"%Purchased a car%\" AND `v`.`id` = `vs`.`vehicle_id` and `vs`.`id` = `l`.`purchased_vehicle_id` and `l`.`id` = `lm`.`lead_id` and `l`.`tag_id` = 2 AND `lm`.`created_by` in (".$filterData['depUsers'].") AND `l`.`duplicate_of` = '0' ";


		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";

		$manual_query .= " group by `v`.`name`, u.id) req 
					
		) combined_table order by id";
		
		
		//$query=$this->db->get();
		//echo $manual_query; exit();
		
		$query = $this->db->query($manual_query);
		
		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $query->result();		
		}
		
		return null;
	}	
	
	public function get_car_sales_consultant_hierarchical8($filterData)  
	{
		
		$manual_query = "select id1 as id, uid, name1 as name, full_name as sc, src, ifnull(purchased_count, 0) as purchased_count from 

		(
		select * from (SELECT u.id as uid, u.full_name, `v`.`id` as id1, `v`.`name` as name1, CONCAT(form_type,'',IF(manual_source = '0', '', (select s.title from sources s where s.id=l.manual_source))) as src, count(l.id) as purchased_count FROM `leads_messages` `lm`, `vehicle_specific_names` `vs`, `vehicles` `v`, `leads` `l`, users u WHERE u.id=lm.created_by and `lm`.`message_id` = 4 and `lm`.`comments` like \"%Purchased a car%\" AND `v`.`id` = `vs`.`vehicle_id` and `vs`.`id` = `l`.`purchased_vehicle_id` and `l`.`id` = `lm`.`lead_id` and `l`.`tag_id` = 2 AND `lm`.`created_by` in (".$filterData['depUsers'].") AND `l`.`duplicate_of` = '0'";
			
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";

		$manual_query .= " group by l.form_type, manual_source, u.id, v.name) req 
					
		) combined_table order by src";
		
		
		//$query=$this->db->get();
		//echo $manual_query; exit();
		
		$query = $this->db->query($manual_query);
		
		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $query->result();		
		}
		
		return null;
		
	}	
	
	public function get_car_sales_consultant_hierarchical9($filterData)  
	{
		
		$manual_query = "select id1 as id, uid, name1 as name, full_name as sc, src, ifnull(purchased_count, 0) as purchased_count from 

		(
		select * from (SELECT u.id as uid, u.full_name, `v`.`id` as id1, `v`.`name` as name1, CONCAT(form_type,'',IF(manual_source = '0', '', (select s.title from sources s where s.id=l.manual_source))) as src, count(l.id) as purchased_count FROM `leads_messages` `lm`, `vehicle_specific_names` `vs`, `vehicles` `v`, `leads` `l`, users u WHERE u.id=lm.created_by and `lm`.`message_id` = 4 and `lm`.`comments` like \"%Purchased a car%\" AND `v`.`id` = `vs`.`vehicle_id` and `vs`.`id` = `l`.`purchased_vehicle_id` and `l`.`id` = `lm`.`lead_id` and `l`.`tag_id` = 2 AND `lm`.`created_by` in (".$filterData['depUsers'].") AND `l`.`duplicate_of` = '0'";
			
		if(isset($filterData['date_from']) && $filterData['date_from']!="")	$manual_query .= " and date(lm.created_at) >= '".$filterData['date_from']."'";
		if(isset($filterData['date_to']) && $filterData['date_to']!="")	$manual_query .= " and date(lm.created_at) <= '".$filterData['date_to']."'";

		$manual_query .= " group by l.form_type, manual_source, v.name, u.id) req 
					
		) combined_table order by src";
		
		
		//$query=$this->db->get();
		//echo $manual_query; exit();
		
		$query = $this->db->query($manual_query);
		
		//echo $this->db->last_query();exit();

		if($query->num_rows() > 0)
		{
			return $query->result();		
		}
		
		return null;
		
	}
	
}