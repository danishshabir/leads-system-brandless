<?php
Class Model_branch extends Base_Model
{
	public function __construct()
	{
		parent::__construct("branches");
		
	}
	
	
	public function getBranches($data)
	{
		
		$this->db->select('*');
	 	$this->db->from('branches');
		$this->db->where($data);
		
		$query = $this->db->get();
		if(!empty($query->result())){
			return $query->result();
		}else{
			return false;
		}
	
		
	}
	
	public function getBranchesByCity($city_id)
	{
		$this->db->select('*');
		$this->db->from("branches");		
		$this->db->where('city_id = "'.$city_id.'"');
		$this->db->order_by('title');
		
		$query = $this->db->get();
		
		//echo $this->db->last_query(); exit;
        $result = $query->result_array();
		
		if (!empty($result)) {
			   return $result;
			}else{
			   return false;

		} 
	}	
	
	public function getServiceBranches()
	{
		$this->db->select('*');
		$this->db->from("branches");		
		$this->db->where('title like "%Service Center%"');
		$this->db->order_by('title');
		
		$query = $this->db->get();
		
		//echo $this->db->last_query(); exit;
        $result = $query->result();
		
		if (!empty($result)) {
			   return $result;
			}else{
			   return false;

		} 
	}
	
	
	
}