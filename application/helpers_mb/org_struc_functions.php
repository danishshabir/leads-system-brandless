<?php function struc($first_single_dep,$top_level_deps,$minWith,$users){
	
	?>
	<div class="org_strSec"> 
		<div class="org_strSecInner">
			<div class="RoleOrgSec">				
				<div class="MangOrgSec">
					<div class="bigBoxOS">
						<div class="topTxt_BtnArea">
						<h1 class="">
							<a  href="javascript:void(0);"><?php echo $first_single_dep[0]->title; ?> </a>                    							
						</h1>
							<div class="dropdown newStandDropDown edit">
							<button title="Edit Department" id="dLabel" onclick="strucManClick($(this))"><i class="fa fa-pencil" aria-hidden="true"></i></button>
							<div style="display: none;" class="dropdown-menu">
								<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
									<h5>Edit Name</h5>
									<input type="text" name="title" />
									<div class="btns">
										<input type="submit" class="btn standardEd" data-toggle="modal" value="Save">
									</div>
									<input type="hidden" name="id" value="<?php echo $first_single_dep[0]->id; ?>">												
									<input type="hidden" name="form_type" value="edit_department_name">
								</form>
							</div>
						</div>
							<div class="dropdown newStandDropDown">                                        
								<button title="Add Department" id="dLabel" onclick="strucManClick($(this))"></button>
								<div style="display: none;" class="dropdown-menu">
									<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
									<input type="hidden" name="parent_id" value="<?php echo $first_single_dep[0]->id; ?>">
									<input type="hidden" name="structure_id" value="1">
									<input type="hidden" name="form_type" value="add_department">
										<h5>Add Department</h5>
										<label>Department Name<span>*</span></label>
									   <input type="text" name="title" />
										<div class="btns">
											<input type="submit" class="btn standardEd" data-toggle="modal" value="Add">
										</div>
									</form>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="relitive2Line">

							<div class="imgBox pull-left">
								<?php if($first_single_dep[0]->manager_id) { ?>
									<img src="<?php echo base_url();?>assets/images/upperEmpImg.png" alt="icon" height="32" width="32" />
								<?php } ?>
							</div>
							<h2><?php if($first_single_dep[0]->manager_id) echo getEmployeeName($first_single_dep[0]->manager_id); ?></h2>
							<h3><?php if($first_single_dep[0]->manager_id) echo "Manager"; ?></h3>


							<div class="dropdown newStandDropDown">
								<button title="Select Managar" id="dLabel" onclick="strucManClick($(this))"></button>
								<div style="display: none;" class="dropdown-menu">
									<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
									<input type="hidden" name="id" value="<?php echo $first_single_dep[0]->id; ?>">
									<input type="hidden" name="form_type" value="select_ceo">
										<h5>Manager</h5>
										<label>Select Manager<span>*</span></label>
										   <select name="manager_id"> 
											<?php foreach($users as $user){ ?>														   
												<option value="<?php echo $user->id;?>"><?php echo $user->full_name; ?></option>							
											<?php } ?>	
										  </select>

										<div class="btns">
											<input type="submit" class="btn standardEd" data-toggle="modal" value="Add">
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						<?php $employeesByDep = getEmployeesByDep($first_single_dep[0]->id); ?>
					   <h4><?php if($employeesByDep) echo "Employees"; ?></h4>
						<ul>										
							<?php if($employeesByDep) foreach($employeesByDep as $employeeByDep) { ?>
								<li>

									<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
									<input type="hidden" name="id" value="<?php echo $employeeByDep->id; ?>">
									<input type="hidden" name="form_type" value="delete_employee">
										<i class="fa fa-times" aria-hidden="true" onClick="if(confirm('Are you sure you want to remove this employee from this department?')) {$(this).closest('form').submit();}"></i>
									</form>


									<a href="javascript:void(0)">
										<?php 
										$styleSubMng = "";
										$userData = getUserById($employeeByDep->user_id); 
										$cSubMngRights = getRights(33,$userData->role_id); //33 Lead Control (Sub Manager)
										if($cSubMngRights && $cSubMngRights->read)
										{
											//its mean this user has the read rights of the "lead control". This user is a "lead contrl sub manager"
											$styleSubMng = "background-color:#4c4cff;";
										}
										
										?>
										<div style="<?php echo $styleSubMng; ?>" class="imgBox"><img title="<?php if($styleSubMng) echo "Sub Mng: "; echo getEmployeeName($employeeByDep->user_id); ?>" src="<?php echo base_url();?>assets/images/admin.png" alt="icon" height="32" width="32" /></div>
									</a>											
								</li>
							<?php } ?>
						   
						</ul>
						<div class="employeesAddBtn">
							<div class="dropdown newStandDropDown">   
								<button title="Add Employee" id="dLabel" onclick="strucManClick($(this))"></button>
								<div class="dropdown-menu" style="display: none;">
										<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
										<input type="hidden" name="struc_dep_id" value="<?php echo $first_single_dep[0]->id; ?>">
										<input type="hidden" name="form_type" value="add_employee">
											<h5>Employee</h5>
											<label>Add Employee<span>*</span></label>
											<select name="user_id">
												<?php foreach($users as $user){ ?>														   
													<option value="<?php echo $user->id;?>"><?php echo $user->full_name; ?></option>							
												<?php } ?>	
											</select>											
											<div class="btns">												
												<input class="btn standardEd" type="submit" data-toggle="modal" value="Add">
											</div>
										</form>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- top level ceo end-->
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="row borderLineEd">
				<div class="with100ScrollX">
					<div class="min-WithScrolling" style="min-width:<?php echo $minWith.'px' ?>;">
			
			<?php if(!empty($top_level_deps))
				{ 
				 $i=0;
				 foreach($top_level_deps as $top_level_dep){
						
						?>
			
					<div class="col-md-3 <?php if(count($top_level_deps)===1) echo "theonlyone"; ?>">
						<div class="bigBoxOS">
							<div class="topTxt_BtnArea">

							<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form delIcon" onsubmit="return false;">
								<i class="fa fa-times" aria-hidden="true" onclick="if(confirm('Are you sure you want to remove this department. Note all child departments will also be removed.')) {$(this).closest('form').submit();}"></i>
								<input type="hidden" name="id" value="<?php echo $top_level_dep->id; ?>">	
								<input type="hidden" name="form_type" value="delete_department">
							</form>

								<h1 class="" title="<?php echo $top_level_dep->title; ?>">									
									<?php if(strlen($top_level_dep->title)>20) echo substr($top_level_dep->title, 0, 20).'...'; else echo $top_level_dep->title; ?>
								</h1>
								<div class="dropdown newStandDropDown edit">									

										<button title="Edit Department" id="dLabel" onclick="strucManClick($(this))"><i aria-hidden="true" class="fa fa-pencil"></i></button>
										<div class="dropdown-menu" style="display: none;">


											<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
												<h5>Edit Name</h5>
												<input type="text" name="title" />
												<div class="btns">
													<input type="submit" class="btn standardEd" data-toggle="modal" value="Save">
												</div>
												<input type="hidden" name="id" value="<?php echo $top_level_dep->id; ?>">	
												<input type="hidden" name="form_type" value="edit_department_name">
											</form>
										</div>
									</div>
								<div class="dropdown newStandDropDown">
									<button title="Add Department" id="dLabel" onclick="strucManClick($(this))"></button>



									<div class="dropdown-menu" style="display: none;">
									   <form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
										<input type="hidden" name="parent_id" value="<?php echo $top_level_dep->id; ?>"><!--it will always be 1 here-->
										<input type="hidden" name="structure_id" value="1">
										<input type="hidden" name="form_type" value="add_department">
											<h5>Add Department</h5>
											<label>Department Name<span>*</span></label>
										   <input type="text" name="title" />
											<div class="btns">
												<input type="submit" class="btn standardEd" data-toggle="modal" value="Add">
											</div>
										</form>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="relitive2Line">
								<div class="imgBox">
									<?php if($top_level_dep->manager_id) { ?>
										<img src="<?php echo base_url();?>assets/images/upperEmpImg.png" alt="icon" height="32" width="32" />
									<?php } ?>
								</div>
								<h2><?php if($top_level_dep->manager_id) echo getEmployeeName($top_level_dep->manager_id); ?></h2>
								<h3><?php if($top_level_dep->manager_id) echo "Manager"; ?></h3>
								<div class="dropdown newStandDropDown">
									<button title="Select Manager" id="dLabel" onclick="strucManClick($(this))"></button>
									<div class="dropdown-menu" style="display: none;">
									   <form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
										<input type="hidden" name="id" value="<?php echo $top_level_dep->id; ?>">
										<input type="hidden" name="form_type" value="select_manager">
											<h5>Manager</h5>
											<label>Select Manager<span>*</span></label>
											   <select name="manager_id"> 
												<?php foreach($users as $user){ ?>														   
													<option value="<?php echo $user->id;?>"><?php echo $user->full_name; ?></option>							
												<?php } ?>	
											  </select>
											<div class="btns">
												<input type="submit" class="btn standardEd" data-toggle="modal" value="Add">
											</div>
										</form>
									</div>
								</div>
						   </div>
							<div class="clearfix"></div>
							<?php $employeesByDep = getEmployeesByDep($top_level_dep->id); ?>
							<h4><?php if($employeesByDep) echo "Employees"; ?></h4>
							<ul>										
								<?php if($employeesByDep) foreach($employeesByDep as $employeeByDep) { ?>
									<li>
									
										<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
										<input type="hidden" name="id" value="<?php echo $employeeByDep->id; ?>">
										<input type="hidden" name="form_type" value="delete_employee">
											<i class="fa fa-times" aria-hidden="true" onClick="if(confirm('Are you sure you want to remove this user from this department?')) {$(this).closest('form').submit();}"></i>
										</form>

										<a href="javascript:void(0)">
											<?php 
											$styleSubMng = "";
											$userData = getUserById($employeeByDep->user_id); 
											if($userData){
												$cSubMngRights = getRights(33,$userData->role_id); //33 Lead Control (Sub Manager)
												if($cSubMngRights && $cSubMngRights->read)
												{
													//its mean this user has the read rights of the "lead control". This user is a "lead contrl sub manager"
													$styleSubMng = "background-color:#0399d4;";
												}
											}
											
											?>
											<div style="<?php echo $styleSubMng; ?>" class="imgBox"><img title="<?php if($styleSubMng) echo "Sub Mng: "; echo getEmployeeName($employeeByDep->user_id); ?>" src="<?php echo base_url();?>assets/images/admin.png" alt="icon" height="32" width="32" /></div>
										</a>
									</li>
								<?php } ?>
							   
							</ul>
							<div class="employeesAddBtn">
								<div class="dropdown newStandDropDown">                                            
									 <button title="Add Employee" id="dLabel" onclick="strucManClick($(this))"></button>
								<div class="dropdown-menu">
									<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
									<input type="hidden" name="struc_dep_id" value="<?php echo $top_level_dep->id; ?>">
									<input type="hidden" name="form_type" value="add_employee">
										<h5>Employee</h5>
										<label>Add Employee<span>*</span></label>
										<select name="user_id">
											<?php foreach($users as $user){ ?>														   
												<option value="<?php echo $user->id;?>"><?php echo $user->full_name; ?></option>							
											<?php } ?>	
										</select>										
										<div class="btns">											
											<input class="btn standardEd" type="submit" data-toggle="modal" value="Add">
										</div>
									</form>
								</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<!--top level dep end-->
						
						
						
						<?php 
						$subDepartments = getSubDepartments($top_level_dep->id); 
						if($subDepartments) 
						foreach($subDepartments as $subDepartment){ ?>
						
						<div class="smallBoxOS"> 
							<div class="topTxt_BtnArea">

								<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form delIcon" onsubmit="return false;">
									<i class="fa fa-times" aria-hidden="true" onclick="if(confirm('Are you sure you want to remove this department. Note all child departments will also be removed.')) {$(this).closest('form').submit();}"></i>
									<input type="hidden" name="id" value="<?php echo $subDepartment->id; ?>">	
									<input type="hidden" name="form_type" value="delete_department">
								</form>

								<h1 class="" title="<?php echo $subDepartment->title; ?>">								
									<?php if(strlen($subDepartment->title)>16) echo substr($subDepartment->title, 0, 16).'...'; else echo $subDepartment->title; ?>
								</h1>
								<div class="dropdown newStandDropDown edit">
										<button title="Edit Department" id="dLabel" onclick="strucManClick($(this))"><i aria-hidden="true" class="fa fa-pencil"></i></button>
										<div class="dropdown-menu" style="display: none;">
											<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
												<h5>Edit Name</h5>
												<input type="text" name="title" />
												<div class="btns">
													<input type="submit" class="btn standardEd" data-toggle="modal" value="Save">
												</div>
												<input type="hidden" name="id" value="<?php echo $subDepartment->id; ?>">												
												<input type="hidden" name="form_type" value="edit_department_name">
											</form>
										</div>
									</div>
								<div class="dropdown newStandDropDown">                                            
									<button title="Add Department" id="dLabel" onclick="strucManClick($(this))"></button>
									<div class="dropdown-menu" style="display: none;">
										<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
										<input type="hidden" name="parent_id" value="<?php echo $subDepartment->id; ?>"><!--it will always be 1 here-->
										<input type="hidden" name="structure_id" value="1">
										<input type="hidden" name="form_type" value="add_department">
											<h5>Add Department</h5>
											<label>Department Name<span>*</span></label>
										   <input type="text" name="title" />
											<div class="btns">
												<input type="submit" class="btn standardEd" data-toggle="modal" value="Add">
											</div>
										</form>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="relitive2Line">
								<div class="imgBox">
									<?php if($subDepartment->manager_id) { ?>
										<img src="<?php echo base_url();?>assets/images/upperEmpImg.png" alt="icon" height="32" width="32" />
									<?php } ?>
								</div>
								<h2><?php if($subDepartment->manager_id) echo getEmployeeName($subDepartment->manager_id); ?></h2>
								<h3><?php if($subDepartment->manager_id) echo "Manager"; ?></h3>
								<div class="dropdown newStandDropDown one">
									<button title="Select Manager" id="dLabel" onclick="strucManClick($(this))"></button>
									<div class="dropdown-menu" style="display: none;">
									   <form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
										<input type="hidden" name="id" value="<?php echo $subDepartment->id; ?>">
										<input type="hidden" name="form_type" value="select_manager">
											<h5>Manager</h5>
											<label>Select Manager<span>*</span></label>
											   <select name="manager_id"> 
												<?php foreach($users as $user){ ?>														   
													<option value="<?php echo $user->id;?>"><?php echo $user->full_name; ?></option>							
												<?php } ?>	
											  </select>
											<div class="btns">
												<input type="submit" class="btn standardEd" data-toggle="modal" value="Add">
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<?php $employeesByDep = getEmployeesByDep($subDepartment->id); ?>
							<h4><?php if($employeesByDep) echo "Employees"; ?></h4>
							<ul>										
								<?php if($employeesByDep) foreach($employeesByDep as $employeeByDep) { ?>
									<li>

										<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
										<input type="hidden" name="id" value="<?php echo $employeeByDep->id; ?>">
										<input type="hidden" name="form_type" value="delete_employee">
											<i class="fa fa-times" aria-hidden="true" onClick="if(confirm('Are you sure you want to remove this user from this department?')) {$(this).closest('form').submit();}"></i>
										</form>

										<a href="javascript:void(0)">
											<?php 
											$styleSubMng = "";
											$userData = getUserById($employeeByDep->user_id); 
											$cSubMngRights = getRights(33,$userData->role_id); //33 Lead Control (Sub Manager)
											if($cSubMngRights && $cSubMngRights->read)
											{
												//its mean this user has the read rights of the "lead control". This user is a "lead contrl sub manager"
												$styleSubMng = "background-color:#0399d4;";
											}
											
											?>
											<div style="<?php echo $styleSubMng; ?>" class="imgBox"><img title="<?php if($styleSubMng) echo "Sub Mng: "; echo getEmployeeName($employeeByDep->user_id); ?>" src="<?php echo base_url();?>assets/images/admin.png" alt="icon" height="32" width="32" /></div>
										</a>
									</li>
								<?php } ?>
							</ul>
							<div class="employeesAddBtn">
								<div class="dropdown newStandDropDown">                                            
									 <button title="Add Employee" id="dLabel" onclick="strucManClick($(this))"></button>
										<div class="dropdown-menu">
									<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
									<input type="hidden" name="struc_dep_id" value="<?php echo $subDepartment->id; ?>">
									<input type="hidden" name="form_type" value="add_employee">
										<h5>Employee</h5>
										<label>Add Employee<span>*</span></label>
										<select name="user_id">
											<?php foreach($users as $user){ ?>														   
												<option value="<?php echo $user->id;?>"><?php echo $user->full_name; ?></option>							
											<?php } ?>	
										</select>										
										<div class="btns">											
											<input class="btn standardEd" type="submit" data-toggle="modal" value="Add">
										</div>
									</form>
								</div>
								</div>
							</div>
							<div class="clearfix"></div>
							



							<?php $childExists = getSubDepartments($subDepartment->id);  
							if(count($childExists))
							{
							?>
								<div class="loadMoreSec">
									<a onclick="loadMoreStrucAjax('<?php echo $subDepartment->id;?>');">Load More</a>
									<a style="display:none;" id="clickLoadMore<?php echo $subDepartment->id;?>" data-target="#bs-multi-more-nested<?php echo $subDepartment->id; ?>" data-toggle="modal"></a>
								</div>  

							<?php 
							}
							?>

							
						</div>

						<?php } ?>


						
					</div><!-- Column 1 -->
											   
				<?php $i++;  } } ?>
					</div>
			   </div>
			</div>
		</div>
	</div>
<?php
	
}
?>