﻿<?php

if (!defined('BASEPATH'))
 exit('No direct script access allowed');



	function getAdmin($id,$assign='') 
	{
	
		if($id == 0 && $assign == ''){
			return 'Website';
		}if($id == 0 && $assign == 1)
		{
			return '';
		}
		else
		{
			
			 $CI = & get_Instance();
			 $CI->load->model('Model_user');
			 $result = $CI->Model_user->get($id);
			 return $result->full_name;
				
		}
	}
   function checkAdminSession()
   {
	   $CI = & get_Instance();
	   if($CI->session->userdata('user'))
	   {
		   return true;
		   
	   }else
	   {
		   redirect($CI->config->item('base_url') . 'admin');
	   }
   }
   
   function getRights($section_id,$role_id)
   {
	    $data = array();
		$CI = & get_Instance();
	    $CI->load->model('Model_right');
		$data['section_id'] = $section_id;
		$data['role_id'] = $role_id;
	    $result = $CI->Model_right->getWithMultipleFields($data);
		return $result;
   }
   
   function getVehicles($vehicle_ids)
   {
	    if($vehicle_ids == '')
		{
			return '';
		}
		$data = array();
		$CI = & get_Instance();
	    $CI->load->model('Model_vehicle');
		$vehicle_ids = explode(',',$vehicle_ids);
		foreach($vehicle_ids as $id)
		{
			$result = $CI->Model_vehicle->get($id);
			$data[] = $result->name;
		}
		
		return implode(',', $data);
   }
   
   function getSubLeads($email)
   {
	    $data = array();
		$CI = & get_Instance();
	    $CI->load->model('Model_lead');
		$result = $CI->Model_lead->getSubLeads($email);
		return $result;
		
		//return implode(',', $data);
   }

   function menu()
   {
	   return ' <li><a href="'.base_url().'lead/leadAction"><i class="sprite sprite-menagment"></i> Create a new lead</a></li>
                        <li><a href="'.base_url().'lead"><i class="fa fa-exchange"></i>  Create a Action</a></li>
                        <li><a href="'.base_url().'user/userAction"><i class="fa fa-user"></i> Create a New User</a></li>
                        <li><a href="'.base_url().'organization_structure"><i class="fa fa-sitemap"></i> Create a Structure</a></li>
                        <li><a href="'.base_url().'survey"><i class="fa fa-file-text"></i> Create a Survey</a></li>
                        <li><a href="'.base_url().'role"><i class="fa fa-users"></i> Create a Role</a></li>';
		
		
   }
   
   function rights($section_id,$action)
   {
	  $CI = & get_Instance();
	  $CI->load->model('Model_right');
	  $data = array();
	  $data['section_id'] = $section_id;
	  $data['role_id'] = $CI->session->userdata['user']['role_id'];
	  $result = $CI->Model_right->getWithMultipleFields($data);
	  switch($action)
	  {
		  case 'read':
		  $result->read;
		  break;
		  case 'write':
		  $result->write;
		  break;
		  case 'update':
		  $result->update;
		  break;
		  case 'delete':
		  $result->delete;
		  break;
		  
	  }
	  
		
		
   }