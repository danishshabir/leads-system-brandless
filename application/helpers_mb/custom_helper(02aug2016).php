<?php
if (!defined('BASEPATH'))
 exit('No direct script access allowed');

include 'leads_functions.php';
include 'org_struc_functions.php';

//for single survey results and also overall survey results stats. note: its regardless of categories.
function surveyResult($survey_id, $lead_id)
{
	   $data = array();
	   $CI = & get_Instance(); 
	   $CI->load->model('Model_survey_result');
	    $CI->load->model('Model_survey');

		$avg = $CI->Model_survey_result->getTotalAvg($survey_id);
        $data['total_avg'] = $avg[0]->average; // this total avg to this only of survey
        if(!empty($avg))
        {
           $data['total_avg'] = round($data['total_avg'], 1);  
        }
        $avg_by_lead = $CI->Model_survey_result->getTotalAvgLead($survey_id,$lead_id);
        $data['total_avg_by_lead'] = $avg_by_lead[0]->average; // this total avg to this only of survey

        if(!empty($avg_by_lead))
        {
           $data['total_avg_by_lead'] = round($data['total_avg_by_lead'], 1);  
        }

		$data['surveyRec'] = $CI->Model_survey->get($survey_id);

		return $data;
}

function currentActionsCountOfUser($user_id)
{

   $data = array();
   $CI = & get_Instance(); 
   $CI->load->model('Model_leads_messages');

   
  // $user_id = $CI->session->userdata['user']['id'];

   $data['created_by'] = $user_id;
   $results = $CI->Model_leads_messages->getMultipleRows($data);
   if($results)
   {
	   return count($results);
   }else
   {
	   return '0';
   }	   

   

}
function getSpecificCars($vehicle_id)
{

   $data = array();
   $cars = array();
   $CI = & get_Instance(); 
   $CI->load->model('Model_vehicle_specific_name');

   
  // $user_id = $CI->session->userdata['user']['id'];

   $data['vehicle_id'] = $vehicle_id;
   $results = $CI->Model_vehicle_specific_name->getMultipleRows($data);
   if($results)
   {
	   foreach($results as $result){
		 $cars[]  = $result->id;  
	   }
	   return $car = implode(',',$cars);
   }else
   {
	   return false;
   }	   

   

}
function getSpecificVehicleNames($vehicle_id)
{

   $data = array();
   $cars = array();
   $CI = & get_Instance(); 
   $CI->load->model('Model_vehicle_specific_name');

   
  // $user_id = $CI->session->userdata['user']['id'];

   $data['vehicle_id'] = $vehicle_id;
   $results = $CI->Model_vehicle_specific_name->getMultipleRows($data);
   if($results)
   {
	   return $results;
   }else
   {
	   return false;
   }	   

   

}

function getSpecificCarName($vehicle_id)
{

   $data = array();
   $CI = & get_Instance(); 
   $CI->load->model('Model_vehicle_specific_name');

   
  // $user_id = $CI->session->userdata['user']['id'];

   
   $result = $CI->Model_vehicle_specific_name->get($vehicle_id);
   if($result)
   {
	  
	   return $result->name;
   }else
   {
	   return false;
   }	   

   

}
function getGeneralCarName($vehicle_id)
{

   $data = array();
   $CI = & get_Instance(); 
   $CI->load->model('Model_vehicle');

   
  // $user_id = $CI->session->userdata['user']['id'];

   
   $result = $CI->Model_vehicle->get($vehicle_id);
   if($result)
   {
	  
	   return $result->name;
   }else
   {
	   return false;
   }	   

   

}

/*
function getUsersByCategory($category_id)
{

   $data = array();
   $CI = & get_Instance(); 
   $CI->load->model('Model_user');

   $dataUsers['depart_id'] = $category_id;
   $results = $data['users'] = $CI->Model_user->getMultipleRows($dataUsers,false,'asc');		


  
   return $results;

}*/

function getUserById($id)
{

   $data = array();
   $CI = & get_Instance(); 
   $CI->load->model('Model_user');

   $results = $data['users'] = $CI->Model_user->get($id);		
  
   return $results;
}



function currentCountOfAssignedLeadsOfUser($user_id)
{

   $data = array();
   $CI = & get_Instance(); 
   $CI->load->model('Model_lead');

  // $user_id = $CI->session->userdata['user']['id'];

   $data['assign_to'] = $user_id;

   $results = $CI->Model_lead->getMultipleRows($data);

  if($results)
   {
	   return count($results);
   }else
   {
	   return '0';
   }

}

function currentCompleteLeadsOfUser($user_id)
{

   $data = array();
   $CI = & get_Instance(); 
   $CI->load->model('Model_lead');

  // $user_id = $CI->session->userdata['user']['id'];

   $data['assign_to'] = $user_id;
   $data['status'] = 'Closed';

   $results = $CI->Model_lead->getMultipleRows($data);

   if($results)
   {
	   return count($results);
   }else
   {
	   return '0';
   }

}


function getCityName($id)
{

	if($id)
	{

	   $data = array();
	   $CI = & get_Instance();
	   $CI->load->model('Model_city');
	   $results = $CI->Model_city->get($id);

	   return $results->title;
	}
	else
		return 'N/A';
	
}

function getCityIdByName($name)
{

	   $data = array();
	   $CI = & get_Instance();
	   $CI->load->model('Model_city');
	   $data['title'] = $name;
	   $results = $CI->Model_city->getWithMultipleFields($data);
	   if(!empty($results))
		return $results->id;
	   else
		 return '0';

}

function getCityIdByNameAr($name)
{

	   $data = array();
	   $CI = & get_Instance();
	   $CI->load->model('Model_city');
	   $data['title_ar'] = $name;
	   $results = $CI->Model_city->getWithMultipleFields($data);
	   if(!empty($results))
		return $results->id;
	   else
		 return '0';

}

function getBranchName($id)
{

	if($id)
	{

	   $data = array();
	   $CI = & get_Instance();
	   $CI->load->model('Model_branch');
	   $results = $CI->Model_branch->get($id);

	   return $results->title;
	}
	else
		return 'N/A';
}

function getBranchNameAr($id)
{

	if($id)
	{

	   $data = array();
	   $CI = & get_Instance();
	   $CI->load->model('Model_branch');
	   $results = $CI->Model_branch->get($id);

	   return $results->branch_title_ar;
	}
	else
		return 'N/A';
}

function getBranchIdByName($cityId, $branchName)
{

	   $data = array();
	   $CI = & get_Instance();
	   $CI->load->model('Model_branch');
	   $data['title'] = $branchName;
	   $data['city_id'] = $cityId;
	   $results = $CI->Model_branch->getWithMultipleFields($data);

	   if(!empty($results))
		return $results->id;
	   else
		 return '0';
}

function getBranchIdByNameAr($cityId, $branchName)
{

	   $data = array();
	   $CI = & get_Instance();
	   $CI->load->model('Model_branch');
	   $data['title_ar'] = $branchName;
	   $data['city_id'] = $cityId;
	   $results = $CI->Model_branch->getWithMultipleFields($data);

	   if(!empty($results))
		return $results->id;
	   else
		 return '0';
}

function getBranchIdByNameCron($cityId, $branchName)
{

	   $data = array();
	   $CI = & get_Instance();
	   $CI->load->model('Model_branch');
	   $data['title_web'] = $branchName;
	   $data['city_id'] = $cityId;
	   $results = $CI->Model_branch->getWithMultipleFields($data);

	   if(!empty($results))
		return $results->id;
	   else
		 return '0';
}

function getQuestionById($id)
{

   $data = array();
   $CI = & get_Instance();
   $CI->load->model('Model_question');
   $results = $CI->Model_question->get($id);
   if(!empty($results)){
    return $results;
   }else{
    return false;
   }
   
}

function getTeamLead($branch_id,$category_id)
 {
   $data = array();
   $CI = & get_Instance();
   $CI->load->model('Model_user');
   $data['branch_id'] = $branch_id; 
   //$data['depart_id'] = $category_id; //user is no more connected to category.
   $data['is_team_lead'] = 1;
      $results = $CI->Model_user->getWithMultipleFields($data);

   
   return $results;

 }

	function getLeadsWithNotifications($check = '')
	{
		$data = array();
		$CI = & get_Instance();
		$CI->load->model('Model_lead');

		$results = $CI->Model_lead->getLeadsWithNotifications($check);

		return $results;
	}

	function checkLeadHasUnreadNotifications($lead_id)
	{

	  $data = array();
	  $CI = & get_Instance();
	  $CI->load->model('Model_lead');

	  $results = $CI->Model_lead->checkLeadHasUnreadNotifications($lead_id);

	  if($results)
		  return true;
	  else
		  return false;

	}

	function getAllCategories()
	{
	  $data = array();
	  $CI = & get_Instance();
	  $CI->load->model('Model_category');
	  	  
	  //$results = $CI->Model_category->getAll();
	  $results = $CI->Model_category->getAllOrderBy(false,"title","asc");

	  
	  return $results;

	}
	
	/*function getBranchManager($branch_id)
	{
	  $data = array();
	  $CI = & get_Instance();
	  $CI->load->model('Model_user');
	  $data['branch_id'] = $branch_id;
	  $data['is_branch_manager'] = 1;
		 $results = $CI->Model_user->getWithMultipleFields($data);

	  
	  return $results;

	}*/
    
    /* users are no more linked with category
	function getManagersByCategories($category_id)
	{
	  $data = array();
	  $CI = & get_Instance();
	  $CI->load->model('Model_user');
	  $data['depart_id'] = $category_id; // depart_id is category id
	  $data['is_branch_manager'] = 1;
      $results = $CI->Model_user->getMultipleRows($data);

	  
	  return $results;

	}*/
    
    /* users are no more linked with category
	function getTeamLeadsByCategory($category_id)
	{
	  $data = array();
	  $CI = & get_Instance();
	  $CI->load->model('Model_user');
	  $data['depart_id'] = $category_id; // depart_id is category id
	  $data['is_team_lead'] = 1;
      $results = $CI->Model_user->getMultipleRows($data);

	  
	  return $results;

	}*/
    
     function getMembersByCategory($category_id)
	{
	  $data = array();
	  $CI = & get_Instance();
	  $CI->load->model('Model_user');
	  $results = $CI->Model_user->getMembersByCategory($category_id);

	  
	  return $results;

	}


	function getBranchEmployees($branch_id)
	 {
	  $data = array();
	  $CI = & get_Instance();
	  $CI->load->model('Model_user');
	  $data['branch_id'] = $branch_id;
	  $results = $CI->Model_user->getMultipleRows($data);

	  
	  return $results;

	 }

	 function getCityIdOfUserByUserId($user_id)
	 {
		$data = array();
		$CI = & get_Instance();
		$CI->load->model('Model_user');
		$results = $CI->Model_user->get($user_id);
		return $results->city_id;
	 }

	 function getBranchIdOfUserByUserId($user_id)
	 {
		$data = array();
		$CI = & get_Instance();
		$CI->load->model('Model_user');
		$results = $CI->Model_user->get($user_id);
		return $results->branch_id;
	 }

	 /* users are no more linked with category
	 function getEmployeeByCategory($category_id,$branch_id)
	 {
	  $data = array();
	  $CI = & get_Instance();
	  $CI->load->model('Model_user');
	  
	  if($category_id!=='-1')
	  $data['depart_id'] = $category_id; // department id is nothing it is category id so donn't be confused

	  $data['branch_id'] = $branch_id;
	  $results = $CI->Model_user->getMultipleRows($data);

	  
	  return $results;

	 }*/

	function getLastMessagePhoneStatus($lead_id)
	{
		$data = array();
		$CI = & get_Instance();
		$CI->load->model('Model_leads_messages');
		
		return $call_status = $CI->Model_leads_messages->getLastMessagePhoneStatus($lead_id);

	}

	function getMessageType($id)
	{
		$data = array();
		$CI = & get_Instance();
		$CI->load->model('Model_messages');
		
		return $CI->Model_messages->get($id);

	}

	function getLeadMessages($lead_id,$howManyLatest)
	{
		$data = array();
		$CI = & get_Instance();
		$CI->load->model('Model_leads_messages');
		
		$data['lead_id'] = $lead_id;
		return $results = $CI->Model_leads_messages->getMessagesByLead($data, $howManyLatest);

	}

	function getCategoryColor($category_id)
	{
		if($category_id)
		{
			$data = array();
			$CI = & get_Instance();
			$CI->load->model('Model_category');
			

			$results = $CI->Model_category->get($category_id);
			if($results)
				return $results->color;
			else 
				return '#FFF';
		}
		else
			return '#FFF';
	}

	function getCategoryName($category_id)
	{
		if($category_id)
		{
			$data = array();
			$CI = & get_Instance();
			$CI->load->model('Model_category');
			

			$results = $CI->Model_category->get($category_id);
			return $results->title;
		}
		else
		{
			return "N/A";
		}
	}

	function calculateMinutesBetweenTwoDatetimes($start, $end)
	{
		$seconds = strtotime($end) - strtotime($start);

		$days    = floor($seconds / 86400);
		$hours   = floor(($seconds - ($days * 86400)) / 3600);
		$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);

		return $totalMinutes = $days*24*60 + $hours*60 + $minutes;
		//$seconds = floor(($seconds - ($days * 86400) - ($hours * 3600) - ($minutes*60)));

	}

	//check if Saturday/Sunday b/w two dates (Actually if Saturday is coming then Sunday is must)
	function isWeekendBw($date, $end_date)
	{		

		while (strtotime($date) <= strtotime($end_date)) {
					//echo "$date\n";
					if(date('N', strtotime($date)) == 6) //if Friday is coming
						return true;

					$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
		}
		return false;
	}

	//by default deadline is for the first call to customer, then its recalculated for close kpi
	function calculateLeadDeadline($created_at, $cat_id, $callOrCloseKPI='callKPI') 
	{
		
		//current logic is basically the lead is assigned to a category and category has the time limit field in hours.		
		$data = array();
		$CI = & get_Instance();
		$CI->load->model('Model_category');

		$cat_hours = getLeadDeadline($cat_id,$callOrCloseKPI);		


		//add hours in the created at and calculate next date.
	
		$seconds = strtotime($created_at. ' +1 day'); //exclude today so add 1 day

		$days    = floor($seconds / 86400);
		$hours   = floor(($seconds - ($days * 86400)) / 3600);
		$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);
		$totalMinutesCreatedAt = $days*24*60 + $hours*60 + $minutes;
		$totalMinutesCat = $cat_hours*60;		


		$totalMinutes = $totalMinutesCreatedAt + $totalMinutesCat;
		$totalSeconds = $totalMinutes*60;

		/*if(isWeekendBw( date("Y-m-d", $seconds), date("Y-m-d", $totalSeconds)) )
		{
			$totalSeconds = $totalSeconds + (24*60*60)*2; //add two more days ahead because there is weekend b/w
		}*/

		$deadline = date("Y-m-d H:i:s", $totalSeconds);

		return $deadline;

	}

	//If more than 2 days. show days.
	//if less then 1 day then show hours like 24 hours left
	//if less then 1 hour then show minutes like 59 minutes left.
	function dueCounter($leadsDueDateTime)
	{
		$remainingDue = "";

		$currentSeconds = strtotime(date("Y-m-d H:i:s"));
		$leadsDueSeconds = strtotime($leadsDueDateTime);
		if($currentSeconds < $leadsDueSeconds)
		{
			$seconds = $leadsDueSeconds - $currentSeconds;
			$days    = floor($seconds / 86400);
			$hours   = floor(($seconds - ($days * 86400)) / 3600);
			$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);			

			if($days>0)
				$remainingDue = $days." days left";
			elseif($hours>1)
				$remainingDue = $hours." hours left";
			elseif($minutes>0)
				$remainingDue = $minutes." min left";
			else
				$remainingDue = $seconds." sec left";

			return $remainingDue;
			
		}
		else
		{
			return 'Delayed';
		}


	}

	function getLeadDeadline($category_id,$callOrCloseKPI='callKPI')
	{
		//current logic is basically the lead is assigned to a category and cateogy has the time limit field in hours.
		$data = array();
		$CI = & get_Instance();
		$CI->load->model('Model_category');
		

		$results = $CI->Model_category->get($category_id);
		if($callOrCloseKPI=='closeKPI')
			return $results->time_limit;
		else
			return $results->call_kpi;

	}

	function minutesToHours($minutes)
	{
		//example 2 Hours and 10 Minutes

		if ($minutes < 60) {
			return $minutes." Minutes";
		}

		$hours = floor($minutes / 60);
		$minutes = ($minutes % 60);
		return  $hours." Hours and ".$minutes." Minutes";

	}


	function getBranches($city_id)
	{
		$data = array();
		$CI = & get_Instance();
		$CI->load->model('Model_branch');
		
		$data['city_id'] =  $city_id;
		$results = $CI->Model_branch->getBranches($data);
		return $results;
	}

	function branchesByCity($is_ajax, $city_id,$branch_id=0)
	{		
		//branch_id > 0 mean in edit more to have the selected value in <select options
		//if ajax then echo and exit, otherwise return.

		$options = "";

		$branches = getBranches($city_id);
		
		echo '<option value= "">Please Select</option>';

		if(is_array($branches))
		{
			foreach($branches as $branch){
				
				$selected = "";
				if($branch_id){ if($branch_id==$branch->id) $selected = 'selected=selected'; } 
				$options .= '<option '.$selected.' value="'.$branch->id.'">'.$branch->title.'</option>';
			}
		}

		if($is_ajax)
		{
			echo $options;
			exit();

		}
		else
		{
			return $options;
		}

		
	}

	function generatTrackId($category_id)
	{
		
		$CI = & get_Instance();
		$CI->load->model('Model_category');
		$CI->load->model('Model_lead');

		$lead_code = "";
		
		if($category_id)
		{
			$result = $CI->Model_category->get($category_id);
			
			//$department_tag = $result->department_tag;
			//$lead_type_tag = $result->lead_type_tag;
			$lead_code = $result->lead_code;
		}


		$latestPkId = $CI->Model_lead->getLastLeadId();

		//NTD0001
		//Departments Tags:N:New Cars U:Used Cards (Pre-Owned) + + Lead Type Tags: TD:Test Drive SE:Sales Enquiry
		//See db category table. Lead trackid is generated by using category

		return $lead_code."".str_pad((((int)$latestPkId)+1), 4, '0', STR_PAD_LEFT);		

	}

	//when lead us updated and category is selected or updated.
	function updateTrackId($category_id,$oldTrackId)
	{
		
		$CI = & get_Instance();
		$CI->load->model('Model_category');		
		
		$result = $CI->Model_category->get($category_id);
		$lead_code = $result->lead_code;
		
		$oldTrackIdNumOnly = preg_replace( '/[^0-9]/', '', $oldTrackId); //track id only numbers

		return $lead_code.$oldTrackIdNumOnly;

	}


	function sendEmail($id,$to,$data="")
	{

		//$id is the id for db record to fetch email sub and body

		$CI = & get_Instance();

		$CI->load->model('Model_email');
		$result = $CI->Model_email->get($id);
		
		$subject = $result->subject;
		$subject = $result->subject;
		//$subject = $subject.' | '.date('j F h:i:s a');
		$message = $result->template;
		$message_ar = $result->template_ar;

		if($id!=7)
		{
			$data['trackid'] = preg_replace( '/[^0-9]/', '', $data['trackid']); //track id only numbers
		}

		if($id==1)
		{
			$subject = str_replace('%%TRACK_ID%%', $data['trackid'] ,$subject); 
			$subject = str_replace('%%SUBJECT%%', $data['category_title'] ,$subject); //category title

			//$message = str_replace('%%NAME%%', ucfirst($data['first_name']) ,$message);
			$message = str_replace('%%TRACK_ID%%', "<strong>".$data['trackid']."</strong>" ,$message);

			//$message_ar = str_replace('%%NAME%%', ucfirst($data['first_name']) ,$message_ar);
			$message_ar = str_replace('%%TRACK_ID%%', "<strong style='font-size:13px'>".$data['trackid']."</strong>" ,$message_ar);

			$message = str_replace('%%Showroom_Name%%', "<strong>".$data['showroom']."</strong>" ,$message);
			$message_ar = str_replace('%%Showroom_Name%%', "<strong style='font-size:13px'>".str_replace(".","",$data['showroom_ar'])."</strong>" ,$message_ar);
			
			$message = str_replace('%%Vehicle_Type%%', "<strong>".$data['vehicles']."</strong>" ,$message);
			$message_ar = str_replace('%%Vehicle_Type%%', "<strong style='font-size:13px'>".$data['vehicles']."</strong>" ,$message_ar);			
			
			$cdt = "";
			$cdt_ar = "";
			if($data['create_action_date'])
			{
				$cdt = date("l j M Y",strtotime($data['create_action_date'])).' '.date("h:i a",strtotime($data['create_action_time'])).'';
				$cdt_ar = date("l j M Y",strtotime($data['create_action_date'])).' '.date("h:i a",strtotime($data['create_action_time'])).'';
			}

			$message = str_replace('%%Scheduled_Date_Time%%', "<strong>".$cdt."</strong>" ,$message);
			$message_ar = str_replace('%%Scheduled_Date_Time%%', "<strong dir='ltr' style='font-size:13px'>".$cdt_ar."</strong>" ,$message_ar);

		}

		elseif($id==2)
		{
			$subject = str_replace('%%TRACK_ID%%', $data['trackid'] ,$subject); 
			$subject = str_replace('%%Assign_To_Name%%', $data['assignee_name'] ,$subject); 
			$subject = str_replace('%%Assign_To_Name_Ar%%', $data['assignee_name_ar'] ,$subject); 
						
			$message = str_replace('%%TRACK_ID%%', $data['trackid'] ,$message);
			$message = str_replace('%%AdminComments%%', $data['body_concatinate'] ,$message);
			$message = str_replace('%%Assign_To_Name%%', "<strong>".$data['assignee_name']." | </strong><strong style='font-size:13px'>".$data['assignee_name_ar']."</strong>" ,$message);
			$message = str_replace('%%Showroom_Name%%', "<strong>".$data['showroom']." | </strong><strong style='font-size:13px'>".$data['showroom_ar']."</strong>" ,$message);
			$message = str_replace('%%Assign_To_Contact%%', "<strong>".$data['assign_to_contact']."</strong>" ,$message);			
			$message = str_replace('%%Job_Title%%', "<strong>".$data['job_title']." | </strong><strong style='font-size:13px'>".$data['job_title_ar']."</strong>" ,$message);			

		}

		elseif($id==3)
		{
			$subject = str_replace('%%TRACK_ID%%', $data['trackid'] ,$subject); 
			$subject = str_replace('%%SUBJECT%%', $data['category_title'] ,$subject); //category title
			$subject = str_replace('%%SUBJECTAR%%', $data['category_title_ar'] ,$subject); //category title

			$lt = date("l j M Y",strtotime($data['created_date'])).' '.date("h:i a",strtotime($data['created_date']));

			$message = str_replace('%%TRACK_ID%%', "<strong>".$data['trackid']."</strong>" ,$message);
			$message = str_replace('%%SUBJECT%%', "<strong>".$data['category_title']."</strong>" ,$message);
			$message = str_replace('%%Date%%', "<strong>".$lt."</strong>" ,$message);
			$message = str_replace('%%SurveyLink%%', $data['survey_link'] ,$message);

			$message_ar = str_replace('%%TRACK_ID%%', "<strong>".$data['trackid']."</strong>" ,$message_ar);
			$message_ar = str_replace('%%SUBJECTAR%%', "<strong>".$data['category_title_ar']."</strong>" ,$message_ar); //category title
			$message_ar = str_replace('%%Date%%', "<strong dir='ltr' style='font-size:13px'>".$lt."</strong>" ,$message_ar);
			$message_ar = str_replace('%%SurveyLinkAr%%', $data['survey_link_ar'] ,$message_ar);

		}
		elseif($id==4)
		{
			$subject = str_replace('%%TRACK_ID%%', $data['trackid'] ,$subject); 
			//$subject = str_replace('%%SUBJECT%%', $data['category_title'] ,$subject); //category title

			$message = str_replace('%%TRACK_ID%%', "<strong>".$data['trackid']."</strong>" ,$message);
			$message_ar = str_replace('%%TRACK_ID%%', "<strong style='font-size:13px'>".$data['trackid']."</strong>" ,$message_ar);

			$ct = date("l j M Y",strtotime($data['call_time'])).' '.date("h:i a",strtotime($data['call_time']));
			$message = str_replace('%%Call_Time%%', "<strong>".$ct."</strong>" ,$message);
			$message_ar = str_replace('%%Call_Time%%', "<strong style='font-size:13px'>".$ct."</strong>" ,$message_ar);

			$message = str_replace('%%Contact_Number%%', "<strong>".$data['contact_number']."</strong>" ,$message);
			$message_ar = str_replace('%%Contact_Number%%', "<strong dir='ltr' style='font-size:13px'>".$data['contact_number']."</strong>" ,$message_ar);

			$message = str_replace('%%Assign_To_Contact%%', "<strong>".$data['assign_to_contact']."</strong>" ,$message);
			$message_ar = str_replace('%%Assign_To_Contact%%', "<strong dir='ltr' style='font-size:13px'>".$data['assign_to_contact']."</strong>" ,$message_ar);
			

		}
		elseif($id==5)
		{
			$subject = str_replace('%%TRACK_ID%%', $data['trackid'] ,$subject); 
			//$subject = str_replace('%%SUBJECT%%', $data['category_title'] ,$subject); //category title

			
			$message = str_replace('%%TRACK_ID%%', "<strong>".$data['trackid']."</strong>" ,$message);
			$message_ar = str_replace('%%TRACK_ID%%', "<strong style='font-size:13px'>".$data['trackid']."</strong>" ,$message_ar);			
			
			$message = str_replace('%%Contact_Number%%', "<strong>".$data['contact_number']."</strong>" ,$message);
			$message_ar = str_replace('%%Contact_Number%%', "<strong dir='ltr' style='font-size:13px'>".$data['contact_number']."</strong>" ,$message_ar);

			$message = str_replace('%%Assign_To_Contact%%', "<strong>".$data['assign_to_contact']."</strong>" ,$message);
			$message_ar = str_replace('%%Assign_To_Contact%%', "<strong dir='ltr' style='font-size:13px'>".$data['assign_to_contact']."</strong>" ,$message_ar);						
			$cdt = "";
			$cdt_ar = "";
			if($data['create_action_date'])
			{
				$cdt = date("l j M Y",strtotime($data['create_action_date'])).' '.date("h:i a",strtotime($data['create_action_time'])).'';
				$cdt_ar = date("l j M Y",strtotime($data['create_action_date'])).' '.date("h:i a",strtotime($data['create_action_time'])).'';
			}

			$message = str_replace('%%Scheduled_Date_Time%%', "<strong>".$cdt."</strong>" ,$message);
			$message_ar = str_replace('%%Scheduled_Date_Time%%', "<strong dir='ltr' style='font-size:13px'>".$cdt_ar."</strong>" ,$message_ar);

		}					
		elseif($id==6)
		{
			$subject = str_replace('%%TRACK_ID%%', $data['trackid'] ,$subject); 
			//$subject = str_replace('%%SUBJECT%%', $data['category_title'] ,$subject); //category title

			$message = str_replace('%%TRACK_ID%%', "<strong>".$data['trackid']."</strong>" ,$message);
			$message_ar = str_replace('%%TRACK_ID%%', "<strong style='font-size:13px'>".$data['trackid']."</strong>" ,$message_ar);			
			
			$message = str_replace('%%Assign_To_Name%%', "<strong>".$data['assignee_name']."</strong>" ,$message);
			$message_ar = str_replace('%%Assign_To_Name%%', "<strong style='font-size:13px'>".$data['assignee_name']."</strong>" ,$message_ar);			

			$message = str_replace('%%Showroom_Name%%', "<strong>".$data['showroom']."</strong>" ,$message);
			$message_ar = str_replace('%%Showroom_Name%%', "<strong style='font-size:13px'>".str_replace(".","",$data['showroom_ar'])."</strong>" ,$message_ar);
			
			$message = str_replace('%%Vehicle_Type%%', "<strong>".$data['vehicles']."</strong>" ,$message);
			$message_ar = str_replace('%%Vehicle_Type%%', "<strong style='font-size:13px'>".$data['vehicles']."</strong>" ,$message_ar);		
			
			$message = str_replace('%%Assign_To_Contact%%', "<strong>".$data['assign_to_contact']."</strong>" ,$message);
			$message_ar = str_replace('%%Assign_To_Contact%%', "<strong dir='ltr' style='font-size:13px'>".$data['assign_to_contact']."</strong>" ,$message_ar);	

			$cdt = "";
			$cdt_ar = "";
			if($data['create_action_date'])
			{
				$cdt = date("l j M Y",strtotime($data['create_action_date'])).' '.date("h:i a",strtotime($data['create_action_time'])).'';
				$cdt_ar = date("l j M Y",strtotime($data['create_action_date'])).' '.date("h:i a",strtotime($data['create_action_time'])).'';
			}

			$message = str_replace('%%Scheduled_Date_Time%%', "<strong>".$cdt."</strong>" ,$message);
			$message_ar = str_replace('%%Scheduled_Date_Time%%', "<strong dir='ltr' style='font-size:13px'>".$cdt_ar."</strong>" ,$message_ar);

		}
		elseif($id==7)
		{
			$loginDetails = "<a href='".base_url()."'>Click here to login</a><br>Email: ".$to."<br>Password: ".$data['password'];
			$loginDetailsAr = "<a href='".base_url()."'>انقر هنا لتسجيل الدخول</a><br>البريد الإلكتروني: ".$to."<br>كلمه السر: ".$data['password'];
			$message = str_replace('%%Login_details%%', $loginDetails ,$message);
			$message_ar = str_replace('%%Login_details%%', $loginDetailsAr ,$message_ar);

		}

		$message = nl2br($message);
		$message_ar = nl2br($message_ar);


		$reply_above = '------ Reply above this line ------';		

		/*$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'mb.communication101@gmail.com',
			'smtp_pass' => 'MERCEDES!@#',
			'mailtype'  => 'text', //html?
			'charset'   => 'iso-8859-1'
		);
		$CI->load->library('email', $config);*/


		if($id==1)
		{
			$titletext = "Test Drive Request | طلب تجربة قيادة";
		}
		elseif($id==3)
		{
			$titletext = "Survey | استبيان";
		}
		elseif($id==4)
		{
			$titletext = "Call Attempt | محاولة اتصال";
		}
		elseif($id==5)
		{
			$titletext = "Scheduled call";
		}
		elseif($id==6)
		{
			$titletext = "Test Drive Confirmation | تأكيد تجربة قيادة";
		}
		else
		{	
			$titletext = $data['category_title'];
		}

		$message_html = '
<div bgcolor="#000" style="margin:0;background-color:#000;color:#fff">
    <table bgcolor="#000" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="font-family:Arial,Helvetica,sans-serif;color:#fff;background-color:#000;background:#000 url('.base_url().'assets/images/gradient.gif) repeat-x">
        <tbody><tr>
            <td align="center" valign="top">
                <table border="0" cellspacing="0" width="100%">
                    <tbody><tr>
                        <td align="center" valign="top">
                            <table border="0" cellspacing="0" width="100%">
                                <tbody><tr>
                                    <td align="center" valign="top">
                                        <table width="600">
                                            <tbody>
                                                <tr>
                                                    <td style="width:60%"><a style="color:#fff" href="http://lm.mercedesbenzksa.com" target="_blank">
                                                        <img src="'.base_url().'assets/images/emailLogoMB.png" alt="Leads System" width="198" height="50" border="0" class="CToWUd"></a></td>
                                                    <td width="1" height="99"></td>
                                                    <td align="right" style="text-align:right;max-width:40%;font-size:13px">'.$titletext.'</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellspacing="0" width="100%">
                                <tbody><tr>
                                    <td align="center" valign="top">
                                        <table width="600" style="background-color:#ffffff;color:#333333" bgcolor="#ffffff">
                                            <tbody>
                                                <tr>
<td width="15"></td>
                                                    <td height="15"></td>
<td width="15"></td>
                                                </tr>
                                                <tr>
<td width="15"></td>
                                                    <td style="line-height:1.4em">
													
														<span style="width:0; overflow:hidden;float:left; display:none">														
														'.$reply_above.'
														</span>

          <h1 style="direction:rtl; font-family:Georgia,Times,serif;color:#333333;font-weight:normal;margin:0 0 0.7em">عزيزي '.ucfirst($data['first_name']).'،</h1>														
														<p style="direction:rtl; margin:0 0 1em;color:#333333;font-size:16px">														
														'.$message_ar.'
														</p>                
														
														
														<h1 style="font-family:Georgia,Times,serif;color:#333333;font-weight:normal;margin:0 0 0.7em">Dear '.ucfirst($data['first_name']).',</h1>	
														
                                                        <p style="margin:0 0 1em;color:#333333;font-size:13px">
														
														'.$message.'
														</p>                                                        
                                                    </td>
<td width="15"></td>
                                                </tr>
                                                <tr>
                                                    <td height="30"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellspacing="0" width="100%">
                                <tbody><tr>
                                    <td align="center" valign="top">
                                        <table width="600" style="font-size:12px">
                                            <tbody>
                                                <tr>
                                                    <td>Juffali Automotive Co.
                                                    </td>
                                                    <td width="1" height="49"></td>
                                                    <td align="right" style="text-align:right"><a href="javascript:void(0);" style="color:#999" target="_blank">شركة إبراهيم الجفالي وإخوانه للسيارات</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>
</div>
';

		$CI->email->set_newline("\r\n");
		$CI->email->set_mailtype("html");
		if($id==7 && $result->attachment_name)
		{
			$CI->email->attach('./uploads/intro_email_attach/'.$result->attachment_name);
		}
		$CI->email->from('jls@mercedesbenzksa.com','Juffali Automotive Co. Leads System | شركة الجفالي للسيارات مرسيدس-بنز');
		$CI->email->reply_to('jls@mercedesbenzksa.com','Juffali Automotive Co. Leads System | شركة الجفالي للسيارات مرسيدس-بنز');
		$CI->email->to($to); 
		$CI->email->subject($subject);
		$CI->email->message($message_html);
		if ($CI->email->send()) { 
		}
		$CI->email->clear(true);

	}

	function getEmployeesByDep($id) 
	{
		
		 $CI = & get_Instance();
		 $CI->load->model('Model_struc_dep_users');
		 $where = array();
		 $where['struc_dep_id'] = $id;
		 $result = $CI->Model_struc_dep_users->getMultipleRows($where);
		 if($result)
			return $result;
		 else
 		    return null;


	}



	function getSubDepartments($id) 
	{
		
		 $CI = & get_Instance();
		 $CI->load->model('Model_structure_departments');
		 $where['parent_id'] = $id;
		 $result = $CI->Model_structure_departments->getMultipleRows($where);
		 if($result)
			return $result;
		 else
     	    return null;	

	}

	function getEmployeeName($id) 
	{
 		 if($id==="0")
			 return '';
		
		 $CI = & get_Instance();
		 $CI->load->model('Model_user');
		 $result = $CI->Model_user->get($id);
		 if($result)
			return $result->full_name;
		 else
     	    return '';	

	}
	function getEmployeeNameAr($id) 
	{
 		 if($id==="0")
			 return '';
		
		 $CI = & get_Instance();
		 $CI->load->model('Model_user');
		 $result = $CI->Model_user->get($id);
		 if($result)
			return $result->full_name_ar;
		 else
     	    return '';	

	}

	function getEmployeeTelNExt($id) 
	{
	
		 $ep = "";

 		 if($id==="0")
			 return '';
		
		 $CI = & get_Instance();
		 $CI->load->model('Model_user');
		 $result = $CI->Model_user->get($id);
		 if($result)
		 {
			$p = $result->phone;
			$pArr = explode("-",$p);
			if(count($pArr)==2)
			{
				$ep = $pArr[0].' Ext. '.$pArr[1];
			}			

			if($result->mobile)
				$ep = $ep.' Cell#: '.$result->mobile;

			return $ep;

		 }
		 else
     	    return '';	

	}

	function getEmployeeJobTitle($id) 
	{
 		 if($id==="0")
			 return '';
		
		 $CI = & get_Instance();
		 $CI->load->model('Model_user');
		 $result = $CI->Model_user->get($id);
		 if($result)
		 {
			/*$p = $result->phone;
			$pArr = explode("-",$p);
			if(count($pArr)==2)
			{
				return $pArr[0].' Ext. '.$pArr[1];
			}
			else
			{
				return '';
			}*/

			return $p = $result->job_title;

		 }
		 else
     	    return '';	

	}

	function getEmployeeJobTitleAr($id) 
	{
 		 if($id==="0")
			 return '';
		
		 $CI = & get_Instance();
		 $CI->load->model('Model_user');
		 $result = $CI->Model_user->get($id);
		 if($result)
		 {
			/*$p = $result->phone;
			$pArr = explode("-",$p);
			if(count($pArr)==2)
			{
				return $pArr[0].' Ext. '.$pArr[1];
			}
			else
			{
				return '';
			}*/

			return $p = $result->job_title_ar;

		 }
		 else
     	    return '';	

	}

	function getCreaterNameById($id) 
	{
	
		if($id == 0){
			return ''; //0 mean the website form via inbox
		}		
		else
		{
			
			 $CI = & get_Instance();
			 $CI->load->model('Model_user');
			 $result = $CI->Model_user->get($id);
			 if($result)
			 return $result->full_name;
				
		}
	}

	function getAssigneeNameById($id) 
	{
	
		if($id == 0){
			return 'Unassigned';
		}		
		else
		{
			
			 $CI = & get_Instance();
			 $CI->load->model('Model_user');
			 $result = $CI->Model_user->get($id);
			 if($result)
			 return $result->full_name;
				
		}
	}

	function getAssignedByNameById($id) 
	{
	
		if($id == 0){
			return '';
		}		
		else
		{
			
			 $CI = & get_Instance();
			 $CI->load->model('Model_user');
			 $result = $CI->Model_user->get($id);
			 if($result)
			 return $result->full_name;
				
		}
	}

	function getAdmin($id,$assign='') 
	{
	
		if($id == 0 && $assign == ''){
			return 'mercedesbenzme.com';
		}

		if($id==-2)
		{
			return "Auto Assigned";
		}
		
		if($id == 0 && $assign == 1)
		{
			return '';
		}
		else
		{
			
			 $CI = & get_Instance();
			 $CI->load->model('Model_user');
			 $result = $CI->Model_user->get($id);
			 if($result)
			 return $result->full_name;
				
		}
	}
   function checkAdminSession()
   {
	   $CI = & get_Instance();
	   if($CI->session->userdata('user'))
	   {
		   return true;
		   
	   }else
	   {
		   redirect($CI->config->item('base_url') . 'admin');
	   }
   }
   
   function getRights($section_id,$role_id)
   {
	    $data = array();
		$CI = & get_Instance();
	    $CI->load->model('Model_right');
		$data['section_id'] = $section_id;
		$data['role_id'] = $role_id;
	    $result = $CI->Model_right->getWithMultipleFields($data);
		return $result;
   }

   
   
   function getVehicleIdByName($vehicle_name)
   {
		$data = array();
		$CI = & get_Instance();
	    $CI->load->model('Model_vehicle');
		
		$data['name'] = $vehicle_name;
		$result = $CI->Model_vehicle->getWithMultipleFields($data);
		if (!empty($result)) {
			return $result->id;
		}
		else
			return '';
		
		
   }


	function getSpecificVehicleNameById($id)
   {
		$data = array();
		$CI = & get_Instance();
	    $CI->load->model('Model_vehicle_specific_name');
		
		$data['id'] = $id;
		$result = $CI->Model_vehicle_specific_name->getWithMultipleFields($data);
		if (!empty($result)) {
			return $result->name;
		}
		else
			return 'N/A';		
   }

   function getGeneralVehicleNameById($id)
   {
		$data = array();
		$CI = & get_Instance();
	    $CI->load->model('Model_vehicle');
		
		$data['id'] = $id;
		$result = $CI->Model_vehicle->getWithMultipleFields($data);
		if (!empty($result)) {
			return $result->name;
		}
		else
			return 'N/A';		
   }

   function getVehicles($vehicle_ids)
   {
	    if($vehicle_ids == '')
		{
			return '';
		}
		$data = array();
		$CI = & get_Instance();
	    $CI->load->model('Model_vehicle');
		$vehicle_ids = explode(',',$vehicle_ids);
		foreach($vehicle_ids as $id)
		{
			$result = $CI->Model_vehicle->get($id);
			if($result){
			$data[] = $result->name;
			}
		}
		
		return implode(', ', $data);
   }
      function getSpeVehicles($vehicle_ids)
   {
	    if($vehicle_ids == '')
		{
			return '';
		}
		$data = array();
		$CI = & get_Instance();
	    $CI->load->model('Model_vehicle_specific_name');
		$vehicle_ids = explode(',',$vehicle_ids);
		foreach($vehicle_ids as $id)
		{
			$result = $CI->Model_vehicle_specific_name->get($id);
			if($result){
				$data[] = $result->name;
			}
			
		}
		
		return implode(', ', $data);
   }
   function getSubLeads($email,$lead_id)
   {
	    $data = array();
		$CI = & get_Instance();
	    $CI->load->model('Model_lead');


		/*$onlyAssignee = 0;
		if( rights(1,'write') )
		{
			//do nothing
			//can see all leads if the logged in user has the rights to create new leads.
		}
		else
		{
			$onlyAssignee = $CI->session->userdata['user']['id'];
		}*/

		//$result = $CI->Model_lead->getSubLeads($email, 0, $onlyAssignee);
		$result = $CI->Model_lead->getSubLeads($email,$lead_id);
		return $result;
		
		//return implode(',', $data);
   }

   function menu()
   {
	   $createRound = "";
			   
		if(rights('1','write'))
			$createRound .= '<li><a href="'.base_url().'lead/createNewLead"><i class="sprite sprite-menagment"></i> Create a New lead</a></li>';
		if(rights('8','write'))
			$createRound .= '<li><a href="'.base_url().'user/userAction"><i class="fa fa-user"></i> Create a New User</a></li>';
		if(rights('11','write'))
			$createRound .= '<li><a href="'.base_url().'role"><i class="fa fa-users"></i>Create a New Role</a></li>';
		if(rights('6','write'))
			$createRound .= '<li><a href="'.base_url().'branch_management"><i class="fa fa-users"></i> Create a New Branch</a></li>';                
		if(rights('5','write'))
			$createRound .= '<li><a href="'.base_url().'organization_structure"><i class="fa fa-sitemap"></i> Create Structure</a></li>';
		if(rights('9','write'))
			$createRound .= '<li><a href="'.base_url().'survey/surveyAction"><i class="fa fa-file-text"></i> Create a Survey</a></li>';

		return $createRound;
		
		
   }
   
   function rights($section_id,$action)
   {
	  $CI = & get_Instance();
	  $CI->load->model('Model_right');
	  $data = array();
	  $data['section_id'] = $section_id;
	  $data['role_id'] = $CI->session->userdata['user']['role_id'];
	  $result = $CI->Model_right->getWithMultipleFields($data,true);
	  //print_r($result);
	  switch($action)
	  {
		  case 'read':
		  return $result['read'];
		  break;
		  case 'write':
	      return $result['write'];
		  break;
		  case 'update':
		  return $result['update'];
		  break;
		  case 'delete':
		  return $result['delete'];
		  break;
          case 'all':
          if($result['read'] != 0 || $result['write'] != 0 || $result['update'] != 0 || $result['delete'] != 0)
          {
            return true;
          }else
          {
            return false;
          }
          
          break;
		  
	  }
	
	}
    function checkIsSubmittedSurvey($survey_id)
   {
	  $CI = & get_Instance();
	  $CI->load->model('Model_survey_result');
	  $data = array();
	  $data['survey_id'] = $survey_id;
      $result = $CI->Model_survey_result->getMultipleRows($data);
	  
      return $result;
	
	}
    
    function checkCallOfActionCreated($lead_id)
   {
	  $CI = & get_Instance();
	  $CI->load->model('Model_leads_messages');
	  $result = $CI->Model_leads_messages->checkCallOfActionCreated($lead_id);
	  
      return $result;
	
	}
    
    function checkIsQuestionSubmittedInSurvey($question_id)
   {
	  $CI = & get_Instance();
	  $CI->load->model('Model_survey_result');
	  $data = array();
	  $data['question_id'] = $question_id;
      $result = $CI->Model_survey_result->getMultipleRows($data);
	  
      return $result;
	
	}
	
   function getCount($status = '')
   {
	  $CI = & get_Instance();
	  $CI->load->model('Model_report');
	  $data = array();
      $result = $CI->Model_report->getCountByStatus($status);
	  
      return $result;
	
	}
	
	function getTrackId($lead_id)
   {
	  $CI = & get_Instance();
	  $CI->load->model('Model_lead');
	  $data = array();
      $result = $CI->Model_lead->get($lead_id);
	  if($result)
		  return $result->trackid;
	  else
		  return '';
	
	}
	
	function checkLoggedInUserInboxRole()
	{
		$CI =& get_instance();
		$CI->load->model('Model_user');

		$logged_in_userid = $CI->session->userdata['user']['id'];

		$assigneeIdsRes = $CI->Model_user->getInboxAssignees();			
		if($assigneeIdsRes)
		{
			foreach($assigneeIdsRes as $assigeeId)
			{
				if(in_array($logged_in_userid, $assigeeId))
				{
					return true;
				}

			}
		}
		
		return false;
	}

	//duration(hours) b/w lead created and the first call to customer.
	function getCallKpiDuration($lead_id)
	{
		$CI =& get_instance();
		$CI->load->model('Model_leads_messages');
		$CI->load->model('Model_lead');

		//first time call to customer date/time
		$firstCallDateTime = $CI->Model_leads_messages->getfirstCallToCustomerDateTime($lead_id);		

		if($firstCallDateTime)
		{
			//get lead created datetime
			$leadRec = $CI->Model_lead->get($lead_id);
			$leadCreatedDateTime = $leadRec->created_at;
			$totalMinutes = calculateMinutesBetweenTwoDatetimes($leadCreatedDateTime, $firstCallDateTime);
			$hours = floor($totalMinutes / 60);
			$minutes = ($totalMinutes % 60);

			if($hours)
			{
				return $hours.' Hrs. & '.$minutes.' Min.';
			}
			else
			{
				return $minutes.' Min.';
			}

		}else
		{
			return 'N/A';
		}

	}

	function getCategoryKpiValue($category_id)
	{
		if($category_id)
		{
			$data = array();
			$CI = & get_Instance();
			$CI->load->model('Model_category');
			

			$results = $CI->Model_category->get($category_id);
			return $results->call_kpi;
		}
		else
		{
			return "N/A";
		}
	}

	
	function getLoggedInUserManagingDeps()
	{			
		$data = array();
		$CI = & get_Instance();
		$CI->load->model('Model_structure_departments');
		return $CI->Model_structure_departments->getUserManagingDeps($CI->session->userdata['user']['id']);
	}

	function getUsersUnderMngr($depId)
	{		
		$data = array();
		$CI = & get_Instance();
		$CI->load->model('Model_struc_dep_users');
		return $CI->Model_struc_dep_users->getUsersUnderMngr($depId);
	}

	function addNotification($comments){	
		
		$dataNotification = array();
		$CI = & get_Instance();
		$CI->load->model('Model_notification');

		$dataNotification['created_at'] = date('Y-m-d H:i:s');
		$dataNotification['created_by'] = $CI->session->userdata['user']['id'];		
		$dataNotification['comments'] = $comments;	
		$CI->Model_notification->save($dataNotification);
	}

	function getAllRelatedLeads(){			

		$dataNotification = array();
		$CI = & get_Instance();
		$CI->load->model('Model_lead');

		$users = array();
		$keyWord = "";
		$checkLoggedInUserInboxRole = checkLoggedInUserInboxRole();
		$applyFilter = false;
		$category_id=0;
		$status="";
		//return $CI->Model_lead->getAllSortedByPaged(0,0,0,$users);
		return $CI->Model_lead->getAllSortedByPaged(0,0,0,$users,$keyWord,$checkLoggedInUserInboxRole,$applyFilter,$category_id,$status);		

	}

	//for call of action and test drive 30 min.
	function checkRemainTimeForAction($lead_id){	
		
		$dataNotification = array();
		$CI = & get_Instance();
		$CI->load->model('Model_leads_messages');

		$data['schedule_date_time >= '] = date('Y-m-d H:i:s');
		$data['schedule_date_time <= '] = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." +30 minutes"));
		$data['lead_id'] = $lead_id;
		$results = $CI->Model_leads_messages->getMultipleRows($data);

		//echo $CI->db->last_query(); exit();
		$actionIds = "";
		if($results)
		{
			foreach($results as $result)
			{
				if($actionIds!="") $actionIds.=",";
				$actionIds.=$result->id;

			}
			return $actionIds;
		}
		
		return false;
				

	}

		//for call of action and test drive 30 min.
	function checkIfTagged($lead_id){	
		
		$dataNotification = array();
		$CI = & get_Instance();
		$CI->load->model('Model_lead_tags');
		
		$data['user_id'] = $CI->session->userdata['user']['id'];
		$data['lead_id'] = $lead_id;
		$results = $CI->Model_lead_tags->getMultipleRows($data);

		//echo $CI->db->last_query(); exit();
		
		if($results)
			return true;
		
		return false;
				

	}

	function getSourceTitle($id)
	{		
		$data = array();
		$CI = & get_Instance();
		$CI->load->model('Model_source');

		$result = $CI->Model_source->get($id);
		return $result->title;	
	}

	function getTagTitle($id)
	{		
		$data = array();
		$CI = & get_Instance();
		$CI->load->model('Model_tag');

		$result = $CI->Model_tag->get($id);
		return $result->title;	
	}
	