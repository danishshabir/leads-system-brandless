<?php 
function rowLead($isSingleDetailLead, $lead, $i, $targetId, $rowClickClass, $dashboard,$logged_in_user_id,$hasNoActionStyle="")
{
	$CI = & get_Instance(); 

	//$d_l = "";
	//if(!$dashboard) $d_l = ' data-leadId="'.$lead->id.'" ';
	$d_l = ' data-leadId="'.$lead->id.'" ';

	$isManager = checkLoggedInUserMng();

	if( rights(33,'read') ) //33 is lead controller sub manager
		$isManager = true;

	?>

				<td style="<?php echo $hasNoActionStyle; ?>" <?php echo $d_l ?> id="<?php if($isSingleDetailLead) echo 'singleDetailLead' ?>" class="<?php echo $rowClickClass ?>" data-toggle="collapse" data-target="<?php echo $targetId; ?><?php echo $lead->id;?>"><?php echo $lead->trackid; ?></td>
				<td <?php echo $d_l ?> class="<?php echo $rowClickClass ?>" data-toggle="collapse" data-target="<?php echo $targetId; ?><?php echo $lead->id;?>"><?php echo $lead->title.' '.ucfirst($lead->first_name).' '.ucfirst($lead->surname); ?></td>

				<?php if(!$dashboard) { ?>
				<!--<td <?php echo $d_l ?> class="<?php echo $rowClickClass ?>" data-toggle="collapse" data-target="<?php echo $targetId; ?><?php echo $lead->id;?>"><a href="javascript:void(0);"><?php echo $lead->email; ?></a></td>-->
				<?php } ?>
				
				<?php if($dashboard) { ?>
				<?php if(!$isManager) { ?>
				<td <?php echo $d_l ?> class="<?php echo $rowClickClass ?>" data-toggle="collapse" data-target="<?php echo $targetId; ?><?php echo $lead->id;?>"><a href="tel:<?php echo $lead->mobile; ?>"><?php echo $lead->mobile; ?></a></td>
				<?php } ?>
				<?php } ?>

				<?php if(!$dashboard) { 
				if(!$isManager && !rights(35,'read')){ 
				?>
				<td <?php echo $d_l ?> class="<?php echo $rowClickClass ?>" data-toggle="collapse" data-target="<?php echo $targetId; ?><?php echo $lead->id;?>"><a href="tel:<?php echo $lead->mobile; ?>"><?php echo $lead->mobile; ?></a></td>
				<?php } } ?>

				<?php if(!$dashboard) { ?>
				
				<?php if(rights(35,'read')){  ?>
				
					<td <?php echo $d_l ?> class="<?php echo $rowClickClass ?>" data-toggle="collapse" data-target="<?php echo $targetId; ?><?php echo $lead->id;?>"><a href="javascript:void(0);"><?php echo getBranchName($lead->branch_id); ?></a></td>
					
				<?php }else{ ?>	
				
					<?php if(!$isManager){  ?>
						<!--<td <?php echo $d_l ?> class="<?php echo $rowClickClass ?>" data-toggle="collapse" data-target="<?php echo $targetId; ?><?php echo $lead->id;?>"><a href="javascript:void(0);"><?php echo getBranchName($lead->branch_id); ?></a></td>-->
					<?php } ?>
				
				<?php } ?>			
				
				<td title="<?php echo getCreaterNameById($lead->orignal_created_by);?>" <?php echo $d_l ?> class="<?php echo $rowClickClass ?>" data-toggle="collapse" data-target="<?php echo $targetId; ?><?php echo $lead->id;?>"><a href="javascript:void(0);"><?php if($lead->form_type!=="") echo $lead->form_type; else {if($lead->manual_source!=="0") echo getSourceTitle($lead->manual_source); else echo getCreaterNameById($lead->created_by/*$lead->orignal_created_by*/);} ?></a></td>

				<?php if(!$isSingleDetailLead) { ?>							
							<?php 
							//if($isManager || rights(35,'read')){ 
							if(rights(35,'read')){
							?>
								
				<td <?php echo $d_l ?> id="<?php if($isSingleDetailLead) echo 'singleDetailLead' ?>" class="<?php echo $rowClickClass ?>" data-toggle="collapse" data-target="<?php echo $targetId; ?><?php echo $lead->id;?>"><?php echo getEventName($lead->event_id); ?></td>
		
				<?php } }  ?>

				<td <?php echo $d_l ?> id="assigneeName<?php echo $lead->id; ?>" class="<?php echo $rowClickClass ?>" data-toggle="collapse" data-target="<?php echo $targetId; ?><?php echo $lead->id;?>"><a href="javascript:void(0);"><?php echo getAssigneeNameById($lead->assign_to); ?></a></td>
				<?php } ?>


				<?php if($dashboard) { ?>
				<!--<td <?php echo $d_l ?> class="<?php echo $rowClickClass ?>" data-toggle="collapse" data-target="<?php echo $targetId; ?><?php echo $lead->id;?>"><a href="javascript:void(0);"><?php echo getCityName($lead->city_id); ?></a></td>-->
				<?php } ?>

				<?php if($dashboard) { ?>
				<?php if(!$isManager) { ?>
				<td <?php echo $d_l ?> class="<?php echo $rowClickClass ?>" data-toggle="collapse" data-target="<?php echo $targetId; ?><?php echo $lead->id;?>"><a href="javascript:void(0);"><?php echo getCategoryName($lead->category_id); ?></a></td>
				<?php } ?>
				<?php } ?>
				
<!--<td id="duedateval<?php echo $lead->id; ?>" title="<?php echo date("j M Y",strtotime($lead->due_date));  ?>" <?php echo $d_l ?> class="<?php echo $rowClickClass ?>" data-toggle="collapse" data-target="<?php echo $targetId; ?><?php echo $lead->id;?>">-->

<td id="duedateval<?php echo $lead->id; ?>" title="" <?php echo $d_l ?> class="<?php echo $rowClickClass ?>" data-toggle="collapse" data-target="<?php echo $targetId; ?><?php echo $lead->id;?>">

<?php
				
						if($lead->is_data_center && $CI->session->userdata['user']['role_id']==="73" && $lead->survey_response==="0") 
						{
							echo "<strong>Survey</strong>";
						}
						else
						{
							if($lead->status=="Closed" || $lead->status=="Finished" || $lead->status=="Approved and Archived" || $lead->status=="Duplicated")
							{							
								echo $lead->status;
							}
							else
							{
								//echo dueCounter($lead->due_date);  
								echo ageCounter($lead->created_at);
							}
						}
					?></td>
					
					<!--<td id="duedateval<?php echo $lead->id; ?>" title="" <?php echo $d_l ?> class="<?php echo $rowClickClass ?>" data-toggle="collapse" data-target="<?php echo $targetId; ?><?php echo $lead->id;?>"><?php
								echo ageCounter($lead->created_at);
					?></td>-->
					
<td><div class="standardEdBtn dropdown">
				
						<button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" type="button" id="dLabel<?php echo $lead->id;?>"></button>
						<ul aria-labelledby="dLabel" class="dropdown-menu">							
							
							<li><a title="Open in new window" target="_blank" href="<?php echo base_url();?>lead/singleLead/<?php echo $lead->id; ?>">View</a></li>

							<?php if(rights(1,'update')){ ?>
							<li><a href="<?php echo base_url();?>lead/editLead/<?php echo $lead->id; ?>">Edit Lead</a></li>
							<?php } ?>							

							<?php if(rights(1,'delete')){ ?>
							<li><a href="javascript:void(0);" onClick="deleteRecord('<?php echo $lead->id; ?>','lead/leadAction','<?php echo base_url();?>lead');">Delete Lead</a></li>
							<?php } ?>

							<?php 
											
							if(rights(2,'write')){ ?>
								<li>
									<a data-target=".assignlead" data-toggle="modal" onclick="beforeAssignPopup('<?php echo $lead->id; ?>','<?php echo getCityName($lead->city_id); ?>', '<?php echo getBranchName($lead->branch_id); ?>', '<?php echo $lead->city_id; ?>','<?php echo $lead->branch_id; ?>');">
										Assign Lead
									</a>
								</li>							
							<?php } ?>
							
<!----- if lead already exist then populate it again in form by clicking here------>	
	<?php
    $ifSearch = $_SERVER['QUERY_STRING'];
	if($ifSearch != ""){ ?>
			<li><a title="Create new lead again"  href="<?php echo base_url();?>lead/createAgainLead/<?php echo $lead->id; ?>">Create Lead Again</a></li>
		<?php 
	 }  ?>


						</ul>

							<?php if($dashboard) { ?>
							<a title="Open in new window" target="_blank" href="<?php echo base_url();?>lead/singleLead/<?php echo $lead->id; ?>">
								<img style="display: inline-block; vertical-align: top; margin-top: 10px;" width="18" title="Open in new Window" height="18" alt="forward_transparent" src="<?php echo base_url();?>assets/images/forward_transparent.png">
							</a>
							<?php } ?>
  </div></td>



	<?php
}


function expandedLead($lead,$i,$logged_in_user_id, $dashboard, $isSingleDetailLead,$vehicles_specific,$is_sublead,$tags)
{

	$CI = & get_Instance(); 

	//this logic will check if the logged in user is a manager of any department.
	$isManager = checkLoggedInUserMng();

	if( rights(33,'read') ) //33 is lead controller sub manager
		$isManager = true;

	?>
		
		<?php if(!$dashboard) { ?>
		<div class="TableBox">
			<table width="100%" border="1">
			<tr>
				<td><label>Receiving Date</label> <small><?php echo date("j M Y",strtotime($lead->created_at));  ?></small></td>				
				<td><label>Status</label> <small><span id="statusval<?php echo $lead->id; ?>"><?php echo $lead->status ?></span></small></td>						
				<td><label>Assigned by</label> <small><a href="javascript:void(0);"><?php echo getAssignedByNameById($lead->assign_by); ?></a></small></td>			
				<td><label>Category</label> <small><?php echo getCategoryName($lead->category_id); ?></small></td>




				<?php 
				$spe_vehicles = getSpeVehicles($lead->vehicle_specific_names_id);
				$vehicles = getVehicles($lead->vehicle); ?>
				<td><label>Vehicle</label> <small><?php if($vehicles || $spe_vehicles) 


				{ 
					echo $spe_vehicles; 
					if($spe_vehicles!="" && $vehicles!="") echo ", ";
					echo $vehicles;  
					
				} else { echo 'N/A'; } ?></small></td>  

			  </tr>

			  <tr>
				<td><label>City</label> <small><?php echo getCityName($lead->city_id); ?></small></td>
				
				<?php if(!rights(35,'read')){ ?>
				<td><label>Branch</label> <small><?php echo getBranchName($lead->branch_id); ?></small></td>

				<?php } ?>
				
				<!--<td><label>Valid driving license</label> <small><?php if($lead->valid_driving_license == 1) echo "Yes"; elseif($lead->valid_driving_license == 0) echo "No"; else echo 'N/A';?></small></td>-->
				<!--<td><label>Pref Test Drive date/time</label> <small><?php echo ($lead->preferred_date=="0000-00-00" ? "N/A" : date("j M Y",strtotime($lead->preferred_date)). ' / '.date("h:i a",strtotime($lead->preferred_time))  );  ?></small></td>
				<td><label>Pref Mode of contact</label> <small><?php if($lead->preferred_mode_of_contact) echo $lead->preferred_mode_of_contact; else echo 'N/A'; ?></small></td>-->
				<td><label>Email</label> <small><?php echo $lead->email; ?></small></td>

				<?php if($isManager || rights(35,'read')){ ?>
				<td><label>Phone/Mobile</label> <small><?php echo $lead->mobile; ?></small></td>

				<?php } ?>

				<?php if(!rights(35,'read')){ ?>
				<td><label>Event</label> <small><?php echo getEventName($lead->event_id); ?></small></td>

				<?php } ?>

				<!--<td><label>Call KPI</label> <small title="Category Call KPI Value is <?php echo getCategoryKpiValue($lead->category_id); ?> Hrs."><?php echo getCallKpiDuration($lead->id); ?></small></td>-->
			  </tr>

			  <tr>				
				<td colspan="5" class="heighlightCmtEd"><label>Comments</label> <small><?php echo $lead->comments; ?></small></td>

			  </tr>

			 

			</table>
			</div>
			<?php } ?>

			<?php //if( $lead->assign_to == $logged_in_user_id || $lead->created_by == $logged_in_user_id || checkLoggedInUserInboxRole() ){ ?>

			<div id="ActionBox<?php echo $lead->id ?>" class="ActionBox <?php if($lead->assign_to == $logged_in_user_id && $lead->status=="Started") echo 'startedBtnShowing'; else echo 'startedBtnNotShowing'; ?>">


				<?php if(!$dashboard) { ?>

				  <div class="ActionHeadingBox">
					<!-- in dashboard and listing screen of leads it will show latest ___ messages -->
						<h2><i class="fa fa-exchange"></i> Actions</h2>
						
						<?php 
						$callIcon = "";
						$callIconMsg = "";
						$call_status = getLastMessagePhoneStatus($lead->id);
						if($call_status==='9')
						{
							$callIcon = "callAns.png"; 
						}
						elseif($call_status==='10')
						{
							$callIcon = "callEnd.png";
						}
						elseif($call_status==='8')
						{
							$callIcon = "callDelay.png";							
						}

						?>
						
						<?php if($callIcon)
						{
							$callStatusType = getMessageType($call_status); 
						?>
								<!--<img src="<?php echo base_url();?>assets/images/<?php echo $callIcon; ?>" alt="" title="<?php echo $callStatusType->title; ?>" class="pull-right" />  -->
						<?php 
						}
						?>
						<div class="clearfix"></div>

					</div>
				<?php } ?>

			<?php 
			
			if($isSingleDetailLead)
			{
				$howManyLatest = '-1'; //-1 mean all. Only on lead delail screen it will show all

			}else
			{
				if($dashboard)
					$howManyLatest = '2';
				else
					$howManyLatest = '-1';
			}

			
			$messages = getLeadMessages($lead->id,$howManyLatest);

				if($messages)
				{

					displayMessages($messages, $logged_in_user_id, $lead, $dashboard, $howManyLatest);
					
				}
				
			?>


				<?php 
				// rights(33,'read') //33 is lead controller sub manager

				if($isManager || rights(33,'read') || $lead->assign_to == $logged_in_user_id || $lead->created_by == $logged_in_user_id || checkLoggedInUserInboxRole() || checkIfTagged($lead->id)){ ?>

					<?php if(!$dashboard){  ?>

						<a class="AddPlusBtn" href="javascript:void(0);" onClick="$('#AddPlusAreaCreateAnAction<?php echo $lead->id;?>').toggle(); $('#schSubm<?php echo $lead->id; ?>').attr('value', 'Save');">+</a>

					<?php } ?>

				<?php } ?>
				
				
			
				<?php  if($dashboard){ ?>
				<div class="dashLeadsAssignedDTL" id="AddPlusArea<?php echo $lead->id;?>">
				
							<div class="step2CallSec">									
								<ul>
									<!--<li><a class="AddPlusBtnIcons" data-toggle="collapse" href="#PhoneBoxCollapse<?php echo $lead->id ?>"><i title="What you talked on phone call with customer?" class="fa fa-phone"></i></a></li>-->
									<li><a class="AddPlusBtnIcons" data-toggle="collapse" href="#CommentBoxCollapse<?php echo $lead->id ?>"><i title="Add internal comment" class="fa fa-commenting"></i></a></li>									
									<li><a class="AddPlusBtnIcons" data-toggle="collapse" href="#SendCustomerEmailCollapse<?php echo $lead->id ?>"><i title="Send email to customer" class="fa fa-envelope"></i></a></li>
								</ul>
								<div class="clearfix"></div>

							</div>


							<div class="typeMessage collapse PhoneBoxCollapse" id="PhoneBoxCollapse<?php echo $lead->id ?>">
								<form data-dashboard="1" data-ajaxactionleadid="<?php echo $lead->id ?>" action="<?php echo base_url();?>lead/leadAction" class="mb_form" onsubmit="return false;">

									<input type="hidden" name="message_id" id="message_id<?php echo $lead->id; ?>" value="9">
									<input type="hidden" name="form_type" value="action_added">
									<input type="hidden" name="lead_id" value="<?php echo $lead->id ?>"> 

									<ul class="callIcons">										
          <li>
            <label>
              <input required type="radio" name="call_status" onclick="$('#message_id<?php echo $lead->id; ?>').val('9');"/>
              <img title="<?php echo getMessageType(9)->title ?>" src="<?php echo base_url();?>assets/images/callAns.png" alt="" height="18" width="18" /></label>
          </li>
          <li>
            <label>
              <input type="radio" name="call_status" onclick="$('#message_id<?php echo $lead->id; ?>').val('10');"/>
              <img title="<?php echo getMessageType(10)->title ?>" src="<?php echo base_url();?>assets/images/callEnd.png" alt="" height="18" width="18" /></label>
          </li>
          <li>
            <label>
              <input type="radio" name="call_status" onclick="$('#message_id<?php echo $lead->id; ?>').val('8');"/>
              <img title="<?php echo getMessageType(8)->title ?>" src="<?php echo base_url();?>assets/images/callDelay.png" alt="" height="18" width="18" /></label>
          </li>
									</ul>

									<input type="text" name="comments" placeholder="Type Here" />
									<input type="submit" class="btn" value="Submit" />
									<div class="clearfix"></div>
								</form>
							</div>


							<div class="typeMessage collapse CommentBoxCollapse" id="CommentBoxCollapse<?php echo $lead->id ?>">
								<form data-dashboard="1" data-ajaxactionleadid="<?php echo $lead->id ?>" action="<?php echo base_url();?>lead/leadAction" class="mb_form" onsubmit="return false;">
									<input type="hidden" name="message_id" id="type" value="4">
									<input type="hidden" name="form_type" value="action_added">
									<input type="hidden" name="lead_id" value="<?php echo $lead->id ?>">
                                    
									<input type="hidden" name="tagged_ids" class="tagged_ids" value="">

									<input type="text" name="comments" class="mention" data-leadid="<?php echo $lead->id; ?>" autocomplete="off" placeholder="Type Internal Comment" />
									<input type="button" class="btn" value="Submit" onClick="getTaggedFromLeadsScr('CommentBoxCollapse<?php echo $lead->id ?>');"/>
									<div class="clearfix"></div>
								</form>
							</div>

							<div class="typeMessage collapse SendCustomerEmailCollapse" id="SendCustomerEmailCollapse<?php echo $lead->id ?>">
								<form data-dashboard="1" data-ajaxactionleadid="<?php echo $lead->id ?>" action="<?php echo base_url();?>lead/leadAction" class="mb_form" onsubmit="return false;">
									<input type="hidden" name="message_id" id="type" value="2">
									<input type="hidden" name="form_type" value="action_added">								
									<input type="hidden" name="lead_id" value="<?php echo $lead->id ?>">

									<input type="text" name="comments" placeholder="Send Email to Customer" />
									<input type="submit" class="btn" value="Submit" />
									<div class="clearfix"></div>
								</form>
							</div>
							
					</div>					
					<?php } ?>
			</div><!-- .ActionBox -->

			<?php //} ?>


						<?php if(!$dashboard) { ?>						
							<div style="display:none;" class="dashLeadsAssignedDTL" id="AddPlusAreaCreateAnAction<?php echo $lead->id;?>">
				

					
							<div class="CreateAnAction">
								<div class="ActionHeadingBox">
									<h2><i class="fa fa-exchange"></i> Create an Action</h2>
								</div>
								<div class="ActionTabsBox">
									<ul class="nav nav-tabs" id="myTab">

										<?php 
											$onlyComments = false;											
											

											if(checkIfTagged($lead->id) && !$isManager) 
											{ 
												$onlyComments = true; 
											}
											
											if($lead->assign_to == $logged_in_user_id && ($lead->status==="Finished" || $lead->status==="No Response") )
											{ 
												$onlyComments = true; 
											}

											if($lead->status==="Closed" || $lead->status==="Approved and Archived")
											{ 
												$onlyComments = true; 
											}

											if($onlyComments) { 

												if(rights(30,'write')) { ?>
												<li class="Comment"><a data-target="#Comment<?php echo $lead->id ?>" data-toggle="tab"><i class="fa fa-commenting"></i> Comment</a></li>
												<?php } ?>

											<?php }else{ 
											
												if(rights(27,'write')) { ?>
												<li class="Call active"><a data-target="#Call<?php echo $lead->id ?>" data-toggle="tab"><i class="fa fa-phone"></i> Call</a></li>
												<?php } ?>

												<?php if(rights(28,'write')) { ?>
												<li class="Email"><a data-target="#Email<?php echo $lead->id ?>" data-toggle="tab"><i class="fa fa-envelope"></i> Email</a></li>
												<?php } ?>

												<?php if(rights(29,'write')) { ?>
												<li class="Visit"><a data-target="#Visit<?php echo $lead->id ?>" data-toggle="tab"><i class="fa fa-user"></i> Visit</a></li>
												<?php } ?>											

												<?php if(rights(31,'write')) { ?>
												<li class="Comment"><a data-target="#Schedule_Test<?php echo $lead->id ?>" data-toggle="tab"><i class="fa fa-commenting"></i> Test Drive</a></li>
												<?php } ?>

												<?php if(rights(30,'write')) { ?>
												<li class="Comment"><a data-target="#Comment<?php echo $lead->id ?>" data-toggle="tab"><i class="fa fa-commenting"></i> Comment</a></li>
												<?php } ?>

											<?php } ?>
									</ul>
									

									
									<div class="tab-content" id="createAnActionFrms<?php echo $lead->id ?>">

										<?php if(rights(27,'write')) { ?>
										<div class="tab-pane callActionEd <?php if(!$onlyComments) echo 'active'; ?>" id="Call<?php echo $lead->id ?>">
										<form data-dashboard="0" data-ajaxactionleadid="<?php echo $lead->id ?>" action="<?php echo base_url();?>lead/leadAction" class="mb_form" data-callofaction="1" onsubmit="return false;">											
											<input type="hidden" name="message_id" id="message_id<?php echo $lead->id; ?>" value="18">
											<input type="hidden" name="tagged_ids" class="tagged_ids" value="">
											
											<div class="row">
												<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
													<label>Did you get an answer?</label>
												</div>
												<div class="col-lg-6 col-md-4 col-sm-4 col-xs-3">													

														<input id="ayrba<?php echo $lead->id; ?>" selected="selected" type="radio" name="call_status" onclick="$('#message_id<?php echo $lead->id; ?>').val('9'); $('#schedule_call<?php echo $lead->id; ?>').hide(); $('.datepicker,.timepicker,.date_alternate,.time_alternate').val(''); $('.datepicker,.timepicker').attr('required', false);"/><label for="ayrba<?php echo $lead->id; ?>"><span><span></span></span>Yes</label>														
														&nbsp;&nbsp;
														<input id="anrba<?php echo $lead->id; ?>" type="radio" name="call_status" onclick="$('#message_id<?php echo $lead->id; ?>').val('10'); $('#schedule_call<?php echo $lead->id; ?>').hide(); $('.datepicker,.timepicker,.date_alternate,.time_alternate').val(''); $('.datepicker,.timepicker').attr('required', false);"/><label for="anrba<?php echo $lead->id; ?>"><span><span></span></span>No</label>

														&nbsp;&nbsp;
														<input id="acblrba<?php echo $lead->id; ?>" type="radio" name="call_status" onclick="$('#message_id<?php echo $lead->id; ?>').val('8'); $('#schedule_call<?php echo $lead->id; ?>').show(); $('.datepicker,.timepicker,.date_alternate,.time_alternate').val(''); $('.datepicker,.timepicker').attr('required', true);"/><label for="acblrba<?php echo $lead->id; ?>"><span><span></span></span>Call Back Later?</label>
													
												</div>												
											</div>

											<div style="display:none;" id="schedule_call<?php echo $lead->id; ?>" class="row">
												<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
													<label>Schedule Call? Date/Time</label>
												</div>
												<div class="col-lg-3 col-md-4 col-sm-4 col-xs-3">
													<input type="text" class="datepicker">  <!--<i class="fa fa-clock-o"></i>-->

													<input type="hidden" name="create_action_date" id="date_alternate" class="date_alternate">
												</div>
												<div class="col-lg-3 col-md-4 col-sm-4 col-xs-3">
													<input type="text" class="timepicker"> <input type="hidden" name="create_action_time" class="time_alternate">  <!--<i class="fa fa-clock-o"></i>-->


												</div>
											</div>
											

											<div class="row">
												<div class="col-md-2 col-sm-2 col-xs-2">
													<label>Comment</label>
												</div>
												<div class="col-md-10 col-sm-10 col-xs-10">
													<textarea name="comments" class="mention" data-leadid="<?php echo $lead->id; ?>" cols="" rows="0" placeholder="Type Here"></textarea>
												</div>
											</div>
											<input type="hidden" name="form_type" value="action_added">
											<input type="hidden" name="lead_id" value="<?php echo $lead->id ?>">
											<input type="hidden" name="update_scheduled_message_id" id="update_scheduled_message_id" class="updSchedMsg" value="0">

											<div class="col-md-12 col-sm-12 col-xs-12 text-right">
												<input type="reset" onClick="expLeadAjax('<?php echo $lead->id; ?>');" class="btn white" value="Discard">
												<input type="submit" data-toggle="modal" class="btn" value="Save" onClick="getTaggedFromLeads('Call<?php echo $lead->id ?>')">
											</div>
											
											</form>
										</div>

										<?php } ?>

										
										<div class="tab-pane" id="Email<?php echo $lead->id ?>">
										<form data-dashboard="0" data-ajaxactionleadid="<?php echo $lead->id ?>" action="<?php echo base_url();?>lead/leadAction" class="mb_form" data-callofaction="1" onsubmit="return false;">
											 <div class="row">
												<div class="col-md-2 col-sm-2 col-xs-2">
													<label>Email message</label>
												</div>
												<div class="col-md-10 col-sm-10 col-xs-10">
													<textarea name="comments" cols="" rows="0" placeholder="Send Email to Customer"></textarea>
												</div>
											</div>
											<input type="hidden" name="message_id" id="type" value="2">
											<input type="hidden" name="form_type" value="action_added">
											<input type="hidden" name="lead_id" value="<?php echo $lead->id ?>">											

											<div class="col-md-12 col-sm-12 col-xs-12 text-right">
												<input type="reset" onClick="$('#AddPlusAreaCreateAnAction<?php echo $lead->id;?>').toggle();" class="btn white" value="Discard">
												<input type="submit" data-toggle="modal" class="btn" value="Send email">
											</div>
											<script>$('#ayrba<?php echo $lead->id; ?>').click();</script>
										</form>
										</div>
										

										
										<div class="tab-pane visitEd" id="Visit<?php echo $lead->id ?>">
										<form data-dashboard="0" data-ajaxactionleadid="<?php echo $lead->id ?>" action="<?php echo base_url();?>lead/leadAction" class="mb_form" data-callofaction="1" onsubmit="return false;">
											 <div class="row">
												<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
													Type
												</div>
												<div class="col-lg-2 col-md-3 col-sm-4 col-xs-4">
													<input checked="checked" type="radio" id="typeOnsite<?php echo $lead->id ?>" name="visit_type" value="Field visit"><label for="typeOnsite<?php echo $lead->id ?>"><span><span></span></span>Field visit</label> 

												</div>
												<div class="col-lg-2 col-md-3 col-sm-4 col-xs-4">
													<input  type="radio" id="typeShowroom<?php echo $lead->id ?>" name="visit_type" value="Showroom"><label for="typeShowroom<?php echo $lead->id ?>"><span><span></span></span>Showroom</label>

												</div>
											</div>
											 <div class="row">
												<div class="col-md-2 col-sm-2 col-xs-2">
													<label>Comment</label>
												</div>
												<div class="col-md-10 col-sm-10 col-xs-10">
													<textarea name="comments" class="mention" data-leadid="<?php echo $lead->id; ?>" cols="" rows="0" placeholder="Type Here"></textarea>
												</div>
											</div>
											<input type="hidden" name="message_id" id="type" value="3">
											<input type="hidden" name="form_type" value="action_added">
											<input type="hidden" name="lead_id" value="<?php echo $lead->id ?>">
											<input type="hidden" name="tagged_ids" class="tagged_ids" value="">

											<div class="col-md-12 col-sm-12 col-xs-12 text-right">
												<input type="reset" onClick="$('#AddPlusAreaCreateAnAction<?php echo $lead->id;?>').toggle();" class="btn white" value="Discard">
												<input type="submit" data-toggle="modal" class="btn"  value="Save" onClick="getTaggedFromLeads('Visit<?php echo $lead->id ?>')">
											</div>
										
										</form>
										</div>						


										
										<div class="tab-pane <?php if($onlyComments) echo 'active'; ?>" id="Comment<?php echo $lead->id ?>">
										<form data-dashboard="0" data-ajaxactionleadid="<?php echo $lead->id ?>" action="<?php echo base_url();?>lead/leadAction" class="mb_form" data-callofaction="1" onsubmit="return false;">
											 <div class="row">
												<div class="col-md-2 col-sm-2 col-xs-2">
													<label>Comment</label>
												</div>
												<div class="col-md-10 col-sm-10 col-xs-10">
													<textarea name="comments" cols="" class="mention" data-leadid="<?php echo $lead->id; ?>" autocomplete="off" rows="0" placeholder="Type Internal Comment"></textarea>
												</div>
											</div>
											<div class="row">
												<div class="col-md-2 col-sm-2 col-xs-2">
													<label>Tag</label>
												</div>
												<div class="col-md-10 col-sm-10 col-xs-10">
													<select onchange="if(this.value==2){ $('#purchasedCarsDivs<?php echo $lead->id; ?>').show(); } else {$('#purchasedCarsDivs<?php echo $lead->id; ?>').hide();}" id="tag_id" name="tag_id">
														<option value="">Please Select</option>
														<?php foreach($tags as $tag){?>
															<option value="<?php echo $tag->id; ?>">
																<?php echo $tag->title; ?>
															</option>
														 <?php } ?>   
													</select> 
												</div>
											</div>
				
											
											
											 <div style="display:none;" class="row" id="purchasedCarsDivs<?php echo $lead->id; ?>">
                                            	<div class="col-md-2 col-sm-2 col-xs-12">                                                	
                                                	<label>Select Purchased Vehicle<span>*</span></label>
                                                </div>
                                            	<div class="col-md-4 col-sm-4 col-xs-12">
                                                	<script>
														$(function() {
															var leadCar1 = [
																 <?php foreach($vehicles_specific as $vehicle1){ ?>
																	{
																	value: "<?php echo $vehicle1->id; ?>",
																	label: "<?php echo $vehicle1->name;?>",
																	},		 
																<?php } ?>      
															];
															$( "#project1<?php echo $lead->id ?>" ).autocomplete({
															  minLength: 1,
															  source: leadCar1,
															  focus: function( event, ui ) {
																$( "#project1<?php echo $lead->id ?>" ).val( ui.item.label );
																return false;
															  },
															  select: function( event, ui ) {
																$( "#project1<?php echo $lead->id ?>" ).val( ui.item.label );
																$( "#project1-id<?php echo $lead->id ?>" ).val( ui.item.value );
																																			
											
																	$( "#project1-icon<?php echo $lead->id ?>" ).html( "<div id="+ ui.item.value +" data-id="+ ui.item.value +" class='addCarUHav specific_car'>"+ ui.item.label +"<a href='javascript:void(0);' >X</a><input type='hidden' name='purchased_vehicle_id' id='purchased_vehicle_id' value='"+ui.item.value+"'/></div>" );

																	$( "#project1<?php echo $lead->id ?>" ).val('');

																	
																	return false;
															  }
															})
															.autocomplete( "instance" )._renderItem = function( ul, item ) {
															  return $( "<li>" )
																.append( "<a>" + item.label + "</a>" )
																.appendTo( ul );
															};			
															$.ui.autocomplete.filter = function (array, term) {
															  var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
															  return $.grep(array, function (value) {
																return matcher.test(value.label || value.value || value);
															  });
															};		
														});
														$(document).on('click', '.addCarUHav a', function() {$(this).parent().remove();
															<?php if($is_sublead) echo 'count_check_number2'; else echo 'count_check_number'; ?>--;
														});
														
													  </script>
                                                    <input type="text" id="project1<?php echo $lead->id ?>" value="" placeholder="Write and Select" class="addCarNewInput" />
                                                    <input type="hidden" id="project1-id<?php echo $lead->id ?>">
                                                </div>
                                            	<div class="col-md-6 col-sm-6 col-xs-12">
                                                	<div id="project1-icon<?php echo $lead->id ?>"></div>
                                                </div>
                                            </div>
											

											
											<input type="hidden" name="message_id" id="type" value="4">
											<input type="hidden" name="form_type" value="action_added">
											<input type="hidden" name="lead_id" value="<?php echo $lead->id ?>">
											<input type="hidden" name="tagged_ids" class="tagged_ids" value="">
											
											<div class="col-md-12 col-sm-12 col-xs-12 text-right">
												<input type="reset" onClick="$('#AddPlusAreaCreateAnAction<?php echo $lead->id;?>').toggle();" class="btn white" value="Discard">
												<input type="button" data-toggle="modal" class="btn" onClick="if(($('#tag_id').val() == '2' && $('#purchased_vehicle_id').length > 0 && $('#purchased_vehicle_id').val() !='') || $('#tag_id').val() != '2'){ getTaggedFromLeadsScr('Comment<?php echo $lead->id ?>'); }else{alert('Please select purchased vehicle.');}" value="Save">
											</div>

										</form>
										</div>						


										
										<div class="tab-pane testDriveEd" id="Schedule_Test<?php echo $lead->id ?>">
										<form data-dashboard="0" data-ajaxactionleadid="<?php echo $lead->id ?>" action="<?php echo base_url();?>lead/leadAction" class="mb_form" data-callofaction="1" onsubmit="return false;">											
											<div class="row">
												<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
													Type
												</div>
												<div class="col-lg-2 col-md-3 col-sm-4 col-xs-4">
													<input class="SchdCheckBtn" onclick="$('#tdrive<?php echo $lead->id ?>').show(); $('.datepicker,.timepicker').attr('required', true); $('#changeTestDrive_<?php echo $lead->id ?>').attr('value',0);"  type="radio" id="typeScheduled<?php echo $lead->id ?>" name="test_drive_type" value="Scheduled"><label for="typeScheduled<?php echo $lead->id ?>"><span><span></span></span>Scheduled</label> 

												</div>
												<div class="col-lg-2 col-md-3 col-sm-4 col-xs-4">
													<input onclick="walkinTestDrive('<?php echo $lead->id ?>'); $('.datepicker,.timepicker').attr('required', false);" type="radio" id="typeUnscheduled<?php echo $lead->id ?>" name="test_drive_type" value="Walk in"><label for="typeUnscheduled<?php echo $lead->id ?>"><span><span></span></span>Walk in</label>

												</div>
											</div>
											<div class="row" style="display:none;" id="tdrive<?php echo $lead->id ?>">
												<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
													Date / Time
												</div>
												<div class="col-lg-2 col-md-3 col-sm-4 col-xs-4">
													<input type="text" onChange="changeButtonToUpdate('<?php echo $lead->id ?>');" class="datepicker">  <!--<i class="fa fa-clock-o"></i>-->

													<input type="hidden" name="create_action_date" id="date_alternate" class="date_alternate">
												</div>
												<div class="col-lg-2 col-md-3 col-sm-4 col-xs-4">
													<input type="text" onChange="changeButtonToUpdate('<?php echo $lead->id ?>');" class="timepicker"> <input type="hidden" name="create_action_time" class="time_alternate"> <!--<i class="fa fa-clock-o"></i>-->


												</div>
											</div>
                                            <div class="row">
                                            	<div class="col-md-2 col-sm-2 col-xs-12">                                                	
                                                	<label>Type Vehicle Name <span>*</span></label>
                                                </div>
                                            	<div class="col-md-4 col-sm-4 col-xs-12">
                                                	<script>
		//this is blogle var
		$alreadyFlag = true;
		//var $vehicleName;
		$(function() {
															var leadCar = [
																 <?php foreach($vehicles_specific as $vehicle1){ ?>
																	{
																	value: "<?php echo $vehicle1->id; ?>",
																	label: "<?php echo $vehicle1->name;?>",
																	},		 
																<?php } ?>      
															];
															$( "#project<?php echo $lead->id ?>" ).autocomplete({
															  minLength: 1,
															  source: leadCar,
															  focus: function( event, ui ) {
																$( "#project<?php echo $lead->id ?>" ).val( ui.item.label );
																return false;
															  },
															  select: function( event, ui ) {
																$( "#project<?php echo $lead->id ?>" ).val( ui.item.label );
																$( "#project-id<?php echo $lead->id ?>" ).val( ui.item.value );
																
																var sp_c_id = ui.item.value;
																//var gen_v_arr = document.getElementsByName("vehicle[]");
																//alert(sp_c_id);
																//alert(gen_v_arr.length);
																/*$(".general_veh").each(function() {
																	//console.log( this.value + ":" + this.checked );
																	var spec_car_of_general = $(this).attr('data-spcars');
																	//alert(spec_car_of_general);
																	spec_car_of_general_arr = spec_car_of_general.split(",");
																	//loop here or check in array
																	if(spec_car_of_general_arr.indexOf(sp_c_id) >= 0) //if newley added specific car has any genaral div already
																	{
																		alert('There is a general car '+$(this).attr("data-name")+'. That will replaced.');
																		$(this).parent().remove();
																		<?php if($is_sublead) echo 'count_check_number2'; else echo 'count_check_number'; ?>--;
																	}
																	
																});*/
																<?php if($is_sublead)
																{	?>																
																//	alert(count_check_number2);
																	<?php
																}
																else
																{
																	?>
																//	alert(count_check_number);
																	<?php
																}
																?>
	//new condition
	var checkFlag = 0;															
	var vehicleCount = $("#project-icon<?php echo $lead->id ?> div").length;
	var chekIfUpdate = $("#schSubm<?php echo $lead->id ?>").val();
	if(vehicleCount > 0 && chekIfUpdate == "Update"){
		var checkFlag = 1;
	}else if(vehicleCount > 0 && chekIfUpdate == "Complete"){
		var checkFlag = 1;
	}
	else{
		var checkFlag = 0;
	}
	
	if(vehicleCount <= 2 && checkFlag == 0){
	
	//old condition															
	//if (<?php //if($is_sublead) echo 'count_check_number2'; else echo 'count_check_number'; ?> <= 3 ) {
		//alert(count_check_number)
		$( "#project-icon<?php echo $lead->id ?>" ).append( "<div id="+ ui.item.value +" data-id="+ ui.item.value +" class='addCarUHav specific_car'>"+ ui.item.label +"<a href='javascript:void(0);' >X</a><input type='hidden' name='vehicle_specific_names_id[]' value='"+ui.item.value+"'/></div>" );

		$( "#project<?php echo $lead->id ?>" ).val('');

		<?php if($is_sublead) echo 'count_check_number2'; else echo 'count_check_number'; ?>++;

		changeButtonToUpdate('<?php echo $lead->id ?>');
							
					
					}
					else if(vehicleCount <= 2 && checkFlag == 1){
						alert('You can only select up to 1 vehicle for update.');
					}
					else
						{
							alert('You can only select up to 3 vehicles.');
						}	

							return false;
					  }
					})
															.autocomplete( "instance" )._renderItem = function( ul, item ) {
															  return $( "<li>" )
																.append( "<a>" + item.label + "</a>" )
																.appendTo( ul );
															};			
															$.ui.autocomplete.filter = function (array, term) {
															  var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
															  return $.grep(array, function (value) {
																return matcher.test(value.label || value.value || value);
															  });
															};		
														});
														$(document).on('click', '.addCarUHav a', function() {$(this).parent().remove();
															<?php if($is_sublead) echo 'count_check_number2'; else echo 'count_check_number'; ?>--;
														});
														
													  </script>
                                                    <input type="text" id="project<?php echo $lead->id ?>" value="" placeholder="Write and Select" class="addCarNewInput" />
                                                    <input type="hidden" id="project-id<?php echo $lead->id ?>">
                                                </div>
                                            	<div class="col-md-6 col-sm-6 col-xs-12">
                                                	<div id="project-icon<?php echo $lead->id ?>"></div>
                                                </div>
                                            </div>
											<div class="row">
												<div class="col-md-2 col-sm-2 col-xs-2">
													<label>Comment</label>
												</div>
												<div class="col-md-10 col-sm-10 col-xs-10">
													<textarea name="comments" cols="" class="updSchedComm mention" data-leadid="<?php echo $lead->id; ?>" rows="0" placeholder="Type Here" style="width:99%;"></textarea>
												</div>
											</div>
											<input type="hidden" name="message_id" id="type" value="5">
											<input type="hidden" name="form_type" value="action_added">
											<input type="hidden" name="lead_id" value="<?php echo $lead->id; ?>">	
											<input type="hidden" name="update_scheduled_message_id" id="update_scheduled_message_id" class="updSchedMsg" value="0">
											<input type="hidden" name="test_drive_completed" id="test_drive_completed" value="0">
											<input type="hidden" name="test_drive_canceled" id="test_drive_canceled" value="0">
											<input type="hidden" name="tagged_ids" class="tagged_ids" value="">
                                            <input type="hidden" id="changeTestDrive_<?php echo $lead->id ?>" name="changeTestDrive" value="0">

											<div class="col-md-12 col-sm-12 col-xs-12 text-right">
												<input type="reset" onClick="expLeadAjax('<?php echo $lead->id; ?>');" class="btn white" value="Discard">

												<input style="display:none; background-color: #d67070;" id="scheduled_canceled_btn" type="button" onClick="testDriveCanceled('<?php echo $lead->id; ?>');" class="btn white" value="Cancel">

												<input type="submit" id="schSubm<?php echo $lead->id; ?>" onclick="return isVehSel<?php echo $lead->id;?>()" data-toggle="modal" class="btn" value="Save">
	<script>
    function isVehSel<?php echo $lead->id;?>()
    {
        if($('#project-id<?php echo $lead->id ?>').val()=="") 
            {
                alert("Please select at least one vehicle.");
                return false;
            }
        else 
            {
                getTaggedFromLeads('Schedule_Test<?php echo $lead->id ?>');
                return true;
            }
    }
    </script>
											</div>
											<script>$('#typeUnscheduled<?php echo $lead->id; ?>').click();</script>
										</form>
										</div>						


									</div>

									<div class="clearfix"></div>
								</div>
							</div>
							</div>
							<?php  }  ?>					



			<?php if(!$dashboard) { //display "no response" or finish button
			?>              

				<?php //if(rights(32,'write')) { 
				//$isManager is a variable that is true if the logged in user is a manager of any department
				if(($lead->assign_to == $logged_in_user_id || $isManager) && $lead->status!=="Closed" && $lead->status!=="Approved and Archived" && $lead->status!=="Finished" && $lead->status!=="No Response") {
				?>
				<div class="btnBoxes">
					<!--<div>	
						<form data-dashboard="0" data-ajaxactionleadid="<?php echo $lead->id ?>" id="noResponseBtnFrm<?php echo $lead->id; ?>" action="<?php echo base_url();?>lead/leadAction" class="mb_form" onsubmit="return false;">
							<input type="hidden" name="form_type" value="action_added">
							<input type="hidden" name="message_id" value="6">
							<input type="hidden" name="comments" value="">
							<input type="hidden" name="lead_id" value="<?php echo $lead->id ?>">									
							<input type="button" id="noresbtn<?php echo $lead->id ?>" onClick="$('#noResponseBtnFrm<?php echo $lead->id; ?>').submit(); $(this).hide(); $('#finbtn<?php echo $lead->id ?>').hide(); $('#statusval<?php echo $lead->id; ?>').html('No Response');" value="No Response" class="btn white">
						</form>							
					</div>-->







					<div class="leadSidePUpEd" id="finishbtn<?php echo $lead->id ?>">
						
						<form data-dashboard="0" data-ajaxactionleadid="<?php echo $lead->id ?>" id="finishBtnFrm<?php echo $lead->id; ?>" action="<?php echo base_url();?>lead/leadAction" class="mb_form" onsubmit="return false;">
						
							<input type="hidden" name="lead_id" value="<?php echo $lead->id ?>">								
							<input type="hidden" value="action_added" name="form_type"/>
							<input type="hidden" name="comments" value="">
							<input type="hidden" name="message_id" value="7">
							
							<input style="padding: 0 10px;" id="finbtn<?php echo $lead->id ?>" onclick="finishBtnProc('<?php echo $lead->id ?>')" type="button" value="Finish" class="btn red" >

							<div style="display:none;" id="hnallns<?php echo $lead->id ?>" class="popupHere">

								<input type="hidden" name="not_a_lead_type" id="nalt<?php echo $lead->id ?>" value="0">
								<ul>
									<li><a href="javascript:void(0);" onclick="$('#finbtn<?php echo $lead->id ?>').prop('value', 'Finish (No Response)'); $('#nalt<?php echo $lead->id ?>').val('5');">No Response</a></li>
									<li><a href="javascript:void(0);" onclick="$('#finbtn<?php echo $lead->id ?>').prop('value', 'Finish (Not a Lead)'); $('#nalt<?php echo $lead->id ?>').val('1');">Not a Lead</a></li>
									<li><a href="javascript:void(0);" onclick="$('#finbtn<?php echo $lead->id ?>').prop('value', 'Finish (Wrong Distributor)'); $('#nalt<?php echo $lead->id ?>').val('2');">Wrong Distributor</a></li>
									<li><a href="javascript:void(0);" onclick="$('#finbtn<?php echo $lead->id ?>').prop('value', 'Finish (Trucks)'); $('#nalt<?php echo $lead->id ?>').val('3');">Trucks</a></li>
									<li><a href="javascript:void(0);" onclick="$('#finbtn<?php echo $lead->id ?>').prop('value', 'Finish (Other)'); $('#nalt<?php echo $lead->id ?>').val('4');">Other</a></li>
								</ul>															
							</div>





						</form>						
					</div>

					<div class="clearfix"></div>
															
				</div>
				<?php }   ?>	

			<?php } ?>
						
			
			
			<form data-dashboard="0" data-ajaxactionleadid="<?php echo $lead->id ?>" id="closeFrm<?php echo $lead->id; ?>" action="<?php echo base_url();?>lead/leadAction" class="mb_form" onsubmit="return false;">
				<input type="hidden" name="form_type" value="action_added">
				<input type="hidden" name="message_id" value="11">
				<input type="hidden" name="send_survey" id="send_survey<?php echo $lead->id; ?>" value="1">
				<input type="hidden" name="lead_id" value="<?php echo $lead->id ?>">
				<input type="hidden" name="comments" value="">
			</form>
			
			<?php if(!$dashboard){ // "Close and Send Survey"  "Disapprove and Send Back"  "Close Without Sending Survey"  "Approve and Archive"
			
			//if(rights(4,'write')){ 
			if( ($lead->created_by == $logged_in_user_id && $lead->assign_to != $logged_in_user_id) || $isManager || (checkLoggedInUserInboxRole() && $lead->created_by === "0")) {
			?>

				<div class="CallBarBox ApprovedandDis">

					<div class="DateTimeBox second last">
						<?php //if($lead->status==="No Response"){ 
						if((int)$lead->not_a_lead_type > 0){ 
						?>
						<script>
						$('#send_survey<?php echo $lead->id; ?>').val('0');
						</script>
						<?php } ?>

						<?php if($lead->status!=="Closed" && $lead->status!=="Approved and Archived"){
						?>
						<a class="approve" href="javascript:void(0);" onclick="if(confirm('Are you sure you want to close this lead?')) { $('#closeFrm<?php echo $lead->id; ?>').submit(); $('#duedateval<?php echo $lead->id; ?>').html('Closed'); }">
							<img src="<?php echo base_url();?>assets/images/icon-approved.png" class="LeftImgIcon" alt=""> <span id="onlyCloseTxt<?php echo $lead->id ?>" class="boderRight"><?php if((int)$lead->not_a_lead_type > 0) echo "Close"; else echo "Close and Send Survey" ?></span> 
						</a>
						<?php } ?>

					</div>

					<?php if( $lead->assign_to > 0 && $lead->status==="Finished") {
					?>
					<div class="DateTimeBox second">
						<a class="disapprove" data-toggle="collapse" href="#DisapprovedAndSndBack<?php echo $lead->id; ?>">
							<img src="<?php echo base_url();?>assets/images/icon-disapproved.png" class="LeftImgIcon" alt=""> <span class="boderRight">Disapprove and Send Back</span> 
						</a>
					</div>					
					<?php } ?>
					
					<!--<div class="DateTimeBox second">
						<a class="approve" data-toggle="collapse" href="#ApprovedAndArchive<?php echo $lead->id; ?>">
							<img src="<?php echo base_url();?>assets/images/icon-approved.png" class="LeftImgIcon" alt=""> <span class="boderRight">Approve and Archive</span> 
						</a>
					</div>-->
				
					<div class="clearfix"></div>
			   </div>

			   <div class="collapse CreateAnAction" id="DisapprovedAndSndBack<?php echo $lead->id; ?>">
				<form data-dashboard="0" data-ajaxactionleadid="<?php echo $lead->id ?>" action="<?php echo base_url();?>lead/leadAction" class="mb_form" onsubmit="return false;">
							<input type="hidden" name="form_type" value="action_added">
							<input type="hidden" name="message_id" value="14">
							<input type="hidden" name="lead_id" value="<?php echo $lead->id ?>">

					<div class="ActionHeadingBox">
						<h2><i class="fa fa-exchange"></i> Disapprove and Send Back for Completion</h2>
					</div>
					<div class="ActionTabsBox">
					  <div class="CommentHeading">
							<i class="fa fa-commenting"></i> Comment</a>
						</div>
						<div class="commenttext">
						<p>Right your comments below to notify the missing information</p>
						<div class="row">
							<div class="col-md-2 col-sm-2 col-xs-2">
								<label>Comment</label>
							</div>
							<div class="col-md-10 col-sm-10 col-xs-10">
								<textarea name="comments" cols="" rows="0" placeholder="Type Here"></textarea>
							</div>
						</div>

					   <div class="col-md-12 col-sm-12 col-xs-12 text-right">
							<input type="reset" data-toggle="collapse" href="#DisapprovedAndSndBack<?php echo $lead->id; ?>" class="btn white" value="Cancel">
							<input type="submit" class="btn" value="Disapprove" data-toggle="modal" />
					  </div>
					  
						<div class="clearfix"></div>
						</div>
					</div>
					</form>
				</div>
				
				<!--<div class="collapse CreateAnAction" id="ApprovedAndArchive<?php echo $lead->id;?>">
				<form action="<?php echo base_url();?>lead/leadAction" class="mb_form" onsubmit="return false;">
							<input type="hidden" name="form_type" value="action_added">
							<input type="hidden" name="message_id" value="">
							<input type="hidden" name="lead_id" value="<?php echo $lead->id ?>">


					<div class="ActionHeadingBox">
						<h2><i class="fa fa-exchange"></i> Approve and Archive</h2>
					</div>
					<div class="ActionTabsBox">
					  <div class="CommentHeading">
							<i class="fa fa-commenting"></i> Comment</a>
						</div>
						<div class="commenttext">
						<p>Right your comments below</p>
						<div class="row">
							<div class="col-md-2 col-sm-2 col-xs-2">
								<label>Comment</label>
							</div>
							<div class="col-md-10 col-sm-10 col-xs-10">
								<textarea name="comments" cols="" rows="0" placeholder="Type Here"></textarea>
							</div>
						</div>

						<div class="col-md-12 col-sm-12 col-xs-12 text-right">
							<input type="button" class="btn white" value="Cancel">
							<input type="submit" class="btn" value="Approved and Archive" data-toggle="modal" />
					  </div>
					  
						<div class="clearfix"></div>
						</div>
					</div>
					</form>
				</div>-->

		   <?php } } ?>
			


			<?php if(!$dashboard){ //survey block
			
			//if(rights(9,'read') || rights(9,'write')){ 
			//11/23/2016
			if(true){  //survey 9 id is to use for left menu survey management. So here the survey block will show to every one who have access to lead.
			
			if($lead->survey_id)
			{
			?>

			<div class="SurveyBox">
			<div class="row ActionWhiteBox">
			<h2><i class="fa fa-file-text"></i> Survey Response </h2>
			<?php if($lead->survey_response) { ?>
			<a title="Click here to view survey response." target="_blank" href="<?php echo base_url().'survey/leadSurveyResponse/'.$lead->survey_id.'/'.$lead->id; ?>">
				&nbsp;&nbsp;<img src="<?php echo base_url();?>assets/images/icon-surveryright.png" alt="" class="CallIcons" />
			</a>
			<?php } ?>
				
			
			</div>
			<div class="row ActionTextBox">
			<div class="col-md-7 col-sm-7 col-xs-7">
			<?php $surveyScore = surveyResult($lead->survey_id, $lead->id); 
			if($surveyScore['surveyRec']){
			?>
				<p>
					<label>
						<?php echo $surveyScore['surveyRec']->title; ?> <?php if($lead->survey_filled_by) {?><strong>(Filled by <?php echo getEmployeeName($lead->survey_filled_by); ?>)</strong><?php } ?>


					</label>
					<?php echo $surveyScore['surveyRec']->description; ?>
				</p>



			<?php } ?>
			</div>
            <?php if($lead->survey_response) { ?>
			<div class="col-md-5 col-sm-5 col-xs-5 waitingresponse">
                    
					<div title="This particular survey score." class="scoreBord"><p>Score<span><?php if($surveyScore['total_avg_by_lead'] != ''){ echo $surveyScore['total_avg_by_lead'];}else{ echo '0'; }  ?></span></p></div>
					<div title="Overall Average by others too." class="scoreBord Two"><p>Average<span><?php if($surveyScore['total_avg'] != ''){ echo $surveyScore['total_avg'];}else{ echo '0'; }  ?></span></p></div>
                    






			</div>
            <?php }else{ ?>
			

			<?php 
			if($lead->is_data_center && $CI->session->userdata['user']['role_id']==="73") { //is_data_center value is set by cronjob and 73 is the call center role id ?>
    <div style="width: auto;" class="col-md-1 col-sm-1 col-xs-5 pull-right waitingresponse takethesurvey"> <a target="_blank" href="<?php echo base_url(); ?>lead_survey/surveyAction/<?php echo $lead->survey_id;?>/<?php echo $lead->id;?>/<?php echo $lead->survey_link_rand; ?>">Take The Survey</a> </div>
    <?php } ?>
    <div style="width: auto;" class="col-md-1 col-sm-1 col-xs-5 pull-right waitingresponse"> <span>Awaiting Response</span> </div>
    <?php } ?>
			</div>

					
			</div>
                        

			<?php } } } ?>

	<?php
}

function displayMessages($messages, $logged_in_user_id, $lead, $dashboard, $howManyLatest)
{
	$grayCount = 0;
	foreach($messages as $message)
	{
		$messageType = getMessageType($message->message_id);
		
		if($dashboard) { ?>

			<?php if(!$messageType->is_white || $message->test_drive_type=="Walk in") $grayCount++; ?>
			
			<div class="row ActionWhiteBox <?php if($messageType->is_white && $message->test_drive_type!="Walk in") echo 'onlywhitedsb'; else echo 'dashboardGrayCmt';?> <?php if($grayCount===2) echo "twogray"; ?>">
					<div class="col-lg-2 col-md-3 col-sm-3 col-xs-3 Left" title="<?php echo date("d M Y", strtotime($message->created_at)); ?>">
						<label><?php echo ($logged_in_user_id === $message->created_by? "By Me" : getEmployeeName($message->created_by)); ?></label> <?php echo date("d M", strtotime($message->created_at)); echo ' '.date("h:ia", strtotime($message->created_at)); ?>
					</div>
					<div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 Right">

						<label>										
							<?php echo $messageType->title; ?>
						</label>										
						

						<?php 
						if($message->visit_type==="Field visit") { ?> 								
							We have visited the <?php echo $lead->title.' '.$lead->first_name.' '.$lead->surname; ?> in his premices.
						<?php } ?>

						<?php 
						if($message->visit_type==="Showroom") { ?> 								
							<?php echo $lead->title.' '.$lead->first_name.' '.$lead->surname; ?> has visited the showroom.
						<?php } ?>


						<?php 
						if($message->test_drive_type==="Walk in") { ?> 								
							<?php echo $lead->title.' '.$lead->first_name.' '.$lead->surname; ?> visited the showroom. <?php if($message->vehicle_specific_names_id != "") ?> Tested 


						<?php } ?>	
						
						<?php 
						if($message->test_drive_type==="Scheduled") { ?> 								
							<?php echo $lead->title.' '.$lead->first_name.' '.$lead->surname; ?> has scheduled a test drive. 
						<?php } ?>

						<?php 
						$schedule_date_time = $message->schedule_date_time; 
						if($schedule_date_time!="0000-00-00 00:00:00") { ?> 								
							(Scheduled at: <?php echo date("d M h:ia", strtotime($schedule_date_time)); ?>)
						<?php } ?>

						<?php 
						if($message->vehicle_specific_names_id != "") {
							
							$specific_cars = explode(',',$message->vehicle_specific_names_id); 
							foreach($specific_cars as $value){
								$sp_car_name = getSpecificCarName($value);
								?>
								<div class="addCarUHav">
								<?php echo $sp_car_name; ?>
								</div>
							<?php }
							
																		
						 } ?>
						<?php if(($message->message_id === '4' || $message->message_id === '9' || $message->message_id === '8' || $message->message_id === '10' || $message->message_id === '3' || $message->message_id === '5') && strpos($message->comments,"[")!==false && strpos($message->comments,"]")!==false) {
							echo str_replace( array("[","]"),array("<strong>","</strong>"),$message->comments);
						}else{
							echo nl2br($message->comments);
						} ?>						
												
					</div>

					<?php
					if($message->message_id === '8' || $message->message_id === '9' || $message->message_id === '10')
					{
						if($message->message_id==='9')
						{
							$callIcon1 = "callAns.png"; 
						}
						elseif($message->message_id==='10')
						{
							$callIcon1 = "callEnd.png";
						}
						elseif($message->message_id==='8')
						{
							$callIcon1 = "callDelay.png";
						}
						?>
						<div class="notification_dashboard_icon"><img src="<?php echo base_url();?>assets/images/<?php echo $callIcon1; ?>" title="<?php echo $messageType->title; ?>" alt="" class="LeftImgIcon" /></div>
						<?php
					}
					elseif($message->message_id === "4")
					{
						?><div class="notification_dashboard_icon"><i title="<?php echo $messageType->title; ?>" class="fa fa-commenting"></i></div><?php


					}
					elseif($message->message_id === "2")
					{
						?><div class="notification_dashboard_icon"><i title="<?php echo $messageType->title; ?>" class="fa fa-envelope"></i></div><?php


					}
					elseif($message->message_id === "3")
					{
						?><div class="notification_dashboard_icon"><i title="<?php echo $messageType->title; ?>" class="fa fa-user"></i></div><?php


					}											
					?>
			</div>

		<?php 
		}
		else{ //leads screen ?>

			<?php if($messageType->is_white && $message->test_drive_type!="Walk in") { //leads screen white ?>

				<div class="row ActionWhiteBox" id="msglog<?php echo $message->id ?>">
					<div class="col-lg-2 col-md-3 col-sm-3 col-xs-3 Left" title="<?php echo date("d M Y", strtotime($message->created_at)); ?>">
						<label><?php echo ($logged_in_user_id === $message->created_by? "By Me" : getEmployeeName($message->created_by)); ?></label> <?php echo date("d M", strtotime($message->created_at)); echo ' '.date("h:ia", strtotime($message->created_at)); ?>
					</div>
					<div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 Right">								
						<label>										
							<?php echo $messageType->title; ?> <?php if($message->test_drive_canceled) {echo '<span style="color:#e21111">(Canceled)</span>';} elseif($message->test_drive_completed) {echo '(Completed)';} ?>

							<?php if((int)$message->message_id === 7 && (int)$lead->not_a_lead_type > 0){
								//if change here then also change above in the same file
								if((int)$lead->not_a_lead_type===1) echo '(Not a Lead)';
								elseif((int)$lead->not_a_lead_type===2) echo '(Not a Lead: Wrong Distributor)';
								elseif((int)$lead->not_a_lead_type===3) echo '(Not a Lead: Trucks)';
								elseif((int)$lead->not_a_lead_type===4) echo '(Not a Lead: Other)';
								elseif((int)$lead->not_a_lead_type===5) echo '(No Response)';								
							} ?>

						</label>																					
						
						<?php 
						if($message->visit_type==="Field visit") { ?> 								
							We have visited the <?php echo $lead->title.' '.$lead->first_name.' '.$lead->surname; ?> in his premices.
						<?php } ?>

						<?php 
						if($message->visit_type==="Showroom") { ?> 								
							<?php echo $lead->title.' '.$lead->first_name.' '.$lead->surname; ?> has visited the showroom.
						<?php } ?>


						<?php 
						if($message->test_drive_type==="Walk in") { ?> 								
							<?php echo $lead->title.' '.$lead->first_name.' '.$lead->surname; ?> visited the showroom. <?php if($message->vehicle_specific_names_id != "") ?> Tested 


						<?php } ?>

						<?php 
						if($message->test_drive_type==="Scheduled") { ?> 								
							<?php echo $lead->title.' '.$lead->first_name.' '.$lead->surname; ?> has scheduled a test drive. 
						<?php } ?>

						<?php 
						$schedule_date_time = $message->schedule_date_time; 
						if($schedule_date_time!="0000-00-00 00:00:00") { ?> 								
							(Scheduled at: <?php echo date("d M h:ia", strtotime($schedule_date_time)); ?>)
						<?php } ?>

						<?php 
						if($message->vehicle_specific_names_id != "") { 
							
							$specific_cars = explode(',',$message->vehicle_specific_names_id); 
							foreach($specific_cars as $value){
								$sp_car_name = getSpecificCarName($value);
								?>
								<div class="addCarUHav">
								<?php echo $sp_car_name; ?>
								</div>
							<?php }
							
																		
						 } ?>

						<?php if(($message->message_id === '4' || $message->message_id === '9' || $message->message_id === '8' || $message->message_id === '10' || $message->message_id === '3' || $message->message_id === '5') && strpos($message->comments,"[")!==false && strpos($message->comments,"]")!==false) {
							echo str_replace( array("[","]"),array("<strong>","</strong>"),$message->comments);
						}else{
							echo nl2br($message->comments);
						} ?>	
						
						<?php 
						if($message->test_drive_type==="Scheduled" && !$message->test_drive_canceled && !$message->test_drive_completed) { ?> 

							<input type="button" onClick="loadTestDriveAction('<?php echo $message->id; ?>','<?php echo $lead->id; ?>')" class="btn scheupdbtn" value="Update" />

						<?php } ?>

						<?php 
						if($message->message_id==="8")  { ?> 

							<input type="button" onClick="loadCallBackAction('<?php echo $message->id; ?>','<?php echo $lead->id; ?>')" class="btn scheupdbtn" value="Update" />

						<?php } ?>
																		
					</div>
				</div>

			<?php }else{ //leads screen and gray ?>

				<div class="row ActionTextBox">
					<div class="col-md-12 InnerTextBox GrayBgTxt">


						<?php 
						if($message->visit_type==="Field visit") { ?> 								
							We have visited the <?php echo $lead->title.' '.$lead->first_name.' '.$lead->surname; ?> in his premises.
						<?php } ?>

						<?php 
						if($message->visit_type==="Showroom") { ?> 								
							<?php echo $lead->title.' '.$lead->first_name.' '.$lead->surname; ?> has visited the showroom.
						<?php } ?>


						<?php 
						if($message->test_drive_type==="Walk in") { ?> 								
							<?php echo $lead->title.' '.$lead->first_name.' '.$lead->surname; ?> visited the showroom. <?php if($message->vehicle_specific_names_id != "") ?> Tested 


						<?php } ?>

						<?php 
						if($message->test_drive_type==="Scheduled") { ?> 								
							<?php echo $lead->title.' '.$lead->first_name.' '.$lead->surname; ?> has scheduled a test drive. 
						<?php } ?>

						<?php 
						$schedule_date_time = $message->schedule_date_time; 
						if($schedule_date_time!="0000-00-00 00:00:00") { ?> 								
							(Scheduled at: <?php echo date("d M h:ia", strtotime($schedule_date_time)); ?>)
						<?php } ?>						

						<?php 
						if($message->vehicle_specific_names_id != "") {
							
							$specific_cars = explode(',',$message->vehicle_specific_names_id); 
							foreach($specific_cars as $value){
								$sp_car_name = getSpecificCarName($value);
								?>
								<div class="addCarUHav">
								<?php echo $sp_car_name; ?>
								</div>
							<?php }																								
						 } ?>
						 						

						 <?php if(($message->message_id === '4' || $message->message_id === '9' || $message->message_id === '8' || $message->message_id === '10' || $message->message_id === '3' || $message->message_id === '5') && strpos($message->comments,"[")!==false && strpos($message->comments,"]")!==false) {
							echo str_replace( array("[","]"),array("<strong>","</strong>"),$message->comments);
						}else{
							echo nl2br($message->comments);
						} ?>
						
						 <?php 
						if((int)$lead->purchased_vehicle_id > 0 && $message->message_id === "4" && strpos($message->comments, "Purchased a car")!==false ) {
							$sp_car_name1 = getSpecificCarName($lead->purchased_vehicle_id);
							?>
							<div class="addCarUHav">
							<?php echo $sp_car_name1; ?>
							</div>
						<?php 																								
						 } ?>

					</div>


					<div class="col-md-12 col-sm-12 col-xs-12 CallBarBox">
						<?php
						if($message->message_id === '8' || $message->message_id === '9' || $message->message_id === '10')
						{
							if($message->message_id==='9')
							{
								$callIcon1 = "callAns.png"; 
							}
							elseif($message->message_id==='10')
							{
								$callIcon1 = "callEnd.png";
							}
							elseif($message->message_id==='8')
							{
								$callIcon1 = "callDelay.png";
							}
							?>
							<img src="<?php echo base_url();?>assets/images/<?php echo $callIcon1; ?>" title="<?php echo $messageType->title; ?>" alt="" class="LeftImgIcon" />
							<?php
						}
						elseif($message->message_id === "4")
						{
							?><i title="<?php echo $messageType->title; ?>" class="fa fa-commenting"></i><?php


						}
						elseif($message->message_id === "2")
						{
							?><i title="<?php echo $messageType->title; ?>" class="fa fa-envelope"></i><?php


						}
						elseif($message->message_id === "3" && $message->visit_type==="Field visit")
						{
							?><i title="Field visit" class="fa fa-male"></i><?php


						}											
						elseif($message->message_id === "3" && $message->visit_type==="Showroom")
						{
							?><i title="Visit Showroom" class="fa fa-user"></i><?php


						}
						elseif($message->message_id === "5" && $message->test_drive_type==="Walk in")
						{
							?></i><i title="Test Drive Walk in" class="fa fa-male"></i><?php


						}
						?>

						<div class="DateTimeBox">	
							<span>
							<?php 
							if($message->message_id=='15' || $message->message_id=='17') 
								echo $messageType->title; 
							else
								echo ($logged_in_user_id != $message->created_by? getEmployeeName($message->created_by) : "By Me");
							?>
							</span>
							<span><?php echo date("j M | h:ia", strtotime($message->created_at)); ?></span> 
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			<?php } ?>




		<?php 
		} 
			 
	} //endforeach
}

function checkLoggedInUserMng()
{
	$isManager = false;
	$loggedInUserManagingDeps = getLoggedInUserManagingDeps();
	if(is_array($loggedInUserManagingDeps) && count($loggedInUserManagingDeps)>0)
	{
		$isManager = true;
	}

	if( rights(33,'read') ) //33 is lead controller
	{
		$isManager = true;
	}
	//logged in user is a submanager(lead controller)

	return $isManager;
}

?>