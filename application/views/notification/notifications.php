<script>
	$(document).ready(function() {
		$('#advSearch').DataTable();
	} );
	
</script>
<section>
	<div class="container">
    	<div class="leadsListingSec">

           <?php $menu = menu(); 
		   if($menu)
		   {
		   ?>
		   <div class="addNewLeads">
            <div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo $menu; ?>
                    </ul>
                </div>
            </div>
			<?php } ?>

        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i class="fa fa-bell"></i> Notifications</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>            	
            </div>
            <div class="createNewLeadSec">
            	<div class="notificationTable">
                	<table id="advSearch" class="table table-condensed table-hover" cellspacing="0" width="100%">
						<thead>
                            <tr>
                            	<th>Track ID</th>								
                                <th>Notification Message</th>
								<th>Customer Name</th>
								<th>Vehicles</th>
                                <th>Date Time</th>
                                <!--<th>Action Type</th> -->
                                <th>Go To Lead</th>
                            </tr>
                        </thead>
                    	<tbody>
						<?php $notificationLeads = getLeadsWithNotifications(); 
								if($notificationLeads) foreach( $notificationLeads as $notificationLead )
								{
								?>
									<tr>
										<td><a href="javascript:void(0);" onclick="notificationRead('<?php echo $notificationLead->id;  ?>', '<?php echo $notificationLead->lead_id ?>');"><?php echo $notificationLead->trackid; ?></a></td>										
										<td>
											<a href="javascript:void(0);" onclick="notificationRead('<?php echo $notificationLead->id;  ?>', '<?php echo $notificationLead->lead_id ?>');">
												<?php echo $notificationLead->comments ?>
											</a>
										</td>
										
										<td><a href="javascript:void(0);" onclick="notificationRead('<?php echo $notificationLead->id;  ?>', '<?php echo $notificationLead->lead_id ?>');"><?php echo trim($notificationLead->title).' '.trim($notificationLead->first_name).' '.trim($notificationLead->surname); ?></a></td>
										
										<td><a href="javascript:void(0);" onclick="notificationRead('<?php echo $notificationLead->id;  ?>', '<?php echo $notificationLead->lead_id ?>');"><?php echo getSpeVehiclesByLeadId($notificationLead->leadid); ?></a></td>

										<!--<td>Gen Manager</td>-->
										<td><?php echo date("d F H:i a", strtotime($notificationLead->created_at)); ?></td>										
										<!--<td><a href="javascript:void(0);"><i><img src="<?php echo base_url();?>assets/images/trashCan.png" alt="trash" height="20" width="40" /></i></a></td>-->
										<td><a onclick="notificationRead('<?php echo $notificationLead->id;  ?>', '<?php echo $notificationLead->lead_id ?>');" target="_blank" href="javascript:void(0);"><i><!--<img src="<?php echo base_url();?>assets/images/forward_transparent.png" alt="trash" height="20" width="40" />-->Go To Lead</i></a></td>
									</tr>
						<?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>            
        </div>
    </div>
</section>