<section>
	<div class="container">
    
    	<div class="leadsListingSec paddingNone">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo menu();?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i class="fa fa-file-text"></i> Upload Survey Management</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>            	
            </div>
            <form action="<?php echo base_url();?>upload_service_surveys/upload" method="post" enctype="multipart/form-data">  
            <div class="createNewLeadSec">
                <div class="survey_inner">
                	<div class="survey_innerHeading">
                    	<div class="row">
                        	<div class="col-md-5 col-sm-4">
                            	<h2><img src="<?php echo base_url();?>assets/images/edit.png" alt="edit" height="18" width="18" />Upload Excel</h2>
                            </div>
                            <div class="col-md-7 col-sm-8">                            
                            </div>
                        </div>
						<?php if(isset($msg)){ ?>
						<div class="row">
                        	<div class="col-md-12">
                            	<h3><?php echo $msg; ?></h3>
                            </div>
                        </div>
						<?php } ?>
                    </div>    
                        <p></p>    
                      
                    <div class="InputFieldsSurvey">                       
                        <div class="row">                        
                            <div class="col-md-5">
                                <label>Choose File</label>
                                    <input type="file" name="service_survey_file"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="questionAir">
                <div class="buttonSec">
                    <input type="button" value="Discard" onclick="history.go(-1);" class="btn standardEd white cancel_button"/>
                    <input type="submit" value="Upload" class="btn standardEd"/>
                </div>
               
                <div class="clearfix"></div>
            </div>
           </form> 
        </div>
    </div>
</section>
