<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"  user-scalable=0;>
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">	
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">	
    <link href="<?php echo base_url();?>assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" media="all">	
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" media="all">
	<title>Leads System</title>
</head>
<body id="LoginPage">
<header>
	<div class="container">
    	<div class="loginLogo"><img src="<?php echo base_url();?>assets/images/logo1.png?v=2" alt="logo" width="60" /></div>
    </div>
</header>
<section>
	<div class="container">
    	<div class="loginSec">
        	<div class="row">
                <div class="col-sm-6">
                    <h1>Leads System</h1>
                    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lobortis, erat in aliquam vestibulum, tortor nisl ullamcorper diam, quis dapibus velit urna et nulla. Cras tincidunt diam sed congue egestas. Nullam vel interdum metus.</p>-->
                </div>
                <div class="col-sm-6">
            	<form action="<?php echo base_url();?>admin/login" method="post" onsubmit="return false;" id="mb_form_login">
                	<h2>Login</h2> 
                    <label>Username</label>
                    <input type="email" name="email" value="" required />
                    
                    <label>Password</label>
                    <div class="buttonAbsolute">
                    	<input type="password" name="password" value="" required />
                        <input type="submit" class="btn" value="Sign in" />
                    </div>
                    <a href="javascript:void(0);" data-toggle="modal" data-target=".leadCreateSussesslogin" id="show_login_messge" >Forgot your password?</a>
                    <div class="clear-fixed"></div>
                </form>
                <input type="hidden" value="Create" class="btn" data-toggle="modal" data-target=".leadCreateSussess" id="show_success_messge" /><!-- just to show success message bootstrap logic -->  
            </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade leadCreateSussess" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header"> 
        	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">x</span></button> 
            <h4 id="mySmallModalLabel" class="modal-title"></h4> 
         </div> 
         <div class="modal-body">
         	<p id="message"></p>
            <p><strong></strong></p>
            <div class="text-center"><button type="button" class="btn" data-dismiss="modal">Dismiss</button></div>
         </div> 
     </div>
  </div>
</div>

<footer>
	<div class="container"><span class="login_fo_cp">Leads System © 2019</span></div>
</footer>  
<script>
    var base_url = '<?php echo base_url(); ?>';
</script>          
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>


<div class="modal fade leadCreateSussesslogin" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabellogin">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header"> 
        	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">x</span></button> 
            <h4 id="mySmallModalLabellogin" class="modal-title">Forgot Password</h4> 
         </div> 
         <div class="modal-body">
         <form action="<?php echo base_url(); ?>admin/forgotPassword" class="mb_form" onsubmit="return false;">
            <label>Email</label>
         	<input type="email" name="email" value="" placeholder="Please Enter Email" required />
            <p><strong></strong></p>
            <div class="text-center"><button type="submit" class="btn" >Send</button></div>
         </form>
         </div> 
     </div>
  </div>
</div>
</body>
</html>
