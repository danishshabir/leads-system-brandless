<?php $noOfCol = count($top_level_deps); 
	$minWith = $noOfCol * 242;
?>

<?php
if(isset($loadonlystruc) && $loadonlystruc==false)
{
?>
	<section>
	<div class="container">
			<div class="leadsListingSec">
				<div class="addNewLeads">
					<div class="standardEdBtn dropdown">
						<a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
						<ul class="dropdown-menu" aria-labelledby="dLabel">
							<?php echo menu(); ?>
						</ul>
					</div>
				</div>
				<div class="leadsListingHead">
					<div class="row">
						<div class="col-sm-6">
							<h1><i><img src="<?php echo base_url();?>assets/images/org_str.png" alt="Org" height="21" width="21" /></i> Organization Structure</h1>
						</div>
						<div class="col-sm-6">&nbsp;</div>
				   </div>            	
				</div>
				
				<div class="createNewLeadSec">
				 <!--<div class="dropdown">
					<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Add Structure
				   <span class="caret"></span></button>
					<div class="dropdown-menu">
						<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
						<input type="hidden" name="form_type" value="add_structure">
							<h5>Add Structure</h5>
							<label>Add structure title<span>*</span></label>
							<input type="text" name="title" value="">
							<div class="btns">
						   
								<input class="btn standardEd" type="submit" data-toggle="modal" value="Add">
							</div>
						</form>
					</div>
				   </div>-->                
					<p></p>
					<div class="orgBtnSec">
						<!--<div class="dropdown pull-left">
							<button data-toggle="dropdown" type="button" class="btn btn-primary dropdown-toggle" aria-expanded="false">Add Structure<span class="caret"></span></button>
							<div class="dropdown-menu">
								<form class="mb_form" action="<?php echo base_url();?>organization_structure/orgAction">
									<input type="hidden" value="add_structure" name="form_type">
									<label>Select Structure</label>
									<select>
										<option>One</option>
										<option>One</option>
										<option>One</option>
									</select>
									<label>Enter Empoyee Name </label>
									<input type="text"  />
									<div class="btns">                                    
										<input type="submit" value="Add" data-toggle="modal" class="btn standardEd">
									</div>
								</form>
							</div>
						</div>-->
						
						<!--
						<div class="dropdown pull-right">
							<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Add Department
							<span class="caret"></span></button>
							<div class="dropdown-menu">
								<form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
								<input type="hidden" name="form_type" value="add_department">
									<h5>Add Structure</h5>
									<label>Add department title<span>*</span></label>
									<input type="text" name="title" value="">
									<label>Select Structure<span>*</span></label>
									<select name="structure_id">
									<?php foreach($structures as $structure){ ?> 
										  
										<option value="<?php echo $structure->id;?>"><?php echo $structure->title; ?></option>
										
										
									<?php } ?>	
									</select>
									<label>Select Structure<span>*</span></label>
									<select name="parent_id">
									<option value="0">Top Level Department</option>
									<?php foreach($first_single_dep as $department){ ?> 
										  
										<option value="<?php echo $department->id;?>"><?php echo $department->title; ?></option>
										
										
									<?php } ?>	
									</select>
									
									<div class="btns">
								   
										<input class="btn standardEd" type="submit" data-toggle="modal" value="Add">
									</div>
								</form>
							</div>
						</div>
						-->
						
						<div class="clearfix"></div>
					</div>
				</div>
				
				<?php 
				struc($first_single_dep,$top_level_deps,$minWith,$users,$branches);
				?>
				
			</div>
		</div>
	</section>

	<!-- ajax fills this-->
	<span id="nestedPops">
		
	</span>


<?php 
}else {

	//for ajax of load more in popup on this function output is needed and above html is not needed
	struc($first_single_dep,$top_level_deps,$minWith,$users,$branches);

} ?>






