<section>
	<div class="container">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo menu(); ?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i><img src="<?php echo base_url();?>assets/images/org_str.png" alt="Org" height="21" width="21" /></i> Organization Structure</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>            	
            </div>
            <div class="createNewLeadSec">
            	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lobortis, erat in aliquam vestibulum, tortor nisl ullamcorper diam, quis dapibus velit urna et nua. Cras tincidunt diam congue null egestas. Nullam vel interdum metus.</p>
            </div>
            <div class="org_strSec">
                <div class="org_strSecInner">
                    <div class="RoleOrgSec">
                        <div class="imgBox"><img src="<?php echo base_url();?>assets/images/ceoImage.png" alt="Role" height="42" width="42" /></div>
                        <div class="clearfix"></div>
						<?php if(isset($ceo_not_exist)){?>
							<a  href="#"> <?php echo $ceo_not_exist; ?> </a>
						<?php }else{ ?>
							<a target= "_blank" href="<?php echo base_url();?>user/view/<?php echo $ceo->id;?>"> <?php echo $ceo->full_name; ?> </a>
						<?php } ?>
                       
                    </div>
                    <div class="row borderLineEd">
					
					<?php if(!empty($managers))
                        { 
                             $i=0;
							 $row_check = 0;
							 $count_rows = 0;
                             if(count($managers)%4 != 0)
							 {
								$count_rows = (floor(count($managers)/4)); 
							 }else
							 {
								 $count_rows = (floor(count($managers)/4)) - 1; 
							 }
							
							 foreach($managers as $user){
                               if($user->is_branch_manager == 1){ 
                                
                                ?>
					<?php if($i%4== 0 && $i != 0){ $row_check++;?>
						  </div>
						  <div class="row borderLineEd <?php if($row_check == $count_rows) { echo 'lastBorderNon';}?>">
					<?php } ?>
                           <div class="col-md-3 <?php if((count($managers)- 1) == $i) { echo 'topLast';}   ?>">
                            <div class="bigBoxOS <?php echo ($i == 0 ? 'topFirst' : 'first'); ?>">
                                <h1>Manager</h1>
                                <div class="imgBox"><img src="<?php echo base_url();?>assets/images/upperEmpImg.png" alt="icon" height="32" width="32" /></div>
                                <h2><?php echo $user->full_name;?></h2>
                                <h3>Manager</h3>
                                <div class="clearfix"></div>
                                <h4><!--Employees --></h4>
                               <!-- <ul>
                                	<li><a href="javascript:void(0)"><div class="imgBox"><img src="<?php echo base_url();?>assets/images/admin.png" alt="icon" height="32" width="32" /></div></a></li>
                                	<li><a href="javascript:void(0)"><div class="imgBox"><img src="<?php echo base_url();?>assets/images/admin.png" alt="icon" height="32" width="32" /></div></a></li>
                                	<li><a href="javascript:void(0)"><div class="imgBox"><img src="<?php echo base_url();?>assets/images/admin.png" alt="icon" height="32" width="32" /></div></a></li>
                                </ul> -->
                                <div class="addEmpSec dropdown">
                                    <button id="dLabel"></button>
                                    <div class="dropdown-menu">
                                        <form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
										<input type="hidden" name="branch_id" value="<?php echo $user->branch_id; ?>">
										<input type="hidden" name="parent_id" value="<?php echo $user->id; ?>">
										<input type="hidden" name="previous_branch_manger" value="<?php echo $user->id; ?>"> 
										<input type="hidden" name="form_type" value="moveuser">
                                        	<h5>Add Employee</h5>
                                            <label>Select Employee from List<span>*</span></label>
                                            <select name="user_id">
											<?php foreach($users as $other_branch_user){ 
                                                   if($other_branch_user->parent_id != $user->id && $other_branch_user->is_ceo != 1 && $other_branch_user->id != 1 && $other_branch_user->is_branch_manager != 1){ ?>
                                            	<option value="<?php echo $other_branch_user->id;?>"><?php echo $other_branch_user->full_name; ?></option>
                                            	
												
											<?php } } ?>	
                                            </select>
											<!--<label>Branch Manager<span>*</span></label>
											<select name="is_branch_manager">
											
                                            	<option value="0">No</option>
												<option value="1">Yes</option>
                                            	
                                            </select> -->
                                            <div class="btns">
                                            	<!--<input type="submit" class="btn standardEd white" value="Discard"> -->
                                                <input class="btn standardEd" type="submit" data-toggle="modal" value="Add">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                               <!-- <div class="standardEdBtn dropdown">
                                    <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a href="javascript:void(0);" data-toggle="modal" data-target="#orgStrPopUpEdit">Edit Role</a></li>
                                        <li><a href="javascript:void(0);" data-toggle="modal" data-target="#orgStrPopUpcreate">Delete Employees</a></li>
                                        <li><a href="javascript:void(0);">Delete Role</a></li>
                                    </ul>
                                </div> -->
                                <div class="clearfix"></div>
                            </div>
							
                              
							
							<div class="bigBoxOS">
                                <h1>Employees</h1>
                                <!--<div class="imgBox"><img src="<?php echo base_url();?>assets/images/admin.png" alt="icon" height="32" width="32" /> </div>-->
                                <h2><!--User Name --></h2>
                                <h3><!--Branch Manager --></h3>
                                <div class="clearfix"></div>
                                <h4>Employees</h4>
                                <ul>
								<?php 
								$show_remove_button = 0;
								foreach($users as $sub_user){ 
								
							   if($user->id == $sub_user->parent_id && $sub_user->is_branch_manager != 1 ){ 
							      $show_remove_button = 1;
							   ?>
                                	<li><a target="_blank" href="<?php echo base_url();?>user/view/<?php echo $sub_user->id;?>"><div class="imgBox"><img src="<?php echo base_url();?>assets/images/employImage.png" alt="icon" title="<?php echo $sub_user->full_name; ?>" height="32" width="32" /></div></a></li>
                                	
                                <?php } } ?>
								</ul>
                                <!-- <div class="addEmpSec dropdown">
                                    <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="dropdown-menu" aria-labelledby="dLabel">
                                        <form action="<?php echo base_url();?>organization_structure/orgAction" class="mb_form">
										<input type="hidden" name="branch_id" value="<?php echo $user->branch_id; ?>">
										<input type="hidden" name="parent_id" value="<?php echo $user->id; ?>">
										<input type="hidden" name="previous_branch_manger" value="<?php echo $user->id; ?>"> 
										<input type="hidden" name="form_type" value="moveuser">
                                        	<h5>Add Employee</h5>
                                            <label>Select Employee from List<span>*</span></label>
                                            <select name="user_id">
											<?php foreach($users as $other_branch_user){ 
                                                   if($other_branch_user->branch_id != 0){ ?>
                                            	<option value="<?php echo $other_branch_user->id;?>"><?php echo $other_branch_user->full_name; ?></option>
                                            	
												
											<?php } } ?>	
                                            </select>
											<label>Branch Manager<span>*</span></label>
											<select name="is_branch_manager">
											
                                            	<option value="0">No</option>
												<option value="1">Yes</option>
                                            	
                                            </select>
                                            <div class="btns">
                                            	<input type="submit" class="btn standardEd white" value="Discard">
                                                <input class="btn standardEd" type="submit" data-toggle="modal" value="Add">
                                            </div>
                                        </form>
                                    </div>
                                </div>-->
								<?php if($show_remove_button === 1){ ?>
                               <div class="standardEdBtn dropdown">
                                    <button id="dLabel<?php echo $user->id; ?>" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <ul class="dropdown-menu" aria-labelledby="dLabel<?php echo $user->id; ?>">
                                        <li><a href="javascript:void(0);" data-toggle="modal" data-target="#orgStrPopUpEdit<?php echo $user->id; ?>">Remove Users</a></li>
                                        <!--<li><a href="javascript:void(0);" data-toggle="modal" data-target="#orgStrPopUpcreate">Delete Employees</a></li>
                                        <li><a href="javascript:void(0);">Delete Role</a></li> -->
                                    </ul>
                                </div> 
								<?php } ?>
                                <div class="clearfix"></div>
                            </div>
								
                        </div><!-- Column 1 -->
						
						<?php $i++; } } } ?>

					   
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php foreach($managers as $manager){ ?>
<div class="modal fade orgStrPopUp" id="orgStrPopUpEdit<?php echo $manager->id; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel<?php echo $manager->id;?>">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header"> 
        	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">x</span></button> 
            <h4 id="mySmallModalLabel<?php echo $manager->id;?>" class="modal-title">Remove Users</h4> 
         </div> 
         <div class="modal-body">
         	<p>You can remove employee from here</p>
            <form action="javascript:void(0);" method="get">
            	<!--<label>Role Title<span>*</span></label> -->
                 <ul class="editRoleList">
                	<?php foreach($users as $sub_user){ ?>
					<?php if($manager->id == $sub_user->parent_id && $sub_user->is_branch_manager != 1 ){?>
					<li>
                    	<h2><?php echo $sub_user->full_name; ?></h2>
                        <!--<h3>Standard User</h3> -->
                        <div class="close" onclick="removeUserFromStr('<?php echo $sub_user->id; ?>');">X</div>
                    </li>
                	<?php } } ?>
                </ul>
               <div class="row">
                	<!--<div class="col-md-12 text-right">
                    	<input type="submit" class="btn standardEd" value="Save"/>
                    </div>  -->
                </div> 
            </form>
         </div> 
     </div>
  </div>
</div>
<?php } ?>
<script>
  function pagination(page_num){
    window.location.href = "<?php echo base_url();?>organization_structure/index/"+page_num;
  }

</script>