<section>
	<div class="container">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo menu();?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i class="fa fa-user"></i> Role Management</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>            	
            </div>
            <div class="createNewLeadSec">
            	
            	
                <ul class="systemSettingPg noBorder">
                	<li class="heading">Role Groups</li>
                	<?php $i=1;$k=1;
					 foreach($roles as $role){?>
                    <li id="<?php echo $role->id; ?>">
                    	<div class="row">
                        	<div class="col-md-8 col-sm-6 col-xs-12"><p><?php echo $i; ?>. <?php echo $role->title;?></p></div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-right">
                            	
								<?php if($role->id != 1) { ?> 
                                 <?php if(rights(11,'update')){ ?>
                                <a class="eidtField" data-toggle="collapse" href="#roleGroup_<?php echo $i;?>" id="roleGroupEditHref_<?php echo $i;?>" aria-expanded="false" aria-controls="roleGroup"><i class="fa fa-pencil"></i></a><?php } ?>
                            	 <?php if(rights(11,'delete')){ ?>
                                <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $role->id; ?>' , 'role/delete' , '');"><i class="fa fa-trash"></i></a>
                            	<?php } }?>
                                
                            </div>
                        </div>
                        <div class="leadCatDDown roleMangSec collapse" id="roleGroup_<?php echo $i;?>">
                        	<form action="<?php echo base_url();?>role/save" method="post" class="mb_form" onsubmit="return false;">
                             <input type="hidden" name="role_id" value="<?php echo $role->id; ?>" />
                                <ul>
                                	<li>
                                    	<div class="row">
                                        	<div class="col-md-4">
                                            	<label>Title of Group</label>
                                                <input placeholder="Write" type="text" value="<?php echo $role->title;?>" name="title"/>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="hidden-xs">
                                    	<h3>Select Rights for group members</h3>
                                        <div class="row">
                                        	<div class="col-sm-6"><label>Right Title</label></div>
                                            <div class="col-sm-6">
                                            	<ol>
                                                	<li><label>Read</label></li>
                                                	<li><label>Write</label></li>
                                                	<li><label>Edit</label></li>
                                                	<li><label>Delete</label></li>
                                                	<!--<li><label>Email</label></li>
                                                	<li><label>Print</label></li>-->
                                                </ol>
                                            </div>
                                        </div>
                                    </li>
                                    <?php 

									$rArr = array(1,33,34,15,11,8,6,7,9,13,14,16,17,18,19,20,21,22,23,24,25,26,5,35,36,41); //section ids for which the checkbox 
									$wArr = array(1,2,27,28,29,30,31,11,8,6,9,14,5,37,38,39,40); //section ids for which the checkbox 
									$eArr = array(1,11,8,6,9,14,5); //section ids for which the checkbox 
									$dArr = array(1,11,8,6,9,14,5); //section ids for which the checkbox

									foreach($sections as $section){
									$sid = $section->id;
									$rights = getRights($section->id,$role->id);
									if($rights)
									{
										if($rights->read == 1)
										{
											$read = "checked";
										}else
										{
											$read = "";
										}
										if($rights->write == 1)
										{
											$write = "checked";
										}else
										{
											$write = "";
										}
										if($rights->update == 1)
										{
											$update = "checked";
										}else
										{
											$update = "";
										}
										if($rights->delete == 1)
										{
											$delete = "checked";
										}else
										{
											$delete = "";
										}
										if($rights->email == 1)
										{
											$email = "checked";
										}else
										{
											$email = "";
										}
										if($rights->print == 1)
										{
											$print = "checked";
										}else
										{
											$print = "";
										}
									}else
									{
										$read = '';
										$write = '';
										$update = '';
										$email = '';
										$print = '';
										$delete = '';
									}
										
									?>
                                    <li>
                                        <div class="row">
                                        	<div class="col-sm-6" title="<?php echo $section->tooltip; ?>"><?php echo $section->title;?></div>
                                            <div class="col-sm-6">
                                            	<ol>
                                                <input type="hidden" name="section[]" value="<?php echo $section->id; ?>" />
                                                	
													<li>
													<?php if(in_array($sid,$rArr)){ ?>
														<label class="visible-xs">Read</label><input id="row_<?php echo $k;?>_<?php echo $k;?>_Read" type="checkbox" name="read<?php echo $section->id;?>" value="1" <?php echo $read;?>><label for="row_<?php echo $k;?>_<?php echo $k;?>_Read"><span></span></label>
													<?php }else{ ?>
														<input class="emptyrcb" disabled type="checkbox"><label><span></span></label>
													<?php } ?>
													</li>
                                                	
													
													<li>
													<?php if(in_array($sid,$wArr)){ ?>
														<label class="visible-xs">Write</label><input id="row_<?php echo $k;?>_<?php echo $k;?>_Write" type="checkbox" name="write<?php echo $section->id;?>" value="1" <?php echo $write;?> ><label for="row_<?php echo $k;?>_<?php echo $k;?>_Write"><span></span></label>
													<?php }else{ ?>
														<input class="emptyrcb" disabled type="checkbox"><label><span></span></label>
													<?php } ?>
													</li>
                                                	
													<li>
													<?php if(in_array($sid,$eArr)){ ?>
														<label class="visible-xs">Edit</label><input id="row_<?php echo $k;?>_<?php echo $k;?>_Edit" type="checkbox" name="update<?php echo $section->id;?>" value="1" <?php echo $update;?> ><label for="row_<?php echo $k;?>_<?php echo $k;?>_Edit"><span></span></label>
													<?php }else{ ?>
														<input class="emptyrcb" disabled type="checkbox"><label><span></span></label>
													<?php } ?>
													</li>
                                                	
													<li>
													<?php if(in_array($sid,$dArr)){ ?>
														<label class="visible-xs">Delete</label><input id="row_<?php echo $k;?>_<?php echo $k;?>_Delete" type="checkbox" name="delete<?php echo $section->id;?>" value="1" <?php echo $delete;?>><label for="row_<?php echo $k;?>_<?php echo $k;?>_Delete"><span></span></label>
													<?php }else{ ?>
														<input class="emptyrcb" disabled type="checkbox"><label><span></span></label>
													<?php } ?>
													</li>

                                                	<!--<li><label class="visible-xs">Email</label><input id="row_<?php echo $k;?>_<?php echo $k;?>_Email" type="checkbox" name="email<?php echo $section->id;?>" value="1" <?php echo $email;?>><label for="row_<?php echo $k;?>_<?php echo $k;?>_Email"><span></span></label></li>
                                                	<li><label class="visible-xs">Print</label><input id="row_<?php echo $k;?>_<?php echo $k;?>_Print" type="checkbox" name="print<?php echo $section->id;?>" value="1" <?php echo $print;?> ><label for="row_<?php echo $k;?>_<?php echo $k;?>_Print"><span></span></label></li>-->
                                                </ol>
                                            </div>
                                        </div>
                                    </li>
                                    <?php  $k++; } ?>
                                </ul>
                                <div class="saveChangSec">
                                <?php if($role->id != 1) { ?> 
                                    <input type="reset" onclick="$('#roleGroupEditHref_<?php echo $i;?>').click();" value="Cancel Changes" class="btn standardEdBtn"  />
                                    <input type="submit" value="Save" class="btn" />
                                <?php } ?>
                                </div>
                            </form>
                        </div>
                    </li>
                    <?php $i++; } ?>
                     <?php if(rights(11,'write')){ ?>
                    <li class="addMore">
                    	<a data-toggle="collapse" href="#AddmoreArea"><i class="fa fa-plus"></i> Add more</a>
                        <div class="collapse leadCatDDown" id="AddmoreArea">
                        	<form action="<?php echo base_url();?>role/saveRole" method="post">
								<input name="title" type="text" placeholder="Enter role title" required>
                                <input type="submit" value="Save" class="btn" />
							</form>	                                
                        </div>
                    </li>
                    <?php } ?>
                </ul>
                <div class="clearfix"></div>
            </div>            
        </div>
    </div>
</section>