<section>
	<div class="container">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                       <?php echo menu(); ?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i class="fa fa-file-text"></i> System Settings</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>            	
            </div>
            <div class="createNewLeadSec">
            	<ul class="systemSettingPg">
                	<li class="heading">Lead Categories</li>
                	<?php  
					$i=1;
					foreach( $categories as $category ) { ?>
                   	 <li style="border-left:5px solid <?php echo $category->color; ?>;" id="<?php echo 'cat'.$category->id; ?>">
                    	<div class="row">
                        	<div class="col-md-8 col-sm-6 col-xs-12"><p><strong><?php echo $i;?>. <?php echo $category->title.'  '.'('.$category->lead_code.')';?> </strong></p></div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-right">
                            	<a class="eidtField" data-toggle="collapse" href="#leadCatDDownFun_cat<?php echo $category->id; ?>" aria-expanded="false" aria-controls="leadCatDDownFun"><i class="fa fa-pencil"></i></a>
                            	
								<?php if($category->id !== "1" ){ ?>
								<a href="javascript:void(0);" onclick="deleteRecordSystemScreen('<?php echo $category->id; ?>' , 'system_settings/deleteCategory' , '','<?php echo 'cat'.$category->id; ?>');"><i class="fa fa-trash"></i></a>
								<?php }else { ?>

								<a href="javascript:void(0);" title="This category can't be deleted, its linked with the online forms" onclick=""><i class="fa fa-trash"></i></a>

								<?php } ?>

                            </div>
                        </div>
                        <div class="leadCatDDown collapse" id="leadCatDDownFun_cat<?php  echo $category->id; ?>">
                        	<form onsubmit="return false;" class="mb_form" action="system_settings/updateCategory">
                               <input type="hidden" name="id" value="<?php echo $category->id; ?>" />
                               <input type="hidden" name="old_color" value="<?php echo $category->color; ?>" />
                               <input type="hidden" name="old_email_subject" value="<?php //echo $category->email_subject; ?>" />
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <label>Title<span>*</span></label>
                                        <input type="text" placeholder="Title" name="title" value="<?php echo $category->title;?>"/>
                                    </div>
                                    <!--<div class="col-md-3 col-sm-6">
                                        <label>Time limit Hours<span>*</span></label>										
                                        <input type="text" placeholder="Hours" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="time_limit" value="<?php echo $category->time_limit;?>"/>
                                    </div>-->
                                    <div class="col-md-8 col-sm-12 colorSelectionCode">
                                        <label class="Title">Select Color<span>*</span></label>
                                        <ul>
                                        	<!-- <input type="radio" /> -->
                                            <li <?php echo ($category->color == '#9cc63c' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>1<?php echo $category->id;?>" type="radio" name="color" value="#9cc63c" <?php echo ($category->color == '#9cc63c' ? 'checked="echecked"': '');?> ><label for="radio<?php echo $category->id;?>1<?php echo $category->id;?>"><span><span style="background-color:#9cc63c;"></span></span></label>                                            	
                                            </li>
                                            <li <?php echo ($category->color == '#c6b55b' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>2<?php echo $category->id;?>" type="radio" name="color" value="#c6b55b" <?php echo ($category->color == '#c6b55b' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>2<?php echo $category->id;?>"><span><span style="background-color:#c6b55b;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#c79b42' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>3<?php echo $category->id;?>" type="radio" name="color" value="#c79b42" <?php echo ($category->color == '#c79b42' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>3<?php echo $category->id;?>"><span><span style="background-color:#c79b42;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#c6772c' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>4<?php echo $category->id;?>" type="radio" name="color" value="#c6772c" <?php echo ($category->color == '#c6772c' ? 'checked="echecked"': '');?> ><label for="radio<?php echo $category->id;?>4<?php echo $category->id;?>"><span><span style="background-color:#c6772c;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#8169c7' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>5<?php echo $category->id;?>" type="radio" name="color" value="#8169c7" <?php echo ($category->color == '#8169c7' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>5<?php echo $category->id;?>"><span><span style="background-color:#8169c7;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#744ebb' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>6<?php echo $category->id;?>" type="radio" name="color" value="#744ebb" <?php echo ($category->color == '#744ebb' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>6<?php echo $category->id;?>"><span><span style="background-color:#744ebb;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#666bc6' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>7<?php echo $category->id;?>" type="radio" name="color" value="#666bc6" <?php echo ($category->color == '#666bc6' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>7<?php echo $category->id;?>"><span><span style="background-color:#666bc6;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#215bad' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>8<?php echo $category->id;?>" type="radio" name="color" value="#215bad" <?php echo ($category->color == '#215bad' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>8<?php echo $category->id;?>"><span><span style="background-color:#215bad;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#c21c20' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>9<?php echo $category->id;?>" type="radio" name="color" value="#c21c20" <?php echo ($category->color == '#c21c20' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>9<?php echo $category->id;?>"><span><span style="background-color:#c21c20;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#c7461e' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>10<?php echo $category->id;?>" type="radio" name="color" value="#c7461e" <?php echo ($category->color == '#c7461e' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>10<?php echo $category->id;?>"><span><span style="background-color:#c7461e;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#45b6de' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>11<?php echo $category->id;?>" type="radio" name="color" value="#45b6de" <?php echo ($category->color == '#45b6de' ? 'checked="echecked"': '');?> ><label for="radio<?php echo $category->id;?>11<?php echo $category->id;?>"><span><span style="background-color:#45b6de;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#259acf' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>12<?php echo $category->id;?>" type="radio" name="color" value="#259acf" <?php echo ($category->color == '#259acf' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>12<?php echo $category->id;?>"><span><span style="background-color:#259acf;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#8aae4a' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>13<?php echo $category->id;?>" type="radio" name="color" value="#8aae4a" <?php echo ($category->color == '#8aae4a' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>13<?php echo $category->id;?>"><span><span style="background-color:#8aae4a;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#56a532' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>14<?php echo $category->id;?>" type="radio" name="color" value="#56a532" <?php echo ($category->color == '#56a532' ? 'checked="echecked"': '');?> ><label for="radio<?php echo $category->id;?>14<?php echo $category->id;?>"><span><span style="background-color:#56a532;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#689727' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>15<?php echo $category->id;?>" type="radio" name="color" value="#689727" <?php echo ($category->color == '#689727' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>15<?php echo $category->id;?>"><span><span style="background-color:#689727;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#1e874f' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>16<?php echo $category->id;?>" type="radio" name="color" value="#1e874f" <?php echo ($category->color == '#1e874f' ? 'checked="echecked"': '');?> ><label for="radio<?php echo $category->id;?>16<?php echo $category->id;?>"><span><span style="background-color:#1e874f;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#a86bc8' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>17<?php echo $category->id;?>" type="radio" name="color" value="#a86bc8" <?php echo ($category->color == '#a86bc8' ? 'checked="echecked"': '');?> ><label for="radio<?php echo $category->id;?>17<?php echo $category->id;?>"><span><span style="background-color:#a86bc8;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#9843c5' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>18<?php echo $category->id;?>" type="radio" name="color" value="#9843c5" <?php echo ($category->color == '#9843c5' ? 'checked="echecked"': '');?> ><label for="radio<?php echo $category->id;?>18<?php echo $category->id;?>"><span><span style="background-color:#9843c5;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#9e47b8' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>19<?php echo $category->id;?>" type="radio" name="color" value="#9e47b8" <?php echo ($category->color == '#9e47b8' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>19<?php echo $category->id;?>"><span><span style="background-color:#9e47b8;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#ff4753' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>20<?php echo $category->id;?>" type="radio" name="color" value="#ff4753" <?php echo ($category->color == '#ff4753' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>20<?php echo $category->id;?>"><span><span style="background-color:#ff4753;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#e66e32' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>21<?php echo $category->id;?>" type="radio" name="color" value="#e66e32" <?php echo ($category->color == '#e66e32' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>21<?php echo $category->id;?>"><span><span style="background-color:#e66e32;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#f38d39' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>22<?php echo $category->id;?>" type="radio" name="color" value="#f38d39" <?php echo ($category->color == '#f38d39' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>22<?php echo $category->id;?>"><span><span style="background-color:#f38d39;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#2596e6' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>23<?php echo $category->id;?>" type="radio" name="color" value="#2596e6" <?php echo ($category->color == '#2596e6' ? 'checked="echecked"': '');?> ><label for="radio<?php echo $category->id;?>23<?php echo $category->id;?>"><span><span style="background-color:#2596e6;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#238acc' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>24<?php echo $category->id;?>" type="radio" name="color" value="#238acc" <?php echo ($category->color == '#238acc' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>24<?php echo $category->id;?>"><span><span style="background-color:#238acc;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#2a915c' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>25<?php echo $category->id;?>" type="radio" name="color" value="#2a915c" <?php echo ($category->color == '#2a915c' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>25<?php echo $category->id;?>"><span><span style="background-color:#2a915c;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#69b294' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>26<?php echo $category->id;?>" type="radio" name="color" value="#69b294" <?php echo ($category->color == '#69b294' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>26<?php echo $category->id;?>"><span><span style="background-color:#69b294;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#2397a6' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>27<?php echo $category->id;?>" type="radio" name="color" value="#2397a6" <?php echo ($category->color == '#2397a6' ? 'checked="echecked"': '');?> ><label for="radio<?php echo $category->id;?>27<?php echo $category->id;?>"><span><span style="background-color:#2397a6;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#72b1b6' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>28<?php echo $category->id;?>" type="radio" name="color" value="#72b1b6" <?php echo ($category->color == '#72b1b6' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>28<?php echo $category->id;?>"><span><span style="background-color:#72b1b6;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#a39497' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>29<?php echo $category->id;?>" type="radio" name="color" value="#a39497" <?php echo ($category->color == '#a39497' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>29<?php echo $category->id;?>"><span><span style="background-color:#a39497;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#a17c83' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>30<?php echo $category->id;?>" type="radio" name="color" value="#a17c83" <?php echo ($category->color == '#a17c83' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>30<?php echo $category->id;?>"><span><span style="background-color:#a17c83;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#c25f7e' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>31<?php echo $category->id;?>" type="radio" name="color" value="#c25f7e" <?php echo ($category->color == '#c25f7e' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>31<?php echo $category->id;?>"><span><span style="background-color:#c25f7e;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#e92766' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>32<?php echo $category->id;?>" type="radio" name="color" value="#e92766" <?php echo ($category->color == '#e92766' ? 'checked="echecked"': '');?> ><label for="radio<?php echo $category->id;?>32<?php echo $category->id;?>"><span><span style="background-color:#e92766;"></span></span></label>
                                            </li>
                                            <li <?php echo ($category->color == '#434343' ? 'class="active"': '');?>>
                                            	<input id="radio<?php echo $category->id;?>33<?php echo $category->id;?>" type="radio" name="color" value="#434343" <?php echo ($category->color == '#434343' ? 'checked="echecked"': '');?>><label for="radio<?php echo $category->id;?>33<?php echo $category->id;?>"><span><span style="background-color:#434343;"></span></span></label>
                                            </li>
                                            
                                           <!-- <li class="addMore">
                                            	<input id="radio35" type="radio" name="color" value="1" ><label for="radio35"><span><span></span></span></label>
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                	<!--<div class="col-md-4 col-sm-6">
                                        <label>Email Subject<span>*</span></label>
                                        <input type="text" placeholder="Type some keywords for email subject"  name="email_subject" value="<?php //echo $category->email_subject; ?>"/>
                                    </div>-->
                                    
									
									
									<!--<div class="col-md-4 col-sm-6">
                                        <label>Department Type Tags<span>*</span></label>
                                        <input type="text" placeholder="Please Enter Department Tag Like : N"  name="department_tag" value="<?php echo $category->department_tag; ?>"/>
                                    </div>
                                     <div class="col-md-4 col-sm-6">
                                        <label>Department Type Name<span>*</span></label>
                                        <input type="text" placeholder="Please Enter Department Name Like : New Cars"  name="department_name" value="<?php echo $category->department_name; ?>"/>
                                    </div>-->

									<div class="col-md-4 col-sm-6">
                                        <label>Lead Code<span>*</span></label>
                                        <input type="text" placeholder="Please Enter Lead Code e.g NTD etc"  name="lead_code" value="<?php echo $category->lead_code; ?>"/>
                                    </div>

									<div class="col-md-4 col-sm-6">
                                        <label>Call KPI<span>*</span></label>
					                    <input type="text" placeholder="Hours" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="call_kpi" value="<?php echo $category->call_kpi;?>"/>
                                    </div>
                                     <div class="col-md-4 col-sm-6">
                                        <label>Close KPI<span>*</span></label>
                                        <input type="text" placeholder="Hours" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="time_limit" value="<?php echo $category->time_limit;?>"/>
                                    </div>

                                </div>

								<div class="row">                                	

									<div class="col-md-4 col-sm-6">
                                        <label>Survey Email Subject (English)<span>*</span></label>
                                        <input type="text" required placeholder="Title"  name="title_email" value="<?php echo $category->title_email; ?>"/>
                                    </div>

									<div class="col-md-4 col-sm-6">
                                        <label>Survey Email Subject (Arabic)<span>*</span></label>
					                    <input dir="rtl" type="text" required placeholder="" name="title_email_ar" value="<?php echo $category->title_email_ar;?>"/>
                                    </div>

                                </div>

                                <div class="row">

                                	<!--<div class="col-md-4 col-sm-6">
                                        <label>Category Lead Type Tag<span>*</span></label>
                                        <input type="text" placeholder="Please Enter Category Lead Type Tag Like : TD"  name="lead_type_tag" value="<?php echo $category->lead_type_tag; ?>"/>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>Category Lead Type Name<span>*</span></label>
                                        <input type="text" placeholder="Please Enter Category Lead Type Name Like : Test Drive"  name="lead_type_name" value="<?php echo $category->lead_type_name; ?>"/>
                                    </div>-->


									<!--<div class="col-md-4 col-sm-6">
                                        <label>Select Auto Assignee<span></span></label>
										<select name="auto_assignee_id">
											<option value="0">Please Select User</option>                                            
                                        </select>
                                    </div>-->
                                </div>
                                <div class="saveChangSec">
                                    <input type="reset" data-toggle="collapse" href="#leadCatDDownFun_cat<?php echo $category->id; ?>" value="Discard" class="btn standardEdBtn" />
                                    <input type="submit" value="Save Changes" class="btn" />
                                </div>
                            </form>
                        </div>
                    </li>
                    <?php $i++; } ?>
                	<li class="addMore">
                    	<a data-toggle="collapse" href="#AddmoreAreaCat"><i class="fa fa-plus"></i> Add more</a>
                        <div class="collapse leadCatDDown" id="AddmoreAreaCat">
                        	<form action="<?php echo base_url();?>system_settings/saveCategory" method="post" class="mb_form">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <label>Title<span>*</span></label>
                                        <input type="text" placeholder="Title" name="title"/>
                                    </div>                                    
                                    <div class="col-md-8 col-sm-12 colorSelectionCode">
                                        <label class="Title">Select Color<span>*</span></label>
                                         <ul>
                                        	<!-- <input type="radio" /> -->
                                            <li>
                                            	<input id="radio_add1" type="radio" name="color" value="#9cc63c" ><label for="radio_add1"><span><span style="background-color:#9cc63c;"></span></span></label>                                            	
                                            </li>
                                            <li>
                                            	<input id="radio_add2" type="radio" name="color" value="#c6b55b"><label for="radio_add2"><span><span style="background-color:#c6b55b;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add3" type="radio" name="color" value="#c79b42" ><label for="radio_add3"><span><span style="background-color:#c79b42;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add4" type="radio" name="color" value="#c6772c" ><label for="radio_add4"><span><span style="background-color:#c6772c;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add5" type="radio" name="color" value="#8169c7" ><label for="radio_add5"><span><span style="background-color:#8169c7;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add6" type="radio" name="color" value="#744ebb" ><label for="radio_add6"><span><span style="background-color:#744ebb;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add7" type="radio" name="color" value="#666bc6" ><label for="radio_add7"><span><span style="background-color:#666bc6;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add8" type="radio" name="color" value="#215bad" ><label for="radio_add8"><span><span style="background-color:#215bad;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add9" type="radio" name="color" value="#c21c20" ><label for="radio_add9"><span><span style="background-color:#c21c20;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add10" type="radio" name="color" value="#c7461e" ><label for="radio_add10"><span><span style="background-color:#c7461e;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add11" type="radio" name="color" value="#45b6de" ><label for="radio_add11"><span><span style="background-color:#45b6de;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add12" type="radio" name="color" value="#259acf" ><label for="radio_add12"><span><span style="background-color:#259acf;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add13" type="radio" name="color" value="#8aae4a" ><label for="radio_add13"><span><span style="background-color:#8aae4a;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add14" type="radio" name="color" value="#56a532" ><label for="radio_add14"><span><span style="background-color:#56a532;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add15" type="radio" name="color" value="#689727" ><label for="radio_add15"><span><span style="background-color:#689727;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add16" type="radio" name="color" value="#1e874f" ><label for="radio_add16"><span><span style="background-color:#1e874f;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add17" type="radio" name="color" value="#a86bc8" ><label for="radio_add17"><span><span style="background-color:#a86bc8;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add18" type="radio" name="color" value="#9843c5" ><label for="radio_add18"><span><span style="background-color:#9843c5;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add19" type="radio" name="color" value="#9e47b8" ><label for="radio_add19"><span><span style="background-color:#9e47b8;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add20" type="radio" name="color" value="#ff4753" ><label for="radio_add20"><span><span style="background-color:#ff4753;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add21" type="radio" name="color" value="#e66e32" ><label for="radio_add21"><span><span style="background-color:#e66e32;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add22" type="radio" name="color" value="#f38d39" ><label for="radio_add22"><span><span style="background-color:#f38d39;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add23" type="radio" name="color" value="#2596e6" ><label for="radio_add23"><span><span style="background-color:#2596e6;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add24" type="radio" name="color" value="#238acc" ><label for="radio_add24"><span><span style="background-color:#238acc;"></span></span></label>
                                            </li>
                                            <li class="active">
                                            	<input id="radio_add25" type="radio" name="color" value="#2a915c" ><label for="radio_add25"><span><span style="background-color:#2a915c;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add26" type="radio" name="color" value="#69b294" ><label for="radio_add26"><span><span style="background-color:#69b294;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add27" type="radio" name="color" value="#2397a6" ><label for="radio_add27"><span><span style="background-color:#2397a6;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add28" type="radio" name="color" value="#72b1b6" ><label for="radio_add28"><span><span style="background-color:#72b1b6;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add29" type="radio" name="color" value="#a39497" ><label for="radio_add29"><span><span style="background-color:#a39497;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add30" type="radio" name="color" value="#a17c83" ><label for="radio_add30"><span><span style="background-color:#a17c83;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add31" type="radio" name="color" value="#c25f7e" ><label for="radio_add31"><span><span style="background-color:#c25f7e;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add32" type="radio" name="color" value="#e92766" ><label for="radio_add32"><span><span style="background-color:#e92766;"></span></span></label>
                                            </li>
                                            <li>
                                            	<input id="radio_add33" type="radio" name="color" value="#434343" ><label for="radio_add33"><span><span style="background-color:#434343;"></span></span></label>
                                            </li>
                                            
                                           <!-- <li class="addMore">
                                            	<input id="radio35" type="radio" name="color" value="1" ><label for="radio35"><span><span></span></span></label>
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                	<!--<div class="col-md-4 col-sm-6">
                                        <label>Email Subject<span>*</span></label>
                                        <input type="text" placeholder="Type some keywords for email subject"  name="email_subject"/>
                                    </div>-->
                                    
									<!--<div class="col-md-4 col-sm-6">
                                        <label>Department Type Tags<span>*</span></label>
                                        <input type="text" placeholder="Please Enter Department Tag Like : N"  name="department_tag"/>
                                    </div>
                                     <div class="col-md-4 col-sm-6">
                                        <label>Department Type Name<span>*</span></label>
                                        <input type="text" placeholder="Please Enter Department Name Like : New Cars"  name="department_name"/>
                                    </div>-->
									<div class="col-md-4 col-sm-6">
                                        <label>Lead Code<span>*</span></label>
                                        <input type="text" placeholder="Please Enter Lead Code e.g NTD etc"  name="lead_code" value=""/>
                                    </div>
									<div class="col-md-4 col-sm-6">
                                        <label>Call KPI<span>*</span></label>
					                    <input type="text" placeholder="Hours" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="call_kpi" value=""/>
                                    </div>
                                     <div class="col-md-4 col-sm-6">
                                        <label>Close KPI<span>*</span></label>
                                        <input type="text" placeholder="Hours" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="time_limit" value=""/>
                                    </div>

                                </div>
								<div class="row">                                	

									<div class="col-md-4 col-sm-6">
                                        <label>Survey Email Subject (English)<span>*</span></label>
                                        <input type="text" required placeholder="Title" name="title_email" value=""/>
                                    </div>

									<div class="col-md-4 col-sm-6">
                                        <label>Survey Email Subject (Arabic)<span>*</span></label>
					                    <input dir="rtl" type="text" required required placeholder="" name="title_email_ar" value=""/>
                                    </div>

                                </div>
                                <div class="row">
                                	
									<!--<div class="col-md-4 col-sm-6">
                                        <label>Category Lead Type Tag<span>*</span></label>
                                        <input type="text" placeholder="Please Enter Category Lead Type Tag Like : TD"  name="lead_type_tag"/>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>Category Lead Type Name<span>*</span></label>
                                        <input type="text" placeholder="Please Enter Category Lead Type Name Like : Test Drive"  name="lead_type_name"/>
                                    </div>-->

                                </div>
                                <div class="saveChangSec">
                                    <input type="reset" value="Discard" data-toggle="collapse" href="#AddmoreAreaCat" class="btn standardEdBtn" />
                                    <input type="submit" value="Save Changes" class="btn" />
                                </div>
							</form>	                                
                        </div>
                    </li>
                </ul>


            	<ul class="systemSettingPg">
                	<li class="heading">Sources</li>
                	<?php  
					$i=1;
					foreach( $sources as $source ) { ?>
                   	 <li id="<?php echo 'sou'.$source->id; ?>">
                    	<div class="row">
                        	<div class="col-md-8 col-sm-6 col-xs-12"><p><strong><?php echo $i;?>. <?php echo $source->title; ?> </strong></p></div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-right">
                            	<a class="eidtField" data-toggle="collapse" href="#leadSouDDownFun_sou<?php echo $source->id; ?>" aria-expanded="false" aria-controls="leadSourceDDownFun"><i class="fa fa-pencil"></i></a>
                            	
								<a href="javascript:void(0);" onclick="deleteRecordSystemScreen('<?php echo $source->id; ?>' , 'system_settings/deleteSource' , '','<?php echo 'sou'.$source->id; ?>');"><i class="fa fa-trash"></i></a>								

                            </div>
                        </div>
                        <div class="leadCatDDown collapse" id="leadSouDDownFun_sou<?php  echo $source->id; ?>">
                        	<form onsubmit="return false;" class="mb_form" action="system_settings/updateSource">
                               <input type="hidden" name="id" value="<?php echo $source->id; ?>" />
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <label>Title<span>*</span></label>
                                        <input type="text" placeholder="Title" name="title" value="<?php echo $source->title;?>"/>
                                    </div>                                                                        
                                </div>                                                           
                                <div class="saveChangSec">
                                    <input type="reset" data-toggle="collapse" href="#leadSouDDownFun_sou<?php echo $source->id; ?>" value="Discard" class="btn standardEdBtn" />
                                    <input type="submit" value="Save Changes" class="btn" />
                                </div>
                            </form>
                        </div>
                    </li>
                    <?php $i++; } ?>
                	<li class="addMore">
                    	<a data-toggle="collapse" href="#AddmoreAreaSource"><i class="fa fa-plus"></i> Add more</a>
                        <div class="collapse leadCatDDown" id="AddmoreAreaSource">
                        	<form action="<?php echo base_url();?>system_settings/saveSource" method="post" class="mb_form">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <label>Title<span>*</span></label>
                                        <input type="text" placeholder="Title" name="title"/>
                                    </div>                                                                       
                                </div>                                
                                <div class="saveChangSec">
                                    <input type="reset" value="Discard" data-toggle="collapse" href="#AddmoreAreaSource" class="btn standardEdBtn" />
                                    <input type="submit" value="Save Changes" class="btn" />
                                </div>
							</form>	                                
                        </div>
                    </li>
                </ul>


				<ul class="systemSettingPg">
                	<li class="heading">Events (Also used For iPad App form with the combination of above sources)</li>
                	<?php  
					$i=1;
					foreach( $events as $event ) { ?>
                   	 <li id="<?php echo 'eve'.$event->id; ?>">
                    	<div class="row">
                        	<div class="col-md-8 col-sm-6 col-xs-12"><p><strong><?php echo $i;?>. <?php echo $event->title; ?> </strong></p></div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-right">
                            	<a class="eidtField" data-toggle="collapse" href="#leadSouDDownFun_eve<?php echo $event->id; ?>" aria-expanded="false" aria-controls="leadEventDDownFun"><i class="fa fa-pencil"></i></a>
                            	
								<a href="javascript:void(0);" onclick="deleteRecordSystemScreen('<?php echo $event->id; ?>' , 'system_settings/deleteEvent' , '','<?php echo 'eve'.$event->id; ?>');"><i class="fa fa-trash"></i></a>								

                            </div>
                        </div>
                        <div class="leadCatDDown collapse" id="leadSouDDownFun_eve<?php  echo $event->id; ?>">
                        	<form onsubmit="return false;" class="mb_form" action="system_settings/updateEvent">
                               <input type="hidden" name="id" value="<?php echo $event->id; ?>" />
                                <div class="row">
                              <!---- checkbox---->  
                                <div class="col-md-2 col-sm-4">
                                <label>Active</label>
                                <input id="activechk<?php echo $event->id; ?>" type="checkbox" class="is_active_event" name="is_active" value="1" <?php if($event->is_active == 1) {  echo 'checked'; } ?> style="
                                position: absolute;
                                ">
                                <label for="activechk<?php echo $event->id; ?>">
                                <span></span></label>
                                    </div>
                                    <!---- checkbox end----> 
                                    <div class="col-md-5 col-sm-4">
                                        <label>Title<span>*</span></label>
                                        <input type="text" placeholder="Title" name="title" value="<?php echo $event->title;?>"/>
                                    </div>                                                                        
									<div class="col-md-5 col-sm-4">
                                        <label>Title Arabic<span>*</span></label>
                                        <input type="text" placeholder="Title Arabic" name="title_ar" value="<?php echo $event->title_ar;?>"/>
                                    </div>  
                                </div>
                                
        <div class="textareaHEre">
           <label>The email is just for iPad App events.<br />
        Body <span>*</span><br />
        %%NAME%%<br />
        %%TRACK_ID%%<br />
        %%Vehicle_Type%%<br />
        %%Showroom_Name%% <br /></label>
          <textarea style="direction: rtl;" id="some-textarea" placeholder="Enter email template in Arabic" name="template_ar" class="blueBorder"><?php echo $event->template_ar;?></textarea>
                      <br>
          <textarea id="some-textarea" name="template" placeholder="Enter tamplate"><?php echo $event->template;?></textarea>
       </div>  
                                
                                
                                
                                
                                
                                                                                           
                                <div class="saveChangSec">
                                    <input type="reset" data-toggle="collapse" href="#leadSouDDownFun_eve<?php echo $event->id; ?>" value="Discard" class="btn standardEdBtn" />
                                    <input type="submit" value="Save Changes" class="btn" />
                                </div>
                            </form>
                        </div>
                    </li>
                    <?php $i++; } ?>
                	<li class="addMore">
                    	<a data-toggle="collapse" href="#AddmoreAreaEvent"><i class="fa fa-plus"></i> Add more</a>
                        <div class="collapse leadCatDDown" id="AddmoreAreaEvent">
                        	<form action="<?php echo base_url();?>system_settings/saveEvent" method="post" class="mb_form">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <label>Title<span>*</span></label>
                                        <input type="text" placeholder="Title" name="title"/>
                                    </div>                                                                       
									 <div class="col-md-4 col-sm-6">
                                        <label>Title Arabic<span>*</span></label>
                                        <input type="text" placeholder="Title Arabic" name="title_ar"/>
                                    </div>  
                                </div> 
                              
                <div class="textareaHEre">
                      <label>The email is just for iPad App events.</label>
                      <textarea style="direction: rtl;" id="some-textarea" placeholder="Enter email template in Arabic" name="template_ar" class="blueBorder"></textarea>
                      <br>
                      <textarea id="some-textarea" name="template" placeholder="Enter tamplate"></textarea>
                </div> 
                              
                                  
                                
                                
                                
                                
                                                               
                                <div class="saveChangSec">
                                    <input type="reset" value="Discard" data-toggle="collapse" href="#AddmoreAreaEvent" class="btn standardEdBtn" />
                                    <input type="submit" value="Save Changes" class="btn" />
                                </div>
							</form>	                                
                        </div>
                    </li>
                </ul>




				<ul class="systemSettingPg">
                	<li class="heading">Tags</li>
                	<?php  
					$i=1;
					foreach( $tags as $tag ) { ?>
                   	 <li id="<?php echo 'tag'.$tag->id; ?>">
                    	<div class="row">
                        	<div class="col-md-8 col-sm-6 col-xs-12"><p><strong><?php echo $i;?>. <?php echo $tag->title; ?> </strong></p></div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-right">
                            	<a class="eidtField" data-toggle="collapse" href="#leadTagDDownFun_tag<?php echo $tag->id; ?>" aria-expanded="false" aria-controls="leadtagDDownFun"><i class="fa fa-pencil"></i></a>
                            	
								<a href="javascript:void(0);" onclick="deleteRecordSystemScreen('<?php echo $tag->id; ?>' , 'system_settings/deleteTag' , '','<?php echo 'tag'.$tag->id; ?>');"><i class="fa fa-trash"></i></a>								

                            </div>
                        </div>
                        <div class="leadCatDDown collapse" id="leadTagDDownFun_tag<?php  echo $tag->id; ?>">
                        	<form onsubmit="return false;" class="mb_form" action="system_settings/updateTag">
                               <input type="hidden" name="id" value="<?php echo $tag->id; ?>" />
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <label>Title<span>*</span></label>
                                        <input type="text" placeholder="Title" name="title" value="<?php echo $tag->title;?>"/>
                                    </div>                                                                        
                                </div>                                                           
                                <div class="saveChangSec">
                                    <input type="reset" data-toggle="collapse" href="#leadTagDDownFun_tag<?php echo $tag->id; ?>" value="Discard" class="btn standardEdBtn" />
                                    <input type="submit" value="Save Changes" class="btn" />
                                </div>
                            </form>
                        </div>
                    </li>
                    <?php $i++; } ?>
                	<li class="addMore">
                    	<a data-toggle="collapse" href="#AddmoreAreaTag"><i class="fa fa-plus"></i> Add more</a>
                        <div class="collapse leadCatDDown" id="AddmoreAreaTag">
                        	<form action="<?php echo base_url();?>system_settings/saveTag" method="post" class="mb_form">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <label>Title<span>*</span></label>
                                        <input type="text" placeholder="Title" name="title"/>
                                    </div>                                                                       
                                </div>                                
                                <div class="saveChangSec">
                                    <input type="reset" value="Discard" data-toggle="collapse" href="#AddmoreAreaTag" class="btn standardEdBtn" />
                                    <input type="submit" value="Save Changes" class="btn" />
                                </div>
							</form>	                                
                        </div>
                    </li>
                </ul>




            	<ul class="systemSettingPg noBorder">
                	<li class="heading">Email Settings</li>

					<?php
                    $i=1;
                    foreach($emails as $email){ ?>

                	<li>
                    	<div class="row">
                        	<div class="col-md-10 col-sm-10 col-xs-12"><p><strong><?php echo $i; ?>. Subject</strong> <?php echo $email->subject; ?></p></div>
                            <div class="col-md-2 col-sm-2 col-xs-12 text-right">
                            	<a class="eidtField" data-toggle="collapse" href="#emailSetting_<?php echo $email->id; ?>" aria-expanded="false" aria-controls="emailSettings"><i class="fa fa-pencil"></i></a>
                            	<!--<a href="javascript:void(0);"><i class="fa fa-trash"></i></a>-->
                            </div>
                        </div>
                        <div class="leadCatDDown collapse" id="emailSetting_<?php echo $email->id; ?>">

							<?php if($i==7) { ?>
                        	<form method="post" enctype="multipart/form-data" action="system_settings/updateEmail">
							<?php }else{ ?>
							<form onsubmit="return false;" class="mb_form" action="system_settings/updateEmail">
							<?php } ?>
                                <div class="textareaHEre">
                                	<label>
									
									<?php if($i==1) echo 'This email is sent out to customer when a new lead is created from this system for test drive category only.<br>'; ?>
									<?php if($i==9) echo 'This email is sent out to customer when a new lead is created from Leads System campaign forms.<br>'; ?>
                                    <?php //if($i==10) echo 'This is the email that is sent when a new lead is created from a searched lead and the searched lead was in "assign to" status. So the old searched lead\'s assignee receives this email.<br>'; ?>
                                    <?php if($i==10) echo 'This is the email that is sent when a new appointment is created<br>'; ?>
                                    <?php if($i==11) echo 'This is the email that is sent when a appointment is changed<br>'; ?>
                                    <?php if($i==12) echo 'This is the email that is reminder of appointment<br>'; ?>
                                    <?php if($i==13) echo 'This is the email that is sent when a appointment is canceled<br>'; ?>
                                    <?php if($i==14) echo 'This is the email that is sent when a appointment is fulfilled<br>'; ?>
                                    <?php if($i==15) echo 'This is the email that is sent when a appointment is not visited<br>'; ?>
									<?php if($i==8) echo 'This email is sent out to customer when a new lead is created from iPad App.<br>'; ?>
									<?php if($i==2) echo 'This email is sent out to customer when in the lead an email comment is added.<br>'; ?>
									<?php if($i==3) echo 'This email is sent out to customer when lead is closed and survey is sent.<br>'; ?>
									<?php if($i==4) echo 'This email is sent out to customer when the action is added for "customer did not answer the phone call".<br>'; ?>
									<?php if($i==5) echo 'This email is sent out to customer when the action is added for "phone call re-scheduled".<br>'; ?>
									<?php if($i==6) echo 'This email is sent out to customer when test drive is scheduled.<br>'; ?>
									<?php if($i==7) echo 'This email is sent out to system users (employees) when admin sends email from the users listing screen.<br>'; ?>
									
									<?php if($i!=7 && $i!=10 && $i!=11&& $i!=12&& $i!=13&& $i!=14&& $i!=15&& $i!=16){?> Body <span>*</span> <br>%%NAME%%<br>%%TRACK_ID%%<br><?php } ?>
                                    <?php if($i==10 || $i==11 || $i==12){?> <br>%%APPOINTMENT_START_TIME%%<br>%%APPOINTMENT_END_TIME%%<br><?php } ?>
                                        <?php if($i==13 || $i==14 || $i==15 || $i==16){?> <br>%%APPOINTMENT_START_TIME%%<br><?php } ?>
									<?php if($i==1) echo '%%Vehicle_Type%%<br>%%Showroom_Name%%<br>'; ?>
									<?php if($i==2) echo '%%AdminComments%%<br>%%Assign_To_Name%%<br>%%Job_Title%%<br>%%Showroom_Name%%<br>%%Assign_To_Contact%%'; ?>
									<?php if($i==3) echo '%%SurveyLink%%<br>'; ?>
									<?php if($i==4) echo '%%Call_Time%%<br>%%Contact_Number%%<br>%%Assign_To_Contact%%'; ?>
									<?php if($i==1 || $i==5 || $i==6) echo '%%Scheduled_Date_Time%%<br>'; ?>									
									<?php if($i==3) echo '%%Vehicle_Type%%<br>%%Showroom_Name%%<br>%%Assign_To_Name%%'; ?>
									<?php if($i==6) echo '%%Assign_To_Contact%%<br>%%Assign_To_Name%%<br>%%Assign_To_Name_Ar%%'; ?>									
									<?php if($i==8 || $i==9) echo '%%Vehicle_Type%%<br>%%Showroom_Name%%'; ?>									
									</label>
									
									<?php if($i!=2) { ?>
									<textarea style="direction: rtl;" id="some-textarea" name="template_ar"><?php echo $email->template_ar; ?></textarea>
									<?php } ?>

									<br/>
                                	<textarea id="some-textarea" name="template"><?php echo $email->template; ?></textarea>

									<?php if($i==7) { ?>
									<br/>
									Attach new user manul: <input type="file" class="umattm" name="user_manual_attachment" id="user_manual_attachment">
									&nbsp; 
										<?php if($email->attachment_name) { ?>
											<a href="uploads/intro_email_attach/<?php echo $email->attachment_name; ?>" target="_blank">
												<?php echo $email->attachment_name; ?>
											</a>
										<?php } ?>
									<br/>
									<?php } ?>

                                </div>
                                <div class="saveChangSec">
                                    <input type="reset" value="Discard" class="btn standardEdBtn" />
									<input type="hidden" name="id" value="<?php echo $email->id; ?>">
                                    <input type="submit" value="Save Changes" class="btn" />
                                </div>
                            </form>
                        </div>
                    </li>
					<?php $i++; } ?>
					<!--
                    <li>
                    	<div class="row">
                        	<div class="col-md-8 col-sm-6 col-xs-12"><p><strong>2. Subject</strong> Gernal Services</p></div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-right">
                            	<a class="eidtField" data-toggle="collapse" href="#emailSetting_2" aria-expanded="false" aria-controls="emailSettings"><i class="fa fa-pencil"></i></a>
                            	<a href="javascript:void(0);"><i class="fa fa-trash"></i></a>
                            </div>
                        </div>
                        <div class="leadCatDDown collapse" id="emailSetting_2">
                        	<form>
                                <div class="textareaHEre">
                                	<label>Body <span>*</span></label>
                                	<textarea id="some-textarea"></textarea>
                                </div>
                                <div class="saveChangSec">
                                    <input type="submit" value="Cancel Changes" class="btn standardEdBtn" />
                                    <input type="submit" value="Save Changes" class="btn" />
                                </div>
                            </form>
                        </div>
                    </li>
                    <li>
                    	<div class="row">
                        	<div class="col-md-8 col-sm-6 col-xs-12"><p><strong>3. Subject</strong> Inquiry Email</p></div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-right">
                            	<a class="eidtField" data-toggle="collapse" href="#emailSetting_3" aria-expanded="false" aria-controls="emailSettings"><i class="fa fa-pencil"></i></a>
                            	<a href="javascript:void(0);"><i class="fa fa-trash"></i></a>
                            </div>
                        </div>
                        <div class="leadCatDDown collapse" id="emailSetting_3">
                        	<form>
                                <div class="textareaHEre">
                                	<label>Body <span>*</span></label>
                                	<textarea id="some-textarea"></textarea>
                                </div>
                                <div class="saveChangSec">
                                    <input type="submit" value="Cancel Changes" class="btn standardEdBtn" />
                                    <input type="submit" value="Save Changes" class="btn" />
                                </div>
                            </form>
                        </div>
                    </li>
                    <li>
                    	<div class="row">
                        	<div class="col-md-8 col-sm-6 col-xs-12"><p><strong>4. Subject</strong> No Reply Email</p></div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-right">
                            	<a class="eidtField" data-toggle="collapse" href="#emailSetting_4" aria-expanded="false" aria-controls="emailSettings"><i class="fa fa-pencil"></i></a>
                            	<a href="javascript:void(0);"><i class="fa fa-trash"></i></a>
                            </div>
                        </div>
                        <div class="leadCatDDown collapse" id="emailSetting_4">
                        	<form>
                                <div class="textareaHEre">
                                	<label>Body <span>*</span></label>
                                	<textarea id="some-textarea"></textarea>
                                </div>
                                <div class="saveChangSec">
                                    <input type="submit" value="Cancel Changes" class="btn standardEdBtn" />
                                    <input type="submit" value="Save Changes" class="btn" />
                                </div>
                            </form>
                        </div>
                    </li>
                    <li>
                    	<div class="row">
                        	<div class="col-md-8 col-sm-6 col-xs-12"><p><strong>5. Subject</strong> Feedback</p></div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-right">
                            	<a class="eidtField" data-toggle="collapse" href="#emailSetting_5" aria-expanded="false" aria-controls="emailSettings"><i class="fa fa-pencil"></i></a>
                            	<a href="javascript:void(0);"><i class="fa fa-trash"></i></a>
                            </div>
                        </div>
                        <div class="leadCatDDown collapse" id="emailSetting_5">
                        	<form>
                                <div class="textareaHEre">
                                	<label>Body <span>*</span></label>
                                	<textarea id="some-textarea"></textarea>
                                </div>
                                <div class="saveChangSec">
                                    <input type="submit" value="Cancel Changes" class="btn standardEdBtn" />
                                    <input type="submit" value="Save Changes" class="btn" />
                                </div>
                            </form>
                        </div>
                    </li> -->

                    <!--<li class="addMore">
                    	<a href="javascript:void(0);"><i class="fa fa-plus"></i> Create New</a>
                    </li>-->
                </ul>

				<form style="display:none;" id="svfrm" action="<?php echo base_url();?>system_settings/removeSpecVehicle" method="post" class="mb_form">
                                <input type="hidden" id="svfrmid" name="id" value="0">
				</form>	

                <ul class="systemSettingPg noBorder">
                	<li class="heading">Vehicle Management</li>
                	<?php foreach($vehicles as $vehicle){ ?>
                    <li id="vehicle<?php echo $vehicle->id; ?>"><p><a href="javascript:void(0) "onclick="deleteRecordSystemScreen('<?php echo $vehicle->id; ?>' , 'system_settings/deleteVehicle' , '','<?php echo 'vehicle'.$vehicle->id; ?>');" ><i class="fa fa-remove"></i></a>
					<br><label><b>General Name: </b><br><?php echo $vehicle->name; ?></label></p>
					 <?php $specificVehicleNames = getSpecificVehicleNames($vehicle->id);?>
					<br>
					<form action="<?php echo base_url();?>system_settings/updateVehicles" class="mb_form" onsubmit="return false;">
					<label><b>Specific Names:</b>
                      <?php if($specificVehicleNames) foreach($specificVehicleNames as $key =>$value){ ?>
						   <input type="hidden" style="width: 109px;" name="specific_name[<?php echo $value->id;?>]" value="<?php echo $value->name;?>"> 
						   <div class="addCarUHav specific_car">
							   <?php echo $value->name;?>
								<a onclick="if(confirm('Are you sure you want to delete this specific vehicle?')) { $('#svfrmid').val('<?php echo $value->id;?>'); $('#svfrm').submit(); $(this).parent().remove();}" href="javascript:void(0);">X</a>
							</div>						   						   
					  <?php } ?>
					 </label>
					  <label><b>Add More Specific Vehicles:</b></label>
					  <input type="text" placeholder="Type comma separated" name="specific_name_new">
					   <input type="hidden"  value="<?php echo $vehicle->id; ?>" class="btn" name="vehicle_id">
					 <input type="submit"  value="Update" class="btn">
					</form>
					</li>
                    <?php } ?>
                	<!--<li><p><a href="javascript:void(0)"><i class="fa fa-remove"></i></a> C-Class</p></li>
                	<li><p><a href="javascript:void(0)"><i class="fa fa-remove"></i></a> E-Class</p></li>
                	<li><p><a href="javascript:void(0)"><i class="fa fa-remove"></i></a> A-Class</p></li>
                	<li><p><a href="javascript:void(0)"><i class="fa fa-remove"></i></a> GLE</p></li>
                	<li><p><a href="javascript:void(0)"><i class="fa fa-remove"></i></a> GL-Class</p></li>-->
                    <li class="addMore">
                    	<a data-toggle="collapse" href="#AddmoreAreaVehicles"><i class="fa fa-plus"></i> Add more</a>
                        <div class="collapse leadCatDDown" id="AddmoreAreaVehicles">
                        	<form action="<?php echo base_url();?>system_settings/saveVehicle" method="post" class="mb_form">
                                <input type="text" placeholder="Type general vehicle name" name="name" required>
								<input type="text" placeholder="Type specific vehicle names comma separated" name="specific_name" required>
                                <input type="submit" value="Save Changes" class="btn" />
							</form>	                                
                        </div>
                    </li>
                </ul>

				<ul class="systemSettingPg noBorder">
                	<li class="heading">News</li>
                	<?php if($newsArr) foreach($newsArr as $news){ ?>
                    <li id="news<?php echo $news->id; ?>"><p><a href="javascript:void(0) "onclick="deleteRecordSystemScreen('<?php echo $news->id; ?>' , 'system_settings/deleteNews' , '','<?php echo 'news'.$news->id; ?>');" ><i class="fa fa-remove"></i></a><?php echo date("d F Y H:i:s a", strtotime($news->created_at)); ?> | <?php echo $news->comments; ?></p></li>
                    <?php } ?>
                	<!--<li><p><a href="javascript:void(0)"><i class="fa fa-remove"></i></a> C-Class</p></li>
                	<li><p><a href="javascript:void(0)"><i class="fa fa-remove"></i></a> E-Class</p></li>
                	<li><p><a href="javascript:void(0)"><i class="fa fa-remove"></i></a> A-Class</p></li>
                	<li><p><a href="javascript:void(0)"><i class="fa fa-remove"></i></a> GLE</p></li>
                	<li><p><a href="javascript:void(0)"><i class="fa fa-remove"></i></a> GL-Class</p></li>-->
                    <li class="addMore">
                    	<a data-toggle="collapse" href="#AddmoreAreanews"><i class="fa fa-plus"></i> Add more</a>
                        <div class="collapse leadCatDDown" id="AddmoreAreanews">
                        	<form action="<?php echo base_url();?>system_settings/saveNews" method="post" class="mb_form">
                                <textarea placeholder="Enter News " name="comments" required></textarea>
                                <input type="submit" value="Save Changes" class="btn" />
							</form>	                                
                        </div>
                    </li>
                </ul>

                <?php if(false): ?>
				<ul class="systemSettingPg">
                	<li class="heading">iPad App Management</li>
                	<?php  
					$i=1;
					foreach( $registered_app_devices as $registered_app_device ) { ?>
                   	 <li id="<?php echo 'rad'.$registered_app_device->id; ?>">
                    	<div class="row">
                        	<div class="col-md-8 col-sm-6 col-xs-12"><p><strong><?php echo $i;?>. <?php echo $registered_app_device->device_name; ?> </strong></p></div>
                            <div class="col-md-4 col-sm-6 col-xs-12 text-right">
                            	<a class="eidtField" data-toggle="collapse" href="#leadSouDDownFun_rad<?php echo $registered_app_device->id; ?>" aria-expanded="false" aria-controls="leadRegADDDownFun"><i class="fa fa-pencil"></i></a>
                            	
								<a href="javascript:void(0);" onclick="deleteRecordSystemScreen('<?php echo $registered_app_device->id; ?>' , 'system_settings/deleteMobileApp' , '','<?php echo 'rad'.$registered_app_device->id; ?>');"><i class="fa fa-trash"></i></a>								

                            </div>
                        </div>
                        <div class="leadCatDDown collapse" id="leadSouDDownFun_rad<?php  echo $registered_app_device->id; ?>">
                        	<form onsubmit="return false;" class="mb_form" action="system_settings/updateMobileApp">
                               <input type="hidden" name="id" value="<?php echo $registered_app_device->id; ?>" />
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <label>Device Name<span>*</span></label>
                                        <input required type="text" placeholder="Device Name" name="device_name" value="<?php echo $registered_app_device->device_name;?>"/>
                                    </div>
									<div class="col-md-4 col-sm-6">
                                        <label>Code to Connect to iPad<span>*</span></label>
                                        <input readonly type="text" placeholder="Code to Connect to iPad" name="code" value="<?php echo $registered_app_device->code;?>"/>
                                    </div>                                                                        
                                </div>                                                           
								<div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <label>Select Source<span>*</span></label>
										<select name="source_id" required>
										<option value="">Please select</option>
										<?php foreach( $sources as $source ) { ?>
											<option <?php if((int)$source->id===(int)$registered_app_device->source_id) echo "selected"; ?> value="<?php echo $source->id; ?>"><?php echo $source->title ?></option>                                            
										<?php } ?>
                                        </select>
                                    </div>
                                    
                                    
                                                                   
									<div class="col-md-4 col-sm-6">
                                        <label>Select Event<span>*</span></label>
										<select name="event_id" required>
										<option value="">Please select</option>
										<?php foreach( $events as $event ) { ?>
											<option <?php if((int)$event->id===(int)$registered_app_device->event_id) echo "selected"; ?> value="<?php echo $event->id; ?>"><?php echo $event->title ?></option>                                            
										<?php } ?>
                                        </select>
                                    </div> 
                                    
                                </div>
                                
                                
                        	<div class="row">
                            <div class="col-md-4 col-sm-6">
                             <?php $catIdsArr = explode(',',$registered_app_device->categories_id); ?>
                                <label>Select Categories<span>*</span></label>
                                <select style="height:100px;" class="multiple" name="categories_id[]" multiple>
								<?php foreach($categories as $category) { ?>   
                                  	<option value="<?php echo $category->id ?>"<?php if(in_array($category->id,$catIdsArr)){ echo 'selected'; } ?>><?php  echo $category->title; ?> - <?php  echo $category->title_email_ar; ?></option>
                                    <?php } ?>
                                </select> 
                            </div>
                            
                            
                            <div class="col-md-4 col-sm-6">
                            <?php $vehIdsArr = explode(',',$registered_app_device->vehicles_id); ?>
                                <label>Select Vehicles<span>*</span></label>
                                <select style="height:100px;" class="multiple" name="vehicles_id[]" multiple>
                                  <?php foreach($vehicles as $vehicle) { ?>                
                                    <option value="<?php echo $vehicle->id; ?>"<?php if(in_array($vehicle->id,$vehIdsArr)){ echo 'selected'; } ?>><?php echo $vehicle->name; ?></option>
                                  <?php } ?>  
                                </select> 
                            </div>
                            
                            </div>
                            
                    <div class="row">
                    <div class="col-md-4 col-sm-6">
                    <?php  $brncIdsArr = explode(',',$registered_app_device->branches_id); ?>
                        <label>Select Branches<span>*</span></label>
                        <select style="height:100px;" class="multiple" name="branches_id[]" multiple>
						
                         <?php 
						 		foreach($cities as $city) {
									
									$branches = $city['branches'];
									if($branches)
									foreach($branches as $branch)
									{ 
									$selected = "";
									
		if(in_array($branch['id'].'|'.$city['id'],$brncIdsArr)){ $selected = "selected='selected'"; }
									?>
									
                                       <option <?php echo $selected; ?> value="<?php echo $branch['id']; ?>|<?php echo $city['id']; ?>">
                                        <?php echo $city['title']; ?>
                                        -
                                        <?php echo $branch['title']; ?>
                                        -
                                        <?php echo $branch['branch_title_ar']; ?>
                                       </option>
                                   
                                    <?php }
							 } ?>
                             
                        </select> 
                        
                        
                        
                        
                        
                        
                    </div>
                    </div>  
                                    
                                
                                
                                <div class="saveChangSec">
                                    <input type="reset" data-toggle="collapse" href="#leadSouDDownFun_rad<?php echo $registered_app_device->id; ?>" value="Discard" class="btn standardEdBtn" />
                                    <input type="submit" value="Save Changes" class="btn" />
                                </div>
                            </form>
                        </div>
                    </li>
                    <?php $i++; } ?>
                	<li class="addMore">
                    	<a data-toggle="collapse" href="#AddmoreAreaRegAD"><i class="fa fa-plus"></i> Add more</a>
                        <div class="collapse leadCatDDown" id="AddmoreAreaRegAD">
                        	<form action="<?php echo base_url();?>system_settings/saveMobileApp" method="post" class="mb_form">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <label>Device Name<span>*</span></label>
                                        <input required type="text" placeholder="Device Name" name="device_name"/>
                                    </div>                                                                       
									<div class="col-md-4 col-sm-6">
                                        <label>Code to Connect to iPad<span>*</span></label>
                                        <input required type="text" placeholder="Code to Connect to iPad" name="code"/>
                                    </div> 
                                </div>                                
								<div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <label>Select Source<span>*</span></label>
										<select name="source_id" required>
										<option value="">Please select</option>
										<?php foreach( $sources as $source ) { ?>
											<option value="<?php echo $source->id; ?>"><?php echo $source->title ?></option>                                            
										<?php } ?>
                                        </select>
                                    </div>                                                                       
									<div class="col-md-4 col-sm-6">
                                        <label>Select Event<span>*</span></label>
										<select name="event_id" required>
										<option value="">Please select</option>
										<?php foreach( $events as $event ) { ?>
											<option value="<?php echo $event->id; ?>"><?php echo $event->title ?></option>                                            
										<?php } ?>
                                        </select>
                                    </div> 
                                </div>
                                
                                <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <label>Select Categories<span>*</span></label>
                                <select style="height:100px;" class="multiple" name="categories_id[]" multiple>
								<?php foreach($categories as $category) { ?>   
                                  	<option value="<?php echo $category->id ?>"><?php  echo $category->title; ?> - <?php  echo $category->title_email_ar; ?></option>
                                    <?php } ?>
                                </select> 
                            </div>
                            
                            
                            <div class="col-md-4 col-sm-6">
                                <label>Select Vehicles<span>*</span></label>
                                <select style="height:100px;" class="multiple" name="vehicles_id[]" multiple>
                                  <?php foreach($vehicles as $vehicle) { ?>                
                                    <option value="<?php echo $vehicle->id; ?>"><?php echo $vehicle->name; ?></option>
                                  <?php } ?>  
                                </select> 
                            </div>
                            
                            </div>
                            
                    <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <label>Select Branches<span>*</span></label>
                        <select style="height:100px;" class="multiple" name="branches_id[]" multiple>
						 <?php 
						 		foreach($cities as $city) {
									
									$branches = $city['branches'];
									
									foreach($branches as $branch)
									{ ?>
									
                                       <option value="<?php echo $branch['id']; ?>|<?php echo $city['id']; ?>">
                                        <?php echo $city['title']; ?>
                                        -
                                        <?php echo $branch['title']; ?>
                                        -
                                        <?php echo $branch['branch_title_ar']; ?>
                                       </option>
                                   
                                    <?php }
							 } ?>
                         
                        </select> 
                    </div>
                    </div>  
                                
                                
                                <div class="saveChangSec">
                                    <input type="reset" value="Discard" data-toggle="collapse" href="#AddmoreAreaRegAD" class="btn standardEdBtn" />
                                    <input type="submit" value="Save Changes" class="btn" />
                                </div>
							</form>	                                
                        </div>
                    </li>
                </ul>
                <?php endif; ?>

				
				<ul class="systemSettingPg">
                	<li class="heading">Appointment Vehicles</li>                	
                   	 <li>
                    	<div class="row">
                        	<div class="col-md-12 col-sm-12 col-xs-12">
								<div id="appointment_vehicles" style=""></div>
								<script type="text/javascript">

									$(document).ready(function () {

										//Prepare jTable
										$('#appointment_vehicles').jtable({
											title: '&nbsp;',
											actions: {
												listAction: 'system_settings/list_appointment_vehicle_type',
												createAction: 'system_settings/insert_appointment_vehicle_type',
												updateAction: 'system_settings/update_appointment_vehicle_type',
												deleteAction: 'system_settings/delete_appointment_vehicle_type'
											},
											messages: {
											addNewRecord: '+ Add New Vehicle Type'
											},
											fields: {
												id: {
													key: true,
													create: false,
													edit: false,
													list: false
												},
												
												
												
												
												
												//CHILD TABLE DEFINITION FOR "MODELS"
												Models: {
													title: 'Manage Models',
													width: '5%',
													sorting: false,
													edit: false,
													create: false,
													display: function (typeData) {
														//Create an image that will be used to open child table
														var $img = $('<img src="'+base_url+'assets/images/leadDropDown.png" title="Manage Models" />');
														//Open child table when user clicks the image
														$img.click(function () {
															$('#appointment_vehicles').jtable('openChildTable',
																	$img.closest('tr'),
																	{
																		title: typeData.record.name + ' - Models',
																		actions: {
																			listAction: 'system_settings/list_appointment_vehicle_model?appnt_veh_type_id=' + typeData.record.id,
																			createAction: 'system_settings/insert_appointment_vehicle_model',
																			updateAction: 'system_settings/update_appointment_vehicle_model',
																			deleteAction: 'system_settings/delete_appointment_vehicle_model'
																		},
																		messages: {
																		addNewRecord: '+ Add New Model'
																		},
																		fields: {
																			appnt_veh_type_id: {
																				type: 'hidden',
																				defaultValue: typeData.record.id
																			},
																			id: {
																				key: true,
																				create: false,
																				edit: false,
																				list: false
																			},
																			model_name: {
																				title: 'Model Name',
																				width: '30%',
																			},
																			model_year: {
																				title: 'Model Year',
																				width: '30%'
																			}
																		}
																	}, function (data) { //opened handler
																		data.childTable.jtable('load');
																	});
														});
														//Return image to show on the person row
														return $img;
													}
												},
												
												
												
												name: {
													title: 'Vehicle Type'													
												}												
											}
										});

										//Load person list from server
										$('#appointment_vehicles').jtable('load');

									});

								</script>
							</div>
						</div>
					</li>
				</ul>
				

                <div class="clearfix"></div>
            </div>            
        </div>
    </div>
</section>