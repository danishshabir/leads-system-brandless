<section>

	<div class="container">

    	<div class="leadsListingSec">

        	<div class="addNewLeads">

            	<div class="standardEdBtn dropdown">

                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>

                    <ul class="dropdown-menu" aria-labelledby="dLabel">

                        <?php echo menu(); ?>

                    </ul>

                </div>

            

            </div>

        	<div class="leadsListingHead">

            	<div class="row">

                	<div class="col-sm-6">

                        <h1><i class="fa fa-user-plus"></i> Create a New User</h1>

                    </div>

                    <div class="col-sm-6">&nbsp;</div>

               </div>            	

            </div>

            <div class="createNewLeadSec">

            	<p></p>

                

                <div class="newUserSec">

                	<form onsubmit="return false;" action="<?php echo base_url();?>user/userAction" method="post" class="mb_form">

                    <div class="row">

                    	<h2>

                        	<i class="fa fa-user-plus"></i><br/>

                            Personal Information

                        </h2>

                        <div class="line"></div>

                        <hr>

                    </div>

					<div class="row">

                        <div class="col-sm-4">

                        	<label>Full Name English <span>*</span></label>

                            <input type="text" placeholder="Write" class="border2Blue" name="full_name" required="required" />

                        </div>

                        <div class="col-sm-4">

                        	<label>Full Name Arabic <span>*</span></label>

                             <input dir="rtl" type="text" placeholder="Write" class="border2Blue" name="full_name_ar" required="required" />

                        </div>  

						<div class="col-sm-4">

                        	<label>Job Title English<span></span></label>

                            <input type="text" id="job_title" placeholder="" name="job_title" value=""/>

                        </div>

                    </div>

                    <div class="row">                        

						<div class="col-sm-4">

                        	<label>Job Title Arabic<span></span></label>

                            <input dir="rtl" type="text" id="job_title_ar" placeholder="" name="job_title_ar" value=""/>

                        </div>

                        <div class="col-sm-4">

                        	<label>Mobile Number <span>*</span></label>

                            <input type="text" id="mobile-number" placeholder="" name="mobile" value="" required="required"/>

                        </div>

                        <div class="col-sm-4">

                        	<label>Telephone - Ext </label>

                            <div class="row">

                            	<div class="col-xs-7 tellNoline"><input type="text" placeholder="" name="phone1" value=""><div class="dashLine">-</div></div>

                            	<div class="col-xs-5"><input type="text" placeholder="" name="phone2" value=""/></div>

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-4">

                        	<label>Email <span>*</span></label>

                            <input type="email" placeholder="" name="email" value="" required="required"/>

                        </div>

                        <div class="col-sm-4">

                        	<label>City <span>*</span></label>

                            <select class="border2Blue" id="cityDDB" name="city_id" required>

                                <option value= "">Please Select</option>

                                <?php foreach($cities as $cities_list){?>

                                          <option value="<?php echo $cities_list->id; ?>"><?php echo $cities_list->title; ?></option>

                                 <?php } ?>   

                            </select>

                        </div>

                        <div class="col-sm-4">

                        	<label>Branch <span>*</span></label>

                            <select name="branch_id" id="branchDDB" required>

                                <option value= "">Please Select</option>

                            </select>	

                        </div>

                       

                    </div>

                    <div class="row">

                     <div class="col-sm-4">

                        

                        	<label>User Role <span>*</span></label>

                            <select name="role_id" required>

                            <option value="">Please Select</option>

                            	<?php foreach($roles as $role){ ?>

                                <option value="<?php echo $role->id;?>"><?php echo $role->title; ?></option>

                            	<?php } ?>

                            </select>

                        </div>

                      

                        <div class="col-sm-4">

                        	<label>Password <span>*</span></label>

                            <input type="password" name="password" value = "" id="password" required/>

                        </div>

                        <div class="col-sm-4">

                        	<label>Confirm Password <span>*</span></label>

                            <input type="password" name="confirm_password" value = "" id="confirm_password" required/>

                        </div>

                        <div class="col-sm-4">&nbsp;</div>

                    </div> 

					

					<div class="row">

                     <div class="col-sm-4">

                        

                        	<label>Source<span></span></label>

                            <select name="source">

								<option value="">Please Select</option>

                            	<?php foreach($sources as $source){ ?>

                                <option value="<?php echo $source->id;?>" ><?php echo $source->title; ?></option>

                            	<?php } ?>

                            </select>

                        </div>

                      

                        <div class="col-sm-4">

							<label>Adviser Type<span></span></label>

							<select name="adviser_type">

								<option value="">Please Select</option>

                                <option value="0">Quick</option>

								<option value="1">Regular</option>

                            </select>

                        </div>

                        <div class="col-sm-4">

                        	<label>Department <span>*</span></label>

                            <select class="border2Blue" id="department_DDB" name="department_id" required>

                                <option value= "">Please Select</option>
                                 
                                <?php foreach($departments as $department){?>
                                          
                                          <option value="<?php echo $department->id; ?>"><?php echo $department->title; ?></option>

                                 <?php } ?>   

                            </select>

                        </div>

                                          

                    </div> 

                    <!--<<div class="row">

                        div class="col-sm-4">

                                <label>Category </label>

                                <select name="depart_id" >

                                <option value="">Please Select</option>

                                    <?php foreach($departments as $depart){ ?>

                                    <option value="<?php echo $depart->id;?>"><?php echo $depart->title; ?></option>

                                    <?php } ?>

                                </select>

                        </div><div class="col-sm-4">

                                

                                <label>Team Lead </label>

                                <select name="is_team_lead" >

                                     <option value="0">No</option>

                                     <option value="1">Yes</option>

                                 </select>

                        </div>



                        <div class="col-sm-4">

                                <label>Branch Manager</label>

                                

                                <select name="is_branch_manager" >

                                     <option value="0">No</option>

                                     <option value="1">Yes</option>

                                 </select>

                        </div>

						

						<div class="col-sm-4">

                                <label>Ceo</label>

                                

                                <select name="is_ceo" >

                                     <option value="0">No</option>

                                     <option value="1">Yes</option>

                                </select>

                        </div>

                    </div>  -->                 

                    <hr>

                    <div class="row">

                    	<div class="col-sm-4">

                        	

                        </div>



                    	<div class="col-sm-8 text-right">

                        	<input type="button" onclick="history.go(-1);" value="Discard" class="btn white"/>

                        	<input type="submit" value="Create" class="btn" />

                            <input type="hidden" value="save" name="form_type"/>

                        </div>

                    </div>

                </form>

                 

                     

                </div>

                

            </div>

        </div>

    </div>

</section> 



