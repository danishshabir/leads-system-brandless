<section>
	<div class="container">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url(); ?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo menu(); ?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i><img src="<?php echo base_url(); ?>assets/images/users.png" alt="User" height="18" width="22" /></i> User Accounts</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>            	
            </div>
            <div class="createNewLeadSec">
            	<p></p>
                
                <div class="newUserSec user_accounts">
                	<div class="userPadding">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2 class="admin"><?php echo $user->full_name; ?></h2>
                                <p>
                                    <?php echo $user->email; ?>
                                    <br />
                                    <!--Standard-->
                                </p>
                                <br class="hidden-xs"><br class="hidden-xs">
                                <p><span><?php echo date("j F H:i A",strtotime($user->last_login)); ?> : Last Login</span></p>
                            </div>
                            <div class="col-sm-6">
							
							<form action=""  name="action_form" id="user_action_form" style="display:none;">
								 <input type="hidden" name="id" value="" id="user_id_action_form"/>
								 <input type="hidden" value="" name="form_type" id= "user_action_form_type"/>
								 <input type="hidden" value="user" name="redirect" />

							</form>

                                <ul class="editEdSec">
                                    <li style="padding-top: 6px;"><a href="javascript:void(0);" onclick="userAction('<?php echo $user->id ?>', 'delete','user');"><i class="fa fa-trash"></i></a></li>
                                    <li>
                                    	<div class="standardEdBtn dropdown">
                                            <button aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" type="button" id="dLabel"></button>
                                            <ul aria-labelledby="dLabel" class="dropdown-menu">
                                                <li><a href="<?php echo base_url(); ?>user/editUser/<?php echo $user->id ?>">Edit Password</a></li>
                                                <li><a href="<?php echo base_url(); ?>user/editUser/<?php echo $user->id ?>">Edit Profile</a></li>
                                                <li><a href="javascript:void(0);" onclick="userAction('<?php echo $user->id ?>', 'suspend');">Suspend User</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="line"></div>
                    <div class="userPadding">
                    	<div class="row">
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">
                                <h2 title="Total no of Actions added by <?php echo $user->full_name; ?> in 'action box'"><?php echo currentActionsCountOfUser($user->id); ?></h2>
                                <p>Current actions</p>
                            </div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">
                                <h2 title="Total no of Leads assigned to <?php echo $user->full_name; ?>"><?php echo currentCountOfAssignedLeadsOfUser($user->id); ?></h2>
                                <p>Leads assigned</p>
                            </div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">
                                <h2 title="Total no of Leads that are in 'Closed' status that where assigned to <?php echo $user->full_name; ?>"><?php echo currentCompleteLeadsOfUser($user->id); ?></h2>
                                <p>Leads completed</p>
                            </div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">
							     <?php if((int)currentCountOfAssignedLeadsOfUser($user->id) != 0){
									 
									 $assng_leads = (int)currentCountOfAssignedLeadsOfUser($user->id);
									 $success_rate = ((int)currentCompleteLeadsOfUser($user->id)/(int)currentCountOfAssignedLeadsOfUser($user->id)) * 100;
								 }else
								 {
									 $success_rate = 0;
								 }	 
								 
								 
								 ?>
                                <h2 title="'Leads completed' / 'Leads assigned' by <?php echo $user->full_name; ?>"><?php echo number_format($success_rate,2); ?>%</h2>
                                <p>Success rate</p>
                            </div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">&nbsp;</div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">&nbsp;</div>
                        </div>
                    </div>

                    <div class="line"></div>
                    <div class="userPadding">
                    	<div class="row">
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">
                            	<p>Phone Number</p>
                                <p><strong><?php echo $user->phone; ?></strong></p>
                            </div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">
                            	<p>Role</p>
                                <p><strong><?php echo $user->user_type; ?></strong></p>
                            </div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">
                            	<p>Branch</p>
                                <p><strong><?php echo $user->branch_title; ?></strong></p>
                            </div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">
                            	<p>Account</p>
                                <p><strong><?php echo ( $user->is_suspend === '1' ? 'In Active' : 'Active') ;?></strong></p>
                            </div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">&nbsp;</div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">&nbsp;</div>
                        </div>
                    </div>
                    <div class="line"></div>
                    <div class="userPadding">
                    	<div class="row">
                        	<!--<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">
                            	<p>Security</p>
                                <p><strong>Verified</strong></p>
                            </div>-->
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">
                            	<p>Password</p>
                                <p><strong>Protected</strong></p>
                            </div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">&nbsp;</div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">&nbsp;</div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">&nbsp;</div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">&nbsp;</div>
                        </div>
                    </div>
                </div>
				<br/>
                <input type="button" onclick="history.go(-1);" value="Back" class="btn">
            </div>
        </div>
    </div>
</section>
