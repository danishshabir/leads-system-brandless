<section>
	<div class="container">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo menu();?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i><img src="<?php echo base_url();?>assets/images/users.png" alt="User" height="18" width="22" /></i> User Accounts</h1>
                    </div>
                    <div class="col-sm-6">
                    	<ul class="editEdSec">
                            <!--<li><a href="javascript:void(0);" onClick="suspendCheckBoxesRec('userCheckbox','user/userAction','<?php echo base_url();?>user');"><i class="fa fa-lock"></i></a></li>-->           <?php if(rights(8,'delete')){ ?>
                            <li><a href="javascript:void(0);" onClick="deleteCheckBoxesRec('userCheckbox','user/userAction','<?php echo base_url();?>user');"><i class="fa fa-trash"></i></a></li><?php } ?>
                           <!-- <li><a href="javascript:void(0);"><img src="<?php // echo base_url();?>assets/images/dots3Btn.png" alt="edit" height="16" width="10" /></a></li>-->
                        </ul>
                    </div>
               </div>            	
            </div>
            <div class="createNewLeadSec">
            	<!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lobortis, erat in aliquam vestibulum, tortor nisl ullamcorper diam, quis dapibus velit urna et nua. Cras tincidunt diam congue null egestas. Nullam vel interdum metus.</p>-->

				<p>
				<form action="<?php echo base_url();?>User/sendEmailAll" class="mb_form" onsubmit="return false;">

					<input style="float:left; margin-right: 10px;" type="submit" onclick="if(confirm('Are you sure you want to send email to all employees?')) return true; else return false;" value="Send Email" class="btn">

					Send email to "All Users" with introduction email that gives them a brief introduction, and have attachement and have their user name and password.
					The content of this email can be modified from the system settings screen. 

				</form>
				</p>				
                
                <div class="userAccountsSection">
                	<table class="table table-hover tab-content">
                    	<thead>
                        	<tr>
                            	<th><input id="checkbox1" type="checkbox" name="userAccountsSec[]" value="1" onClick="selectMultipleCheckBoxes(this,'userAccountsSec[]');"><label for="checkbox1"><span></span></label></th>
                            	<th>&nbsp;</th>
                            	<th>Name</th>
                            	<th>Type</th>
                            	<th>Branch</th>
                            	<th>Current Status</th>
                            	<th>Last Active</th>
                            	<th>&nbsp;</th>
                            </tr>
                        </thead>
                    	<tbody>
                        <?php 
						$i=1;
						foreach($users as $user){?>
                        	<tr id="user-<?php echo $user->id; ?>">
                            	<td><input id="userAccountsSec_<?php echo $i; ?>" type="checkbox" name="userAccountsSec[]" class="userCheckbox" value="<?php echo ($user->id == 1 ? '' : $user->id);?>" <?php echo ($user->id == 1 ? 'disabled' : ''); ?>><label for="userAccountsSec_<?php echo $i; ?>"><span></span></label></td>
                                <td><img src="<?php echo base_url();?>assets/images/admin.png" alt="icon" height="32" width="32"/> </td>
                                <td><?php echo $user->full_name; ?></td>
                                <td><?php echo $user->user_type; ?></td>
                                <td><?php echo $user->branch_title; ?></td>                                
                                <td><?php echo ($user->is_suspend == 0 ? 'Active' : 'In Active'); ?></td>
                                <td><?php echo ($user->last_login == '0000-00-00 00:00:00' ? '' : date('F j, g:i a', strtotime($user->last_login))); ?></td>
                                <td>
                                    <div class="dropdown">
                                        <button id="dLabel<?php echo $i;?>" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                        <ul class="dropdown-menu" aria-labelledby="dLabel<?php echo $i;?>">
                                            <!--<li><a href="javascript:void(0);">Reset Password</a></li>-->                                            
                                            <?php if(rights(8,'read')){ ?>
                                            <li><a href="<?php echo base_url();?>user/view/<?php echo $user->id;?>">View</a></li>
                                            <?php } ?>
                                            <?php if(rights(8,'update')){ ?>
                                            <li><a href="<?php echo base_url();?>user/editUser/<?php echo $user->id;?>">Edit Profile</a></li>
                                            <?php } ?>
                                            <?php if($user->id != 1){?>
                                            <?php if(rights(8,'update')){ ?>
                                            <li><a href="javascript:void(0);" onclick="userAction('<?php echo $user->id;?>', 'suspend');">Suspend User</a></li>
                                            <?php } ?>
                                             <?php if(rights(8,'delete')){ ?>
                                            <li><a href="javascript:void(0);" onclick="userAction('<?php echo $user->id;?>', 'delete');">Delete User</a></li>
                                            <?php }  } ?>
											<li><a href="javascript:void(0);" onclick="userAction('<?php echo $user->id;?>', 'send_email');">Send Email</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <?php $i++; } ?>    
                        	
                        </tbody>
                    </table>
                </div>
            </div>
            <!--<div class="loadMore"><a href="javascript:void(0);"><i class="fa fa-caret-down"></i> Load More</a></div>-->
        </div>
    </div>
    <!--<input type="hidden" value="Create" class="btn" data-toggle="modal" data-target=".leadCreateSussess" id="show_success_messge" />--><!-- just to show success message bootstrap logic --> 
</section>
<!--<div class="modal fade leadCreateSussess" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header"> 
        	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">x</span></button> 
            <h4 id="mySmallModalLabel" class="modal-title"></h4> 
         </div> 
         <div class="modal-body">
         	<p id="message"></p>
            <p><strong></strong></p>
            <div class="text-center"><button type="button" class="btn" data-dismiss="modal">Dismiss</button></div>
         </div> 
     </div>
  </div>
</div>-->

<form action=""  name="action_form" id="user_action_form" style="display:none;">
 <input type="hidden" name="id" value="" id="user_id_action_form"/>
 <input type="hidden" value="" name="form_type" id= "user_action_form_type"/>

</form>