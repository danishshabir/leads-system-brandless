	<section>

	<div class="container">

    	<div class="leadsListingSec">

        	<div class="addNewLeads">

            	<div class="standardEdBtn dropdown">

                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>

                    <ul class="dropdown-menu" aria-labelledby="dLabel">

                        <?php echo menu(); ?>

                    </ul>

                </div>

            </div>

        	<div class="leadsListingHead">

            	<div class="row">

                	<div class="col-sm-6">

                        <h1><i class="fa fa-user-plus"></i> Edit User</h1>

                    </div>

                    <div class="col-sm-6">&nbsp;</div>

               </div>            	

            </div>

            <div class="createNewLeadSec">

            	<p></p>

                

                <div class="newUserSec">

                	<form onsubmit="return false;" action="<?php echo base_url();?>user/userAction" method="post" class="mb_form">

                    <input type="hidden" name="user_id" value = "<?php echo $user->id; ?>" />

                    <div class="row">

                    	<h2>

                        	<i class="fa fa-user-plus"></i><br/>

                            Personal Information

                        </h2>

                        <div class="line"></div>

                        <hr>

                    </div>

					<div class="row">

                        <div class="col-sm-4">

                        	<label>Full Name English <span>*</span></label>

                            <input type="text" placeholder="Write" class="border2Blue" required="required" name="full_name" value = "<?php echo $user->full_name; ?>" />

                        </div>

                        <div class="col-sm-4">

                        	<label>Full Name Arabic <span>*</span></label>

                            <input dir="rtl" type="text" placeholder="Write" class="border2Blue" required="required" name="full_name_ar" value = "<?php echo $user->full_name_ar; ?>" />

                        </div>                        

						<div class="col-sm-4">

                        	<label>Job Title English<span></span></label>

                            <input type="text" id="job_title" placeholder="" name="job_title" value="<?php echo $user->job_title; ?>"/>

                        </div>

                    </div>

                    <div class="row">    

						<div class="col-sm-4">

                        	<label>Job Title Arabic<span></span></label>

                            <input dir="rtl" type="text" id="job_title_ar" placeholder="" name="job_title_ar" value="<?php echo $user->job_title_ar; ?>"/>

                        </div>

                        <div class="col-sm-4">

                        	<label>Mobile Number</label>

                            <input type="text" placeholder="" name="mobile" value = "<?php echo $user->mobile; ?>" required="required"/>

                        </div>

                        <div class="col-sm-4">

                        	<label>Telephone - Ext </label>

                            <div class="row">

                            <?php $phone =  explode('-',$user->phone); ?>

                            	<div class="col-xs-7 tellNoline"><input type="text" placeholder=""  name="phone1" value="<?php if(isset($phone[0])) { echo $phone[0]; } ?>"><div class="dashLine">-</div></div>

                            	<div class="col-xs-5"><input type="text" placeholder="" name="phone2" value="<?php if(isset($phone[1])) { echo $phone[1]; } ?>"/></div>

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-4">

                        	<label>Email <span>*</span></label>

                            <input type="email" placeholder="" name="email" value="<?php echo $user->email; ?>" required/>

							<input type="hidden" placeholder="" name="oldEmail" value="<?php echo $user->email; ?>"/>

                        </div>

                         <div class="col-sm-4">

                        	<label>City <span>*</span></label>

                            <select class="border2Blue" id="cityDDB" name="city_id" <?php echo( $user->id == $this->session->userdata['user']['id'] ? 'disabled="disabled"' : 'required');?> >

                                <option value= "">Please Select</option>

                                <?php foreach($cities as $cities_list){?>

                                          <option value="<?php echo $cities_list->id; ?>" <?php echo ($user->city_id == $cities_list->id ? 'selected' : '' ); ?> ><?php echo $cities_list->title; ?></option>

                                 <?php } ?>   

                            </select>

                        </div>

                        <div class="col-sm-4">

                        

                        	<label>Branch <span>*</span></label>

                            <select name="branch_id" id="branchDDB" <?php echo( $user->id == $this->session->userdata['user']['id'] ? 'disabled="disabled"' : 'required');?> >

                            	<?php 

								echo branchesByCity(false,$user->city_id,$user->branch_id); ?>

                            </select>

                        </div>

                        

                    </div>

                    <div class="row">

                       <div class="col-sm-4">

                        	<label>User Role <span>*</span></label>

                            <select name="role_id" <?php echo( $user->id == $this->session->userdata['user']['id'] ? 'disabled="disabled"' : 'required');?>  >

                            	<?php foreach($roles as $role){ ?>

                                <option value="<?php echo $role->id;?>" <?php echo ($role->id == $user->role_id ?  'selected' : ''); ?>><?php echo $role->title; ?></option>

                            	<?php } ?>

                            </select>

                        </div>

                        <div class="col-sm-4">

                        	<label>Password <span>*</span></label>

                            <input type="text" name="password" value = "<?php echo $user->password; ?>" id="password" required/>

                        </div>

                       

                         <div class="col-sm-4">

                        	<label>Active User </label>

                            <select name="is_suspend" <?php echo( $user->id == $this->session->userdata['user']['id'] ? 'disabled="disabled"' : '');?> >

                            	

                                <option value="0" <?php echo ($user->is_suspend == 0 ?  'selected' : ''); ?> >Active</option>

                                <option value="1" <?php echo ($user->is_suspend == 1 ?  'selected' : ''); ?> >In Active</option>

                            	

                            </select>

                        </div>

                    </div>  

					

					 <div class="row">

                       <div class="col-sm-4">

                        	<label>Source<span></span></label>

                            <select name="source" <?php echo( $user->id == $this->session->userdata['user']['id'] ? 'disabled="disabled"' : '');?>  >

								<option value="">Please Select</option>

                            	<?php foreach($sources as $source){ ?>

                                <option value="<?php echo $source->id;?>" <?php echo ($source->id == $user->source ?  'selected' : ''); ?>><?php echo $source->title; ?></option>

                            	<?php } ?>

                            </select>

                        </div>

                        <div class="col-sm-4">

						<label>Adviser Type<span></span></label>

							<select name="adviser_type">

								<option value="">Please Select</option>

                                <option <?php if($user->adviser_type!=null && $user->adviser_type==0) echo "selected"; ?> value="0">Quick</option>

								<option <?php if($user->adviser_type!=null && $user->adviser_type==1) echo "selected"; ?> value="1">Regular</option>

                            </select>

                        </div>

						

						<div class="col-sm-4">

                        	<label>Color <span></span></label>

                            <input type="text" class="color" name="pie_color" value="<?php echo $user->pie_color; ?>"/> 

                        </div>

                                              

                    </div>  

                    <div class="row">
                        <div class="col-sm-4">

                            <label>City <span>*</span></label>

                            <select class="border2Blue" id="department_DDB" name="department_id" <?php echo( $user->id == $this->session->userdata['user']['id'] ? 'disabled="disabled"' : 'required');?> >

                                <option value= "">Please Select</option>

                                <?php foreach($departments as $department){?>

                                        <option value="<?php echo $department->id; ?>" <?php echo ($user->department_id == $department->id ? 'selected' : '' ); ?>> <?php echo $department->title; ?></option>

                                <?php } ?>   

                            </select>

                        </div>
                    
                    </div>

					

					

					<?php if($user->id != $this->session->userdata['user']['id']){?>

                   <!-- <div class="row">

                    	<div class="col-sm-4">

                        	<label>Category </label>

                            <select name="depart_id" >

                            	<?php foreach($departments as $depart){ ?> 

                                <option value="<?php echo $depart->id;?>" <?php echo ($depart->id == $user->depart_id ?  'selected' : ''); ?> ><?php echo $depart->title; ?></option>

                            	<?php } ?>

                            </select>

                        </div>

						<div class="col-sm-4 showCheckBox">

                                 <label>Team Lead </label>

                                <select name="is_team_lead" >

                                     <option value="0" <?php if ($user->is_team_lead == 0 ){ echo 'selected'; }?>>No</option>

                                     <option value="1" <?php if ($user->is_team_lead == 1 ){ echo 'selected'; }?>>Yes</option>

                                 </select>

                        </div>

                        <div class="col-sm-4 showCheckBox">

                                <label>Branch Manager</label>

                                <select name="is_branch_manager" >

                                     <option value="0" <?php if ($user->is_branch_manager == 0 ){ echo 'selected'; }?>>No</option>

                                     <option value="1" <?php if ($user->is_branch_manager == 1 ){ echo 'selected'; }?>>Yes</option>

                                </select>

                        </div>

						<div class="col-sm-4 showCheckBox">

                                <label>Ceo</label>

                                <select name="is_ceo" >

                                     <option value="0" <?php if ($user->is_ceo == 0 ){ echo 'selected'; }?>>No</option>

                                     <option value="1" <?php if ($user->is_ceo == 1 ){ echo 'selected'; }?>>Yes</option>

                                </select>

                        </div>

						

                    </div> -->

					<?php } ?>

                    <hr>

                    <div class="row">

                    	<div class="col-sm-4">

                        	

                        </div>

                    	

                    	<div class="col-sm-8 text-right">

                        	<input type="button" value="Discard" onclick="history.go(-1);" class="btn white"/>

                        	<input type="submit" value="Update" class="btn" />

                            <input type="hidden" value="update" name="form_type"/>

                        </div>

                    </div>

                </form>

                <!--<input type="hidden" value="Create" class="btn" data-toggle="modal" data-target=".leadCreateSussess" id="show_success_messge" />--><!-- just to show success message bootstrap logic -->      

                </div>

                

            </div>

        </div>

    </div>

</section> 

<!--<div class="modal fade leadCreateSussess" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">

  <div class="modal-dialog modal-sm">

    <div class="modal-content">

    	<div class="modal-header"> 

        	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">x</span></button> 

            <h4 id="mySmallModalLabel" class="modal-title"></h4> 

         </div> 

         <div class="modal-body">

         	<p id="message"></p>

            <p><strong></strong></p>

            <div class="text-center"><button type="button" class="btn" data-dismiss="modal">Dismiss</button></div>

         </div> 

     </div>

  </div>

</div>-->

