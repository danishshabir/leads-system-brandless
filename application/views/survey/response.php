<section>
	<div class="container">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                       <?php echo menu();?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i class="fa fa-file-text"></i> Survey Response</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>            	
            </div>
            <div class="createNewLeadSec">                
                <div class="survey_inner">
                	<div class="survey_innerHeading">
                    	<div class="row">
                        	<div class="col-md-6 col-sm-6">
                            	<h2>Response</h2>
                            </div>
                            <div class="col-md-6 col-sm-6">
                            	<ul class="editEdSec">
                                	<li>
                                    	<div class="standardEdBtn dropdown">
                                          <!--  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                <li><a href="javascript:void(0);">Email</a></li>
                                                <li><a href="javascript:void(0);">Print</a></li>
                                                <li><a href="javascript:void(0);">Call for Satisfaction</a></li> 
                                            </ul> -->
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="survey_ResHeading">
                    	<div class="row">
                   		<div class="col-md-9 col-sm-9 col-xs-12">
                            <h1>Total Average</h1> 
                            </div>
                        	<div class="col-md-3 col-sm-3 col-xs-12">
                            	Survey Scores
                                
                                <div class="scoreBord">
                                	<p>Score<span><?php if($total_avg != ''){ echo $total_avg;}else{ echo '0'; }  ?></span></p>
                                </div><p> Survey Avg</p>
                                
                                <div class="scoreBordTwo">
                                	<p>Average<span><?php if($total_avg_cat != ''){ echo $total_avg_cat;}else{ echo '0'; }  ?></span></p>
                                </div><p>All surveys average over category <?php echo getCategoryName($survey->category_id);?></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-sm-9 col-xs-12">Questions</div>
                        <div class="col-xs-3">Scores</div>
                    </div>
                    <ul class="surveyResList">
                    	<!--                         
                        **Note:
                        		In progress bar help for developer 
                                multiply by rating and then add zero and give to two places			aria-valuenow="60"				aria-valuenow="60"
                                
                                for example:                                
                                we have 3.2
                                3.2*2 = 6.4 add 0 which make 64                                
                        -->
                    	<?php 
                        $i = 1;
                        if(!empty($results)){
                        foreach($results as $result){
                            
                            $question = getQuestionById($result->id);
                            if(!empty($question))
                            {
                            ?>    
                          <li>
                        	<div class="row">
                                <div class="col-sm-9 col-xs-12"><h6><?php echo $i; ?></h6><?php echo $question->question; ?></div>
                                <div class="col-sm-3 col-xs-12">
                                    <div class="progressSection one">
                                    	
                                        <h6><?php echo round($result->average); ?></h6>
                                        <div class="progress">
                                        <?php $bar_value = ($result->average/5)*100;  ?>
                                          <div class="progress-bar progress-bar-striped " role="progressbar" aria-valuenow="<?php echo $bar_value;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $bar_value;?>%;"></div>
                                        </div>
                                    </div>
                                    <div class="progressSection Two">
                                    	<h6>5</h6>
                                        <div class="progress">
                                          <div class="progress-bar progress-bar-striped " role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php $i++;	} } } ?>
                        
                    </ul>
                </div> 
                
                <div class="clearfix"></div>
            </div>            
        </div>
    </div>
</section>