<section>
	<div class="container">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo menu();?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i class="fa fa-file-text"></i> Survey Management</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>            	
            </div>
            <form action="<?php echo base_url();?>survey/surveyAction" method="post" class="mb_form" onsubmit="return false;"  id="tinymce_form">
            <div class="createNewLeadSec">                
                <div class="survey_inner">
                	<div class="survey_innerHeading">
                    	<div class="row">
                        	<div class="col-md-5 col-sm-4">
                            	<h2><img src="<?php echo base_url();?>assets/images/edit.png" alt="edit" height="18" width="18" />Survey edit</h2>
                            </div>
                            <div class="col-md-7 col-sm-8">
                            	<ul class="editEdSec">
                                	<li><a href="javascript:void(0);"><i class="fa fa-share"></i></a></li>
                                	<li>
                                   <div class="dropdown">
                                       <?php $check = checkIsSubmittedSurvey($survey->id); ?> 
                                        <select name="category_id" <?php if($check){ echo 'disabled';}?>>
                                        	<option value="">Assign to a Category</option>
                                        <?php foreach($categories as $category){ ?>
                                             <option value="<?php echo $category->id;?>" <?php echo ($survey->category_id == $category->id ? 'selected' : ''); ?>><?php echo $category->title;?></option>
                                            <?php } ?>
                                        </select>
										 
										 <?php if($check){ 
										 ?> <input type="hidden" name="category_id" value="<?php echo $survey->category_id; ?>"> <?php
										 }?>

                                    </div> 
                                    </li> 
                                    <!--<li class="active"><a href="javascript:void(0);"><i class=""><img src="<?php echo base_url();?>assets/images/edit_active.png" alt="edit" height="18" width="18" /></i></a></li> -->
                                   <?php if(rights(9,'delete')){ ?> 
                                    <li><a href="javascript:void(0);" onclick="deleteRecord('<?php echo $survey->id; ?>','survey/deleteSurvey','<?php echo base_url(); ?>survey');"><i class="fa fa-trash"></i></a></li> <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>    
                        <p></p>    
                        
                    <div class="InputFieldsSurvey">
                       <input type="hidden" name="survey_id" value="<?php echo $survey->id;?>" />
                        <div class="row">
                            <div class="col-sm-5">
                                <label>Title</label>
                                    <input type="text" value="<?php echo $survey->title;?>" name="title" />
                            </div>
                        </div>
                        <div class="row">
                        	<div class="col-sm-5">
                            	<label>Is Active</label>
                            	<select name="is_active">
                                	
                                    <option value="0" <?php echo ($survey->is_active == 0 ? 'selected' : '');?>>No</option>
                                    <option value="1" <?php echo ($survey->is_active == 1 ? 'selected' : '');?>>Yes</option>
                                    
                                </select>
                            </div>
                    <!-- <div class="row">
                        	<div class="col-sm-5">
                            	<label>Catagories</label>
                            	<select name="category_id">
                                	<option>Please Select</option>
                                    <?php foreach($categories as $category){ ?>
                                    <option value="<?php echo $category->id;?>" <?php echo ($survey->category_id == $category->id ? 'selected' : ''); ?>><?php echo $category->title;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div> -->
                            <div class="col-sm-12">
                                <label>Description</label>
                                    <textarea id="desc" name="description"><?php echo $survey->description; ?></textarea> 
                            </div>
                              
                    </div>
                </div>
            </div>
            
            <div class="questionAir">
            	<div class="questionAirHead2">
                	<div class="row">
                        <div class="col-xs-12">Questions</div>
                        <div class="col-xs-12"><p><b>Note: </b> If any lead filled this survey. You cannot delete questions and cannot edit the answers of questions.</p>Questions</div>                         
                    </div>
                </div>
                <ul class="EditQAirListing">
                <?php 
                if(!empty($questions)){
                $i=1;
                foreach($questions as $question){ ?>
                	<li id="<?php echo $question->id;?>">
                    <?php $checkSurveySubmitted = checkIsQuestionSubmittedInSurvey($question->id); 
                    	if(empty($checkSurveySubmitted)){ ?>
                        <div class="deleteRow" onclick="deleteRecord('<?php echo $question->id; ?>','survey/surveyAction','');">x</div> <?php } ?>
                    	 <label for="survey_<?php echo $i;?>"><span></span><div class="suvryID"><?php echo $i;?></div><?php // echo $question->question; ?></label><input type="text" name="question[<?php echo $question->id; ?>]" value="<?php echo $question->question; ?>"> 
                        <select name="answer[<?php echo $question->id; ?>]" <?php if(!empty($checkSurveySubmitted)){ echo 'disabled'; } ?>>
                               <!--<option value="Rating" <?php echo ($question->answer == 'Rating' ? 'selected' : '') ?>>Rating 1-5</option>                               
                               <option value="Textarea" <?php echo ($question->answer == 'Textarea' ? 'selected' : '') ?>>Textarea</option>0-->
							   <option value="Yes or No" <?php echo ($question->answer == 'Yes or No' ? 'selected' : '') ?>>Yes or No</option>
							   <option value="Satisfied Ok! Not Satisfied" <?php echo ($question->answer == 'Satisfied Ok! Not Satisfied' ? 'selected' : '') ?>>Satisfied Ok! Not Satisfied</option>
							   <option value="High Average Low" <?php echo ($question->answer == 'High Average Low' ? 'selected' : '') ?>>High Average Low</option>
                         </select>	


						<?php if(!empty($checkSurveySubmitted)){ 
						 ?> <input type="hidden" name="answer[<?php echo $question->id; ?>]" value="<?php echo $question->answer; ?>"> <?php
						 }?>

						 <?php if(empty($checkSurveySubmitted)){ ?>
                        <div class="deleteRow" style="background-color: #ffffff">&nbsp;</div> <?php } ?>
						 <label for="survey_ar_<?php echo $i;?>"><span></span><div class="suvryID">&nbsp;</div></label><input type="text" dir="rtl" name="question_ar[<?php echo $question->id; ?>]" value="<?php echo $question->question_ar; ?>"> 
                        <div class="clearfix"></div>                    	 
                    </li>
                 <?php $i++; 
                 }
                 } ?>   
                	
                         <li>
                    	<div class="row">
                        	<div class="col-md-12">
                                <div id="CreateSurvey">
                                    <!--<div id="newQAppend"> -->
                                        <div class="form-group hide" id="CreateSurveyTemplate"> 
                                        	<div class="col-sm-1">
                                                <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                                            </div>
                                            <div class="col-sm-11">
                                            <textarea cols="" rows="0" placeholder="Question in english..." name="questionadd[]"></textarea>
                                            <select name="answeradd[]">
                                                 <!--<option value="Rating">Rating 1-5</option>                                                
                                                <option value="Textarea">Textarea</option>-->
												<option value="Yes or No">Yes or No</option>
												<option value="Satisfied Ok! Not Satisfied">Satisfied Ok! Not Satisfied</option>
												<option value="High Average Low">High Average Low</option>
                                            </select>
											<textarea dir="rtl" style="margin: 20px 0 20px 0;" cols="" rows="0" placeholder="Question in arabic" name="questionadd_ar[]"></textarea>
                                            </div>
                                        </div>
                                        <!--append here-->
                                        
                                       
                                    <!--</div> -->
                                    <div class="form-group AddBtnStyle">
                                        <button type="button" id="addMoreSurQ" class="btn btn-default addButton">
                                            <div class="AddRow">+</div>
                                            <label>Add a question</label>
                                        </button>
                                    </div>
                                </div>
								
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </li>
                </ul>
                <div class="buttonSec">
                    <input type="button" value="Discard" onclick="history.go(-1);" class="btn standardEd white cancel_button"/>
                    <input type="submit" value="Save" class="btn standardEd"/>
                    <input type="hidden" value="update" class="btn standardEd" name="form_type"/>
                </div>
                
                <div class="clearfix"></div>
            </div>
            </form>
        </div>
    </div>
</section>