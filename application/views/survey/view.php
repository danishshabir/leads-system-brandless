<section>
	<div class="container">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                         <?php echo menu();?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i class="fa fa-file-text"></i> Survey Management</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>            	
            </div>
            <div class="createNewLeadSec">                
                <div class="survey_inner">
                	<div class="survey_innerHeading">
                    	<div class="row">
                        	<div class="col-lg-6 col-md-5 col-sm-4 col-xs-12">
                            	<h3><?php echo $survey->title;?>   </h3>
                            </div>
                            <div class="col-lg-6 col-md-7 col-sm-8 col-xs-12">
                            	<ul class="editEdSec">
                                <!--<li><a href="javascript:void(0);"><i class="fa fa-share"></i></a></li>
                                	  <li>
                                    	<div class="dropdown">
                                        <button aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" type="button" id="dLabel">Assign to a Category <span class="caret"></span></button>
                                        <ul aria-labelledby="dLabel" class="dropdown-menu">
                                            <li><a href="javascript:void(0);">Reset Password</a></li>
                                            <li><a href="javascript:void(0);">Edit Profile</a></li>
                                            <li><a href="javascript:void(0);">Suspend User</a></li>
                                            <li><a href="javascript:void(0);">Delete User</a></li>
                                        </ul>
                                    </div>
                                    </li> -->
                                   <?php  if(rights(9,'update')){ ?> 
                                    <li><a href="<?php echo base_url();?>survey/edit/<?php echo $survey_id; ?>"><i class=""><img src="<?php echo base_url();?>assets/images/edit.png" alt="edit" height="18" width="18" /></i></a></li> <?php } ?>
                                   <?php  if(rights(9,'delete')){ ?> 
                                    <li><a href="javascript:void(0);" onclick="deleteRecord('<?php echo $survey_id; ?>','survey/deleteSurvey','<?php echo base_url(); ?>survey');"><i class="fa fa-trash"></i></a></li><?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>    
                        <?php echo $survey->description;?>                   
                </div>
            </div>
            
            <?php 
                if(!empty($questions))
                { ?>
            <div class="questionAir">
            	<div class="questionAirHead2">
                	<div class="row">
                        <div class="col-sm-6 col-xs-12">Questions</div>
                        <div class="col-sm-6 col-xs-12">
                            <!--<ul>
                                <li>1</li>
                                <li>2</li>
                                <li>3</li>
                                <li>4</li>
                                <li>5</li>
                            </ul>-->
                        </div>
                    </div>
                </div>
                <ul class="QAirListing">
                <?php
                    $i = 0;
                    $k = 0;
                    foreach($questions as $question)
                    { 
                         echo '<input type="hidden" name="question['.$question->id.']" value="">';
                        switch($question->answer)
                        {
                           
                            case 'Rating':
                            echo '<li>
                                        <ol>
                                            <li><input id="radio1'.$question->id.'_'.$i.($k+1).'" type="radio" name="radio'.$question->id.'" value="1"><label for="radio1'.$question->id.'_'.$i.($k+1).'"><span><span></span></span></label></li>
                                            <li><input id="radio2'.$question->id.'_'.$i.($k+2).'" type="radio" name="radio'.$question->id.'" value="2"><label for="radio2'.$question->id.'_'.$i.($k+2).'"><span><span></span></span></label></li>
                                            <li><input id="radio3'.$question->id.'_'.$i.($k+3).'" type="radio" name="radio'.$question->id.'" value="3"><label for="radio3'.$question->id.'_'.$i.($k+3).'"><span><span></span></span></label></li>
                                            <li><input id="radio4'.$question->id.'_'.$i.($k+4).'" type="radio" name="radio'.$question->id.'" value="4"><label for="radio4'.$question->id.'_'.$i.($k+4).'"><span><span></span></span></label></li>
                                            <li><input id="radio5'.$question->id.'_'.$i.($k+5).'" type="radio" name="radio'.$question->id.'" value="5"><label for="radio5'.$question->id.'_'.$i.($k+5).'"><span><span></span></span></label></li>
                                        </ol>
                                    	<p>'.$question->question.'</p><div class="clearfix"></div>
                                 </li>';
                            break;
                            case 'Yes or No' :
                            echo '<li>
                                    <div class="yesNoOption">
                                    	<input id="YNOYes'.$i.($k+1).'_'.$question->id.'" type="radio" name="yesno'.$question->id.'" value="Yes" checked="checked"><label for="YNOYes'.$i.($k+1).'_'.$question->id.'"><span><span></span></span>Yes</label>
                                        <input id="YNONo'.$i.($k+2).'_'.$question->id.'" type="radio" name="yesno'.$question->id.'" value="No"><label for="YNONo'.$i.($k+2).'_'.$question->id.'"><span><span></span></span>No</label>
                                    </div>
                                	<p>'.$question->question.'</p><div class="clearfix"></div>
                                </li>';
                            break;
                            case 'Textarea' :
                            echo '<li>
                    	           '.$question->question.'
                                   <textarea placeholder="Write Here" name="answertext['.$question->id.']"></textarea>
                                 </li>';
                            break;
							case 'Satisfied Ok! Not Satisfied' :
                            echo '<li>
                    	          <div class="yesNoOption">
                                    	<input id="SatOkNotS'.$i.($k+1).'_'.$question->id.'" type="radio" name="satoknots'.$question->id.'" value="Satisfied" checked="checked"><label for="SatOkNotS'.$i.($k+1).'_'.$question->id.'"><span><span></span></span>Satisfied</label>
                                        <input id="YNONo'.$i.($k+2).'_'.$question->id.'" type="radio" name="satoknots'.$question->id.'" value="Ok!"><label for="YNONo'.$i.($k+2).'_'.$question->id.'"><span><span></span></span>Ok!</label>
										<input id="YNONo'.$i.($k+3).'_'.$question->id.'" type="radio" name="satoknots'.$question->id.'" value="Not Satisfied"><label for="YNONo'.$i.($k+3).'_'.$question->id.'"><span><span></span></span>Not Satisfied</label>
                                    </div>
                                	<p>'.$question->question.'</p><div class="clearfix"></div>
                                 </li>';
                            break;
							case 'High Average Low' :
                            echo '<li>
                    	          <div class="yesNoOption">
                                    	<input id="HAL'.$i.($k+1).'_'.$question->id.'" type="radio" name="hal'.$question->id.'" value="High" checked="checked"><label for="HAL'.$i.($k+1).'_'.$question->id.'"><span><span></span></span>High</label>
                                        <input id="HAL'.$i.($k+2).'_'.$question->id.'" type="radio" name="hal'.$question->id.'" value="Average"><label for="HAL'.$i.($k+2).'_'.$question->id.'"><span><span></span></span>Average</label>
										<input id="HAL'.$i.($k+2).'_'.$question->id.'" type="radio" name="hal'.$question->id.'" value="Low"><label for="HAL'.$i.($k+2).'_'.$question->id.'"><span><span></span></span>Low</label>
                                    </div>
                                	<p>'.$question->question.'</p><div class="clearfix"></div>
                                 </li>';
                            break;
                            
                        }
                        
                        $i++;
                        $k++;
                
                    
                    }
                    
                }
                 ?>
                	
                </ul>
            </div>
            
        </div>
    </div>
</section>
