<section>
	<div class="container">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo menu(); ?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i class="fa fa-file-text"></i> Survey Management</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>            	
            </div>
            <div class="createNewLeadSec">
            	<p></p>
                
                <div class="survey_management">
                	<div class="row">
                    	<?php foreach($surveys as $survey){ 
                    	   if($survey->is_archive != 1){
                           ?>
                        <div class="col-md-4">
                        	<div class="surveyBox">
                                <div class="surveyHeading" style="border-left:5px solid <?php echo getCategoryColor($survey->category_id);?>;">
                                    <h3><?php echo (strlen($survey->title) > 16 ? substr($survey->title,0,16).'...' : $survey->title);?></h3>
                                    <ul>
                                     <?php if(rights(9,'read')){ ?>
                                        <li>
										<!--<a href="<?php echo base_url();?>survey/view/<?php echo $survey->id;?>" target="_blank">-->

										<a href="<?php echo base_url();?>lead_survey/userview/<?php echo $survey->id;?>" target="_blank">

										<img src="<?php echo base_url();?>assets/images/forward_transparent.png" alt="forward" height="18" width="18" /></a></li>
                                        <li>
                                        <?php } ?>
                                        	<div class="standardEdBtn dropdown">
                                                <button aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" type="button" id="dLabel"></button>
                                                <ul aria-labelledby="dLabel" class="dropdown-menu">
                                                <?php if(rights(9,'read')){ ?>
                                                    <li><a href="<?php echo base_url();?>survey/view/<?php echo $survey->id;?>">View</a></li>
                                                 <?php } if(rights(9,'update')){ ?>  
                                                    <li><a href="<?php echo base_url();?>survey/edit/<?php echo $survey->id;?>">Edit</a></li>
                                                  <?php } if(rights(9,'read')){ ?>   
                                                    <!--<li><a href="<?php echo base_url();?>survey/response/<?php echo $survey->id;?>/<?php echo $survey->category_id; ?>">Response</a></li> -->
                                                  <?php } if(rights(9,'delete')){ ?>    
                                                    <li><a href="javascript:void(0);" onclick="deleteRecord('<?php echo $survey->id; ?>','survey/deleteSurvey','<?php echo base_url(); ?>survey');">Delete</a></li>                         <?php } if(rights(9,'delete')){ ?>  
                                                    <li><a href="javascript:void(0);" onclick="deleteRecord('<?php echo $survey->id; ?>','survey/archiveSurvey','<?php echo base_url(); ?>survey');">Archive</a></li><?php } ?>  
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <?php echo $survey->description;?>
                                <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lobortis, erat in aliquam vestibulum, tortor nisl ullamcorper diam, quis dapibus velit urna et nulla. Cras tincidunt diam sed congue egestas. Nullam vel interdum metus.</p>
                                <ul>
                                    <li>It is a long established fact</li>
                                    <li>There are many variations of passages</li>
                                    <li>Contrary to popular belief</li>
                                </ul>-->
                                <div class="clearfix"></div>
                        	</div>
                        </div>
                        <?php } } ?>
                    	
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
