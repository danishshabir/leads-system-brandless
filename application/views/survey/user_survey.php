<section>
	<div class="container">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                   <!-- <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                         <?php //echo menu();?>
                    </ul> -->
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i class="fa fa-file-text"></i>Survey</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>            	
            </div>
            <div class="createNewLeadSec">                
                <div class="survey_inner">
                	<div class="survey_innerHeading">
                    	<div class="row">
                        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            	<h3 class="pull-right"><?php echo $survey->title;?>   </h3>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            	<ul class="editEdSec">
                                
                                </ul>
                            </div>
                        </div>
                    </div>    
					<div class="row">
						<div class="col-md-12">
							<span dir="rtl" style="font-size:16px;float: right;">
في تارخ <strong><span dir="ltr"><?php echo $lead_created_at_ar; ?></span></strong> قد تقدمت بطلب <strong><?php echo $lead_category_ar; ?></strong> رقم <strong><?php echo $trackid_ar; ?></strong> من فريق مرسيدس-بنز.
<br/>و لكي نتمكن من أن نقدم أفضل الخدمات.
لكي نتمكن من أن نقدم أفضل الخدمات لك ولكل عملائنا الكرام، نرجو منك إكمال الاستبيان التال:
						</span>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<br />
							On <strong><?php echo $lead_created_at; ?></strong> You have submitted <strong><?php echo $lead_category; ?></strong> request number <strong><?php echo $trackid; ?></strong> from Leads System team.
							<br>
							For us to improve our services, we kindly ask you to complete the following Survey:	

							
							<?php // if($survey->description) echo '<br><br>'.$survey->description;?>  			

							<br>
							<?php if($submitted) { ?>
							<h2 style="text-align:center;">Thank you for taking the time to submit this survey.</h2>
							<?php } ?>
							<h2 id="thankyoumsg" style="display:none; text-align:center;">Thank you for taking the time to submit this survey.</h2>
						</div>
					</div>
					
                </div>
            </div>
            
            <?php 
                if(!empty($questions) && !$submitted)
                { ?>

            <?php if($button){ ?>
            <form action="<?php echo base_url();?>lead_survey/surveyAction"  class="mb_form" onsubmit="return false;">			
			<?php }else{ ?>
			<form onsubmit="return false;">
			<?php } ?>
            <input type="hidden" name="lead_id" value="<?php echo $lead_id; ?>" > 
            <input type="hidden" name="survey_id" value="<?php echo $survey_id; ?>" >
            <div class="questionAir">
            	<div class="questionAirHead2">
                	<div class="row">
                        <div class="col-sm-6 col-xs-12"><!--Questions--></div>
                        <div class="col-sm-6 col-xs-12">
                            <!--<ul>
                                <li>1</li>
                                <li>2</li>
                                <li>3</li>
                                <li>4</li>
                                <li>5</li>
                            </ul>-->
                        </div>
                    </div>
                </div>
                <ul class="QAirListing" style="list-style-type: none;">
                <?php
                    $i = 0;
                    $k = 0;
                    foreach($questions as $question)
                    { 
                         echo '<input type="hidden" name="question['.$question->id.']" value="'.$question->answer.'">';
                        switch($question->answer)
                        {
                           
                           /* case 'Rating':
                            echo '<li>
                                        <ol>
                                            <li><input id="radio1'.$question->id.'_'.$i.($k+1).'" type="radio" name="radio['.$question->id.']" value="1"><label for="radio1'.$question->id.'_'.$i.($k+1).'"><span><span></span></span></label></li>
                                            <li><input id="radio2'.$question->id.'_'.$i.($k+2).'" type="radio" name="radio['.$question->id.']" value="2"><label for="radio2'.$question->id.'_'.$i.($k+2).'"><span><span></span></span></label></li>
                                            <li><input id="radio3'.$question->id.'_'.$i.($k+3).'" type="radio" name="radio['.$question->id.']" value="3"><label for="radio3'.$question->id.'_'.$i.($k+3).'"><span><span></span></span></label></li>
                                            <li><input id="radio4'.$question->id.'_'.$i.($k+4).'" type="radio" name="radio['.$question->id.']" value="4"><label for="radio4'.$question->id.'_'.$i.($k+4).'"><span><span></span></span></label></li>
                                            <li><input id="radio5'.$question->id.'_'.$i.($k+5).'" type="radio" name="radio['.$question->id.']" value="5"><label for="radio5'.$question->id.'_'.$i.($k+5).'"><span><span></span></span></label></li>
                                        </ol>
                                    	<p class="arq">'.$question->question_ar.'</p>
										<p>'.$question->question.'</p>
										<div class="clearfix"></div>
                                 </li>';
                            break;*/
                            case 'Yes or No' :
                            echo '<li class="sqfpli">
									<div class="row">
										<div class="col-md-8 col-sm-6 text-right pull-right">
											<p class="arq" style="display: block; font-size:16px;">'.$question->question_ar.'</p>
											<p style="display: block; ">'.$question->question.'</p>
										</div>
										<div class="col-md-4 col-sm-6 text-center pull-left">
											<div class="pull-left">
												<div class="ar">نعم</div> Yes
												<input id="YNOYes'.$i.($k+1).'_'.$question->id.'" type="radio" name="yesno['.$question->id.']" value="Yes" checked="checked"><label for="YNOYes'.$i.($k+1).'_'.$question->id.'"><span><span></span></span></label>
											</div>
											<div class="pull-left">
												 <div class="ar">لا</div> No
												 <input id="YNONo'.$i.($k+2).'_'.$question->id.'" type="radio" name="yesno['.$question->id.']" value="No"><label for="YNONo'.$i.($k+2).'_'.$question->id.'"><span><span></span></span></label>
											</div>
											<div class="clearfix"></div>											
										</div>
									</div>
										<div class="listStyleNo">'.($i+1).'</div>
                                </li>';
                            break;
                            /*case 'Textarea' :
                            echo '<li>
                    	           '.$question->question_ar .'<br>'. $question->question.'
                                   <textarea placeholder="Write Here" name="answertext['.$question->id.']"></textarea>
                                 </li>';
                            break;*/
							case 'Satisfied Ok! Not Satisfied' :
                            echo '<li class="sqfpli">									
									<div class="row">
										<div class="col-md-8 col-sm-6 text-right pull-right">
											<p class="arq" style="display: block; font-size:16px;">'.$question->question_ar.'</p>
											<p style="display: block; ">'.$question->question.'</p>
										</div>
										<div class="col-md-4 col-sm-6  text-center pull-left">
											<div class="pull-left">
												<div class="ar">راضي</div> Satisfied
												<input id="SatOkNotS'.$i.($k+1).'_'.$question->id.'" type="radio" name="satoknots['.$question->id.']" value="Satisfied" checked="checked"><label for="SatOkNotS'.$i.($k+1).'_'.$question->id.'"><span><span></span></span></label>
											</div>
											<div class="pull-left">
												<div class="ar">مقبول</div> Ok!
												<input id="YNONo'.$i.($k+2).'_'.$question->id.'" type="radio" name="satoknots['.$question->id.']" value="Ok!"><label for="YNONo'.$i.($k+2).'_'.$question->id.'"><span><span></span></span></label>
											</div>
											<div class="pull-left">
												<div class="ar">غير راضي</div> Not Satisfied
												<input id="YNONo'.$i.($k+3).'_'.$question->id.'" type="radio" name="satoknots['.$question->id.']" value="Not Satisfied"><label for="YNONo'.$i.($k+3).'_'.$question->id.'"><span><span></span></span></label>
											</div>
											<div class="clearfix"></div>											
										</div>
									</div>
										<div class="listStyleNo">'.($i+1).'</div>                                	
                                 </li>';
                            break;

							case 'High Average Low' :
                            echo '<li class="sqfpli">
									<div class="row">
										<div class="col-md-8 col-sm-6 text-right pull-right">
											<p class="arq" style="display: block; font-size:16px;">'.$question->question_ar.'</p>
											<p style="display: block; ">'.$question->question.'</p>
										</div>
										<div class="col-md-4 col-sm-6  text-center pull-left">
											<div class="pull-left">
												<div class="ar">ممتاز</div> High
												<input id="HAL'.$i.($k+1).'_'.$question->id.'" type="radio" name="hal['.$question->id.']" value="High" checked="checked"><label for="HAL'.$i.($k+1).'_'.$question->id.'"><span><span></span></span></label>
											</div>
											<div class="pull-left">
												<div class="ar">متوسط</div> Average
												 <input id="HAL'.$i.($k+2).'_'.$question->id.'" type="radio" name="hal['.$question->id.']" value="Average"><label for="HAL'.$i.($k+2).'_'.$question->id.'"><span><span></span></span></label>
											</div>
											<div class="pull-left">
												<div class="ar">ضعيف</div> Low
												<input id="HAL'.$i.($k+3).'_'.$question->id.'" type="radio" name="hal['.$question->id.']" value="Low"><label for="HAL'.$i.($k+3).'_'.$question->id.'"><span><span></span></span></label>
											</div>
											<div class="clearfix"></div>											
										</div>
									</div>
										<div class="listStyleNo">'.($i+1).'</div>
                                 </li>';
                            break;
                            
                        }
                        
                        $i++;
                        $k++;
                
                    
                    }
                    
                }
                 ?>
                	
                </ul>
                <div class="buttonSec">
				<?php if(!$submitted){ ?>
                    <?php if($button){ ?>
						<input type="submit" value="Submit" class="btn standardEd" onClick="$('.buttonSec,.mb_form').hide(); $('#thankyoumsg').show();"/>
						<input type="hidden" value="submit_survey" class="btn standardEd" name="form_type"/>
					<?php }else{ ?>
						<input type="button" value="Submit" class="btn standardEd"/>
					<?php } ?>
				<?php } ?>
                </div>
            </div>
            </form>
        </div>
    </div>
</section>
