<section>
	<div class="container">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                         <?php echo menu();?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i class="fa fa-file-text"></i> Survey Response</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>            	
            </div>
            <div class="createNewLeadSec">                
                <div class="survey_inner">
                	<div class="survey_innerHeading">
                    	<div class="row">
                        	<div class="col-md-6 col-sm-6">
                            	<h2>Lead Number: <?php echo $lead->trackid; ?></h2>
                            </div>
                            <div class="col-md-6 col-sm-6">
                            	<ul class="editEdSec">
                                	<li>
                                    	<div class="standardEdBtn dropdown">
                                            <!--<button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>-->
                                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                <!--<li><a href="javascript:void(0);">Email</a></li>
                                                <li><a href="javascript:void(0);">Print</a></li>
                                                <li><a href="javascript:void(0);">Call for Satisfaction</a></li> -->
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="survey_ResHeading">
                    	<div class="row">
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">
                            	<label>Name</label>
                                <p><?php echo $lead->first_name. ' '.$lead->surname; ?></p>
                            	<label>Category</label>
                                <p><?php echo getCategoryName($lead->category_id); ?></p>
                            </div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">
                            	<label>Lead Created</label>
                                <p><?php echo date("j F Y",strtotime($lead->created_at));  ?></p>
                            	<label>Survey Received</label>
                                <p><?php if(!empty($questions)){ echo date("j F Y",strtotime($questions[0]->sur_submitted)); }  ?></p>
                            </div>
                        	<div class="col-md-2 col-sm-3 col-xs-6 MobileFull">
                            	<label>City</label>
                                <p><?php echo getCityName($lead->city_id); ?></p>
                            	
                            </div>
                        	<div class="col-md-3 col-sm-3 col-xs-6 MobileFull">
                            	<label>Vehicle</label>
                                <p><?php echo $vehicles = getVehicles($lead->vehicle); ?></p>
                            	
                            </div>
                        	<div class="col-md-3 col-sm-3 col-xs-12">
                            	Survey Scores
                                <div title="This particular survey score." class="scoreBord">
                                	<p>Score<span><?php if($total_avg_by_lead != ''){ echo $total_avg_by_lead;}else{ echo '0'; }  ?></span></p>
                                </div>
                                <div title="Overall Average by others too." class="scoreBordTwo">
                                	<p>Average<span><?php if($total_avg != ''){ echo $total_avg;}else{ echo '0'; }  ?></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-sm-9 col-xs-12">Questions</div>
                        <div class="col-xs-3">Answers</div>
                    </div>
                    <ul class="surveyResList">
                    	<!--                         
                        **Note:
                        		In progress bar help for developer 
                                multiply by rating and then add zero and give to two places			aria-valuenow="60"				aria-valuenow="60"
                                
                                for example:                                
                                we have 3.2
                                3.2*2 = 6.4 add 0 which make 64                                
                        -->
                        <?php 
                        if(!empty($questions)){
                           $i=1;
                           foreach($questions as $question){ 
                              switch($question->answer)
                              {
                               /* case 'Rating';
                                $avg = ($question->user_answer/5)*100;
                                echo '<li>
                        	<div class="row">
                                <div class="col-sm-9 col-xs-12"><h6>'.$i.'</h6>'.$question->question.'</div>
                                <div class="col-sm-3 col-xs-12">
                                    <div class="progressSection one">
                                    	<h6>'.$question->user_answer.'</h6>
                                        <div class="progress">
                                          <div class="progress-bar progress-bar-striped " role="progressbar" aria-valuenow="'.$avg.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$avg.'%;"></div>
                                        </div>
                                    </div>
                                    <div class="progressSection Two">
                                    	<h6>5</h6>
                                        <div class="progress">
                                          <div class="progress-bar progress-bar-striped " role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>';
                                break;*/
                                case 'Yes or No';
                                echo '<li>
                        	<div class="row">
                                <div class="col-sm-9 col-xs-12"><h6>'.$i.'</h6>'.$question->question.'</div>
                                <div class="col-sm-3 col-xs-12">
                                    
                                    	<h6>'.$question->user_answer.'</h6>
                                        
                                    
                                    
                                </div>
                            </div>
                        </li>';
                                
                                break;

								case 'High Average Low';
                                echo '<li>
                        	<div class="row">
                                <div class="col-sm-9 col-xs-12"><h6>'.$i.'</h6>'.$question->question.'</div>
                                <div class="col-sm-3 col-xs-12">
                                    
                                    	<h6>'.$question->user_answer.'</h6>
                                        
                                    
                                    
                                </div>
                            </div>
                        </li>';
                                
                                break;

								case 'Satisfied Ok! Not Satisfied';
                                echo '<li>
                        	<div class="row">
                                <div class="col-sm-9 col-xs-12"><h6>'.$i.'</h6>'.$question->question.'</div>
                                <div class="col-sm-3 col-xs-12">
                                    
                                    	<h6>'.$question->user_answer.'</h6>
                                        
                                    
                                    
                                </div>
                            </div>
                        </li>';
                                
                                break;
                                /*case 'Textarea';
                                echo '<li>
                        	<div class="row">
                                <div class="col-xs-12"><h6>'.$i.'</h6>'.$question->question.'</div>   
                            </div>
                            <div class="row">
                            	<div class="col-sm-9 col-xs-12">
                                	<p>'.$question->user_answer.'</p>
                                </div>
                            	<div class="col-sm-3 col-xs-12">&nbsp;</div>
                            </div>
                        </li>';
                                break;*/
                                
                              }
                       $i++; } }  ?>
                    	
                        
                    </ul>
                </div> 
                
                <div class="clearfix"></div>
            </div>            
        </div>
    </div>
</section>