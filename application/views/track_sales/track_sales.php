<section>
	<div class="container lead_create_edit_frm">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo menu(); ?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i class="sprite sprite-menagment"></i>
						Track Sales
						</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>
            </div>
            <div class="createNewLeadSec">
            	<!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lobortis, erat in aliquam vestibulum, tortor nisl ullamcorper diam, quis dapibus velit urna et nua. Cras tincidunt diam congue null egestas. Nullam vel interdum metus.</p>-->
				<p>&nbsp;</p>
				<form action="" class="" onsubmit="">

					<div class="row">

						<div class="col-md-3">
						&nbsp;
						</div>
						<div class="col-md-3">
							<?php
							// use this to set an option as selected (ie you are pulling existing values out of the database)
							$already_selected_value = date("Y");
							$earliest_year = date("Y")-1;

							print '<select name="soled_year" onchange="$(this).closest(\'form\').submit();">';
							echo "<option value=''>Select Sold Year</option>";
							foreach (range(date('Y'), $earliest_year) as $x) {
								print '<option '.($x == $_GET['soled_year'] ? 'selected="selected"' : '').' value="'.$x.'"'.($x === $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
							}
							print '</select>';
							?>
						</div>
						<div class="col-md-3">
							<select name="sold_month" size='1' class="blueBorder" onchange="$(this).closest('form').submit();">
								<?php
								echo "<option value=''>Select Sold Month</option>";
								for ($i = 0; $i < 12; $i++) {
									$time = strtotime(sprintf('%d months', $i));
									$label = date('F', $time);
									$value = date('n', $time);
									echo "<option ".($value == $_GET['sold_month'] ? 'selected="selected"' : '')." value='$value'>$label</option>";
								}
								?>
							</select>
						</div>
						<div class="col-md-3">
						&nbsp;
						</div>
					</div>

				</form>
				<p>&nbsp;</p>

                <div class="userAccountsSection">
                	<table class="table table-hover tab-content">
                    	<thead>
                        	<tr>
                            	<!--<th><input id="checkbox1" type="checkbox" name="userAccountsSec[]" value="1" onclick=""><label for="checkbox1"><span></span></label></th>-->
								<th>&nbsp; </th>
                            	<th>Trackid</th>                            	
                            	<th>Called by</th>
                            	<th>Called Time</th>
                            	<th>&nbsp; </th>
								<th>Sold Vehicle</th>
                            	<th>Sold by</th>
                            	<th>Sold Date</th>
                            </tr>
                        </thead>
                    	<tbody>
							<?php if($sales){ 
							foreach($sales as $sale)
							{
							?>
							<tr>                            	
								<td>&nbsp;</td>
								<td><?php echo $sale->trackid; ?></td>
                                <td><?php echo $sale->phone_called_by; ?></td>
								<td><?php echo date('F j, g:i a', strtotime($sale->called_at)); ?></td> 
                                <td>&nbsp;</td>
                                <td><?php echo $sale->sold_vehicle; ?></td>
                                <td><?php echo $sale->sold_by; ?></td>
								<td><?php echo ($sale->purchased_at == '0000-00-00 00:00:00' ? '' : date('F j, g:i a', strtotime($sale->purchased_at))); ?></td>                                
                            </tr>
							<?php } }else{ ?>
							<tr>                            	
								<td colspan="8">                                
								Sorry no sales found!
								</td>
                            </tr>
							<?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

