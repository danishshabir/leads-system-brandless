<style type="text/css">

    .review_appointment .popover-content {
        display: flex;
        justify-content: space-around;
        flex-wrap: wrap;
    }

    .review_appointment .popover-content > * {
        width: 48% !important;
        word-break: break-word;
        white-space: normal;
        padding: 10px;
    }

    .popover{
        width: 450px;
        max-width: 450px;
        min-width: 450px;
    }
    i.initPopover {
        display: block;
    }
</style>
<div class="tblRghtMain">
	<table cellspacing="0">
		<caption>
			<div class="leftCapTop">
				<a date="<?php echo date('Y-m-d', strtotime((date('w', strtotime(date('Y-m-d', strtotime('-1 days',strtotime($weekStart))))) == 5 ? -2 : -1).' days',strtotime($weekStart))); ?>" class="date-trigger">
				<i class="fa fa-caret-left" aria-hidden="true"></i>
				</a>
				<?php echo date('D/jS M,Y', strtotime($weekStart)); ?>
				<a date="<?php echo date('Y-m-d', strtotime((date('w', strtotime(date('Y-m-d', strtotime('+1 days',strtotime($weekStart))))) == 5 ? +2 : +1).' days',strtotime($weekStart))); ?>" class="date-trigger">
				<i class="fa fa-caret-right" aria-hidden="true"></i>
				</a>
			</div>
			<div class="rightCapBtns">
				<a class="load-calender" data-page="2" href="javascript:void(0);"><input class="btn <?php echo ($this->input->post('page') == 2 ? 'active' : ''); ?>" value="Day" type="button"></a>
				<a class="load-calender" data-page="1" href="javascript:void(0);"><input class="btn <?php echo ($this->input->post('page') == 1 ? 'active' : ''); ?>" value="Week" type="button"></a>
			</div>
			<div class="clearfix"></div>
		</caption>
		<thead>
			<tr>
				<th>Time</td>
				<th>Client</td>
				<th>Vehicle</td>
				<th>Contact</td>
				<th>&nbsp;</td>
			</tr>
		</thead> 
		<tbody class="tblBtnsStp4">
			<?php
			$skip = array();
			$start_time     = strtotime($branchRec->start_time);
			$end_time    = strtotime($branchRec->end_time);
			$repeat = 1; 
			for ($i = $start_time; $i < $end_time; $i += $interval) { ?>
				<tr class="self-daily-appointment <?php echo ($repeat != 0 && $repeat%4 == 0 ? 'hourEnd' : '') ?>">
					<td><?php echo date('g:i A', $i); ?></td>
					<?php if ((check_time($branchRec->break2_start, $branchRec->break2_end, date('H:i:s', $i))) || (check_time($branchRec->break1_start, $branchRec->break1_end, date('H:i:s', $i)))) { ?>
						<td colspan="4" class="breakRow"><span class="breakTxt">Breaktime</span></td>
					<?php } else { 
						$dateTime = $weekStart.' '.date('H:i:s', $i); 
						
						if($this->session->userdata['user']['adviser_type'] == 1 && $repeat % 2 == 1)
						{
							array_push($skip,date("Y-m-d H:i:s", strtotime('+15mins', strtotime($dateTime))));
						}
						
						if(in_array($dateTime, $appointment['appointment_start_time'])) { 
							$key = array_search($dateTime,$appointment['appointment_start_time']);
							$vehicle_model = vehicle_model_name($appointment['appointment_vehicle_id'][$key]);
							?>

                            <?php
                            $status = $appointment['status'][$key];
                            $content = "<button class='btn btn-primary appointment-view'>Appointment View</button><button class='btn btn-info appointment-fulfilled' data-status='7'>Appointment Fulfilled</button><button class='btn btn-warning appointment-no-show' data-status='8'>Appointment Not Visited</button><button class='btn btn-danger appointment-cancel'>Appointment Canceled</button>";
                            $colorClass = 'gray';
                            if($status == 7) {
                                $content = "<button class='btn btn-primary appointment-view'>Appointment View</button>";
                                $colorClass = 'text-success';
                                $text = 'Fulfilled';
                            } elseif ($status == 8) {
                                $content = "<button class='btn btn-primary appointment-view'>Appointment View</button>";
                                $colorClass = 'text-danger';
                                $text = 'Not Visited';
                            } elseif ($status == 9) {
                                $content = "<button class='btn btn-primary appointment-view'>Appointment View</button>";
                                $colorClass = 'fa fa-plus plusBtn';
                            } elseif ($status == 10) {
                                $content = "<button class='btn btn-primary appointment-view'>Appointment View</button><button class='btn btn-info appointment-fulfilled' data-status='7'>Appointment Fulfilled</button><button class='btn btn-warning appointment-no-show' data-status='8'>Appointment Not Visited</button><button class='btn btn-danger appointment-cancel'>Appointment Canceled</button>";
                                $colorClass = 'text-default';
                                $text = "Pending";
                            }
                            ?>

							<td class="blue review_appointment" data-id="<?php echo $appointment['id'][$key]; ?>">
								<span class="<?php echo $colorClass; ?> initPopover" data-toggle="popover" data-trigger="focus" title="Appointment Action" data-content="<?php echo $content; ?>" data-html="true" data-placement="bottom"><?php echo $appointment['name'][$key]; ?></span>
							</td>
							<td class="blue"><span class="<?php echo $colorClass; ?>"><?php echo $vehicle_model->model_name.' '.$vehicle_model->model_year; ?></span></td>

							<td class="blue"><span class="<?php echo $colorClass; ?>"><?php echo $appointment['mobile'][$key]; ?></span></td>
							<?php if($appointment['status'][$key] != NULL) { ?>
								<td class="<?php if($appointment['status'][$key] == 7) echo "green"; elseif($appointment['status'][$key] == 8) echo "red"; elseif($appointment['status'][$key] == 9) echo "red";  elseif($appointment['status'][$key] == 10) echo "blue";?>">
								<strong><?php if($appointment['status'][$key] == 7) echo "Fulfilled"; elseif($appointment['status'][$key] == 8) echo "Not visited"; elseif($appointment['status'][$key] == 9) echo "Canceled"; elseif($appointment['status'][$key] == 10) echo "Pending"; ?></strong>
								</td>
							<?php } else { ?>
								<td class="text-center buttons">
									<span class="">
									    <?php
                                            if ($status == 7) {
                                                echo "Fulfilled";
                                            }
                                        ?>
									<div class="clearfix"></div>
									</span>
								</td>
							<?php } ?>
						<?php } else { 
							if(!in_array($dateTime, $skip)) {
							?>
								<td class="green"><span class="breakTxt">No Appointment</span></td>
								<td colspan="2"><span class="breakTxt">&nbsp;</span></td>
								<td class="text-center appointment_time" date="<?php echo $dateTime; ?>"><span class="plusBtn" style="cursor:pointer;"><i class="fa fa-plus" aria-hidden="true" style="cursor:pointer;"></i></span></td>
							<?php
							} else 
							{ ?>
								<td colspan="4">&nbsp;</td><?php
							}							
						 }
					} ?>
				</tr>
				<?php 
				$repeat++;
			} ?>
		</tbody>
	</table>
</div>
<input type="hidden" value="<?php echo date('Y-m-d', strtotime($weekStart)); ?>" class="weekStart"/>
	