<style type="text/css">

    .review_appointment .popover-content {
        display: flex;
        justify-content: space-around;
        flex-wrap: wrap;
    }

    .review_appointment .popover-content > * {
        width: 48% !important;
        word-break: break-word;
        white-space: normal;
        padding: 10px;
    }

    .popover{
        width: 450px;
        max-width: 450px;
        min-width: 450px;
    }
    i.initPopover {
        display: block;
    }
</style>
<?php /*15th Sep 2017*/$appnt_dur = 900;if($this->session->userdata['user']['adviser_type'] == 1)	$appnt_dur = 1800;$total_slots = count_appointment_intervals($branchRec->id, $appnt_dur);							?><div class="tblRghtMain">
	<table cellspacing="0">
		<caption>
			<div class="leftCapTop">
				<a date="<?php echo date('Y-m-d', strtotime('-7 days',strtotime($weekStart))); ?>" class="date-trigger">
				<i class="fa fa-caret-left" aria-hidden="true"></i>
				</a>
				<?php echo date('D/jS M,Y', strtotime($weekStart)); ?>
				<a date="<?php echo date('Y-m-d', strtotime('+7 days',strtotime($weekStart))); ?>" class="date-trigger">
				<i class="fa fa-caret-right" aria-hidden="true"></i>
				</a>
			</div>
			<div class="rightCapBtns">
				<a class="load-calender" data-page="2" href="javascript:void(0);"><input class="btn <?php echo ($this->input->post('page') == 2 ? 'active' : ''); ?>" value="Day" type="button"></a>
				<a class="load-calender" data-page="1" href="javascript:void(0);"><input class="btn <?php echo ($this->input->post('page') == 1 ? 'active' : ''); ?>" value="Week" type="button"></a>
			</div>
			<div class="clearfix"></div>
		</caption>
		<thead>
			<tr>
				<th class="print">
					Time
				</th>
				<?php for ( $j = strtotime($weekStart); $j <= strtotime($weekEnd); $j = $j + 86400 ) { if(date('w', $j) != 5) { ?>
					<th scope="col" class="<?php echo(date('w', $j) == 4 ? 'next-holiday' : '') ?>">
						<?php echo date('D d/m', $j); ?>						
					</th>
				<?php }
				} ?>
			</tr>
		</thead> 
		<tbody>					<tr class="adv_counts">						<td class="print">					&nbsp;				</td>				<?php for ( $j = strtotime($weekStart); $j <= strtotime($weekEnd); $j = $j + 86400 ) { if(date('w', $j) != 5) { ?>					<td scope="col">						<?php 							/*15th Sep 2017*/														if(isset($appointment['date_vise_appnt_counts'][date('Y-m-d', $j)]))							{								echo $appointment['date_vise_appnt_counts'][date('Y-m-d', $j)];							}							else							{								echo "0";							}									echo "/";														echo $total_slots;						?>					</td>				<?php }				} ?>			</tr>																			
			<?php 
			$skip = array();						$start_time     = strtotime($branchRec->start_time);			$end_time    = strtotime($branchRec->end_time);
			$repeat = 1; 
			for ($i = $start_time; $i < $end_time; $i += $interval) { ?>
				<tr class="<?php echo ($repeat != 0 && $repeat%4 == 0 ? 'hourEnd' : '') ?>">
					<td><?php echo date('g:i A', $i); ?></td>
					<?php if ((check_time($branchRec->break2_start, $branchRec->break2_end, date('H:i:s', $i))) || (check_time($branchRec->break1_start, $branchRec->break1_end, date('H:i:s', $i)))) { ?>
						<td colspan="8" class="breakRow"><span class="breakTxt">Breaktime</span></td>
					<?php } else { 
						for ( $j = strtotime($weekStart); $j <= strtotime($weekEnd); $j = $j + 86400 ) { 
							$dateTime = date( 'Y-m-d', $j ).' '.date('H:i:s', $i);
							if(date('w', $j) != 5) { ?>	
								<?php 	
								if($this->session->userdata['user']['adviser_type'] == 1 && $repeat % 2 == 1)
								{
									array_push($skip,date("Y-m-d H:i:s", strtotime('+15mins', strtotime($dateTime))));
								}
								if(!in_array($dateTime, $skip)) {
									if(in_array($dateTime, $appointment['appointment_start_time'])) {
										$key = array_search($dateTime,$appointment['appointment_start_time']); ?>
                                        <?php
                                        $status = $appointment['status'][$key];

                                        $content = "<button class='btn btn-primary appointment-view'>Appointment View</button><button class='btn btn-info appointment-fulfilled' data-status='7'>Appointment Fulfilled</button><button class='btn btn-warning appointment-no-show' data-status='8'>Appointment Not Visited</button><button class='btn btn-danger appointment-cancel'>Appointment Canceled</button>";
                                        $colorClass = 'gray';
                                        if($status == 7) {
                                            $content = "<button class='btn btn-primary appointment-view'>Appointment View</button>";
                                            $colorClass = 'text-success';
                                        } elseif ($status == 8) {
                                            $content = "<button class='btn btn-primary appointment-view'>Appointment View</button>";
                                            $colorClass = 'fa fa-exclamation text-danger';
                                        } elseif ($status == 9) {
                                            $content = "<button class='btn btn-primary appointment-view'>Appointment View</button>";
                                            $colorClass = 'fa fa-plus plusBtn';
                                        }
                                        ?>
										<td class="review_appointment " date="<?php echo $dateTime; ?>" data-id="<?php echo $appointment['id'][$key]; ?>"><i class="fa fa-check <?php echo $colorClass; ?> initPopover" data-toggle="popover" data-trigger="focus" title="Appointment Action" data-content="<?php echo $content; ?>" data-html="true" data-placement="bottom" aria-hidden="true"></i></td>
									<?php } else { ?>
										<td class="appointment_time" date="<?php echo $dateTime; ?>"><span class="plusBtn" style="cursor:pointer;"><i class="fa fa-plus" aria-hidden="true" style="cursor:pointer;"></i></span></td>
									<?php } 
								} else {
									?><td>&nbsp;</td><?php
								}
							}
						}
					} 
					$repeat++; ?>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<input type="hidden" value="<?php echo date('Y-m-d', strtotime($weekStart)); ?>" class="weekStart"/>
