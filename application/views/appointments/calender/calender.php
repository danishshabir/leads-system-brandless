
<?php $date = ($data['date'] ? $data['date'] : date('y-m-d')); ?>
<div class="calTopCapton">
	<a date="<?php echo date('Y-m-d', strtotime('-3 month',strtotime($date))); ?>" class="calender-trigger calEvt left">
		<i class="fa fa-caret-left" aria-hidden="true"></i>
	</a>
	<span class="title">Calendar</span> <?php echo date('Y', strtotime($date)); ?>
	<a date="<?php echo date('Y-m-d', strtotime('+3 month',strtotime($date))); ?>" class="calender-trigger calEvt right">
		<i class="fa fa-caret-right" aria-hidden="true"></i>
	</a>
</div>
<div class="clearfix"></div>
<div class="calW_Day">
	<table>
		<thead>
			<tr>
				<th>S</th>
				<th>S</th>
				<th>M</th>
				<th>T</th>
				<th>W</th>
				<th>T</th>
				<th>F</th>
			</tr>
		</thead>
	</table>
</div>
<?php 
$type = (rights(40,'write') ? 2 : 1);
if($type == 2) {
	$q_count_appointment_intervals = count_appointment_intervals($this->input->post('branchId'), 900);
	$r_count_appointment_intervals = count_appointment_intervals($this->input->post('branchId'), 1800);
} elseif($type == 1) {
	$interval = ($this->session->userdata['user']['adviser_type']==0 ? 900 : 1800);
	$count_appointment_intervals = count_appointment_intervals($this->input->post('branchId'), $interval);
}
$start = $month = strtotime($date); 
$end = strtotime("+3 month", $start);
while($month < $end) 
{ 
	$cMonth = date('n', $month); 
	$cYear = date('Y', $month); 
	/*Check Appointments on Dates*/
	$appointment_array = array();
	$data['start_date'] = date("Y-m-d",strtotime($cYear."-".$cMonth."-01"));
	$data['end_date'] = date('Y-m-d', strtotime('+1 month',strtotime($data['start_date'])));
	if($type == 1) {
		$appointments = monthly_appointments($data);
		$appointment_array = array(
			'date' => []
		);
		if($appointments) {
			foreach($appointments as $key => $appointment) {
				$appointment_array['date'][$key] = $appointment->appointment_time;
				$appointment_array['total'][$key] = $appointment->total_count;
			}
		} 
	} elseif($type == 2) {
		/*Quick Adviser Appointments*/
		$data['type'] = 0;
		$q_appointments = monthly_appointments($data);
		$q_appointment_array = array(
			'date' => []
		);
		if($q_appointments) {
			foreach($q_appointments as $key => $appointment) {
				$q_appointment_array['date'][$key] = $appointment->appointment_time;
				$q_appointment_array['total'][$key] = $appointment->total_count;
			}
		} 
		/*Regular Adviser Appointments*/
		$data['type'] = 1;
		$r_appointments = monthly_appointments($data);
		$r_appointment_array = array(
			'date' => []
		);
		if($r_appointments) {
			foreach($r_appointments as $key => $appointment) {
				$r_appointment_array['date'][$key] = $appointment->appointment_time;
				$r_appointment_array['total'][$key] = $appointment->total_count;
			}
		} 
	}
	/*End*/
	?>
	<table class="monthLftCal">
		<thead>
			<tr>
				<td colspan="7" class="cMonth">
					<span class="whtBgMM"><?php echo date("F, Y",strtotime($cYear."-".$cMonth."-01")); ?></span>
				</td>
			</tr>
		</thead>
		<tbody>
			<?php 
				$first_day_timestamp = mktime(0,0,0,$cMonth,1,$cYear); // time stamp for first day of the month used to calculate 
				$maxday = date("t",$first_day_timestamp); // number of days in current month
				$thismonth = getdate($first_day_timestamp); // find out which day of the week the first date of the month is
				$startday = (int)$thismonth['wday']+2 ; // find week day and plus 2 to start from Saturday
				//if (!$thismonth['wday']) $startday = 7;	
				for ($i=1; $i<($maxday+$startday); $i++) {		
					if (($i % 7) == 1 ) { echo "<tr>"; }
						if ($i < $startday) { echo "<td>&nbsp;</td>"; continue; }
						$current_day = $i - $startday + 1;
						$calender_date = date("Y-m-d",strtotime($cYear."-".$cMonth."-".$current_day));
						$regular = true;
						?>
						<td class="<?php echo (date('w', strtotime($calender_date)) != 5 ? 'date-trigger' : 'left-cal-fri') ?>" date="<?php echo $calender_date; ?>"><?php echo $current_day; ?>
							<?php 
							if(date('w', strtotime($calender_date)) == 5) {/*Nothing*/}
							elseif($type == 1) { 
								if(in_array($calender_date, $appointment_array['date'])) {
								$key = array_search($calender_date,$appointment_array['date']);
								if($appointment_array['total'][$key] != $count_appointment_intervals) { ?>
									<div class='greenCir'></div>
								<?php } 
								} else {
									?><div class='greenCir'></div><?php
								}
							} elseif($type == 2) {
								$q_green_full = false;
								$r_green_full = false;
								if(in_array($calender_date, $q_appointment_array['date']))
								{
									$key = array_search($calender_date,$q_appointment_array['date']);

									if($q_appointment_array['total'][$key] == $q_count_appointment_intervals) {									
										$q_green_full = true;
									}
								}								
								if(in_array($calender_date, $r_appointment_array['date'])) 
								{
									$key = array_search($calender_date,$r_appointment_array['date']);
									if($r_appointment_array['total'][$key] == $r_count_appointment_intervals) { 
										$r_green_full = true;
									} 
								}								
								if(!$q_green_full || !$r_green_full) {
									?><div class='greenCir'></div><?php
								}
							} ?>
						</td>
						<?php if (($i % 7) == 0 ) echo "</tr>";
				}
				//close the last tr if it was not closed in the above loop.
				if(date('D',strtotime(date('Y-m-t',strtotime("".$cYear."-".$cMonth."-01")))) != "Fri")
				{
					echo "</tr>";
				}	
			?>
		</tbody>
	</table>
	<?php 
	$month = strtotime("+1 month", $month); 
} ?>