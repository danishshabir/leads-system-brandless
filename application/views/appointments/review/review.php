<?php 
if($appointment->appointment_vehicle_id)
{
	$vehicle_model = vehicle_model_name($appointment->appointment_vehicle_id); 
	$vehicle_n = $vehicle_model->model_name.' '.$vehicle_model->model_year;
}
else{
	$vehicle_type = vehicle_type_name($appointment->appointment_vehicle_type_id);
	$vehicle_n = $vehicle_type->name;
}
$vehicle_model = vehicle_model_name($appointment->appointment_vehicle_id); 
?>
<div class="row" style="padding-left: 20px; padding-right: 20px;">
	<div class="col-md-4 app_rev_flds">
		<label><strong>No.</strong></label>
		<p><?php echo $appointment->id; ?></p>
	</div>
	<div class="col-md-4 app_rev_flds">
		<label><strong>Name</strong></label>
		<p><?php echo $appointment->name; ?></p>
	</div>	
	<div class="col-md-4 app_rev_flds">
		<label><strong>Mobile</strong></label>
		<p><?php echo $appointment->mobile; ?></p>
	</div>
	<div class="col-md-4 app_rev_flds">
		<label><strong>Email</strong></label>
		<p><?php echo $appointment->email; ?></p>
	</div>	
	<div class="col-md-4 app_rev_flds">
		<label><strong>Vehicle</strong></label>
		<p><?php echo $vehicle_n; ?></p>
	</div>
	<div class="col-md-4 app_rev_flds">
		<label><strong>Assigned to</strong></label>
		<p><?php echo getEmployeeName($appointment->adviser_id).' '.($appointment->type == 1 ? '(Regular)' : '(Quick)'); ?></p>
	</div>			
	<div class="col-md-4 app_rev_flds">
		<label><strong>Check In</strong></label>
		<p><?php echo date('M j | g:i A', strtotime($appointment->appointment_start_time)) ?></p>
	</div>
	<div class="col-md-4 app_rev_flds">
		<label><strong>Status</strong></label>
		<?php if($appointment->status != NULL) { ?>
			<strong class="<?php if($appointment->status == 7) echo "green"; elseif($appointment->status == 8) echo "gray"; elseif($appointment->status == 9) echo "red"; ?>"><?php if($appointment->status == 7) echo "Fulfilled"; elseif($appointment->status == 8) echo "Not Visited"; elseif($appointment->status == 9) echo "Canceled"; elseif($appointment->status == 10 ) echo "Pending"; ?></strong>
		<?php } ?>
	</div>	
	<div class="col-md-4 app_rev_flds">
		<label><strong>Assigned by</strong></label>
		<p><?php echo getEmployeeName($appointment->created_by); ?></p>
	</div>
	<div class="col-md-4 app_rev_flds">
		<label><strong>Branch</strong></label>
		<p><?php echo getBranchName($appointment->branch_id); ?></p>
	</div>
	<?php if($appointment->status == NULL) { ?>
		<div class="col-md-12">
			<button class="btn white pull-left delete_appointment" data-id="<?php echo $appointment->id; ?>" type="button">Delete</button>
			<button class="btn edit_appointment" data-id="<?php echo $appointment->id; ?>" style="margin-left: 15px;" type="button">Edit</button>
			<div class="clearfix"></div>
		</div>
	<?php } ?>
</div>