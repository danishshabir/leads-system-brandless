<div class="tblRghtMain">
	<?php if(!$this->input->post('branchId')) { ?>
		<h2>Please select the branch from above form!</h2>
	<?php } else if(!$branchRec) { ?>
		<h2>Please set the branch timing!</h2>
	<?php } elseif(!$advisers) { ?>
		<h2>There is no service advisers in this branch!</h2>
	<?php } ?>
</div>