<style type="text/css">

    .popover-content {
        display: flex;
        justify-content: space-around;
        flex-wrap: wrap;
    }

    .popover-content > * {
        width: 48% !important;
        word-break: break-word;
        white-space: normal;
        padding: 10px;
    }

    .popover{
        width: 450px;
        max-width: 450px;
        min-width: 450px;
    }
    a.initPopover {
        display: block;
    }
</style>
<div class="tblRghtMain">
	<table cellspacing="0" class="dailyAdv" cellpadding="0" border="0">
		<caption>
			<div class="leftCapTop">
				<a date="<?php echo date('Y-m-d', strtotime((date('w', strtotime(date('Y-m-d', strtotime('-1 days',strtotime($weekStart))))) == 5 ? -2 : -1).' days',strtotime($weekStart))); ?>" class="date-trigger">
				<i class="fa fa-caret-left" aria-hidden="true"></i>
				</a>
				<?php echo date('D/jS M,Y', strtotime($weekStart)); ?>
				<a date="<?php echo date('Y-m-d', strtotime((date('w', strtotime(date('Y-m-d', strtotime('+1 days',strtotime($weekStart))))) == 5 ? +2 : +1).' days',strtotime($weekStart))); ?>" class="date-trigger">
				<i class="fa fa-caret-right" aria-hidden="true"></i>
				</a>
			</div>
			<div class="rightCapBtns">
				<a class="load-calender" data-page="4" href="javascript:void(0);"><input class="btn <?php echo ($this->input->post('page') == 4 ? 'active' : ''); ?>" value="Day" type="button"></a>
				<a class="load-calender" data-page="3" href="javascript:void(0);"><input class="btn <?php echo ($this->input->post('page') == 3 ? 'active' : ''); ?>" value="Week" type="button"></a>
			</div>
			<div class="clearfix"></div>
		</caption>
		<thead>
			<tr>
				<th>Time</td>
				<?php foreach($advisers as $adviser) { ?>
					<th><?php echo $adviser->full_name; ?></td>
				<?php } ?>
			</tr>
		</thead>
		<tbody>
			<?php 
			$skip = array();
			$start_time     = strtotime($branchRec->start_time);
			$end_time    = strtotime($branchRec->end_time);
			$repeat = 1; 
			for ($i = $start_time; $i < $end_time; $i += $interval) { ?>
				<tr class="<?php echo ($repeat != 0 && $repeat%4 == 0 ? 'hourEnd' : '') ?>">
					<td><?php echo date('g:i A', $i); ?></td>
					<?php if ((check_time($branchRec->break2_start, $branchRec->break2_end, date('H:i:s', $i))) || (check_time($branchRec->break1_start, $branchRec->break1_end, date('H:i:s', $i)))) { ?>
						<td colspan="<?php echo count($advisers); ?>"><p class="blue">Breaktime</p></td>
					<?php } else { 
						$dateTime = $weekStart.' '.date('H:i:s', $i); 
						foreach($advisers as $adviser) { 
							$adviser_id = $adviser->id;
							$appointment_times = (isset($appointment[$adviser_id]['appointment_start_time']) ? $appointment[$adviser_id]['appointment_start_time'] : []);
							if($adviser->adviser_type == 1 && $repeat % 2 == 1)
							{
								array_push($skip,$adviser_id.date("Y-m-d H:i:s", strtotime('+15mins', strtotime($dateTime))));
							}
							if(!in_array($adviser_id.$dateTime, $skip)) {
								if(in_array($dateTime, $appointment_times)) { 
									$key = array_search($dateTime,$appointment_times); ?>
                                    <?php
                                    $status = $appointment['status'][$key];
                                    $content = "<button class='btn btn-primary appointment-view'>Appointment View</button><button class='btn btn-info appointment-fulfilled' data-status='7'>Appointment Fulfilled</button><button class='btn btn-warning appointment-no-show' data-status='8'>Appointment Not Visited</button><button class='btn btn-danger appointment-cancel'>Appointment Canceled</button>";
                                    $colorClass = 'gray';
                                    if($status == 7) {
                                        $content = "<button class='btn btn-primary appointment-view'>Appointment View</button>";
                                        $colorClass = 'text-success';
                                        $text = 'Fulfilled';
                                    } elseif ($status == 8) {
                                        $content = "<button class='btn btn-primary appointment-view'>Appointment View</button>";
                                        $colorClass = 'text-danger';
                                        $text = 'Not Visited';
                                    } elseif ($status == 9) {
                                        $content = "<button class='btn btn-primary appointment-view'>Appointment View</button>";
                                        $colorClass = 'fa fa-plus plusBtn';
                                    } elseif ($status == 10) {
                                        $content = "<button class='btn btn-primary appointment-view'>Appointment View</button><button class='btn btn-info appointment-fulfilled' data-status='7'>Appointment Fulfilled</button><button class='btn btn-warning appointment-no-show' data-status='8'>Appointment Not Visited</button><button class='btn btn-danger appointment-cancel'>Appointment Canceled</button>";
                                        $colorClass = 'text-default';
                                        $text = "Pending";
                                    }
                                    ?>
									<td><p class=""><a href="javascript:void(0);" class="review_appointment initPopover <?php echo $colorClass; ?>" data-toggle="popover" data-trigger="focus" title="Appointment Action" data-content="<?php echo $content; ?>" data-html="true" data-placement="bottom" data-id="<?php echo $appointment['id'][$key]; ?>"><?php echo $appointment['name'][$key]; echo " [" . $text . "]"; ?></a></p></td>
								<?php } else { ?>
									<td class="appointment_time" date="<?php echo $dateTime; ?>" data-adviser="<?php echo $adviser_id;?>">
										<p class="blue">
											<span class="plusBtn emptyBox" style="cursor:pointer;">
												Empty
												<i class="fa fa-plus" aria-hidden="true" style="cursor:pointer;"></i>
												<span class="clearfix"></span>
											</span>
										</p>
									</td>
								<?php }
							} else {
								?><td><p class="blue">&nbsp;</p></td><?php
							}
						} 
					} ?>
				</tr>
				<?php 
				$repeat++;
			} ?>
		</tbody>
	</table>
</div>
<input type="hidden" value="<?php echo date('Y-m-d', strtotime($weekStart)); ?>" class="weekStart"/>