<section>
	<div class="container">
		<div class="leadsListingSec">
			<div class="addNewLeads">
				<div class="standardEdBtn dropdown">
					<a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
					<ul class="dropdown-menu" aria-labelledby="dLabel">
						<?php echo menu(); ?>
					</ul>
				</div>
			</div>
			<div class="leadsListingHead">
				<div class="row">
					<div class="col-sm-6">
						<h1>
							<i class="fa fa-file-text"></i> My Appointments 
							<?php
							if($this->session->userdata['user']['adviser_type']!=null) {
								if($this->session->userdata['user']['adviser_type']==0) 	echo " (Quick Advisor)";
								elseif($this->session->userdata['user']['adviser_type']==1) 
									echo " (Regular Advisor)";
							} elseif(rights(40,'write')) {
								echo " (Multi Advisers Calendar)";
							}
							?>
						</h1>
					</div>
					<div class="col-sm-6">&nbsp;</div>
				</div>
			</div>
			<div class="createNewLeadSec appointSelect">
				<form type="post" action="<?php echo base_url(); ?>appointment/insert" onsubmit="return false" class="form_submit">
					<div class="row">
						<div class="col-md-3">
							<input type="text" placeholder="Full Name*" id="full_name_txt" name="name" required/>
						</div>
						<div class="col-md-3">
							<input type="text" placeholder="Mobile Number*" name="mobile" id="mobile-number" required/>
						</div>
						<div class="col-md-3">
							<input type="text" placeholder="Email*" name="email" id="email" required/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<!--<select name="appointment_vehicle_id" required>
								<option value="">Vehicle*</option>
								<?php foreach($appointmentVehicles as $vehicle) { ?>	
									<optgroup label="<?php echo $vehicle['type_name']; ?>">
									<?php foreach($vehicle['models'] as $model){ ?>
										<option value="<?php echo $model->model_id; ?>"><?php echo $model->model_name.' '.$model->model_year; ?></option>
									<?php } ?>
									</optgroup>
								<?php } ?>	
							</select>-->
							
							<?php
							//==========Start also allowed to set the vehicle type========
							?>
							<style>
							.optionGroup {
								font-weight: bold;
								font-style: italic;
							}
								
							.optionChild {
								/*padding-left: 15px;*/
							}
							</style>
							<select name="appointment_vehicle_id" required>
								<option value="">Vehicle*</option>
								<?php foreach($appointmentVehicles as $key=>$vehicle) { ?>										
									 <option class="optionGroup" value="type-<?php echo $key; ?>"><?php echo $vehicle['type_name']; ?></option>
									<?php foreach($vehicle['models'] as $model){ ?>
										<option class="optionChild" value="model-<?php echo $model->model_id; ?>">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $model->model_name.' '.$model->model_year; ?></option>
									<?php } ?>
									
								<?php } ?>	
							</select>
							<?php 
							//==========End also allowed to set the vehicle type========
							?>
						</div>
						<div class="col-md-3">
							<select <?php if($this->session->userdata['user']['adviser_type']!=null) echo "disabled"; ?> name="branch_id" required>
								<option data-title-ar="" value="">Select Branch* </option>
								<?php foreach($service_branches as $branch ){ ?>
									<option <?php if($user_bid==$branch->id) {echo "selected";} ?> value="<?php echo $branch->id; ?>" data-title-ar="<?php echo $branch->branch_title_ar; ?>"><?php echo $branch->title; ?></option>
								<?php } ?>
							</select>
							<?php if($this->session->userdata['user']['adviser_type']!=null) { ?>
								<input type='hidden' name='branch_id' value='<?php echo $user_bid; ?>'>
                                <input type="hidden" name="branch_title_ar">
							<?php } ?>
						</div>
						<div class="col-md-3">
							<select onchange="$('[name=type]').val($(this).find(':selected').data('type'));" <?php if($this->session->userdata['user']['adviser_type']!=null) echo "disabled"; ?> name="adviser_id" required>
								<option data-type="" value="">Adviser*</option>
								<?php if($advisers) foreach($advisers as $adviser ){ ?>
								  <option data-type="<?php echo $adviser->adviser_type; ?>" <?php if($this->session->userdata['user']['id']==$adviser->id) echo "selected"; ?> value="<?php echo $adviser->id; ?>"><?php echo $adviser->full_name; ?></option>
								<?php } ?>
							</select>
						</div>
						<?php if($this->session->userdata['user']['adviser_type']!=null) { ?>
							<input type='hidden' name='adviser_id' value='<?php echo $this->session->userdata['user']['id']; ?>'>
						<?php } ?>
						<!--update this service_type value by selected advisor-->
						<?php
						if($this->session->userdata['user']['adviser_type'] != null) 
						{
							//show self calendar
							//check if reg or quick
							//$this->session->userdata['user']['adviser_type']==0/1	= 0 quick and 1 regular
							?>
							<input type='hidden' name='type' value='<?php echo $this->session->userdata['user']['adviser_type'];?>'>
							<?php
							
						}
						elseif(rights(40,'write'))
						{
							//show multi advisers calendar
							//value will be changed by user onchange
							?>						
							<input type='hidden' name='type' value=''>
							<?php
						}
						?>
						<input type="hidden" name="appointment_start_time"/>
						<input type="hidden" name="id"/>
						<input type="hidden" value="<?php echo $this->session->userdata('appointment_lead_id')?>" name="lead_id"/>
						<div class="col-md-3">
							<input type="button" value="Reset" 	class="btn white pull-left reset_form" />
							<input type="submit" value="Create" class="btn pull-right" />
							<div class="clearfix"></div>
						</div>
					</div>
				</form>
				<div class="row tablNCak">
					<div class="col-md-3 leftCalendarEd">
						<div id="leftSideMonthlyCalendars"> </div>
					</div>
					<div class="col-md-9 rightCalendarEd">
						<div id="shSec" class="showSec"></div>
						<div id="ajaxcalendar"> </div>
					</div>
				</div>
				<input type="hidden" value="<?php if($this->session->userdata['user']['adviser_type']!=null) echo '1'; elseif(rights(40,'write')) echo '3'; ?>" class="appointment_page"/>
				<style type="text/css">
					.leftCalendarEd {
						display: block;
					}
					.showSec {
						position: absolute;
						left: 10px;
						top: -3px;
					}
					.showSec:before {
						content: "<";
						font-family: monospace;
						font-size: 25px;
						color: #2397ce;
						cursor: pointer;
					}
					.showSec.showLeftSec:before {
						content: ">";
						font-family: monospace;
						font-size: 25px;
						color: #2397ce;
						cursor: pointer;
					}
					.ox-auto {
						overflow-x: auto;
						padding: 0 15px;
					}
				</style>
				<script>
                    var appointment_id = 0;
					if($(window).width() > 1200) {
						$('#ajaxcalendar').addClass('ox-auto');
						$('.showSec').on( "click", function() {
							if($('.leftCalendarEd').css('display') == 'block') {
								jQuery('.leftCalendarEd').hide();
								jQuery('.showSec').addClass('showLeftSec');
								jQuery('.rightCalendarEd').addClass('col-md-12');
								jQuery('.rightCalendarEd').removeClass('col-md-9');
							} else if($('.leftCalendarEd').css('display') == 'none') {
								jQuery('.showSec').removeClass('showLeftSec');
								jQuery('.leftCalendarEd').show();
								jQuery('.rightCalendarEd').addClass('col-md-9');
								jQuery('.rightCalendarEd').removeClass('col-md-12');
							}
						});
					}
					$(function () {
						calender.leftCalender();
						calender.appointmentCalender();
						$(document).on("click", ".date-trigger", function() { 
							var date = $(this).attr('date');
							calender.appointmentCalender(date);
						});
						$(document).on("click", ".calender-trigger", function() { 
							var date = $(this).attr('date');
							calender.leftCalender(date);
						});
						$(document).on("click", ".load-calender", function() { 
							$(".appointment_page").val($(this).attr('data-page'))
							calender.appointmentCalender($(".weekStart").val());
						});
						$(document).on("click", ".appointment_time", function() { 
							/*Store Appointment Time*/
							var date = $(this).attr('date');
							$("#ajaxcalendar table tbody tr td").find('span').removeClass('blue');
							$("[name=appointment_start_time]").val('');
							if(!$(this).hasClass('blue')) {
								$("[name=appointment_start_time]").val(date);
								$(this).find('span').first().addClass('blue');
							}
							/*Select Adviser Id*/
							if($(this).data('adviser') != undefined) $("[name=adviser_id]").val($(this).data('adviser'));
							/*Set Adviser Type*/
							$("[name=type]").val($("[name=adviser_id]").find(':selected').data('type'));
						});
						/*Update Appointment Status*/
						$(document).on("click", ".change_status", function() { 
							var $this = $(this);
							var id = $this.attr('data-id');
							var status = $this.attr('data-status');
							$.ajax({
								url: base_url + 'appointment/change_status',
								type: "POST",
								data: {
									id: id,
									status: status
								},
								dataType: "json",
								success: function(response) {
									if(response.success) calender.appointmentCalender($(".weekStart").val());
								}
							});
						});
						/*Update/Insert Appointment*/
						$(document).on("submit", ".form_submit", function() { 
							var $this = $(this);
							var url = ($("[name=id]").val() ? "<?php echo base_url(); ?>appointment/update" : "<?php echo base_url(); ?>appointment/insert");
							var branch_id = $('select[name="branch_id"]').val();
							var branch_title_ar = $('select[name="branch_id"]').find('option:selected').data('title-ar');
							var branch_title_en = $('select[name="branch_id"]').find('option:selected').text();
							if($("[name=appointment_start_time]").val()) {
								if($('#loader-content').length == 0)  $('body').prepend('<div id="loader-content"><div class="loader">Loading...</div></div>');
								$.ajax({
									type: "post",
									url: url,                          
									data: $this.serialize() + '&branchTitleAr='+branch_title_ar  + '&branchTitleEn='+branch_title_en,
									dataType: "json",
									success: function (arr) {
										$(".label-danger").remove();
										if(arr.success) {
											calender.appointmentCalender($(".weekStart").val());
											$this[0].reset();
											$("[name=lead_id], [name=id]").val("");
											$("[name=branch_id]").val(branch_id);
											$(".form_submit").find("input[type=submit]").val("Create");
											if(arr.sms_result == 'false') {
                                                $("#error_alert .message").html("SMS not sent to client. Please contact Administrator");
                                                $(".modal-title").text("SMS Error");
                                                $("#error_alert").modal('show');
                                                $('#loader-content').fadeOut('slow',function(){$(this).remove();});
                                            }
										} else {
										    if (arr.errors.appointment_counts) {
                                                $("#error_alert .message").html(arr.errors.appointment_counts);
                                            } else if(arr.errors.datetime_error) {
                                                $("#error_alert .message").html(arr.errors.datetime_error);
                                            } else {
											    $("#error_alert .message").html(arr.errors.appointment_start_time);
                                            }
											$("#error_alert").modal('show');
											$('#loader-content').fadeOut('slow',function(){$(this).remove();});
										}
									},
									error: function (xhr, text, error) {              
										alert('Error: ' + error);
									}
								});
							} else {
								$("#error_alert .message").html("Please select the appointment time!");
								$("#error_alert").modal('show');
							}
						});
						/*Advisers By Branch*/
						$("[name=branch_id]").on("change",function() {
							var branchId = $(this).val();
							$.ajax({
								type: "POST",
								url: base_url+'appointment/advisersByBid',
								data: {'bid' : branchId},
								dataType:"json",
								cache: false,
								success: function(result){	
									$("[name=adviser_id]").html("").append($('<option>', { value : '' }).text('Adviser*'));
									$.each(result.advisers, function(key, value) {  
										 $("[name=adviser_id]").append($('<option>', { value : value.id }).text(value.full_name+" "+(value.adviser_type == 1 ? "(Regular)" : "(Quick)")).attr('data-type', value.adviser_type)); 
									});
									calender.appointmentCalender();
									calender.leftCalender();
								}
							});
						});
						$("[name=adviser_id]").on("change",function() {
							/*Clear Appointment Time*/
							$("#ajaxcalendar table tbody tr td").find('span').removeClass('blue');
							$("[name=appointment_start_time]").val('');
						});
						/*Reset Appointment Form*/
						$(document).on("click", ".reset_form", function() { 
							$(this).closest('form')[0].reset();
						});
						/*Review Appointment Form*/
						$(document).on("click", ".review_appointment", function() {
                            var $this = $(this);
                            appointment_id = $this.data('id');
                            /*$("#appointment_action").modal("show");*/

                            /*$("#review_appointment").modal("show");
                            $.ajax({
                                url: base_url + 'appointment/review_appointment',
                                type: "POST",
                                data: {
                                    id: $this.data('id')
                                },
                                dataType: "json",
                                success: function(response) {
                                    $("#review_appointment").find('.modal-body:first').html(response.html);
                                }
                            });*/
						});

						// Farooq
                        /*View Appointment*/
                        $(document).on("click", ".appointment-view", function() {
                           $(".initPopover").popover('hide');
//                           $("#review_appointment").modal("show");
                           $.ajax({
                               url: base_url + 'appointment/review_appointment',
                               type: "POST",
                               data: {
                                   id: appointment_id
                               },
                               dataType: "json",
                               success: function(response) {
                                   $("#review_appointment").find('.modal-body:first').html(response.html);
                                   $("#review_appointment").modal("show");
                               }
                           });
                       });

                       $(document).on('click', '.appointment-fulfilled', function() {

                            $.ajax({
                                url: base_url + 'appointment/change_status',
                                type: "POST",
                                data: {
                                    id: appointment_id,
                                    status: 7
                                },
                                dataType: "json",
                                beforeSend: function() {
                                    $('body').prepend("<div id='loader-content'><div class='loader'>Loading...</div>");
                                },
                                success: function(response) {
                                    $('#loader-content').fadeOut('slow',function(){$(this).remove();});
                                    $(".initPopover").popover('hide');
                                    if(response.success) {
                                        var column = $('td[data-id="' + appointment_id + '"]');
                                        var icon = column.find('i');
                                        var content = '<button class=\'btn btn-primary appointment-view\'>Appointment View</button>';
                                        icon.removeClass('gray').addClass('text-success').attr('data-content', content);
                                        $('a.load-calender[data-page] .btn.active').parent().click();
                                    }
                                }
                            });
                       });

                        $(document).on('click', '.appointment-no-show', function() {

                            $.ajax({
                                url: base_url + 'appointment/change_status',
                                type: "POST",
                                data: {
                                    id: appointment_id,
                                    status: 8
                                },
                                dataType: "json",
                                beforeSend: function() {
                                    $('body').prepend("<div id='loader-content'><div class='loader'>Loading...</div>");
                                },
                                success: function(response) {
                                    $('#loader-content').fadeOut('slow',function(){$(this).remove();});
                                    $(".initPopover").popover('hide');
                                    if(response.success) {
                                        var column = $('td[data-id="' + appointment_id + '"]');
                                        var icon = column.find('i');
                                        var content = '<button class=\'btn btn-primary appointment-view\'>Appointment View</button>';
                                        icon.removeClass('gray').addClass('fa fa-exclamation text-danger').attr('data-content', content);
                                        $('a.load-calender[data-page] .btn.active').parent().click();
                                    }
                                }
                            });
                        });

                        $(document).on('click', '.appointment-cancel', function() {
                            var ask = window.confirm("Do you want to cancel this appointment ?");
                            if (ask == true) {
                                $.ajax({
                                    url: base_url + 'appointment/change_status',
                                    type: "POST",
                                    data: {
                                        id: appointment_id,
                                        status: 9
                                    },
                                    dataType: "json",
                                    beforeSend: function() {
                                        $('body').prepend("<div id='loader-content'><div class='loader'>Loading...</div>");
                                    },
                                    success: function(response) {
                                        $('#loader-content').fadeOut('slow',function(){$(this).remove();});
                                        $(".initPopover").popover('hide');
                                        if(response.success) {
                                            var column = $('td[data-id="' + appointment_id + '"]');
                                            var icon = column.find('i');
                                            var content = '<button class=\'btn btn-primary appointment-view\'>Appointment View</button>';
                                            icon.removeClass('gray').addClass('fa fa-plus plusBtn').attr('data-content', content);
                                            $('a.load-calender[data-page] .btn.active').parent().click();
                                        }
                                    }
                                });
                            }
                        });

						/*Delete Appointment*/
						$(document).on("click", ".delete_appointment", function() { 
							var $this = $(this);
							if (confirm("Are you sure you want to delete?")) {
								$.ajax({
									url: base_url + 'appointment/delete_appointment',
									type: "POST",
									data: {
										id: $this.data('id')
									},
									dataType: "json",
									success: function(response) {
										if(response.success) {
											$("#review_appointment").modal("hide");
											calender.appointmentCalender($(".weekStart").val());
										}
									}
								});
							}
							return false;
						});
						/*On Hover Appointment Status Show*/
						$(document).on({
							mouseover: function(){
								$(this).find('.appointment_status').removeClass('invisible');
							},
							mouseout: function(){
								$(this).find('.appointment_status').addClass('invisible');
							}
						}, '.tblRghtMain table tr.self-daily-appointment');
						/*Load Data For Edit Appointment*/
						$(document).on("click", ".edit_appointment", function() { 
							var $this = $(this);
							$(".review_appointment").html('<i class="fa fa-check gray" aria-hidden="true"></i>');
							$("#ajaxcalendar table tbody tr td").find('span').removeClass('blue');
							$(this).find('span').first().addClass('blue');
							$.ajax({
								url: base_url + 'appointment/review_appointment',
								type: "POST",
								data: {
									id: $this.data('id')
								},
								dataType: "json",
								success: function(response) {
									$("[name=id]").val(response.data.id);
									$("[name=type]").val(response.data.type);
									$("[name=name]").val(response.data.name);
									$("[name=mobile]").val(response.data.mobile);
									$("[name=email]").val(response.data.email);  // Farooq
									if(response.data.appointment_vehicle_id!=null) 
										$("[name=appointment_vehicle_id]").val("model-"+response.data.appointment_vehicle_id);
									else
										$("[name=appointment_vehicle_id]").val("type-"+response.data.appointment_vehicle_type_id);									
									$("[name=branch_id]").val(response.data.branch_id);
									$("[name=adviser_id]").val(response.data.adviser_id);
									$("[name=appointment_start_time]").val(response.data.appointment_start_time);
									$("[name=lead_id]").val(response.data.lead_id);
									$(".review_appointment[data-id="+response.data.id+"]").html('<span class="plusBtn blue"><i class="fa fa-plus" aria-hidden="true"></i></span>');
									$("#review_appointment").modal("hide");
									$(".form_submit").find("input[type=submit]").val("Update");
								}
							});
						});
						/*On Blur Mobile Number*/
						$(document).on("blur", "[name=mobile]", function() { 
							if($('#full_name_txt').val()=="")
							{
								var $this = $(this);
								if($('#loader-content').length == 0) $('body').prepend('<div id="loader-content"><div class="loader">Loading...</div></div>');
								$.ajax({
									url: base_url + 'appointment/fetch_name',
									type: "POST",
									data: {
										mobile: $this.val()
									},
									dataType: "json",
									success: function(response) {
										$("[name=name]").val(response.name);
										$('#loader-content').fadeOut('slow',function(){$(this).remove();});
									}
								});
							}
						});
					});
					calender = {
						leftCalender: function (date=null) {
							var branchId = $("[name=branch_id]").val();
							var appointment_page = $(".appointment_page").val();
							var adviser_id = $("[name=adviser_id]").val();
							if($('#loader-content').length == 0) $('body').prepend('<div id="loader-content"><div class="loader">Loading...</div></div>');
							$.ajax({
								url: base_url + 'appointment/ajaxMonthlyCalendars/',
								type: "POST",
								data: {
									date: date,
									branchId: branchId,
									page: appointment_page,
									adviser_id: adviser_id
								},
								dataType: "json",
								success: function(response) {
									$('#leftSideMonthlyCalendars').html(response.html);
									$('#loader-content').fadeOut('slow',function(){$(this).remove();});
								}
							});
						},
						appointmentCalender: function (date='') {
							var branchId = $("[name=branch_id]").val();
							var appointment_page = $(".appointment_page").val();
							var adviser_id = $("[name=adviser_id]").val();
							if($('#loader-content').length == 0) $('body').prepend('<div id="loader-content"><div class="loader">Loading...</div></div>');
							$.ajax({
								url: base_url + 'appointment/ajaxCalendar/',
								type: "POST",
								data: {
									weekStartDate: date,
									branchId: branchId,
									page: appointment_page,
									adviser_id: adviser_id
								},
								dataType: "json",
								success: function(response) {
									$('#ajaxcalendar').html(response.html);
									$('#loader-content').fadeOut('slow',function(){$(this).remove();});
								}
							});
						}
					};
				</script>
				<div class="clearfix"/>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="review_appointment">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
    	<div class="modal-header"> 
        	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">x</span></button> 
            <h4 id="mySmallModalLabel" class="modal-title"><i class="fa fa-share-square" aria-hidden="true"></i>&nbsp;View Appointment</h4>
         </div> 
         <div class="modal-body">
         	
         </div> 
     </div>
  </div>
</div>
<!-- Appointment Action Modal-->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="appointment_action">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">x</span></button>
                <h4 id="mySmallModalLabel" class="modal-title"><i class="fa fa-share-square" aria-hidden="true"></i>&nbsp;Appointment Action</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3"><button class="btn btn-primary appointment-view">Appointment View</button></div>
                    <div class="col-md-3"><button class="btn btn-info appointment-fulfilled" data-status="7">Appointment Fulfilled</button></div>
                    <div class="col-md-3"><button class="btn btn-warning appointment-no-show" data-status="8">Appointment Not Visited</button></div>
                    <div class="col-md-3"><button class="btn btn-danger delete_appointment">Appointment Canceled</button></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="error_alert">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header"> 
        	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">x</span></button> 
            <h4 id="mySmallModalLabel" class="modal-title"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Error</h4> 
         </div> 
         <div class="modal-body">
         	<h4 class="message"></h4>
         </div> 
     </div>
  </div>
</div>