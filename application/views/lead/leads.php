<?php 

if(isset($loadonlyleads) && $loadonlyleads==true)
{
	//do nothing	
}
else
{
		
		
?>

<section>
	<div class="container">
    	<div class="leadsListingSec">
		<?php if(!$dashboard) { ?>
           
		   <?php $menu = menu($managerView); 
		   if($menu)
		   {
		   ?>
		   <div class="addNewLeads">
            <div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo $menu; ?>
                    </ul>
                </div>
            </div>
			<?php } ?>
		
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6 col-xs-12">
                        <h1><i class="sprite sprite-menagment"></i> Lead Management</h1>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <ul>
                        
            <?php 
			
				$turnOnOff = "";
				if( rights(35,'read')) //if user have view all role then show option to on/off
				{
					$turnOffLeads = 1;
					if(isset($this->session->userdata['viewLeadStatus']) && $this->session->userdata['viewLeadStatus'] == 1){
						$turnOffLeads = 0;
					}else{
						$turnOffLeads = 1;
					}
					if($turnOffLeads == 0){
						 $turnOnOffV = 'On';
						 $colorOnOff = '#0097d1;';
					}else{
						 $turnOnOffV = 'Off';
						 $colorOnOff = '#e21111;';
					}

					$turnOnOff = '<li>
					
					<a style="color:'.$colorOnOff.' font-size: 16px;font-weight: bold;text-transform: capitalize;" title="Turn '.$turnOnOffV.' view all leads" href="javascript:void(0)" data-status="1" onClick="turnOffLeads('.$turnOffLeads.')">'; 
					$turnOnOff .= $turnOnOffV;
					$turnOnOff .= '</a></li>';
				}
					echo $turnOnOff;
			?>
                        
                            <?php if(rights(1,'delete')){ ?>
							<li><a href="javascript:void(0);" onClick="deleteCheckBoxesRec('leadCheckbox','lead/leadAction','<?php echo base_url();?>lead');"><img src="<?php echo base_url();?>assets/images/trashCan.png" alt="Del" height="20" width="40" /></a></li>
							<?php } ?>


							<?php if(rights(2,'write')){ ?>
							<li>
								<a style="display:none;" id="multiAssignPop" data-target=".assignlead" data-toggle="modal">
								<!--open assign-lead popup via js-->
								</a>
								<a href="javascript:void(0);" onclick="assignMultiLeads();">
									<img src="<?php echo base_url();?>assets/images/forward.png" title="Assign Multi Leads" alt="Forward" height="20" width="40" />
								</a>
							</li>
							<?php } ?>
                            
							<?php if(!$isSingleDetailLead) { ?>
							<li>
                            	<div class="dropdown leadCategoriesFilter">
                                    <a style="z-index:3;" id="leadCategories" href="javascript:void(0);"><!--  data-target="#"  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" 	-->
                                        <img src="<?php echo base_url();?>assets/images/leadDropDown.png" alt="list" height="20" width="40" />
                                    </a>
                                    <div style="z-index:2;" class="dropdown-menu leadCategories"><!--	 aria-labelledby="leadCategories"	-->
                                    	<h2>Browse Leads</h2>
                                        <ul class="parrentlist">
                                        	<li><a href="javascript:void(0)">Categories</a></li>
                                        	<li style="border-left:5px solid rgba(0,0,0,0)">
                                            	<a href="javascript:void(0)">View All</a>
                                                <ul class="innerDDown">
                                                    <li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/All/All'">View All</a></li>
<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/All/Approved and Archived'">Approved and Archived</a></li>
<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/All/Assigned'">Assigned</a></li>
<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/All/Closed'">Closed</a></li>
<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/All/Disapproved'">Disapproved</a></li>                          
<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/All/Finished'">Finished</a></li>
<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/All/No Response'">No Response</a></li>
<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/All/Not Assigned'">Not Assigned</a></li>
<!--<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/All/Started'">Started</a></li>-->
<!--<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/All/Paused'">Paused</a></li>-->                          
                                                </ul>
                                            </li>
                                        	
											<?php if($categories) foreach( $categories as $category ){ ?>
									
											
                                        	<li style="border-left:5px solid <?php echo getCategoryColor($category->id); ?>;">
											<a href="javascript:void(0)"><?php echo $category->title; ?></a>
                                                <ul class="innerDDown">                           

                                                    <li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/<?php echo $category->id ?>/All'">View All</a></li>
<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/<?php echo $category->id ?>/Approved and Archived'">Approved and Archived</a></li>
<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/<?php echo $category->id ?>/Assigned'">Assigned</a></li>
<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/<?php echo $category->id ?>/Closed'">Closed</a></li>
<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/<?php echo $category->id ?>/Disapproved'">Disapproved</a></li>  
<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/<?php echo $category->id ?>/Finished'">Finished</a></li>
<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/<?php echo $category->id ?>/No Response'">No Response</a></li>
<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/<?php echo $category->id ?>/Not Assigned'">Not Assigned</a></li>
<!--<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/<?php echo $category->id ?>/Started'">Started</a></li>-->
<!--<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/<?php echo $category->id ?>/Paused'">Paused</a></li>-->
                                                  
                                                </ul>
                                           </li>
										   <?php } ?>
                                        	
                                        </ul>
                                    </div>
                                </div>
                            </li>
							<?php } ?>


                        </ul>
                    </div>
               </div>            	
            </div>
			<?php } ?>
            <div class="leadsListingTable leadPgEd">      
				<!--sarfraz work-->
				<style>
					.leadsListingTable table thead tr.leadSelctTable th {padding:0 !important; border:1px solid white !important; line-height:0;}
					.leadsListingTable table thead tr.leadSelctTable th:first-child::before {border-bottom: 1px solid #fff;}
					.leadsListingTable table thead tr.leadSelctTable th select { display:block; max-width:100px; font-size:12px; line-height:1.5;border: 1px solid #ddd;}
				</style>
				<!--///-->
            	<table class="table table-hover tab-content" >
                	<thead>

						<!--sarfraz work-->
						<!--<tr class="leadSelctTable">
                        	<th>&nbsp;</th>
							<th>&nbsp;&nbsp;&nbsp;</th>
                        	<th>&nbsp;</th>
                        	<th>&nbsp;</th>
                        	<th>&nbsp;</th>
                        	<th>&nbsp;</th>
                        	<th>&nbsp;</th>
                        	<th>&nbsp;</th>
                        	<th colspan="2">
                            	<select id="duedate_selectbox" onchange="filterLeads('duedate',$(this),this.value);" >
                                    <option value="All">Please Select</option>
                                    <option value="Delayed">Delayed</option>
                                    <option value="Closed">Closed</option>
                                    <option value="Finished">Finished</option>
                                    <option value="Approved and Archived">Approved and Archived</option>
                                    <option value="Duplicated">Duplicated</option>
                                </select>
                            </th>
                        </tr>-->
						<!--///-->
                    	<tr>
                        	<th>
								<?php if(!$isSingleDetailLead) { ?>
								<input id="ChkAll" type="checkbox" name="CheckAll" value="1" ><label for="ChkAll"><span></span></label>
								<?php } ?>
                        	</th>
                        	<!--<th>No.</th>-->
							<th class="sortTh" data-ascdesc="Desc" style="min-width: 55px;padding-left: 10px; width: 65px;">
                            	
                                <?php if(!$isSingleDetailLead) { ?>
                                
                                <div class="leadSortList bellHeadTbl">
									<a href="javascript:void(0);" id="href_ntf" onclick="sortLeadsExcel('ntf','Desc');">
                                        <span class="" id="notibell7134">
                                                <i class="fa fa-bell"></i>
                                        </span>
                                        <span class="sortIconGen sortIcon_ntf">
                                            <i class="fa fa-sort" aria-hidden="true"></i>
                                        </span>
                                    </a>
								</div>
                                
                                <?php } ?>
                              
                            </th>
							<th>Track ID.</th>
                        	<th>Name</th>
                        	<!--<th>Email</th>-->
							<?php if(!$dashboard && !$managerView && !rights(35,'read')){ 
							?>
                        	<th>Phone/Mobile</th>  
							<?php } ?>
							
							<?php if(!$isSingleDetailLead) { ?>
							<!--<th class="sortTh" data-AscDesc='Asc' onClick="sortLeads('branch',$(this));">Branch <span><i class="fa fa-sort" aria-hidden="true"></i></span> <img src="<?php echo base_url();?>assets/images/black-loader.gif" alt="loading" id="loadingbranch" height="16" width="16" style="display:none;" /></th> -->
							


							<?php if( 
								rights(35,'read') 
								|| ($managerView && rights(40,'write') || $logged_in_user_id==136 ) 
								) { ?>

							<th class="sortTh" data-AscDesc='Asc'>
								<div class="leadSortList">
									<button id="btn_branch" class="button" type="button">Branch <span class="sortIconGen sortIcon_branch"><i class="fa fa-sort" aria-hidden="true"></i></span></button>
									<ul class="dropdown-menu"><!--  aria-labelledby="dLabel" -->
										<li class="short-az" onclick="sortLeadsExcel('branch','Asc');">Sort A to Z</li>
										<li class="short-za" onclick="sortLeadsExcel('branch','Desc');">Sort Z to A</li>
										<li>
											<div class="advFilterList">
												Filters
												<div class="searchFilter"><input type="text" id="branch_search" placeholder="Search" />
												
												<script>
												  $( function() {
													var availableTags = [
														<?php foreach($branchDDB as $branchr)
														{ ?>
														  {
															value: "<?php echo $branchr->bid; ?>",
															label: "<?php echo $branchr->branch_name; ?>",														
														  },
														<?php } ?>													 
													];
													$( "#branch_search" ).autocomplete({
													  source: availableTags,
													  select: function( event, ui ) {
															if(ui.item)
															{
															  event.preventDefault();
															  //alert(ui.item.value);
															  $('#branch_search').val('');
															  $('#chk_a_'+ui.item.value+'').prop('checked', true);

															  $('#branch_scroll').animate({
																	scrollTop: 0
															  }, 0);

															  $('#branch_scroll').animate({
																	scrollTop: $("#li_branch_scroll_"+ui.item.value+"").offset().top-360
															  }, 500);
															  return false;
															}
													  }
													});
												  } );
												 </script>
												
												</div>
												<div class="fixHeight" id="branch_scroll">
													<ul id="branchLis">

													<li id="li_branch_scroll_<?php echo "0"; ?>"><input class="filterChks branch" id="chk_a_<?php echo "0"; ?>" type="checkbox" value="branch-<?php echo "0"; ?>|<?php echo $noBranchCount->nobranch_count; ?>|"><label for="chk_a_<?php echo "0"; ?>"><span></span><?php echo "N/A"; ?> 
													(<?php echo $noBranchCount->nobranch_count; ?>)</label></li>

													<?php foreach($branchDDB as $branchr)
													{ ?>													
													
													<li id="li_branch_scroll_<?php echo $branchr->bid; ?>"><input class="filterChks branch" id="chk_a_<?php echo $branchr->bid; ?>" type="checkbox" value="branch-<?php echo $branchr->bid; ?>|<?php echo $branchr->branch_count; ?>|"><label for="chk_a_<?php echo $branchr->bid; ?>"><span></span><?php echo $branchr->branch_name; ?> (<?php echo $branchr->branch_count; ?>)</label></li>

													<?php } ?>
														
													</ul>
												</div>
												<img src="<?php echo base_url();?>assets/images/black-loader.gif" alt="loading" id="loadingbranch" height="16" width="16" style="display:none;" />
												<div class="btnSecAdv">													
													<a href="javascript:void(0);" onclick="filterLeadsExcel('branch');"><input type="button" class="btn standardEd" value="Ok" /></a>
													<!--<a href="javascript:void(0);"><input type="button" class="btn cancelBtnEd" value="Cancel" /></a>-->
													<a href="javascript:void(0);" onclick="document.location.href=window.location.pathname;"><input type="button" class="btn resetEdBtn" value="Reset" /></a>
												</div>
											</div>
										</li>
									</ul>
								</div>								
							</th>

							<?php }else{ ?>
							
							
							<?php if(!$managerView){ ?>							
							<!--<th>Branch </th>-->
							<?php } ?>
							
							
							<?php } ?>



							<?php }else { ?>
                            
	                            <?php if(rights(35,'read')){ ?>
									<th>Branch </th>
                                <?php } ?>
							<?php } ?>

							<?php if(!$isSingleDetailLead) { ?>
							
							<!--<th class="sortTh" data-AscDesc='Asc' onClick="sortLeads('source',$(this));">Source <span><i class="fa fa-sort" aria-hidden="true"></i></span> <img src="<?php echo base_url();?>assets/images/black-loader.gif" alt="loading" id="loadingsource" height="16" width="16" style="display:none;" /></th> -->							

							<?php if($managerView){ ?>

							<th class="sortTh" data-AscDesc='Asc'>
								<div class="leadSortList">
									<button id="btn_source" class="button" type="button">Source <span class="sortIconGen sortIcon_source"><i class="fa fa-sort" aria-hidden="true"></i></span></button>
									<ul class="dropdown-menu"><!--  aria-labelledby="dLabel" -->
										<!--<li class="short-az" onclick="sortLeadsExcel('source','Asc');">Sort A to Z</li>
										<li class="short-za" onclick="sortLeadsExcel('source','Desc');">Sort Z to A</li>-->
										<li>
											<div class="advFilterList">
												Filters
												<!--<div class="searchFilter"><input type="text" id="source_search" placeholder="Search" />		
												
												</div>-->	
												<div class="fixHeight" id="source_scroll">
													<ul id="sourceLis">
													
													<?php if($sourceDDB) foreach($sourceDDB as $sourcer)
													{ ?>													
													
													<li id="li_source_scroll_manual_source<?php echo $sourcer->msid; ?>"><input class="filterChks source" id="chk_b_manual_source<?php echo $sourcer->msid; ?>" type="checkbox" value="source::manual_source<?php echo $sourcer->msid; ?>|<?php echo $sourcer->msource_count; ?>|"><label for="chk_b_manual_source<?php echo $sourcer->msid; ?>"><span></span>
														
														<?php 
														/*if(strpos($sourceId, "manual_source")!==false){ echo getSourceTitle(str_replace("manual_source","",$sourceId)); }
														elseif(strpos($sourceId, 'created_by')!==false){ echo getCreaterNameById(str_replace("created_by","",$sourceId)); }
														else{echo $sourceId;}*/
														echo $sourcer->msource_name;
														?>
														<?php //echo (is_numeric($sourceId)? getEmployeeName($sourceId) : $sourceId) ?> 
														
														(<?php 
														//echo $sourceDDBCounter[$sourceId]; 
														echo $sourcer->msource_count;
														?>)
													</label></li>

													<?php } ?>
													
													<?php if($formTypeDDB) foreach($formTypeDDB as $formtyper)
													{ ?>													
													
													<li id="li_source_scroll_<?php echo $formtyper->form_type; ?>"><input class="filterChks source" id="chk_b_<?php echo $formtyper->form_type; ?>" type="checkbox" value="source::<?php echo $formtyper->form_type; ?>|<?php echo $formtyper->form_type_count; ?>|"><label for="chk_b_<?php echo $formtyper->form_type; ?>"><span></span>
														
														<?php 														
														echo $formtyper->form_type;
														?>														
														
														(<?php 
														//echo $sourceDDBCounter[$sourceId]; 
														echo $formtyper->form_type_count;
														?>)
													</label></li>

													<?php } ?>
														
													</ul>
												</div>
												<img src="<?php echo base_url();?>assets/images/black-loader.gif" alt="loading" id="loadingsource" height="16" width="16" style="display:none;" />
												<div class="btnSecAdv">													
													<a href="javascript:void(0);" onclick="filterLeadsExcel('source');"><input type="button" class="btn standardEd" value="Ok" /></a>
													<!--<a href="javascript:void(0);"><input type="button" class="btn cancelBtnEd" value="Cancel" /></a>-->
													<a href="javascript:void(0);" onclick="document.location.href=window.location.pathname;"><input type="button" class="btn resetEdBtn" value="Reset" /></a>
												</div>
											</div>
										</li>
									</ul>
								</div>								
							</th>
							<?php }else{ ?>
								<th>Source </th>
							<?php } ?>




							<?php }else { ?>
								<th>Source </th>
							<?php } ?>

							
							
							
							
							
							<?php if(!$isSingleDetailLead) { ?>
							
							<?php 
							//if($managerView || rights(35,'read')){
							if(rights(35,'read')){
							?>
							
								<!--<th>Event &nbsp;&nbsp;&nbsp;</th>-->




									<th class="sortTh" data-AscDesc='Asc'>
										<div class="leadSortList">
											<button id="btn_event" class="button" type="button"><!--Event-->Mkt. Activity <span class="sortIconGen sortIcon_event"><i class="fa fa-sort" aria-hidden="true"></i></span></button>
											<ul class="dropdown-menu"><!--  aria-labelledby="dLabel" -->
												<li class="short-az" onclick="sortLeadsExcel('event','Asc');">Sort A to Z</li>
												<li class="short-za" onclick="sortLeadsExcel('event','Desc');">Sort Z to A</li>
												<li>
													<div class="advFilterList">
														Filters
														<div class="searchFilter"><input type="text" id="event_search" placeholder="Search" />
														
														<script>
														  $( function() {
															var availableTags = [
																<?php if($eventDDB) foreach($eventDDB as $eventr)
																{ ?>
																  {
																	value: "<?php echo $eventr->eid; ?>",
																	label: "<?php echo $eventr->event_name; ?>",														
																  },
																<?php } ?>													 
															];
															$( "#event_search" ).autocomplete({
															  source: availableTags,
															  select: function( event, ui ) {
																	if(ui.item)
																	{
																	  event.preventDefault();
																	  //alert(ui.item.value);
																	  $('#event_search').val('');
																	  $('#chk_e_'+ui.item.value+'').prop('checked', true);

																	  $('#event_scroll').animate({
																			scrollTop: 0
																	  }, 0);

																	  $('#event_scroll').animate({
																			scrollTop: $("#li_event_scroll_"+ui.item.value+"").offset().top-360
																	  }, 500);
																	  return false;
																	}
															  }
															});
														  } );
														 </script>
														
														</div>
														<div class="fixHeight" id="event_scroll">
															<ul id="eventLis">
															<?php if($eventDDB) foreach($eventDDB as $eventr)
															{ ?>													
															
															<li id="li_event_scroll_<?php echo $eventr->eid; ?>"><input class="filterChks event" id="chk_e_<?php echo $eventr->eid; ?>" type="checkbox" value="event-<?php echo $eventr->eid; ?>|<?php echo $eventr->event_count; ?>|"><label for="chk_e_<?php echo $eventr->eid; ?>"><span></span><?php echo $eventr->event_name; ?> (<?php echo $eventr->event_count; ?>)</label></li>

															<?php } ?>
																
															</ul>
														</div>
														<img src="<?php echo base_url();?>assets/images/black-loader.gif" alt="loading" id="loadingevent" height="16" width="16" style="display:none;" />
														<div class="btnSecAdv">													
															<a href="javascript:void(0);" onclick="filterLeadsExcel('event');"><input type="button" class="btn standardEd" value="Ok" /></a>
															<!--<a href="javascript:void(0);"><input type="button" class="btn cancelBtnEd" value="Cancel" /></a>-->
															<a href="javascript:void(0);" onclick="document.location.href=window.location.pathname;"><input type="button" class="btn resetEdBtn" value="Reset" /></a>
														</div>
													</div>
												</li>
											</ul>
										</div>								
									</th>














							<?php } } ?>
							
							
							
							
							
							<?php if(!$isSingleDetailLead) { ?>
							
							<?php if(!$managerView){ ?>
							<!--<th class="sortTh" data-AscDesc='Asc' onClick="sortLeads('assignto',$(this));">
								Assign to <span><i class="fa fa-sort" aria-hidden="true"></i></span>
								<img src="<?php echo base_url();?>assets/images/black-loader.gif" alt="loading" id="loadingassignto" height="16" width="16" style="display:none;" /></th>	-->
								<th>Assign to </th>
							<?php }else { ?>
							
							<th class="sortTh" data-AscDesc='Asc'>
								<div class="leadSortList">
									<button id="btn_assignto" class="button" type="button">Assign to <span class="sortIconGen sortIcon_assignto"><i class="fa fa-sort" aria-hidden="true"></i></span></button>
									<ul class="dropdown-menu"><!--  aria-labelledby="dLabel" -->
										<li class="short-az" onclick="sortLeadsExcel('assignto','Asc');">Sort A to Z</li>
										<li class="short-za" onclick="sortLeadsExcel('assignto','Desc');">Sort Z to A</li>
										<li>
											<div class="advFilterList">
												Filters
												<div class="searchFilter"><input type="text" id="assignto_search" placeholder="Search" />
												
												<script>
												  $( function() {
													var availableTags = [
														<?php foreach($assignToDDB as $assignr)
														{ ?>
														  {
															value: "<?php echo $assignr->uid; ?>",
															label: "<?php echo $assignr->full_name; ?>",														
														  },
														<?php } ?>													 
													];
													$( "#assignto_search" ).autocomplete({
													  source: availableTags,
													  select: function( event, ui ) {
															if(ui.item)
															{
															  event.preventDefault();
															  //alert(ui.item.value);
															  $('#assignto_search').val('');
															  $('#chk_c_'+ui.item.value+'').prop('checked', true);
															  //$('#chk_c_'+ui.item.value+'').focus();

															  $('#assignto_scroll').animate({
																	scrollTop: 0
															  }, 0);

															  $('#assignto_scroll').animate({
																	scrollTop: $("#li_assignto_scroll_"+ui.item.value+"").offset().top-360
															  }, 500);
															  return false;
															}
													  }
													});
												  } );
												 </script>
												
												</div>
												<div class="fixHeight" id="assignto_scroll">
													<ul id="assignToLis">
																										
													<li id="li_assignto_scroll_<?php echo "0"; ?>"><input class="filterChks assignto" id="chk_c_<?php echo "0"; ?>" type="checkbox" value="assignto-<?php echo "0"; ?>|<?php echo $noAssignToCount->noassign_count; ?>|"><label for="chk_c_<?php echo "0"; ?>"><span></span><?php echo "Unassigned"; ?> (<?php echo $noAssignToCount->noassign_count; ?>)</label></li>

													<?php if($assignToDDB) foreach($assignToDDB as $assignr)
													{ ?>													
													
													<li id="li_assignto_scroll_<?php echo $assignr->uid; ?>"><input class="filterChks assignto" id="chk_c_<?php echo $assignr->uid; ?>" type="checkbox" value="assignto-<?php echo $assignr->uid; ?>|<?php echo $assignr->count; ?>|"><label for="chk_c_<?php echo $assignr->uid; ?>"><span></span><?php echo $assignr->full_name; ?> (<?php echo $assignr->count; ?>)</label></li>

													<?php } ?>
														
													</ul>
												</div>
												<img src="<?php echo base_url();?>assets/images/black-loader.gif" alt="loading" id="loadingassignto" height="16" width="16" style="display:none;" />
												<div class="btnSecAdv">													
													<a href="javascript:void(0);" onclick="filterLeadsExcel('assignto');"><input type="button" class="btn standardEd" value="Ok" /></a>
													<!--<a href="javascript:void(0);"><input type="button" class="btn cancelBtnEd" value="Cancel" /></a>-->
													<a href="javascript:void(0);" onclick="document.location.href=window.location.pathname;"><input type="button" class="btn resetEdBtn" value="Reset" /></a>
												</div>
											</div>
										</li>
									</ul>
								</div>
								<!--<select title="Counter is including connected leads too" style="width:90px" id="assignto_selectbox" onchange="filterLeads('assignto',$(this),this.value);" >
									<option value="All">Assign to</option>
									<option value="assignto-0">Unassigned</option>
									<?php /* ?>
									<?php foreach($assignToDDB as $assigneeId)
									{ ?>
									<option value="assignto-<?php echo $assigneeId; ?>|<?php echo $assignToDDBCounter[$assigneeId]; ?>|<?php echo $assignToDDBCounter[$assigneeId]-$assignToDDBCounterWOC[$assigneeId]?>"><?php echo getEmployeeName($assigneeId) ?> (<?php echo $assignToDDBCounter[$assigneeId]; ?>)</option>
									<?php } ?>
									<?php */ ?>
								</select>-->								
							</th>
							<?php } ?>
							<?php }else { ?>
								<th>Assign to </th>
							<?php } ?>
                        	<!--<th class="sortTh" data-AscDesc='Asc' onClick="sortLeads('duedate',$(this));">Due Date <span><i class="fa fa-sort" aria-hidden="true"></i></span> <img src="<?php echo base_url();?>assets/images/black-loader.gif" alt="loading" id="loadingduedate" height="16" width="16" style="display:none;" />-->

							<?php if(!$isSingleDetailLead) { ?>




							<th class="sortTh" data-AscDesc='Asc'>
								<div class="leadSortList">
									<button id="btn_duedate" class="button" type="button">Lead Age <span class="sortIconGen sortIcon_duedate"><i class="fa fa-sort" aria-hidden="true"></i></span></button>
									<ul class="dropdown-menu"><!--  aria-labelledby="dLabel" -->
										<li class="short-az" onclick="sortLeadsExcel('duedate','Asc');">Sort A to Z</li>
										<li class="short-za" onclick="sortLeadsExcel('duedate','Desc');">Sort Z to A</li>
										<li>
											<div class="advFilterList">
												Filters
												<!--<div class="searchFilter"><input type="text" id="duedate_search" placeholder="Search" /></div>-->
												<div class="fixHeight">
													<ul>													
													
														<li><input class="filterChks duedate" id="chk_d_<?php echo $statusCounter[0]->status; ?>" type="checkbox" value="<?php echo $statusCounter[0]->status; ?>|<?php echo $statusCounter[0]->status_count;?>|"><label for="chk_d_<?php echo $statusCounter[0]->status; ?>"><span></span><?php echo $statusCounter[0]->status;?> (<?php echo $statusCounter[0]->status_count;?>)</label></li>
														
														<li><input class="filterChks duedate" id="chk_d_<?php echo $statusCounter2[0]->status; ?>" type="checkbox" value="<?php echo $statusCounter2[0]->status; ?>|<?php echo $statusCounter2[0]->status_count;?>|"><label for="chk_d_<?php echo $statusCounter2[0]->status; ?>"><span></span><?php echo $statusCounter2[0]->status;?> (<?php echo $statusCounter2[0]->status_count;?>)</label></li>
														
																												
														<li><input class="filterChks duedate" id="chk_d_2delayed" type="checkbox" value="Delayed|<?php echo $delayedCounter[0]->delayed_count; ?>|"><label for="chk_d_2delayed"><span></span>Delayed (<?php echo $delayedCounter[0]->delayed_count; ?>)</label></li>

														<?php /*if(rights(35,'read') && false){ ?>
														<li><label for="chk_d_5"><span></span>Duplicated (<?php echo $duplicatedCounter;?>)</label></li>
														<?php } */?>

														

														<li><input class="filterChks duedate" id="chk_d_4upcoming" type="checkbox" value="Upcomming|<?php echo $upComingCounter[0]->up_count; ?>|"><label for="chk_d_4upcoming"><span></span>Upcomming Due (<?php echo $upComingCounter[0]->up_count; ?>)</label></li>
                                                        
                                                        <?php if( rights(35,'read') and $this->session->userdata['user']['id']==="132" ){ ?>
                                                        <li><input class="filterChks duedate" id="chk_d_4survey" type="checkbox" value="SurveyDC|<?php echo $surveyDCCounter->sur_count; ?>|"><label for="chk_d_4survey"><span></span>Survey Not Filled (ROI) (<?php echo $surveyDCCounter->sur_count; ?>)</label></li>
                                                        <?php } ?>	

																												
														
													</ul>
												</div>
												<img src="<?php echo base_url();?>assets/images/black-loader.gif" alt="loading" id="loadingduedate" height="16" width="16" style="display:none;" />
												<div class="btnSecAdv">													
													<a href="javascript:void(0);" onclick="filterLeadsExcel('duedate');"><input type="button" class="btn standardEd" value="Ok" /></a>
													<!--<a href="javascript:void(0);"><input type="button" class="btn cancelBtnEd" value="Cancel" /></a>-->
													<a href="javascript:void(0);" onclick="document.location.href=window.location.pathname;"><input type="button" class="btn resetEdBtn" value="Reset" /></a>
												</div>
											</div>
										</li>
									</ul>
								</div>								
							</th>														
				<!--<select title="Counter is including connected leads too" style="width:90px" id="duedate_selectbox" onchange="filterLeads('duedate',$(this),this.value);" >
					<option value="All" title="Due Date">Select Filter</option>
					<option value="Approved and Archived|<?php //echo $approvedCounterWoc;?>|<?php //echo $approvedCounter-$approvedCounterWoc;?>">Approved and Archived (<?php //echo $approvedCounter;?>)</option>
					<option value="Closed|<?php //echo $closedCounterWoc;?>|<?php //echo $closedCounter-$closedCounterWoc;?>">Closed (<?php //echo $closedCounter;?>)</option>
					<option value="Delayed|<?php //echo $delayedCounterWoc;?>|<?php //echo $delayedCounter-$delayedCounterWoc;?>">Delayed (<?php //echo $delayedCounter;?>)</option>
					<option value="Duplicated|<?php //echo $duplicatedCounterWoc;?>|<?php //echo $duplicatedCounter-$duplicatedCounterWoc;?>">Duplicated (<?php //echo $duplicatedCounter;?>)</option>
					<option value="Finished|<?php //echo $finishedCounterWoc;?>|<?php //echo $finishedCounter-$finishedCounterWoc;?>">Finished (<?php //echo $finishedCounter;?>)</option>
					<option value="Upcomming|<?php //echo $upcommingCounterWoc;?>|<?php //echo $upcommingCounter-$upcommingCounterWoc;?>">Upcomming Due (<?php //echo $upcommingCounter;?>)</option>
				</select>-->
								<!--<img src="<?php echo base_url();?>assets/images/black-loader.gif" alt="loading" id="loadingduedate" height="16" width="16" style="display:none;" />-->
							</th>
							<?php }else { ?>
								<th>Lead Age</th>
							<?php } ?>

                        	<th>&nbsp;</th>
                        </tr>
                    </thead>                    
                    <tbody id="ajaxLoadMoreTBody">
                    
                    <?php 
					} //same trs are used in ajax load more

					 $i=count($leads);
					 	
					 if($leads)
					 {
						 
					 foreach($leads as $lead){ 
					 
						//===
						//$hasNoActionCls = "";
						$hasNoActionStyle = "";
						 if(isset($leadIdsNoActionArr) && count($leadIdsNoActionArr)>0)
						 {
							 if(in_array($lead->id,$leadIdsNoActionArr))
							 {
								 //$hasNoActionCls = " noactionhl";
								 $hasNoActionStyle = " color:red; ";
								 
							 }						 
						 }
					 
					 ?>
                    	<tr class="First">
							
							<?php //$catColor = "border-right: 5px solid ".getCategoryColor($lead->category_id).";"; ?>

							<?php $catColor = getCategoryColor($lead->category_id); ?>

                            <td class="fillCB" data-colorFillCB="<?php echo getCategoryColor($lead->category_id); ?>">
							
									<?php if(!$dashboard && !$isSingleDetailLead) { ?>
									
										<input id="checkbox<?php echo $lead->id;?>" type="checkbox" name="leadCheckbox[]" class="leadCheckbox" value="<?php echo $lead->id; ?>" >
										<label for="checkbox<?php echo $lead->id;?>"><span></span></label>

										<div class="color5pxBorder" style="background-color:<?php echo $catColor; ?>"></div>

									<?php }else{ ?>

										<div class="color5pxBorder" style="background-color:<?php echo $catColor; ?>"></div>

									<?php } ?>


							</td>
                            <!--<td id="<?php if($isSingleDetailLead) echo 'singleDetailLead' ?>" class="rowClick" data-toggle="collapse" data-target=".LeadsMangDtl_<?php echo $lead->id;?>"><?php echo $i; ?></td>-->

							<!--<td id="<?php if($isSingleDetailLead) echo 'singleDetailLead' ?>" class="rowClick" data-toggle="collapse" data-target=".LeadsMangDtl_<?php echo $lead->id;?>">-->
                            <td>
                            &nbsp;

							<?php 
							//$searchedOrFiltered this condition will always be true now.
							if(!$searchedOrFiltered){ ?>
								<?php 
									$subCount = 0;
									//if($lead->new_lead_with_same_email && ($lead->created_by === $logged_in_user_id OR $lead->created_by === "0"))
									$subCount = getSubLeads($lead->email,$lead->mobile,$lead->id); 
									if($subCount) $subCount = count($subCount); 
										//if($subCount>1) {
										if($subCount>0) {
											?>
                                            <p title="Click to show older leads of this customer" data-toggle="collapse" data-target=".LeadsMangDtl_<?php echo $lead->id;?>" class="leadPlus rowClick LoadMoreIcon"><?php echo $subCount; ?></p>
                                             <?php
										} elseif($subCount===1) {
											?>
                                            <img title="Click to show older leads of this customer" class="leadPlus rowClick" data-toggle="collapse" data-target=".LeadsMangDtl_<?php echo $lead->id;?>" src='<?php echo base_url(); ?>assets/images/leadPlus.png' alt='Nested lead' />
                                            <?php
										}
								?>
										

								<span class="<?php if($subCount>1) echo "mt2ch"; ?>" id="notibell<?php echo $lead->id; ?>">
									<?php
									//check if this lead has any unread notification for this user who is logged in.
									if(checkLeadHasUnreadNotifications($lead->id))
									{
										?>
											<i class="fa fa-bell"></i>
										<?php
									}
									?>
								</span>

							<?php } ?>
							
							</td>
                            
							
							<?php rowLead($isSingleDetailLead, $lead, $i, ".LeadsMangDtl_", "rowClick", $dashboard,$logged_in_user_id, $hasNoActionStyle); ?>


                        </tr>
						
						
						<tr style="display:none;" class="selfData collapse CollapseJsOuter LeadsMangDtl_<?php echo $lead->id;?>" id="">    <!--   SUbs -->
                            <td colspan="11" class="DetailBox">
                            
							
                            <table class="fisrtTable Upper1">
                            	<tr>
                                	<td colspan="10" style="position:relative;">
										<div class="colorDiv" style="background-color:<?php echo getCategoryColor($lead->category_id); ?>;"></div>

									
                                    <div id="subCountDetails<?php echo $lead->id; ?>" style="" class="detailSecLead">
                                    <div class="row BorderBottom">
                                    	<div class="col-md-6"><h2>Details</h2><img src="http://lm.mercedesbenzksa.com/mb/assets/images/black-loader.gif" alt="loading" id="loadingAjaxDetail<?php echo $lead->id; ?>" height="16" width="16" style="display: none;"></div>
                                        <div class="col-md-6">
                                        <ul class="topBtnList">
                                            <!--<li><a class="OtherLeadsLink" href="javascript:void(0);">Other Leads</a></li>-->
                                            <!--<li><a href="javascript:void(0);"><img width="40" height="20" alt="Del" src="<?php echo base_url();?>assets/images/trashCan.png"></a></li>-->
											<?php 
											
											if(rights(2,'write')){ ?>
												<li>
													<a data-target=".assignlead" data-toggle="modal" onclick="beforeAssignPopup('<?php echo $lead->id; ?>','<?php echo getCityName($lead->city_id); ?>', '<?php echo getBranchName($lead->branch_id); ?>', '<?php echo $lead->city_id; ?>','<?php echo $lead->branch_id; ?>');">
														<img width="40" height="20" alt="Forward" title="Assign Lead" src="<?php echo base_url();?>assets/images/forward.png">
													</a>
												</li>							
											<?php } ?>

											
                                            <li><a target="_blank" href="<?php echo base_url();?>lead/singleLead/<?php echo $lead->id; ?>"><img width="18" title="Open in new Window" height="18" alt="forward_transparent" src="<?php echo base_url();?>assets/images/forward_transparent.png"></a></li>
                                        </ul>
                                        </div>
                                    </div>



								<?php //will be called from ajax now expandedLead($lead,$i,$logged_in_user_id, $dashboard, $isSingleDetailLead,$vehicles_specific,false); ?>

								<span data-counter=<?php echo $i; ?> data-loggedinuserid="<?php echo $logged_in_user_id; ?>" data-dashboard="<?php if($dashboard) echo '1'; else echo '0'; ?>" data-issingledetaillead="<?php if($isSingleDetailLead) echo '1'; else echo '0';?>" data-issublead="0" id="expandedleadajax<?php echo $lead->id; ?>">
								</span>

                                    
                                </div>    
								
                                </td>
                                </tr>
                            </table>

                            <table class="tableUp">

                            </table>
                            </td>                                
                        </tr>
                               <?php 
							     $k=1;
								 $sub_leads = array();
								 //if($lead->new_lead_with_same_email && ($lead->created_by === $logged_in_user_id OR $lead->created_by === "0"))
							     $sub_leads = getSubLeads($lead->email,$lead->mobile,$lead->id);
 								 $totalSubs = count($sub_leads);
								 $j = $totalSubs;
							     if(!empty($sub_leads)){
								 foreach($sub_leads as $sub_lead){
							    ?>
                            	
                                <tr class="nestedLeads collapse CollapseJsOuter LeadsMangDtl_<?php echo $lead->id;?>" id="">
									
									<?php //$catColor = "border-left: 5px solid ".getCategoryColor($sub_lead->category_id).";"; ?>
									<?php $catColor = getCategoryColor($sub_lead->category_id); ?>

									<td class="fillCB" data-colorFillCB="<?php echo getCategoryColor($sub_lead->category_id); ?>">
										<?php if(!$isSingleDetailLead) { ?>
										
										<?php if(!$dashboard) { ?>
											<input id="checkbox<?php echo $sub_lead->id;?>" type="checkbox" name="leadCheckbox[]" class="leadCheckbox" value="<?php echo $sub_lead->id; ?>" ><label for="checkbox<?php echo $sub_lead->id;?>"><span></span></label>	

										<?php } ?>
										
										<?php } ?>
									</td>


									
									<!-- this was the old td where we had used the right border color -->
                                    <td style="position:relative;" data-toggle="collapse" data-target="#LeadsMangSubDtl_<?php echo $sub_lead->id;?>">
										<div class="color5pxBorder" style="background-color:<?php echo $catColor; ?>"></div>
									</td>
									
									
									
									<?php rowLead($isSingleDetailLead, $sub_lead, $i.$k, "#LeadsMangSubDtl_", "subRowClick", $dashboard,$logged_in_user_id); ?>




                                </tr>

                                <!--<tr class="BorderBottom">
                                	<td>&nbsp;</td>
                                    <td colspan="8"><?php echo $sub_lead->comments; ?></td>
                                </tr>-->
                                <tr class="collapse CollapseJsInner" id="LeadsMangSubDtl_<?php echo $sub_lead->id;?>">  <!-- inner SUbs -->
                                    <td class="innerSubDetail" colspan="11">
                                    	<div class="colorDivInner" style="background-color:<?php echo getCategoryColor($sub_lead->category_id); ?>;"></div>
                                        <div class="detailSecLead">
                                            <div class="row BorderBottom">
                                                <div class="col-md-6"><h2>Details</h2><img src="http://lm.mercedesbenzksa.com/mb/assets/images/black-loader.gif" alt="loading" id="loadingAjaxDetail<?php echo $sub_lead->id; ?>" height="16" width="16" style="display: none;"></div>
                                                <div class="col-md-6">
                                                <ul class="topBtnList">
                                                    <!--<li><a class="OtherLeadsLink" href="javascript:void(0);">Other Leads</a></li>
                                                    <li><a href="javascript:void(0);"><img width="40" height="20" alt="Del" src="<?php echo base_url();?>assets/images/trashCan.png"></a></li>-->
													<?php if(rights(2,'write')){ ?>
														<li>
															<a href="javascript:void(0);" data-target=".assignlead" data-toggle="modal" onclick="beforeAssignPopup('<?php echo $sub_lead->id; ?>','<?php echo getCityName($sub_lead->city_id); ?>', '<?php echo getBranchName($sub_lead->branch_id); ?>', '<?php echo $sub_lead->city_id; ?>','<?php echo $sub_lead->branch_id; ?>');">
																<img width="40" height="20" alt="Forward" title="Assign Lead" src="<?php echo base_url();?>assets/images/forward.png">
															</a>
														</li>
													<?php } ?>
													
                                                    <li><a target="_blank" href="<?php echo base_url();?>lead/singleLead/<?php echo $sub_lead->id; ?>"><img width="18" title="Open in new Window" height="18" alt="forward_transparent" src="<?php echo base_url();?>assets/images/forward_transparent.png"></a></li>
                                                </ul>
                                                </div>
                                            </div>

											<?php //will be called from ajax now expandedLead($sub_lead,$i.$k,$logged_in_user_id, $dashboard, $isSingleDetailLead,$vehicles_specific,true); ?>

											<span data-counter=<?php echo $i.$k; ?> data-loggedinuserid="<?php echo $logged_in_user_id; ?>" data-dashboard="<?php if($dashboard) echo '1'; else echo '0'; ?>" data-issingledetaillead="<?php if($isSingleDetailLead) echo '1'; else echo '0';?>" data-issublead="1" id="expandedleadajax<?php echo $sub_lead->id; ?>">
											</span>


                                        </div>
                                    </td>
                                </tr>


								<?php if($k===$totalSubs) { ?>
									<tr style="" class="emptyTr nestedLeads collapse CollapseJsOuter LeadsMangDtl_<?php echo $lead->id;?>" id="">
										<td colspan="11">&nbsp;
										
										</td>										
									</tr>
								<?php } ?>

                              <?php $k++; $j--; } }?>
            	
                        <?php $i--; } }else
						{
							
							if(isset($_GET['search_keyword']) && $_GET['search_keyword'] != "" && rights('1','write')){
							$param = getSearchParam();
							?>
                            <tr>
                            	<td colspan="10">
                                	<div style="text-align:center;">
                                    
                                    <a style="color:#0097d1; font-size: 16px;font-weight: bold;text-transform: capitalize;" href="<?php echo base_url()?>lead/createNewLead/<?php echo $param ;?>"> 
                                    	Create a New lead for <?php echo urldecode($param);?> <strong></strong>
                                    </a>
                                    
                                    </div>
                                </td>
                            </tr>
                            <?php 
							}	
							
						}
						
						if(isset($loadonlyleads) && $loadonlyleads==true)
						{
							//do nothing	
						}
						else
						{						
						
						?>
                    </tbody>
                </table>
            </div>
			
            <?php if(!$isSingleDetailLead) { ?>
			

				<?php if(isset($loadmore) && $loadmore){ ?>
								

				<?php if(isset($loadMoreNeeded) && $loadMoreNeeded) { ?>
					<div class="text-center" style="line-height: 63px;">
						<div class="loadMore"><a href="javascript:void(0);" onclick="loadMoreLeads('<?php echo base_url(); ?>');"><i class="fa fa-caret-down"></i> Load More <img src="<?php echo base_url();?>assets/images/mbCom-loader.gif" alt="loading" id="mbCom-loader" height="16" width="16" style="display:none;" /></a>
						</div>

						<div class="leads_display_count">					
							Showing <span id="loadedLeadsCount">10</span> <span id="ofWord">of</span> <span id="totalLeadsCount"><?php echo $totalLeadsCountForPaging; ?></span><span id="connMsg" style="display:none;"> (Others are connected)</span>
						</div>
					</div>

				<?php } ?>

				<?php } ?>

				<?php /* ?>
				<div style="float:right; color: #fff;">
					Total (Leads + Connected): 
					<strong>
						<?php echo $totalLeadsCountWithOutConnected .' + '.$totalConnectedLeadsCount .' = '.($totalLeadsCountWithOutConnected+$totalConnectedLeadsCount); ?>
					</strong>
					
					<br/>
					
					Assigned (Leads + Connected): 
					<strong>
						<?php echo $totalAssignedLeadsCountWithOutConnected .' + '.$totalConnectedAssignedLeadsCount .' = '.($totalAssignedLeadsCountWithOutConnected+$totalConnectedAssignedLeadsCount); ?>
					</strong>

					<br>
					
					Unassigned (Leads + Connected): 
					<strong>
						<?php 
						//$a = $totalLeadsCountWithOutConnected-$totalAssignedLeadsCountWithOutConnected;
						//$b = $totalConnectedLeadsCount-$totalConnectedAssignedLeadsCount;
						$a = $totalUnAssignedLeadsCountWithOutConnected;
						$b = $totalConnectedUnAssignedLeadsCount;

						echo $a.' + '.$b.' = '.($a+$b); ?>
					</strong>
				</div>
				<?php */ ?>

			<?php } ?>

        </div>
    </div> 
</section>

<div class="modal fade assignlead" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
<form data-assignleadfrm="1" action="<?php echo base_url();?>lead/leadAction" class="mb_form" onsubmit="return false;">

									<input type="hidden" name="form_type" value="admin_assign_lead_action">									
									<input type="hidden" id="assign_lead_id" name="lead_id" value="0">
									<input type="hidden" id="assign_lead_city" name="city_id" value="0">
									<input type="hidden" id="assign_lead_branch" name="branch_id" value="0">

  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header"> 
        	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">x</span></button> 
            <h4 class="modal-title">Assign Lead(s)</h4> 
         </div> 
         <div class="modal-body">
         	<p></p>
            <!--<div class="col-md-12">
            	<label>City <span>*</span></label>
							<select id="cityDDB" required>
                                <option value= "">Please Select</option>
                                <?php foreach($cities as $cities_list){?>
                                          <option value="<?php echo $cities_list->id; ?>"><?php echo $cities_list->title; ?></option>
                                 <?php } ?>   
                            </select>                
            </div>
            <div class="col-md-12">
            	<label>Branch Name <span>*</span></label> 
                	<select name="branch_id" id="branchDDB" required> 
                               
									<option value= "">Please Select</option>								
								
                    </select>	
            </div>-->
            <div class="col-md-12">
            	<label style="display:none;">Following are the Users with the City "<span id="lead_city_for_assign"></span>" and Branch "<span id="lead_branch_for_assign"></span>"<span></span></label>
                	<select id="employeeDDB" name="assign_to" required>
                    	<option value="">Please Select</option>
                    </select>

            </div>
            <div class="text-right"><!--<input type="button" class="btn" data-dismiss="modal">Assign</button>-->
			<input type="submit" class="btn" value="Submit" />			
			</div>
         </div> 
     </div>
     
  </div>
  </form>
</div>

<script>
//		Dynamic Function For Drop Down List START
$('.leadSortList .dropdown-menu').hide();
function leadSortList(event) {
	if($(event).parent().hasClass('open')) {
		$(event).parent().removeClass('open');
		$(event).siblings('.dropdown-menu').hide();	
		$('.leadSortList').removeClass('open');
		$('.leadSortList .dropdown-menu').hide();
	} else {
		$('.leadSortList').removeClass('open');
		$('.leadSortList .dropdown-menu').hide();
		$(event).siblings('.dropdown-menu').show();
		$(event).parent().addClass('open');
	}
}
$('.leadSortList button.button').click(function(e) {
	leadSortList(this);
});
$(document).mouseup(function (e)
{
    var container = $(".leadSortList .dropdown-menu, .leadSortList button.button,.ui-autocomplete");
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
     {leadSortList();}
});/*=======  END  ======*/
//		Dynamic Function For Drop Down List END


var isInternalCollapse = false;

	
function expLeadAjax(leadId)
{	

	$("#expandedleadajax"+leadId+"").html(""); //reset
	$('#loadingAjaxDetail'+leadId+'').show();

	var expLAjax = $("#expandedleadajax"+leadId+"");
	
	counter = expLAjax.attr("data-counter");
	logged_in_user_id = expLAjax.attr("data-loggedinuserid");
	dashboard = expLAjax.attr("data-dashboard");
	is_single = expLAjax.attr("data-issingledetaillead");
	is_sublead = expLAjax.attr("data-issublead");

	$.ajax({
		type: "POST",
		url: base_url+'lead/ajaxSelfData',
		data: {'lead_id':leadId, 'counter':counter, 'logged_in_user_id':logged_in_user_id, 'dashboard':dashboard, 'is_single':is_single, 'is_sublead':is_sublead},
		//dataType:"json",
		cache: false,
		success: function(result){

			$('#loadingAjaxDetail'+leadId+'').hide();
			$("#expandedleadajax"+leadId+"").html(result);	
			bindDateTimePicker();
			bindMention();
		}
	});
}

$(document).ready(function(){

	//sortUnorderedList("branchLis");
	//sortList(document.getElementById('branchLis'), true);
	//sortList(document.getElementById('sourceLis'), false);
	//sortList(document.getElementById('eventLis'), false);
	//sortList(document.getElementById('assignToLis'), true);
	
	//$('.rowClick').on('click', function (e) {
	$(".leadsListingTable").on("click", ".rowClick", function(e){

		count_check_number = 1;

		var leadId = $(this).attr('data-leadId');

		allNotificationRead(leadId);

		var classOfDiv = $(this).attr("data-target");

		$(".leadsListingTable table tbody tr.activeSub").removeClass("activeSub");
		$(".leadsListingTable table tbody tr.subLeadsRow").removeClass("subLeadsRow");
		$(".leadsListingTable table tbody tr").removeClass("emptyTrSelfData");
		$(".leadsListingTable table tbody tr").removeClass("emptyTrSubsOnly");
		$(".leadsListingTable table tbody tr").removeClass("subLeadsRowFirst");
		$('.CollapseJsInner').removeClass('in');
		
		if( !$(classOfDiv).hasClass("in") ){
	  		$('.CollapseJsOuter').removeClass('in');			
			$(".selfData").hide();

			//===
			if($(this).hasClass("leadPlus"))
			{
				//plus clicked
				$(classOfDiv).addClass("subLeadsRow");			
				$(this).closest("tr").siblings(".emptyTr").addClass("emptyTrSubsOnly");
				$(this).closest("tr").next("tr").next("tr").addClass("subLeadsRowFirst");
			}
			else
			{
				$(classOfDiv + ".selfData").show();
				$(this).closest("tr").siblings(".emptyTr").addClass("emptyTrSelfData");

				expLeadAjax(leadId);

			}
			//====

			//1. remove from all .fillCB
			$(".fillCB").css('background-color','rgba(0,0,0,0)');	

			//2. add color it is first time expanding
			var fillCB = $(this).siblings(".fillCB");
			var colorFillCB = fillCB.attr("data-colorFillCB");
			$(fillCB).css('background-color',colorFillCB);	

			$(this).closest("tr").addClass("activeSub");

		}
		else
		{
			
			if($(this).hasClass("leadPlus"))
			{
				//3. closing so remove all again .fillCB
				$(".fillCB").css('background-color','rgba(0,0,0,0)');
				$(classOfDiv + ".selfData").hide();		
			}
			else
			{				
				if($(classOfDiv + ".selfData").is(':visible'))
				{
					//3. closing so remove all again .fillCB
					$(".fillCB").css('background-color','rgba(0,0,0,0)');
					$(classOfDiv + ".selfData").hide();		

				}
				else
				{
					$(classOfDiv + ".selfData").show();
					e.stopPropagation();

					expLeadAjax(leadId);					
				}
			}
		}
		

	});


	//when multiple sub leads open then only one will open
	//$('.subRowClick').on('click', function () {
	$(".leadsListingTable").on("click", ".subRowClick", function(e){

		var leadId = $(this).attr('data-leadId');
		
		count_check_number2 = 1;

		var classOfDiv = $(this).attr("data-target");
				
		if( !$(classOfDiv).hasClass("in") ){	  	
			$('.CollapseJsInner').removeClass('in');
			expLeadAjax(leadId);
		}

	});



	

	
	//$('.AddPlusBtnIcons').on('click', function () {
	$(".leadsListingTable").on("click", ".AddPlusBtnIcons", function(e){

		var idOfDiv = $(this).attr("href");

		if( !$(idOfDiv).hasClass("in") ){
	  		$('.PhoneBoxCollapse').removeClass('in');
			$('.CommentBoxCollapse').removeClass('in');
			$('.SendCustomerEmailCollapse').removeClass('in');
		}

	});


	<?php if($isSingleDetailLead) { ?>
		$("#singleDetailLead").click();
	<?php } ?>



	if(getParameterByName('search_keyword')!=null && getParameterByName('search_keyword')!="")
	{
		var src_str = $("#ajaxLoadMoreTBody").html();
		var term = getParameterByName('search_keyword');
		term = term.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");
		var pattern = new RegExp("("+term+")(?!([^<]+)?>)", "gi");

		src_str = src_str.replace(pattern, "<span class='highlight_search'>$1</span>");
		src_str = src_str.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/,"$1</mark>$2<mark>$4");
		
		$("#ajaxLoadMoreTBody").html(src_str);
	}

});
	
	
function sortList(ul, flag){
	if(!ul)
		return;
    var new_ul = ul.cloneNode(false);

    // Add all lis to an array
    var lis = [];
    var first_child = [];
    for(var i = ul.childNodes.length; i--;){
    		if(i == 1 && flag)
        {
        	first_child.push(ul.childNodes[i]);
        }
        else if(ul.childNodes[i].nodeName === 'LI')
            lis.push(ul.childNodes[i]);
    }
		//console.log(lis);
//    console.log(first_child);
    // Sort the lis in descending order
    lis.sort(function(a, b){
    	//console.log('data=' + a.childNodes[0].data);
       //return parseInt(a.childNodes[1].innerText , 10) - 
         //     parseInt(b.childNodes[1].innerText , 10);
         return b.childNodes[1].innerText < a.childNodes[1].innerText;
    });

    // Add them into the ul in order
     if(flag)
     {
     	new_ul.appendChild(first_child[0]);
     }
    for(var i = 0; i < lis.length; i++)
        new_ul.appendChild(lis[i]);
    ul.parentNode.replaceChild(new_ul, ul);
}

</script>

<?php } ?>

