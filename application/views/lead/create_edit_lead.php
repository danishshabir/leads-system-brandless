<?php 
$isSalesConsultantSpecialForm = false;
$isPreOwnedSpecialForm = false;
$isServiceForm = false; 
if(rights(37,'write')) $isSalesConsultantSpecialForm = true; 
if(rights(38,'write')) $isPreOwnedSpecialForm = true; 
//if(rights(39,'write')) $isServiceForm = true; 

// //commented for permission sep 17 ,2019
// if(rights(37,'write') && rights(38,'write')) 
// {
// 	echo '<br><br><p style="text-align:center;color:white;">Sorry you might have conflicting b/w multiple types of "Lead Forms".</p>'; exit();
// }






//===check manager
	$loggedInUserManagingDeps = getLoggedInUserManagingDeps();		
	if(is_array($loggedInUserManagingDeps) && count($loggedInUserManagingDeps)>0)
	{						
		$isSalesConsultantSpecialForm = false;  //manager will always see longer old form
		$isPreOwnedSpecialForm = false; 
		$isServiceForm = false; 
	}
	elseif( rights(33,'read') ) //33 is lead controller
	{
		//logged in user is a submanager(lead controller) 
		$isSalesConsultantSpecialForm = false;  //sub manager will always see longer old form
		$isPreOwnedSpecialForm = false; 
		$isServiceForm = false; 
	}
	elseif(rights(35,'read') ) //view all leads
	{	
		$isSalesConsultantSpecialForm = false;  //view all leads person will also see longer form.
		$isPreOwnedSpecialForm = false; 
		$isServiceForm = false; 
	}
	elseif(rights(37,'write') && isset($is_short_frm_lead) && !$is_short_frm_lead && !isset($again)) 
	{  //check if sales consultant is editing any old lengthy form lead then don't let him edit it.
		echo '<br><br><p style="text-align:center;color:white;">Sorry you can\'t edit this lead.</p>'; exit();
	}
	elseif(rights(38,'write') && isset($is_preowned_frm_lead) && !$is_preowned_frm_lead && !isset($again)) 
	{  //check if is editing any old lengthy form lead then don't let him edit it.
		echo '<br><br><p style="text-align:center;color:white;">Sorry you can\'t edit this lead.</p>'; exit();
	}

//check if param is define and do some fucntionality here
		// if param is email
	if(isset($searchParam)){
		if(filter_var($searchParam, FILTER_VALIDATE_EMAIL)) {
			 $searchEamil = $searchParam;
		}
		else{
			$searchEamil = "";
		}
		
		// if param is numeric 
		if (is_numeric($searchParam) && strlen($searchParam) == 9)
		{
			 $searchPhone = "+966".$searchParam;
		}
		elseif (is_numeric($searchParam) && strlen($searchParam) == 10 && strpos($searchParam, "0") === 0)
		{
			 $searchParamRemove = ltrim($searchParam, '0');
			 $searchPhone = "+966".$searchParamRemove;
			 
		}
		else 
		{
			$searchPhone = "";
		}
	}
	
	
	if($isServiceLead===true)
	{
		$isSalesConsultantSpecialForm = false;  //sub manager will always see longer old form
		$isPreOwnedSpecialForm = false; 
		$isServiceForm = true; 		
	}
?>
<section>
	<div class="container lead_create_edit_frm">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo menu(); ?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i class="sprite sprite-menagment"></i> 
						<?php if($edit) { ?>
							Edit Lead
						<?php }else{ ?>
							Create a new Lead
						<?php } ?>						
						</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>            	
            </div>
            <div class="createNewLeadSec">
            	<p></p>
                
                <h2>Details</h2>
                <!--<p>Please note, test drive cannot be booked without a valid driving license</p>-->
                <div class="line"></div>
                <hr>
                <form data-leadfrm="1" action="<?php echo base_url();?>lead/leadAction" method="post" class="mb_form">
					<?php if($edit) { ?>
					<input type="hidden" name="id" value="<?php echo $id?>">
					<?php } ?>
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        	<label>Title <span></span></label>
                            <select class="border2Blue" name="title">
                            	<option value= "">Please Select</option>
                                <option <?php if($edit){ if($title=='Mr.') echo 'selected=selected'; }
								if(isset($again)){ if($title=='Mr.') echo 'selected=selected'; } ?> value="Mr.">Mr.</option>
                                <option <?php if($edit){ if($title=='Mrs.') echo 'selected=selected'; }
								if(isset($again)){ if($title=='Mrs.') echo 'selected=selected'; } ?> value="Mrs.">Mrs.</option>
								<option <?php if($edit){ if($title=='Miss') echo 'selected=selected';} if(isset($again)){ if($title=='Miss.') echo 'selected=selected'; }?> value="Miss">Miss</option>
                            </select>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        	<label>First Name <span>*</span></label>
                            
                            <input type="text" placeholder="Write Here" name="first_name" value="<?php if($edit) echo ucfirst($first_name); if(isset($again)) echo ucfirst($first_name);   ?>" required />
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        	<label>Surname <span></span></label>
                            <input type="text" placeholder="Write Here" name="surname" value="<?php 
							if($edit) echo ucfirst($surname); if(isset($again)) echo ucfirst($surname);  ?>" />
                        </div>
                   
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        	<label>Email <span>*</span></label>
                            <input type="email" placeholder="Write Here" name="email" value="<?php 
							if($edit) echo $email; if(isset($again)) echo $email; if(isset($searchEamil))echo $searchEamil;  ?>" required />
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        	<label>Phone/Mobile <span>*</span></label>
                            <input type="text" id="<?php if(!$edit) echo "mobile-number"; ?>" name="mobile" value="<?php if($edit) echo $mobile; if(isset($again)) {echo $mobile; } if(isset($searchPhone)) echo $searchPhone;  ?>" required/>
                        </div>
						 <!--<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        	<label>Valid Driving License </label>
                            <div class="radioBtns">
                            	<input <?php if($edit){ if($valid_driving_license=='1') echo 'checked="checked"';} ?> type="radio" id="ValidDrivingLicenseYes" name="valid_driving_license" value="1"><label for="ValidDrivingLicenseYes"><span><span></span></span>Yes</label>
                                <input <?php if($edit){ if($valid_driving_license=='0') echo 'checked="checked"';} ?> type="radio" id="ValidDrivingLicenseNo" name="valid_driving_license" value="0"><label for="ValidDrivingLicenseNo"><span><span></span></span>No</label>
                                
                            </div>
                        </div>-->
                    </div>

					<?php if(!$isSalesConsultantSpecialForm && !$isServiceForm) { ?>
					<hr>
					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        	<label>Preferred Mode of Contact </label>
                            <div class="radioBtns">
                            	<input <?php if($edit){ if($preferred_mode_of_contact=='Phone/Mobile') echo 'checked="checked"';} if(isset($again)){ if($preferred_mode_of_contact=='Phone/Mobile') echo 'checked="checked"';} ?> id="PhoneMobile" type="radio" name="preferred_mode_of_contact" value="Phone/Mobile"><label for="PhoneMobile"><span><span></span></span>Phone/Mobile</label>
                                <input <?php if($edit){ if($preferred_mode_of_contact=='Email') echo 'checked="checked"';} if(isset($again)){ if($preferred_mode_of_contact=='Email') echo 'checked="checked"';} ?> id="Email" type="radio" name="preferred_mode_of_contact" value="Email"><label for="Email"><span><span></span></span>Email</label>
                                
                            </div>
                        </div>
					</div>

                    <hr>
					<?php } ?>

                    <!--<h2>Your Vehicle(s) of Interest</h2>
                    <label>Please select up to 3 vehicles <span></span></label>
                    <div class="line"></div>-->
                    
                    <div class="row">
                    	<div class="col-md-12">
							<script>
								count_check_number = 1;
							</script>
							<?php 	
							if($edit)
							{
								$vehicle_specific_names_id_arr = array();
								$vehicle_arr = array();

								$count_check_number1 = 0;
								$count_check_number2 = 0;

								if($vehicle)
								{
									$vehicle_arr = explode(",",$vehicle); 
									$count_check_number1 = count($vehicle_arr);
								}

								if($vehicle_specific_names_id)
								{
									$vehicle_specific_names_id_arr = explode(",",$vehicle_specific_names_id); 
									$count_check_number2 = count($vehicle_specific_names_id_arr);
								}
							?>
							<script>

								count_check_number=0;
																
								count_check_number1 = <?php echo $count_check_number1; ?>;
								count_check_number2 = <?php echo $count_check_number2; ?>;

								count_check_number = count_check_number1 + count_check_number2;

								count_check_number++; //what ever the value is here add ++

							</script>
							<?php 
							}
							if(isset($again))
							{
								$vehicle_specific_names_id_arr = array();
								$vehicle_arr = array();
								$count_check_number1 = 0;
								$count_check_number2 = 0;
								if($vehicle)
								{
									$vehicle_arr = explode(",",$vehicle); 
									$count_check_number1 = count($vehicle_arr);
								}
								if($vehicle_specific_names_id)
								{
									$vehicle_specific_names_id_arr = explode(",",$vehicle_specific_names_id); 
									$count_check_number2 = count($vehicle_specific_names_id_arr);
								}
							?>
							<script>
								count_check_number=0;
								count_check_number1 = <?php echo $count_check_number1; ?>;
								count_check_number2 = <?php echo $count_check_number2; ?>;
								count_check_number = count_check_number1 + count_check_number2;
								count_check_number++; //what ever the value is here add ++
							</script>
							<?php 
							}
							?>
                           
                              
						<script>							
                            $(function() {
                                var leadCar = [
                                     <?php foreach($vehicles_specific as $vehicle1){ ?>
                                        {
                                        value: "<?php echo $vehicle1->id; ?>",
                                        label: "<?php echo $vehicle1->name;?>",
                                        },		 
                                    <?php } ?>      
                                ];
									
                                $( "#project" ).autocomplete({
                                  minLength: 1,
                                  source: leadCar,
                                  focus: function( event, ui ) {
                                    $( "#project" ).val( ui.item.label );
                                    return false;
                                  },
                                  select: function( event, ui ) {
                                    $( "#project" ).val( ui.item.label );
                                    $( "#project-id" ).val( ui.item.value );
									
									var sp_c_id = ui.item.value;
									//var gen_v_arr = document.getElementsByName("vehicle[]");
									//alert(sp_c_id);
									//alert(gen_v_arr.length);
									$(".general_veh").each(function() {
										//console.log( this.value + ":" + this.checked );
										var spec_car_of_general = $(this).attr('data-spcars');
										//alert(spec_car_of_general);
										spec_car_of_general_arr = spec_car_of_general.split(",");
										//loop here or check in array
										if(spec_car_of_general_arr.indexOf(sp_c_id) >= 0) //if newley added specific car has any genaral div already
										{
											alert('There is a general car '+$(this).attr("data-name")+'. That will be replaced.');
											$(this).parent().remove();
											count_check_number--;
										}
										
									});
									
									
	// here set limit 1 old was 3								
	if (count_check_number <= 1 ) {
				
										$( "#project-icon" ).append( "<div class='addCarUHav specific_car'>"+ ui.item.label +"<a href='javascript:void(0);' >X</a><input type='hidden' name='vehicle_specific_names_id[]' value='"+ui.item.value+"'/></div>" );
										$( "#project" ).val('');
										count_check_number++;
										
									}else
									{	// here set 1 old was 3
										alert('You can only select up to 1 vehicles.');
									}	

										return false;
                                  }
                                })
                                .autocomplete( "instance" )._renderItem = function( ul, item ) {
                                  return $( "<li>" )
                                    .append( "<a>" + item.label + "</a>" )
                                    .appendTo( ul );
                                };
								
								
								
								$.ui.autocomplete.filter = function (array, term) {
								  var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
								  return $.grep(array, function (value) {
									return matcher.test(value.label || value.value || value);
								  });
								};
								
								
								
								
                            });
							$(document).on('click', '.addCarUHav a', function() {$(this).parent().remove();
								count_check_number--;
							});
							
                          </script>
                            <div class="row newVehicleInterest">
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                	<!--<label>Type Vehicle Name <span>*</span></label>-->
									<label>
									<?php if($isServiceForm)						
											echo "Select Vehicle";
										  else											
											echo "Select Vehicle(s) of Interest";
									?>
									<?php if($isSalesConsultantSpecialForm  || $isPreOwnedSpecialForm){ ?><span>*</span> <?php } ?> </label>
                                    <input id="project" type="text" placeholder="Write and Select">
                                    <input type="hidden" id="project-id">
                                </div>
                                <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12" id="project-icon">
                                <?php if($edit){
																		
									foreach($vehicle_specific_names_id_arr as $specific_veh_id){										
									?>										
										<div class='addCarUHav'>
											<?php echo getSpecificVehicleNameById($specific_veh_id); ?>
											
											<a href='javascript:void(0);'>X</a>
											
											<input type='hidden' name='vehicle_specific_names_id[]' value='<?php echo $specific_veh_id; ?>'/>
										</div>
									<?php
									}
										
									//======

									foreach($vehicle_arr as $general_veh_id){										
									?>										
										<div class='addCarUHav'>
											<?php echo getGeneralVehicleNameById($general_veh_id); ?>

											<a href='javascript:void(0);'>X</a>

											<input class='general_veh' data-name="<?php echo getGeneralVehicleNameById($general_veh_id); ?>" data-spcars="<?php echo getSpecificCars($general_veh_id); ?>" type='hidden' name='vehicle[]' value='<?php echo $general_veh_id; ?>'/>
										</div>
									<?php
									}
								}
								if(isset($again)){
									foreach($vehicle_specific_names_id_arr as $specific_veh_id){										
									?>
										<div class='addCarUHav'>
											<?php echo getSpecificVehicleNameById($specific_veh_id); ?>
											<a href='javascript:void(0);'>X</a>
											<input type='hidden' name='vehicle_specific_names_id[]' value='<?php echo $specific_veh_id; ?>'/>
										</div>
									<?php
									}
									//======
									foreach($vehicle_arr as $general_veh_id){										
									?>										
										<div class='addCarUHav'>
											<?php echo getGeneralVehicleNameById($general_veh_id); ?>
											<a href='javascript:void(0);'>X</a>
											<input class='general_veh' data-name="<?php echo getGeneralVehicleNameById($general_veh_id); ?>" data-spcars="<?php echo getSpecificCars($general_veh_id); ?>" type='hidden' name='vehicle[]' value='<?php echo $general_veh_id; ?>'/>
										</div>
									<?php
									}
								}
									?>
								</div>
                            </div>    
                           
                    	</div>
                    </div>
                    
                    <hr>
                    <div class="row">
                    	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Category of Lead <span>*</span></label>														

							<?php if($isServiceForm || true) { ?>
								<select class="border2Blue" name="category_id" required onChange="if(this.value==38){ $('#complaint_type').show(); $('#complaint_type_id').attr('required', true); } else {$('#complaint_type').hide(); $('#complaint_type_id').attr('required', false);} if(this.value==37){ $('#createAppointment').val('1'); $('#cLSubmitBtn').attr('value', 'Create Appointment'); } else { $('#createAppointment').val('0'); $('#cLSubmitBtn').attr('value', 'Update');}">
							<?php }else{ ?>
								<select class="border2Blue" name="category_id" required>								
							<?php } ?>

								
								<?php if($isPreOwnedSpecialForm) { ?>
									<option value= "36">Pre-Owned Enquiry (UE)</option>
								<?php } 
								elseif($isSalesConsultantSpecialForm) { ?>
									<option value= "33">New Cars Enquiry (NE)</option>
								<?php } 
								elseif($isServiceForm) { ?>
									<option value= "">Please Select</option>
									<option value= "37" <?php if($edit){ if("37"==$category_id) echo 'selected=selected'; } if(isset($again)){ if("37"==$category_id) echo 'selected=selected'; } ?>>Service Dept. Appointment Request (SA)</option>
									<option value= "39" <?php if($edit){ if("39"==$category_id) echo 'selected=selected'; } if(isset($again)){ if("39"==$category_id) echo 'selected=selected'; } ?>>Service Dept. Enquiry (SE)</option>
									<option value= "38" <?php if($edit){ if("38"==$category_id) echo 'selected=selected'; } if(isset($again)){ if("38"==$category_id) echo 'selected=selected'; } ?>>Service Dept. Customer Complaint (SC)</option>
								<?php }else{ ?>
									<option value= "">Please Select</option>
									<?php foreach( $categories as $category ){ ?>
										<option <?php if($edit){ if($category_id==$category->id) echo 'selected=selected'; } if(isset($again)){ if($category_id==$category->id) echo 'selected=selected'; } ?> value="<?php echo $category->id; ?>"><?php echo $category->title.'  '.'('.$category->lead_code.')'; ?></option>
								<?php } } ?>



                            </select>
                    	</div>
						
						<?php if($isServiceForm || true) { 
							$sdn = "display:none;";
							if($edit && (int)$category_id===38){
								$sdn = "";
							}
						?>
						
						
						<div id="complaint_type" style="<?php echo $sdn; ?>" class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>Complaint Type <span>*</span></label>														

							<select id="complaint_type_id" name="complaint_type_id" class="border2Blue">
								<option value="">Please select</option>
								<?php foreach( $complaint_types as $complaint_type ){ ?>
										<option <?php if($edit){ if($complaint_type_id==$complaint_type->id) echo 'selected=selected'; } ?> value="<?php echo $complaint_type->id; ?>"><?php echo $complaint_type->title; ?></option>
								<?php } ?>
							</select>                           		
                    	</div>
						
						<?php } ?>
                    
						<?php if(!$isServiceForm) { ?>
						
                    	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <label>When are you planning to buy your next car <span><?php if($isSalesConsultantSpecialForm) echo '*'; ?></span></label>														

							<select id="expected_to_buy" name="expected_to_buy" class="border2Blue" <?php if($isSalesConsultantSpecialForm) echo 'required'; ?>>
								<option value="">Please select</option>
								<option <?php if($edit){ if("0 - 3 Months"==$expected_to_buy) echo 'selected=selected'; } if(isset($again)){ if("0 - 3 Months"==$expected_to_buy) echo 'selected=selected'; } ?> value="0 - 3 Months">0 - 3 Months</option>
								<option <?php if($edit){ if("3 - 6 Months"==$expected_to_buy) echo 'selected=selected'; } if(isset($again)){ if("3 - 6 Months"==$expected_to_buy) echo 'selected=selected'; } ?> value="3 - 6 Months">3 - 6 Months</option>
								<option <?php if($edit){ if("6+ Months"==$expected_to_buy) echo 'selected=selected'; } if(isset($again)){ if("6+ Months"==$expected_to_buy) echo 'selected=selected'; } ?> value="6+ Months">6+ Months</option>
								<option <?php if($edit){ if("Not planning at the moment"==$expected_to_buy) echo 'selected=selected'; } if(isset($again)){ if("Not planning at the moment"==$expected_to_buy) echo 'selected=selected'; } ?> value="Not planning at the moment">Not planning at the moment</option>
							</select>                           		
                    	</div>
						<?php } ?>
						
                    </div>
					<?php if($isSalesConsultantSpecialForm) { ?>
						
						<input type="hidden" name="city_id" value="<?php echo $this->session->userdata['user']['city_id']; ?>">
						<input type="hidden" name="branch_id" value="<?php echo $this->session->userdata['user']['branch_id']; ?>">

					<?php }else{ ?>
					
                    <?php if(!$isPreOwnedSpecialForm) { ?>
					<h2>
					<?php if($isServiceForm)						
							echo "Select City and Branch";
						  else											
							echo "Select Vehicle(s) of Interest";
									?>										
					</h2>
                    <div class="line"></div>
                    <hr>
					<?php } ?>
					
                    <div class="row">
                    	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        	<label>City <span>*</span></label>
                            <select class="border2Blue" id="cityDDB" name="city_id" required>
                                <option value= "">Please Select</option>
                                <?php foreach($cities as $cities_list){?>
                                          <option <?php if($edit){ if($city_id==$cities_list->id) echo 'selected=selected'; } if(isset($again)){ if($city_id==$cities_list->id) echo 'selected=selected'; } ?> value="<?php echo $cities_list->id; ?>"><?php echo $cities_list->title; ?></option>
                                 <?php } ?>   
                            </select>
                        </div>
                    	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        	<label>Branch <span>*</span></label>
                            <?php if($edit && (!isset($again))){ ?>
							<select name="branch_id" id="branchDDB"> <!--with ajax in case of new lead or call custom helper function to load in edit mode-->
                            <?php } if(!$edit && isset($again)){ ?>
                            <select name="branch_id" id="branchDDB"> <!--with ajax in case of new lead or call custom helper function to load in edit mode-->
							<?php }if(!$edit && !isset($again)){ ?>
							<select name="branch_id" id="branchDDB" onchange="/*leadAssignDDBWhileCreatingLeadAjax();*/"> <!--with ajax in case of new lead or call custom helper function to load in edit mode-->
							<?php } ?>
                                <?php if(!$edit && (!isset($again))){ ?>
									<option value= "">Please Select</option>
								<?php } ?>
								<?php if($edit && (!isset($again))) echo branchesByCity(false, $city_id, $branch_id); ?>
                                <?php if(!$edit && (isset($again))) echo branchesByCity(false, $city_id, $branch_id); ?>
                            </select>							 
                        </div>
						<?php if(!$isPreOwnedSpecialForm && !$isServiceForm) { ?>
                    	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        	<label>Preferred time for your test drive </label>
                            <input style="width:310px" type="text" class="calendar timepicker" placeholder="" value="<?php if($edit && $preferred_time!="00:00:00") echo $preferred_time; if((isset($again)) && $preferred_time!="00:00:00") echo $preferred_time; ?>" /> <input type="hidden" name="preferred_time" class="time_alternate">
                        </div>
						
                   
                    	<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        	<label>Preferred date for your test drive</label>
                            <input style="width:310px" type="text" class="calendar datepicker" placeholder="" name="" value="<?php if($edit && $preferred_date!='0000-00-00') echo date("d F Y", strtotime($preferred_date)); if(isset($again) && $preferred_date!='0000-00-00') echo date("d F Y", strtotime($preferred_date)); ?>"/>
							<input type="hidden" name="preferred_date" id="date_alternate" class="date_alternate" value="<?php if($edit && $preferred_date!='0000-00-00') echo $preferred_date;if(isset($again) && $preferred_date!='0000-00-00') echo $preferred_date; ?>">
                        </div>

						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        	<label>Preferred time to call</label>
                            <select id="prefered_time_to_call" name="prefered_time_to_call" class="border2Blue">
								<option value="">Please select</option>
								<option <?php if($edit){ if("Anytime"==$prefered_time_to_call) echo 'selected=selected'; } if(isset($again)){ if("Anytime"==$prefered_time_to_call) echo 'selected=selected'; } ?> value="Anytime">Anytime</option>
								<option <?php if($edit){ if("Morning"==$prefered_time_to_call) echo 'selected=selected'; } if(isset($again)){ if("Morning"==$prefered_time_to_call) echo 'selected=selected'; } ?> value="Morning">Morning</option>
								<option <?php if($edit){ if("Afternoon"==$prefered_time_to_call) echo 'selected=selected'; } ?> value="Afternoon">Afternoon</option>
							</select>
                        </div>
						<?php } ?>

                    </div>                    
                    <?php } ?>
					
					<?php if($isSalesConsultantSpecialForm || $isPreOwnedSpecialForm) { ?>
						<input type="hidden" name="assign_to" value="">
					<?php }else{ ?>

					<?php if(!$edit && !isset($again)){ ?>
                    <div class="row">
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							<h2>Assign lead</h2>
							<!--<div class="line"></div>-->
							
							<label style="display:none;">Following are the Users with the City "<span id="lead_city_for_assign">Select City Above</span>" and Branch "<span id="lead_branch_for_assign">Select Branch Above</span>"</label>
							<select id="employeeDDB" name="assign_to">
								<option value="">Please Select</option>
							</select>     
						</div>
					</div>
					<script>
						leadAssignDDBWhileCreatingLeadAjax();
					</script>
					<?php } 
					if(!$edit && isset($again)){ ?>
                    <div class="row">
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							<h2>Assign lead</h2>
							<!--<div class="line"></div>-->
							<label style="display:none;">Following are the Users with the City "<span id="lead_city_for_assign">Select City Above</span>" and Branch "<span id="lead_branch_for_assign">Select Branch Above</span>"</label>
							<select id="employeeDDB" name="assign_to">
								<option value="">Please Select</option>
							</select>     
						</div>
					</div>
					<script>
						leadAssignDDBWhileCreatingLeadAjax();
					</script>
					<?php } } ?>

					<div class="row">
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							<label>Source<span>*</span></label>
							<!--<div class="line"></div>							
							<label></label>-->
							<?php
							$disabled = "";
							if($edit && (int)$this->session->userdata['user']['source']>0)
							{
								$disabled = 'disabled="disabled"';
							}
							if(isset($again) && (int)$this->session->userdata['user']['source']>0)
							{
								$disabled = 'disabled="disabled"';
							}
							?>
							<select id="manual_source" name="manual_source" required <?php echo $disabled;?>>
								
								<?php if($isPreOwnedSpecialForm) { ?>

									<option value="">Please Select</option>
									<?php foreach($sources as $source){ if((int)$source->id===8 || (int)$source->id===9 || (int)$source->id===13) { } else{continue;} ?>
										<option <?php if($edit){ if($manual_source==$source->id) echo 'selected=selected'; } if(isset($again)){ if($manual_source==$source->id) echo 'selected=selected'; } ?> value="<?php echo $source->id; ?>">
											<?php echo $source->title; ?>
										</option>
									 <?php }  ?>   

								<?php }
								elseif($isSalesConsultantSpecialForm) { ?>

									<option value="">Please Select</option>
									<?php foreach($sources as $source){ if((int)$source->id===8 || (int)$source->id===13 || (int)$source->id===10 || (int)$source->id===15) { } else{continue;} ?>
										<option <?php if($edit){ if($manual_source==$source->id) echo 'selected=selected'; } if(isset($again)){ if($manual_source==$source->id) echo 'selected=selected'; } ?> value="<?php echo $source->id; ?>">
											<?php echo $source->title; ?>
										</option>
									 <?php }  ?>   

								<?php }else{ ?>

									<option value="">Please Select</option>
									<?php foreach($sources as $source){
											if($edit && (int)$this->session->userdata['user']['source']>0)
											{
												// do nothing
											}
											elseif((int)$this->session->userdata['user']['source']!==0 && (int)$this->session->userdata['user']['source']!== (int)$source->id)
											{
												continue;
											}
									?>
										<option <?php if($edit){ if($manual_source==$source->id) echo 'selected=selected'; } if(isset($again)){ if($manual_source==$source->id) echo 'selected=selected'; } ?> value="<?php echo $source->id; ?>">
											<?php echo $source->title; ?>
										</option>
									 <?php } } ?>   

							</select>    

							<script>
							if($('#manual_source option').size()==2)
							{
								 $("#manual_source option:last").attr("selected", "selected");
							}
							</script>
							
						</div>

						<?php if(!$isServiceForm) { ?>
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
							<label>Event</label>
							<!--<div class="line"></div>							
							<label></label>-->
							<select id="event_id" name="event_id">								
								<option value="">Please Select</option>
								<?php foreach($events as $event){?>
									<option <?php if($edit){ if($event_id==$event->id) echo 'selected=selected'; } if(isset($again)){ if($event_id==$event->id) echo 'selected=selected'; } ?> value="<?php echo $event->id; ?>">
										<?php echo $event->title; ?>
									</option>
								 <?php } ?>   
							</select>     
						</div>
						<?php } ?>
					</div>

                    <h2>Additional comments</h2>
                    <div class="line"></div>
                    
                    <label>Comments</label>
                    <textarea placeholder="Write Here" class="border2Blue" name="comments"><?php if($edit) {$comments = str_replace("\n"," ", $comments); echo trim(str_replace("\t","", $comments)); } ?></textarea>
                    
                    <div class="row">
                    	<div class="col-md-12 text-right">
                        	<input type="button" onClick="document.location.href='<?php echo base_url();?>lead'" value="Discard" class="btn white"/>
							<?php
							$vhsp = "";
							if($isSalesConsultantSpecialForm  || $isPreOwnedSpecialForm){
								$vhsp = "return checkVehicle();";
							}
							?>
                        	<input id="cLSubmitBtn" type="submit" onClick="<?php echo $vhsp; ?>" value="<?php if($edit && !isset($again)) echo 'Update'; if(!$edit && isset($again)) echo 'Create'; if(!$edit && !isset($again)) echo 'Create'; ?>" class="btn"/>
							<?php if($edit){ ?>
								<input type="hidden" value="update" name="form_type"/>                                
							<?php }else{ ?>
								<input type="hidden" value="save" name="form_type"/>
							<?php } ?>
                            <?php if(isset($again)){ ?>
								<input type="hidden" name="old_assign" value="<?php echo $assign_to; ?>">
                                <input type="hidden" name="searched_lead_id" value="<?php echo $id; ?>">
								<input type="hidden" id="createAgain" value="createAgain">
							<?php }else{
								?>
								<input type="hidden" id="createAgain" value="">
								<?php
							} ?>
                        </div>
                    </div>
					<?php if(!$edit && $isSalesConsultantSpecialForm) { ?>
					<input type="hidden" name="is_short_frm_lead" value="1">
					<?php } ?>	
					<?php if(!$edit && $isPreOwnedSpecialForm) { ?>
					<input type="hidden" name="is_preowned_frm_lead" value="1">
					<?php } ?>					
					<input type="hidden" id="createAppointment" value="0">					
                </form>
				
               <!-- <input type="hidden" value="Create" class="btn" data-toggle="modal" data-target=".leadCreateSussess" id="show_success_messge" />--><!-- just to show success message bootstrap logic -->

            </div>
        </div>
    </div>
</section> 
<!--- this function is for validation of vehivcle selection---->
<script type="text/javascript">
function checkVehicle()
{
		var  vehicleVal = $(".addCarUHav").text();
		if(vehicleVal.length == 0){
			alert("Please select vehicle of interest."); 			
			return false;
		}
		
		return true;
}
</script>
<!--<div class="modal fade leadCreateSussess" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header"> 
        	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">x</span></button> 
            <h4 id="mySmallModalLabel" class="modal-title"></h4> 
         </div> 
         <div class="modal-body">
         	<p id="message"></p>
            <p><strong></strong></p>
            <div class="text-center"><button type="button" class="btn" data-dismiss="modal">Dismiss</button></div>
         </div> 
     </div>
  </div>
</div>-->

