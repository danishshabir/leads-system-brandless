<section>
	<div class="container">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo menu(); ?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-4">
                        <h1><i class="sprite sprite-reports"></i> Reports</h1>
                    </div>
					<div class="col-sm-4">							
						
					</div>
					
                    <div class="col-sm-4">	
							
					</div>
               </div>            	
            </div>
            <div class="createNewLeadSec reportPageN">

			<form action="" method="get">
            
				<div class="reportPageNTOP">
                	<div class="row">
					  
                        <div class="col-md-4 col-sm-3 col-xs-12">
                           <h4>Filter</h4>
                        </div>
					  
                        <div class="col-md-8 col-sm-9 col-xs-12">
                            <ul>
                                <li>
                                	<div class="dropdown" style="border-right: none;">
										
                                    </div>
                                </li>                                
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row">
				                	
                    <div class="col-md-3">
                    	<div class="reportDateRange">                                
                                <div class="row">
                                    <div class="col-md-12">
										<input type="text" style="width:200px" onfocusout="if($(this).val()=='') $('.date_alternate1').val('');" class="datepicker1" value="<?php if(isset($_GET['date_from']) && $_GET['date_from']!='') echo date("d F Y", strtotime($_GET['date_from'])); ?>" placeholder="From"/>
										<input type="hidden" value="<?php if(isset($_GET['date_from']) && $_GET['date_from']!='') echo $_GET['date_from']; ?>" class="date_alternate1" name="date_from"/>
									</div>									
                                </div>
                        </div>
                    </div>

					<div class="col-md-3">
                    	<div class="reportDateRange">                                
                                <div class="row">                                    
                                    <div class="col-md-12">										
										<input type="text" style="width:200px" onfocusout="if($(this).val()=='') $('.date_alternate2').val('');" class="datepicker2" value="<?php if(isset($_GET['date_to']) && $_GET['date_to']!='') echo date("d F Y", strtotime($_GET['date_to'])); ?>" placeholder="To"/>
										<input type="hidden" value="<?php if(isset($_GET['date_to']) && $_GET['date_to']!='') echo $_GET['date_to']; ?>" class="date_alternate2" name="date_to"/>

									</div>
                                </div>
                        </div>
                    </div>
					
					<?php if($this->session->userdata['user']['id']!=="132" && $this->session->userdata['user']['id']!=="133" && $this->session->userdata['user']['id']!=="172" || $this->session->userdata['user']['id']==="135" || $this->session->userdata['user']['id']==="292"){ ?>
					
						<?php if(isset($dep_id) && $dep_id==52){ ?>
							<div class="col-md-3">
							  <div style="font-weight:bold; color:#337ab7;"><a href="<?php echo base_url();?>manager_report?madina">View Madina Reports</a></div>
							</div>						
						<?php } ?>
						
						<?php if(isset($dep_id) && $dep_id==80){ ?>
							<div class="col-md-3">
							  <div style="font-weight:bold; color:#337ab7;"><a href="<?php echo base_url();?>manager_report">View Madina Road Reports</a></div>
							</div>						
						<?php } ?>
						
						
						<?php if(isset($dep_id) && $dep_id==53){ ?>
							<div class="col-md-3">
							  <div style="font-weight:bold; color:#337ab7;"><a href="<?php echo base_url();?>manager_report?abha">View Abha Reports</a></div>
							</div>						
						<?php } ?>
						
						<?php if(isset($dep_id) && $dep_id==54){ ?>
							<div class="col-md-3">
							  <div style="font-weight:bold; color:#337ab7;"><a href="<?php echo base_url();?>manager_report">View Automall Reports</a></div>
							</div>						
						<?php } ?>
						
					
					
					<div class="col-md-3">
					  <div style="float:right; font-weight:bold; color:#337ab7;"><a href="<?php echo base_url();?>report" target="_blank">View Individual Reports</a></div>
					</div>
					
						
					
					<?php } ?>

			  </div>	
				
				<?php if($this->session->userdata['user']['id']==="132" || $this->session->userdata['user']['id']==="133" || $this->session->userdata['user']['id']==="172" || $this->session->userdata['user']['id']==="135" || $this->session->userdata['user']['id']==="292"){ ?>
				
				<div class="row">
					<div class="col-md-3">						
						<select id="manager_id_dep_id" name="manager_id_dep_id">
							<option value="0">Select Manager</option>
								<?php for($i=0; $i<count($managers); $i++)
								{ 
								?>
									<option <?php if(isset($dep_id) && $dep_id==$managers[$i]->dep_id) ECHO 'selected'; ?> value="<?php echo $managers[$i]->id; ?>|<?php echo $managers[$i]->dep_id; ?>">
										<?php echo $managers[$i]->name; ?> (<?php echo $managers[$i]->dep_name; ?>)
									</option>
								<?php 
								} ?>		
						</select>						
					</div>

					<div class="col-md-3">
					
								
					</div>


					
                </div>
				
				
				
				
				
				<div class="row">
					<div class="col-md-6">	


					
						<strong>( Conversions )</strong> 	
								<input id="assigned_date" type="radio" name="conversion_date_range" value="assign" selected style="height:0;width:0;">
								<label for="assigned_date" style="width:auto;"><span><span></span></span>Assigned Date</label>
							
								<input id="lead_created" type="radio" name="conversion_date_range" value="lead_created" style="height:0;width:0;">
								<label for="lead_created" style="width:auto;"><span><span></span></span>Lead Created </label>
								<?php if(isset($_GET['conversion_date_range']) && $_GET['conversion_date_range']=="assign"){ ?>
									<script>$('#assigned_date').click();</script>
								<?php }elseif(isset($_GET['conversion_date_range']) && $_GET['conversion_date_range']=="lead_created"){ ?>
									<script>$('#lead_created').click();</script>
								<?php }else{ ?>
									<script>$('#assigned_date').click();</script>
								<?php } ?>
					</div>

					<div class="col-md-3">
												
					</div>

                </div>
				
				<?php } ?>
			
               <!-- <h3>Reporting</h3>-->


			<input type="submit" class="btn" value="Filter"> <a href="<?php echo base_url();?>manager_report">(Remove Filter)</a>
			<input type="hidden" name="filter_reports" value="1">
			
			<?php if(isset($_GET["abha"])){ ?>
				<input type="hidden" name="abha" value="1">
			<?php } ?>
			
			<?php if(isset($_GET["madina"])){ ?>
				<input type="hidden" name="madina" value="1">
			<?php } ?>
			
			</form>

<br><br>

<!--<h2>Graphs</h2>-->


<?php
if(isset($_GET['manager_id_dep_id']) && $_GET['manager_id_dep_id']!="0")
{
//==============start grapahs==============
?>

<script type="text/javascript">
//=====start Hieracrchical bar chart====

    // Chart setup
    function stackedMultiples(element, height, jsonURL) {


        // Basic setup
        // ------------------------------

        // Define main variables
        var d3Container = d3.select(element),
            margin = {top: 25, right: 40, bottom: 20, left: 180},
            width = d3Container.node().getBoundingClientRect().width - margin.left - margin.right,
            height = height - margin.top - margin.bottom - 5,
            barHeight = 30,
            duration = 750,
            delay = 25;



        // Construct scales
        // ------------------------------

        // Horizontal
        var x = d3.scale.linear()
            .range([0, width]);

        // Colors
        var color = d3.scale.ordinal()
            .range(["#26A69A", "#ccc"]);



        // Create axes
        // ------------------------------

        // Horizontal
        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("top");



        // Create chart
        // ------------------------------

        // Add SVG element
        var container = d3Container.append("svg");

        // Add SVG group
        var svg = container
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


        // Construct chart layout
        // ------------------------------

        // Partition
        var partition = d3.layout.partition()
            .value(function(d) { return d.size; });



        // Load data
        // ------------------------------

		//d3.json("<?php echo base_url();?>report2/d3_bars_hierarchical_cars", function(error, root) {		
			d3.json(jsonURL, function(error, root) {	

			partition.nodes(root);			
			
			//custom extra logic for height			
			if(root.value!=0) {
				$("#indCHOSDiv").css( "height", "400px" );			
				//console.log("#indCHOSDiv increasing  height");
			}
			//====			
            
            x.domain([0, root.value]).nice();
            down(root, 0);
        });


        //
        // Append chart elements
        //

        // Add background bars
        svg.append("rect")
            .attr("class", "d3-bars-background")
            .attr("width", width)
            .attr("height", height)
            .style("fill", "#fff")
            .on("click", up);


        // Append axes
        // ------------------------------

        // Horizontal
        svg.append("g")
            .attr("class", "d3-axis d3-axis-horizontal d3-axis-strong");


        // Append bars
        // ------------------------------

        // Create hierarchical structure
        function down(d, i) {
            if (!d.children || this.__transition__) return;
            var end = duration + d.children.length * delay;

            // Mark any currently-displayed bars as exiting.
            var exit = svg.selectAll(".enter")
                .attr("class", "exit");

            // Entering nodes immediately obscure the clicked-on bar, so hide it.
            exit.selectAll("rect").filter(function(p) { return p === d; })
                .style("fill-opacity", 1e-6);

            // Enter the new bars for the clicked-on data.
            // Per above, entering bars are immediately visible.
            var enter = bar(d)
                .attr("transform", stack(i))
                .style("opacity", 1);

            // Have the text fade-in, even though the bars are visible.
            // Color the bars as parents; they will fade to children if appropriate.
            enter.select("text").style("fill-opacity", 1e-6);
            enter.select("rect").style("fill", color(true));

            // Update the x-scale domain.
            x.domain([0, d3.max(d.children, function(d) { return d.value; })]).nice();

            // Update the x-axis.
            svg.selectAll(".d3-axis-horizontal").transition()
                .duration(duration)
                .call(xAxis);

            // Transition entering bars to their new position.
            var enterTransition = enter.transition()
                .duration(duration)
                .delay(function(d, i) { return i * delay; })
                .attr("transform", function(d, i) { return "translate(0," + barHeight * i * 1.2 + ")"; });

            // Transition entering text.
            enterTransition.select("text")
                .style("fill-opacity", 1);

            // Transition entering rects to the new x-scale.
            enterTransition.select("rect")
                .attr("width", function(d) { return x(d.value); })
                .style("fill", function(d) { return color(!!d.children); });

            // Transition exiting bars to fade out.
            var exitTransition = exit.transition()
                .duration(duration)
                .style("opacity", 1e-6)
                .remove();

            // Transition exiting bars to the new x-scale.
            exitTransition.selectAll("rect")
                .attr("width", function(d) { return x(d.value); });

            // Rebind the current node to the background.
            svg.select(".d3-bars-background")
                .datum(d)
                .transition()
                .duration(end);

            d.index = i;
        }

        // Return to parent level
        function up(d) {
            if (!d.parent || this.__transition__) return;
            var end = duration + d.children.length * delay;

            // Mark any currently-displayed bars as exiting.
            var exit = svg.selectAll(".enter")
                .attr("class", "exit");

            // Enter the new bars for the clicked-on data's parent.
            var enter = bar(d.parent)
                .attr("transform", function(d, i) { return "translate(0," + barHeight * i * 1.2 + ")"; })
                .style("opacity", 1e-6);

            // Color the bars as appropriate.
            // Exiting nodes will obscure the parent bar, so hide it.
            enter.select("rect")
                .style("fill", function(d) { return color(!!d.children); })
                .filter(function(p) { return p === d; })
                .style("fill-opacity", 1e-6);

            // Update the x-scale domain.
            x.domain([0, d3.max(d.parent.children, function(d) { return d.value; })]).nice();

            // Update the x-axis.
            svg.selectAll(".d3-axis-horizontal").transition()
                .duration(duration)
                .call(xAxis);

            // Transition entering bars to fade in over the full duration.
            var enterTransition = enter.transition()
                .duration(end)
                .style("opacity", 1);

            // Transition entering rects to the new x-scale.
            // When the entering parent rect is done, make it visible!
            enterTransition.select("rect")
                .attr("width", function(d) { return x(d.value); })
                .each("end", function(p) { if (p === d) d3.select(this).style("fill-opacity", null); });

            // Transition exiting bars to the parent's position.
            var exitTransition = exit.selectAll("g").transition()
                .duration(duration)
                .delay(function(d, i) { return i * delay; })
                .attr("transform", stack(d.index));

            // Transition exiting text to fade out.
            exitTransition.select("text")
                .style("fill-opacity", 1e-6);

            // Transition exiting rects to the new scale and fade to parent color.
            exitTransition.select("rect")
                .attr("width", function(d) { return x(d.value); })
                .style("fill", color(true));

            // Remove exiting nodes when the last child has finished transitioning.
            exit.transition()
                .duration(end)
                .remove();

            // Rebind the current parent to the background.
            svg.select(".d3-bars-background")
                .datum(d.parent)
                .transition()
                .duration(end);
        }

        // Creates a set of bars for the given data node, at the specified index.
        function bar(d) {
            var bar = svg.insert("g", ".d3-axis-vertical")
                .attr("class", "enter")
                .attr("transform", "translate(0,5)")
                .selectAll("g")
                .data(d.children)
                .enter()
                .append("g")
                    .style("cursor", function(d) { return !d.children ? null : "pointer"; })
                    .on("click", down);

            bar.append("text")
                .attr("x", -6)
                .attr("y", barHeight / 2)
                .attr("dy", ".35em")
                .style("text-anchor", "end")
                .text(function(d) { return d.name; });
							

            bar.append("rect") 
                .attr("width", function(d) { return x(d.value); })
                .attr("height", barHeight);
				
			setTimeout(
			  function() 
			  {
				bar.append("text")
                .attr("x", function(d) { return x(d.value)+25; })				
                .attr("y", barHeight / 2)
                .attr("dy", ".35em")
                .style("text-anchor", "end")
				.style("fill", "#808080")
                .text(function(d) { return d.value; });
			  }, 1000);			

            return bar;
        }

        // A stateful closure for stacking bars horizontally.
        function stack(i) {
            var x0 = 0;
            return function(d) {
                var tx = "translate(" + x0 + "," + barHeight * i * 1.2 + ")";
                x0 += x(d.value);
                return tx;
            };
        }



        // Resize chart
        // ------------------------------

        // Call function on window resize
        $(window).on('resize', resize);

        // Call function on sidebar width change
        //$('.sidebar-control').on('click', resize);

        // Resize function
        // 
        // Since D3 doesn't support SVG resize by default,
        // we need to manually specify parts of the graph that need to 
        // be updated on window resize
        function resize() {

            // Layout variables
            width = d3Container.node().getBoundingClientRect().width - margin.left - margin.right;


            // Layout
            // -------------------------

            // Main svg width
            container.attr("width", width + margin.left + margin.right);

            // Width of appended group
            svg.attr("width", width + margin.left + margin.right);


            // Axes
            // -------------------------

            // Horizontal range
            x.range([0, width]);

            // Horizontal axis
            svg.selectAll('.d3-axis-horizontal').call(xAxis);


            // Chart elements
            // -------------------------

            // Bars
            svg.selectAll('.enter rect').attr("width", function(d) { return x(d.value); });
        }
    }

//===end Hieracrchical bar chart===
</script>
	
	
<script type="text/javascript">
google.load("visualization", "1", {packages:["corechart"]});

google.setOnLoadCallback(graph1);
function graph1() {
	
    var data = google.visualization.arrayToDataTable([
        ['Users', 'Avg Days to Call Attempt', { role: 'annotation' }, {role: 'tooltip'}, '# of Leads with Call Attempts', { role: 'annotation' }, {role: 'tooltip'}],
		
        /*['User1', 10, '10', 'tool tip',    20, '20', 'tool tip'],
        ['User2', 12, '12', 'tool tip',    15, '15', 'tool tip'],
        ['User3', 18, '18', 'tool tip',     25, '25', 'tool tip']*/
		
		<?php
		if($timeToMakeCallActionEachUserAvg)
		foreach($timeToMakeCallActionEachUserAvg as $avg) 
		{
			echo "['".$avg->full_name."', ".round((secondsToTextDurInHours($avg->avgval)/24),0).", '".round((secondsToTextDurInHours($avg->avgval)/24),0)."', '".secondsToTextDur($avg->avgval)."', ".$avg->num_leads.", '".$avg->num_leads."', 'Num of call attempted leads: ".$avg->num_leads."'],";
		}
		else{
			echo "['',0,'','',0,'','']";
		}
		?>
    ]);

	<?php
	$basedUpon = "";
	if(
	(!isset($_GET['date_from']) || (isset($_GET['date_from']) && $_GET['date_from']==""))
	&& (!isset($_GET['date_from']) || (isset($_GET['date_from']) && $_GET['date_from']==""))
	)
	{
		$basedUpon = "(Based upon: ".date('F Y').")";
	}
	?>

    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Team Avg. Time for 1st Call <?php echo $basedUpon; ?>',
		height: 500,		
        fontSize: 12,
		bar: {groupWidth: "55%"},
		colors: ['#299949', '#0099C6'],
        chartArea: {
            left: '15%',
            width: '80%',
            height: 450
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 12
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    var bar = new google.visualization.BarChart($('#graph1')[0]);
    bar.draw(data, options_bar);
}

//====

google.setOnLoadCallback(graph2);
function graph2() {
	
    var data = google.visualization.arrayToDataTable([
        ['Users', 'Avg Days to Finish a Lead', { role: 'annotation' }, {role: 'tooltip'}, '# of Finished Leads', { role: 'annotation' }, {role: 'tooltip'}],
		
        /*['User1', 10, '10', 'tool tip',    20, '20', 'tool tip'],
        ['User2', 12, '12', 'tool tip',    15, '15', 'tool tip'],
        ['User3', 18, '18', 'tool tip',     25, '25', 'tool tip']*/

		
		<?php 
		if($timeToFinishActionEachUserAvg)
		foreach($timeToFinishActionEachUserAvg as $avg)
		{
					
			echo "['".$avg->full_name."', ".round((secondsToTextDurInHours($avg->avgval)/24),0).", '".round((secondsToTextDurInHours($avg->avgval)/24),0)."', '".secondsToTextDur($avg->avgval)."', ".$avg->num_leads.", '".$avg->num_leads."', 'Number of finished leads: ".$avg->num_leads."'],";
		 
		}
		else{
			echo "['',0,'','',0,'','']";
		}		
		?>
		
    ]);

    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Team Avg. Time to Finish Lead',
		height: 500,		
        fontSize: 12,
		bar: {groupWidth: "55%"},
		colors: ['#299949', '#0099C6'],
        chartArea: {
            left: '15%',
            width: '75%',
            height: 450
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 12
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    var bar = new google.visualization.BarChart($('#graph2')[0]);
    bar.draw(data, options_bar);
}

//====

google.setOnLoadCallback(graph3);
function graph3() {
	
	//average Disapproved/Finished
	
    var data = google.visualization.arrayToDataTable([
         ['Users', 'Total Number of times Clicked Finished', { role: 'annotation' }, {role: 'tooltip'}, 'Total Number of times Manager Dissapproved', { role: 'annotation' }, {role: 'tooltip'}, 'Disapproved %', { role: 'annotation' }, {role: 'tooltip'}],
         /*['User1',   165,'165','tool tip',    50,'938','tool tip',    30,'30%','tool tip'],
		 ['User2',   135,'135','tool tip',    50,'1120','tool tip',    37,'37%','tool tip'],
		 ['User3',   157,'135','tool tip',    60,'135','tool tip',      50,'50%','tool tip'],
		 ['User4',   139,'135','tool tip',    70,'135','tool tip',     10,'10%','tool tip']*/
		 
		<?php 
		if($numOfDisapprovedActionEachUser)
		foreach($numOfDisapprovedActionEachUser as $avg)
		{			
				
			echo "['".$avg->full_name."',".$avg->finished.",'".$avg->finished."','# Clicked finished: ".$avg->finished."', ".$avg->disapproved.",'".$avg->disapproved."','# Dissapproved: ".$avg->disapproved."',  ".($avg->finished ? round((($avg->disapproved/$avg->finished)*100),0) : "").",'".($avg->finished ? round((($avg->disapproved/$avg->finished)*100),0) : "")."%','Dissapproved: ".($avg->finished ? round((($avg->disapproved/$avg->finished)*100),0) : "")."%'],\r\n";
		}
		else{
			echo "['',0,'0','', 0,'0','',0,'0','']";
		}
		?>
		
    ]);

    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Team Dissapproved Ratios',
		height: 500,		
        fontSize: 12,
		seriesType: 'bars',
		series: {2: {type: 'line'}},
		bar: {groupWidth: "45%"},
		colors: ['#0099C6', '#EA4335', '#323a86' ],
        chartArea: {
            left: '10%',
			right: '2%',
            width: '100%',
            height: 400
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 12
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    var bar = new google.visualization.ComboChart($('#graph3')[0]);
    bar.draw(data, options_bar);
}

//====

google.setOnLoadCallback(graph4);
function graph4() {
	
    var data = google.visualization.arrayToDataTable([
        ['Users', 'Total of Successful Calls', { role: 'annotation' }, {role: 'tooltip'}, 'Total Number of Active Leads', { role: 'annotation' }, {role: 'tooltip'}],
		
        /*['User1', 10, '10', 'tool tip',    20, '20', 'tool tip'],
        ['User2', 12, '12', 'tool tip',    15, '15', 'tool tip'],
        ['User3', 18, '18', 'tool tip',     25, '25', 'tool tip']  */

		<?php 
		if($numOfSuccessfullCallActionEachUser)
		foreach($numOfSuccessfullCallActionEachUser as $avg)
		{			
			echo "['".$avg->full_name."', ".$avg->success.", '".$avg->success."', '# of successfull calls: ".$avg->success."',    ".$avg->num_leads.", '".$avg->num_leads."', 'Total number of leads: ".$avg->num_leads."'],";
		}
		else{
			echo "['',0,'','', 0,'','']";
		}
		?>
		
    ]);

    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Successful Calls vs. Leads',
		height: 500,		
        fontSize: 12,
		bar: {groupWidth: "65%"},
		colors: ['#299949', '#0099C6'],
        chartArea: {
            left: '15%',
            width: '80%',
            height: 450
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 12
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    var bar = new google.visualization.BarChart($('#graph4')[0]);
    bar.draw(data, options_bar);
}

//====


google.setOnLoadCallback(graph5);
function graph5() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Branch', 'Score', {role: 'tooltip'}, {role: 'style'}, { role: 'annotation' } ],
		
		/*["Branch1",4.8,"tool tip","#3366CC", "4.8"],
		["Branch2",4.4,"tool tip","#3366CC", "4.4"],
		["My Branch",3.51,"tool tip","#eef213", "3.51"],
		["Average",3.4,"tool tip","#299949", "3.4"]*/
		
		<?php		
		if($branchesAvgScore)
		foreach($branchesAvgScore as $avg) 
		{			
			$colorCodeBaSm = "#3366CC";		
			if((int)$avg->id===(int)$dep_id) $colorCodeBaSm = "#eef213";
			echo '["'.$avg->title.'",'.round($avg->avgval,2).',"Average Score: '.round($avg->avgval,2).'","'.$colorCodeBaSm.'", "'.round($avg->avgval,2).'"],';			
		}
		else{
			echo "['',0,'','',''],";
		}
		if($totalAvgScore->avgval)
		{
			echo '["All Average",'.round($totalAvgScore->avgval,2).',"Average Score: '.round($totalAvgScore->avgval,2).'","#299949", "'.round($totalAvgScore->avgval,2).'"],';		
		}
		?>
		
    ]);


    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Branch Average Score',   //actually this is from organization struc dep that actually branches :)
		height: 500,		
        fontSize: 12,
		bar: {groupWidth: "35%"},
        chartArea: {
            left: '15%',
            width: '80%',
            height: 450
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 12
            }
        },
		hAxis: {
			viewWindow: {
				max: 5
			},
			gridlines:{
                color: '#e5e5e5',
                count: 11
            },
            minValue: 0
		},        
        legend: {
            position: 'none'            
        }
    };

    // Draw chart
    var bar = new google.visualization.BarChart($('#graph5')[0]);
    bar.draw(data, options_bar);

}

//======

google.setOnLoadCallback(graph6);
function graph6() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Year', 'Score', { role: 'annotation' }],
		
		/*['Jul 2016',1.8,1.8],
		['Aug 2016',2,2],
		['Sep 2016',3,3],
		['Oct 2016',3.5,3.5],
		['Nov 2016',2,2],
		['Dec 2016',4,4]*/
		
		<?php 
		if($avgScoreYearly)  
		{			
			if(isset($_GET['date_from']) && $_GET['date_from']!="")
				$start = $month = strtotime($_GET['date_from']);
			else
				$start = $month = strtotime('2019-7-01'); //July 2016

			if(isset($_GET['date_to']) && $_GET['date_to']!="")
				$end = strtotime($_GET['date_to']);
			else
				$end = strtotime(date("Y-m-d"));


			$lcc = 0;
			while($month <= $end)
			{
				 $key = date('M Y', $month);
				 
				 if($lcc>0) echo ",";
				 echo "[";
				 
				 echo "'".date('M Y', $month)."'";

				 if(isset($avgScoreYearly[$key])) 
				 {
					 $val = $avgScoreYearly[$key];
					 //echo '&nbsp; | &nbsp;'.$key.' <b>['.round($val,2).'</b>]'; 
					 echo ",".round($val,2);
					 echo ",".round($val,2);
				 }
				 else 
				 {
					 // for missing months, display overall avg. 
					$val = $avgScore->avgval; // overall avg
					//echo '&nbsp; | &nbsp;'.$key.' <b>['.round($val,2).'</b>]'; 
					echo ",".round($val,2);
					echo ",".round($val,2);
				 }


				 $month = strtotime("+1 month", $month);
				 
				 echo "]";
				$lcc++;
			}
		}
		else 
		{
			echo "['0000', 0 , 0]";
		} 
		?>
		
    ]);

    // Options
    var options = {
        fontName: 'Arial',
        height: 300,
		title: 'Annual Satisfaction Score',
        curveType: 'function',
        fontSize: 12,
        chartArea: {
            left: '10%',
			right: '2%',
            width: '100%',
            height: 250
        },
        pointSize: 4,
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 12
            }
        },
        vAxis: {
           // title: 'Sales and Expenses',
            titleTextStyle: {
                fontSize: 12,
                italic: false
            },
            gridlines:{
                color: '#e5e5e5',
                count: 11
            },
            minValue: 0,
			viewWindow: {
				max: 5
			}			
        },		
        legend: {
           position: 'none' 
        }
    };

    // Draw chart
    var line_chart12 = new google.visualization.LineChart($('#graph6')[0]);
    line_chart12.draw(data, options);
}

//=====

google.setOnLoadCallback(graph7);
function graph7() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Year', 'TD Completed', { role: 'annotation' }, {role: 'style'}],
		
		/*['Jul 2016',9,9,'#3367d6',0,0,'#FF7F50'],
		['Aug 2016',9,9,'#3367d6',0,0,'#FF7F50'],
		['Sep 2016',0,0,'#3367d6',0,0,'#FF7F50'],
		['Oct 2016',61,61,'#3367d6',2,2,'#FF7F50'],
		['Nov 2016',5,5,'#3367d6',0,0,'#FF7F50'],
		['Dec 2016',21,21,'#3367d6',0,0,'#FF7F50']*/

	<?php 
		if($dGraphTotalLeadsYearly)  
		{	
			if(isset($_GET['date_from']) && $_GET['date_from']!="")
				$start = $month = strtotime($_GET['date_from']);
			else
				$start = $month = strtotime('2019-7-01'); //July 2016

			if(isset($_GET['date_to']) && $_GET['date_to']!="")
				$end = strtotime($_GET['date_to']);
			else
				$end = strtotime(date("Y-m-d"));
			$lcc = 0;
			while($month <= $end)
			{
				 $key = date('M Y', $month);
				 
				 if($lcc>0) echo ",";
				 
				 echo "[";
				 
				 echo "'".date('M Y', $month)."'";

				 /*if(isset($dGraphTotalLeadsYearly[$key])) 
				 {
					 $val = $dGraphTotalLeadsYearly[$key];
					 echo ",".$val; 
					 echo ",".$val; 
					 echo ",'#3367d6'";						 
				 }
				 else 
				 {
					// for missing months, display 0. 
					$val = 0;
					echo ",".$val;
					echo ",".$val;
					echo ",'#3367d6'";						
				 }
				 */
				 
				 //completed test drives logic here===
				 if(isset($dGraphTotalComLeadsYearly[$key])) 
				 {
					 $val = $dGraphTotalComLeadsYearly[$key];						 
					 echo ",".$val; 
					 echo ",".$val; 
					 echo ",'#FF7F50'";
				 }
				 else 
				 {
					// for missing months, display 0. 
					$val = 0;						
					echo ",".$val;
					echo ",".$val;
					echo ",'#FF7F50'";
				 }


				 $month = strtotime("+1 month", $month);
				 
				 echo "]";
				$lcc++;
			}
				
			  
		}else
		{			
			echo "['0', 0, '0', '', 0, '0', '']";
		}
	?>		
		
    ]);

    // Options
    var options = {
        fontName: 'Arial',
        height: 350,
		title: 'TD Completed per Month',
        curveType: 'function',
        fontSize: 12,
        chartArea: {
            left: '10%',
			right: '2%',
            width: '100%',
            height: 280
        },
        pointSize: 4,
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 12
            }
        },	
		vAxis: {
			viewWindowMode: "explicit", 
			viewWindow:{ min: 0 }
		},	
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    // Draw chart
    var line_chart11 = new google.visualization.LineChart($('#graph7')[0]);
    line_chart11.draw(data, options);
}

//======
google.setOnLoadCallback(graph8);   
function graph8() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['User', 'Leads per User'],
		
		/*["User1 name",20],
		["User2 name",30],
		["User3 name",68],
		["User4 name",40],
		["User5 name",45]*/
		
		<?php
		if($userTLeads)
		{
			foreach($userTLeads as $userTLeadr)
			{
				echo '["'.$userTLeadr->full_name.'",'.$userTLeadr->num.'],';
			}
		}
		else{
			echo "['',0]";
		}
		?>
		
    ]);

    // Options
    var options_pie = {
		title: 'Leads per User (Total: <?php echo $countDepLeads; ?>)',
		sliceVisibilityThreshold: 0,		
        fontName: 'Arial',
        height: 350,    
		colors: ['#3366CC','#DC3912','#109618','#FF9900','#5aafce','#990099','#DD4477'],
        chartArea: {
           left: '15%',
           width: '80%',
           height: 300
        }
    };
    

    // Instantiate and draw our chart, passing in some options.
    var pie = new google.visualization.PieChart($('#graph8')[0]);
    pie.draw(data, options_pie);
}

//======
google.setOnLoadCallback(graph9);   
function graph9() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Source', 'Leads per Source'],
		
		/*["Source1 name",20],
		["Source2 name",30],
		["Source3 name",68],
		["Source4 name",40],
		["Source5 name",45]*/
		
		<?php
		if($perCatAndSource)
		{
			foreach($perCatAndSource as $percasr)
			{
				echo '["'.$percasr->full_name.'",'.$percasr->num.'],';
			}
		}
		else{
			echo "['',0]";
		}
		?>
		
		
    ]);

    // Options
    var options_pie = {
		title: 'Leads Source (Date Range: Lead Created)',
		sliceVisibilityThreshold: 0,		
        fontName: 'Arial',
        height: 350,    
		colors: ['#3366CC','#DC3912','#109618','#FF9900','#5aafce','#990099','#DD4477'],
        chartArea: {
           left: '15%',
           width: '80%',
           height: 300
        }
    };
    

    // Instantiate and draw our chart, passing in some options.
    var pie = new google.visualization.PieChart($('#graph9')[0]);
    pie.draw(data, options_pie);
}

//======
/*google.setOnLoadCallback(graph10);   
function graph10() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Source', 'Leads per Source'],
		
		<?php
		if($perCatAndSourceCurrentMonth)
		{
			foreach($perCatAndSourceCurrentMonth as $percasr)
			{
				echo '["'.$percasr->title.'",'.$percasr->num.'],';
			}
		}
		?>
    ]);

	<?php
	$basedUpon = "";
	if(
	(!isset($_GET['date_from']) || (isset($_GET['date_from']) && $_GET['date_from']==""))
	&& (!isset($_GET['date_from']) || (isset($_GET['date_from']) && $_GET['date_from']==""))
	)
	{
		$basedUpon = "(".date('F Y').")";
	}
	?>
	
    // Options
    var options_pie = {
		title: 'Leads Source <?php echo $basedUpon; ?>',
		sliceVisibilityThreshold: 0,		
        fontName: 'Arial',
        height: 350,    
		colors: ['#3366CC','#DC3912','#109618','#FF9900','#5aafce','#990099','#DD4477'],
        chartArea: {
           left: '15%',
           width: '80%',
           height: 300
        }
    };
    

    // Instantiate and draw our chart, passing in some options.
    var pie = new google.visualization.PieChart($('#graph10')[0]);
    pie.draw(data, options_pie);
}*/

	
	google.setOnLoadCallback(drawBarCP);
	// Chart settings
	function drawBarCP() {

		// Data
		var data = google.visualization.arrayToDataTable([
			['GCarType', 'Purcahsed %', { role: 'annotation' }, {role: 'tooltip'}, {role: 'style'}],
			
			/*['Car1', 20, 20, '#FF9900' ,10, 10, '#3366CC' ,5, 5, '#26A69A'],
			['Car2', 40, 40, '#FF9900' ,5, 5, '#3366CC' ,2, 2, '#26A69A'],
			['Car3', 10, 10, '#FF9900' ,10, 10, '#3366CC' ,0, 0, '#26A69A'],
			['Car4', 30, 30, '#FF9900' , 8, 8, '#3366CC' ,1, 1, '#26A69A']*/

			<?php
			if($carsConversions3)
			{
				foreach($carsConversions3 as $carC)
				{
					if($carC->requests_count==0) $carC->requests_count=1;
					echo "['".$carC->name."', ".round((($carC->purchased_count/$carC->requests_count)*100),2).", '".round((($carC->purchased_count/$carC->requests_count)*100),2)."%', '# Purchased: ".($carC->purchased_count)."', '#109618'],";
				}
			}else
			{
				echo "[0,0,0,'']"; 
			}
			?>

		]);

		// Options
		var options_bar = {
			fontName: 'Arial',
			title: 'Leads to Purchased Progress',			
			height: 400,
			fontSize: 12,
			colors: ['#109618', '#3366CC', '#FF9900'],
			chartArea: {
				left: '15%',
				width: '70%',            			
				height: 350			
			},
			tooltip: {
				textStyle: {
					fontName: 'Arial',
					fontSize: 13
				}
			},
			vAxis: {
				gridlines:{
					color: '#e5e5e5',
					count: 10
				},
				minValue: 0
			},
			legend: {
				position: 'bottom',
				alignment: 'center',
				textStyle: {
					fontSize: 12
				}
			},
			annotations: {alwaysOutside: true},
		};

		// Draw chart
		var bar = new google.visualization.BarChart($('#car_purc_conv')[0]);
		bar.draw(data, options_bar);

	}

google.setOnLoadCallback(drawBar);
function drawBar() {

    // Data
    var data = google.visualization.arrayToDataTable([
        /*['GCarType', 'Scheduled TD %', { role: 'annotation' }, {role: 'tooltip'}, {role: 'style'}, 'Completed TD %', { role: 'annotation' }, {role: 'tooltip'} , {role: 'style'}, 'Purcahsed %', { role: 'annotation' }, {role: 'tooltip'}, {role: 'style'}],*/
		
		['GCarType', 'Scheduled TD %', { role: 'annotation' }, {role: 'tooltip'}, {role: 'style'}, 'Completed TD %', { role: 'annotation' }, {role: 'tooltip'} , {role: 'style'}],
		
        /*['Car1', 20, 20, '#FF9900' ,10, 10, '#3366CC' ,5, 5, '#26A69A'],
        ['Car2', 40, 40, '#FF9900' ,5, 5, '#3366CC' ,2, 2, '#26A69A'],
        ['Car3', 10, 10, '#FF9900' ,10, 10, '#3366CC' ,0, 0, '#26A69A'],
        ['Car4', 30, 30, '#FF9900' , 8, 8, '#3366CC' ,1, 1, '#26A69A']*/

		<?php
		if($carsConversions)
		{
			foreach($carsConversions as $carC)
			{
				//echo "['".$carC->name."', ".round((($carC->scheduled_count/$carC->requests_count)*100),2).", '".round((($carC->scheduled_count/$carC->requests_count)*100),2)."%', '# Scheduled: ".($carC->scheduled_count)."', '#FF9900' , ".round((($carC->completed_count/$carC->requests_count)*100),2).", '".round((($carC->completed_count/$carC->requests_count)*100),2)."%', '# Completed: ".($carC->completed_count)."', '#3366CC', ".round((($carC->purchased_count/$carC->requests_count)*100),2).", '".round((($carC->purchased_count/$carC->requests_count)*100),2)."%', '# Purchased: ".($carC->purchased_count)."', '#109618'],";
				
				echo "['".$carC->name."', ".round((($carC->scheduled_count/$carC->requests_count)*100),2).", '".round((($carC->scheduled_count/$carC->requests_count)*100),2)."%', '# Scheduled: ".($carC->scheduled_count)."', '#FF9900' , ".round((($carC->completed_count/$carC->requests_count)*100),2).", '".round((($carC->completed_count/$carC->requests_count)*100),2)."%', '# Completed: ".($carC->completed_count)."', '#3366CC'],";
			}
		}else
		{
			echo "['', 0,0,0,'',0,0,0,'',0,0,0,'']"; 
		}
		?>

    ]);

    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Leads to Test Drives Progress',	
		<?php if($carsConversions)
		{ ?>
		height: 900,        
		<?php }else{ ?>
		height: 200,
		<?php } ?>
        fontSize: 12,
		colors: ['#FF9900', '#3366CC', '#109618'],
        chartArea: {
            left: '15%',
            width: '70%',            
			<?php if($carsConversions)
			{ ?>
			height: 800       
			<?php }else{ ?>
			height: 100
			<?php } ?>
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    // Draw chart
    var bar = new google.visualization.BarChart($('#car_conversions')[0]);
    bar.draw(data, options_bar);

}	
</script>

<style>
.ircd {
    float: left;
    width: 48%;
    text-align: center;
    background-color: green;
    color: #fff;
    margin: 0 0 10px;
    padding: 15px 0;
}
p.c_rating {
	color: #fff;
    font-size: 30px;
    font-weight: bold;
    margin: 10px 0 0;
}
.ircd.one {
background: #ffa84c; /* Old browsers */
background: -moz-linear-gradient(top, #ffa84c 0%, #ff7b0d 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #ffa84c 0%,#ff7b0d 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #ffa84c 0%,#ff7b0d 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffa84c', endColorstr='#ff7b0d',GradientType=0 ); /* IE6-9 */
	
}
.ircd.two {
float:right;
background: #63b6db; /* Old browsers */
background: -moz-linear-gradient(top, #63b6db 0%, #309dcf 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #63b6db 0%,#309dcf 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #63b6db 0%,#309dcf 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#63b6db', endColorstr='#309dcf',GradientType=0 ); /* IE6-9 */
}
.ircd.three {
background: #a4b357; /* Old browsers */
background: -moz-linear-gradient(top, #a4b357 0%, #75890c 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #a4b357 0%,#75890c 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #a4b357 0%,#75890c 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a4b357', endColorstr='#75890c',GradientType=0 ); /* IE6-9 */
}
.ircd.four {
float:right;
background: #f3e2c7; /* Old browsers */
background: -moz-linear-gradient(top,  #f3e2c7 0%, #c19e67 50%, #b68d4c 100%, #e9d4b3 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top,  #f3e2c7 0%,#c19e67 50%,#b68d4c 100%,#e9d4b3 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom,  #f3e2c7 0%,#c19e67 50%,#b68d4c 100%,#e9d4b3 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f3e2c7', endColorstr='#e9d4b3',GradientType=0 ); /* IE6-9 */

}
</style>


<div id="graph1" style="text-align:center; width: 50%; height: 500px; float:left"></div>
<div id="graph2" style="text-align:center; width: 50%; height: 500px; float:right"></div>

<div style="clear:both;"></div>
<br><br><br>

<div id="graph3" style="text-align:center; width: 100%; height: 500px;"></div>

<div style="clear:both;"></div>
<br><br><br>

<div id="graph4" style="text-align:center; width: 50%; height: 500px; float:left"></div>
<div id="graph5" style="text-align:center; width: 50%; height: 500px; float:right"></div>

<div style="clear:both;"></div>
<br><br><br>

<div id="graph6" style="text-align:center; width: 100%; height: 300px;"></div>

<div style="clear:both;"></div>
<br><br><br>

<div id="graph7" style="text-align:center; width: 100%; height: 350px;"></div>

<div style="clear:both;"></div>
<br><br><br>

<div style="text-align:center; width: 100%; height: 350px;"> 
<div id="graph8" style="text-align:center; width: 100%; height: 350px;"></div>
</div>

<div style="clear:both;"></div>
<br><br>

<div id="graph9" style="text-align:center; width: 100%; height: 350px; float:left"></div>

<!--<div id="graph10" style="text-align:center; width: 50%; height: 350px; float:left"></div>-->
<!--////////////////////start conversions////////////////-->

<!----
<div id="graph11" style="text-align:center; width: 50%; height: 320px; float:right; padding-left:15px;">
	
	<b style="text-align:left; display: block; margin-bottom:15px;">Leads Conversions</b>
	<div style="clear:both;"></div>
	
	<div class="ircd one">
		<b>Scheduled Test Drives %</b><br> 
		(Scheduled / Leads ) (<?php echo $countDepScheduled; ?> / <?php echo $countDepLeads; ?>)		
		<p class="c_rating"><?php if($countDepLeads) echo round(($countDepScheduled/$countDepLeads)*100,2); ?> %</p>
	</div>


	<div class="ircd two">
		<b>Completed Test Drives %</b><br>
		(Completed TD / Leads ) (<?php echo $countDepCompleted; ?> / <?php echo $countDepLeads; ?>)
		<p class="c_rating"><?php if($countDepLeads) echo round(($countDepCompleted/$countDepLeads)*100,2);  ?> %</p>
	</div>

	<div class="ircd three">
		<b>Purchased Vehicles %</b><br>
		(Purchased Vehicles / Leads) (<?php echo $countDepPurchased; ?> / <?php echo $countDepLeads; ?>)
		<p class="c_rating"><?php if($countDepLeads) echo round(($countDepPurchased/$countDepLeads)*100,2);  ?> %</p>
	</div>

	<div class="ircd four">
		<b>Lost Sales %</b><br>
		(Lost Leads / Leads) (<?php echo $countDepLost; ?> / <?php echo $countDepLeads; ?>)
		<p class="c_rating"><?php if($countDepLeads) echo round(($countDepLost/$countDepLeads)*100,2);  ?> %</p>
	</div>
	
</div>
--->
<div style="clear:both;"></div>
<br>
<!-------------
<div id="graph12" style="text-align:center; width: 50%; height: 320px; float:left; padding-right:15px;">
	<b style="text-align:left; display: block; margin-bottom:15px;">Source Conversions - CallCentrROI</b>
	<div style="clear:both;"></div>
	
	<div class="ircd one">
		<b>Scheduled Test Drives %</b><br> 
		(Scheduled / Leads ) (<?php echo $countDepScheduled1; ?> / <?php echo $countDepLeads1; ?>)
		<p class="c_rating"><?php if($countDepLeads1) echo round(($countDepScheduled1/$countDepLeads1)*100,2);  ?> %</p>
	</div>


	<div class="ircd two">
		<b>Completed Test Drives %</b><br>
		(Completed TD / Leads ) (<?php echo $countDepCompleted1; ?> / <?php echo $countDepLeads1; ?>)
		<p class="c_rating"><?php if($countDepLeads1) echo round(($countDepCompleted1/$countDepLeads1)*100,2);  ?> %</p>
	</div>

	<div class="ircd three">
		<b>Purchased Vehicles %</b><br>
		(Purchased Vehicles / Leads) (<?php echo $countDepPurchased1; ?> / <?php echo $countDepLeads1; ?>)
		<p class="c_rating"><?php if($countDepLeads1) echo round(($countDepPurchased1/$countDepLeads1)*100,2);  ?> %</p>
	</div>

	<div class="ircd four">
		<b>Lost Sales %</b><br>
		(Lost Leads / Leads) (<?php echo $countDepLost1; ?> / <?php echo $countDepLeads1; ?>)
		<p class="c_rating"><?php if($countDepLeads1) echo round(($countDepLost1/$countDepLeads1)*100,2);  ?> %</p>
	</div>
</div>
<div id="graph13" style="text-align:center; width: 50%; height: 320px; float:right; padding-left:15px;">
	<b style="text-align:left; display: block; margin-bottom:15px;">Source Conversions - Call Center</b>
	<div style="clear:both;"></div>
	
	<div class="ircd one">
		<b>Scheduled Test Drives %</b><br> 
		(Scheduled / Leads ) (<?php echo $countDepScheduled2; ?> / <?php echo $countDepLeads2; ?>)
		<p class="c_rating"><?php if($countDepLeads2) echo round(($countDepScheduled2/$countDepLeads2)*100,2);  ?> %</p>
	</div>


	<div class="ircd two">
		<b>Completed Test Drives %</b><br>
		(Completed TD / Leads ) (<?php echo $countDepCompleted2; ?> / <?php echo $countDepLeads2; ?>)
		<p class="c_rating"><?php if($countDepLeads2) echo round(($countDepCompleted2/$countDepLeads2)*100,2);  ?> %</p>
	</div>

	<div class="ircd three">
		<b>Purchased Vehicles %</b><br>
		(Purchased Vehicles / Leads) (<?php echo $countDepPurchased2; ?> / <?php echo $countDepLeads2; ?>)
		<p class="c_rating"><?php if($countDepLeads2) echo round(($countDepPurchased2/$countDepLeads2)*100,2);  ?> %</p>
	</div>

	<div class="ircd four">
		<b>Lost Sales %</b><br>
		(Lost Leads / Leads) (<?php echo $countDepLost2; ?> / <?php echo $countDepLeads2; ?>)
		<p class="c_rating"><?php if($countDepLeads2) echo round(($countDepLost2/$countDepLeads2)*100,2);  ?> %</p>
	</div>
</div>

<div style="clear:both;"></div>
<br>

<div id="graph14" style="text-align:center; width: 50%; height: 320px; float:left; padding-right:15px;">
	<b style="text-align:left; display: block; margin-bottom:15px;">Source Conversions - Inbound Call</b>
	<div style="clear:both;"></div>
	
	<div class="ircd one">
		<b>Scheduled Test Drives %</b><br> 
		(Scheduled / Leads ) (<?php echo $countDepScheduled3; ?> / <?php echo $countDepLeads3; ?>)
		<p class="c_rating"><?php if($countDepLeads3) echo round(($countDepScheduled3/$countDepLeads3)*100,2);  ?> %</p>
	</div>


	<div class="ircd two">
		<b>Completed Test Drives %</b><br>
		(Completed TD / Leads ) (<?php echo $countDepCompleted3; ?> / <?php echo $countDepLeads3; ?>)
		<p class="c_rating"><?php if($countDepLeads3) echo round(($countDepCompleted3/$countDepLeads3)*100,2);  ?> %</p>
	</div>

	<div class="ircd three">
		<b>Purchased Vehicles %</b><br>
		(Purchased Vehicles / Leads) (<?php echo $countDepPurchased3; ?> / <?php echo $countDepLeads3; ?>)
		<p class="c_rating"><?php if($countDepLeads3) echo round(($countDepPurchased3/$countDepLeads3)*100,2);  ?> %</p>
	</div>

	<div class="ircd four">
		<b>Lost Sales %</b><br>
		(Lost Leads / Leads) (<?php echo $countDepLost3; ?> / <?php echo $countDepLeads3; ?>)
		<p class="c_rating"><?php if($countDepLeads3) echo round(($countDepLost3/$countDepLeads3)*100,2);  ?> %</p>
	</div>
</div>
<div id="graph15" style="text-align:center; width: 50%; height: 320px; float:right; padding-left:15px;">
	<b style="text-align:left; display: block; margin-bottom:15px;">Source Conversions - Inbound Email</b>
	<div style="clear:both;"></div>
	
	<div class="ircd one">
		<b>Scheduled Test Drives %</b><br> 
		(Scheduled / Leads ) (<?php echo $countDepScheduled4; ?> / <?php echo $countDepLeads4; ?>)
		<p class="c_rating"><?php if($countDepLeads4) echo round(($countDepScheduled4/$countDepLeads4)*100,2);  ?> %</p>
	</div>


	<div class="ircd two">
		<b>Completed Test Drives %</b><br>
		(Completed TD / Leads ) (<?php echo $countDepCompleted4; ?> / <?php echo $countDepLeads4; ?>)
		<p class="c_rating"><?php if($countDepLeads4) echo round(($countDepCompleted4/$countDepLeads4)*100,2);  ?> %</p>
	</div>

	<div class="ircd three">
		<b>Purchased Vehicles %</b><br>
		(Purchased Vehicles / Leads) (<?php echo $countDepPurchased4; ?> / <?php echo $countDepLeads4; ?>)
		<p class="c_rating"><?php if($countDepLeads4) echo round(($countDepPurchased4/$countDepLeads4)*100,2);  ?> %</p>
	</div>

	<div class="ircd four">
		<b>Lost Sales %</b><br>
		(Lost Leads / Leads) (<?php echo $countDepLost4; ?> / <?php echo $countDepLeads4; ?>)
		<p class="c_rating"><?php if($countDepLeads4) echo round(($countDepLost4/$countDepLeads4)*100,2);  ?> %</p>
	</div>
</div>

<div style="clear:both;"></div>
<br>

<div id="graph16" style="text-align:center; width: 50%; height: 320px; float:left; padding-right:15px;">
	<b style="text-align:left; display: block; margin-bottom:15px;">Source Conversions - Internal Referral</b>
	<div style="clear:both;"></div>
	
	<div class="ircd one">
		<b>Scheduled Test Drives %</b><br> 
		(Scheduled / Leads ) (<?php echo $countDepScheduled5; ?> / <?php echo $countDepLeads5; ?>)
		<p class="c_rating"><?php if($countDepLeads5) echo round(($countDepScheduled5/$countDepLeads5)*100,2);  ?> %</p>
	</div>


	<div class="ircd two">
		<b>Completed Test Drives %</b><br>
		(Completed TD / Leads ) (<?php echo $countDepCompleted5; ?> / <?php echo $countDepLeads5; ?>)
		<p class="c_rating"><?php if($countDepLeads5) echo round(($countDepCompleted5/$countDepLeads5)*100,2);  ?> %</p>
	</div>

	<div class="ircd three">
		<b>Purchased Vehicles %</b><br>
		(Purchased Vehicles / Leads) (<?php echo $countDepPurchased5; ?> / <?php echo $countDepLeads5; ?>)
		<p class="c_rating"><?php if($countDepLeads5) echo round(($countDepPurchased5/$countDepLeads5)*100,2);  ?> %</p>
	</div>

	<div class="ircd four">
		<b>Lost Sales %</b><br>
		(Lost Leads / Leads) (<?php echo $countDepLost5; ?> / <?php echo $countDepLeads5; ?>)
		<p class="c_rating"><?php if($countDepLeads5) echo round(($countDepLost5/$countDepLeads5)*100,2);  ?> %</p>
	</div>
</div>
<div id="graph17" style="text-align:center; width: 50%; height: 320px; float:right; padding-left:15px;">
	<b style="text-align:left; display: block; margin-bottom:15px;">Source Conversions- Online & Digital</b>
	<div style="clear:both;"></div>
	
	<div class="ircd one">
		<b>Scheduled Test Drives %</b><br> 
		(Scheduled / Leads ) (<?php echo $countDepScheduled6; ?> / <?php echo $countDepLeads6; ?>)
		<p class="c_rating"><?php if($countDepLeads6) echo round(($countDepScheduled6/$countDepLeads6)*100,2);  ?> %</p>
	</div>


	<div class="ircd two">
		<b>Completed Test Drives %</b><br>
		(Completed TD / Leads ) (<?php echo $countDepCompleted6; ?> / <?php echo $countDepLeads6; ?>)
		<p class="c_rating"><?php if($countDepLeads6) echo round(($countDepCompleted6/$countDepLeads6)*100,2);  ?> %</p>
	</div>

	<div class="ircd three">
		<b>Purchased Vehicles %</b><br>
		(Purchased Vehicles / Leads) (<?php echo $countDepPurchased6; ?> / <?php echo $countDepLeads6; ?>)
		<p class="c_rating"><?php if($countDepLeads6) echo round(($countDepPurchased6/$countDepLeads6)*100,2);  ?> %</p>
	</div>

	<div class="ircd four">
		<b>Lost Sales %</b><br>
		(Lost Leads / Leads) (<?php echo $countDepLost6; ?> / <?php echo $countDepLeads6; ?>)
		<p class="c_rating"><?php if($countDepLeads6) echo round(($countDepLost6/$countDepLeads6)*100,2);  ?> %</p>
	</div>
</div>

<div style="clear:both;"></div>
<br>

<div id="graph18" style="text-align:center; width: 50%; height: 320px; float:left; padding-right:15px;">
	<b style="text-align:left; display: block; margin-bottom:15px;">Source Conversions - Social Media</b>
	<div style="clear:both;"></div>
	
	<div class="ircd one">
		<b>Scheduled Test Drives %</b><br> 
		(Scheduled / Leads ) (<?php echo $countDepScheduled7; ?> / <?php echo $countDepLeads7; ?>)
		<p class="c_rating"><?php if($countDepLeads7) echo round(($countDepScheduled7/$countDepLeads7)*100,2);  ?> %</p>
	</div>


	<div class="ircd two">
		<b>Completed Test Drives %</b><br>
		(Completed TD / Leads ) (<?php echo $countDepCompleted7; ?> / <?php echo $countDepLeads7; ?>)
		<p class="c_rating"><?php if($countDepLeads7) echo round(($countDepCompleted7/$countDepLeads7)*100,2);  ?> %</p>
	</div>

	<div class="ircd three">
		<b>Purchased Vehicles %</b><br>
		(Purchased Vehicles / Leads) (<?php echo $countDepPurchased7; ?> / <?php echo $countDepLeads7; ?>)
		<p class="c_rating"><?php if($countDepLeads7) echo round(($countDepPurchased7/$countDepLeads7)*100,2);  ?> %</p>
	</div>

	<div class="ircd four">
		<b>Lost Sales %</b><br>
		(Lost Leads / Leads) (<?php echo $countDepLost7; ?> / <?php echo $countDepLeads7; ?>)
		<p class="c_rating"><?php if($countDepLeads7) echo round(($countDepLost7/$countDepLeads7)*100,2);  ?> %</p>
	</div>
</div>
<div id="graph19" style="text-align:center; width: 50%; height: 320px; float:right; padding-left:15px;">
	<b style="text-align:left; display: block; margin-bottom:15px;">Source Conversions - Walk Ins</b>
	<div style="clear:both;"></div>
	
	<div class="ircd one">
		<b>Scheduled Test Drives %</b><br> 
		(Scheduled / Leads ) (<?php echo $countDepScheduled8; ?> / <?php echo $countDepLeads8; ?>)
		<p class="c_rating"><?php if($countDepLeads8) echo round(($countDepScheduled8/$countDepLeads8)*100,2);  ?> %</p>
	</div>


	<div class="ircd two">
		<b>Completed Test Drives %</b><br>
		(Completed TD / Leads ) (<?php echo $countDepCompleted8; ?> / <?php echo $countDepLeads8; ?>)
		<p class="c_rating"><?php if($countDepLeads8) echo round(($countDepCompleted8/$countDepLeads8)*100,2);  ?> %</p>
	</div>

	<div class="ircd three">
		<b>Purchased Vehicles %</b><br>
		(Purchased Vehicles / Leads) (<?php echo $countDepPurchased8; ?> / <?php echo $countDepLeads8; ?>)
		<p class="c_rating"><?php if($countDepLeads8) echo round(($countDepPurchased8/$countDepLeads8)*100,2);  ?> %</p>
	</div>

	<div class="ircd four">
		<b>Lost Sales %</b><br>
		(Lost Leads / Leads) (<?php echo $countDepLost8; ?> / <?php echo $countDepLeads8; ?>)
		<p class="c_rating"><?php if($countDepLeads8) echo round(($countDepLost8/$countDepLeads8)*100,2);  ?> %</p>
	</div>
</div>

<div style="clear:both;"></div>
<br>

<div id="graph20" style="text-align:center; width: 50%; height: 320px; float:left; padding-right:15px;">
	<b style="text-align:left; display: block; margin-bottom:15px;">Source Conversions - Outsourced</b>
	<div style="clear:both;"></div>
	
	<div class="ircd one">
		<b>Scheduled Test Drives %</b><br> 
		(Scheduled / Leads ) (<?php echo $countDepScheduled9; ?> / <?php echo $countDepLeads9; ?>)
		<p class="c_rating"><?php if($countDepLeads9) echo round(($countDepScheduled9/$countDepLeads9)*100,2);  ?> %</p>
	</div>


	<div class="ircd two">
		<b>Completed Test Drives %</b><br>
		(Completed TD / Leads ) (<?php echo $countDepCompleted9; ?> / <?php echo $countDepLeads9; ?>)
		<p class="c_rating"><?php if($countDepLeads9) echo round(($countDepCompleted9/$countDepLeads9)*100,2);  ?> %</p>
	</div>

	<div class="ircd three">
		<b>Purchased Vehicles %</b><br>
		(Purchased Vehicles / Leads) (<?php echo $countDepPurchased9; ?> / <?php echo $countDepLeads9; ?>)
		<p class="c_rating"><?php if($countDepLeads9) echo round(($countDepPurchased9/$countDepLeads9)*100,2);  ?> %</p>
	</div>

	<div class="ircd four">
		<b>Lost Sales %</b><br>
		(Lost Leads / Leads) (<?php echo $countDepLost9; ?> / <?php echo $countDepLeads9; ?>)
		<p class="c_rating"><?php if($countDepLeads9) echo round(($countDepLost9/$countDepLeads9)*100,2);  ?> %</p>
	</div>
</div>
<div id="graph21" style="text-align:center; width: 50%; height: 320px; float:right; padding-left:15px;">
	<b style="text-align:left; display: block; margin-bottom:15px;">Source Conversions - TD</b>
	<div style="clear:both;"></div>
	
	<div class="ircd one">
		<b>Scheduled Test Drives %</b><br> 
		(Scheduled / Leads ) (<?php echo $countDepScheduled10; ?> / <?php echo $countDepLeads10; ?>)
		<p class="c_rating"><?php if($countDepLeads10) echo round(($countDepScheduled10/$countDepLeads10)*100,2);  ?> %</p>
	</div>


	<div class="ircd two">
		<b>Completed Test Drives %</b><br>
		(Completed TD / Leads ) (<?php echo $countDepCompleted10; ?> / <?php echo $countDepLeads10; ?>)
		<p class="c_rating"><?php if($countDepLeads10) echo round(($countDepCompleted10/$countDepLeads10)*100,2);  ?> %</p>
	</div>

	<div class="ircd three">
		<b>Purchased Vehicles %</b><br>
		(Purchased Vehicles / Leads) (<?php echo $countDepPurchased10; ?> / <?php echo $countDepLeads10; ?>)
		<p class="c_rating"><?php if($countDepLeads10) echo round(($countDepPurchased10/$countDepLeads10)*100,2);  ?> %</p>
	</div>

	<div class="ircd four">
		<b>Lost Sales %</b><br>
		(Lost Leads / Leads) (<?php echo $countDepLost10; ?> / <?php echo $countDepLeads10; ?>)
		<p class="c_rating"><?php if($countDepLeads10) echo round(($countDepLost10/$countDepLeads10)*100,2); ?> %</p>
	</div>
</div>

<!--////////////////////end conversions////////////////-->

<div style="clear:both;"></div>
<br><br>
!>
<div style="width: 50%; float:left">
	<div id="indCHOSDiv" style="display: block; margin: 0 auto; text-align:center; width: 95%; height: 200px; float:left; overflow-x:hidden; overflow-y:auto;">
	<b style="float:left;">Leads by Car</b>
	<div class="chart-container">
		<div class="chart" id="leads_bycar"></div>
	</div>
	<?php
	$date_from="";
	$date_to="";		
	if(isset($_GET['date_from']) && $_GET['date_from']!="")	$date_from = $_GET['date_from'];
	if(isset($_GET['date_to']) && $_GET['date_to']!="")	$date_to = $_GET['date_to'];
	?>
	<script>
	stackedMultiples('#leads_bycar', 900, "<?php echo base_url();?>manager_report/d3_bars_hierarchical_cars/<?php echo $dep_id."/".$date_from."/".$date_to; ?>");
	</script>
	</div>

	<div style="clear:both;"></div>
	<br><br><br>
	
	
	<div id="car_purc_conv" style="text-align:center; width: 95%; height: 400px;"></div>

</div>
	
	
<?php 
$carcheight = "200px";
if(isset($carsConversions))
{ 
	$carcheight = "900px";
}
?>
<div id="car_conversions" style="text-align:center; width: 50%; height: <?php echo $carcheight;?>; float:right"></div>
	
<div style="clear:both;"></div>

<br><br><br>

<!--


	<u>	
	Users > Cars(Type) > (Completed, Not Completed))
	</u>
	<div class="chart-container">
		<div class="chart" id="d3-hierarchical-bars"></div>
	</div>

	<script type="text/javascript">
	<?php 
		$date_from="";
		$date_to="";
		if(isset($_GET['date_from']) && $_GET['date_from']!="")	$date_from = $_GET['date_from'];
		if(isset($_GET['date_to']) && $_GET['date_to']!="")	$date_to = $_GET['date_to'];
	?>
	// Initialize chart
	//stackedMultiples('#d3-hierarchical-bars', 900, "<?php //echo base_url();?>manager_report/d3_bars_hierarchical_cars2/<?php //echo $dep_id."/".$date_from."/".$date_to; ?>");

	</script>

<div style="clear:both;"></div>
<br><br>-->



	<div style="float:left; width:50%; overflow-x:hidden; overflow-y:auto;">
		<strong style="display:block; margin-bottom:15px; text-align:center;">Leads Created and Assigned (Car > Sales Consultant)</strong>
		<div id="hierarchical_10" style="text-align:center; width: 95%; height: 500px;"></div>
	</div>
	
	<div style="float:left; width:50%; overflow-x:hidden; overflow-y:auto;">
		<strong style="display:block; margin-bottom:15px; text-align:center;">Leads per source for sales consultant (Sales Consultant > Cars > Source)</strong>
		<div id="hierarchical_11" style="text-align:center; width: 95%; height: 500px;"></div>
	</div>
	
	<div style="clear:both;"></div>
	<br><br>
	
	<div style="float:left; width:50%; overflow-x:hidden; overflow-y:auto;">
		<strong style="display:block; margin-bottom:15px; text-align:center;">Test Drive Completed (Car > Sales consultant)</strong>
		<div id="hierarchical_12" style="text-align:center; width: 95%; height: 500px;"></div>
	</div>
	
	<div style="float:left; width:50%; overflow-x:hidden; overflow-y:auto;">
		<strong style="display:block; margin-bottom:15px; text-align:center;">Test Drive Completed Sources (Source > Car)</strong>
		<div id="hierarchical_13" style="text-align:center; width: 95%; height: 500px;"></div>
	</div>
	
	<div style="clear:both;"></div>
	<br><br>
	
	<div style="float:left; width:50%; overflow-x:hidden; overflow-y:auto;">
		<strong style="display:block; margin-bottom:15px; text-align:center;">Leads Per Source (Source > Car)</strong>
		<div id="hierarchical_14" style="text-align:center; width: 95%; height: 500px;"></div>
	</div>
	
	<div style="float:left; width:50%; overflow-x:hidden; overflow-y:auto;">
		<strong style="display:block; margin-bottom:15px; text-align:center;">Purchase (Sales Consultant > Car)</strong>
		<div id="hierarchical_15" style="text-align:center; width: 95%; height: 500px;"></div>
	</div>
	
	<div style="clear:both;"></div>
	<br><br>
	
	<div style="float:left; width:50%; overflow-x:hidden; overflow-y:auto;">
		<strong style="display:block; margin-bottom:15px; text-align:center;">Purchase (Car > Sales Consultant)</strong>
		<div id="hierarchical_16" style="text-align:center; width: 95%; height: 500px;"></div>
	</div>
	
	<div style="float:left; width:50%; overflow-x:hidden; overflow-y:auto;">
		<strong style="display:block; margin-bottom:15px; text-align:center;">Purchase (Source > Sales Consultant > Car)</strong>
		<div id="hierarchical_17" style="text-align:center; width: 95%; height: 500px;"></div>
	</div>
	
	<div style="clear:both;"></div>
	<br><br>	
	
	<div style="float:left; width:50%; overflow-x:hidden; overflow-y:auto;">
		<strong style="display:block; margin-bottom:15px; text-align:center;">Purchase (Source > Car > Sales Consultant)</strong>
		<div id="hierarchical_18" style="text-align:center; width: 95%; height: 500px;"></div>
	</div>
	
	<div style="clear:both;"></div>
	<br><br>





	<script type="text/javascript">
	<?php 
		$date_from="";
		$date_to="";
		if(isset($_GET['date_from']) && $_GET['date_from']!="")	$date_from = $_GET['date_from'];
		if(isset($_GET['date_to']) && $_GET['date_to']!="")	$date_to = $_GET['date_to'];
	?>
	
	stackedMultiples('#hierarchical_10', 900, "<?php echo base_url();?>manager_report/car_sales_consultant_hierarchical/<?php echo $dep_id."/".$date_from."/".$date_to; ?>");
	
	stackedMultiples('#hierarchical_11', 900, "<?php echo base_url();?>manager_report/car_sales_consultant_hierarchical3/<?php echo $dep_id."/".$date_from."/".$date_to; ?>");
	
	stackedMultiples('#hierarchical_12', 900, "<?php echo base_url();?>manager_report/car_sales_consultant_hierarchical2/<?php echo $dep_id."/".$date_from."/".$date_to; ?>");
	
	stackedMultiples('#hierarchical_13', 900, "<?php echo base_url();?>manager_report/car_sales_consultant_hierarchical4/<?php echo $dep_id."/".$date_from."/".$date_to; ?>");
	
	stackedMultiples('#hierarchical_14', 900, "<?php echo base_url();?>manager_report/car_sales_consultant_hierarchical5/<?php echo $dep_id."/".$date_from."/".$date_to; ?>");
	
	stackedMultiples('#hierarchical_15', 900, "<?php echo base_url();?>manager_report/car_sales_consultant_hierarchical6/<?php echo $dep_id."/".$date_from."/".$date_to; ?>");
	
	stackedMultiples('#hierarchical_16', 900, "<?php echo base_url();?>manager_report/car_sales_consultant_hierarchical7/<?php echo $dep_id."/".$date_from."/".$date_to; ?>");
	
	stackedMultiples('#hierarchical_17', 900, "<?php echo base_url();?>manager_report/car_sales_consultant_hierarchical8/<?php echo $dep_id."/".$date_from."/".$date_to; ?>");
	
	stackedMultiples('#hierarchical_18', 1500, "<?php echo base_url();?>manager_report/car_sales_consultant_hierarchical9/<?php echo $dep_id."/".$date_from."/".$date_to; ?>");


	</script>

	
	
	
	
	
	
	
<?php	
//==============end grapahs=================
}
?>

            </div>            
        </div>
    </div>
</section>

<?php

function secondsToDays($seconds)
{
	
	$text = 0;
			
	$days    = floor($seconds / 86400);
	$hours   = floor(($seconds - ($days * 86400)) / 3600);
	$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);			

	if($days>0)
		$text =  $days." Days ";
	
		//$text =  $days." Days ". $hours." Hrs. ".$minutes." Min.";
	/*elseif($hours>0)
		$text =  $hours." Hrs. ".$minutes." Min.";
	elseif($minutes>0)
		$text = $minutes." Min.";*/

	return $text;

}
	
?>