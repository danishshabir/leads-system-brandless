<script type="text/javascript">
//=====start Hieracrchical bar chart====

    // Chart setup
    function stackedMultiples(element, height, jsonURL) {


        // Basic setup
        // ------------------------------

        // Define main variables
        var d3Container = d3.select(element),
            margin = {top: 25, right: 40, bottom: 20, left: 180},
            width = d3Container.node().getBoundingClientRect().width - margin.left - margin.right,
            height = height - margin.top - margin.bottom - 5,
            barHeight = 30,
            duration = 750,
            delay = 25;



        // Construct scales
        // ------------------------------

        // Horizontal
        var x = d3.scale.linear()
            .range([0, width]);

        // Colors
        var color = d3.scale.ordinal()
            .range(["#26A69A", "#ccc"]);



        // Create axes
        // ------------------------------

        // Horizontal
        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("top");



        // Create chart
        // ------------------------------

        // Add SVG element
        var container = d3Container.append("svg");

        // Add SVG group
        var svg = container
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


        // Construct chart layout
        // ------------------------------

        // Partition
        var partition = d3.layout.partition()
            .value(function(d) { return d.size; });



        // Load data
        // ------------------------------

		//d3.json("<?php echo base_url();?>report/d3_bars_hierarchical_cars", function(error, root) {		
			d3.json(jsonURL, function(error, root) {	

			partition.nodes(root);			
			
			//custom extra logic for height			
			if(root.value!=0) {
				$("#indCHOSDiv").css( "height", "400px" );			
				//console.log("#indCHOSDiv increasing  height");
			}
			//====			
            
            x.domain([0, root.value]).nice();
            down(root, 0);
        });


        //
        // Append chart elements
        //

        // Add background bars
        svg.append("rect")
            .attr("class", "d3-bars-background")
            .attr("width", width)
            .attr("height", height)
            .style("fill", "#fff")
            .on("click", up);


        // Append axes
        // ------------------------------

        // Horizontal
        svg.append("g")
            .attr("class", "d3-axis d3-axis-horizontal d3-axis-strong");


        // Append bars
        // ------------------------------

        // Create hierarchical structure
        function down(d, i) {
            if (!d.children || this.__transition__) return;
            var end = duration + d.children.length * delay;

            // Mark any currently-displayed bars as exiting.
            var exit = svg.selectAll(".enter")
                .attr("class", "exit");

            // Entering nodes immediately obscure the clicked-on bar, so hide it.
            exit.selectAll("rect").filter(function(p) { return p === d; })
                .style("fill-opacity", 1e-6);

            // Enter the new bars for the clicked-on data.
            // Per above, entering bars are immediately visible.
            var enter = bar(d)
                .attr("transform", stack(i))
                .style("opacity", 1);

            // Have the text fade-in, even though the bars are visible.
            // Color the bars as parents; they will fade to children if appropriate.
            enter.select("text").style("fill-opacity", 1e-6);
            enter.select("rect").style("fill", color(true));

            // Update the x-scale domain.
            x.domain([0, d3.max(d.children, function(d) { return d.value; })]).nice();

            // Update the x-axis.
            svg.selectAll(".d3-axis-horizontal").transition()
                .duration(duration)
                .call(xAxis);

            // Transition entering bars to their new position.
            var enterTransition = enter.transition()
                .duration(duration)
                .delay(function(d, i) { return i * delay; })
                .attr("transform", function(d, i) { return "translate(0," + barHeight * i * 1.2 + ")"; });

            // Transition entering text.
            enterTransition.select("text")
                .style("fill-opacity", 1);

            // Transition entering rects to the new x-scale.
            enterTransition.select("rect")
                .attr("width", function(d) { return x(d.value); })
                .style("fill", function(d) { return color(!!d.children); });

            // Transition exiting bars to fade out.
            var exitTransition = exit.transition()
                .duration(duration)
                .style("opacity", 1e-6)
                .remove();

            // Transition exiting bars to the new x-scale.
            exitTransition.selectAll("rect")
                .attr("width", function(d) { return x(d.value); });

            // Rebind the current node to the background.
            svg.select(".d3-bars-background")
                .datum(d)
                .transition()
                .duration(end);

            d.index = i;
        }

        // Return to parent level
        function up(d) {
            if (!d.parent || this.__transition__) return;
            var end = duration + d.children.length * delay;

            // Mark any currently-displayed bars as exiting.
            var exit = svg.selectAll(".enter")
                .attr("class", "exit");

            // Enter the new bars for the clicked-on data's parent.
            var enter = bar(d.parent)
                .attr("transform", function(d, i) { return "translate(0," + barHeight * i * 1.2 + ")"; })
                .style("opacity", 1e-6);

            // Color the bars as appropriate.
            // Exiting nodes will obscure the parent bar, so hide it.
            enter.select("rect")
                .style("fill", function(d) { return color(!!d.children); })
                .filter(function(p) { return p === d; })
                .style("fill-opacity", 1e-6);

            // Update the x-scale domain.
            x.domain([0, d3.max(d.parent.children, function(d) { return d.value; })]).nice();

            // Update the x-axis.
            svg.selectAll(".d3-axis-horizontal").transition()
                .duration(duration)
                .call(xAxis);

            // Transition entering bars to fade in over the full duration.
            var enterTransition = enter.transition()
                .duration(end)
                .style("opacity", 1);

            // Transition entering rects to the new x-scale.
            // When the entering parent rect is done, make it visible!
            enterTransition.select("rect")
                .attr("width", function(d) { return x(d.value); })
                .each("end", function(p) { if (p === d) d3.select(this).style("fill-opacity", null); });

            // Transition exiting bars to the parent's position.
            var exitTransition = exit.selectAll("g").transition()
                .duration(duration)
                .delay(function(d, i) { return i * delay; })
                .attr("transform", stack(d.index));

            // Transition exiting text to fade out.
            exitTransition.select("text")
                .style("fill-opacity", 1e-6);

            // Transition exiting rects to the new scale and fade to parent color.
            exitTransition.select("rect")
                .attr("width", function(d) { return x(d.value); })
                .style("fill", color(true));

            // Remove exiting nodes when the last child has finished transitioning.
            exit.transition()
                .duration(end)
                .remove();

            // Rebind the current parent to the background.
            svg.select(".d3-bars-background")
                .datum(d.parent)
                .transition()
                .duration(end);
        }

        // Creates a set of bars for the given data node, at the specified index.
        function bar(d) {
            var bar = svg.insert("g", ".d3-axis-vertical")
                .attr("class", "enter")
                .attr("transform", "translate(0,5)")
                .selectAll("g")
                .data(d.children)
                .enter()
                .append("g")
                    .style("cursor", function(d) { return !d.children ? null : "pointer"; })
                    .on("click", down);

            bar.append("text")
                .attr("x", -6)
                .attr("y", barHeight / 2)
                .attr("dy", ".35em")
                .style("text-anchor", "end")
                .text(function(d) { return d.name; });
							

            bar.append("rect") 
                .attr("width", function(d) { return x(d.value); })
                .attr("height", barHeight);
				
			setTimeout(
			  function() 
			  {
				bar.append("text")
                .attr("x", function(d) { 
					var ahead = 25;
					if(d.value>1000) ahead = 33;
					return x(d.value)+ahead; 
					})					
                .attr("y", barHeight / 2)
                .attr("dy", ".35em")
                .style("text-anchor", "end")
				.style("fill", "#808080")
                .text(function(d) { return d.value; });
			  }, 1000);			

            return bar;
        }

        // A stateful closure for stacking bars horizontally.
        function stack(i) {
            var x0 = 0;
            return function(d) {
                var tx = "translate(" + x0 + "," + barHeight * i * 1.2 + ")";
                x0 += x(d.value);
                return tx;
            };
        }



        // Resize chart
        // ------------------------------

        // Call function on window resize
        $(window).on('resize', resize);

        // Call function on sidebar width change
        //$('.sidebar-control').on('click', resize);

        // Resize function
        // 
        // Since D3 doesn't support SVG resize by default,
        // we need to manually specify parts of the graph that need to 
        // be updated on window resize
        function resize() {

            // Layout variables
            width = d3Container.node().getBoundingClientRect().width - margin.left - margin.right;


            // Layout
            // -------------------------

            // Main svg width
            container.attr("width", width + margin.left + margin.right);

            // Width of appended group
            svg.attr("width", width + margin.left + margin.right);


            // Axes
            // -------------------------

            // Horizontal range
            x.range([0, width]);

            // Horizontal axis
            svg.selectAll('.d3-axis-horizontal').call(xAxis);


            // Chart elements
            // -------------------------

            // Bars
            svg.selectAll('.enter rect').attr("width", function(d) { return x(d.value); });
        }
    }

//===end Hieracrchical bar chart===





</script>
<section>
	<div class="container">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo menu(); ?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-4">
                        <h1><i class="sprite sprite-reports"></i> Reports</h1>
                    </div>
					<div class="col-sm-4">							
						
					</div>
					
                    <div class="col-sm-4">	
							
					</div>
               </div>            	
            </div>
            <div class="createNewLeadSec reportPageN">

			<form action="" method="get">
            
				<div class="reportPageNTOP">
                	<div class="row">
					  
                        <div class="col-md-4 col-sm-3 col-xs-12">
                           <h4>Filter</h4>
                        </div>
					  
                        <div class="col-md-8 col-sm-9 col-xs-12">
                            <ul>
                                <li>
                                	<div class="dropdown" style="border-right: none;">
                                       <!-- <button type="button" id="showBranches">Select Branches <i class="fa fa-caret-down"></i></button>
										
                                        <ul class="dropdown-menu" id="BranchesList">                                        	
                                            <?php foreach($cities as $city){ 
					                                $branches = getBranches($city->id);
                                                    $i = 1;
						                            if(!empty($branches)){
				                            	?>
                                            <li>
												<span class="report_city"><?php echo $city->title; ?></span>
                                                <ul>
                                                    <?php foreach($branches as $branch){?>
                                                    <li><input id="Branch_<?php echo $branch->id; ?>" type="checkbox" name="branch_id[]" value="<?php echo $branch->id; ?>"><label for="Branch_<?php echo $branch->id; ?>"><span></span><?php echo $branch->title; ?></label></li>
                                                    
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                            <?php } } ?>
                                            
                                        </ul>-->
										
                                    </div>
                                </li>                                
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="row">
				
                	<!--<div class="col-md-3" style="position:relative;">
	                 
							
							<select disabled class="centerList" name="category_id" id="category_id">
							   <option value = ""> Select Category</option>
								<?php
								if(!empty($categories))
								{
									foreach($categories as $category)
									{ ?>
										<option value = "<?php echo $category->id; ?>" <?php if(isset($_GET['category_id'])) {echo ($_GET['category_id'] == $category->id ? 'Selected' : '');}?>><?php echo $category->title; ?></option>
								<?php	
									}
								}
									
								?>																				
							</select>

						</div>

						<div class="col-md-3" style="position:relative;">

							<select class="centerList" name="user_id" id="user_id">
							   <option value = ""> Select User</option>
								<?php
								if(!empty($users))
								{
									foreach($users as $user)
									{ ?>
										<option value = "<?php echo $user->id; ?>" <?php if(isset($_GET['user_id'] )) {echo ($_GET['user_id']  == $user->id ? 'Selected' : '');}?>><?php echo $user->full_name; ?></option>
								<?php
									}
								}									
								?>																		
							</select>
											                                             
					</div>-->
					
                    <div class="col-md-3">
                    	<div class="reportDateRange">                                
                                <div class="row">
                                    <div class="col-md-12">
										<input type="text" style="width:200px" onfocusout="if($(this).val()=='') $('.date_alternate1').val('');" class="datepicker1" value="<?php if(isset($_GET['date_from']) && $_GET['date_from']!='') echo date("d F Y", strtotime($_GET['date_from'])); ?>" placeholder="From"/>
										<input type="hidden" value="<?php if(isset($_GET['date_from']) && $_GET['date_from']!='') echo $_GET['date_from']; ?>" class="date_alternate1" name="date_from"/>
									</div>									
                                </div>
                        </div>
                    </div>

					<div class="col-md-3">
                    	<div class="reportDateRange">                                
                                <div class="row">                                    
                                    <div class="col-md-12">										
										<input type="text" style="width:200px" onfocusout="if($(this).val()=='') $('.date_alternate2').val('');" class="datepicker2" value="<?php if(isset($_GET['date_to']) && $_GET['date_to']!='') echo date("d F Y", strtotime($_GET['date_to'])); ?>" placeholder="To"/>
										<input type="hidden" value="<?php if(isset($_GET['date_to']) && $_GET['date_to']!='') echo $_GET['date_to']; ?>" class="date_alternate2" name="date_to"/>

									</div>
                                </div>
                        </div>
                    </div>
					
					<?php if($this->session->userdata['user']['id']==="132" || $this->session->userdata['user']['id']==="133" || $this->session->userdata['user']['id']==="172" || $this->session->userdata['user']['id']==="135" || $this->session->userdata['user']['id']==="292"){ ?>
					<div class="col-md-6">
					  <div style="float:right; font-weight:bold; color:#337ab7;"><a href="<?php echo base_url();?>manager_report" target="_blank">View Manager Reports</a></div>
					</div>
					<?php } ?>

			  </div>
			  
			 
			  <div class="row">
			  
				<?php if($this->session->userdata['user']['id']==="132" || $this->session->userdata['user']['id']==="133" || $this->session->userdata['user']['id']==="172" || $this->session->userdata['user']['id']==="135" || $this->session->userdata['user']['id']==="292"){ ?>
				
					<div class="col-md-3">
						<select id="dep_id" name="dep_id">
							<option value="0">Select Department</option>
								<?php for($i=0; $i<count($depUsers[0]); $i++)
								{ 
								?>
									<option <?php if(isset($_GET['dep_id']) && $_GET['dep_id']==$depUsers[0][$i]) ECHO 'selected'; ?> value="<?php echo $depUsers[0][$i]; ?>">
										<?php echo $depUsers[1][$i]; ?>
									</option>
								<?php 
								} ?>		
						</select>						
					</div>
					
					<?php } ?>
					
				    <?php if($this->session->userdata['user']['id']==="132" || $this->session->userdata['user']['id']==="133" || $this->session->userdata['user']['id']==="172" || $this->session->userdata['user']['id']==="135" || $this->session->userdata['user']['id']==="292" || $is_manager){ ?>

					<div class="col-md-3">
						<select id="user_id" name="user_id">
						<option value="0">Select User</option>
							<?php 
							$dep_id = 0;
							if(isset($_GET['dep_id'])) $dep_id = $_GET['dep_id'];
							if($dep_id)
							{
								$depUsersResArr = $depUsers[2][$dep_id];
								foreach($depUsersResArr as $depUser)
								{ 
								?>
									<option <?php if(isset($_GET['user_id']) && $_GET['user_id']==$depUser['user_id']) ECHO 'selected'; ?> value="<?php echo $depUser['user_id']; ?>">
										<?php echo $depUser['full_name']; ?>
									</option>
								<?php 
								}
							}else{ ?>		
								<option value="0">First select department</option>
							<?php
							}
							?>
						</select>	
					</div>

					<?php } ?>
					
                </div>
				
			  
			
               <!-- <h3>Reporting</h3>-->


			<?php 
			$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
			$actual_link = 'http://' . $_SERVER['HTTP_HOST'] . $uri_parts[0]; 
			?>
			<input type="submit" class="btn" value="Filter"> <a href="<?php echo $actual_link; ?>">(Remove Filter)</a>
			
			<input type="hidden" name="filter_reports" value="1">
			
			<?php if(isset($_GET["abha"])){ ?>
				<input type="hidden" name="abha" value="1">
			<?php } ?>
			
			<?php if(isset($_GET["madina"])){ ?>
				<input type="hidden" name="madina" value="1">
			<?php } ?>
			
			</form>

<?php if($this->session->userdata['user']['id']==="132" || $this->session->userdata['user']['id']==="133" || $this->session->userdata['user']['id']==="172" || $this->session->userdata['user']['id']==="135" || $this->session->userdata['user']['id']==="292"){ ?>			
<br><br>
<i>Duplicated leads are not included in the following below calculations </i>
<?php } ?>

 <?php 
if(!isset($filter_individual_report) && !isset($filter_dep_report))
{
?>
<h2>Walk-in & Inbound Call report</h2>
<br>
<form action="<?php echo base_url(); ?>report/download_excel" method="post" target="_blank">
	<input type="hidden" name="download_excel_walkin" value="1">
	<input type="submit" class="btn" value="Download Excel">
</form>

<h2>MKT Report</h2>
<br>
<form action="<?php echo base_url(); ?>report/download_excel" method="post" target="_blank">
	<input type="hidden" name="download_excel_mkt_report" value="1">
	<input type="submit" class="btn" value="Download Excel">
</form>

<h2>Leads Count Report</h2>
<br>
<form action="<?php echo base_url(); ?>report/download_excel" method="post" target="_blank">
	<input type="hidden" name="download_excel_leads_count_report" value="1">
	<input type="submit" class="btn" value="Download Excel">
</form>

<h2>Events MKT Report</h2>
<br>
<form action="<?php echo base_url(); ?>report/download_excel" method="post" target="_blank">
	<input type="hidden" name="download_excel_events_count_report" value="1">
	<input type="submit" class="btn" value="Download Excel">
</form>


<h2>List of sales consultants, cars, sources, and completed test drive (First set date range and click filter above.) (Date Range: Lead Created)</h2>
<br>
<form action="<?php echo base_url(); ?>report/download_excel" method="post" target="_blank">
	<input type="hidden" name="download_excel_sc_testdrive_report" value="1">
	<input type="hidden" name="date_from" value="<?php if(isset($_GET['date_from'])) ECHO $_GET['date_from']; ?>">
	<input type="hidden" name="date_to" value="<?php if(isset($_GET['date_to'])) ECHO $_GET['date_to']; ?>">
	<input type="submit" class="btn" value="Download Excel">
</form>

<?php
}
?>


<h2>User Performance</h2>
<?php 
if(!isset($filter_individual_report) && !$is_manager)
{
?>
(Note: After the "Users on vacations" module will be implemented in future, Then we will apply the vacations logic in these reporting, so the vacations of a user will not be included in these calculations.)
<br>FOR NOW YOU CAN USE THE ABOVE DATE RANGE FILTER
<?php } ?>

<?php
$colorArray[0] = '#DC3912';
$colorArray[1] = '#109618';
$colorArray[2] = '#3366CC';
$colorArray[3] = '#990099';
$colorArray[4] = '#FF9900';
$colorArray[5] = '#0099C6';
$colorArray[6] = '#009688';//#F5DEB3';
$colorArray[7] = '#9575CD';//#F0F8FF
$colorArray[8] = '#c3dff7';//#EEE8AA
$colorArray[9] = '#ff7f50';
$colorArray[10] = '#87cefa';
$colorArray[11] = '#5ab1ef';
$colorArray[12] = '#ffb980';
$colorArray[13] = '#d87a80';
$colorArray[14] = '#4da571';
$colorArray[15] = '#8d98b3';
$colorArray[16] = '#e5cf0d';
$colorArray[17] = '#97b552';
$colorArray[18] = '#dc69aa';
$colorArray[19] = '#07a2a4';
$colorArray[20] = '#c05050';

$colorArray2[0] = '#808000';
$colorArray2[1] = '#ADFF2F';
$colorArray2[2] = '#FF6347';
$colorArray2[3] = '#8B4513';
$colorArray2[4] = '#07a2a4';
$colorArray2[5] = '#c05050';
$colorArray2[6] = '#F08080';//#F5DEB3';
$colorArray2[7] = '#B22222';//#F0F8FF
$colorArray2[8] = '#FFD700';//#EEE8AA
$colorArray2[9] = '#483D8B';
$colorArray2[10] = '#8d98b3';
$colorArray2[11] = '#5ab1ef';
$colorArray2[12] = '#ffb980';
$colorArray2[13] = '#d87a80';
$colorArray2[14] = '#4da571';
$colorArray2[15] = '#87cefa';
$colorArray2[16] = '#e5cf0d';
$colorArray2[17] = '#97b552';
$colorArray2[18] = '#dc69aa';
$colorArray2[19] = '#07a2a4';
$colorArray2[20] = '#c05050';

$colorArray3[0] = '#696969';
$colorArray3[1] = '#ffb980';
$colorArray3[2] = '#DA70D6';
$colorArray3[3] = '#708090';
$colorArray3[4] = '#07a2a4';
$colorArray3[5] = '#c05050';
$colorArray3[6] = '#F08080';//#F5DEB3';
$colorArray3[7] = '#B22222';//#F0F8FF
$colorArray3[8] = '#FFD700';//#EEE8AA
$colorArray3[9] = '#483D8B';
$colorArray3[10] = '#8d98b3';
$colorArray3[11] = '#5ab1ef';
$colorArray3[12] = '#ADFF2F';
$colorArray3[13] = '#d87a80';
$colorArray3[14] = '#4da571';
$colorArray3[15] = '#87cefa';
$colorArray3[16] = '#FF6347';
$colorArray3[17] = '#97b552';
$colorArray3[18] = '#dc69aa';
$colorArray3[19] = '#07a2a4';
$colorArray3[20] = '#c05050';

?>

<br><br>

<?php 
if(!isset($filter_individual_report) && !$is_manager)
{
?>

<br><br><u title='Calculated the average of duration between the "assigned" and the "first call attempted to customer"'>1. Time to make the first call (This Avg is after the lead is assigned.) 
<?php
if(
(!isset($_GET['date_from']) || (isset($_GET['date_from']) && $_GET['date_from']==""))
&& (!isset($_GET['date_from']) || (isset($_GET['date_from']) && $_GET['date_from']==""))
)
{
?>
(Based upon: <?php echo date('F Y'); ?>)
<?php
}
?>
</u>
<br><br>User avg to make the first Call [





<?php if($timeToMakeCallActionEachUserAvg) 
foreach($timeToMakeCallActionEachUserAvg as $avg) echo $avg->full_name .' <b>'.secondsToTextDur($avg->avgval).'</b> | '; ?>]


<script type="text/javascript">



    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["Employees", "Time for First call", {role: 'tooltip'}, {role: 'style'} ],
        
		<?php 		
		/*
		#FFF5EE
		#FFEBCD
		#FFD39B
		#FFC1C1
		#FEE5AC
		#F6CCDA
		*/
		$iCount=0;  
		if($timeToMakeCallActionEachUserAvg)
		foreach($timeToMakeCallActionEachUserAvg as $avg) 
		{
			echo '["' . $avg->full_name .'",'. secondsToTextDurInHours($avg->avgval)/24 .',"' . $avg->full_name . '  ' . secondsToTextDur($avg->avgval) . '","' . $colorArray[$iCount%(sizeof($colorArray)-1)] .'"],';
			$iCount++;
		}
		else{
			echo "['n/a',0,'','']";
		}
		?>
/*        ["Copper", 8.94, "#b87333"],
        ["Silver", 10.49, "silver"],
        ["Gold", 19.30, "gold"],
        ["Platinum", 21.45, "color: #e5e4e2"]*/
      ]);
      var view = new google.visualization.DataView(data);
      /*
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);
		*/
      var options = {
        title: "User avg to make the first Call",
        bar: {groupWidth: "80%"},
        legend: 'none',
        hAxis: {
        	textPosition: 'none',
          title: '',
          legend: 'none',
		},
        vAxis: {
          title: 'Time (days)',
 
		 
        },		
		chartArea: {
			left: 50,			
			right: 15
		}
        
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
  </script>
 
<?php 
$columnchart_values_height = "600px";
if(isset($filter_dep_report))
{ 
	$columnchart_values_height = "300px";
}
?>

<div id="columnchart_values" style="width: 100%; height: <?php echo $columnchart_values_height;?>;"></div>

<u title='Calculated the average of duration between the "assigned" and the "finished"'>2. Time to finish the lead (This Avg is after the lead is assigned.)</u>
<br><br>User avg to finish the lead [<?php if($timeToFinishActionEachUserAvg) foreach($timeToFinishActionEachUserAvg as $avg) echo $avg->full_name .' <b>'.secondsToTextDur($avg->avgval).'</b> | '; ?>]


<script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart1);
    function drawChart1() {
      var data = google.visualization.arrayToDataTable([
        ["Element", "Density", {role: 'tooltip'}, {role: 'style'} ],
        
		<?php 

		
		$iCount=0;  
		
		if($timeToFinishActionEachUserAvg) foreach($timeToFinishActionEachUserAvg as $avg)
		{
		
		 echo '["' . $avg->full_name .'",'. secondsToTextDurInHours($avg->avgval)/24 . ',"' . $avg->full_name . '  ' . secondsToTextDur($avg->avgval) . '","' . $colorArray2[$iCount%(sizeof($colorArray)-1)] .'"],';
		 $iCount++;
		}
		else{
			echo "['n/a',0,'','']";
		}
		
		
		?>
/*        ["Copper", 8.94, "#b87333"],
        ["Silver", 10.49, "silver"],
        ["Gold", 19.30, "gold"],
        ["Platinum", 21.45, "color: #e5e4e2"]*/
      ]);
      var view = new google.visualization.DataView(data);
      /*
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);
		*/
      var options = {
        title: "Time to finish the lead after assigned",
        bar: {groupWidth: "80%"},
        legend: 'none',
        hAxis: {
        	textPosition: 'none',
          title: '',
          legend: 'none',
		},
        vAxis: {
          title: 'Time (days)'
//          ticks: [0, 10000, 2000, 4000, 6000]
        },
		chartArea: {
			left: 50,			
			right: 15
		}
        
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values1"));
      chart.draw(view, options);
  }
  </script>
<?php 
$columnchart_values1_height = "600px";
if(isset($filter_dep_report))
{ 
	$columnchart_values1_height = "300px";
}
?>
<div id="columnchart_values1" style="width: 100%; height: <?php echo $columnchart_values1_height;?>;"></div>

<u title='Calculated the average of duration between the "created" and the "closed"'>3. Time to close the lead (Managers/Sub Managers) (This Avg is when the lead is created->closed.)</u>
<br><br>Avg to close the lead [<?php if($timeToCloseActionEachUserAvg) foreach($timeToCloseActionEachUserAvg as $avg) echo $avg->full_name .' <b>'.secondsToTextDur($avg->avgval).'</b> | '; ?>]


<script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart2);
    function drawChart2() {
      var data = google.visualization.arrayToDataTable([
        ["Element", "Density", {role: 'tooltip'}, {role: 'style'} ],
        
		<?php 

		
		$iCount=0;
		
		
		if($timeToCloseActionEachUserAvg) 
		foreach($timeToCloseActionEachUserAvg as $avg)
		{
		 echo '["' . $avg->full_name .'",'. secondsToTextDurInHours($avg->avgval)/24 . ',"' . $avg->full_name . '  ' . secondsToTextDur($avg->avgval) . '","' .  $colorArray3[$iCount%(sizeof($colorArray)-1)] .'"],'; // $avg->pie_color 
		 $iCount++;
		}
		else{
			echo "['n/a',0,'','']";
		}
		
		
		?>
/*        ["Copper", 8.94, "#b87333"],
        ["Silver", 10.49, "silver"],
        ["Gold", 19.30, "gold"],
        ["Platinum", 21.45, "color: #e5e4e2"]*/
      ]);
      var view = new google.visualization.DataView(data);
      /*
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);
		*/
      var options = {
        title: "Time to close the lead (Managers)",
        bar: {groupWidth: "80%"},
        legend: 'none',
        hAxis: {
        	textPosition: 'none',
          title: '',
          legend: 'none',
		},
        vAxis: {
          title: 'Time (days)',
//          ticks: [0, 10000, 2000, 4000, 6000]
        },
		chartArea: {
			left: 50,			
			right: 15
		}
        
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values2"));
      chart.draw(view, options);
  }
  </script>

<?php 
$columnchart_values2_height = "600px";
if(isset($filter_dep_report))
{ 
	$columnchart_values2_height = "300px";
}
?>
<div id="columnchart_values2" style="width: 100%; height: <?php echo $columnchart_values2_height;?>;"></div>


<br><br><u title='Calculated the total count of disapproved. (irrelevant of number of Leads)'>4. Number of disapproved from finished</u>
<br><br>[<?php if($numOfDisapprovedActionEachUser) foreach($numOfDisapprovedActionEachUser as $num) echo $num->full_name .' <b>'.$num->num.'</b> | '; ?>]

<br><br>

<div class="chart-container">
	<div class="chart" id="google-bar-stacked"></div>
</div>

<script type="text/javascript">
// Stacked bars
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawBarStacked);

// Chart settings
function drawBarStacked() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Let it empty', 'Closed by Managers', '# of Times Disapproved', 'Finished(Pending Close)', { role: 'annotation' }  ],

		<?php
			if($usersFromLeads)
			{
				for($i=0; $i<count($usersFromLeads); $i++)
				{
					if($i>0) echo ",";
					echo "['".$usersFromLeads[$i]->full_name."', ".getUserPercCFDLeads($usersFromLeads[$i]->userid,$usersNumOfClosedLeads,$usersNumOfFinishedButNotClosedLeads,$numOfDisapprovedActionEachUser)." ,'']";

				}
			}
			else{
			echo "['',0,0,0,'']";
			}
		?>
        /*['name1', 30, 35, 40, ''],
        ['name2', 20, 25, 30, ''],
        ['name3', 24, 20, 32, ''],
        ['name4', 25, 30, 35, ''],
        ['name5', 22, 23, 30, ''],
        ['name6', 26, 20, 40, ''],
        ['name7', 19, 29, 30, '']*/
    ]);


    // Options
    var options_bar_stacked = {
        fontName: 'Arial',
		<?php 
		if(!isset($filter_dep_report))
		{
		?>
        height: 1200,
		<?php 
		}else{
		?>
		height: 300,
		<?php 
		}
		?>
        fontSize: 12,
        chartArea: {
            left: '20%',
            width: '65%',
            height: 1100
        },
		series: {
          0:{color:'#0099C6'},
          1:{color:'#f96b80'},
          2:{color:'#79cae0'},
          3:{color:'#AAA'},
          4:{color:'#EEE'}
        },
        isStacked: 'percent',
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
        hAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend: {
            position: 'top',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    // Draw chart
    var bar_stacked = new google.visualization.BarChart($('#google-bar-stacked')[0]);
    bar_stacked.draw(data, options_bar_stacked);
}


// Resize chart
// ------------------------------

$(function () {

    // Resize chart on sidebar width change and window resize
    $(window).on('resize', resize);
    //$(".sidebar-control").on('click', resize);

    // Resize function
    function resize() {
        drawBarStacked();
    }
});
</script>



<br><br><u title="(This count includes all successfull calls, including multiple successfull calls for a single lead)">5. Number of successful call</u>
<br><br>[<?php if($numOfSuccessfullCallActionEachUser) foreach($numOfSuccessfullCallActionEachUser as $num) echo $num->full_name .' <b>'.$num->num.'</b> | '; ?>]



<div class="chart-container">
	<div class="chart" id="google-bar-stacked1"></div>
</div>

<script type="text/javascript">
// Stacked bars
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawBarStacked1);

<?php
function getUserPercSNLeads($uId,$numOfSuccessfullCallActionEachUser,$numOfNoAnsActionEachUser)
{
	$successCount = 0;
	$noAnsCount = 0;
	if($numOfSuccessfullCallActionEachUser)
	{
		foreach ($numOfSuccessfullCallActionEachUser as $key => $row) 
		{
		   if ((int)$row->userid === (int)$uId) 
		   {
			   $successCount = $row->num;
		   }
	   }
	}

	if($numOfNoAnsActionEachUser)
	{
		foreach ($numOfNoAnsActionEachUser as $key => $row) 
		{
		   if ((int)$row->userid === (int)$uId) 
		   {
			   $noAnsCount = $row->num;
		   }
	   }
	}		
	
	return $successCount.",".$noAnsCount;
}
?>

// Chart settings
function drawBarStacked1() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Let it empty', 'Successfull Calls', 'No Answer Calls', { role: 'annotation' }  ],

		<?php
			if($usersFromLeads)
			{
				for($i=0; $i<count($usersFromLeads); $i++)
				{
					if($i>0) echo ",";
					echo "['".$usersFromLeads[$i]->full_name."', ".getUserPercSNLeads($usersFromLeads[$i]->userid,$numOfSuccessfullCallActionEachUser,$numOfNoAnsActionEachUser)." ,'']";

				}
			}
			else{
			echo "['',0,0,'']";
			}
		?>
        /*['name1', 30, 35, 40, ''],
        ['name2', 20, 25, 30, ''],
        ['name3', 24, 20, 32, ''],
        ['name4', 25, 30, 35, ''],
        ['name5', 22, 23, 30, ''],
        ['name6', 26, 20, 40, ''],
        ['name7', 19, 29, 30, '']*/
    ]);


    // Options
    var options_bar_stacked = {
        fontName: 'Arial',
        <?php 
		if(!isset($filter_dep_report))
		{
		?>
        height: 1200,
		<?php 
		}else{
		?>
		height: 300,
		<?php 
		}
		?>
        fontSize: 12,
        chartArea: {
            left: '20%',
            width: '65%',
            height: 1100
        },
		series: {
          0:{color:'#0099C6'},
          1:{color:'#f96b80'},
          2:{color:'#79cae0'},
          3:{color:'#AAA'},
          4:{color:'#EEE'}
        },
        isStacked: 'percent',
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
        hAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend: {
            position: 'top',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    // Draw chart
    var bar_stacked = new google.visualization.BarChart($('#google-bar-stacked1')[0]);
    bar_stacked.draw(data, options_bar_stacked);
}


// Resize chart
// ------------------------------

$(function () {

    // Resize chart on sidebar width change and window resize
    $(window).on('resize', resize);
    //$(".sidebar-control").on('click', resize);

    // Resize function
    function resize() {
        drawBarStacked1();
    }
});
</script>



<br><br><u title="(This count includes all no answer calls, including multiple no ans calls for a single lead)">6. Number of no answers call</u>
<br><br>[<?php if($numOfNoAnsActionEachUser) foreach($numOfNoAnsActionEachUser as $num) echo $num->full_name .' <b>'.$num->num.'</b> | '; ?>]

<br><br><u title='Current logic of this report is: Total count and the average time of calls.'>7. Time between no answer and successful calls </u>
<br>
<br>
<br>



<script>
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawVisualization2);


      function drawVisualization2() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
         ['SuccessCalls', 'Count', 'Count', { role: 'annotation' }],
         /*['2004/05',  162,  165 ],
         ['2005/06',  132,  135],
         ['2006/07',  154,   157],
         ['2007/08',  136,    139],
         ['2008/09',  133,    136]*/
		 <?php
		 if($hourTimeOfSuccessfullCallAction)		
		 foreach($hourTimeOfSuccessfullCallAction as $num) 
		 {
			 echo '["' . $num->timehour .'",'. $num->num  . ','. $num->num .',"'.$num->num.'"],';
		 }
		 else{
			echo "['',0,0,'']";
			}
		 ?>
      ]);

    var options = {
      title : 'Successfull Calls Timeline',
      vAxis: {title: 'Calls'},
      hAxis: {title: 'Time'},
      seriesType: 'bars',
      series: {1: {type: 'line'},},
      bar: {
		groupWidth: 20
	  },
	  legend: {
        position: 'none' 
      }	  
    };

    var chart = new google.visualization.ComboChart(document.getElementById('successfull_calls_timeline2'));
    chart.draw(data, options);
  }

</script>

<div class="chart-container">
	<div class="chart" id="successfull_calls_timeline2"></div>
</div>



<br><br>User avg "Num of no ans"/"Avg time": [<?php if($avgTimeOfNoAnsActionEachUser) foreach($avgTimeOfNoAnsActionEachUser as $num) echo $num->full_name .' <b>'.$num->num.'/'.$num->avgval.'</b> | '; ?>]
<br><br>User avg "Num of successfull calls"/"Avg time": [<?php if($avgTimeOfSuccessfullCallActionEachUser) foreach($avgTimeOfSuccessfullCallActionEachUser as $num) echo $num->full_name .' <b>'.$num->num.'/'.$num->avgval.'</b> | '; ?>]

<br><br><u title="User and user's department average score" style="">8. User average score vs same branch.</u>

<br><br>Total avg score (all branchs): [<?php if($totalAvgScore) echo round($totalAvgScore->avgval,2); ?>]
<br>User/Dep avg score: [<?php if($userAvgScore1) foreach($userAvgScore1 as $avg) { if(isset($userDepsAvgScore[$avg->userid])) { echo $avg->full_name .' <b>'.round($avg->avgval,2).'/'.round($userDepsAvgScore[$avg->userid],2).'</b> | ';} } ?>]

<br><br>
<div class="chart-container">
	<div style="height:300px" id="jsl_scores"></div>
</div>

<script>
// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawLineChart21);


// Chart settings
function drawLineChart21() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Names', 'User Score', { role: 'annotation' }, 'Branch Score', { role: 'annotation' }],
       /* ['name1',  100, 100 , 200 , 200],
        ['name2',  100, 100 , 200, 200],
        ['name3',  100, 100 , 200 , 200],
        ['name4',  100, 100 , 200 , 200],*/
		
		<?php 
		$is_found = false;
		if($userAvgScore1) 
		foreach($userAvgScore1 as $avg) 
		{
			if(isset($userDepsAvgScore[$avg->userid])) { 				
				echo "['".$avg->full_name."',  ".round($avg->avgval,2).", ".round($avg->avgval,2)." , ".round($userDepsAvgScore[$avg->userid],2)." , ".round($userDepsAvgScore[$avg->userid],2)."],";
				$is_found = true;
			} 
		} 
		
		if(!$is_found)	echo "['',0,'',0,'']";
		
		?>
		
    ]);

    // Options
    var options = {
        fontName: 'Arial',
        height: 250,
		title: 'User vs Department Score',
        curveType: 'function',
        fontSize: 12,
        chartArea: {
            left: '5%',
            width: '100%',
            height: 160
        },
        pointSize: 4,
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },	
		vAxis: {
			viewWindowMode: "explicit", 
			viewWindow:{ min: 0 }
		},	
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    // Draw chart
    var line_chart111 = new google.visualization.LineChart($('#jsl_scores')[0]);
    line_chart111.draw(data, options);
}
</script>

<!--<div class="panel-body">
	<div class="chart-container">
		<div class="chart has-fixed-height has-minimum-width" style="height:<?php echo $nested_pie;?>;" id="nested_pie"></div>
	</div>
</div>-->

<?php
$depScore = "";
$depUsersScore = "";
$depScoreIterator = 0;
$depUSIterator = 0; //let it be out here
if($depUsersSurveyScores)
{
	foreach($depUsersSurveyScores as $depId=>$depResArr)
	{
		$depName = $depUsersSurveyScores[$depId]['depName'];
		$totalDepAvg = $depUsersSurveyScores[$depId]['totalDepAvg'];

		if($depScoreIterator>0) $depScore .= ",";
		$depScore .= "{value: ".$totalDepAvg.", name: '".str_replace("Showroom","",$depName)."'}";

		unset($depResArr['depName']);
		unset($depResArr['totalDepAvg']);
		
		foreach($depResArr as $userScore)
		{
			$full_name = $userScore['full_name'];
			$avgval = round($userScore['avgval'],2);

			if($depUSIterator>0) $depUsersScore .= ",";
			$depUsersScore .= "{value: ".$avgval.", name: '".$full_name."'}";

			$depUSIterator++;
		}

		$depScoreIterator++;
	}
}

?>

<br><br><u title="Every answer has a number value for example No=0, Ok=3 ... etc So total average of the received survey results /user for each month">9. Score on yearly timeline</u>
<br>
<?php 

if($userAvgScore1) {foreach($userAvgScore1 as $avg) { if(isset($totalAvgScoreYearly[$avg->userid])) { echo '<br>'.$avg->full_name; 


/*foreach($totalAvgScoreYearly[$avg->userid] as $key=>$val)
{
	echo '&nbsp; | &nbsp;'.$key.' <b>['.round($val,2).'</b>]'; 
} */

if(isset($_GET['date_from']) && $_GET['date_from']!="")
	$start = $month = strtotime($_GET['date_from']);
else
	$start = $month = strtotime('2016-7-01'); //July 2016

if(isset($_GET['date_to']) && $_GET['date_to']!="")
	$end = strtotime($_GET['date_to']);
else
	$end = strtotime(date("Y-m-d"));


while($month <= $end)
{
     $key = date('M Y', $month);

	 if(isset($totalAvgScoreYearly[$avg->userid][$key])) 
	 {
		 $val = $totalAvgScoreYearly[$avg->userid][$key];
		 echo '&nbsp; | &nbsp;'.$key.' <b>['.round($val,2).'</b>]'; 
	 }
	 else 
	 {
		 // for missing months, display user's overall avg. 
		$val = $userAvgScoreAsUidKey[$avg->userid]; //user total avg
		echo '&nbsp; | &nbsp;'.$key.' <b>['.round($val,2).'</b>]'; 
	 }


     $month = strtotime("+1 month", $month);
}




} } } ?>


<div class="panel-body">
	<div class="chart-container">
		<div class="chart" id="c3-line-chart"></div>
	</div>
</div>

<script type="text/javascript">
$(function () {
    

    // Line chart
    // ------------------------------

    // Generate chart
    var line_chart = c3.generate({
        bindto: '#c3-line-chart',
        point: { 
            r: 4   
        },
        size: { 
		<?php 
		if(!isset($filter_dep_report))
		{
		?>
		height: 600
		<?php 
		}else{
		?>
		height: 300
		<?php 
		}
		?>	
			
		},
		padding: {
        //top: 40,
        right: 20,
        bottom: 10,
        //left: 100,
		},
        color: {
            pattern: ['#4CAF50', '#F4511E', '#1E88E5']
        },
		axis : {
        x : {
				type : 'timeseries',
				tick: {
					format: '%b %Y'
				}
			}
		},
        data: {
			x: 'months',
            columns: [
                //['data1', 30, 200, 100, 400, 150, 250],
                //['data2', 50, 20, 10, 40, 15, 25]
				['months',
				//['months','2016-07-01','2016-08-01','2016-09-01','2016-10-01','2016-11-01'],
					<?php
					if(isset($_GET['date_from']) && $_GET['date_from']!="")
						$start = $month = strtotime($_GET['date_from']);
					else
						$start = $month = strtotime('2016-7-01'); //July 2016

					if(isset($_GET['date_to']) && $_GET['date_to']!="")
						$end = strtotime($_GET['date_to']);
					else
						$end = strtotime(date("Y-m-d"));


					while($month <= $end)
					{
						 echo "'".date('Y-m-d', $month)."',";
						 $month = strtotime("+1 month", $month);
					}
					?>
				],

				<?php 
					if($userAvgScore1) 
					{
						$lcc = 0;
						foreach($userAvgScore1 as $avg) 
						{ 
							if(isset($totalAvgScoreYearly[$avg->userid])) 
							{ 
								if($lcc>0) echo ",";
								echo "['".$avg->full_name."'";

								/*foreach($totalAvgScoreYearly[$avg->userid] as $key=>$val)
								{
									//echo '&nbsp; | &nbsp;'.$key.' <b>['.round($val,2).'</b>]'; 
									echo ",".round($val,2);

								}*/




								if(isset($_GET['date_from']) && $_GET['date_from']!="")
									$start = $month = strtotime($_GET['date_from']);
								else
									$start = $month = strtotime('2016-7-01'); //July 2016

								if(isset($_GET['date_to']) && $_GET['date_to']!="")
									$end = strtotime($_GET['date_to']);
								else
									$end = strtotime(date("Y-m-d"));


								while($month <= $end)
								{
									 $key = date('M Y', $month);

									 if(isset($totalAvgScoreYearly[$avg->userid][$key])) 
									 {
										 $val = $totalAvgScoreYearly[$avg->userid][$key];
										 //echo '&nbsp; | &nbsp;'.$key.' <b>['.round($val,2).'</b>]'; 
										 echo ",".round($val,2);
									 }
									 else 
									 {
										 // for missing months, display user's overall avg. 
										$val = $userAvgScoreAsUidKey[$avg->userid]; //user total avg
										//echo '&nbsp; | &nbsp;'.$key.' <b>['.round($val,2).'</b>]'; 
										echo ",".round($val,2);
									 }


									 $month = strtotime("+1 month", $month);
								}





								echo "]";
								$lcc++;
							} 							
						} 
					} 
				?>

            ],
            type: 'spline'
        },
        grid: {
            y: {
                show: true
            }
        }
    });



    
});
</script>


<br><br><u style="color:blue;">10. Score vs Number of leads monthly vs % difference </u>
Calculate Sum of the total number of answered survey divided by sum of all score in a month

<br><br><u title="Leads that were closed by the manager but there was never attempted any phone call">11. Leads closed with no calls</u>
<br><br>[<?php if($numOfClosedWithNoCallsActionEachUser) foreach($numOfClosedWithNoCallsActionEachUser as $num) echo $num->full_name .' <b>'.$num->num.'</b> | '; ?>]

<br><br>
<div class="chart-container">
	<div class="chart" id="google-bar"></div>
</div>
<script type="text/javascript">
// Bar chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawBar);


// Chart settings
function drawBar() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Lead types', 'No Calls', 'No Answers'],
        //['2004',  1000,      400],
        //['2005',  1170,      460],
        //['2006',  660,       1120],
        //['2007',  1030,      540]

		<?php 
			$is_found = false;
			$noAnsCallsArr = array();
			if($numOfClosedWithNoAnsweredEachUser) foreach($numOfClosedWithNoAnsweredEachUser as $num) $noAnsCallsArr[$num->userid] = $num->num;

			if($numOfClosedWithNoCallsActionEachUser) 
			{
				$counterNa = 0;
				foreach($numOfClosedWithNoCallsActionEachUser as $num) 
				{
					if($counterNa>0) echo ",";
					$noAnsCount = 0;
					if(isset($noAnsCallsArr[$num->userid])) $noAnsCount = $noAnsCallsArr[$num->userid];
					echo "['".$num->full_name."', ".$num->num.", ".$noAnsCount."]";
					$counterNa++;
					$is_found = true;
				}
			}
			
			if(!$is_found) echo "['',0,0]";			
		
		?>

    ]);


    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Leads Closed with No Call vs No Answer',
		<?php 
		if(!isset($filter_dep_report))
		{
		?>
        height: 1000,
		<?php 
		}else{
		?>
		height: 300,
		<?php 
		}
		?>
        
        fontSize: 12,
        chartArea: {
            left: '15%',
            width: '80%',
            height: 900
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend: {
            position: 'top',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    // Draw chart
    var bar = new google.visualization.BarChart($('#google-bar')[0]);
    bar.draw(data, options_bar);

}
</script>

<br><br><u title="Leads that were closed in which call was attempted but client never responded. Excluding leads in which client received the call later.">12. Leads closed with no answered calls </u>
<br><br>[<?php if($numOfClosedWithNoAnsweredEachUser) foreach($numOfClosedWithNoAnsweredEachUser as $num) echo $num->full_name .' <b>'.$num->num.'</b> | '; ?>]

<br><br><u title="Leads that were closed but there was never attempted any phone call or email">13. Leads closed with no calls and no emails</u>
<br><br>[<?php if($numOfClosedWithNoCallsEmailsActionEachUser) foreach($numOfClosedWithNoCallsEmailsActionEachUser as $num) echo $num->full_name .' <b>'.$num->num.'</b> | '; ?>]

<br><br><u title="Count of assigned leads">14. Leads per month yearly view for user</u>
<br>
<?php if($userTLPM1) {foreach($userTLPM1 as $total) { if(isset($totalLeadsYearly[$total->userid])) { echo '<br>'.$total->full_name; 

/*foreach($totalLeadsYearly[$total->userid] as $key=>$val)
{
	echo '&nbsp; | &nbsp;'.$key.' <b>['.$val.'</b>]'; 
}*/

if(isset($_GET['date_from']) && $_GET['date_from']!="")
	$start = $month = strtotime($_GET['date_from']);
else
	$start = $month = strtotime('2016-7-01'); //July 2016

if(isset($_GET['date_to']) && $_GET['date_to']!="")
	$end = strtotime($_GET['date_to']);
else
	$end = strtotime(date("Y-m-d"));


while($month <= $end)
{
     $key = date('M Y', $month);

	 if(isset($totalLeadsYearly[$total->userid][$key])) 
	 {
		 $val = $totalLeadsYearly[$total->userid][$key];
		 echo '&nbsp; | &nbsp;'.$key.' <b>['.$val.'</b>]'; 
	 }
	 else 
	 {
		// for missing months, display 0. 
		$val = 0;
		echo '&nbsp; | &nbsp;'.$key.' <b>['.$val.'</b>]';
	 }


     $month = strtotime("+1 month", $month);
}





	
} } } ?>


<div class="panel-body">
	<div class="chart-container">
		<div class="chart" id="c3-line-chart2"></div>
	</div>
</div>

<script type="text/javascript">
$(function () {
    

    // Line chart
    // ------------------------------

    // Generate chart
    var line_chart2 = c3.generate({
        bindto: '#c3-line-chart2',
        point: { 
            r: 4   
        },
        size: { 
				<?php 
				if(!isset($filter_dep_report))
				{
				?>
				height: 800
				<?php 
				}else{
				?>
				height: 400
				<?php 
				}
				?>
 
		},
		padding: {
        //top: 40,
        right: 20,
        bottom: 10,
        //left: 100,
		},
		axis : {
        x : {
				type : 'timeseries',
				tick: {
					format: '%b %Y'
				}
			}
		},
        color: {
            pattern: ['#4CAF50', '#F4511E', '#1E88E5']
        },
        data: {
			x: 'months',
            columns: [
                //['data1', 30, 200, 100, 400, 150, 250],
                //['data2', 50, 20, 10, 40, 15, 25]
				['months',
				//['months','2016-07-01','2016-08-01','2016-09-01','2016-10-01','2016-11-01'],
					<?php
					if(isset($_GET['date_from']) && $_GET['date_from']!="")
						$start = $month = strtotime($_GET['date_from']);
					else
						$start = $month = strtotime('2016-7-01'); //July 2016

					if(isset($_GET['date_to']) && $_GET['date_to']!="")
						$end = strtotime($_GET['date_to']);
					else
						$end = strtotime(date("Y-m-d"));


					while($month <= $end)
					{
						 echo "'".date('Y-m-d', $month)."',";
						 $month = strtotime("+1 month", $month);
					}
					?>
				],
				<?php 
					if($userTLPM1) 
					{
						$lcc = 0;
						foreach($userTLPM1 as $total) 
						{ 
							if(isset($totalLeadsYearly[$total->userid])) 
							{ 
								if($lcc>0) echo ",";
								echo "['".$total->full_name."'";
								
								/*foreach($totalLeadsYearly[$total->userid] as $key=>$val)
								{
									//echo '&nbsp; | &nbsp;'.$key.' <b>['.round($val,2).'</b>]'; 
									echo ",".$val;

								}*/


								if(isset($_GET['date_from']) && $_GET['date_from']!="")
									$start = $month = strtotime($_GET['date_from']);
								else
									$start = $month = strtotime('2016-7-01'); //July 2016

								if(isset($_GET['date_to']) && $_GET['date_to']!="")
									$end = strtotime($_GET['date_to']);
								else
									$end = strtotime(date("Y-m-d"));


								while($month <= $end)
								{
									 $key = date('M Y', $month);

									 if(isset($totalLeadsYearly[$total->userid][$key])) 
									 {
										 $val = $totalLeadsYearly[$total->userid][$key];
										 echo ",".$val; 
									 }
									 else 
									 {
										// for missing months, display 0. 
										$val = 0;
										echo ",".$val;
									 }


									 $month = strtotime("+1 month", $month);
								}




								echo "]";
								$lcc++;
							} 							
						} 
					} 
				?>

            ],
            type: 'spline'
        },
        grid: {
            y: {
                show: true
            }
        }
    });



    
});
</script>

<?php 
if(!isset($filter_dep_report))
{
?>

<br><br><u title="Count of leads per branch">15. Leads per month yearly view per branch (Other than org structure. Fetched by lead's selected branch)</u>
<br>
<?php if($branchTLPM1) {foreach($branchTLPM1 as $total) { if(isset($totalBranchLeadsYearly[$total->branchid])) { echo '<br>'.$total->title; 

/*foreach($totalBranchLeadsYearly[$total->branchid] as $key=>$val)
{
	echo '&nbsp; | &nbsp;'.$key.' <b>['.$val.'</b>]'; 
} */

if(isset($_GET['date_from']) && $_GET['date_from']!="")
	$start = $month = strtotime($_GET['date_from']);
else
	$start = $month = strtotime('2016-7-01'); //July 2016

if(isset($_GET['date_to']) && $_GET['date_to']!="")
	$end = strtotime($_GET['date_to']);
else
	$end = strtotime(date("Y-m-d"));


while($month <= $end)
{
     $key = date('M Y', $month);

	 if(isset($totalBranchLeadsYearly[$total->branchid][$key])) 
	 {
		 $val = $totalBranchLeadsYearly[$total->branchid][$key];
		 echo '&nbsp; | &nbsp;'.$key.' <b>['.$val.'</b>]';  
	 }
	 else 
	 {
		// for missing months, display 0. 
		$val = 0;
		echo '&nbsp; | &nbsp;'.$key.' <b>['.$val.'</b>]'; 
	 }


     $month = strtotime("+1 month", $month);
}




	
} } } ?>



<div class="panel-body">
	<div class="chart-container">
		<div class="chart" id="graph7"></div>
	</div>
</div>

<script>
google.setOnLoadCallback(graph7);
function graph7() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Year', 'Abha Showroom', { role: 'annotation' }, {role: 'tooltip', p:{html:true}}, 'Automall Showroom', { role: 'annotation' }, {role: 'tooltip', p:{html:true}}, 'Dammam Showroom', { role: 'annotation' }, {role: 'tooltip', p:{html:true}}, 'Khurais Showroom', { role: 'annotation' }, {role: 'tooltip', p:{html:true}}, 'Madina Road Showroom', { role: 'annotation' }, {role: 'tooltip', p:{html:true}}, 'Uroubah Showroom', { role: 'annotation' }, {role: 'tooltip', p:{html:true}}],
		
		/*['Jul 2016',9,9,'#3367d6',0,0,'#FF7F50'],
		['Aug 2016',9,9,'#3367d6',0,0,'#FF7F50'],
		['Sep 2016',0,0,'#3367d6',0,0,'#FF7F50'],
		['Oct 2016',61,61,'#3367d6',2,2,'#FF7F50'],
		['Nov 2016',5,5,'#3367d6',0,0,'#FF7F50'],
		['Dec 2016',21,21,'#3367d6',0,0,'#FF7F50']*/

	<?php 
		if($orgStrucDepLeadsYearly)  
		{	
			foreach($orgStrucDepLeadsYearly as $orgSDLYr)
			{
				echo "
				['".$orgSDLYr->month."', 
				".$orgSDLYr->{"Abha Showroom"}.",".$orgSDLYr->{"Abha Showroom"}.",
				'Abha:".$orgSDLYr->{"Abha Showroom"}."<br/> Automall:".$orgSDLYr->{"Automall Showroom"}."<br/> Dammam:".$orgSDLYr->{"Dammam Showroom"}."<br/> Khurais:".$orgSDLYr->{"Khurais Showroom"}."<br/> Madina:".$orgSDLYr->{"Madina Road Showroom"}."<br/> Uroubah:".$orgSDLYr->{"Uroubah Showroom"}."',
				
				".$orgSDLYr->{"Automall Showroom"}.",".$orgSDLYr->{"Automall Showroom"}.",
				'Abha:".$orgSDLYr->{"Abha Showroom"}."<br/> Automall:".$orgSDLYr->{"Automall Showroom"}."<br/> Dammam:".$orgSDLYr->{"Dammam Showroom"}."<br/> Khurais:".$orgSDLYr->{"Khurais Showroom"}."<br/> Madina:".$orgSDLYr->{"Madina Road Showroom"}."<br/> Uroubah:".$orgSDLYr->{"Uroubah Showroom"}."',
				
				".$orgSDLYr->{"Dammam Showroom"}.",".$orgSDLYr->{"Dammam Showroom"}.",
				'Abha:".$orgSDLYr->{"Abha Showroom"}."<br/> Automall:".$orgSDLYr->{"Automall Showroom"}."<br/> Dammam:".$orgSDLYr->{"Dammam Showroom"}."<br/> Khurais:".$orgSDLYr->{"Khurais Showroom"}."<br/> Madina:".$orgSDLYr->{"Madina Road Showroom"}."<br/> Uroubah:".$orgSDLYr->{"Uroubah Showroom"}."',
				
				".$orgSDLYr->{"Khurais Showroom"}.",".$orgSDLYr->{"Khurais Showroom"}.",
				'Abha:".$orgSDLYr->{"Abha Showroom"}."<br/> Automall:".$orgSDLYr->{"Automall Showroom"}."<br/> Dammam:".$orgSDLYr->{"Dammam Showroom"}."<br/> Khurais:".$orgSDLYr->{"Khurais Showroom"}."<br/> Madina:".$orgSDLYr->{"Madina Road Showroom"}."<br/> Uroubah:".$orgSDLYr->{"Uroubah Showroom"}."',
				
				".$orgSDLYr->{"Madina Road Showroom"}.",".$orgSDLYr->{"Madina Road Showroom"}.",
				'Abha:".$orgSDLYr->{"Abha Showroom"}."<br/> Automall:".$orgSDLYr->{"Automall Showroom"}."<br/> Dammam:".$orgSDLYr->{"Dammam Showroom"}."<br/> Khurais:".$orgSDLYr->{"Khurais Showroom"}."<br/> Madina:".$orgSDLYr->{"Madina Road Showroom"}."<br/> Uroubah:".$orgSDLYr->{"Uroubah Showroom"}."',
				
				".$orgSDLYr->{"Uroubah Showroom"}.",".$orgSDLYr->{"Uroubah Showroom"}.",
				'Abha:".$orgSDLYr->{"Abha Showroom"}."<br/> Automall:".$orgSDLYr->{"Automall Showroom"}."<br/> Dammam:".$orgSDLYr->{"Dammam Showroom"}."<br/> Khurais:".$orgSDLYr->{"Khurais Showroom"}."<br/> Madina:".$orgSDLYr->{"Madina Road Showroom"}."<br/> Uroubah:".$orgSDLYr->{"Uroubah Showroom"}."'
				],";
			}
			  
		}else
		{			
			echo "['0', 0, '0', 0, '0', 0, '0', 0, '0', 0, '0']";
		}
	?>		
		
    ]);

    // Options
    var options = {
        fontName: 'Arial',
        height: 450,
		title: 'Leads per month yearly view per branch',
        curveType: 'function',
        fontSize: 12,		
        chartArea: {
            left: '5%',
			right: '2%',
            width: '100%',
            height: 360
        },
        pointSize: 4,
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 14
            },
			isHtml: true
        },	
		vAxis: {
			viewWindowMode: "explicit", 
			viewWindow:{ min: 0 }
		},	
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    // Draw chart
    var line_chart11 = new google.visualization.LineChart($('#graph7')[0]);
    line_chart11.draw(data, options);
}
</script>

<?php 
}
?>

<br><br><u title="Same like above 14th number but without /month">16. Total number of leads per user </u>
<br><br>[<?php if($userTLPM) foreach($userTLPM as $num) echo $num->full_name .' <b>'.$num->num.'</b> | '; ?>]



<div class="chart-container">
	<div class="chart" id="google-bar1"></div>
</div>
<script type="text/javascript">
// Bar chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawBar1);


// Chart settings
function drawBar1() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Leads', 'Number of leads per user', {role: 'tooltip'}, {role: 'style'} ],
        //['2004',  1000,      400],
        //['2005',  1170,      460],
        //['2006',  660,       1120],
        //['2007',  1030,      540]

		<?php 

		
		$iCount=0;  
		foreach($userTLPM as $employee) 
		{
			echo '["' . $employee->full_name .'",'. $employee->num  . ',"'. $employee->full_name .' '. $employee->num  . '","' . $colorArray[$iCount%(sizeof($colorArray)-1)] .'"],';
			$iCount++;
		}
		?>

    ]);


    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Number of leads per user',
        <?php 
		if(!isset($filter_dep_report))
		{
		?>
		height: 1000,
		<?php 
		}else{
		?>
		height: 300,
		<?php 
		}
		?>
        fontSize: 12,
        chartArea: {
            left: '15%',
            width: '80%',
            height: 900
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend: {
            position: 'none'            
        }
    };

    // Draw chart
    var bar = new google.visualization.BarChart($('#google-bar1')[0]);
    bar.draw(data, options_bar);

}
</script>

<?php 
if(!isset($filter_dep_report))
{
?>

<br><br><u title="Same like above 15th number but without /month">17. Total number of leads per branch (Other than org structure. Fetched by lead's selected branch)</u>
<br><br>[<?php if($branchTLPM) foreach($branchTLPM as $num) echo $num->title .' <b>'.$num->num.'</b> | '; ?>]



<script type="text/javascript">
        
		
google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart3);
      function drawChart3() {

        var data = google.visualization.arrayToDataTable([
          ['Leads', 'Branches', ], //{role: 'tooltip'}, {role: 'style'}],

		<?php 

		
		$iCount=0;  
		foreach($orgStrucDepLeads as $branch) 
		{
			if($iCount>0) echo ",";
			echo '["' . $branch->title .'",'. $branch->num .']';//',"' . $branch->title . '  ' . $branch->num . '","' . $colorArray[$iCount%(sizeof($colorArray)-1)] .'"],';
			$iCount++;
		}
		?>
        ]);

        var options = {
          title: 'Leads per branch',
          legend: {fontSize : 12},
		  chartArea: {
			'height': '70%'
		  }
        };

        var chart = new google.visualization.PieChart(document.getElementById('columnchart_values_LeadsPerBranches'));

        chart.draw(data, options);
      }
  </script>
<div id="columnchart_values_LeadsPerBranches" style="text-align:center; width: 100%; height: 600px;"></div>

<?php 
}
?>


<u>18. Number of closed leads per user / per branch</u>
<br><br>
Finished Leads /User (Following are the users who clicked on the finished button and now either the lead is in finished or closed or archived status.)<br>
[<?php if($userFinishedTLPM) foreach($userFinishedTLPM as $num) echo $num->full_name .' <b>'.$num->num.'</b> | '; ?>]

<br><br>
Closed and Approved Leads /User (Following are the managers who clicked on the closed button and now either the lead is in closed or archived status.)<br>[<?php if($userClosedTLPM) foreach($userClosedTLPM as $num) echo $num->full_name .' <b>'.$num->num.'</b> | '; ?>]

<?php 
if(!isset($filter_dep_report))
{
?>
<br><br>
Finished Leads /Branch (Following are the branches with finished leads and now either the lead is in finished or closed or archived status.)<br>
[<?php if($orgStrucBranchFinishedTLPM) foreach($orgStrucBranchFinishedTLPM as $num) echo $num->title .' <b>'.$num->num.'</b> | '; ?>]
<br>
(Other than org structure. Fetched by lead's selected branch)
<br>
[<?php if($branchFinishedTLPM) foreach($branchFinishedTLPM as $num) echo $num->title .' <b>'.$num->num.'</b> | '; ?>]
<br>Without Any Branch Finished Leads(e.g Contact us forms): [<?php if($noBranchFinishedTLPM) echo '<b>'.$noBranchFinishedTLPM->num.'</b> | '; ?>]
<br><br>
Closed and Approved Leads /Branch (Following are the branches with closed leads and now either the lead is in closed or archived status.)<br>
[<?php if($orgStrucBranchClosedTLPM) foreach($orgStrucBranchClosedTLPM as $num) echo $num->title .' <b>'.$num->num.'</b> | '; ?>]
<br>
(Other than org structure. Fetched by lead's selected branch)
<br>
[<?php if($branchClosedTLPM) foreach($branchClosedTLPM as $num) echo $num->title .' <b>'.$num->num.'</b> | '; ?>]
<br>Without Any Branch Closed Leads(e.g Contact us forms): [<?php if($noBranchClosedTLPM) echo '<b>'.$noBranchClosedTLPM->num.'</b> | '; ?>]

<br><br>19. Number of finished & approved leads per user / per branch
<br>This report is covered in above 18th report.

<br><br>
20. Time to view a lead
<br>For this report we need to update the logic of when employee click the lead to expand its detail.
<?php 
}
?>
<br><br>

<?php 
if(!isset($filter_dep_report))
{
?>

<h2>Source & Categories</h2>

<br><u title="That are not assigned any category yet">1 Number of leads per website source (Leads to which category not assigned)</u>
<br>[<?php if($numberOfLeadsPerWebsiteSource) foreach($numberOfLeadsPerWebsiteSource as $num) echo $num->title .' <b>'.$num->num.'</b> | '; ?>]


<br><br><u>2. Number of leads per categories</u>
<br>[<?php if($numberOfLeadsPerCategories) foreach($numberOfLeadsPerCategories as $num) echo $num->title .' <b>'.$num->num.'</b> | '; ?>]


<script type="text/javascript">
        
		
google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart4);
      function drawChart4() {

        var data = google.visualization.arrayToDataTable([
          ['Leads', 'Branches', ], //{role: 'tooltip'}, {role: 'style'}],

<?php 

		
		$iCount=0;  
		foreach($numberOfLeadsPerCategories as $num) 
		{
			echo '["' . $num->title .'",'. $num->num .'],';//',"' . $branch->title . '  ' . $branch->num . '","' . $colorArray[$iCount%(sizeof($colorArray)-1)] .'"],';
			$iCount++;
		}
		?>
        ]);

        var options = {
          title: 'Number of leads per category',
          legend: {fontSize : 12},
		  chartArea: {
			'height': '70%'
		  }
        };

        var chart = new google.visualization.PieChart(document.getElementById('columnchart_values_LeadsPerCategory'));

        chart.draw(data, options);
      }
  </script>
<div id="columnchart_values_LeadsPerCategory" style="text-align:center; width: 100%; height: 600px;"></div>

<u>3. Number of leads by model per source / category (Currently following is Vehicle Model Scheduled Count) It needs clarification to improve this following report. </u>
<?php 
if($allCarsPerSource)
{	

	$allCarsKSAArr = array_count_values($allCarsPerSource[0]);
	//$allCarsKSAArrComp = array_count_values($allCarsPerSource[1]);
	$cars = "";
	foreach($allCarsKSAArr as $carId=>$count)
	{
		$compCount = 0;
		//if(isset($allCarsKSAArrComp[$carId])) $compCount = $allCarsKSAArrComp[$carId];
		$cars .= getSpecificCarName($carId)." <b>(Scheduled Count ".$count.")</b><br>";
		//if($compCount) $cars .= " = ".round(($compCount/$count*100),2)."%"."<br>";
		//else $cars .= " = 0%"."<br>";

	}
	echo '<br>'.$cars."<br>";

}
?>

<br>4. Conversion rates (per source / category) (See following 4: a.b.c.d)


<script type="text/javascript">		
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart5);
	function drawChart5() {
		var data = google.visualization.arrayToDataTable([
		  ['Leads', 'Numbers'], //{role: 'tooltip'}, {role: 'style'}],

			<?php 		
			$iCount=0;  
			if($numberOfCompletedTestDrivesPerCategories)
			foreach($numberOfCompletedTestDrivesPerCategories as $num) 
			{
				echo '["' . $num->title .'",'. $num->num .'],';//',"' . $branch->title . '  ' . $branch->num . '","' . $colorArray[$iCount%(sizeof($colorArray)-1)] .'"],';
				$iCount++;
			}
			?>

		]);

		var options = {
		  title: 'Completed / category and source (Date Range: Lead Created)',
		  legend: {fontSize : 12},
		  chartArea: {
			'height': '70%'
		  }
		};

		var chart = new google.visualization.PieChart(document.getElementById('numberOfCompletedTestDrivesPerCategories'));
		chart.draw(data, options);
    }

	//===
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart6);
	function drawChart6() {
		var data = google.visualization.arrayToDataTable([
		  ['Leads', 'Numbers'], //{role: 'tooltip'}, {role: 'style'}],

			<?php 		
			$iCount=0;  
			if($numberOfPurchPerCategories)
			foreach($numberOfPurchPerCategories as $num) 
			{
				echo '["' . $num->title .'",'. $num->num .'],';//',"' . $branch->title . '  ' . $branch->num . '","' . $colorArray[$iCount%(sizeof($colorArray)-1)] .'"],';
				$iCount++;
			}
			?>

		]);

		var options = {
		  title: 'Purchased / category or source',
		  legend: {fontSize : 12},
		  chartArea: {
			'height': '70%'
		  }
		};

		var chart = new google.visualization.PieChart(document.getElementById('numberOfPurchPerCategories'));
		chart.draw(data, options);
    }
  </script>
 
<div style="clear:both;"></div>
<div id="numberOfCompletedTestDrivesPerCategories" style="text-align:center; width: 50%; height: 300px; float:left"></div>
<div id="numberOfPurchPerCategories" style="text-align:center; width: 50%; height: 300px; float:right"></div>
<div style="clear:both;"></div>


<br><br><u title="Total number of leads that are marked as completed test drive">a. Leads to Completed Test Drive</u>

<br>[<?php if($numberOfCompletedTestDrivesPerCategories) foreach($numberOfCompletedTestDrivesPerCategories as $num) echo $num->title .' <b>'.$num->num.' Completed</b> | '; ?>]

<br><br><u title="Eventually every lead has a category or otherwise have a source. So following is first categories and then source based finished leads.">b. Leads to finished</u>
<br>[<?php if($numberOfFinishedPerCategories) foreach($numberOfFinishedPerCategories as $num) echo $num->title .' <b>'.$num->num.' Finished</b> | '; ?>]
<br>[<?php if($numberOfFinishedPerWebsiteSource) foreach($numberOfFinishedPerWebsiteSource as $num) echo $num->title .' <b>'.$num->num.' Finished</b> | '; ?>]


<br><br><u title="Count of leads that are tagged as lost">c. Leads to lost</u>
<br>[<?php if($numberOfLostPerCategories) foreach($numberOfLostPerCategories as $num) echo $num->title .' <b>'.$num->num.' Lost</b> | '; ?>]
<br>[<?php if($numberOfLostPerWebsiteSource) foreach($numberOfLostPerWebsiteSource as $num) echo $num->title .' <b>'.$num->num.' Lost</b> | '; ?>]


<br><br><u title="this needs to be fixed as currently we can't track exactly which car was purchased. So need to update the logic first">d. Leads to purchase</u>

<br>[<?php if($numberOfPurchPerCategories) foreach($numberOfPurchPerCategories as $num) echo $num->title .' <b>'.$num->num.' Purchased</b> | '; ?>]


<br><br><u title="Avg score of received survey results">5. Score</u>

<br>[<?php if($avgScorePerCategories) foreach($avgScorePerCategories as $num) echo $num->title .' <b>'.round($num->avgval,2).' Avg Score</b> | '; ?>]

<br>[<?php if($avgScorePerWebsiteSource) foreach($avgScorePerWebsiteSource as $num) echo $num->title .' <b>'.round($num->avgval,2).' Avg Score</b> | '; ?>]


<br><br>

<?php 
}
?>


<?php 
if(!isset($filter_dep_report))
{
?>

<h2>Survey</h2>
<u style="color:blue;">Each user score per question %; Horizontal Bar Chart http://demo.interface.club/limitless/layout_1/LTR/default/d3_bars_basic.html</u>

<br><br>
1. Number of open emails
<br>This report can't be done at the moment. Need to update the logic of survey emails first.

<br><br><u title="Count of sent surveys">2.1 Number of sent surveys</u>
<br>[<?php if($numberOfSentSurvey) echo 'Total numbers <b>'.$numberOfSentSurvey->num.'</b>'; ?>]

<br><br><u title="(This count is the total number of opened surveys e.g clicked from the survey email.)">2.2 Number of viewed surveys (This count is when the link is clicked from the survey email.)</u>
<br>Note: This logic is 15 nov onwards, there must be more opened surveys in real, but this logic was implemented in the mid of September 2016
<br>[<?php if($numberOfViewedSurvey) echo 'Total numbers <b>'.$numberOfViewedSurvey->num.'</b>'; ?>]

<br><br><u title="Count of completed surveys">3. Number of completed surveys</u>
<br>[<?php if($numberOfCompletedSurvey) echo 'Total numbers <b>'.$numberOfCompletedSurvey->num.'</b>'; ?>]

<br><br>4. List of leads with uncompleted survey (with access to fill lead survey) 
<br>This is done. For now only the call center users will see those leads for which the survey is not filled till two weeks. The due date column will show "Survey". Even though the lead will be in archived status the call center guys will  be able to see such lead until they fill the survey. 

<br><br>

<?php 
}
?>


<?php 
if(!isset($filter_dep_report))
{
?>

<h2>Cars</h2>

 
<u>
<!--City > Branch > Users > Cars(Type) > (requests, scheduled, completed+walkin)-->
City > Branch > Users > Cars(Type) > (Completed, Not Completed)
</u>
<div class="chart-container">
	<div class="chart" id="d3-hierarchical-bars"></div>
</div>

<u>
<!--Cars(Type) > Branch > (requests, scheduled, completed+walkin)-->
Cars(Type) > Branch > (Completed, Not Completed)
</u>
<div class="chart-container">
	<div class="chart" id="d3-hierarchical-bars2"></div>
</div>

<?php 
}
?>

<script type="text/javascript">
//=====start Hieracrchical bar chart====
	
	<?php 
	if(!isset($filter_dep_report))
	{
		$date_from="";
		$date_to="";
		if(isset($_GET['date_from']) && $_GET['date_from']!="")	$date_from = $_GET['date_from'];
		if(isset($_GET['date_to']) && $_GET['date_to']!="")	$date_to = $_GET['date_to'];
	?>

    // Initialize chart
    stackedMultiples('#d3-hierarchical-bars', 900, "<?php echo base_url();?>report/d3_bars_hierarchical_cars/<?php echo $date_from."/".$date_to; ?>");

	stackedMultiples('#d3-hierarchical-bars2', 900, "<?php echo base_url();?>report/d3_bars_hierarchical_cars2/<?php echo $date_from."/".$date_to; ?>");
	
	<?php 
	}
	?>
//===end Hieracrchical bar chart===

</script>

<?php 
if(!isset($filter_dep_report))
{
?>


<u>Car type showing completed+walkin/schedule as %</u>
<div class="chart-container">
	<div class="chart has-fixed-height" style="height:500px;" id="stacked_clustered_columns"></div>
</div>
<br>
<br>
<br>
<u>Car type showing purchased/completed as %</u>
<div class="chart-container">
	<div class="chart has-fixed-height" style="height:500px;" id="stacked_clustered_columns2"></div>
</div>

<?php 
$carNames = "";
$completedPerc = "";
$actualValues = array();
if($allCarsKSAPerc)
{	

	$allCarsKSAArr = array_count_values($allCarsKSAPerc[0]);
	$allCarsKSAArrComp = array_count_values($allCarsKSAPerc[1]);
	$cars = "";
	foreach($allCarsKSAArr as $carId=>$count)
	{
		$compCount = 0;
		if(isset($allCarsKSAArrComp[$carId])) $compCount = $allCarsKSAArrComp[$carId];
		
		if($carNames!="") $carNames .= ",";
		$carNames .= "'".getGenVehShortName($carId)."'";
		
		if($completedPerc!="") $completedPerc .= ",";
		$completedPerc .= "'".round(($compCount/$count*100),2)."'";

		$actualValues[] = "(".$compCount."/".$count.")";

	}
}
?>

<?php 
$carNames2 = "";
$completedPerc2 = "";
$actualValues2 = array();
if($allPurchCarsKSAPerc)
{	

	$allCarsKSAArr = array_count_values($allPurchCarsKSAPerc[1]);
	$allCarsKSAArrPurch = array_count_values($allPurchCarsKSAPerc[2]);
	$cars = "";
	foreach($allCarsKSAArr as $carId=>$count)
	{
		$purchCount = 0;
		if(isset($allCarsKSAArrPurch[$carId])) $purchCount = $allCarsKSAArrPurch[$carId];

		/*$cars .= getSpecificCarName($carId)."(Purchased/Compl)(".$purchCount."/".$count.")";
		if($purchCount) $cars .= " = ".round(($purchCount/$count*100),2)."%"."<br>";
		else $cars .= " = 0%"."<br>";*/


		if($carNames2!="") $carNames2 .= ",";
		$carNames2 .= "'".getGenVehShortName($carId)."'";
		
		if($completedPerc2!="") $completedPerc2 .= ",";
		$completedPerc2 .= "'".round(($purchCount/$count*100),2)."'";

		$actualValues2[] = "(".$purchCount."/".$count.")";

	}
}
?>

<script type="text/javascript">

<?php echo "var actualValues = ". json_encode($actualValues) . ";\n"; ?>
<?php echo "var actualValues2 = ". json_encode($actualValues2) . ";\n"; ?>

</script>

<br>
<br>

<?php 
}
?>

<u title="The lead that are assinged and then they scheduled and complete the test drive including the Walk in for completed test drives">1. Cars per users</u>
<br>Customer request to be added to users pending here in textual report, but it is showen in the above graphical report.
<br>
<br>
<?php 
if($numOfCarsEachUser) foreach($numOfCarsEachUser[0] as $uId) 
{
	echo getEmployeeName($uId).': <u>Scheduled</u>=> ';
	if(isset($numOfCarsEachUser[1][$uId]))
	{
		$carsOfThisUserArr = explode(",",$numOfCarsEachUser[1][$uId]); //specific
		$carsOfThisUserArr = array_count_values($carsOfThisUserArr);
		$cars = "";
		foreach($carsOfThisUserArr as $carName=>$count)
		{
			if($count>1)
				$cars .= getSpecificCarName($carName)."(".$count."), ";
			else
				$cars .= getSpecificCarName($carName).", ";
		}
		echo '<b>'.rtrim($cars,', ')."</b>";
	}

	echo ': <u>Completed</u>=> ';
	if(isset($numOfCarsEachUser[2][$uId]))
	{
		$carsOfThisUserArr = explode(",",$numOfCarsEachUser[2][$uId]); //specific
		$carsOfThisUserArr = array_count_values($carsOfThisUserArr);
		$cars = "";
		foreach($carsOfThisUserArr as $carName=>$count)
		{
			if($carName)
			{
				if($count>1)
					$cars .= getSpecificCarName($carName)."(".$count."), ";
				else
					$cars .= getSpecificCarName($carName).", ";
			}
		}
		if($cars) echo '<b>'.rtrim($cars,", ")."</b><br>";
		else echo "<b>None</b><br>";
	}
}
?>

<?php 
if(!isset($filter_dep_report))
{
?>

<br><u title="The lead /branch that are scheduled and completed the test drive including the Walk in for completed test drives">2. Cars per branch</u>
<?php 
//if($numOfCarsEachBranch) foreach($numOfCarsEachBranch[0] as $bId) 
//if($numOfAllCarsEachBranch) foreach($numOfAllCarsEachBranch[0] as $bId) 
if($allBranches) foreach($allBranches as $branch) 
{
	$bId = $branch->id;
	if(getBranchName($bId)!='N/A' && isset($numOfAllCarsEachBranch[1][$bId]) && $numOfAllCarsEachBranch[1][$bId]!="" )
	{
		echo '<br><br>'.getBranchName($bId).': <br><u>Customer Requests</u>=> ';
		
		if(isset($numOfAllCarsEachBranch[1][$bId]))
		{
		
			$carsOfThisBranchArr = explode(",",$numOfAllCarsEachBranch[1][$bId]); //general all received from customers
			$carsOfThisBranchArr = array_count_values($carsOfThisBranchArr);
			$cars = "";
			foreach($carsOfThisBranchArr as $carName=>$count)
			{
				if($carName)
				{
					if($count>1)
						$cars .= getGeneralVehicleNameById($carName)."(".$count."), ";
					else
						$cars .= getGeneralVehicleNameById($carName).", ";
				}
			}
			echo '<b>'.rtrim($cars,', ')."</b>";
		}

		echo ' <br><u>Scheduled</u>=> ';	

		if(isset($numOfCarsEachBranch[1][$bId]))
		{
			$carsOfThisBranchArr = explode(",",$numOfCarsEachBranch[1][$bId]); //specific
			$carsOfThisBranchArr = array_count_values($carsOfThisBranchArr);
			$cars = "";
			foreach($carsOfThisBranchArr as $carName=>$count)
			{
				if($count>1)
					$cars .= getSpecificCarName($carName)."(".$count."), ";
				else
					$cars .= getSpecificCarName($carName).", ";
			}
			echo '<b>'.rtrim($cars,', ')."</b>";
		}

		echo ' <br><u>Completed</u>=> ';

		if(isset($numOfCarsEachBranch[2][$bId]))
		{		
			$carsOfThisBranchArr = explode(",",$numOfCarsEachBranch[2][$bId]); //specific
			$carsOfThisBranchArr = array_count_values($carsOfThisBranchArr);
			$cars = "";
			foreach($carsOfThisBranchArr as $carName=>$count)
			{
				if($carName)
				{
					if($count>1)
						$cars .= getSpecificCarName($carName)."(".$count."), ";
					else
						$cars .= getSpecificCarName($carName).", ";
				}
			}
			if($cars) echo '<b>'.rtrim($cars,", ")."</b>";
			else echo "<b>None</b>";
		}
	}
}
?>


<br><br><br><br><u title="Including the walkin for completed test drives">3. All cars Ksa</u>
<br><br>
<?php 
if($allCarsKSA)
{	
	echo '<u>Customer Requests</u>=> ';
	$allCarsKSAArr = array_count_values($allCarsKSA[0]);
	$cars = "";
	foreach($allCarsKSAArr as $carName=>$count)
	{
		if(getGeneralVehicleNameById($carName)!='N/A')
		{
			if($count>1)
				$cars .= getGeneralVehicleNameById($carName)."(".$count."), ";
			else
				$cars .= getGeneralVehicleNameById($carName).", ";
		}
	}
	echo '<b>'.rtrim($cars,', ')."</b><br>";


	echo '<u>Scheduled</u>=> ';
	$allCarsKSAArr = array_count_values($allCarsKSA[1]);
	$cars = "";
	foreach($allCarsKSAArr as $carName=>$count)
	{
		if($count>1)
			$cars .= getSpecificCarName($carName)."(".$count."), ";
		else
			$cars .= getSpecificCarName($carName).", ";
	}
	echo '<b>'.rtrim($cars,', ')."</b><br>";

	echo '<u>Completed</u>=> ';
	$allCarsKSAArr = array_count_values($allCarsKSA[2]);
	$cars = "";
	foreach($allCarsKSAArr as $carName=>$count)
	{
		if($count>1)
			$cars .= getSpecificCarName($carName)."(".$count."), ";
		else
			$cars .= getSpecificCarName($carName).", ";
	}
	echo '<b>'.rtrim($cars,', ')."</b><br>";
}
?>

<br><br><br><u title="Including the walkin for completed test drives">4. Cars per city</u>
<?php 
//if($numOfCarsEachCity) foreach($numOfCarsEachCity[0] as $cId) 
//if($numOfAllCarsEachCity) foreach($numOfAllCarsEachCity[0] as $cId) 
if($cities) foreach($cities as $city) 
{
	$cId = $city->id;

	if(getCityName($cId)!='N/A' && isset($numOfAllCarsEachCity[1][$cId]) && $numOfAllCarsEachCity[1][$cId]!='')
	{
		echo '<br><br>'.getCityName($cId).': <br><u>Customer Requests</u>=> ';
		if(isset($numOfAllCarsEachCity[1][$cId]))
		{
			$carsOfThisBranchArr = explode(",",$numOfAllCarsEachCity[1][$cId]); //general all received from customers
			$carsOfThisBranchArr = array_count_values($carsOfThisBranchArr);
			$cars = "";
			foreach($carsOfThisBranchArr as $carName=>$count)
			{
				if($carName)
				{
					if($count>1)
						$cars .= getGeneralVehicleNameById($carName)."(".$count."), ";
					else
						$cars .= getGeneralVehicleNameById($carName).", ";
				}
			}
			echo '<b>'.rtrim($cars,', ')."</b>";
		}

		echo ' <br><u>Scheduled</u>=> ';
		if(isset($numOfCarsEachCity[1][$cId]))
		{
			$carsOfThisCityArr = explode(",",$numOfCarsEachCity[1][$cId]); //specific
			$carsOfThisCityArr = array_count_values($carsOfThisCityArr);
			$cars = "";
			foreach($carsOfThisCityArr as $carName=>$count)
			{
				if($count>1)
					$cars .= getSpecificCarName($carName)."(".$count."), ";
				else
					$cars .= getSpecificCarName($carName).", ";
			}
			echo '<b>'.rtrim($cars,', ')."</b>";
		}

		echo ' <br><u>Completed</u>=> ';
		if(isset($numOfCarsEachCity[2][$cId]))
		{
			$carsOfThisCityArr = explode(",",$numOfCarsEachCity[2][$cId]); //specific
			$carsOfThisCityArr = array_count_values($carsOfThisCityArr);
			$cars = "";
			foreach($carsOfThisCityArr as $carName=>$count)
			{
				if($carName)
				{
					if($count>1)
						$cars .= getSpecificCarName($carName)."(".$count."), ";
					else
						$cars .= getSpecificCarName($carName).", ";
				}
			}
			if($cars) echo '<b>'.rtrim($cars,", ")."</b>";
			else echo "<b>None</b>";
		}
	}
}
?>


<br><br><u title="Including the walkin for completed test drives">5. Successful test drive on branch (Completed test drives)</u>
<br><br>
<?php 
if($numOfSuccessDrivesEachBranch) foreach($numOfSuccessDrivesEachBranch[0] as $bId) 
{
	echo getBranchName($bId).': ';
	if(isset($numOfSuccessDrivesEachBranch[1][$bId]))
	{
		$carsOfThisBranchArr = explode(",",$numOfSuccessDrivesEachBranch[1][$bId]); //specific
		$carsOfThisBranchArr = array_count_values($carsOfThisBranchArr);
		$cars = "";
		foreach($carsOfThisBranchArr as $carName=>$count)
		{
			if($count>1)
				$cars .= getSpecificCarName($carName)."(".$count."), ";
			else
				$cars .= getSpecificCarName($carName).", ";
		}
		echo '<b>'.rtrim($cars,', ')."</b><br>";
	}
}
?>

<br><u title="Including the walkin for completed test drives">6. Successful test drive per cars model and type (Completed test drives)</u>
<br><br>
<?php 
if($successDrives) foreach($successDrives[0] as $gCId) 
{
	echo getGeneralCarName($gCId).': ';
	if(isset($successDrives[1][$gCId]))
	{
		$carsSpecArr = explode(",",$successDrives[1][$gCId]); //specific
		$carsSpecArr = array_count_values($carsSpecArr);
		$cars = "";
		foreach($carsSpecArr as $carName=>$count)
		{
			if($count>1)
				$cars .= getSpecificCarName($carName)."(".$count."), ";
			else
				$cars .= getSpecificCarName($carName).", ";
		}
		echo '<b>'.rtrim($cars,', ')."</b><br>";
	}
}
?>

<br><u title="Completed vs scheduled (per car type), Including the walkin for completed test drives">7. Completed vs scheduled (per car type)</u>
<br>
<?php 
if($allCarsKSAPerc)
{	

	$allCarsKSAArr = array_count_values($allCarsKSAPerc[0]);
	$allCarsKSAArrComp = array_count_values($allCarsKSAPerc[1]);
	$cars = "";
	foreach($allCarsKSAArr as $carId=>$count)
	{
		$compCount = 0;
		if(isset($allCarsKSAArrComp[$carId])) $compCount = $allCarsKSAArrComp[$carId];
		$cars .= getGeneralVehicleNameById($carId)."(Compl+Walkin/Sche)(".$compCount."/".$count.")";
		if($compCount) $cars .= " = ".round(($compCount/$count*100),2)."%"."<br>";
		else $cars .= " = 0%"."<br>";

	}
	echo '<b><br>'.$cars."</b><br>";

}
?>

 
<u title="Purchased vs Completed (per car type), Including the walkin for completed test drives">8. Purchased vs Completed (per car type)</u>
<br>
<?php 
if($allPurchCarsKSAPerc)
{	

	$allCarsKSAArr = array_count_values($allPurchCarsKSAPerc[1]);
	$allCarsKSAArrPurch = array_count_values($allPurchCarsKSAPerc[2]);
	$cars = "";
	foreach($allCarsKSAArr as $carId=>$count)
	{
		$purchCount = 0;
		if(isset($allCarsKSAArrPurch[$carId])) $purchCount = $allCarsKSAArrPurch[$carId];
		$cars .= getGeneralVehicleNameById($carId)."(Purchased/Compl)(".$purchCount."/".$count.")";
		if($purchCount) $cars .= " = ".round(($purchCount/$count*100),2)."%"."<br>";
		else $cars .= " = 0%"."<br>";

	}
	echo '<b><br>'.$cars."</b><br>";

}
?>

<?php 
}
?>

<?php 
if(!isset($filter_dep_report))
{
?>

<h2>Conversions</h2>
<br>[<?php if($numberOfLeads) echo "Total Number Leads: <b>".$numberOfLeads->num."</b>"; ?>]
<br>[<?php if($numberOfTestDriveLeads) echo "Total Number of Test Drive Leads: <b>".$numberOfTestDriveLeads->num."</b>"; ?>] Leads with category "Online Test Drive Request" or "New Cars Test Drive"
<br>

<br><u title="Total counts without duplicates. as all above reports and calculations are also without duplicates.">1. Lead to test drive completed</u>

<br><?php if($numberOfCompletedTestDrivesLeads && $numberOfCompletedTestDrivesLeads->num) echo "Total Number of Completed Test Drive / Total Number of Test Drive Leads: <br><b>".$numberOfCompletedTestDrivesLeads->num."/".$numberOfTestDriveLeads->num." = ".round((((int)$numberOfCompletedTestDrivesLeads->num/(int)$numberOfTestDriveLeads->num)*100),2)."%</b>"; ?>

<br><br><u title="The purchased tag needs to be updated to track which car is purchased. for now total leads that are tagged with purchased is following count">2. Lead to buy</u>
<br><?php if($numberOfPurchasedCarsLeads && $numberOfLeads->num) echo "Total Number of Purcahsed / Total Number Leads: <br><b>".$numberOfPurchasedCarsLeads->num."/".$numberOfLeads->num." = ".round((((int)$numberOfPurchasedCarsLeads->num/(int)$numberOfLeads->num)*100),2)."%</b>"; ?>

<br><br><u title="Average time between the leads created that were converted to purcahsed">3. Time from lead creation to purchase </u>
<br><?php if($timeFromLeadCreationToPurchase) echo "<b>Average: ".secondsToDays($timeFromLeadCreationToPurchase->avgval)."</b>"; ?>

<br><br><u title="Average time between lead is created and then visit (showroom & field)">4. Lead to visit (showroom & field)</u>
<br><?php if($timeFromLeadCreationToVisit && $numberOfLeads->num) echo "Total Number of Showroom or Field Visit / Total Number Leads: <br><b>".$timeFromLeadCreationToVisit->num."/".$numberOfLeads->num." = ".round((((int)$timeFromLeadCreationToVisit->num/(int)$numberOfLeads->num)*100),2)."%</b>"; ?>

<br><br><u title="Average time between the lead is created and it is tagged as lost">5. Lead to lost</u>
<br><?php if($timeFromLeadCreationToLost && $numberOfLeads->num) echo "Total Number of Lost Leads / Total Number Leads: <br><b>".$timeFromLeadCreationToLost->num."/".$numberOfLeads->num." = ".round((((int)$timeFromLeadCreationToLost->num/(int)$numberOfLeads->num)*100),2)."%</b>"; ?>


<br><br>

<?php 
}
?>


<?php 
if(!isset($filter_dep_report))
{
?>

<h2>Versus Branches</h2>
<br><u title="Received surveys average score / branch">1. Scores</u>
<br>[<?php if($branchesAvgScore) foreach($branchesAvgScore as $avg) { echo $avg->title .' <b>'.round($avg->avgval,2).'</b> | '; } ?>]

<div class="chart-container">
	<div class="chart" id="google-bar13"></div>
</div>
<script type="text/javascript">
// Bar chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawBar13);


// Chart settings
function drawBar13() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Surveys', 'Scores', {role: 'tooltip'}, {role: 'style'}, { role: 'annotation' } ],
        //['2004',  1000,      400],
        //['2005',  1170,      460],
        //['2006',  660,       1120],
        //['2007',  1030,      540]

		<?php 

		
		$iCount=0;  
		if($branchesAvgScore)
		foreach($branchesAvgScore as $avg) 
		{
			echo '["' . $avg->title .'",'. $avg->avgval  . ',"'. $avg->title .' '. $avg->avgval  . '","' . $colorArray[$iCount%(sizeof($colorArray)-1)] .'", '.$avg->avgval.'],';
			$iCount++;
		}
		else
		{
			echo "['',0,'','','']";
		}
		?>

    ]);


    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Branches average score',        
		height: 300,		
        fontSize: 12,
        chartArea: {
            left: '15%',
            width: '80%',
            height: 250
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0			
        },
		hAxis: {
			viewWindow: {
				max: 5
			},
			gridlines:{
                color: '#e5e5e5',
                count: 11
            },
            minValue: 0
		},
        legend: {
            position: 'none'            
        },
		/*hAxis: {
		format:'#%'
		}*/
    };

    // Draw chart
    var bar = new google.visualization.BarChart($('#google-bar13')[0]);
    bar.draw(data, options_bar);

}
</script>

<br><br>2. Test drive completed
<br><br>
<div class="chart-container">
	<div class="chart" id="google-bar22"></div>
</div>
<script type="text/javascript">
// Bar chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawBar22);


// Chart settings
function drawBar22() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Branches', 'Number of Test Drives', { role: 'annotation' }],
        /*['branch1',  1000,      400],
        ['branch2',  1170,      460],
        ['branch3',  660,       1120],
        ['branch4',  1030,      540]*/

		<?php 
		if($compTestDrivesBranches)
		foreach($compTestDrivesBranches as $num) 
		{
			echo '["' . $num->title .'",'. $num->num  . ','. $num->num .'],';
			$iCount++;
		}
		else
		{
			echo "['',0,'']";
		}
		
		?>

    ]);


    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Number of Test Drive Completed',
		height: 300,		
        fontSize: 12,
        chartArea: {
            left: '15%',
            width: '80%',
            height: 250
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    // Draw chart
    var bar = new google.visualization.BarChart($('#google-bar22')[0]);
    bar.draw(data, options_bar);

}
</script>



<br><br>

<br><br>3. Purchased
<br><br>
<div class="chart-container">
	<div class="chart" id="google-bar222"></div>
</div>
<script type="text/javascript">
// Bar chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawBar222);


// Chart settings
function drawBar222() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Branches', 'Number of Purchased', { role: 'annotation' }],
        /*['branch1',  1000,      400],
        ['branch2',  1170,      460],
        ['branch3',  660,       1120],
        ['branch4',  1030,      540]*/

		<?php 
		if($purchTestDrivesBranches)
		foreach($purchTestDrivesBranches as $num) 
		{
			echo '["' . $num->title .'",'. $num->num  . ','. $num->num .'],';
			$iCount++;
		}
		else
		{
			echo "['',0,'']";
		}
		?>

    ]);


    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Purchased',
		height: 300,		
        fontSize: 12,
        chartArea: {
            left: '15%',
            width: '80%',
            height: 250
        },
		colors: ['#299949'],
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    // Draw chart
    var bar = new google.visualization.BarChart($('#google-bar222')[0]);
    bar.draw(data, options_bar);

}
</script>

<br><br>

<?php 
}
?>

<?php 
if(!isset($filter_dep_report))
{
?>

<h2>Versus Sales Consultants</h2>
<br>
1. Scores
<br>

<div class="chart-container">
	<div class="chart" id="google-bar15"></div>
</div>
<script type="text/javascript">
// Bar chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawBar15);


// Chart settings
function drawBar15() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Surveys', 'Scores', {role: 'tooltip'}, {role: 'style'}, { role: 'annotation' } ],
        //['2004',  1000,      400],
        //['2005',  1170,      460],
        //['2006',  660,       1120],
        //['2007',  1030,      540]

		<?php 

		
		$iCount=0;  

		foreach($userAvgScore2 as $avg) 
		{
			echo '["' . $avg->full_name .'",'. $avg->avgval  . ',"'. $avg->full_name .' '. $avg->avgval  . '","' . $colorArray[$iCount%(sizeof($colorArray)-1)] .'", '.$avg->avgval.'],';
			$iCount++;
		}
		?>

    ]);


    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Sales Consultants Average Score',        
		height: 600,		
        fontSize: 12,
        chartArea: {
            left: '15%',
            width: '80%',
            height: 550
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0			
        },
		hAxis: {
			viewWindow: {
				max: 5
			},
			gridlines:{
                color: '#e5e5e5',
                count: 11
            },
            minValue: 0
		},
        legend: {
            position: 'none'            
        },
		/*hAxis: {
		format:'#%'
		}*/
    };

    // Draw chart
    var bar = new google.visualization.BarChart($('#google-bar15')[0]);
    bar.draw(data, options_bar);

}
</script>


<br><br>2. Leads to purchase
<br> 
<br>

<div class="chart-container">
	<div class="chart" id="google-bar16"></div>
</div>
<script type="text/javascript">
// Bar chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawBar16);


// Chart settings
function drawBar16() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Name', 'count', {role: 'tooltip'}, {role: 'style'}, { role: 'annotation' } ],
        //['2004',  1000,      400],
        //['2005',  1170,      460],
        //['2006',  660,       1120],
        //['2007',  1030,      540]

		<?php 

		
		$iCount=0;  
		if($purchasedSalesCons)
		foreach($purchasedSalesCons as $num) 
		{
			echo '["' . $num->full_name .'",'. $num->num  . ',"'. $num->full_name .' '. $num->num  . '","' . $colorArray[$iCount%(sizeof($colorArray)-1)] .'", '.$num->num.'],';
			$iCount++;
		}
		else{
			echo "['',0,'','','']";
		}
		?>

    ]);


    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Sales Consultants Purchased Count',        
		height: 600,		
        fontSize: 12,
        chartArea: {
            left: '15%',			
            width: '80%',
            height: 550
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
        legend: {
            position: 'none'            
        },
		/*hAxis: {
		format:'#%'
		}*/
    };

    // Draw chart
    var bar = new google.visualization.BarChart($('#google-bar16')[0]);
    bar.draw(data, options_bar);

}
</script>


<?php 
}
?>
<br>
<u title="Total count of lost leads /user. Users who marked the leads as lost">3. Leads to lost</u>
<br>[<?php if($userLostTLPM) foreach($userLostTLPM as $num) echo $num->full_name .' <b>'.$num->num.'</b> | '; ?>]

<div class="chart-container">
	<div class="chart" id="google-bar14"></div>
</div>
<script type="text/javascript">
// Bar chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawBar14);


// Chart settings
function drawBar14() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Leads', 'Number of Lost', {role: 'tooltip'}, {role: 'style'}, { role: 'annotation' } ],
        //['2004',  1000,      400],
        //['2005',  1170,      460],
        //['2006',  660,       1120],
        //['2007',  1030,      540]

		<?php 

		
		$iCount=0;  
		foreach($userLostTLPM as $num) 
		{
			echo '["' . $num->full_name .'",'. $num->num  . ',"'. $num->full_name .' '. $num->num  . '","' . $colorArray[$iCount%(sizeof($colorArray)-1)] .'", '.$num->num.'],';
			$iCount++;
		}
		?>

    ]);


    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Leads to Lost',        
		height: 600,		
        fontSize: 12,
        chartArea: {
            left: '15%',
            width: '80%',
            height: 550
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0			
        },
        legend: {
            position: 'none'            
        },
		/*hAxis: {
		format:'#%'
		}*/
    };

    // Draw chart
    var bar = new google.visualization.BarChart($('#google-bar14')[0]);
    bar.draw(data, options_bar);

}
</script>


<?php
}
?>


<?php 
if(isset($filter_individual_report))
{
?>


<script type="text/javascript">		
	google.charts.load('current', {'packages':['corechart']}); 
	google.charts.setOnLoadCallback(drawChart5);
	function drawChart5() {
		var data = google.visualization.arrayToDataTable([
		  ['Leads', 'Numbers'], //{role: 'tooltip'}, {role: 'style'}],

			<?php 		
			
			if($usersNumOfClosedLeads) echo '["Leads Closed",'. $usersNumOfClosedLeads[0]->num .'],';
			else echo '["Leads Closed",0],';
			
			if($usersNumOfFinishedButNotClosedLeads) echo '["Leads Pending Manager",'. $usersNumOfFinishedButNotClosedLeads[0]->num .'],';
			else echo '["Leads Pending Manager",0],';
			
			if($numOfDisapprovedActionEachUser) echo '["# of Disapproved",'. $numOfDisapprovedActionEachUser[0]->num .']';
			else echo '["# of Disapproved",0]';
				
			
			?>
			
		]);

		var options = {
		  title: 'Approval Rates',
		  legend: {fontSize : 12},
		  legend: 'bottom',
		  colors: ['#299949', '#41ABE4', '#E52727'],
		  chartArea: {
			'height': '70%',
			'width': '100%'
		  }
		};

		var chart = new google.visualization.PieChart(document.getElementById('approval_rates'));
		chart.draw(data, options);
    }

	//===
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart6);
	function drawChart6() {
		var data = google.visualization.arrayToDataTable([
		  ['Leads', 'Numbers'], //{role: 'tooltip'}, {role: 'style'}],

			<?php 		
			
			if($numOfSuccessfullCallActionEachUser) echo '["Successful Calls",'. $numOfSuccessfullCallActionEachUser[0]->num .'],';
			else echo '["Successful Calls",0],';
			
			if($numOfNoAnsActionEachUser) echo '["No Answer Call",'. $numOfNoAnsActionEachUser[0]->num .'],';
			else echo '["No Answer Call",0],';
			
			if($numOfScheduledCallActionEachUser) echo '["Scheduled Call",'. $numOfScheduledCallActionEachUser[0]->num .']';
			else echo '["Scheduled Call",0]';
				
			
			?>

		]);

		var options = {
		  title: 'Call Rates',		 
		  legend: {fontSize : 12},
		  legend: 'bottom',
		  colors: ['#299949', '#41ABE4', '#E52727'],
		  chartArea: {
			'height': '70%',
			'width': '100%'
		  }
		};

		var chart = new google.visualization.PieChart(document.getElementById('call_rates'));
		chart.draw(data, options);
    }
  </script>
 
<div style="clear:both;"></div>
<div id="approval_rates" style="display: block; margin: 0 auto; text-align:center; width: 50%; height: 300px; float:left"></div>
<div id="call_rates" style="text-align:center; width: 50%; height: 300px; float:right"></div>
<div style="clear:both;"></div>


<script type="text/javascript">
// Bar chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawBar6);


// Chart settings
function drawBar6() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Satisfaction Score', 'Score', {role: 'tooltip'}, {role: 'style'}, { role: 'annotation' } ],
        //['2004',  1000,      400],
        //['2005',  1170,      460],
        //['2006',  660,       1120],
        //['2007',  1030,      540]

		
		<?php
		$ifUserSelfScore = false;
		if($userGraphAvgScore)
		{
			foreach($userGraphAvgScore as $userScore)
			{
				$full_name = $userScore['full_name'];
				$avgval = round($userScore['avgval'],2);
				$colorDUC = "#3366CC";
				$userSelfColor = "#eef213"; //yellow
				if($userScore['userid']===$_GET['user_id']) 
				{
					$ifUserSelfScore = true;
					$colorDUC = $userSelfColor;
					if($userGraphDepsAvgScore)
					echo '["Department",'. round($userGraphDepsAvgScore->avgval,2)  . ',"Department '. round($userGraphDepsAvgScore->avgval,2)  . '","#299949", "'.round($userGraphDepsAvgScore->avgval,2).'"],';
				}
				
				echo '["' . $full_name .'",'. $avgval  . ',"'. $full_name .' '. $avgval  . '","' . $colorDUC .'", "'.$avgval.'"],';
				
			}	
			if(!$ifUserSelfScore)
				if($userGraphDepsAvgScore)
					echo '["Department",'. round($userGraphDepsAvgScore->avgval,2)  . ',"Department '. round($userGraphDepsAvgScore->avgval,2)  . '","#299949", "'.round($userGraphDepsAvgScore->avgval,2).'"],';			
		}
		else
		 {
			 echo "['',0,'','','']";
		 }
		?>
				
    ]);


    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Satisfaction Score',        
		height: 250,		
        fontSize: 11,
		bar: {groupWidth: "50%"},
        chartArea: {
            left: '15%',
            width: '80%',
            height: 220
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
		hAxis: {
			viewWindow: {
				max: 5
			},
			gridlines:{
                color: '#e5e5e5',
                count: 11
            },
            minValue: 0
		},        
        legend: {
            position: 'none'            
        }
    };

    // Draw chart
    var bar = new google.visualization.BarChart($('#satisfaction_score')[0]);
    bar.draw(data, options_bar);

}


//===========
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawVisualization);


      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
         ['SuccessCalls', 'Count', 'Count', { role: 'annotation' }],
         /*['2004/05',  162,  165 ],
         ['2005/06',  132,  135],
         ['2006/07',  154,   157],
         ['2007/08',  136,    139],
         ['2008/09',  133,    136]*/
		 <?php
		 if($hourTimeOfSuccessfullCallAction)		
		 foreach($hourTimeOfSuccessfullCallAction as $num) 
		 {
			 echo '["' . $num->timehour .'",'. $num->num  . ','. $num->num .',"'.$num->num.'"],';
		 }
		 else
		 {
			 echo "['',0,0,'']";
		 }
		 ?>
      ]);

    var options = {
      title : 'Successfull Calls Timeline',
      vAxis: {title: 'Calls'},
      hAxis: {title: 'Time'},
      seriesType: 'bars',
      series: {1: {type: 'line'},},
      bar: {
		groupWidth: 20
	  },
	  legend: {
        position: 'none' 
      }	  
    };

    var chart = new google.visualization.ComboChart(document.getElementById('successfull_calls_timeline'));
    chart.draw(data, options);
  }

</script>

<div style="clear:both;"></div>
<br><br><br><br>
<div id="successfull_calls_timeline" style="display: block; margin: 0 auto; text-align:center; width: 50%; height: 250px; float:left"></div>
<div id="satisfaction_score" style="text-align:center; width: 50%; height: 300px; float:right"></div>
<div style="clear:both;"></div>





<script>
// Line chart
// ------------------------------


// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawLineChart);


// Chart settings
function drawLineChart() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Year', 'Score', { role: 'annotation' }],
        /*['2004',  1.5, 444],
        ['2005',  1.5, 222],
        ['2006',  1.5, 333],
        ['2007',  1.5, 454]*/
		
		<?php 
		if($uGraphTotalAvgScoreYearly)  
		{
			
			if(isset($uGraphTotalAvgScoreYearly[$_GET['user_id']])) 
			{ 
				
				if(isset($_GET['date_from']) && $_GET['date_from']!="")
					$start = $month = strtotime($_GET['date_from']);
				else
					$start = $month = strtotime('2016-7-01'); //July 2016

				if(isset($_GET['date_to']) && $_GET['date_to']!="")
					$end = strtotime($_GET['date_to']);
				else
					$end = strtotime(date("Y-m-d"));

				$lcc = 0;
				while($month <= $end)
				{
					 $key = date('M Y', $month);
					 
					 if($lcc>0) echo ",";
					 echo "[";
					 
					 echo "'".date('M Y', $month)."'";

					 if(isset($uGraphTotalAvgScoreYearly[$_GET['user_id']][$key])) 
					 {
						 $val = $uGraphTotalAvgScoreYearly[$_GET['user_id']][$key];
						 //echo '&nbsp; | &nbsp;'.$key.' <b>['.round($val,2).'</b>]'; 
						 echo ",".round($val,2);
						 echo ",".round($val,2);
					 }
					 else 
					 {
						 // for missing months, display user's overall avg. 
						$val = $userGraphAvgScoreAsUidKey[$_GET['user_id']]; //user total avg
						//echo '&nbsp; | &nbsp;'.$key.' <b>['.round($val,2).'</b>]'; 
						echo ",".round($val,2);
						echo ",".round($val,2);
					 }


					 $month = strtotime("+1 month", $month);
					 
					 echo "]";
					$lcc++;
				}
				
			}  
		}else {
			echo "['2016', 0 , 0]";
		} 
	?>
	
    ]);

    // Options
    var options = {
        fontName: 'Arial',
        height: 250,
		title: 'Average Score (empty months are filled by overall avg)',
        curveType: 'function',
        fontSize: 12,
        chartArea: {
            left: '15%',
            width: '80%',
            height: 200
        },
        pointSize: 4,
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
        vAxis: {
           // title: 'Sales and Expenses',
            titleTextStyle: {
                fontSize: 13,
                italic: false
            },
            gridlines:{
                color: '#e5e5e5',
                count: 11
            },
            minValue: 0,
			viewWindow: {
				max: 5
			}			
        },		
        legend: {
           position: 'none' 
        }
    };

    // Draw chart
    var line_chart12 = new google.visualization.LineChart($('#annual_avg_score')[0]);
    line_chart12.draw(data, options);
}



// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawLineChart2);


// Chart settings
function drawLineChart2() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Year', 'Total Leads', { role: 'annotation' }, {role: 'style'}, 'Completed TD', { role: 'annotation' }, {role: 'style'}],
        /*['2004',  100, 100 , 20 , 20],
        ['2004',  100, 100 , 20 , 20],
        ['2004',  100, 100 , 20 , 20],
        ['2004',  100, 100 , 20 , 20],*/
		
	<?php 
		if($uGraphTotalLeadsYearly)  
		{
			
			if(isset($uGraphTotalLeadsYearly[$_GET['user_id']])) 
			{ 
				
				if(isset($_GET['date_from']) && $_GET['date_from']!="")
					$start = $month = strtotime($_GET['date_from']);
				else
					$start = $month = strtotime('2016-7-01'); //July 2016

				if(isset($_GET['date_to']) && $_GET['date_to']!="")
					$end = strtotime($_GET['date_to']);
				else
					$end = strtotime(date("Y-m-d"));

				$lcc = 0;
				while($month <= $end)
				{
					 $key = date('M Y', $month);
					 
					 if($lcc>0) echo ",";
					 echo "[";
					 
					 echo "'".date('M Y', $month)."'";

					 if(isset($uGraphTotalLeadsYearly[$_GET['user_id']][$key])) 
					 {
						 $val = $uGraphTotalLeadsYearly[$_GET['user_id']][$key];
						 echo ",".$val; 
						 echo ",".$val; 
						 echo ",'#3367d6'";						 
					 }
					 else 
					 {
						// for missing months, display 0. 
						$val = 0;
						echo ",".$val;
						echo ",".$val;
						echo ",'#3367d6'";						
					 }
					 
					 //completed test drives logic here===
					 if(isset($uGraphTotalComLeadsYearly[$_GET['user_id']][$key])) 
					 {
						 $val = $uGraphTotalComLeadsYearly[$_GET['user_id']][$key];						 
						 echo ",".$val; 
						 echo ",".$val; 
						 echo ",'#FF7F50'";
					 }
					 else 
					 {
						// for missing months, display 0. 
						$val = 0;						
						echo ",".$val;
						echo ",".$val;
						echo ",'#FF7F50'";
					 }


					 $month = strtotime("+1 month", $month);
					 
					 echo "]";
					$lcc++;
				}
				
			}  
		}else
		{			
			echo "['0', 0, '0', '', 0, '0', '']";
		}
	?>
		
		
    ]);

    // Options
    var options = {
        fontName: 'Arial',
        height: 250,
		title: 'Number of Leads',
        curveType: 'function',
        fontSize: 12,
        chartArea: {
            left: '15%',
            width: '80%',
            height: 160
        },
        pointSize: 4,
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },	
		vAxis: {
			viewWindowMode: "explicit", 
			viewWindow:{ min: 0 }
		},	
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    // Draw chart
    var line_chart11 = new google.visualization.LineChart($('#leads_td')[0]);
    line_chart11.draw(data, options);
}
</script>





<div style="clear:both;"></div>
<br><br><br>
<div id="annual_avg_score" style="display: block; margin: 0 auto; text-align:center; width: 50%; height: 250px; float:left"></div>
<div id="leads_td" style="text-align:center; width: 50%; height: 300px; float:right"></div>
<div style="clear:both;"></div>




<script>
// Pie chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawPie4);


// Chart settings    
function drawPie4() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Type', 'Source'],
        /*['Work',     11],
        ['Eat',      2],
        ['Commute',  2],
        ['Watch TV', 2],
        ['Sleep',    7]*/
		<?php
		$colorOptFP4 = "";
		$iCount=0;  
		if($uGraphLeadSource)		
		foreach($uGraphLeadSource as $num) 
		{
			$alscolor = "#5aafce";
			
			$alscolarr['Call Back'] = "#3366CC";
			$alscolarr['Contact us'] = "#DC3912";
			$alscolarr['Test Drive'] = "#109618";
			$alscolarr['Online & Digital'] = "#990099";
			$alscolarr['Call Center - ROI'] = "#FF9900";			
			$alscolarr['Social Media'] = "#0099C6";
			$alscolarr['Walk-In'] = "#DD4477";
			
			if(isset($alscolarr[$num->source])) $alscolor = $alscolarr[$num->source];
			
			if($iCount>0) $colorOptFP4 .= ",";
			$colorOptFP4 .= "'".$alscolor."'";
		
			echo '["' . $num->source .'",'. $num->num .'],';
			$iCount++;
		}
		?>
		
    ]);

    // Options
    var options_pie = {
		title: 'Leads Source Breakdown',
		sliceVisibilityThreshold: 0,		
        fontName: 'Arial',
        height: 250,    
		colors: [<?php echo $colorOptFP4 ?>],
        chartArea: {
           left: '15%',
            width: '80%',
            height: 160
        }
    };
    

    // Instantiate and draw our chart, passing in some options.
    var pie = new google.visualization.PieChart($('#leads_source1')[0]);
    pie.draw(data, options_pie);
}

// Pie chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawPie5);


// Chart settings    
function drawPie5() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Type', 'Source'],
        /*['Work',     11],
        ['Eat',      2],
        ['Commute',  2],
        ['Watch TV', 2],
        ['Sleep',    7]*/
		<?php
		$colorOptFP5 = "";
		$iCount=0;  
		if($uGraphLeadSource2)		
		foreach($uGraphLeadSource2 as $num) 
		{
			$alscolor = "#5aafce";
			
			$alscolarr['Call Back'] = "#3366CC";
			$alscolarr['Contact us'] = "#DC3912";
			$alscolarr['Test Drive'] = "#109618";
			$alscolarr['Online & Digital'] = "#990099";
			$alscolarr['Call Center - ROI'] = "#FF9900";			
			$alscolarr['Social Media'] = "#0099C6";
			$alscolarr['Walk-In'] = "#DD4477";
			
			if(isset($alscolarr[$num->source])) $alscolor = $alscolarr[$num->source];
			
			if($iCount>0) $colorOptFP5 .= ",";
			$colorOptFP5 .= "'".$alscolor."'";
			
			echo '["' . $num->source .'",'. $num->num .'],';
			$iCount++;
		}
		?>
		
    ]);

    // Options
    var options_pie = {
		title: 'Current Month Leads Source Breakdown (<?php echo date('F Y'); ?>)',
		sliceVisibilityThreshold: 0,		
        fontName: 'Arial',
		colors: [<?php echo $colorOptFP5 ?>],
        height: 250,        
        chartArea: {
           left: '15%',
            width: '80%',
            height: 160
        }
    };
    

    // Instantiate and draw our chart, passing in some options.
    var pie2 = new google.visualization.PieChart($('#leads_source2')[0]);
    pie2.draw(data, options_pie);
}
</script>


<div style="clear:both;"></div>
<br><br><br>
<div id="leads_source1" style="display: block; margin: 0 auto; text-align:center; width: 50%; height: 250px; float:left"></div>
<div id="leads_source2" style="text-align:center; width: 50%; height: 300px; float:right"></div>
<div style="clear:both;"></div>



<div style="clear:both;"></div>
<br><br><br>
<div style="width: 50%; float:left">
<div id="indCHOSDiv" style="display: block; margin: 0 auto; text-align:center; width: 95%; height: 200px; float:left; overflow-x:hidden; overflow-y:auto;">
<b style="float:left;">Leads by Car</b>
<div class="chart-container">
	<div class="chart" id="leads_bycar"></div>
</div>
<?php
		$date_from="";
		$date_to="";
		$user_id="";
		if(isset($_GET['date_from']) && $_GET['date_from']!="")	$date_from = $_GET['date_from'];
		if(isset($_GET['date_to']) && $_GET['date_to']!="")	$date_to = $_GET['date_to'];
		if(isset($_GET['user_id']) && $_GET['user_id'])	$user_id = $_GET['user_id'];
?>
<script>
stackedMultiples('#leads_bycar', 900, "<?php echo base_url();?>report/d3_bars_hierarchical_cars3/<?php echo $user_id."/".$date_from."/".$date_to; ?>");
</script>
</div>

<div style="clear:both;"></div>
<br><br><br>

<script type="text/javascript">
// Bar chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawBarCP);
// Chart settings
function drawBarCP() {

    // Data
    var data = google.visualization.arrayToDataTable([
        ['GCarType', 'Purcahsed %', { role: 'annotation' }, {role: 'tooltip'}, {role: 'style'}],
		
        /*['Car1', 20, 20, '#FF9900' ,10, 10, '#3366CC' ,5, 5, '#26A69A'],
        ['Car2', 40, 40, '#FF9900' ,5, 5, '#3366CC' ,2, 2, '#26A69A'],
        ['Car3', 10, 10, '#FF9900' ,10, 10, '#3366CC' ,0, 0, '#26A69A'],
        ['Car4', 30, 30, '#FF9900' , 8, 8, '#3366CC' ,1, 1, '#26A69A']*/

		<?php
		if($carsConversions3)
		{
			foreach($carsConversions3 as $carC)
			{
				if($carC->requests_count==0) $carC->requests_count=1;
				echo "['".$carC->name."', ".round((($carC->purchased_count/$carC->requests_count)*100),2).", '".round((($carC->purchased_count/$carC->requests_count)*100),2)."%', '# Purchased: ".($carC->purchased_count)."', '#109618'],";
			}
		}else
		{
			echo "[0,0,0,'']"; 
		}
		?>

    ]);

    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Leads to Purchased Progress',			
		height: 400,
        fontSize: 12,
		colors: ['#109618', '#3366CC', '#FF9900'],
        chartArea: {
            left: '15%',
            width: '70%',            			
			height: 350			
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    // Draw chart
    var bar = new google.visualization.BarChart($('#car_purc_conv')[0]);
    bar.draw(data, options_bar);

}
</script>

<div id="car_purc_conv" style="text-align:center; width: 95%; height: 400px;">
<!--Purchased graph here-->

</div>

</div>



<?php 
$carcheight = "200px";
if(isset($carsConversions))
{ 
	$carcheight = "900px";
}
?>
	
	
<div id="car_conversions" style="text-align:center; width: 50%; height: <?php echo $carcheight;?>; float:right"></div>		

<div style="clear:both;"></div>

<br><br><br><br>

<!--<div id="car_purchased" style="text-align:center; width: 50%; height: 300px;"></div>-->

<div style="clear:both;"></div>



<script type="text/javascript">
// Bar chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawBar);


// Chart settings
function drawBar() {

    // Data
    var data = google.visualization.arrayToDataTable([
        /*['GCarType', 'Scheduled TD %', { role: 'annotation' }, {role: 'tooltip'}, {role: 'style'}, 'Completed TD %', { role: 'annotation' }, {role: 'tooltip'} , {role: 'style'}, 'Purcahsed %', { role: 'annotation' }, {role: 'tooltip'}, {role: 'style'}],*/
		
		['GCarType', 'Scheduled TD %', { role: 'annotation' }, {role: 'tooltip'}, {role: 'style'}, 'Completed TD %', { role: 'annotation' }, {role: 'tooltip'} , {role: 'style'}],
		
        /*['Car1', 20, 20, '#FF9900' ,10, 10, '#3366CC' ,5, 5, '#26A69A'],
        ['Car2', 40, 40, '#FF9900' ,5, 5, '#3366CC' ,2, 2, '#26A69A'],
        ['Car3', 10, 10, '#FF9900' ,10, 10, '#3366CC' ,0, 0, '#26A69A'],
        ['Car4', 30, 30, '#FF9900' , 8, 8, '#3366CC' ,1, 1, '#26A69A']*/

		<?php
		if($carsConversions)
		{
			foreach($carsConversions as $carC)
			{
				//echo "['".$carC->name."', ".round((($carC->scheduled_count/$carC->requests_count)*100),2).", '".round((($carC->scheduled_count/$carC->requests_count)*100),2)."%', '# Scheduled: ".($carC->scheduled_count)."', '#FF9900' , ".round((($carC->completed_count/$carC->requests_count)*100),2).", '".round((($carC->completed_count/$carC->requests_count)*100),2)."%', '# Completed: ".($carC->completed_count)."', '#3366CC', ".round((($carC->purchased_count/$carC->requests_count)*100),2).", '".round((($carC->purchased_count/$carC->requests_count)*100),2)."%', '# Purchased: ".($carC->purchased_count)."', '#109618'],";
				
				echo "['".$carC->name."', ".round((($carC->scheduled_count/$carC->requests_count)*100),2).", '".round((($carC->scheduled_count/$carC->requests_count)*100),2)."%', '# Scheduled: ".($carC->scheduled_count)."', '#FF9900' , ".round((($carC->completed_count/$carC->requests_count)*100),2).", '".round((($carC->completed_count/$carC->requests_count)*100),2)."%', '# Completed: ".($carC->completed_count)."', '#3366CC'],";
			}
		}else
		{
			echo "['', 0,0,0,'',0,0,0,'',0,0,0,'']"; 
		}
		?>

    ]);

    // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Leads to Test Drives Progress',	
		<?php if($carsConversions)
		{ ?>
		height: 900,        
		<?php }else{ ?>
		height: 200,
		<?php } ?>
        fontSize: 12,
		colors: ['#FF9900', '#3366CC', '#109618'],
        chartArea: {
            left: '15%',
            width: '70%',            
			<?php if($carsConversions)
			{ ?>
			height: 800       
			<?php }else{ ?>
			height: 100
			<?php } ?>
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend: {
            position: 'bottom',
            alignment: 'center',
            textStyle: {
                fontSize: 12
            }
        }
    };

    // Draw chart
    var bar = new google.visualization.BarChart($('#car_conversions')[0]);
    bar.draw(data, options_bar);

}

// Bar chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawBar61);


// Chart settings
function drawBar61() {

    // Data
    var data = google.visualization.arrayToDataTable([
        //['Satisfaction Score', 'Score', {role: 'tooltip'}, {role: 'style'}, { role: 'annotation' } ],
		['Vehicle', 'Purchased', { role: 'annotation' } ],
        /*['name1',  1000,      400],
        ['name2',  1170,      460],
        ['name3',  660,       1120],
        ['name4',  1030,      540]*/
		
		
		<?php
		if($carsConversions2)
		{
			foreach($carsConversions2 as $carC)
			{
				echo "['".$carC->name."', ".$carC->purchased_count.",".$carC->purchased_count."],";
			}
		}else
		{
			echo "['', 0,0],";
		}
		?>
				
    ]);


     // Options
    var options_bar = {
        fontName: 'Arial',
		title: 'Purchased Vehicles %',		
		height: 300,        
        fontSize: 12,
		colors: ['#FF9900', '#3366CC', '#3366CC'],
        chartArea: {
            left: '15%',
            width: '80%',
            height: 200
        },
        tooltip: {
            textStyle: {
                fontName: 'Arial',
                fontSize: 13
            }
        },
        vAxis: {
            gridlines:{
                color: '#e5e5e5',
                count: 10
            },
            minValue: 0
        },
        legend:'none'
    };

    // Draw chart
    //var bar = new google.visualization.BarChart($('#car_purchased')[0]);
    //bar.draw(data, options_bar);

}
</script>


<div style="clear:both;"></div>

<br><br><br>
<div id="leads_conversions" style="display: block; margin: 0 auto; width: 50%; height: 250px; float:left">

<b><u>Conversions</u></b>
<br><br>
<?php if($countUserLeads){ ?>

<style>
.ircd {
    float: left;
    width: 48%;
    text-align: center;
    background-color: green;
    color: #fff;
    margin: 0 0 10px;
    padding: 15px 0;
}
p.c_rating {
	color: #fff;
    font-size: 30px;
    font-weight: bold;
    margin: 10px 0 0;
}
.ircd.one {
background: #ffa84c; /* Old browsers */
background: -moz-linear-gradient(top, #ffa84c 0%, #ff7b0d 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #ffa84c 0%,#ff7b0d 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #ffa84c 0%,#ff7b0d 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffa84c', endColorstr='#ff7b0d',GradientType=0 ); /* IE6-9 */
	
}
.ircd.two {
float:right;
background: #63b6db; /* Old browsers */
background: -moz-linear-gradient(top, #63b6db 0%, #309dcf 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #63b6db 0%,#309dcf 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #63b6db 0%,#309dcf 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#63b6db', endColorstr='#309dcf',GradientType=0 ); /* IE6-9 */
}
.ircd.three {
background: #a4b357; /* Old browsers */
background: -moz-linear-gradient(top, #a4b357 0%, #75890c 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #a4b357 0%,#75890c 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #a4b357 0%,#75890c 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a4b357', endColorstr='#75890c',GradientType=0 ); /* IE6-9 */
}
.ircd.four {
float:right;
background: #f3e2c7; /* Old browsers */
background: -moz-linear-gradient(top,  #f3e2c7 0%, #c19e67 50%, #b68d4c 100%, #e9d4b3 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top,  #f3e2c7 0%,#c19e67 50%,#b68d4c 100%,#e9d4b3 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom,  #f3e2c7 0%,#c19e67 50%,#b68d4c 100%,#e9d4b3 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f3e2c7', endColorstr='#e9d4b3',GradientType=0 ); /* IE6-9 */

}
</style>



<div class="ircd one">
	<b>Scheduled Test Drives %</b><br> 
	(Scheduled / Leads ) (<?php echo $countUserScheduled; ?> / <?php echo $countUserLeads; ?>)
	<p class="c_rating"><?php echo round(($countUserScheduled/$countUserLeads)*100,2); ?> %</p>
</div>


<div class="ircd two">
	<b>Completed Test Drives %</b><br>
	(Completed TD / Leads ) (<?php echo $countUserCompleted; ?> / <?php echo $countUserLeads; ?>)
	<p class="c_rating"><?php echo round(($countUserCompleted/$countUserLeads)*100,2); ?> %</p>
</div>

<div class="ircd three">
	<b>Purchased Vehicles %</b><br>
	(Purchased Vehicles / Leads) (<?php echo $countUserPurchased; ?> / <?php echo $countUserLeads; ?>)
	<p class="c_rating"><?php echo round(($countUserPurchased/$countUserLeads)*100,2); ?> %</p>
</div>

<div class="ircd four">
	<b>Lost Sales %</b><br>
	(Lost Leads / Leads) (<?php echo $countUserLost; ?> / <?php echo $countUserLeads; ?>)
	<p class="c_rating"><?php echo round(($countUserLost/$countUserLeads)*100,2); ?> %</p>
</div>








<?php }else { echo "<br><br>No Data";} ?>

</div>

<script>
     google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawVisualization);


      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
         //['Month', 'Score', { role: 'annotation' }, {role: 'tooltip'}],
		 ['Month', 'Score', { role: 'annotation' }, {role: 'tooltip'}, 'Progress', { role: 'annotation' }, {role: 'tooltip'}],
         /*['2004/05',  162,  165 ],
         ['2005/06',  132,  135],
         ['2006/07',  154,   157],
         ['2007/08',  136,    139],
         ['2008/09',  133,    136]*/
		 
		 <?php 
		if($uGraphGradeAvgScoreYearly)  
		{
			
			if(isset($uGraphGradeAvgScoreYearly[$_GET['user_id']])) 
			{ 
				
				if(isset($_GET['date_from']) && $_GET['date_from']!="")
					$start = $month = strtotime($_GET['date_from']);
				else
					$start = $month = strtotime('2016-7-01'); //July 2016

				if(isset($_GET['date_to']) && $_GET['date_to']!="")
					$end = strtotime($_GET['date_to']);
				else
					$end = strtotime(date("Y-m-d"));

				$lcc = 0;
				$prevPerc = 0;
				while($month <= $end)
				{
					 $key = date('M Y', $month);
					 
					 if($lcc>0) echo ",";
					 echo "[";
					 
					 echo "'".date('M Y', $month)."'";

					 if(isset($uGraphGradeAvgScoreYearly[$_GET['user_id']][$key])) 
					 {
						 $val = $uGraphGradeAvgScoreYearly[$_GET['user_id']][$key]['actual_score'];
						 $val2 = $uGraphGradeAvgScoreYearly[$_GET['user_id']][$key]['surveys_completed'];
						 //echo '&nbsp; | &nbsp;'.$key.' <b>['.round($val,2).'</b>]'; 
						 $gradP = round( (($val/($val2*5))*100) ,2);
						 echo ",".$gradP;
						 //echo ',"'.$gradP.'%"';
						 echo ',""';
						 
						 echo ',"Score: '.$gradP.' %"';						
						
 						 if($lcc>0)
							echo ",".($gradP-$prevPerc).",'".($gradP-$prevPerc)." %','Progress: ".($gradP-$prevPerc)." %'";
						 else
							echo ",0,'0','0'";
						
						 $prevPerc = $gradP;
					 }
					 else 
					 {
						 // for missing months, 
						 if($lcc>0)
							echo ',0,"0","'.(0-$prevPerc).' %"';
						 else
							echo ',0,"0",""';
						
						echo ",0,'0','0'";
					 }


					 $month = strtotime("+1 month", $month);
					 
					 echo "]";
					$lcc++;
				}
				
			}  
		}else {
			echo "['2016', 0 , 0, 0]";
		} 
	?>
		 
      ]);

    var options = {
      title : 'Score Grade',
      vAxis: {title: 'Grades %'},      
      seriesType: 'bars',
      series: {1: {type: 'line'},},
      bar: {
		groupWidth: 20
	  },
	  legend: {
        position: 'none' 
      }	  
    };

    var chart = new google.visualization.ComboChart(document.getElementById('overall_user_score'));
    chart.draw(data, options);
  }
 </script>

<div id="overall_user_score" style="text-align:center; width: 50%; height: 300px; float:right"></div>

<div style="clear:both;"></div>

<br>
<br>
<br>
<br>
<u>User vs Department</u>
<br>

<div class="panel-body">
	<div class="chart-container">
		<div class="chart has-fixed-height" style="height:400px; width:50%; float:left;" id="radar_basic"></div>
	</div>
</div>

<?php 
}
?>









				
				<!--<div class="row">
					<div class="col-md-4">
                    	<ul class="categoryReport">
						<li class="heading">Heading here</li> 
							
                        	<li class="category_no active"><a href="javascript:void(0);">show 1<i class="fa fa-caret-right"></i></a></li> 

							<li class="category_no"><a href="javascript:void(0);">show 2<i class="fa fa-caret-right"></i></a></li>

							<li class="category_no"><a href="javascript:void(0);">show 3<i class="fa fa-caret-right"></i></a></li>
							
							<li class="category_no"><a href="javascript:void(0);">show 4<i class="fa fa-caret-right"></i></a></li>
							
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-2">&nbsp;</div>
                    <div class="col-md-6">
                        <ul id="CategoriesGrapf">						
							<li>1</li>
							<li style="display:none;">2</li>
							<li style="display:none;">3</li>
							<li style="display:none;">4</li>
                        </ul>
					</div>  
                </div>
				
                <h3>Graphs</h3>
                <div class="row">
				    <div class="col-md-12"></div>                   					
                </div>-->
             

            </div>            
        </div>
    </div>
</section>


<script type="text/javascript">

/* ------------------------------------------------------------------------------
 *  # Echarts - columns and waterfalls
 * ---------------------------------------------------------------------------- */

$(function () {

    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: 'assets/js/js_charts/plugins/visualization/echarts'
        }
    });


    // Configuration
    // ------------------------------

	//http://demo.interface.club/limitless/layout_1/LTR/default/assets/js/plugins/visualization/echarts/chart/funnel.js
	///public_html/mb/assets/js/js_charts/plugins/visualization/echarts/chart/bar.js

    require(
        [
            'echarts',
            'echarts/theme/limitless',
            'echarts/chart/bar',
            'echarts/chart/line',
			'echarts/chart/pie',
            'echarts/chart/funnel',
			'echarts/chart/scatter',
  		    'echarts/chart/k',
			'echarts/chart/radar',
			'echarts/chart/gauge'
        ],


        // Charts setup
        function (ec, limitless) {


            // Initialize charts
            // ------------------------------

            <?php 
			if(!isset($filter_individual_report))
			{
			?>

			<?php 
			if(!isset($filter_dep_report))
			{
			?>
			var stacked_clustered_columns = ec.init(document.getElementById('stacked_clustered_columns'), limitless);
			var stacked_clustered_columns2 = ec.init(document.getElementById('stacked_clustered_columns2'), limitless);
			<?php 
			}
			?>
			//var nested_pie = ec.init(document.getElementById('nested_pie'), limitless);
			<?php }else{ ?>

			var radar_basic = ec.init(document.getElementById('radar_basic'), limitless);

			<?php } ?>

            // Charts setup
            // ------------------------------


            <?php 
			if(!isset($filter_individual_report))
			{
			?>

			<?php 
			if(!isset($filter_dep_report))
			{
			?>
            stacked_clustered_columns_options = {

                // Setup grid
                grid: {
                    x: 65,
                    x2: 20,
                    y: 40,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legends
                /*legend: {
                    data: [
                        'Version 1.7 - 2k data','Version 1.7 - 2w data','Version 1.7 - 20w data','',
                        'Version 2.0 - 2k data','Version 2.0 - 2w data','Version 2.0 - 20w data'
                    ]
                },*/

                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [
                    {
                        type: 'category',
                        //data: ['Line','Bar','Scatter','Pies','Map']
						data: [<?php echo $carNames; ?>]						
                    },
                    {
                        type: 'category',
                        axisLine: {show:false},
                        axisTick: {show:false},
                        axisLabel: {show:false},
                        splitArea: {show:false},
                        splitLine: {show:false},
                        //data: ['Line','Bar','Scatter','Pies','Map']
						data: [<?php echo $carNames; ?>]	
                    }
                ],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {formatter:'{value} %'},
                    axisLine: {
                        lineStyle: {
                            color: '#dc143c'
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Completed %',
                        type: 'bar',
                        itemStyle: {
                            normal: {
                                color: '#2196F3',
                                label: {
                                    show: true,
                                    textStyle: {
                                        color: '#2196F3'
                                    },
									/*formatter: function(p) {
										return (p.name +'\n' );
									}*/
									formatter: function (params) {
                                        for (var i = 0, l = stacked_clustered_columns_options.xAxis[0].data.length; i < l; i++) {
                                            if (stacked_clustered_columns_options.xAxis[0].data[i] == params.name) {
                                                return params.value + "%\n" +actualValues[i];
                                            }
                                        }
                                    }
                                }
                            },
                            emphasis: {
                                label: {
                                    show: true									
                                }
                            }
                        },
                        //data: [247, 187, 95, 175, 270, 247, 187, 95, 175, 270, 10, 680, 819, 564, 724, 890]
						data: [<?php echo $completedPerc; ?>]	
                    }/*,
                    {
                        name: 'Completed',
                        type: 'bar',
                        xAxisIndex: 1,
                        itemStyle: {
                            normal: {
                                color: '#64B5F6',
                                label: {
                                    show: true,
                                    formatter: function(p) {return p.value > 0 ? (p.value +'\n'):'';}
                                }
                            },
                            emphasis: {
                                label: {
                                    show: true
                                }
                            }
                        },
                        //data: [680, 819, 564, 724, 890, 680, 819, 564, 724, 890, 25, 980, 919, 664, 824, 990]
						data: [<?php echo $completedPerc; ?>]						 
                    }*/
                ]
            };

		stacked_clustered_columns_options2 = {

                // Setup grid
                grid: {
                    x: 65,
                    x2: 20,
                    y: 40,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legends
                /*legend: {
                    data: [
                        'Version 1.7 - 2k data','Version 1.7 - 2w data','Version 1.7 - 20w data','',
                        'Version 2.0 - 2k data','Version 2.0 - 2w data','Version 2.0 - 20w data'
                    ]
                },*/

                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [
                    {
                        type: 'category',
                        //data: ['Line','Bar','Scatter','Pies','Map']
						data: [<?php echo $carNames2; ?>]						
                    },
                    {
                        type: 'category',
                        axisLine: {show:false},
                        axisTick: {show:false},
                        axisLabel: {show:false},
                        splitArea: {show:false},
                        splitLine: {show:false},
                        //data: ['Line','Bar','Scatter','Pies','Map']
						data: [<?php echo $carNames2; ?>]	
                    }
                ],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {formatter:'{value} %'},
                    axisLine: {
                        lineStyle: {
                            color: '#dc143c'
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Purchased %',
                        type: 'bar',
                        itemStyle: {
                            normal: {
                                color: '#2196F3',
                                label: {
                                    show: true,
                                    textStyle: {
                                        color: '#2196F3'
                                    },
									/*formatter: function(p) {
										return (p.name +'\n' );
									}*/
									formatter: function (params) {
                                        for (var i = 0, l = stacked_clustered_columns_options2.xAxis[0].data.length; i < l; i++) {
                                            if (stacked_clustered_columns_options2.xAxis[0].data[i] == params.name) {
                                                return params.value + "%\n" +actualValues2[i];
                                            }
                                        }
                                    }
                                }
                            },
                            emphasis: {
                                label: {
                                    show: true									
                                }
                            }
                        },
                        //data: [247, 187, 95, 175, 270, 247, 187, 95, 175, 270, 10, 680, 819, 564, 724, 890]
						data: [<?php echo $completedPerc2; ?>]	
                    }/*,
                    {
                        name: 'Completed',
                        type: 'bar',
                        xAxisIndex: 1,
                        itemStyle: {
                            normal: {
                                color: '#64B5F6',
                                label: {
                                    show: true,
                                    formatter: function(p) {return p.value > 0 ? (p.value +'\n'):'';}
                                }
                            },
                            emphasis: {
                                label: {
                                    show: true
                                }
                            }
                        },
                        //data: [680, 819, 564, 724, 890, 680, 819, 564, 724, 890, 25, 980, 919, 664, 824, 990]
						data: [<?php echo $completedPerc; ?>]						 
                    }*/
                ]
            };

			<?php 
			}
			?>

			//
            // Nested pie charts options
            //



           /* nested_pie_options = {

                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },
             

                // Enable drag recalculate
                calculable: false,

                // Add series
                series: [

                    // Inner
                    {
                        name: 'Survey Score',
                        type: 'pie',
                        selectedMode: 'single',
						<?php 
						if(!isset($filter_dep_report))
						{
						?>						
						radius: [0, '40%'],
						<?php 
						}else{
						?>
						radius: [0, '30%'],
						<?php 
						}
						?>                        
                        // for funnel
                        x: '15%',
                        y: '7.5%',
                        width: '40%',
						<?php 
						if(!isset($filter_dep_report))
						{
						?>						
						height: '85%',
						<?php 
						}else{
						?>
						height: '65%',
						<?php 
						}
						?>                         
                        funnelAlign: 'right',
                        max: 1548,

                        itemStyle: {
                            normal: {
                                label: {
                                    position: 'inner'
                                },
                                labelLine: {
                                    show: false
                                }
                            },
                            emphasis: {
                                label: {
                                    show: true
                                }
                            }
                        },

                        data: [
                            //{value: 535, name: 'Italy'},
                            //{value: 679, name: 'Spain'},
                            //{value: 1548, name: 'Austria'}
							<?php echo $depScore; ?>
                        ]
                    },

                    // Outer
                    {
                        name: 'User Survey Score',
                        type: 'pie',
						<?php 
						if(!isset($filter_dep_report))
						{
						?>						
						radius: ['60%', '85%'],
						<?php 
						}else{
						?>
						radius: ['40%', '65%'],
						<?php 
						}
						?>  
                        

                        // for funnel
                        x: '55%',
                        y: '7.5%',
                        width: '35%',
                        <?php 
						if(!isset($filter_dep_report))
						{
						?>						
						height: '85%',
						<?php 
						}else{
						?>
						height: '65%',
						<?php 
						}
						?>
                        funnelAlign: 'left',
                        max: 1048,

                        data: [
                            //{value: 535, name: 'Italy'},

                            //{value: 310, name: 'Germany'},
                            //{value: 234, name: 'Poland'},
                            //{value: 135, name: 'Denmark'},

                            //{value: 948, name: 'Hungary'},
                            //{value: 251, name: 'Portugal'},
                            //{value: 147, name: 'France'},
                            //{value: 202, name: 'Netherlands'}
							<?php echo $depUsersScore; ?>
                        ]
                    }
                ]
            };*/

			<?php }else{ ?>
			//
            // Basic radar chart
            //

            radar_basic_options = {

                // Add title
                title: {
                    text: '',
                    subtext: '',
                    x: 'right'
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                legend: {
                    orient: 'horizontal',
                    x: 'left',
                    data: ['User','Department']
                },

                // Setup polar coordinates
                polar: [{
                    radius: '60%',
                    indicator: [
                        {text: 'First Call Avg Hours', max: <?php if($depRedar['firstCall']>$userRedar['firstCall'])echo $depRedar['firstCall']; else echo $userRedar['firstCall'];?>},
                        {text: 'Finish Leads Avg Hours', max: <?php if($depRedar['finishLeads']>$userRedar['finishLeads'])echo $depRedar['finishLeads']; else echo $userRedar['finishLeads'];?>},
                        {text: 'Disapproved Count', max: <?php if($depRedar['disapproved']>$userRedar['disapproved'])echo $depRedar['disapproved']; else echo $userRedar['disapproved'];?>},
                        {text: 'Score Avg', max: <?php if($depRedar['score']>$userRedar['score'])echo $depRedar['score']; else echo $userRedar['score'];?>},
                        {text: 'Test Drive Count', max: <?php if($depRedar['testDrive']>$userRedar['testDrive'])echo $depRedar['testDrive']; else echo $userRedar['testDrive'];?>},
                        {text: 'Sales Count', max: <?php if($depRedar['sales']>$userRedar['sales'])echo $depRedar['sales']; else echo $userRedar['sales'];?>}
                    ]
                }],

                // Enable drag recalculate
                calculable: false,

                // Add series
                series: [{
                    name: 'User vs Department',
                    type: 'radar',
                    data: [
                        {
                            //value: [5, 20, 10, 2.5, 5, 0],
							value: [<?php echo $userRedar['firstCall'].",".$userRedar['finishLeads'].",".$userRedar['disapproved'].",".$userRedar['score'].",".$userRedar['testDrive'].",".$userRedar['sales']; ?>],							
                            name: 'User'
                        },
                        {
                            //value: [10, 50, 25, 5, 7, 50],
							value: [<?php echo $depRedar['firstCall'].",".$depRedar['finishLeads'].",".$depRedar['disapproved'].",".$depRedar['score'].",".$depRedar['testDrive'].",".$depRedar['sales']; ?>],
                            name: 'Department'
                        }
                    ]
                }]
            };

			<?php } ?>



            // Apply options
            // ------------------------------

			<?php 
			if(!isset($filter_individual_report))
			{
			?>

			<?php 
			if(!isset($filter_dep_report))
			{
			?>
            stacked_clustered_columns.setOption(stacked_clustered_columns_options);
			stacked_clustered_columns2.setOption(stacked_clustered_columns_options2);
			<?php 
			}
			?>

			//nested_pie.setOption(nested_pie_options);
			<?php }else{ ?>

			radar_basic.setOption(radar_basic_options);
			<?php } ?>

            // Resize charts
            // ------------------------------

            window.onresize = function () {
                setTimeout(function () {
					<?php 
					if(!isset($filter_individual_report))
					{
					?>
					<?php 
					if(!isset($filter_dep_report))
					{
					?>
                    stacked_clustered_columns.resize();
					stacked_clustered_columns2.resize();
					<?php 
					}
					?>
					//nested_pie.resize();
					<?php }else{ ?>
					radar_basic.resize();
					<?php } ?>

                }, 200);
            }
        }
    );
});


</script>

<?php
function getUserPercCFDLeads($uId,$usersNumOfClosedLeads,$usersNumOfFinishedButNotClosedLeads,$numOfDisapprovedActionEachUser)
{
	$closedCount = 0;
	$finishedCount = 0;
	$disapprovedCount = 0;
	if($usersNumOfClosedLeads)
	{
		foreach ($usersNumOfClosedLeads as $key => $row) 
		{
		   if ((int)$row->userid === (int)$uId) 
		   {
			   $closedCount = $row->num;
		   }
	   }
	}

	if($usersNumOfFinishedButNotClosedLeads)
	{
		foreach ($usersNumOfFinishedButNotClosedLeads as $key => $row) 
		{
		   if ((int)$row->userid === (int)$uId) 
		   {
			   $finishedCount = $row->num;
		   }
	   }
	}	

	if($numOfDisapprovedActionEachUser)
	{
		foreach ($numOfDisapprovedActionEachUser as $key => $row) 
		{
		   if ((int)$row->userid === (int)$uId) 
		   {
			   $disapprovedCount = $row->num;
		   }
	   }
	}
	
	return $closedCount.",".$disapprovedCount.",".$finishedCount; 
}

	function secondsToDays($seconds)
	{
		
		$text = 0;
				
		$days    = floor($seconds / 86400);
		$hours   = floor(($seconds - ($days * 86400)) / 3600);
		$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);			

		if($days>0)
			$text =  $days." Days ";
		
			//$text =  $days." Days ". $hours." Hrs. ".$minutes." Min.";
		/*elseif($hours>0)
			$text =  $hours." Hrs. ".$minutes." Min.";
		elseif($minutes>0)
			$text = $minutes." Min.";*/

		return $text;

	}
	
?>