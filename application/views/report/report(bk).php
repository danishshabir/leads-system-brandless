<style>
.reportPageN {}
.reportPageN #chart_div {overflow:hidden;}
.reportPageN #chart_div > div {margin-left:-90px; margin-top:-25px;}
/*
#chart_div svg text,
#donutVehicleChart svg text,
#donutVehicleChart1 svg text,
#donutVehicleChart2 svg text,
#donutVehicleChart3 svg text,
#donutchart svg text, 
#donutCatDirChart svg text,
#donutCatChart svg text, 
#donutCompChart svg text
*/
.reportPageN svg text {
    fill: #202020 !important;
    font-family: 'corporate_slight'!important;
    font-weight: normal;
    font-size: 14px !important;
}
#donutVehicleChart1 div[dir="ltr"] {width:100% !important;}
#donutVehicleChart1 div svg[width="400"] {width:100% !important;}
#donutVehicleChart1 div svg[width="400"] rect {width:100% !important;}
</style>

<section>
	<div class="container">
    	<div class="leadsListingSec">
        	<div class="reportSec dropdown">
                <button id="dLabel"><i aria-hidden="true" class="fa fa-gear"></i></button>
                <div class="dropdown-menu">
                    <form action="" class="mb_form">
                        <h5>Reports</h5>
                        <label>Please select option to change the Report</label>
                        <select class="centerList" onchange="redirect();" name="category_id" id="category_id">
						   <option value = ""> Select Category</option>
							<?php
							if(!empty($categories))
							{
								foreach($categories as $category)
								{?>
									<option value = "<?php echo $category->id; ?>" <?php if(isset($_GET['category_id'])) {echo ($_GET['category_id'] == $category->id ? 'Selected' : '');}?>><?php echo $category->title; ?></option>
						<?php	}
							}
							
						?>
							
					
						</select>
                        <select class="centerList" onchange="redirect();" name="user_id" id="user_id">
						   <option value = ""> Select User</option>
							<?php
							if(!empty($users))
							{
								foreach($users as $user)
								{?>
									<option value = "<?php echo $user->id; ?>" <?php if(isset($_GET['user_id'] )) {echo ($_GET['user_id']  == $user->id ? 'Selected' : '');}?>><?php echo $user->full_name; ?></option>
						<?php	}
							}
							
						?>
							
					
						</select>
                    </form>
                </div>
            </div>
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo menu(); ?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-4">
                        <h1><i class="sprite sprite-reports"></i> Reports</h1>
                    </div>
					<div class="col-sm-4">
						<style>
						.centerList {
							width: 100%;
							border: 0;
							padding-left: 8px;
							color: #0097D1;
							background-color: transparent;
							max-width: 280px;
							margin: 0 auto;
							float: none;
							display: inherit;
						}
						</style>
						
					</div>
					
                    <div class="col-sm-4">
					
					
					</div>
               </div>            	
            </div>
            <div class="createNewLeadSec reportPageN">
            
				<div class="reportPageNTOP">
                	<div class="row">
					  
                        <div class="col-md-4 col-sm-3 col-xs-12">
                           <?php if(rights('16','read')){ ?>  <h4>Overview</h4> <?php } ?>
                        </div>
					  
                        <div class="col-md-8 col-sm-9 col-xs-12">
                            <ul>
                                <li>
                                	<div class="dropdown">
                                        <button type="button" id="showBranches">Select Branches(es) <i class="fa fa-caret-down"></i></button> <!-- aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" -->
										<form action="<?php echo base_url();?>report/reportByBranches" method="post">
                                        <ul class="dropdown-menu" id="BranchesList"><!--  aria-labelledby="dLabel" -->
                                        	<li class="dpMSubmitBtn"><input type="submit" value="Submit" class="btn standardEd" /></li>
                                            <li><input id="report_1" type="checkbox" name="report_all" value="1"><label for="report_1"><span></span>All</label></li>
                                            <?php foreach($cities as $city){ 
					                                $branches = getBranches($city->id);
                                                    $i = 1;
						                            if(!empty($branches)){
				                            	?>
                                            <li>
                                            	<input id="<?php echo $city->title; ?>" type="checkbox" value="<?php echo $city->id; ?>"><label for="<?php echo $city->title; ?>"><span></span><?php echo $city->title; ?></label>
                                                <ul>
                                                    <?php foreach($branches as $branch){?>
                                                    <li><input id="Branch_<?php echo $branch->id; ?>" type="checkbox" name="branch_id[]" value="<?php echo $branch->id; ?>"><label for="Branch_<?php echo $branch->id; ?>"><span></span><?php echo $branch->title; ?></label></li>
                                                    
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                            <?php } } ?>
                                            
                                        </ul>
										</form>
                                    </div>
                                </li>
                                <?php if(isset($compare) && $compare == 1){ ?>
                                    <li><a href="javascript:void(0);"><?php echo $lead_compare_date; ?></a></li>
                               <?php } ?>
                                
                            </ul>
                        </div>
                    </div>
                </div>
			<?php if(rights('16','read')){ ?>
                <div class="row">
				
                	<div class="col-md-8" style="position:relative;">
	                    <div  id="chart_div" title="Leads register in the system on specific dates"></div>
                        <style>
							@media only screen and (max-width: 1300px) {
								.reportPageN #chart_div > div {margin-left: -72px;}
								#datesonly {min-width: 140px !important;}
							}
						</style>
                        <div id="datesonly" style="min-width: 170px;position: absolute;right: 0;top: 17px;">
                        	<div id="date1Show" style="margin-bottom:4px;"><?php if(isset($lead_compare_date)) echo $lead_compare_date; ?></div>
                        	<div id="date2Show"><?php if(isset($leads_compare_with_date)) echo $leads_compare_with_date; ?></div>
                        </div>
                    </div>
					
                    <div class="col-md-4">
                    	<div class="reportDateRange">
                        	<form action="<?php echo base_url();?>report/index" method="post">
                                <label>Date Range</label>
                                <select onchange="graphByWeek(this.value);">
                                    <option value="">Select</option>
									<option value="Week"<?php if(isset($type )) {echo ($type == 'Week' ? 'Selected' : '');}?>>Week</option>
									<option value="Month" <?php if(isset($type )) {echo ($type == 'Month' ? 'Selected' : '');}?>>Month</option>
                                    <option value="Year" <?php if(isset($type )) {echo ($type == 'Year' ? 'Selected' : '');}?>>Year</option>
                                </select>
                                <label>Search Date</label>
                                <div class="row">
                                    <div class="col-md-6"><input type="text" id="date_to_1" class="calendar" placeholder="To" name="date_to_1" required /></div>
                                    <div class="col-md-6"><input type="text" id="date_to_2" class="calendar" placeholder="From" name="date_from_1" required /></div>
                                </div>
                                <label>Compare To</label>
                                <div class="row">
                                    <div class="col-md-6"><input type="text" id="date_to_3" class="calendar" placeholder="To" name="date_to_2" required/></div>
                                    <div class="col-md-6"><input type="text" id="date_to_4" class="calendar" placeholder="From" name="date_from_2" required/></div>
                                </div>
                                <input type="submit" class="btn standardEd" value="Generate" />
                            </form>
                        </div>
                    </div>
					
                </div>
			<?php } ?>
                <h3>Reporting</h3>
                <div class="row">
                	<div class="col-md-4">
						<h2>Number of Total Leads in the System : <?php echo $total_lead_in_the_system;?> </h2>
						<br />
                    	<ul class="categoryReport">
						<?php if(rights('17','read') || rights('17','read') || rights('17','read') || rights('17','read')){ ?>
						<li class="heading">Categories</li> <?php } ?>
							<?php if(rights('17','read')){ ?>
                        	<li class="category_no active"><a href="javascript:void(0);">Leads</a></li> <?php } ?>
                        	<?php if(rights('18','read')){ ?>
							<li class="category_no"><a href="javascript:void(0);">City</a></li><?php } ?>
                        	<?php if(rights('19','read')){ ?>
							<li class="category_no"><a href="javascript:void(0);">Cars<i class="fa fa-caret-right"></i></a></li> <?php } ?>
							
                        	<?php if(!isset($_GET['user_id'])){ ?>
							<?php if(rights('20','read')){ ?>
							<li class="category_no"><a href="javascript:void(0);">Users</a></li><?php } ?>
							<?php } ?>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-2">&nbsp;</div>
                    <div class="col-md-6">
						<style>
							   #CategoriesGrapf li {display:none;}
							   #CategoriesGrapf li:first-child {display:block;}
						  </style>
                        <ul id="CategoriesGrapf">
						<?php if(rights('17','read')){ ?>
						<li><div  id="donutVehicleChart" title="Total active leads in the system where status not equal to close"></div></li> <?php } ?>
						<?php if(rights('18','read')){ ?>
						<li><div  id="donutVehicleChart1" title="Total active leads against each city where status not equal to close" style="width:100%;"></div></li><?php } ?>
						<?php if(rights('19','read')){ ?>
						<li><div  id="donutVehicleChart2" title="Total active leads by selected cars" ></div></li><?php } ?>
						 <?php if(!isset($_GET['user_id'])){ ?>
						 <?php if(rights('20','read')){ ?>
                         <li><div  id="donutVehicleChart3" title="Total active leads in the system assign to each user"></div></li><?php } ?>
						 <?php } ?>
                        </ul>
					</div>  
                    <!--<div class="col-md-8"><img src="<?php echo base_url();?>assets/images/reportChart.jpg" alt="graph" height="407" width="796" /></div>-->
                </div>
                <h3>Graphs</h3>
                <div class="row">
				<?php if(rights('21','read')){ ?>
				    <div class="col-md-6" id="sat_chart_div" title="It is over all average of answers of each survey according to date when survey submitted"></div><?php } ?>
                    <!--<div class="col-md-6" id="donutchart"></div> -->
                <?php if(rights('22','read')){ ?>   
				<div class="col-md-6" id="donutCompChart" title="Percentage of complaints by departments"></div><?php } ?>
                </div>
                <div class="row">                  
                   <?php if(rights('23','read')){ ?>
				   <div class="col-md-6" id="rate_chart_div" title="It is average response rate from lead created to finished of each user"></div> <?php } ?>
				   <?php if(rights('24','read')){ ?>
				   <div class="col-md-6" id="donutCatChart" title="Total active leads by category where status not equal to closed"></div>  <?php } ?>
                </div>
                <div class="row">
                    <div class="col-md-6" id="donutCatDirChart" title="Total active leads by specific region where status not equal to closed"></div> 
                </div>
                <div class="row">                  
				<?php if(rights('25','read')){ ?>  
                    <div class="col-md-6" id="columnchart_material" title="Total active leads by specific region where status not equal to closed"></div>   <?php } ?>
				<?php if(rights('26','read')){ ?>  					
                    <div class="col-md-6" id="columnchartbranch" title="Total active leads by branch where status not equal to closed"></div>
				<?php } ?>
                </div>
                
                
                
               <!-- <h3>Multi Information Graph</h3>
                <div class="row">
                	<div class="col-md-12">
                    	<img src="<?php echo base_url();?>assets/images/multiInfoChart.jpg" alt="graph" height="340" width="1252" />
                    </div>
                </div> -->
                 <!--<div class="col-sm-6" id="chart_div" style="width: 280px; height: 180px;"></div>-->
            </div>            
        </div>
    </div>
</section>
<?php 

	//Line Chart
	if(rights('16','read')){
		if(isset($compare) && $compare == 1){
		$leads_counts = '';
		$chart_data = '';
		if(count($leads_compare) >= count($leads_compare_with)){
			$i=0;
			foreach($leads_compare as $lead)
			{
			//$created_date = date('d',strtotime($lead->created_at));
			$leads_counts_compare= $lead->count_leads;
			$leads_counts_compare_with= (array_key_exists($i, $leads_compare_with) ? $leads_compare_with[$i]->count_leads : '0');
			$chart_data .= "['".$i."', ".$leads_counts_compare.",".$leads_counts_compare_with."],";
			$i++; 
		   }
		}else
		{
			foreach($leads_compare_with as $lead)
			{
			//$created_date = date('d',strtotime($lead->created_at));
			$leads_counts_compare_with= $lead->count_leads;
			$leads_counts_compare= (array_key_exists($i, $leads_compare) ? $leads_compare[$i]->count_leads : '0');
			$chart_data .= "['".$i."', ".$leads_counts_compare.",".$leads_counts_compare_with."],";
			$i++;
		   }
			
		}
		  
		
		$chart_data = rtrim($chart_data,','); 
		
		}else{
		  $leads_counts = '';
		$chart_data = '';
		if($leads){
		  foreach($leads as $lead)
		{   if(isset($type) && $type == 'Year')
			{
				$created_date = date('m',strtotime($lead->created_at));
			}else
			{
				$created_date = date('M j',strtotime($lead->created_at));
			}	
			
			
			$leads_counts = $lead->count_leads;
			$chart_data .= "['".$created_date."', ".$leads_counts."],";
		}
		$chart_data = rtrim($chart_data,','); 
		}else
		{
		 $chart_data = "['0', 0]";   
		}  
		}
	
    }
	
	//Total Active Leads in System by Category
	$color_arr = array('#02ADC1','#00649F','#0287D2','#0076BC','#3DB6E3','#81DFEB');
	/*$color_count = count($color_arr);
	$pie_counts = '';
	$pie_chart_data = '';
	$color = '';
	$j=0;
    if(!empty($pie_leads))
    {
        foreach($pie_leads as $pie)
    	{
    		$j = rand(0,5);
			$chart_title = $pie->cat_title;
    		$pie_counts = $pie->cat_count;
    		$pie_chart_data .= "['".$chart_title."', ".$pie_counts."],";
    		$color .= "'".$color_arr[$j]."',";
    		$j++;
    	}        
        
            
 $j++;
	}else
	{
		$pie_chart_data = "['No Data', 0],";
    	$color = "'#02ADC1',";
	}
	$pie_chart_data = rtrim($pie_chart_data,',');
	$color = "[".rtrim($color,',')."]";
	*/
    if(rights('22','read'))
	{ 
		//complaint chart
		$comp_counts = '';
		$comp_chart_data = '';
		$comp_color = '';
		$k=0;
		if($comp_leads)
		{
			foreach($comp_leads as $comp)
		{
			$comp_chart_title = $comp->comp_type;
			$comp_counts = $comp->comp_count;
			$comp_chart_data .= "['".$comp_chart_title."', ".$comp_counts."],";
			$comp_color .= "'".$color_arr[$k]."',";
			$k++;
		}
		
			
		}else
		{
			$comp_chart_data = "['No Data', 0],";
			$comp_color = "'#02ADC1',";
		}
		$comp_chart_data = rtrim($comp_chart_data,',');
		$comp_color = "[".rtrim($comp_color,',')."]";
	}
	
	//Satisfaction Chart
	if(rights('21','read')){ 
		$sat_counts = '';
		$sat_chart_data = '';
		$sat_chart_data_val = '';
		if($satisfaction)
		{
		   foreach($satisfaction as $sat)
		{
			$sat_chart_data = $sat->date_lead;
			$sat_counts = round($sat->answers_count, 2);
			$sat_chart_data_val .= "['".$sat_chart_data."', ".$sat_counts."],";
		} 
		}else
		{
		 $sat_chart_data_val = "['0', 0]";   
		}
		
		$sat_chart_data_val = rtrim($sat_chart_data_val,',');
	}
	if(rights('23','read')){ 
		//Rate Chart
		$rate_counts = '';
		$rate_chart_data = '';
		$rate_chart_data_val = '';
		if($rate_category)
		{
		   foreach($rate_category as $rate)
		 {
			$rate_chart_data = $rate->rate_title;
			$rate_counts = round($rate->rate_avg, 2);
			$rate_chart_data_val .= "['".$rate_chart_data."', ".$rate_counts."],";
		 }
		}else
		{
			$rate_chart_data_val = "['Response time', 0],";
		}
		
		$rate_chart_data_val = rtrim($rate_chart_data_val,',');
	}
	if(rights('24','read')){ 
		//Categories chart
		$cat_counts = '';
		$cat_chart_data = '';
		$cat_chart_data_val = '';
		$cat_color = '';
		$k=0;
		if($related_category)
		{
			foreach($related_category as $cat)
		{
			$k= rand(0,5);
			$cat_chart_data = $cat->cat_type;
			$cat_counts = $cat->cat_count;
			$cat_chart_data_val .= "['".$cat_chart_data."', ".$cat_counts."],";
			$cat_color .= "'".$color_arr[$k]."',";
			$k++;
		}
			
		
		}else
		{
			$cat_chart_data_val .= "['No data', 0],";
			$cat_color .= "'#02ADC1',";
			
		}
		
			$cat_chart_data_val = rtrim($cat_chart_data_val,',');
			$cat_color = "[".rtrim($cat_color,',')."]";
	
	}
	
	//====================================================
	
	//cities Chart
	if(rights('18','read')){ 
		$cities_counts = '';
		$city_chart_data = '';
		$city_chart_data_val = '';
		if($leads_by_city)
		{
		   foreach($leads_by_city as $city)
		{
			$city_chart_data = $city->city_name;
			$city_counts = $city->city_count;
			$city_chart_data_val .= "['".$city_chart_data."', ".$city_counts."],";
		} 
		}else
		{
			$city_chart_data_val = "['No Data', 0],";
		}
		
		$city_chart_data_val = rtrim($city_chart_data_val,',');
	
	}
	
	//user Chart
	if(rights('20','read')){ 
		$user_counts = '';
		$user_chart_data = '';
		$user_chart_data_val = '';
		if($leads_by_user)
		{
		   foreach($leads_by_user as $user)
		{
			$user_chart_data = $user->user_name;
			$user_counts = $user->user_count;
			$user_chart_data_val .= "['".$user_chart_data."', ".$user_counts."],";
		} 
		}else
		{
			$user_chart_data_val = "['No Data', 0],";
		}	
		
		$user_chart_data_val = rtrim($user_chart_data_val,',');
	}
	
	//total leads
	if(rights('17','read')){
		$total_lead_counts = '';
		$total_lead_chart_data = '';
		$total_lead_chart_data_val = '';
		if($leads_totals)
		{
		   foreach($leads_totals as $total_lead)
		{
			$total_lead_chart_data = 'Total Leads';
			$total_lead_counts = $total_lead->lead_count;
			$total_lead_chart_data_val .= "['".$total_lead_chart_data."', ".$total_lead_counts."],";
		} 
		}else
		{
			$total_lead_chart_data_val = "['No Data', 0],";
		}
		
		$total_lead_chart_data_val = rtrim($total_lead_chart_data_val,',');
	}
	//====================================================
	//Vehicle Chart
	if(rights('19','read')){ 
		$veh_counts = '';
		$veh_chart_data = '';
		$veh_chart_data_val = '';
		if($vehicle_type)
		{
			$total_leads_by_cars = 0;
		   foreach($vehicle_type as $veh)
		{
			$veh_chart_data = $veh->vehicle_name;
			$veh_counts = $veh->vehicle_count;
			$total_leads_by_cars += $veh->vehicle_count;
			$veh_chart_data_val .= "['".$veh_chart_data."', ".$veh_counts."],";
			
		}
			 $total_cars =  $total_lead_in_the_system - $total_leads_by_cars;
			 $veh_chart_data_val .= "['With no cars', ".$total_cars."]";	
		}else
		{
			$veh_chart_data_val = "['No Data', 0],";
		}
		
		$veh_chart_data_val = rtrim($veh_chart_data_val,',');
	}
	
	
	//Director Categories chart
	/*$dir_counts = '';
	$dir_chart_data = '';
	$dir_chart_data_val = '';
	$dir_color = '';
	$k=0;
    if($related_category_dir)
    {
       foreach($related_category_dir as $dir)
	{
		$dir_chart_data = $dir->dir_type;
		$dir_counts = $dir->dir_count;
		$dir_chart_data_val .= "['".$dir_chart_data."', ".$dir_counts."],";
		$dir_color .= "'".$color_arr[$k]."',";
		$k++;
	} 
    }else
	{
		$dir_chart_data_val = "['No Data', 0],";
		$dir_color = "'#02ADC1',";
	}	
	
	$dir_chart_data_val = rtrim($dir_chart_data_val,',');
	$dir_color = "[".rtrim($dir_color,',')."]";
	*/
	if(rights('25','read')){ 
		//Region Chart
		$reg_counts = '';
		$reg_chart_data = '';
		$reg_chart_data_val = '';
		if($leads_by_region)
		{
		  foreach($leads_by_region as $reg)
		{
			$reg_chart_data = $reg->region_title;
			$reg_counts = $reg->region_count;
			$reg_chart_data_val .= "['".$reg_chart_data."', ".$reg_counts."],";
		}  
		}else
		{
			$reg_chart_data_val = "['No Data', 0],";
		}	
		
		$reg_chart_data_val = rtrim($reg_chart_data_val,',');
	 } 
	if(rights('26','read')){ 
		//Branch Chart
		$brn_counts = '';
		$brn_chart_data = '';
		$brn_chart_data_val = '';
		if($leads_by_branch)
		{
			foreach($leads_by_branch as $brn)
			{
				$brn_chart_data = $brn->branch_title;
				$brn_counts = $brn->branch_count;
				$brn_chart_data_val .= "['".$brn_chart_data."', ".$brn_counts."],";	
			}
		}else
		{
			$brn_chart_data_val = "['No Data', 0],";
		}	
		
		$brn_chart_data_val = rtrim($brn_chart_data_val,',');
	}
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    //Google chart
 google.charts.load('current', {packages: ['corechart']});
<?php if(rights('16','read')){ ?>
 //Line Chart
google.charts.setOnLoadCallback(drawBackgroundColor);

function drawBackgroundColor() {
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Day');
     
      <?php
        if(isset($compare) && $compare == 1)
        { ?>
             data.addColumn('number', '');
            data.addColumn('number', '');
      <?php  }else{ ?>
        data.addColumn('number', '');
     <?php          
      } 
      ?>
      data.addRows([       
		<?php echo $chart_data;?>
      ]);

      var options = {
		pointSize: 4,
        backgroundColor: '#ffffff',
		/*width:600,*/
        height:250,
		series: {
            0: { color: '#008FC9' },
             <?php
        if(isset($compare) && $compare == 1)
        { ?>
            1: { color: '#e7711b' },
          <?php  } 
      ?>
          }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }
<?php } ?>
	//Total Active Leads in System by Category
    /*  google.charts.setOnLoadCallback(drawPieChart);
      function drawPieChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
		  <?php echo $pie_chart_data;?>
        ]);

        var options = {
          title: 'Total Active Leads in System by Category',
          pieHole: 0.4,
		  /*width: 369,*/
		 /* height: 254,
		  colors: <?php echo $color;?>
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }
	  */
	   <?php if(rights('22','read')){ ?>
	  //complaint chart
	  google.charts.setOnLoadCallback(drawPieCompChart);
      function drawPieCompChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
		  <?php echo $comp_chart_data;?>
        ]);

        var options = {
          title: 'Number of Complaints by Department',
          pieHole: 0.4,
		  /*width: 369,*/
		  height: 254,
		  colors: <?php echo $comp_color;?>
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutCompChart'));
        chart.draw(data, options);
      }
	   <?php } ?> 
	  <?php if(rights('21','read')){ ?>
	  //Satisfaction Chart
	   	google.charts.setOnLoadCallback(drawSatChart);
		  function drawSatChart() {
		  
			var data = google.visualization.arrayToDataTable
				([['X', 'Avg'],
				  <?php echo $sat_chart_data_val;?>
			]);
	
			var options = {
			  hAxis: { minValue: 0, maxValue: 5, color: '#FFFFFF' },
			  pointSize: 4,
			  title: 'Satisfaction',
			  subtitle: 'Customer Services',
			  titleTextStyle: { fontSize: 16,
								color: 'white'},
			  backgroundColor: '#676767',
			  hAxis: {gridlines: {color: '#C9C9C9', count: 1, },
			  textStyle: {color: 'transparent'}
			  , ticks: [1]},
		  vAxis: {gridlines: {color: '#808080', count: 5},
		  textStyle: {
				color: '#FFFFFF',
		 
				bold: true
			  }},
		  series: {
			0:{color: '#FFFFFF'}},
			/*width: 369,*/
		  	height: 254
			  
			};
	
			var chart = new google.visualization.LineChart(document.getElementById('sat_chart_div'));
			chart.draw(data, options);
		}
	  <?php } ?>
<?php if(rights('23','read')){ ?>	  
		//Bar Chart
		google.charts.setOnLoadCallback(drawRateChart);
		function drawRateChart() {
		  var data = google.visualization.arrayToDataTable([
			["Element", "Response time Min"],
			<?php echo $rate_chart_data_val;?>
		  ]);
	
		  var view = new google.visualization.DataView(data);
	
		  var options = {
			title: "Response Rate of Leads By Each User",
			/*width: 475,*/
			height: 300,
			bar: {groupWidth: "95%"},
			colors: ['#007FD0', '#0195DB']
			//legend: { position: "none" },
		  };
		  var chart = new google.visualization.BarChart(document.getElementById("rate_chart_div"));
		  chart.draw(view, options);
	  }
<?php } ?>	
<?php if(rights('24','read')){ ?>  
	  //category chart
	  google.charts.setOnLoadCallback(drawPieCatChart);
      function drawPieCatChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
		  <?php echo $cat_chart_data_val;?>
        ]);

        var options = {
          title: 'Total Active Leads by Categories',
          pieHole: 0.4,
		 /* width: 369,*/
		  height: 254,
		  colors: <?php echo $cat_color;?>
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutCatChart'));
        chart.draw(data, options);
      }
<?php } ?>	  
	  <?php if(rights('19','read')){ ?>
	  //Vehicle Chart
		google.charts.setOnLoadCallback(drawVehChart);
		function drawVehChart() {
		  var data = google.visualization.arrayToDataTable([
			["Element", "Vehicles"],
			<?php echo $veh_chart_data_val;?>
		  ]);
	
		  var view = new google.visualization.DataView(data);
	
		  var options = {
			title: "Total Active Leads by Vehicle Type",
			/*width: 600,*/
			height: 400,
			bar: {groupWidth: "95%"},
			colors: ['#007FD0', '#0195DB'],
			//legend: { position: "none" },
		  };
		  var chart = new google.visualization.BarChart(document.getElementById("donutVehicleChart2"));
		  chart.draw(view, options);
	  }
	  <?php } ?>
	  <?php if(rights('18','read')){ ?>
	  //City Chart
		google.charts.setOnLoadCallback(drawVehChartCity);
		function drawVehChartCity() {
		  var data = google.visualization.arrayToDataTable([
			["Element", "Leads"],
			<?php echo $city_chart_data_val;?>
		  ]);
	
		  var view = new google.visualization.DataView(data);
	
		  var options = {
			title: "Total Active Leads by City",
			/*width: 600,*/
			height: 400,
			bar: {groupWidth: "95%"},
			colors: ['#007FD0', '#0195DB'],
			
			//legend: { position: "none" },
		  };
		  var chart = new google.visualization.BarChart(document.getElementById("donutVehicleChart1"));
		  chart.draw(view, options);
	  }
	  <?php } ?>
	  <?php if(rights('17','read')){ ?>
	  //Total Leads
		google.charts.setOnLoadCallback(drawVehChartTotalLead);
		function drawVehChartTotalLead() {
		  var data = google.visualization.arrayToDataTable([
			["Element", "Leads"],
			<?php echo $total_lead_chart_data_val;?>
		  ]);
	
		  var view = new google.visualization.DataView(data);
	
		  var options = {
			title: "Total Active Leads",
			// width: 600,
			height: 400,
			bar: {groupWidth: "95%"},
			colors: ['#007FD0', '#0195DB'],
			//legend: { position: "none" },
		  };
		  var chart = new google.visualization.BarChart(document.getElementById("donutVehicleChart"));
		  chart.draw(view, options);
	  } 
	  <?php } ?>
	  <?php if(rights('20','read')){ ?>
	  //user Chart
		google.charts.setOnLoadCallback(drawVehChartUser);
		function drawVehChartUser() {
		  var data = google.visualization.arrayToDataTable([
			["Element", "Leads"],
			<?php echo $user_chart_data_val;?>
		  ]);
	
		  var view = new google.visualization.DataView(data);
	
		  var options = {
			title: "Total Active Leads by User",
			//width: 600,
			height: 400,
			bar: {groupWidth: "95%"},
			colors: ['#007FD0', '#0195DB'],
			//legend: { position: "none" },
		  };
		  var chart = new google.visualization.BarChart(document.getElementById("donutVehicleChart3"));
		  chart.draw(view, options);
	  }
	  <?php } ?>
	  //Director chart
	/*  google.charts.setOnLoadCallback(drawPieDirChart);
      function drawPieDirChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
		  <?php echo $dir_chart_data_val;?>
        ]);

        var options = {
          title: 'Number of Active Complaints',
          pieHole: 0.4,
		  /*width: 369,*/
		/*  height: 254,
		  colors: <?php echo $dir_color;?>
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutCatDirChart'));
        chart.draw(data, options);
      }
	  */
	  <?php if(rights('25','read')){ ?>  
	  //Region chart	
	  
	  google.charts.setOnLoadCallback(drawChart3);
    
		function drawChart3() {
		  var data = google.visualization.arrayToDataTable([
			["Element", "" ],
			<?php echo $reg_chart_data_val;?>
			
		  ]);
	
		  var view = new google.visualization.DataView(data);
		  var options = {
			title: "Total Active Leads by Region",
			titleTextStyle: {color:"#3B7CA2"},
			/*width: 475,*/
			height: 300,
			bar: {groupWidth: "85%"},
			legend: { position: "none" },
		  };
		  var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_material"));
		  chart.draw(view, options);
	  }
	  <?php } ?>
<?php if(rights('26','read')){ ?>  	  
	  //Branch chart	  
	  google.charts.setOnLoadCallback(drawChart4);
    
		function drawChart4() {
		  var data = google.visualization.arrayToDataTable([
			["Element", "" ],
			<?php echo $brn_chart_data_val;?>
			
		  ]);
	
		  var view = new google.visualization.DataView(data);
		  var options = {
			title: "Total Active Leads by Branch",
			titleTextStyle: {color:"#3B7CA2"},
			/*width: 475,*/
			height: 300,
			bar: {groupWidth: "85%"},
			legend: { position: "none" },
		  };
		  var chart = new google.visualization.ColumnChart(document.getElementById("columnchartbranch"));
		  chart.draw(view, options);
	  }
<?php } ?>  
	  $(window).resize(function(){
		  drawBackgroundColor();
		  drawPieChart();
		  drawPieCompChart();
		  drawSatChart();
		  drawRateChart();
		  drawPieCatChart();
		  drawVehChart();
		  drawPieDirChart();
		  drawChart3();
		  drawChart4();
	  });
	 $( document ).ready(function() {
     $(function() {
    	$( "#date_to_1,#date_to_2,#date_to_3,#date_to_4").datepicker({
    	  changeMonth: true,
    	  changeYear: true,
    	  dateFormat: 'yy-mm-dd'
    	  
	});
	
});
    
            
  	   
	 }); 
  function graphByWeek(type)
  {
	  if(type == '')
	 {
		 window.location.href = '<?php echo base_url();?>report';
	 }
	  window.location.href = '<?php echo base_url();?>report/index/'+type;
  }
  /*function getReportsByCategory(type,cat_id)
  {
	 if(cat_id == '')
	 {
		  window.location.href = '<?php echo base_url();?>report';
	 }
	 window.location.href = '<?php echo base_url();?>report/index/'+type+'/'+cat_id;
  }*/
  function redirect() {

        var category_id = document.getElementById("category_id").value;
        var user_id = document.getElementById("user_id").value;
 var anyParmExists = false;
 var url = '<?php echo base_url();?>report/index';
        if(category_id !=""){
         url ='?category_id=' + category_id;
         
  anyParmExists = true;
        
        }if(user_id !="" ){
     if(anyParmExists)
  url = url + '&user_id=' + user_id;
     else
  url = url + '?user_id=' + user_id;

           
        }

         window.location.href = url;

}
  
</script>