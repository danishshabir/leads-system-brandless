<section>
	<div class="container">
    	<div class="leadsListingSec">
        	<div class="addNewLeads">
            	<div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                       <?php echo menu(); ?>
                    </ul>
                </div>
            </div>
        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i class="fa fa-building"></i> Branch Management</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>            	
            </div>
            <div class="createNewLeadSec">            	
                <ul class="systemSettingPg noBorder">
                	<li class="heading">Branches</li>
                    <?php foreach($cities as $city){ 
					       $branches = getBranches($city->id);
						   $i = 1;
						   if(!empty($branches)){
					?>
                    <li class="haveSubBranch">
                    	<a href="javascript:void(0);"><?php echo $city->title; ?></a>
                        <ul> 
                        <?php foreach($branches as $branch){?>
                        	<li id="<?php echo $branch->id;?>">
                            	<div class="row">
                                    <div class="col-md-8 col-sm-6 col-xs-12"><p><?php echo $i;?>. <?php echo $branch->title;?> </p></div>
                                    <div class="col-md-4 col-sm-6 col-xs-12 text-right">
                                    <?php if(rights(6,'update')){ ?>
                                        <a class="eidtField" data-toggle="collapse" href="#BrMang<?php echo $city->id;?>_<?php echo str_replace(" ","",$city->title); ?>_<?php echo $i;?>" aria-expanded="false" aria-controls="BrMang"><i class="fa fa-pencil"></i></a><?php } ?>
                                        <?php if(rights(6,'delete')){ ?>
                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $branch->id; ?>','branch_management/delete','')"><i class="fa fa-trash"></i></a>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="leadCatDDown roleMangSec collapse" id="BrMang<?php echo $city->id;?>_<?php echo str_replace(" ","",$city->title); ?>_<?php echo $i;?>">
                                    <form action="<?php echo base_url();?>branch_management/update" method="post">
                                    <input type="hidden" name="id" value="<?php echo $branch->id;?>"  />
                                        <ul>
                                            <li>
                                                <div class="row">
                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                        <label><?php echo $branch->title;?></label>
                                                        English: <input placeholder="Write" type="text" name="title" value="<?php echo $branch->title;?>" />
														Arabic: <input placeholder="Write" type="text" name="branch_title_ar" value="<?php echo $branch->branch_title_ar;?>" />
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                        <label>City</label>
                                                        <select name="city_id">
                                                         <?php foreach($cities as $cities_list){?>
                                                            <option value="<?php echo $cities_list->id; ?>" <?php echo ($cities_list->id == $branch->city_id ? 'selected' : '') ;?>><?php echo $cities_list->title; ?></option>
                                                          <?php } ?>   
                                                        </select>
                                                    </div>
                                                </div>
												<div class="row">
                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                        <label>merc..me.com Form's Branch</label>
                                                        <input placeholder="Write" type="text" name="title_web" value="<?php echo $branch->title_web;?>" />
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                        <label>merc..ksa.com Form's Branch</label>
														<input placeholder="Write" type="text" name="title_ar" value="<?php echo $branch->title_ar;?>" />
                                                    </div>
                                                </div>
												<div class="row">
                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                        <label>Start Time</label>
                                                        <input class="timepicker1" style="width: 50%;" placeholder="Write" type="text" value="<?php echo date("h:i a",strtotime($branch->start_time));?>" />
														<input type="hidden" name="start_time" class="time_alternate1" value="<?php echo $branch->start_time;?>" style="cursor: pointer;">
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                        <label>End Time</label>
														<input class="timepicker2" style="width: 50%;" placeholder="Write" type="text" value="<?php echo date("h:i a",strtotime($branch->end_time));?>" />
														<input type="hidden" name="end_time" class="time_alternate2" value="<?php echo $branch->end_time;?>" style="cursor: pointer;">
                                                    </div>
                                                </div>
												<div class="row">
                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                        <label>First Break Start Time</label>
                                                        <input class="timepicker3" style="width: 50%;" placeholder="Write" type="text" value="<?php echo date("h:i a",strtotime($branch->break1_start));?>" />
														<input type="hidden" name="break1_start" class="time_alternate3" value="<?php echo $branch->break1_start;?>" style="cursor: pointer;">
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                        <label>First Break End Time</label>
														<input class="timepicker4" style="width: 50%;" placeholder="Write" type="text" value="<?php echo date("h:i a",strtotime($branch->break1_end));?>" />
														<input type="hidden" name="break1_end" class="time_alternate4" value="<?php echo $branch->break1_end;?>" style="cursor: pointer;">
                                                    </div>
                                                </div>
												<div class="row">
                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                        <label>Second Break Start Time</label>
                                                        <input class="timepicker5" style="width: 50%;" placeholder="Write" type="text" value="<?php echo date("h:i a",strtotime($branch->break2_start));?>" />
														<input type="hidden" name="break2_start" class="time_alternate5" value="<?php echo $branch->break2_start;?>" style="cursor: pointer;">
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                        <label>Second Break End Time</label>
														<input class="timepicker6" style="width: 50%;" placeholder="Write" type="text" value="<?php echo date("h:i a",strtotime($branch->break2_end));?>" />
														<input type="hidden" name="break2_end" class="time_alternate6" value="<?php echo $branch->break2_end;?>" style="cursor: pointer;">
                                                    </div>
                                                </div>
												
                                            </li>
                                        </ul>
                                        <div class="saveChangSec">
                                            <input type="reset" value="Discard" data-toggle="collapse" href="#BrMang<?php echo $city->id;?>_<?php echo $city->title;?>_<?php echo $i;?>" class="btn standardEdBtn" />
                                            <input type="submit" value="Save" class="btn" />
                                        </div>
                                    </form>
                                </div>
                            </li>
                        <?php $i++; } ?>                           	
                        	
                        </ul>
                    </li>
                    <?php }
					 }
					 ?>
                    <?php if(rights(6,'write')){ ?>
                    <li class="addMore">
                    	<a data-toggle="collapse" href="#AddmoreArea"><i class="fa fa-plus"></i> Add more</a>
                        <div class="collapse leadCatDDown" id="AddmoreArea">
                        	<form action="<?php echo base_url();?>branch_management/save" method="post">
                                <input type="text" placeholder="Enter Branch Name" name="title" required>
                                <div class="CitySelectArea">
                                    <label>City</label>
                                                        <select name="city_id" required>
                                                         <?php foreach($cities as $cities_list){?>
                                                            <option value="<?php echo $cities_list->id; ?>"><?php echo $cities_list->title; ?></option>
                                                          <?php } ?>   
                                                        </select>
                                </div>
                                <input type="submit" value="Save" class="btn" />
							</form>	                                
                        </div>
                    </li>
                    <?php } ?>
                </ul>
                <div class="clearfix"></div>
            </div>            
        </div>
    </div>
</section>