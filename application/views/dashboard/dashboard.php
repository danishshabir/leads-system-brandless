<section>
	<div class="container">
    	<div class="dashboardSecMain">


			<div class="dashBordSec dropdown">
                <!--<button id="dLabel"><i class="fa fa-gear" aria-hidden="true"></i></button>-->
                <!--<div class="dropdown-menu" style="display: none;">
                    <form class="mb_form" action="">
                        <h5>Graphs</h5>
                        <label>Please select option to change the graph</label>
                        <select class="dashboardSelctEd" onchange="graphsBy(this.value);">
                            <option value="assign" <?php //if($check_for_fetching_leads == 'assign') { echo 'selected'; } ?>>Graphs by leads assign to me</option>
                            <option  value="creator" <?php //if($check_for_fetching_leads == 'creator') { echo 'selected'; } ?>>Graphs by leads created by me</option>
                            <?php //if($this->session->userdata['user']['is_branch_manager'] == 1){ ?>
                            <option  value="manager" <?php //if($check_for_fetching_leads == 'manager') { echo 'selected'; } ?>>Graphs by leads of users under me</option>	
                            <?php //} ?>                            
                       </select>
                    </form>
                </div>-->
            </div>
           <?php $menu = menu(); 
		   if($menu)
		   {
		   ?>
		   <div class="addNewLeads">
            <div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo $menu; ?>
                    </ul>
                </div>
            </div>
			<?php } ?>


        	<div class="row">
            	<div class="col-md-8 col-sm-12 col-xs-12">
                	<div class="row marginBottom4Px">
                    	<!--<div class="col-md-6" id="sat_chart_div">-->
						<div class="col-md-6">
						
						
							<div class="panel panel-flat">
								<div class="panel-heading newCusHead" style="position:static;">
									<h5 class="panel-title" title='Including Connected Leads'>Satisfaction Score</h5>
								</div>

								<div class="panel-body">
									<div class="dashBordTextChart">
										<?php
										/*if($managerView)
										{
											echo "<label>Your Branch Average </label><span>".$totalSatisfactionBranchAvg;
											echo "</span>";
											echo "<label>All Branches Average </label><span>".$totalSatisfactionAllBranchsAvg;
											echo "</span>";
										}
										else
										{
	
											//echo "<label>Your Average: </label><span>".round($totalSatisfactionUserAvgWithOutConnected+$totalSatisfactionUserAvgConnected,2);
											echo "<label>Your Average: </label><span>".round($totalSatisfactionUserAvg,2);
											echo "</span>";
											echo "<label>Branch Average: </label><span>".$totalSatisfactionBranchAvg;
											echo "</span>";
										}*/
										if(rights(35,'read'))
										{
											echo "<label style='border-left: 0; left: auto; right: auto;'>All Branches Average </label><span>".$allScore;
											echo "</span>";
										}
										elseif($managerView)
										{
											echo "<label>Your Branch Average </label><span>".$depAvgScore;
											echo "</span>";
											echo "<label>All Branches Average </label><span>".$allScore;
											echo "</span>";
										}
										else
										{											
											echo "<label>Your Average: </label><span>".$userAvgScore;
											echo "</span>";
											echo "<label>Branch Average: </label><span>".$depAvgScore;
											echo "</span>";
										}
										?>
										</div>
								</div>
							</div>
													
						
						</div>
                    	<!--<div class="col-sm-6" id="chart_div" style="width: 280px; height: 180px;">
                        	
                        </div>-->
                    	<!--<div class="col-md-6" id="donutchart">-->
						<div class="col-md-6">


							<!-- Basic pie chart -->
							<div class="panel panel-flat">
								<div class="panel-heading newCusHead">
									<h5 class="panel-title" title='Including Connected Leads'>Assigned Status</h5>
								</div>

								<div class="panel-body">
									<div class="chart-container">
										<div class="chart has-fixed-height has-minimum-width" id="basic_pie"></div>
									</div>
								</div>
							</div>
							<!-- /bacis pie chart -->



                        	
                        	<!--  <img src="<?php echo base_url();?>assets/images/greap_2.jpg" alt="graph" height="250" width="450" /> -->
                        </div>
                    </div>
                    <div class="row" style="padding: 0 1px 0 2px;">
                    	
                    	

					<!-- leads start-->

<?php 



if(isset($loadonlyleads) && $loadonlyleads==true)
{
	//do nothing	
}
else
{
	
?>
            <div class="leadsListingTable dashboardListing fixHeight"> 
            	<div class="topBar">
                    <h2><i class="fa fa-bell"></i> Quick View</h2>
                    <div class="dashboardLatesFeedReload"><a href="javascript:void(0);" onclick="window.location.reload();"><i class="fa fa-refresh"></i></a></div>
                </div>
                            
								   
				<?php 
				$isManager = checkLoggedInUserMng(); 
				?>

            	<table class="table table-hover tab-content" >
                	<thead>
                    	<tr>
						
                        	<!--<th>&nbsp;</th>-->
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>Track ID.</th>
                        	<th>Name</th>
                        	<!--<th>Email</th>-->
							<?php if(!$isManager) { ?>
                        	<th>Phone/Mobile</th>
							<?php } ?>
							<!--<th>City</th>-->
							<?php if(!$isManager) { ?>
							<th>Category</th>
							<?php } ?>
							<th>Lead Age</th>
                        	<th>&nbsp;</th>
                        </tr>
                    </thead>                    
                    <tbody id="ajaxLoadMoreTBody">

                    <?php 
					}
					
					 $counterShowLead = 0;

					 $i=count($leads);
					 if($leads)
					 foreach($leads as $lead){


				 
				     /*$last_message = getLeadMessages($lead->id,1);

					 if(!empty($last_message))
					 {
						 $lastMsgType = getMessageType($last_message[0]->message_id);
						
						 
						 //if last message is from user who is logged in and self show=1
						 if($last_message[0]->created_by === $logged_in_user_id && $lastMsgType->self_show)
						 {
							//lets go further and show the lead
						 }
						 elseif($lead->assign_to == $logged_in_user_id)
						 {
							if($last_message[0]->created_by !== $logged_in_user_id && $lastMsgType->show_on_assignee_dashboard)
							{
								//lets go further and show the lead
							}
							else
							{
								continue;
							}
						 }
						 elseif($lead->created_by == $logged_in_user_id)
						 {
							if($last_message[0]->created_by !== $logged_in_user_id && $lastMsgType->show_on_creator_dashboard)
							{
								//lets go further and show the lead
							}
							else
							{
								continue;
							}
						 }
						 elseif(checkIfTagged($lead->id) && $lastMsgType->id==="4" && strpos($last_message[0]->comments,getEmployeeName($logged_in_user_id))!==false )
						 {
							 //its a tagged user and if last message is of comment type in which he is tagged

							 //lets go further and show the lead
						 }
						 elseif(checkLoggedInUserInboxRole() && $lead->created_by==="0")
						 {
  						  //now this is the lead that is inbox lead and this user is inbox user, so we can show leads on his dashboard if the last message is with  show_on_inbox_dashboard message for him by the assignee							 
							if($lastMsgType->show_on_inbox_dashboard)
							{
								//lets go further and show the lead
							}
							else
							{
								continue;
							}

						 }
						 else
						 {
							 continue; // all other cases don't show this lead on dashboard
						 }
							 
						 
					 }
					 else
					 {
						 continue; //it is necessory to have the at least one message. the dashboard logic is based upon the messages.
					 }

					//if test drive completed or canceled then don't show on dashboard.
					 if($last_message[0]->test_drive_canceled || $last_message[0]->test_drive_completed)
					 {
						 continue;
					 }

					 $counterShowLead++;

					 if($counterShowLead>10) break;
					 
					 */
					 
					 
					 
					 
					 

					?>
                    	<tr class="First">
							
							<?php //$catColor = "border-right: 5px solid ".getCategoryColor($lead->category_id).";"; ?>
							<?php $catColor = getCategoryColor($lead->category_id); ?>


                            <!--<td style="<?php echo $catColor; ?>">
							<?php if(!$isSingleDetailLead) { ?>
							
							
							<?php } ?>
							</td>-->
                            <!--<td id="<?php //if($isSingleDetailLead) echo 'singleDetailLead' ?>" class="rowClick" data-toggle="collapse" data-target="#LeadsMangDtl_<?php //echo $lead->id;?>"><?php //echo $i; ?></td>-->

							<!--<td id="<?php //if($isSingleDetailLead) echo 'singleDetailLead' ?>" class="rowClick" data-toggle="collapse" data-target="#LeadsMangDtl_<?php //echo $lead->id;?>">
								<?php //$subCount = getSubLeads($lead->email); if($subCount) $subCount = count($subCount); echo ($subCount>0 ? "+".$subCount:"");  ?>
							</td>-->
							<td>
								<div class="color5pxBorder" style="background-color:<?php echo $catColor; ?>"></div>
							</td>

							<td>
								<span id="notibell_db<?php echo $lead->id; ?>">
								<!--ajax fills it with a clock icon-->
								</span>
							</td>
                            
							
							<?php rowLead($isSingleDetailLead, $lead, $i, "#LeadsMangDtl_", "rowClick", $dashboard,$logged_in_user_id); ?>


                        </tr>
						
						
						<tr class="yellow collapse CollapseJsOuter" id="LeadsMangDtl_<?php echo $lead->id;?>">    <!--   SUbs -->
                            <td colspan="9" class="DetailBox dashboardDetailBox">
                            <!--<div class="colorDiv" style="background-color:<?php echo getCategoryColor($lead->category_id); ?>;"></div>-->
                            	<table class="dashbordCarList">
                                	<tbody>
                                    	<?php if($lead->vehicle!="" || $lead->vehicle_specific_names_id!="") { ?>										
										<tr>
                                        	<td style="background-color:<?php echo $catColor; ?>">&nbsp;</td>
                                        	<td style="background-color:<?php echo $catColor; ?>">Vehicle</td>
                                            <?php $specific_car_names = explode(',',$lead->vehicle_specific_names_id); 
											foreach($specific_car_names as $car_id){
												$get_specific_car_name = getSpecificCarName($car_id);?>
												<td><?php echo $get_specific_car_name; ?></td>
                                           
											<?php } ?>
											<?php $general_car_names = explode(',',$lead->vehicle); 
											foreach($general_car_names as $car_id){
												$get_general_car_names = getGeneralCarName($car_id);?>
												<td><?php echo $get_general_car_names; ?></td>
                                           
											<?php } ?>
											
                                            <td colspan="3">&nbsp;</td>
                                        </tr>
										<?php } ?>

                                    </tbody>
                                </table>
								<div class="color5pxBorder" style="background-color:<?php echo $catColor; ?>"></div>
                            	<table class="fisrtTable">
                            	<tr>
                                	<td colspan="8">
                                    	<div class="detailSecLead dashboardLeadsGrid">
											<?php expandedLead($lead,$i,$logged_in_user_id, $dashboard, $isSingleDetailLead,$vehicles_specific,false,array()); ?>
                                		</div>                                
                                	</td>
                                </tr>
                            </table>

							
							<!--Dashboard will not show sub leads in nested way. It will show all the leads that are supposed to show on dashboard in desc order-->


                            </td>                                
                        </tr>
                                             
                    	
                        <?php $i--; } 
						
						if(isset($loadonlyleads) && $loadonlyleads==true)
						{
							//do nothing	
						}
						else
						{						
						
						?>
                    </tbody>
                </table>
            </div>
            <?php if(!$isSingleDetailLead) { ?>

				<?php if(isset($loadmore) && $loadmore){ ?>

					<div class="loadMore"><a href="javascript:void(0);" onclick="loadMoreLeads('<?php echo base_url(); ?>');"><i class="fa fa-caret-down"></i> Load More</a></div>

				<?php } ?>

			<?php } ?>







<script>

var isInternalCollapse = false;

	
$(document).ready(function(){

	$('.rowClick').on('click', function () {

		var idOfDiv = $(this).attr("data-target");

		if( !$(idOfDiv).hasClass("in") ){
	  		$('.CollapseJsOuter').removeClass('in');
			$('.CollapseJsInner').removeClass('in');
		}

		$(".rowClick").closest("tr").removeClass("dashboardRowActive");
		//$(this :parent tr).addClass("dashboardRowActive");
		$(this).closest("tr").addClass("dashboardRowActive");

	});

	
	$('.AddPlusBtnIcons').on('click', function () {

		var idOfDiv = $(this).attr("href");

		if( !$(idOfDiv).hasClass("in") ){
	  		$('.PhoneBoxCollapse').removeClass('in');
			$('.CommentBoxCollapse').removeClass('in');
			$('.SendCustomerEmailCollapse').removeClass('in');
		}

	});


	<?php if($isSingleDetailLead) { ?>
		$("#singleDetailLead").click();
	<?php } ?>


});
	
	
</script>

<?php } ?>

					<!-- leads end-->


                    </div>
                </div>
            	<div class="col-md-4 col-sm-12 col-sm-12">
                	<div class="row">
                    	<!--<div class="col-sm-12 marginBottom4Px" id="columnchart_material">-->
						<div class="col-sm-12 marginBottom4Px">




							<div class="panel panel-flat">
								<div class="panel-heading newCusHead" style="position:static;">
									<?php
									if($managerView)
									{ ?>
										<h5 class="panel-title" title='Including Connected Leads'>Active Leads</h5>
									<?php }else{ ?>
										<h5 class="panel-title" title='Including Connected Leads'>Active Leads</h5>
									<?php } ?>
								</div>

								<div class="panel-body">
									<div class="dashBordTextChart">
										<?php
										/*if($managerView)
										{
											echo "<label>Total Active Leads </label><span>".$totalManagerActiveLeads;
											echo "</span>";
											echo "<label>Total Unassigned </label><span>".($totalUnAssignedConnected+$totalUnAssignedWithOutConnected);
											echo "</span>";
										}
										else
										{	
											echo "<label style='border-left: none;right: auto;'>Active Leads </label><span>".$totalUserActiveLeads;
											echo "</span>";											
										}*/
										if($managerView || rights(35,'read'))
										{
											echo "<label>Total Active Leads </label><span>".$activeLeadsCount;
											echo "</span>";
											echo "<label>Total Unassigned </label><span>".$unAssignedCount;
											echo "</span>";
										}
										else
										{	
											echo "<label style='border-left: none;right: auto;'>Active Leads </label><span>".$activeLeadsCount;
											echo "</span>";											
										}
										?>
										</div>
								</div>
							</div>


                        	<!-- <img src="<?php echo base_url();?>assets/images/greap_4.jpg" alt="graph" height="250" width="449" /> -->
                        </div>
                    	<!--<div class="col-sm-12" id="donutCatChart">-->
						<div class="col-sm-12">


							<div class="panel panel-flat">
								<div class="panel-heading newCusHead">
									<h5 class="panel-title" title='Including Connected Leads'>
										<?php
										if($managerView)
										{ ?>
											Team Leads 
										<?php }else{ ?>
											Vehicles
										<?php } ?>
										
										</h5>
								</div>
								<style>
									.circlesChart {text-align: center; overflow: hidden;height: 324px; margin-top: -20px;}
								</style>
								<div class="panel-body">
									<div class="chart-container circlesChart">
										<?php
										if($managerView)
										{ ?>
											<div style="height: 324px;" class="chart has-fixed-height has-minimum-width" id="basic_pie_users_under_mng"></div>
										<?php 
										}else{ ?>
											<div style="height: 324px;" class="chart svg-center" id="d3-bubbles"></div>
										<?php } ?>

									</div>
								</div>
							</div>

                        	<!-- <img src="<?php echo base_url();?>assets/images/greap_3.jpg" alt="graph" height="250" width="449" /> -->
                        </div>
                    	
                    	<div class="col-sm-12">
                        	<div class="dashboardLatesFeedSlider">
                            	<h2>News</h2>
                                <div class="dashboardLatesFeedReload"><a href="javascript:void(0);" onclick="window.location.reload();"><i class="fa fa-refresh"></i></a></div>
                            	<div id="dashboardLeadsAssigned" class="carousel slide" data-ride="carousel">
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">
                                        
										<?php $active = 'active'; if($newsArr) foreach($newsArr as $news){ ?>
										<div class="item <?php echo $active; ?>">
                                        	<?php echo date("d F Y h:i:s a", strtotime($news->created_at)); ?> | <?php echo $news->comments; ?>
                                        </div>
										<?php $active = ''; } ?>
                                    </div>                                    
                                    <!-- Controls -->
                                    <a class="left carousel-control" href="#dashboardLeadsAssigned" role="button" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                                    <a class="right carousel-control" href="#dashboardLeadsAssigned" role="button" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<div class="modal fade assignlead" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
<form action="<?php echo base_url();?>lead/leadAction" class="mb_form" onsubmit="return false;">

									<input type="hidden" name="form_type" value="admin_assign_lead_action">									
									<input type="hidden" id="assign_lead_id" name="lead_id" value="0">
									<input type="hidden" id="assign_lead_city" name="city_id" value="0">
									<input type="hidden" id="assign_lead_branch" name="branch_id" value="0">

  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header"> 
        	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">x</span></button> 
            <h4 class="modal-title">Assign a Lead</h4> 
         </div> 
         <div class="modal-body">
         	<p></p>
            <!--<div class="col-md-12">
            	<label>City <span>*</span></label>
							<select id="cityDDB" required>
                                <option value= "">Please Select</option>
                                <?php foreach($cities as $cities_list){?>
                                          <option value="<?php echo $cities_list->id; ?>"><?php echo $cities_list->title; ?></option>
                                 <?php } ?>   
                            </select>                
            </div>
            <div class="col-md-12">
            	<label>Branch Name <span>*</span></label> 
                	<select name="branch_id" id="branchDDB" required> 
                               
									<option value= "">Please Select</option>								
								
                    </select>	
            </div>-->
            <div class="col-md-12">
            	<label style="display:none;">Following are the Users with the City "<span id="lead_city_for_assign"></span>" and Branch "<span id="lead_branch_for_assign"></span>"<span></span></label>
                	<select id="employeeDDB" name="assign_to" required>
                    	<option value="">Please Select</option>
                    </select>

            </div>
            <div class="text-right"><!--<input type="button" class="btn" data-dismiss="modal">Assign</button>-->
			<input type="submit" class="btn" value="Submit" />			
			</div>
         </div> 
     </div>
     
  </div>
  </form>
</div>


<script>
/*
function graphsBy(type)
{
	window.location.href = "<?php echo base_url();?>dashboard/index/"+type;
}
*/

$(document).ready(function(){

	$(".leadsListingTable").on("click", ".rowClick", function(e){

		var leadId = $(this).attr('data-leadId');

		allNotificationRead(leadId);

	});
});



//=======start pie chart for delayed or no action leads.
<?php 
//$delayedTotal = $totalAssignedDelayedWithOutConnected+$totalUnAssignedDelayedWithOutConnected+$totalAssignedDelayedConnected+$totalUnAssignedDelayedConnected; 
$delayedTotal = $assignedDelayedCounter->delayed_count;
//$noActionTotal = $totalNotActionTakenLeadsWithoutConnected+$totalNotActionTakenLeadsConnected;
$noActionTotal = $assignedNoActionCounter->noaction_count;

if($delayedTotal || $noActionTotal)
{ ?>

$(function () {

    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: 'assets/js/js_charts/plugins/visualization/echarts'
        }
    });


    // Configuration
    // ------------------------------

    require(
        [
            'echarts',
            'echarts/theme/limitless',
            'echarts/chart/pie',
            'echarts/chart/funnel'
        ],


        // Charts setup
        function (ec, limitless) {


            // Initialize charts
            // ------------------------------

            var basic_pie = ec.init(document.getElementById('basic_pie'), limitless);            

            // Charts setup
            // ------------------------------                    

            //
            // Basic pie options
            //

            basic_pie_options = {

                // Add title
                title: {
                    text: '',
                    subtext: '',
                    x: ''
                },

                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    //formatter: "{a} <br/>{b}: {c} ({d}%)"
					formatter: "{b}: {c} ({d}%)"
                },                


                // Enable drag recalculate
                calculable: true,


                // Add series
                series: [{
                    name: '',
                    type: 'pie',
                    radius: '70%',
                    center: ['50%', '50%'],
					itemStyle: {
						normal: {
							label: {
								position: 'inner',
								formatter: '{c}'
							},
							labelLine: {
								show: false
							}
						},
						emphasis: {
							label: {
								show: true,
								formatter: '{c}'
							}
						}
					},
                    data: [						
                        {value: <?php echo $delayedTotal ?>, name: 'Delayed'},
                        {value: <?php echo $noActionTotal; ?>, name: 'No Action'}
                       // {value: 1548, name: 'Chrome'}
                    ]
                }]
            };


            // Apply options
            // ------------------------------

            basic_pie.setOption(basic_pie_options);           


            // Resize charts
            // ------------------------------

            window.onresize = function () {
                setTimeout(function (){
                    basic_pie.resize();
                }, 200);
            }
        }
    );
});

<?php } ?>

//=====end pie chart===

<?php if($managerView && count($usersLeadCount))
{ ?>

//=======start pie chart for leads of users under manager.
$(function () {

    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: 'assets/js/js_charts/plugins/visualization/echarts'
        }
    });


    // Configuration
    // ------------------------------

    require(
        [
            'echarts',
            'echarts/theme/limitless',
            'echarts/chart/pie',
            'echarts/chart/funnel'
        ],


        // Charts setup
        function (ec, limitless) {


            // Initialize charts
            // ------------------------------

            var basic_pie = ec.init(document.getElementById('basic_pie_users_under_mng'), limitless);            

            // Charts setup
            // ------------------------------                    

            //
            // Basic pie options
            //

            basic_pie_options = {

                // Add title
                title: {
                    text: '',
                    subtext: '',
                    x: ''
                },

				color: [
					<?php //for($umalc=0; $umalc<count($usersUnderMangColorArr); $umalc++) { ?>
						<?php //if($umalc>0) echo ","; ?>
						//'<?php //echo $usersUnderMangColorArr[$umalc]; ?>'

					<?php //} ?>
					
					<?php $ulci =0; foreach($usersLeadCount as $userLeadCountr) {
						if($ulci>0) echo ",";
						echo "'".$userLeadCountr->color."'";						
						$ulci++;
					} ?>

					/*'#ff7f50','#0098d7','#0098d7','#0098d7','#0098d7',
					'#0098d7','#0098d7','#0098d7','#0098d7','#0098d7',
					'#0098d7','#0098d7','#0098d7','#0098d7','#0098d7',
					'#0098d7','#0098d7','#0098d7','#0098d7','#0098d7',
					'#0098d7','#0098d7','#0098d7','#0098d7','#0098d7',
					'#0098d7','#0098d7','#0098d7','#0098d7','#0098d7'*/
				],

                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    //formatter: "{a} <br/>{b}: {c} ({d}%)"
					formatter: "{b}: {c} ({d}%)"
                },                


                // Enable drag recalculate
                calculable: true,


                // Add series
                series: [{
                    name: '',
                    type: 'pie',
                    radius: '60%',
                    center: ['50%', '55%'],
					itemStyle: {
						normal: {
							label: {
								position: 'inner',
								formatter: '{c}'
							},
							labelLine: {
								show: false
							}
						},
						emphasis: {
							label: {
								show: true,
								formatter: '{c}'
							}
						}
					},
                    data: [

						<?php //for($umal=0; $umal<count($usersUnderMangNameArr); $umal++) { ?>
							
							<?php //if($umal>0) echo ","; ?>

							//{value: <?php //echo $usersUnderMangAcLCountArr[$umal]; ?>, name: '<?php //echo $usersUnderMangNameArr[$umal]; ?>'}

						<?php //} ?>
						
						<?php $ulci =0; foreach($usersLeadCount as $userLeadCountr) {
							if($ulci>0) echo ",";
							echo "{value: ".$userLeadCountr->assign_count.", name: '".$userLeadCountr->full_name."'}";
							$ulci++;
						} ?>
						
                       // {value: 1548, name: 'Chrome'}
                    ]
                }]
            };


            // Apply options
            // ------------------------------

            basic_pie.setOption(basic_pie_options);           


            // Resize charts
            // ------------------------------

            window.onresize = function () {
                setTimeout(function (){
                    basic_pie.resize();
                }, 200);
            }
        }
    );
});

//=====end pie chart===




<?php }else{ 
	
if(count($usersVehiclesCount)) {
	
	?>
//=====start bubbles====

$(function () {

    // Initialize chart
    bubbles('#d3-bubbles', 370);

    // Chart setup
    function bubbles(element, diameter) {

		//var colorCodes = <?php //echo json_encode($genaralVehicleColorArr) ?>;// don't use quotes
		//var generalVehicleShrNmVsNorName = <?php //echo json_encode($generalVehicleShrNmVsNorName) ?>;// don't use quotes
		
		//{"E":"#0B3B0B","C":"#D7DF01","C-Coupe":"#01DFA5","GLC":"#61380B"};
		
		var colorCodes = {<?php $uvci=0; foreach($usersVehiclesCount as $uservcr){ if($uvci>0) echo ","; echo '"'.$uservcr->short_name.'":"'.$uservcr->color.'"'; $uvci++;}?>};
		var generalVehicleShrNmVsNorName = {<?php $uvci=0; foreach($usersVehiclesCount as $uservcr){ if($uvci>0) echo ","; echo '"'.$uservcr->short_name.'":"'.$uservcr->name.'"'; $uvci++;}?>};
		
		/*$.each(colorCodes, function(key, value) {
			console.log('stuff : ' + key + ", " + value);
		});*/


        // Basic setup
        // ------------------------------

        // Format data
        var format = d3.format(",d");

        // Color scale
        color = d3.scale.category10();



        // Create chart
        // ------------------------------

        var svg = d3.select(element).append("svg")
            .attr("width", diameter)
            .attr("height", diameter)
            .attr("class", "bubble");



        // Create chart
        // ------------------------------

        // Add tooltip
        var tip = d3.tip()
            .attr('class', 'd3-tip')
            .offset([-5, 0])
            .html(function(d) {
                return generalVehicleShrNmVsNorName[''+d.className+''] + ": " + format(d.value);
				//return d.className;
            });

        // Initialize tooltip
        svg.call(tip);



        // Construct chart layout
        // ------------------------------

        // Pack
        var bubble = d3.layout.pack()
            .sort(null)
            .size([diameter, diameter])
            .padding(1.5);



        // Load data
        // ------------------------------

        //d3.json("<?php echo base_url();?>assets/js/d3_charts/bubble.json", function(error, root) {			

			//var myjson = '{"name": "flare","children": [{"name": "analytics","children": [{"name": "cluster","children": [{"name": "MergeEdge", "size": 10 }]}]}]}';			

			var myjson = '{"name": "vehicles","children": [ { "name": "groups", "children": [';

			<?php for($vc=0; $vc<count($usersVehiclesCount); $vc++) { ?>

				<?php if($vc>0){ ?> myjson +=','; <?php } ?>
				myjson +=' {"name": "<?php echo $usersVehiclesCount[$vc]->short_name; ?>", "size": <?php echo $usersVehiclesCount[$vc]->scheduled_count; ?>}';

			<?php } ?>

			myjson +='   ] } ] }';

			root = JSON.parse( myjson );

            //
            // Append chart elements
            //

            // Bind data
            var node = svg.selectAll(".d3-bubbles-node")
                .data(bubble.nodes(classes(root))
                .filter(function(d) { return !d.children; }))
                .enter()
                .append("g")
                    .attr("class", "d3-bubbles-node")
                    .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

            // Append circles
            node.append("circle")
                .attr("r", function(d) { return d.r; })
                .style("fill", function(d) { 

					//return color(d.packageName); 

					/*if(d.className=="vehicle1")
						return "#0098d7";
					else
						return "#555656";					
						*/
					
					return colorCodes[''+d.className+''];
					//return "#0098d7";

				})
                .on('mouseover', tip.show)
                .on('mouseout', tip.hide);

            // Append text
            node.append("text")
                .attr("dy", ".3em")
                .style("fill", "#fff")
                .style("font-size", 12)
                .style("text-anchor", "middle")
                .text(function(d) { return d.className.substring(0, d.r / 3); });
        //});


        // Returns a flattened hierarchy containing all leaf nodes under the root.
        function classes(root) {
            var classes = [];

            function recurse(name, node) {
                if (node.children) node.children.forEach(function(child) { recurse(node.name, child); });
                else classes.push({packageName: name, className: node.name, value: node.size});
            }

            recurse(null, root);
            return {children: classes};
        }
    }
});

//=====end bubbles====
<?php } } ?>


</script>