<section>
	<div class="container">
    	<div class="dashboardSecMain">


           <?php $menu = menu(); 
		   if($menu)
		   {
		   ?>
		   <div class="addNewLeads">
            <div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo $menu; ?>
                    </ul>
                </div>
            </div>
			<?php } ?>


        	<div class="row">
            	<div class="col-md-8 col-sm-12 col-xs-12">
                	<div class="row marginBottom4Px">
                    	<div class="col-md-6" id="sat_chart_div"></div>
                    	<!--<div class="col-sm-6" id="chart_div" style="width: 280px; height: 180px;">
                        	
                        </div>-->
                    	<div class="col-md-6" id="donutchart">
                        	
                        	<!--  <img src="<?php echo base_url();?>assets/images/greap_2.jpg" alt="graph" height="250" width="450" /> -->
                        </div>
                    </div>
                    <div class="row">
                    	
                    	

					<!-- leads start-->

<?php 



if(isset($loadonlyleads) && $loadonlyleads==true)
{
	//do nothing	
}
else
{
	
?>
            <div class="leadsListingTable dashboardListing"> 
            	<div class="topBar">
                    <h2><i class="fa fa-bell"></i> Leads Assigned</h2>
                    <div class="dashboardLatesFeedReload"><a href="javascript:void(0);" onclick="window.location.reload();"><i class="fa fa-refresh"></i></a></div>
                </div>
                                   
            	<table class="table table-hover tab-content" >
                	<thead>
                    	<tr>
						
                        	<th>&nbsp;</th>
						
                        	<!--<th>No.</th>-->
							<th>&nbsp;</th>
							<th>Track ID.</th>
                        	<th>Name</th>
                        	<th>Email</th>
                        	<th>Mobile</th>                        	
                        	<!--<th>Created At</th>-->
							<th>Update At</th>
                        	<th>&nbsp;</th>
                        </tr>
                    </thead>                    
                    <tbody id="ajaxLoadMoreTBody">

                    <?php 
					} //same trs are used in ajax load more

					 $i=count($leads);
					 if($leads)
					 foreach($leads as $lead){ ?>
                    	<tr class="First">
							
							<?php $catColor = "border-right: 5px solid ".getCategoryColor($lead->category_id).";"; ?>

                            <td style="<?php echo $catColor; ?>">
							<?php if(!$isSingleDetailLead) { ?>
							
							
							<?php } ?>
							</td>
                            <!--<td id="<?php if($isSingleDetailLead) echo 'singleDetailLead' ?>" class="rowClick" data-toggle="collapse" data-target="#LeadsMangDtl_<?php echo $lead->id;?>"><?php echo $i; ?></td>-->

							<td id="<?php if($isSingleDetailLead) echo 'singleDetailLead' ?>" class="rowClick" data-toggle="collapse" data-target="#LeadsMangDtl_<?php echo $lead->id;?>"><?php $subCount = getSubLeads($lead->email); if($subCount) $subCount = count($subCount); echo ($subCount>0 ? "+".$subCount:"");  ?></td>
                            
							
							<?php rowLead($isSingleDetailLead, $lead, $i, "#LeadsMangDtl_", "rowClick", $dashboard); ?>


                        </tr>
						
						
						<tr class="yellow collapse CollapseJsOuter" id="LeadsMangDtl_<?php echo $lead->id;?>">    <!--   SUbs -->
                            <td colspan="10" class="DetailBox">
                            <div class="colorDiv" style="background-color:<?php echo getCategoryColor($lead->category_id); ?>;"></div>
                            <table class="fisrtTable">
                            	<tr>
                                	<td colspan="10">

                                    <div class="detailSecLead">


									<a class="newWindowLeadDB" target="_blank" href="<?php echo base_url();?>lead/singleLead/<?php echo $lead->id; ?>"><img width="18" height="18" alt="forward_transparent" src="<?php echo base_url();?>assets/images/forward_transparent.png"></a>
									


								<?php expandedLead($lead,$i,$logged_in_user_id, $dashboard, $isSingleDetailLead); ?>

                                    
                                </div>                                
                                </td>
                                </tr>
                            </table>
                            <table class="tableUp">
                               <?php 
							     $k=1;
							     $sub_leads = getSubLeads($lead->email);
 								 $j = count($sub_leads);
							     if(!empty($sub_leads)){
								 foreach($sub_leads as $sub_lead){
							    ?>
                            	
                                <tr class="First">
									
									<?php $catColor = "border-left: 5px solid ".getCategoryColor($sub_lead->category_id).";"; ?>

                                    <td style="<?php echo $catColor; ?>" data-toggle="collapse" data-target="#LeadsMangSubDtl_<?php echo $sub_lead->id;?>"><?php echo $j; ?></td>
									
									
									
									<?php rowLead($isSingleDetailLead, $sub_lead, $i.$k, "#LeadsMangSubDtl_", "", $dashboard); ?>




                                </tr>
                                <!--<tr class="BorderBottom">
                                	<td>&nbsp;</td>
                                    <td colspan="8"><?php echo $sub_lead->comments; ?></td>
                                </tr>-->
                                <tr class="yellow collapse CollapseJsInner" id="LeadsMangSubDtl_<?php echo $sub_lead->id;?>">  <!-- inner SUbs -->
                                    <td colspan="10">
                                    	<div class="colorDivInner" style="background-color:<?php echo getCategoryColor($sub_lead->category_id); ?>;"></div>
                                        <div class="detailSecLead">                                            

											<?php expandedLead($sub_lead,$i.$k,$logged_in_user_id, $dashboard, $isSingleDetailLead); ?>


                                        </div>
                                    </td>
                                </tr>
                              <?php $k++; $j--; } }?>
                                
                            </table>
                            </td>                                
                        </tr>
                                             
                    	
                        <?php $i--; } 
						
						if(isset($loadonlyleads) && $loadonlyleads==true)
						{
							//do nothing	
						}
						else
						{						
						
						?>
                    </tbody>
                </table>
            </div>
            <?php if(!$isSingleDetailLead) { ?>

				<?php if(isset($loadmore) && $loadmore){ ?>

					<div class="loadMore"><a href="javascript:void(0);" onclick="loadMoreLeads('<?php echo base_url(); ?>');"><i class="fa fa-caret-down"></i> Load More</a></div>

				<?php } ?>

			<?php } ?>





<div class="modal fade assignlead" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
<form action="<?php echo base_url();?>lead/leadAction" class="mb_form" onsubmit="return false;">

									<input type="hidden" name="form_type" value="admin_assign_lead_action">									
									<input type="hidden" id="assign_lead_id" name="lead_id" value="0">
									<input type="hidden" id="assign_category_id" name="category_id" value="0">

  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header"> 
        	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">x</span></button> 
            <h4 class="modal-title">Assign a Lead</h4> 
         </div> 
         <div class="modal-body">
         	<p>Please fill up the following form to assign the lead to an employee</p>
            <div class="col-md-12">
            	<label>City <span>*</span></label>
							<select id="cityDDB" required>
                                <option value= "">Please Select</option>
                                <?php foreach($cities as $cities_list){?>
                                          <option value="<?php echo $cities_list->id; ?>"><?php echo $cities_list->title; ?></option>
                                 <?php } ?>   
                            </select>                
            </div>
            <div class="col-md-12">
            	<label>Branch Name <span>*</span></label> 
                	<select name="branch_id" id="branchDDB" required> <!--with ajax in case of new lead or call custom helper function to load in edit mode-->
                               
									<option value= "">Please Select</option>								
								
                    </select>	
            </div>
            <div class="col-md-12">
            	<label style="color:red;">IMPORTANT: Employees with Category <span id="lead_cat_for_assign"></span> with the above selected City and Branch<span>*</span></label>
                	<select id="employeeDDB" name="assign_to" required>
                    	<option value="">Please Select</option>
                    </select>
            </div>
            <div class="text-right"><!--<input type="button" class="btn" data-dismiss="modal">Assign</button>-->
			<input type="submit" class="btn" value="Submit" />			
			</div>
         </div> 
     </div>
     
  </div>
  </form>
</div>

<script>

var isInternalCollapse = false;

	
$(document).ready(function(){

	$('.rowClick').on('click', function () {

		var idOfDiv = $(this).attr("data-target");

		if( !$(idOfDiv).hasClass("in") ){
	  		$('.CollapseJsOuter').removeClass('in');
			$('.CollapseJsInner').removeClass('in');
		}

	});

	
	$('.AddPlusBtnIcons').on('click', function () {

		var idOfDiv = $(this).attr("href");

		if( !$(idOfDiv).hasClass("in") ){
	  		$('.PhoneBoxCollapse').removeClass('in');
			$('.CommentBoxCollapse').removeClass('in');
			$('.SendCustomerEmailCollapse').removeClass('in');
		}

	});


	<?php if($isSingleDetailLead) { ?>
		$("#singleDetailLead").click();
	<?php } ?>


});
	
	
</script>

<?php } ?>

					<!-- leads end-->


                    </div>
                </div>
            	<div class="col-md-4 col-sm-12 col-sm-12">
                	<div class="row">
                    	<div class="col-sm-12 marginBottom4Px" id="columnchart_material">
                        	<!-- <img src="<?php echo base_url();?>assets/images/greap_4.jpg" alt="graph" height="250" width="449" /> -->
                        </div>
                    	<div class="col-sm-12" id="donutCatChart">
                        	<!-- <img src="<?php echo base_url();?>assets/images/greap_3.jpg" alt="graph" height="250" width="449" /> -->
                        </div>
                    	
                    	<div class="col-sm-12">
                        	<div class="dashboardLatesFeedSlider">
                            	<h2>Latest Feed</h2>
                                <div class="dashboardLatesFeedReload"><a href="javascript:void(0);" onclick="window.location.reload();"><i class="fa fa-refresh"></i></a></div>
                            	<div id="dashboardLeadsAssigned" class="carousel slide" data-ride="carousel">
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">
                                        
										<?php $active = 'active'; foreach($newsArr as $news){ ?>
										<div class="item <?php echo $active; ?>">
                                        	<?php echo date("d F Y h:i:s a", strtotime($news->created_at)); ?> | <?php echo $news->comments; ?>
                                        </div>
										<?php $active = ''; } ?>
                                    </div>                                    
                                    <!-- Controls -->
                                    <a class="left carousel-control" href="#dashboardLeadsAssigned" role="button" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                                    <a class="right carousel-control" href="#dashboardLeadsAssigned" role="button" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
	$pie_chart_data = '';
    if($leads_status){
	foreach($leads_status as $status_val){
		$status = $status_val->status;
		$count = getCount($status);
		
		$pie_chart_data .= "['".$status."', ".$count."],";
	}
}	
	$pie_chart_data = rtrim($pie_chart_data,',');
	
	//Satisfaction Chart
	$sat_counts = '';
	$sat_chart_data = '';
	$sat_chart_data_val = '';
	if($satisfaction){
    foreach($satisfaction as $sat)
	{
		$sat_chart_data = $sat->MONTH.' '.$sat->YEAR;
		$sat_counts = round($sat->answers_count, 2);
		$sat_chart_data_val .= "['".$sat_chart_data."', ".$sat_counts."],";
	}
    $sat_chart_data_val = rtrim($sat_chart_data_val,',');
    }else
    {
     $sat_chart_data_val = "['0', 0]";   
    }
	
    
	
	//Categories chart
	$color_arr = array('#02ADC1','#00649F','#0287D2','#0076BC','#3DB6E3','#81DFEB');
	$color_count = count($color_arr);
	$cat_counts = '';
	$cat_chart_data = '';
	$cat_chart_data_val = '';
	$cat_color = '';
	$k=0;
	if($related_category)
	{
		foreach($related_category as $cat)
		{
			$cat_chart_data = $cat->cat_type;
			$cat_counts = $cat->cat_count;
			$cat_chart_data_val .= "['".$cat_chart_data."', ".$cat_counts."],";
			$cat_color .= "'".$color_arr[$k]."',";
			$k++;
		}
	}
	$cat_chart_data_val = rtrim($cat_chart_data_val,',');
	$cat_color = "[".rtrim($cat_color,',')."]";
	
	//Region Chart
	$reg_counts = '';
	$reg_chart_data = '';
	$reg_chart_data_val = '';
    if($leads_by_region){
	foreach($leads_by_region as $reg)
	{
		$reg_chart_data = $reg->region_title;
		$reg_counts = $reg->region_count;
		$reg_chart_data_val .= "['".$reg_chart_data."', ".$reg_counts."],";
	}
    }
	else{
     $reg_chart_data_val = "['0', 0]";   
    }
	$reg_chart_data_val = rtrim($reg_chart_data_val,',');
	

?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    //Satisfaction chart
 google.charts.load("current", {packages:["corechart"]});
    /*  google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
      
        var data = google.visualization.arrayToDataTable
            ([['X', 'Y'],
              [1, 3],
              [2, 2.5],
              [3, 3],
              [4, 4],
              [5, 4],
              [6, 3],
              [7, 2.5],
              [8, 3]
        ]);

        var options = {
        legend : 'none',
          hAxis: { minValue: 0, maxValue: 5, color: '#FFFFFF' },
          pointSize: 4,
          title: 'Satisfaction',
          subtitle: 'Customer Services',
          titleTextStyle: { fontSize: 16,
          					color: 'white'},
          backgroundColor: '#676767',
          hAxis: {gridlines: {color: '#C9C9C9', count: 1, },
          textStyle: {color: 'transparent'}
          , ticks: [1]},
      vAxis: {gridlines: {color: '#808080', count: 5},
      textStyle: {
            color: '#FFFFFF',
     
            bold: true
          }},
      series: {
        0:{color: '#FFFFFF'}}
          
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }*/
    
    
        //Lead Progress chart
         // google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart1);
      function drawChart1() {
		  var data = google.visualization.arrayToDataTable([
          ['Task', 'Status'],
          <?php echo $pie_chart_data;?>
        ]);

        var options = {
          title: 'Leads by status',
		/*width: 393,*/
		height: 202
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));

        chart.draw(data, options);
       /* var data1 = google.visualization.arrayToDataTable([
          ['Total',    'tasks'],
          <?php //echo $pie_chart_data.',';?>

        ]);
 var data2 = google.visualization.arrayToDataTable([
          ['Total',    'tasks'],
          <?php //echo $pie_chart_data;?>
        ]);


        var options = {
          title: 'Leads Progress',
          titleTextStyle: { fontSize: 16,
          					color: '#0299C6'},
          subtitle: 'Completion',
          pieSliceText: 'value',
          colors:['#0091CD','#04538B'],
          legend: {position:'bottom'},
          
          
          
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        var diffData2 = chart.computeDiff(data1, data2);
chart.draw(diffData2, options);
        //chart.draw(data, options);*/
      }
      
	  /*google.charts.load("current", {packages:["corechart"]});
		google.charts.setOnLoadCallback(drawChart);
		function drawChart() {
		  var data = google.visualization.arrayToDataTable([
			["Element", "Density", { role: "style" } ],
			["Copper", 8.94, "#b87333"],
			["Silver", 10.49, "silver"],
			["Gold", 19.30, "gold"],
			["Platinum", 21.45, "color: #e5e4e2"]
		  ]);
	
		  var view = new google.visualization.DataView(data);
		  view.setColumns([0, 1,
						   { calc: "stringify",
							 sourceColumn: 1,
							 type: "string",
							 role: "annotation" },
						   2]);
	
		  var options = {
			title: "Density of Precious Metals, in g/cm^3",
			width: 600,
			height: 400,
			bar: {groupWidth: "95%"},
			legend: { position: "none" },
		  };
		  var chart = new google.visualization.BarChart(document.getElementById("categorychart"));
		  chart.draw(view, options);
	  }*/
	  
      //category_chart
	  
      //google.charts.load('current', {'packages':['corechart']});
      /*google.charts.setOnLoadCallback(drawChart_category);
      function drawChart_category() {
        var data = google.visualization.arrayToDataTable([
          ['Director (Year)',  'Rotten Tomatoes', 'IMDB'],
          ['', 8.4,         7.9],
          ['',     6.9,         7.8],
          ['',        6.5,         8.9],
          ['',      4.4,         10.8]
        ]);

        var options = {
          title: 'Categories',
          vAxis: {title: 'Comparison'},
          isStacked: true,
          
          series: [
  {color: '#007FD0', visibleInLegend: false, curveType: 'function', seriesType: 'area'}, 
  {color: '#0195DB', visibleInLegend: false, curveType: 'function', seriesType: 'area'}
]
        };

        var chart = new google.visualization.SteppedAreaChart(document.getElementById('categorychart'));

        chart.draw(data, options);
      }*/

/// Monthly Bar chart

    /*google.charts.setOnLoadCallback(drawChart3);
    
    function drawChart3() {
      var data = google.visualization.arrayToDataTable([
        ["Element", "", { role: "style" } ],
        ["Jan", 3, "#0295DA"],
        ["Feb", 2.5, "#0295DA"],
        ["Mar", 1.9, "#0295DA"],
        ["Apr", 2.7, "color: #0295DA"],
        ["May", 2.1, "color: #000000"],
        ["Jan", 3, "#0295DA"],
        ["Feb", 2.5, "#0295DA"],
        ["Mar", 1.9, "#0295DA"],
        ["Apr", 2.7, "color: #0295DA"],
        ["May", 2.1, "color: #000000"],
        ["Jan", 3, "#0295DA"],
        ["Feb", 2.5, "#0295DA"],
        ["Mar", 1.9, "#0295DA"],
        ["Apr", 2.7, "color: #0295DA"],
        ["May", 2.1, "color: #000000"],
        ["Jan", 3, "#0295DA"],
        ["Feb", 2.5, "#0295DA"],
        ["Mar", 1.9, "#0295DA"],
        ["Apr", 2.7, "color: #0295DA"],
        ["May", 2.1, "color: #000000"]
        
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Monthly Progress",
        titleTextStyle: {color:"#3B7CA2"},
        width: 600,
        height: 400,
        bar: {groupWidth: "85%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("chart_div1"));
      chart.draw(view, options);
  }*/
  
  //Satisfaction Chart
	google.charts.setOnLoadCallback(drawSatChart);
	  function drawSatChart() {
	  
		var data = google.visualization.arrayToDataTable
			([['X', 'Y'],
			  <?php echo $sat_chart_data_val;?>
		]);

		var options = {
		  hAxis: { minValue: 0, maxValue: 5, color: '#FFFFFF' },
		  pointSize: 4,
		  title: 'Satisfaction',
		  subtitle: 'Customer Services',
		  titleTextStyle: { fontSize: 16,
							color: 'white'},
		  backgroundColor: '#676767',
		  hAxis: {gridlines: {color: '#C9C9C9', count: 1, },
		  textStyle: {color: 'transparent'}
		  , ticks: [1]},
	  vAxis: {gridlines: {color: '#808080', count: 5},
	  textStyle: {
			color: '#FFFFFF',
	 
			bold: true
		  }},
	  series: {
		0:{color: '#FFFFFF'}},
		/*width: 341,*/
		height: 202
		  
		};

		var chart = new google.visualization.LineChart(document.getElementById('sat_chart_div'));
		chart.draw(data, options);
	}
    
	//complaint chart
	  google.charts.setOnLoadCallback(drawPieCatChart);
      function drawPieCatChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
		  <?php echo $cat_chart_data_val;?>
        ]);

        var options = {
          title: 'Total Active Leads by Categories',
          pieHole: 0.4,
		  /*width: 337,*/
		  height: 202,
		  colors: <?php echo $cat_color;?>
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutCatChart'));
        chart.draw(data, options);
      }
	  
	  //Region chart	  
	  google.charts.setOnLoadCallback(drawChart3);
    
		function drawChart3() {
		  var data = google.visualization.arrayToDataTable([
			["Element", "" ],
			<?php echo $reg_chart_data_val;?>
			
		  ]);
	
		  var view = new google.visualization.DataView(data);
		  var options = {
			title: "Total Active Leads by Region",
			titleTextStyle: {color:"#3B7CA2"},
			/*width: 337,*/
			height: 202,
			bar: {groupWidth: "85%"},
			legend: { position: "none" },
		  };
		  var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_material"));
		  chart.draw(view, options);
	  }
	  $(window).resize(function(){
		  drawChart1();
		  drawSatChart();
		  drawPieCatChart();
		  drawChart3();
	  });
    </script>
