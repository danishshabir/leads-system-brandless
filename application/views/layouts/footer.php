<input type="hidden" value="Create" class="btn" data-toggle="modal" data-target=".leadCreateSussess" id="show_success_messge" /><!-- just to show success message bootstrap logic --> 
<div class="modal fade leadCreateSussess" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header"> 
        	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">x</span></button> 
            <h4 id="mySmallModalLabel" class="modal-title"></h4> 
         </div> 
         <div class="modal-body">
         	<p id="message"></p>
            <p><strong></strong></p>
            <div class="text-center"><button type="button" class="btn" data-dismiss="modal">Dismiss</button></div>
         </div> 
     </div>
  </div>
</div>

<input type="hidden" value="remain" class="btn" data-toggle="modal" data-target=".leadRemain" id="leadRemain" /><!-- just to show success message bootstrap logic --> 
<div class="modal fade leadRemain" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    	<div class="modal-header"> 
        	<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">x</span></button> 
            <h4 id="remainLabel" class="modal-title">Reminder</h4> 
         </div> 
         <div class="modal-body">
         	<p id="timeMessage"></p>
            <p><strong></strong></p>
            <div class="text-center"><button type="button" class="btn" data-dismiss="modal">Dismiss</button></div>
         </div> 
     </div>
  </div>
</div>

<!--	New Model	22-Apr-2016		-->
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#nModeEdFtr" id="late_lead" style="display:none;">
  Launch modal
</button>

<!-- Modal -->
<div class="modal fade" id="nModeEdFtr" tabindex="-1" role="dialog" aria-labelledby="nModeEdFtrLabel" style="display:none;//">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="nModeEdFtrLabel">Your Late Leads</h4>
      </div>
	  <form action="<?php echo base_url();?>lead/LeadAction" method="post" class="mb_form" id="late_lead_form">
	  
      <div class="modal-body">
	  
        <ul>
		<?php if(!empty($get_late_leads)){
			foreach($get_late_leads as $late_lead){
		?>
			<li> <?php echo $late_lead->trackid;?> is late and its due date is : <?php echo date('D M j, Y  ',strtotime($late_lead->due_date)); ?></li>
		    <input type="hidden" name="ids[]" value="<?php echo $late_lead->id;?>" />
			<input type="hidden" name="creator_id[]" value="<?php echo $late_lead->created_by;?>" />
		<?php } } ?>
        	
        	
        </ul>
	  </div>
      <div class="modal-footer">
	  <input type="hidden" name="form_type" value="updateLateLeadNotifination" />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-default" style="margin-top:5px;" >Don't Show Again</button>
      </div>
	  </form>
    </div>
  </div>
</div>
    <footer>
    	<script>
            $(function(){
                $(document).on('click', '[data-toggle="popover"]', function(e){
                    e.preventDefault();
                    $('[data-toggle="popover"]').not(e.target).popover('hide');
                    $(e.target).popover('toggle');
                });
                $(document).click(function(e){
                    if(!$(e.target).hasClass('initPopover')) {
                        $('[data-toggle="popover"]').popover('hide');
                    }
                })
            });
        </script>
    </footer>
<?php if(!empty($get_late_leads)){?>	
  <script>
  
 // document.getElementById("late_lead").click();
 </script> 
<?php } ?>
<!--<script>
$(function () {

  $('textarea.mention').mentionsInput({
  onDataRequest:function (mode, query, callback) {
    var data = [
      { id:1, name:'Kenneth Auchenberg', 'avatar':'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif', 'type':'contact' },
      { id:2, name:'Jon Froda', 'avatar':'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif', 'type':'contact' },
      { id:3, name:'Anders Pollas', 'avatar':'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif', 'type':'contact' },
      { id:4, name:'Kasper Hulthin', 'avatar':'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif', 'type':'contact' },
      { id:5, name:'Andreas Haugstrup', 'avatar':'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif', 'type':'contact' },
      { id:6, name:'Pete Lacey', 'avatar':'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif', 'type':'contact' }
    ];

    data = _.filter(data, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });

    callback.call(this, data);
  }
});

});
		</script> -->
		<script type="text/javascript">jQuery('.fixHeight').enscroll();</script>
</body>
</html>