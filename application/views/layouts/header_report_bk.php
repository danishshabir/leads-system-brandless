<!doctype html>
<html>
<head>
<meta charset="utf-8">
<!--<meta name="viewport" content="width=device-width, initial-scale=1.0"  user-scalable=no;>-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">	
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">	
    <link href="<?php echo base_url();?>assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" media="all">	
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/datatables.min.css"/>
    <link href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" media="all">
	<link href="<?php echo base_url();?>assets/css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" media="all">
    <link href="<?php echo base_url();?>assets/css/intlTelInput.css" rel="stylesheet" type="text/css" media="all">
    <link href="<?php echo base_url();?>assets/css/style.css?v=3.8" rel="stylesheet" type="text/css" media="all">
	<link href='<?php echo base_url();?>assets/css/jquery.mentionsInput.css' rel='stylesheet' type='text/css'>
    <script>
    var base_url = '<?php echo base_url(); ?>';
	var is_lead_mng_screen = <?php if($view==="lead/leads") echo 'true'; else echo 'false'; ?>;
    </script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>	
    <!--<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.ui.timepicker.js"></script>-->
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui-timepicker-addon.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/datatables.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/tinymce.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js?v=3.9"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/intlTelInput.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.maskedinput.js"></script>
	<!--<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.caretposition.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.sew.js"></script> -->
	<script src='<?php echo base_url();?>assets/js/jquery.events.input.js' type='text/javascript'></script>
  <script src='<?php echo base_url();?>assets/js/jquery.elastic.js' type='text/javascript'></script>
	<script src='<?php echo base_url();?>assets/js/underscore-min.js' type='text/javascript'></script>
	<script src='<?php echo base_url();?>assets/js/jquery.mentionsInput.js' type='text/javascript'></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/enscroll-0.6.1.min.js"></script>	
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.cookie.js"></script>	
	<?php if(isset($pagetitle)){ ?>	
	<title><?php echo $pagetitle; ?></title>
	<?php }else { ?>
	<title>Leads System</title>
	<?php } ?>



	<!-- Global stylesheets -->
	<link href="<?php echo base_url();?>assets/css/css_charts/components.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!--<script type="text/javascript" src="<?php echo base_url();?>assets/js/js_charts/charts/echarts/pies_donuts.js"></script>-->

<!--		
<script type="text/javascript" src="http://demo.interface.club/limitless/layout_1/LTR/default/assets/js/plugins/loaders/pace.min.js"></script>
<script type="text/javascript" src="http://demo.interface.club/limitless/layout_1/LTR/default/assets/js/plugins/loaders/blockui.min.js"></script>
-->
		
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/d3_charts/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/d3_charts/d3.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/d3_charts/d3_tooltip.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>	
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/js_charts/c3.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/js_charts/plugins/visualization/echarts/echarts.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<!--	
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/js_charts/c3_lines_areas.js"></script>
-->

</head>
<body>

<?php if(!isset($publicSurvey)){ ?>
<header>
	<div class="MainMenu">
        <nav class='sidebar sidebar-menu-collapsed'>
            <a href='#' id='justify-icon' class="MenuIconBtn">
                <i class="sprite sprite-menuIcon"></i>
            </a>
            <ul>
                <li class="<?php if(isset($dashboard_class)) { echo $dashboard_class;} ?>">
                    <div  class='expandable'>
                        <a href='<?php echo base_url(); ?>dashboard' title='Dashboard' class="menuLinksBtn">
                            <i class="sprite sprite-homeIcon"></i>
                        </a>
                        <div class='expanded-element'>
                            <a href='<?php echo base_url(); ?>dashboard' title='Dashboard'><h2>Dashboard</h2>
                            <p>View Notifications, Performance</p></a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <?php if(rights(1,'all')){ ?>
                <li class="dropdown <?php if(isset($lead_class)) { echo $lead_class;} ?>">
                    <div  class='expandable'>
                        <a href="<?php echo base_url(); ?>lead" title='Lead Management' class="menuLinksBtn">
                            <i class="sprite sprite-menagment"></i>
                        </a>
                        <div class='expanded-element'>
                            <a href='<?php echo base_url(); ?>lead' title='Lead Management'><h2>Lead Management</h2>
                            <p>Manage your leads</p></a>
                        <ul class="innerDropDown">
						
						<?php $categoriesSideBar = getAllCategories(); if($categoriesSideBar) foreach( $categoriesSideBar as $category ){ ?>
						                            
							<li><a href="javascript:void(0)" onclick="document.location.href='<?php echo base_url(); ?>lead/index/filter/<?php echo $category->id ?>/All'"><i class="sprite sprite-menagment"></i><?php echo $category->title; ?></a></li>
                            
						<?php } ?>
                        </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <?php } ?>
                <?php if(rights(13,'all')){ ?>
                <li class="<?php if(isset($report)) { echo $report; } ?>">
                    <div class='expandable'>
                        <a href="<?php echo base_url(); ?>report" title='Reports' class="menuLinksBtn">
                            <i class="sprite sprite-reports"></i>
                        </a>
                        <div class='expanded-element'>
                            <a href='<?php echo base_url(); ?>report' title='Reports'><h2>Reports</h2>
                            <p>Generate Analytics Reports</p></a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <?php } ?>
                
                <li class="dropdown <?php if(isset($system_setting)) { echo $system_setting; } ?>">
                    <div  class='expandable'>
                
						<?php if(rights(14,'all')){ ?>
						<a href="<?php echo base_url(); ?>system_settings" title='Settings' class="menuLinksBtn">
                            <i class="fa fa-gear"></i>
                        </a>
						<?php } ?>
                        <div class='expanded-element'>
						<?php if(rights(14,'all')){ ?>
                            <a href='<?php echo base_url(); ?>system_settings' title='Settings'><h2>Settings</h2>
                            <p>Manage all your flow of work</p></a>
						<?php } ?>
                        <ul class="innerDropDown">
						<?php if(rights(8,'read')){ ?>
                            <li><a href="<?php echo base_url(); ?>user"><i class="fa fa-users"></i>User Management</a></li>
						<?php } if(rights(5,'read')){ ?>
                            <li><a href="<?php echo base_url(); ?>organization_structure"><i class="fa fa-sitemap"></i>Organization Structure</a></li>
						<?php } if(rights(11,'read')){ ?>
                            <li><a href="<?php echo base_url(); ?>role"><i class="fa fa-user"></i>Role Management</a></li>
						<?php } if(rights(7,'read')){ ?>
                            <li><a href="<?php echo base_url(); ?>log"><i class="fa fa-file-text"></i>Log Management</a></li>
						<?php } if(rights(9,'read')){ ?>
                            <li><a href="<?php echo base_url(); ?>survey"><i class="fa fa-file"></i>Survey Management</a></li>
						<?php } if(rights(6,'read')){ ?>
                            <li><a href="<?php echo base_url(); ?>branch_management"><i class="fa fa-building"></i>Branch Management</a></li>
						<?php } ?>
                            
                        </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </li> 
                
            </ul>
        </nav>
        <div class="dropdown menuLinksBtn" id="logout-icon">
                <a href="javascript:void(0);" id="logoutDtlBox" type="button" class="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="sprite sprite-user-icon"></i></a>
                <ul class="dropdown-menu" aria-labelledby="logoutDtlBox">
                    <li class="heading"><a href="javascript:void(0);"><?php echo $this->session->userdata['user']['full_name'];?></a></li>
                    <?php if( $this->session->userdata['user']['id'] != 1){ ?>
                    <li class=""><a href="<?php echo base_url(); ?>user/editUser/<?php echo $this->session->userdata['user']['id'];?>">User Profile</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>user/editUser/<?php echo $this->session->userdata['user']['id'];?>">Changes Password</a></li>
                    <?php } ?>
                    <li class=""><a href="<?php echo base_url();?>admin/logout">Log Out</a></li>
                </ul>
            </div>
    </div>
    <div class="clearfix"></div>
    <div class="toplogoSec">
    	<div class="container">
        	<div class="row">
            	<div class="col-lg-6 col-md-7 col-sm-8 col-xs-12">
		        	<div class="loginLogo"><img src="<?php echo base_url();?>assets/images/logo.png" alt="logo" height="45" width="174"><span>Communication System</span></div>
                </div>
            	<div class="col-lg-6 col-md-5 col-sm-4 col-xs-12">
		            <div class="topRightSection">
					<?php $notificationLeads = getLeadsWithNotifications('header'); ?>
                    	<ul>
                        	<li>								

								<form action="<?php echo base_url();?>lead/" method="get" id="searchfrm">
									
									<!--<a href="javascript:void(0);" onclick="$('#searchfrm').submit();"><i class="fa fa-search"></i></a>
									<i class="fa fa-search"></i>-->
									<input type="text" id="search_keyword" name="search_keyword" value="">

								</form>
							</li>
                        	<li>
                            	<div class="dropdown notificationDDown">
                                    <a id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="javascript:void(0);">
									
									<i id="noNotifi" style="display:<?php if($notificationLeads) echo "none"; else echo "block";?>;" class="fa fa-bell"></i>

									<img title="Total Notifications: <?php echo count(getLeadsWithNotifications()); ?>" style="display:<?php if($notificationLeads) echo "block"; else echo "none";?>;" id="flashNotifi" src="<?php echo base_url();?>assets/images/bell_icon_anim.gif" alt="logo" width="22">
									
									<!--<span style="display:<?php if($notificationLeads) echo "block"; else echo "none";?>;" id="headerNotiEst" class="notification">*</span>-->
									
									</a>
                                        <div class="dropdown-menu" aria-labelledby="dLabel">
                                        	<h3><i class="fa fa-bell"></i> Notification</h3>
                                            <table class="table">
                                            	<tbody id="headerNotiTable">
												<?php 
													if($notificationLeads) foreach( $notificationLeads as $notificationLead )
													{
														?>
														<tr class="hnt<?php echo $notificationLead->lead_id ?>">
                                                    	<td><a href="javascript:void(0);" onclick="notificationRead('<?php echo $notificationLead->id;  ?>', '<?php echo $notificationLead->lead_id ?>');"><?php echo $notificationLead->trackid; ?></a></td>
                                                        <td>
															<a href="javascript:void(0);" onclick="notificationRead('<?php echo $notificationLead->id;  ?>', '<?php echo $notificationLead->lead_id ?>');">
																<?php echo $notificationLead->comments ?>															
															<br/>
															<?php echo date("d F Y H:i a", strtotime($notificationLead->created_at)); ?>
															</a>
														</td>                                                        
														</tr>
														<?php
													}													
													?>
														<p style="display:<?php if($notificationLeads) echo "none"; else echo "block";?>;" id="nomnhp">No more notifications</p>
													<?php
													
												?>                                                	
                                                </tbody>
                                            </table>
											<p style="display:<?php if($notificationLeads) echo "block"; else echo "none";?>;" id="viewallnha"><a href="<?php echo  base_url();?>notifications">View all</a></p>
											
                                        </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    
</header>
<?php } ?>