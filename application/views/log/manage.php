<script>
	$(document).ready(function() {
		$('#advSearch').DataTable({
		
		"order": [[ 0, "desc" ]]

		});
	} );
	
</script>
<section>
	<div class="container">
    	<div class="leadsListingSec">

        	<?php $menu = menu(); 
		   if($menu)
		   {
		   ?>
		   <div class="addNewLeads">
            <div class="standardEdBtn dropdown">
                    <a id="dLabel" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url();?>assets/images/addNewLeads.png" alt="add New" height="62" width="62"/></a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php echo $menu; ?>
                    </ul>
                </div>
            </div>
			<?php } ?>

        	<div class="leadsListingHead">
            	<div class="row">
                	<div class="col-sm-6">
                        <h1><i class="fa fa-file-text"></i> Log Management</h1>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
               </div>            	
            </div>
            <div class="createNewLeadSec">
            	<div class="filterText">
                	<h2>Filters</h2>
                    <p></p>
                </div>
            	<div class="advSeachTbl logManagment">
                    <table id="advSearch" class="table table-condensed table-hover" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                            	<th>Created At</th>
                                <th>Lead Track ID</th>
                                <th>Action</th>
                                <!--<th>Action Type</th> -->
                                <th>Created by</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        if($logs){
                         foreach($logs as $log){?>
                        	<tr>    
                            	<td><?php echo date('j F Y h:i a',strtotime($log->created_at)); ?></td>                               
                                <td><a href="<?php echo base_url();?>lead/singleLead/<?php echo $log->lead_id; ?>"><?php echo $log->trackid; ?></a></td>
                                <td><?php echo $log->comments; ?></td>
                                <!--<td>Call</td>-->
                                <td><?php echo ($log->created_by === '-2' ? 'Automatic System' : getAdmin($log->created_by) );?></td>
                            </tr>
                       <?php } } ?> 
                            
                        </tbody>
                    </table>
                </div>
            </div>            
        </div>
    </div>
</section>
