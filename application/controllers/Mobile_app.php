<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class Mobile_app extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
		//checkAdminSession();
      	//$this->load->model('Model_report2');
      	$this->load->model('Model_registered_app_devices');
		$this->load->model('Model_common'); //use this in every controller because it is accessed in header.
		$this->load->model('Model_lead');
		$this->load->model('Model_notification');
		$this->load->model('Model_category');
		$this->load->model('Model_event');

    }
	
/*

//first time app is opened then ask for code and in return get the event_id and source_id and other data..
http://lm.mercedesbenzksa.com/staging/mobile_app/connect_apple_device?code=123
http://lm.mercedesbenzksa.com/mb/mobile_app/connect_apple_device?code=123

//afater push notification from settings screen, when from iPad the notification is clicked then the following url is called. (Reason we can't send much data in push notification so this new service is developed.)
http://lm.mercedesbenzksa.com/mb/mobile_app/connect_apple_device?code=dev123&push_call=1

//send the token for push notification
http://lm.mercedesbenzksa.com/staging/mobile_app/send_token_for_apple_fcm?token=kasjflaksj3453&code=123
http://lm.mercedesbenzksa.com/mb/mobile_app/send_token_for_apple_fcm?token=kasjflaksj3453&code=123

//send form data
http://lm.mercedesbenzksa.com/staging/mobile_app/send_form/?name=testname&email=test@test.com&mobile=123456799&branch_id=1|2&category_id=1&vehicle_ids=1,2,3&comments=testing%20comments&created_at=2016-10-03 15:05:02&code=123&created_id=1&source_id=1&event_id=2
http://lm.mercedesbenzksa.com/mb/mobile_app/send_form/?name=testname&email=test@test.com&mobile=123456799&branch_id=1|2&category_id=1&vehicle_ids=1,2,3&comments=testing%20comments&created_at=2016-10-03 15:05:02&code=123&created_id=1&source_id=1&event_id=2



Push notifications are sent from Settings Screen

*/

/*
now leads are also coming from ajax form at http://mercedesbenzksa.com/campaign/
and http://mercedesbenzksa.com/e-classcampaign/
*/

	
	public function index(){ 
		
	}	

	//receive the form data
	public function send_form(){ 

		$post_data = $this->input->get();
		
		if(!count($post_data)) {echo "direct access is not allowed"; exit();}

		$name = $post_data['name'];
		$email = $post_data['email'];
		if($email=="") exit(); //some time google boats are triggering this url.
		$mobile = fixContactNumber($post_data['mobile']);		
		$branch_id = $post_data['branch_id']; //it will be in | with branch_id|city_id but user might be only shown the branch
		if($branch_id=="") $branch_id="|";
		$branch_id_arr = explode("|",$branch_id);
		$city_id = $branch_id_arr['1'];
		$branch_id = $branch_id_arr['0'];		
		$category_id = $post_data['category_id']; //service ddb in app
		$vehicle_ids = "";
		$vehicle_specific_names_id = "";
		if(isset($post_data['is_vehicle_specific']) && (int)$post_data['is_vehicle_specific']===1)		
			$vehicle_specific_names_id = $post_data['vehicle_specific_names_id'];	
		else
			$vehicle_ids = $post_data['vehicle_ids']; //general vehicle ids		
		$comments = $post_data['comments'];
		if(isset($post_data['created_at'])) $created_at = $post_data['created_at']; //2016-10-03 01:05:01 or 2016-10-03 15:05:02
		else { $created_at = date('Y-m-d H:i:s'); $post_data['created_at'] = $created_at; }
		$code = $post_data['code'];
		$source_id = $post_data['source_id'];
		$event_id = $post_data['event_id'];
		$created_id = $post_data['created_id'];
		$http_referer = "";
		if(isset($post_data['http_referer']))
			$http_referer = $post_data['http_referer'];		


		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		//just in case send emails
		mail("waqas@astutesol.com","form post_data from Leads System iPad or campaign forms","Name: ".$name.'<br>Email: '.$email.'<br>Mobile: '.$mobile.'<br>City id: '.$city_id.'<br>Branch id: '.$branch_id.'<br>Category id: '.$category_id.'<br>Vehicle id or ids comma sep: '.$vehicle_ids.'<br>Vehicle specific id: '.$vehicle_specific_names_id.'<br>Comments: '.$comments.'<br>Created at: '.$created_at.'<br>Code: '.$code.'<br>Source id: '.$source_id.'<br>Event id: '.$event_id."<br>Created id: ".$created_id."<br>Http Referer: ".$http_referer,$headers);

		
		//=========Start create new lead stuff=================
			$data = array();
			
			if(isset($post_data['is_vehicle_specific']) && (int)$post_data['is_vehicle_specific']===1)
			{	//single id from drop down box
				$data['vehicle_specific_names_id'] = $post_data['vehicle_specific_names_id']; 				
			}else
			{
				//general comma sep vehicle ids from ipad
				// or single value from online campaign form.
				$data['vehicle'] = $post_data['vehicle_ids']; 
			}
			
			if((int)$event_id===31 && (int)$branch_id===26)
			{
				$data['assign_to'] = 168;
				$data['status'] = "Assigned";
			}
			if((int)$event_id===31 && (int)$branch_id===27)
			{
				$data['assign_to'] = 243;
				$data['status'] = "Assigned";
			}
			if((int)$event_id===31 && (int)$branch_id===43)
			{
				$data['assign_to'] = 235;
				$data['status'] = "Assigned";
			}
			
			$data['city_id'] = $city_id;
			$data['branch_id'] = $branch_id;
			$data['first_name'] = $name;
			$data['manual_source'] = $source_id;
			$data['mobile'] = $mobile;			
		
			//update all previous leads to zero
			$arr_update_email['new_lead_with_same_email'] = 0;
			$arr_update_email_by['email'] = $post_data['email'];			
			$this->Model_lead->update($arr_update_email,$arr_update_email_by);
			
			/*foreach($post_data as $key => $value)
			{
				
				if($key != 'vehicle_ids' && $key != 'branch_id' && $key!='created_id' && $key!='name' && $key!='source_id')
				{
				 $data[$key] = $value;	
				}
			}*/

			$data['email'] = $post_data['email'];
			$data['comments'] = $post_data['comments'];
			$data['event_id'] = $post_data['event_id'];
			$data['code'] = $post_data['code'];
			$data['category_id'] = $post_data['category_id'];



			  
			//$data['created_at'] = date('Y-m-d H:i:s');
			$data['new_lead_with_same_email'] = 1;
			$data['created_by'] = 0; //this will be 0 for leads created by cronjob email parsing or from mobile apps
			//$data['orignal_created_by'] = $this->session->userdata['user']['id']; //this is a backup column, because the created by column can contain the latest manager id so the original creator id will remain here for future use 
			$data['trackid'] = generatTrackId($data['category_id']);

			if(!isset($data['created_at'])) $data['created_at'] = date('Y-m-d H:i:s');

			$data['due_date'] = calculateLeadDeadline($data['created_at'], $data['category_id']);
			
			$data['http_referer'] = $http_referer;
			

			//check double sending from iPad app. Becasue sometime app is submitting double leads.
			$arr_check_dup = array();
			$arr_check_dup['code'] = $post_data['code'];
			$arr_check_dup['created_at'] =  $post_data['created_at'];
			$duplicated_lead = $this->Model_lead->getMultipleRows($arr_check_dup);

			if($duplicated_lead && count($duplicated_lead)>0)
			{
				//do nothing
			}else
			{


				$insert_lead_id = $this->Model_lead->save($data);

				//Send Email To Customer For New Lead Created
				$cat_row = $this->Model_category->get($data['category_id'],true);
				$data['category_title'] = $cat_row['title'];
				$data['category_title_ar'] = $cat_row['title_email_ar'];
				
				$event_row = $this->Model_event->get($data['event_id'],true);
				$data['event_title'] = $event_row['title'];
				$data['event_title_ar'] = $event_row['title_ar'];

				$emailData = $data;			
				$emailData['showroom'] = getBranchName($data['branch_id']);
				$emailData['showroom_ar'] = getBranchNameAr($data['branch_id']);
				$vehicles = "";
				
				if(isset($post_data['is_vehicle_specific']) && (int)$post_data['is_vehicle_specific']===1)
				{
					if($data['vehicle_specific_names_id']!="")
						$emailData['vehicles'] = getSpecificVehicleNameById($data['vehicle_specific_names_id']);
					else
						$emailData['vehicles'] = "";
				}
				else
				{
					$vehicle_general_names_id_arr = explode(",",$data['vehicle']);				
					foreach($vehicle_general_names_id_arr as $vsi)
					{
						if($vehicles!="") $vehicles .= ", ";
						$vehicles .= getGeneralVehicleNameById($vsi);
					}
					$emailData['vehicles'] = $vehicles;
				}


				if(isset($post_data['frm_from_ksa_com']))
				{
					sendEmail('9',$data['email'],$emailData); 
				}
				else
				{	$emailData['template'] = $event_row['template'];
					$emailData['template_ar'] = $event_row['template_ar'];
					$emailData['subject'] = $event_row['subject'];
					sendEmail('-2',$data['email'],$emailData); 
				}

				
				//add notification
				$dataLead = array();
				$dataNotification = array();
				//$dataNotification['inbox_notification_read'] = 1;
				$dataNotification['created_at'] = date('Y-m-d H:i:s');
				$dataNotification['created_by'] = 0;	
				$dataNotification['lead_id'] = $insert_lead_id;
				if(isset($post_data['frm_from_ksa_com']))
				{
					$dataNotification['comments'] = "Lead received from Leads System campaign";
				}
				else
				{
					$dataNotification['comments'] = "Lead received from iPad App with Code ".$code;
				}
				$this->Model_notification->save($dataNotification);
				//====
			}



			//$data['trackid'];
		//===========End create new lead stuff======================
		

		$dataRes['status'] = 1; //0 or 1
		$dataRes['code'] = $data['code'];
		$dataRes['created_at'] = $post_data['created_at']; 
		$dataRes['created_id'] = $post_data['created_id']; 
		echo json_encode($dataRes);
		
	}	


	//get token for fcm push notification
	public function send_token_for_apple_fcm()
	{
		$fetch_by = array();
		$data = array();
		$response = array();

		$token = $_GET["token"]; // fcm Registration ID, token
		$code = $_GET["code"]; // fcm Registration ID


		$registered_app_device = $this->Model_registered_app_devices->getWithMultipleFields(array("code"=>$code));
		if($registered_app_device)
		{
			//The received code will always already exist in the table registered_app_devices.
			$this->Model_registered_app_devices->update(array("token"=>$token),array("id"=>$registered_app_device->id));

			$response['status'] = 1; //0 or 1			
			echo json_encode($response);
			exit();

		}else
		{
			$response['status'] = 0; //0 or 1
			echo json_encode($response);
			exit();
		}
		

	}

	
	//first time app is opened then ask for code
	public function connect_apple_device()
	{
		$fetch_by = array();
		$data = array();
		$response = array();
		
		$push_call = 0;
		$code = $_GET["code"]; // fcm Registration ID
		if(isset($_GET["push_call"]) && (int)$_GET["push_call"]===1)
		$push_call = 1; //0/1

		$registered_app_device = $this->Model_registered_app_devices->getWithMultipleFields(array("code"=>$code));
		if($registered_app_device)
		{
			if(!$registered_app_device->code_connected || $push_call)
			{
				//update the row to save that device is connected so that this code can't be used in any other app connectivity.
				if(!$push_call)
				{
					$this->Model_registered_app_devices->update(array("code_connected"=>1),array("id"=>$registered_app_device->id));
				}

				if(!$push_call)
				{
					//The received code will always already exist in the table registered_app_devices.
					$response['status'] = 1; //0 or 1
				}
				
				$response['source_id'] =  $registered_app_device->source_id;
				$response['event_id'] =  $registered_app_device->event_id;	
				$response["categories"] = $this->Model_category->getMultipleCategories($registered_app_device->categories_id);
				$response["vehicles"] = $this->Model_category->getMultipleVehicles($registered_app_device->vehicles_id);
				$response["branches"] = $this->Model_category->getMultipleBranches($registered_app_device->branches_id);
				
				if(!$push_call)
				{
					$response['message'] = "Device connected successfully";
				}
				echo json_encode($response,JSON_UNESCAPED_UNICODE);
				exit();
			}
			else
			{
				//someone is trying to connect the already connected code.
				$response['status'] = 0; //0 or 1				
				$response['message'] = "This code is already connected to other app";
				echo json_encode($response);
				exit();

			}
		}
		else
		{
			$response['status'] = 0; //0 or 1				
			$response['message'] = "Invalid code";
			echo json_encode($response);
			exit();
		}

		
		

	}


}

