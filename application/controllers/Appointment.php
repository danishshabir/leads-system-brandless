<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Appointment extends CI_Controller { 

	public function __construct() 
    { 
        parent::__construct();
        checkAdminSession();
        // if(rights(40,'write') || $this->session->userdata['user']['adviser_type'] != NULL)
		// {	
		// 	// Do Nothing
		// } else {
		// 	echo "Sorry! You don't have permission to access this page, Please ask admin to allow you to open \"Multi Advisers Calendar\" So you can set appointment for this customer.";
		// 	exit;
		// }
      	$this->load->model('Model_lead'); 
		$this->load->model('Model_branch'); 
		$this->load->model('Model_common'); 
		$this->load->model('Central_model');     
		$this->load->model('Model_appointment');     
		$this->load->model('Model_leads_messages');
		$this->load->model('Model_appointment');
    }
	 
	public function index() 
	{ 
		$data = array(); 
		if(isset($_GET['lead_id'])) {
			$this->session->set_flashdata('appointment_lead_id', $_GET['lead_id']);
			redirect(base_url().'appointment');
			exit;
		}
		$data['service_branches'] = $this->Model_branch->getServiceBranches();		
		$data['user_bid'] = $this->Model_appointment->getUserBIdFromOrgStruc($this->session->userdata['user']['id']);
		$data['advisers'] = $this->Model_appointment->getAdviserUsersByBidFromOrgStruc($data['user_bid']);
		$data['appointmentVehicles'] = $this->Model_appointment->getAppointmentVehicles();
		$data['view'] = 'appointments/index'; 
		$data['appointment_cls'] = 'active'; 
		$this->load->view('template',$data); 
	}  
	 
	public function ajaxCalendar($lead_id=Null){ 
		$data = array(); 
		$response = array(); 
		$data['branchId'] = $this->input->post('branchId'); 
		$data['adviser_id'] = $this->input->post('adviser_id'); 
		$data['page'] = $this->input->post('page');
		$data['interval'] = 900; // Interval in seconds
		$data['branchRec'] = $this->Model_branch->get($data['branchId']); 
		$data['weekStart'] = ($this->input->post('weekStartDate') ? $this->input->post('weekStartDate') : (date('w', strtotime(date('Y-m-d'))) == 5 ? date('Y-m-d', strtotime('+1 days',strtotime(date('Y-m-d')))) : date('Y-m-d'))); 
		$data['day'] = ($data['weekStart'] ? date('w', strtotime($data['weekStart'])) : date('w')); 
		$data['weekEnd'] = ($data['weekStart'] ? date('Y-m-d', strtotime('+6 days', strtotime($data['weekStart']))) : date('Y-m-d', strtotime('+6 days')));   
		$data['appointment'] = array(
			'id' => [],
			'appointment_start_time' => [],
		); 
		if($this->input->post('page') == 1 || $this->input->post('page') == 2)
		{
			if($data['branchId']) {
				if($this->input->post('page') == 1)
				{
					$page = "weekly";
					$appointments = $this->Model_appointment->get_appointments($data); 
				} elseif($this->input->post('page') == 2) {
					$page = "daily";
					$appointments = $this->Model_appointment->get_appointment($data);
				}
				if($appointments) { 
					foreach($appointments as $key => $row) { 
						$data['appointment']['id'][$key] = $row->id; 
						$data['appointment']['appointment_start_time'][$key] = $row->appointment_start_time; 
						$data['appointment']['type'][$key] = $row->type; 
						$data['appointment']['name'][$key] = $row->name;
						$data['appointment']['appointment_vehicle_id'][$key] = $row->appointment_vehicle_id; 
						$data['appointment']['adviser_id'][$key] = $row->adviser_id;
						$data['appointment']['mobile'][$key] = $row->mobile; 
						$data['appointment']['status'][$key] = $row->status;
						$data['appointment']['date'][$key] = $row->appointment_start_time; // For left Calender						
						//15th Sep 2017
						$data['appointment']['date_vise_appnt_counts'][$key] = date("Y-m-d", strtotime($row->appointment_start_time));
					} 					
					//15th Sep 2017
					$date_vise_appnt_counts = array_count_values($data['appointment']['date_vise_appnt_counts']);
					$data['appointment']['date_vise_appnt_counts'] = $date_vise_appnt_counts;
				}

				$html = $this->load->view('appointments/list/'.$page, $data, true);
			} else {
				$html = $this->load->view('appointments/messages/messages', $data, true);
			}
		}
		else if($this->input->post('page') == 3 || $this->input->post('page') == 4) 
		{
			$data['advisers'] = $this->Model_appointment->getAdviserUsersByBidFromOrgStruc($data['branchId']);
			if($data['advisers']) {
				if($this->input->post('page') == 3) {
					$page = "weekly";
					$appointments = $this->Model_appointment->get_appointments($data);
				} elseif($this->input->post('page') == 4) {
					$page = "daily";
					$appointments = $this->Model_appointment->get_appointment($data); 
				}
				if($appointments) {
					foreach($appointments as $key => $row) { 
						$data['appointment'][$row->adviser_id]['appointment_start_time'][$key] = $row->appointment_start_time; 
						$data['appointment']['id'][$key] = $row->id;
						$data['appointment']['type'][$key] = $row->type; 
						$data['appointment']['name'][$key] = $row->name; 
						$data['appointment']['adviser_id'][$key] = $row->adviser_id;
						$data['appointment']['date'][$key] = $row->appointment_start_time; // For Left Calender
                        $data['appointment']['status'][$key] = $row->status;
						//15th Sep 2017
						$data['appointment']['date_vise_appnt_counts'][$row->adviser_id][$key] = date("Y-m-d", strtotime($row->appointment_start_time));
					} 
					//15th Sep 2017					
					foreach($data['appointment']['date_vise_appnt_counts'] as $key=>$date_advis_appnt_arr)
					{
						$data['appointment']['date_vise_appnt_counts'][$key] = array_count_values($date_advis_appnt_arr);
					}
				}
				$html = $this->load->view('appointments/grid/'.$page, $data, true);
			} else {
				$html = $this->load->view('appointments/messages/messages', $data, true);
			}
		}			 
		$response['html'] = $html; 
		echo json_encode($response); 
	}
	public function ajaxMonthlyCalendars() 
	{ 
		$response = array(); 
		$data['data'] = $this->input->post(); 
		$response['html'] = $this->load->view('appointments/calender/calender', $data, true); 
		echo json_encode($response); 	 
	}	
	public function insert()
	{
		$data = array();
		if($this->input->method(TRUE) == 'POST') {
			$this->load->library('form_validation');
			$data = $this->input->post();

			$branchTitleEn = isset($data['branchTitleEn']) ? $data['branchTitleEn'] : '';
			$branchTitleAr = isset($data['branchTitleAr']) ? $data['branchTitleAr'] : '';
			unset($data['branchTitleAr'], $data['branchTitleEn']);
 			$rules = array(
				[
					'field' => 'name',
					'label' => 'Full Name',
					'rules' => 'trim|required',
				],
				[
					'field' => 'mobile',
					'label' => 'Mobile',
					'rules' => 'trim|required',
				],
				[
					'field' => 'type',
					'label' => 'Type',
					'rules' => 'trim|required',
				],
				[
					'field' => 'appointment_vehicle_id',
					'label' => 'Vehicle',
					'rules' => 'trim|required',
				],
				[
					'field' => 'branch_id',
					'label' => 'Branch',
					'rules' => 'trim|required',
				],
				[
					'field' => 'appointment_start_time',
					'label' => 'Appointment Time',
					'rules' => 'trim|required|callback_check_appointment['.$data['branch_id'].'||'.$data['adviser_id'].']',
				],
				[
					'field' => 'adviser_id',
					'label' => 'Adviser',
					'rules' => 'trim|required',
				],
			);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run()) {
                $current_time = date("Y-m-d H:i:s");
                $posted_time = $this->input->post('appointment_start_time');

                if ($current_time > $posted_time) {
                    $errors = array(
                        'datetime_error' => "Unable to book an Appointment in Back Date / Time"
                    );
                    echo json_encode(array(
                        'errors' => $errors,
                        'success' => false,
                    ));
                    exit();
                }

                //==========Start also allowed to set the vehicle type========
                if (strpos($data['appointment_vehicle_id'], "type-") !== false) {
                    //its a parent id that is type id.
                    $data['appointment_vehicle_type_id'] = str_replace("type-", "", $data['appointment_vehicle_id']);
                    unset($data['appointment_vehicle_id']);
                } else {
                    //$data['appointment_vehicle_id'] is a model id
                    $data['appointment_vehicle_id'] = str_replace("model-", "", $data['appointment_vehicle_id']);
                }
                //==========End also allowed to set the vehicle type========

                // Counting Daily Appointment for the selected Adviser
                $this->getCountDailyAppointments($data);

                // ========= End Counting
                $data['appointment_end_time'] = ($data['type'] == 0 ? date("Y-m-d H:i:s", strtotime('+15mins', strtotime($data['appointment_start_time']))) : date("Y-m-d H:i:s", strtotime('+30mins', strtotime($data['appointment_start_time']))));
                $data['lead_id'] = $this->save_lead($data);  // Farooq
                $data['lead_id'] = NULL;
                $data['created_by'] = $this->session->userdata['user']['id'];
                $data['created_at'] = date("Y-m-d H:i:s");
                $data['status'] = 10;
                $save = array(
                    'name' => $data['name'],
                    'mobile' => $data['mobile'],
                    'email' => $data['email'],
                    'type' => $data['type'],
                    'adviser_id' => $data['adviser_id'],
                    'created_by' => $data['created_by'] = $this->session->userdata['user']['id'],
                    'branch_id' => $data['branch_id'],
                    'appointment_vehicle_type_id' => $data['appointment_vehicle_type_id'],
                    'appointment_vehicle_id' => $data['appointment_vehicle_id'],
                    'appointment_start_time' => $data['appointment_start_time'],
                    'appointment_end_time' => $data['appointment_end_time'],
                    'status' => 10,
                    'created_at' => $data['created_at'] = date("Y-m-d H:i:s"),
                    'lead_id' => $data['lead_id']
                );
                $id = $this->Central_model->save("appointment", $save);
                if ($id) {
                    //send_sms(array("phone_no" => str_replace(array('(', ')', ' '), array('', '', ''), $data['mobile']), "sms_text" => "Dear ".$data['name'].", this is to inform you that your appointment for vehicle service is booked at ".date("F j, Y, g:i a", strtotime($data['appointment_start_time']))." Regards Leads System"));
                    // Arabic SMS

                    $data['mobile'] = str_replace('+','',$data['mobile']);

                    // Latest
                    // $sms_result = send_sms(array("phone_no" => str_replace(array('(', ')', ' '), array('', '', ''), $data['mobile']), "sms_text" => "عزیزي  " . $data['name'] . ", شكرًا لتواصلكم معنا، تم تأكید موعدكم مع فریق الصیانة ویسعدنا استقبالكم في  $branchTitleAr, " . date("F j, Y, g:i a", strtotime($data['appointment_start_time'])) . " Regards Leads System"));

                    // English SMS
                    //$sms_result_ar = send_sms(array("phone_no" => str_replace(array('(', ')', ' '), array('', '', ''), $data['mobile']), "sms_text" => "Dear " . $data['name'] . ", Thank you for confirming your booking with our After Sales team. We look forward to welcoming you at  $branchTitleEn" . date("F j, Y, g:i a", strtotime($data['appointment_start_time'])) . " Regards Leads System"));

                    $emailData['first_name'] = $data['name'];
                    $emailData['branch_title_ar'] = $branchTitleAr;
                    $emailData['branch_title_en'] = $branchTitleEn;
                    $emailData['appointment_start_time'] = $data['appointment_start_time'];
                    $emailData['appointment_end_time'] = $data['appointment_end_time'];
                    if ($data['email']) {
                        sendEmail(11, $data['email'], $emailData);
                    }
					// Farooq
					$this->lead_comment($id, 'Appointment Time: '.date("j M Y - g:i A", strtotime($data['appointment_start_time'])));
					echo json_encode(array(
						'success' => true,
                        'sms_result' => $sms_result,
					));
				}
			} else {
				$errors = array(
					'appointment_start_time' => form_error('appointment_start_time')
				); 				
				echo json_encode(array(
					'errors' => $errors,
					'success' => false,
				));
			}
		}
	}
	
	public function save_lead($data)
	{
		if(!$data['lead_id']) {
			$leadData = array();
			$leadData['first_name'] = $data['name'];
			$leadData['mobile'] = $data['mobile'];
			$leadData['email'] = $data['email'];
			$leadData['branch_id'] = $data['branch_id'];
			$leadData['assign_to'] = $data['adviser_id'];
			$leadData['status'] = "Assigned";
			$leadData['manual_source'] = 5;
			$leadData['created_by'] = $this->session->userdata['user']['id'];
			$leadData['orignal_created_by'] = $this->session->userdata['user']['id'];
			$leadData['trackid'] = generatTrackId(37);
			$leadData['category_id'] = 37;			
			
			if(isset($data['appointment_vehicle_id']))
			{
				$vehicle_model = vehicle_model_name($data['appointment_vehicle_id']);
				$leadData['service_survey_vehicle'] = $vehicle_model->model_name.' '.$vehicle_model->model_year;
			}
			else{
				$vehicle_model = vehicle_type_name($data['appointment_vehicle_type_id']);
				$leadData['service_survey_vehicle'] = $vehicle_model->name;
			}
			
			$leadData['comments'] = "This lead is auto generated from appointments!";
			$leadData['created_at'] = date('Y-m-d H:i:s');
			return $insert_lead_id = $this->Model_lead->save($leadData);
		} else {
			return $data['lead_id'];
		}
	}
	
	public function check_appointment($appointment_start_time, $params)
	{
		$parameters = explode("||", $params);
		if(isset($parameters[2]))
			$count = $this->Central_model->count_rows("appointment", array("appointment_start_time" => $appointment_start_time, "branch_id" => $parameters[0], "adviser_id" => $parameters[1], "is_deleted" => 0), array("id" => $parameters[2]));
		else 
			$count = $this->Central_model->count_rows("appointment", array("appointment_start_time" => $appointment_start_time, "branch_id" => $parameters[0], "adviser_id" => $parameters[1], "is_deleted" => 0));
		if ($count > 0)
		{
				$this->form_validation->set_message('check_appointment', 'This slot is already booked!');
				return FALSE;
		}
		else
		{
				return TRUE;
		}
	}
	
	public function change_status()
	{
		$response = array();
		if($this->input->method(TRUE) == 'POST') {
			$id = $this->Central_model->update("appointment", array("status" => $this->input->post('status')), 'id', $this->input->post('id'));
	        $data = $this->Model_appointment->get($this->input->post('id'));
	        $branch = $this->Model_branch->get($data->branch_id);

                // Appointment Fulfilled
                if($this->input->post('status') == 7) {
                    //Latest
                    //send_sms(array("phone_no" => str_replace(array('(', ')', ' '), array('', '', ''), '00923219410733'), "sms_text" => "شكرًا لزیارتكم مركز صیانة مرسیدس- بنز نأمل أن تكون الخدمة المقدمة قد حازت على رضاكم ونتطلع إلى اعطاءنا تقییم عالي حین استقبال مكالمة تقییم الخدمة خلال الأیام القادمة.. نتطلع لخدمتك مجددًا " . " Regards Leads System"));

                    
                    // English SMS
                    //send_sms(array("phone_no" => str_replace(array('(', ')', ' '), array('', '', ''), '+923219410733'), "sms_text" => "Many thanks for your visit today. Here at Leads System we strive to ensure that every visit to our Service Centers meets or even exceeds your expectation. You may receive a call in the next day’s regarding your experience with us today. We are very happy to receive your feedback and where you are completely satisfied we would appreciate you to reflect this in the survey by scoring us a 10! We look forward to seeing you for your next Service appointment." . "\n" ." Regards Leads System"));

                    $emailData['first_name'] = $data->name;
                    if($data->email){
                        sendEmail(15, $data->email, $emailData);
                    }
                }
                // Appointment No show
                elseif ($this->input->post('status') == 8) {

                    //Latest
                    //send_sms(array("phone_no" => str_replace(array('(', ')', ' '), array('', '', ''), '00923219410733'), "sms_text" => "عزیزي  ".$data->name.", شكرًا لتواصلكم معنا، تم تأكید موعدكم مع فریق الصیانة ویسعدنا استقبالكم في  $branch->branch_title_ar, ".date("F j, Y, g:i a", strtotime($data->appointment_start_time)). " ولحجز موعد جدید الرجاء التواصل مع مركز خدمة العملاء على الرقم (8002443000) " ." Regards Leads System"));

                    // English SMS
                    //send_sms(array("phone_no" => str_replace(array('(', ')', ' '), array('', '', ''), '+923219410733'), "sms_text" => "Dear ".$data->name .", We expected your visit to our Service Center today at 
                    //$branch->title".date("F j, Y, g:i a", strtotime($data->appointment_start_time))." Unfortunately, you did not arrive for your appointment. Please contact 8002443000 in order to book a new appointment. Thank you, Regards Leads System"));

                    $emailData['first_name'] = $data->name;
                    $emailData['branch_title_ar'] = $branch->branch_title_ar;
                    $emailData['branch_title_en'] = $branch->title;
                    $emailData['appointment_start_time'] = $data->appointment_start_time;

                    if($data->email){
                        sendEmail(16, $data->email, $emailData);
                    }
                }
                // Appointment Canceled
                elseif ($this->input->post('status') == 9) {

                    //latest
                    //send_sms(array("phone_no" => str_replace(array('(', ')', ' '), array('', '', ''), '00923219410733'), "sms_text" => "عزیزي  ".$data->name.", تم تأكید طلب إلغاء موعد الصیانة الخاص بكم في 
                    //$branch->branch_title_ar, ".date("F j, Y, g:i a", strtotime($data->appointment_start_time)). " ولحجز موعد جدید الرجاء التواصل مع مركز خدمة العملاء على الرقم (8002443000) " ." Regards Leads System"));

                    //English SMS
                    //send_sms(array("phone_no" => str_replace(array('(', ')', ' '), array('', '', ''), '+923219410733'), "sms_text" => "Dear ".$data->name .", Confirming the cancellation of your Service appointment at  
                    //$branch->title".date("F j, Y, g:i a", strtotime($data->appointment_start_time))." Regards Leads System"));

                    $emailData['first_name'] = $data->name;
                    $emailData['branch_title_ar'] = $branch->branch_title_ar;
                    $emailData['branch_title_en'] = $branch->title;
                    $emailData['appointment_start_time'] = $data->appointment_start_time;

                    if($data->email){
                        sendEmail(14, $data->email, $emailData);
                    }

                    $this->delete_appointment();
                }
            echo json_encode(array(
                'success' => true
            ));
            exit();
			if($id) {
				if($this->input->post('status') == 0) 
					$status = "On Time"; 
				elseif($this->input->post('status') == 1) 
					$status = "Delayed"; 
				elseif($this->input->post('status') == 2) 
					$status = "No Show";
				$this->lead_comment($id, $status);
				$this->close_lead($id);
				echo json_encode(array(
					'success' => true
				));
			}
		}
	}
	
	public function close_lead($id)
	{
		$row = $this->Model_appointment->get($id);
		$dataMsg['created_at'] = date('Y-m-d H:i:s');
		$dataMsg['message_id'] = 11;
		$dataMsg['lead_id'] = $row->lead_id;
		$this->Model_leads_messages->save($dataMsg);
		
		$leadCloseData = array();
		$leadCloseData['close_time'] = date('Y-m-d H:i:s');
		$leadCloseData['status'] = "Closed";
		$this->Model_lead->update($leadCloseData,array("id"=>$row->lead_id));
	}

	public function advisersByBid()
	{
		$response = array(); 
		$branchId = $this->input->post('bid'); 
		$advisers = $this->Model_appointment->getAdviserUsersByBidFromOrgStruc($branchId);
		//print_r($advisers); die();
		echo json_encode(array(
			'advisers' => $advisers
		));		
		exit();
	}
	public function review_appointment()
	{
		$response = array(); 
		$data['appointment'] = $this->Model_appointment->get($this->input->post('id'));
		$html = $this->load->view('appointments/review/review', $data, true);
		echo json_encode(array(
			'data' => $data['appointment'],
			'html' => $html
		));		
		exit();
	}
	public function delete_appointment()
	{
		$response = array(); 
		$id = $this->Central_model->update("appointment", array("is_deleted" => 1), 'id', $this->input->post('id'));
		if($id) {
			$this->close_lead($this->input->post('id'));
			echo json_encode(array(
				'success' => true
			));		
			exit();
		}	
	}
	public function lead_comment($id, $comment)
	{
		$row = $this->Model_appointment->get($id);
		$dataMsg['created_at'] = date('Y-m-d H:i:s');
		$dataMsg['message_id'] = 4;
		$dataMsg['comments'] = $comment;
		$dataMsg['lead_id'] = $row->lead_id;
		$this->Model_leads_messages->save($dataMsg);
	}
	public function update()
	{
		$data = array();
		if($this->input->method(TRUE) == 'POST') {
			$this->load->library('form_validation');
			$data = $this->input->post();
            $branchTitleEn = isset($data['branchTitleEn']) ? $data['branchTitleEn'] : '';
            $branchTitleAr = isset($data['branchTitleAr']) ? $data['branchTitleAr'] : '';
            unset($data['branchTitleAr'], $data['branchTitleEn']);
			$rules = array(
				[
					'field' => 'id',
					'label' => 'ID',
					'rules' => 'trim|required',
				],
				[
					'field' => 'name',
					'label' => 'Full Name',
					'rules' => 'trim|required',
				],
				[
					'field' => 'mobile',
					'label' => 'Mobile',
					'rules' => 'trim|required',
				],
				[
					'field' => 'type',
					'label' => 'Type',
					'rules' => 'trim|required',
				],
				[
					'field' => 'appointment_vehicle_id',
					'label' => 'Vehicle',
					'rules' => 'trim|required',
				],
				[
					'field' => 'branch_id',
					'label' => 'Branch',
					'rules' => 'trim|required',
				],
				[
					'field' => 'appointment_start_time',
					'label' => 'Appointment Time',
					'rules' => 'trim|required|callback_check_appointment['.$data['branch_id'].'||'.$data['adviser_id'].'||'.$data['id'].']',
				],
				[
					'field' => 'adviser_id',
					'label' => 'Adviser',
					'rules' => 'trim|required',
				],
			);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run())
			{
				//==========Start also allowed to set the vehicle type========
				if( strpos($data['appointment_vehicle_id'],"type-") !== false )
				{
					//its a parent id that is type id.
					$data['appointment_vehicle_type_id'] = str_replace("type-","",$data['appointment_vehicle_id']);
					unset($data['appointment_vehicle_id']);
				}
				else
				{
					//$data['appointment_vehicle_id'] is a model id
					$data['appointment_vehicle_id'] = str_replace("model-","",$data['appointment_vehicle_id']);
				}				 
				//==========End also allowed to set the vehicle type========
                // Counting Daily Appointment for the selected Adviser
                $this->getCountDailyAppointments($data, $data['id']);
                // ========= End Counting

				$data['appointment_end_time'] = ($data['type'] == 0 ? date("Y-m-d H:i:s", strtotime('+15mins', strtotime($data['appointment_start_time']))) : date("Y-m-d H:i:s", strtotime('+30mins', strtotime($data['appointment_start_time']))));
				$data['updated_by'] = $this->session->userdata['user']['id'];
				$data['updated_at'] = date("Y-m-d H:i:s");
				$id = $this->Central_model->update("appointment",$data, "id", $data['id']);
				if($id) {					
					//send_sms(array("phone_no" => str_replace(array('(', ')', ' '), array('', '', ''), $data['mobile']), "sms_text" => "Dear ".$data['name'].", this is to inform you that your appointment for vehicle service is updated at ".date("F j, Y, g:i a", strtotime($data['appointment_start_time']))." Regards Leads System"));

                    //Latest
                    //send_sms(array("phone_no" => str_replace(array('(', ')', ' '), array('', '', ''), '00923219410733'), "sms_text" => "عزیزي  ".$data['name'].", نود إعلامكم بتغییر موعد الصیانة الخاص بكم بناء على طلبكم وذلك الى الموعد الجدید في  $branchTitleAr, ".date("F j, Y, g:i a", strtotime($data['appointment_start_time']))." Regards Leads System"));

                    // English SMS
                    //send_sms(array("phone_no" => str_replace(array('(', ')', ' '), array('', '', ''), '+923219410733'), "sms_text" => "Dear ".$data['name'].", thank you for rescheduling your Service appointment. The new appointment is at  $branchTitleEn".date("F j, Y, g:i a", strtotime($data['appointment_start_time']))." Regards Leads System"));

                    $emailData['first_name'] = $data['name'];
                    $emailData['branch_title_ar'] = $branchTitleAr;
                    $emailData['branch_title_en'] = $branchTitleEn;
                    $emailData['appointment_start_time'] = $data['appointment_start_time'];
                    $emailData['appointment_end_time'] = $data['appointment_end_time'];
                    if($data['email']){
                        sendEmail(12, $data['email'], $emailData);
                    }
					// Farooq
					$this->lead_comment($id, 'Appointment Updated, Appointment Time: '.date("j M Y - g:i A", strtotime($data['appointment_start_time'])));


					echo json_encode(array(
						'success' => true
					));
				}
			} else {
				$errors = array(
					'appointment_start_time' => form_error('appointment_start_time')
				); 				
				echo json_encode(array(
					'errors' => $errors,
					'success' => false,
				));
			}
		}
	}
	public function fetch_name()
	{
		$data = array();
		$mobile = $this->input->post('mobile');
		$row = $this->Model_lead->getWithMultipleFields(array("mobile" => $mobile));
		echo json_encode(array(
			'name' => ($row ? $row->title.' '.$row->first_name.' '.$row->surname : '') 
		));
	}

	public function getCountDailyAppointments($data, $id = null)
    {
        $created_at_start = date("Y-m-d", strtotime($data['appointment_start_time'])) . " 00:00:00";
        $created_at_end = date("Y-m-d", strtotime($data['appointment_start_time'])) . " 23:59:59";

        $appointment_counts = $this->Central_model->countAppointmentByAdviserId("appointment", array("adviser_id" => $data['adviser_id'], 'appointment_start_time >=' => $created_at_start, 'appointment_start_time <=' => $created_at_end), $id);

        if (($appointment_counts >= 8 && $data['type'] == 1) || ($appointment_counts >= 12 && $data['type'] == 0)) {
            $errors = array(
                'appointment_counts' => "Daily Appointment counts reached at it maximum"
            );
            echo json_encode(array(
                'errors' => $errors,
                'success' => false,
            ));
            exit();
        }
    }

    public function appointment_fulfilled() {
        $id = $this->input->post('id');
        $data = [
            'id' => $id,
            'status' => 7
        ];
        $status = $this->Central_model->update('appointment', $data);

    }


} 