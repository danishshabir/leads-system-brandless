<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	private $allChild = array();

	public function __construct()
    {
        parent::__construct();
		checkAdminSession();
       $this->load->model('Model_report');
	   $this->load->model('Model_city');
	   $this->load->model('Model_category');
	   $this->load->model('Model_lead');
	   $this->load->model('Model_news');
	   $this->load->model('Model_leads_messages');
	   $this->load->model('Model_messages');
	   $this->load->model('Model_notification');
	   $this->load->model('Model_vehicle_specific_name');
	   $this->load->model('Model_vehicle');
	   $this->load->model('Model_structure_departments');
	   $this->load->model('Model_survey_result');
	   $this->load->model('Model_struc_dep_users');
	   $this->load->model('Model_user');
	   $this->load->model('Model_common'); //use this in every controller because it is accessed in header.
	   
    }
	
	
	
	
	public function index($check_for_fetching_leads = 'assign'){ // by default it gets reports of leads that assign to me

		$data = array();
		$data['view'] = 'dashboard/dashboard';
		$users = "";
		$branchIdOfLoggedInManager = 0;
		$branchIdOfLoggedInSubMng = 0;
		$branchId = 0;
		$data['managerView'] = false;

		$checkLoggedInUserInboxRole = checkLoggedInUserInboxRole();

		$loggedInUserManagingDeps = getLoggedInUserManagingDeps();		
		if(is_array($loggedInUserManagingDeps) && count($loggedInUserManagingDeps)>0)
		{						
			//logged in user is a  manager from org structure for one or more departments.
			//get users under him so this manager can see the leads of his users too.
			$users = $this->usersUnderMngRecursive();

			$branchIdOfLoggedInManager = getBranchIdOfUserByUserId($this->session->userdata['user']['id']);
			$branchId = $branchIdOfLoggedInManager;
			$data['managerView'] = true;
		}
		else
		{

			//its mean logged in user is not a manager. Now check if the logged in user is a lead controller?
			if( rights(33,'read') ) //33 is lead controller
			{
				//logged in user is a submanager(lead controller) now get all the users of his departments.
				$users = $this->usersUnderSubmngRecursive();
				$branchIdOfLoggedInSubMng = getBranchIdOfUserByUserId($this->session->userdata['user']['id']);
				$branchId = $branchIdOfLoggedInSubMng;
				$data['managerView'] = true;

			}
		}
		$usersArr = explode(",",$users); //convert to array // users under the manger or sub manager
		
		//new faster===
		$branch_id_for_filter = 0;
		$simple_user_id_for_filter = 0;
		if(!rights(35,'read'))
		{
			if((int)$branchIdOfLoggedInManager !== 0) $branch_id_for_filter = $branchIdOfLoggedInManager;
			elseif((int)$branchIdOfLoggedInSubMng !== 0) $branch_id_for_filter = $branchIdOfLoggedInSubMng;
			elseif(!rights(35,'read') && !$branch_id_for_filter) $simple_user_id_for_filter = $this->session->userdata['user']['id'];
		}

		if($data['managerView'])
		{
			$data['depAvgScore'] = $this->Model_lead->getDepAvgScore($users);
		}
				
		if(rights(35,'read') || $data['managerView'])
		{
			$data['unAssignedCount'] = $this->Model_lead->getUnAssignedCount($branch_id_for_filter);	
			$data['allScore'] = $this->Model_lead->getAllScore($users);	
		}
		
		if(!rights(35,'read') and !$data['managerView'])
		{
			$data['userAvgScore'] = $this->Model_lead->getUserAvgScore($this->session->userdata['user']['id']);	
			$depUsers = $this->Model_lead->getDepUsers($this->session->userdata['user']['id']);	
			$data['depAvgScore'] = $this->Model_lead->getDepAvgScore($depUsers);
			$data['usersVehiclesCount'] = $this->Model_lead->getUsersVehiclesCount($this->session->userdata['user']['id']);			
		}		
		
		//$data['activeLeadsCount'] = $this->Model_lead->getActiveLeadsCount($branch_id_for_filter,$simple_user_id_for_filter);
		
			//===
			$sepcialData['countOnly'] = 1;
			$countOnlyRes = $this->Model_lead->getAllSortedByPaged(1,0,1,$users,"",$checkLoggedInUserInboxRole,false,0,"",$branchIdOfLoggedInManager,$branchIdOfLoggedInSubMng,$sepcialData);
			$data['activeLeadsCount'] = $countOnlyRes[0]->count; 
			//===
			
		
		$data['assignedDelayedCounter'] = $this->Model_lead->getAssignedDelayedCounter($branch_id_for_filter,$simple_user_id_for_filter);		
		$data['assignedNoActionCounter'] = $this->Model_lead->getAssignedNoActionCounter($branch_id_for_filter,$simple_user_id_for_filter);
		
		if($branchIdOfLoggedInManager or $branchIdOfLoggedInSubMng)
		{
			$data['usersLeadCount'] = $this->Model_lead->getUsersLeadCount($users);			
		}
		
		
		//end faster====
				
		
		//==========Start for graphs===============

		
		
		//instead of users under manager. all users
		/*if( rights(35,'read') ) //if the user have the role of view all leads
		{
			$usersArr = array();
			$usersAll = $this->Model_user->getAll();
			
			if($usersAll)
			foreach($usersAll as $user)
			{						
				$usersArr[] = $user->id;
			}
		}*/


		//Without Connected leads. Total leads
				//$allViewAbleLeadsForThisUser = $this->Model_lead->getAllSortedByPaged(0,0,0,$users,"",$checkLoggedInUserInboxRole,false,0,"",$branchIdOfLoggedInManager,$branchIdOfLoggedInSubMng);	
				//$data['totalLeadsCountWithOutConnected'] = 0;
				//if($allViewAbleLeadsForThisUser)
				//$data['totalLeadsCountWithOutConnected'] = count($allViewAbleLeadsForThisUser);


			//Further calculations
				//$data['totalConnectedLeadsCount'] = 0;
				//$data['totalConnectedAssignedLeadsCount'] = 0;
				//$data['totalAssignedLeadsCountWithOutConnected'] = 0;
				//$data['totalAssignedDelayedWithOutConnected'] = 0;
				//$data['totalAssignedNotDelayedWithOutConnected'] = 0;
				//$data['totalUnAssignedDelayedWithOutConnected'] = 0;
				//$data['totalUnAssignedWithOutConnected'] = 0;
				//$data['totalUnAssignedNotDelayedWithOutConnected'] = 0;
				//$data['totalUnAssignedConnected'] = 0;
				//$data['totalUnAssignedDelayedConnected'] = 0;
				//$data['totalUnAssignedNotDelayedConnected'] = 0;				
				//$data['totalAssignedDelayedConnected'] = 0;
				//$data['totalAssignedNotDelayedConnected'] = 0;
				//$data['totalActionTakenLeadsWithoutConnected'] = 0;
				//$data['totalNotActionTakenLeadsWithoutConnected'] = 0;
				//$data['totalActionTakenLeadsConnected'] = 0;
				//$data['totalNotActionTakenLeadsConnected'] = 0;

				//$data['totalSatisfactionUserAvgWithOutConnected'] = 0; 
				//$data['totalSatisfactionUserAvgConnected'] = 0;
				//$data['totalSatisfactionBranchAvg'] = 0;
				//$data['totalSatisfactionAllBranchsAvg'] = 0;

				//$data['totalUserActiveLeads'] = 0; 
				//$data['totalManagerActiveLeads'] = 0; 

				//$data['genaralVehicleNameArr'] = array();
				//$data['genaralVehicleCountArr'] = array();
				//$data['genaralVehicleColorArr'] = array();
				//$data['genaralVehicleShrNmArr'] = array();
				//$data['generalVehicleShrNmVsNorName'] = array();				

				//$data['usersUnderMangNameArr'] = array();
				//$data['usersUnderMangColorArr'] = array();
				//$data['usersUnderMangAcLCountArr'] = array();


				//$tSUserAvgConCounter = 0;
				//$tSUserAvgWoConCounter = 0;

				//$isMain = true;


				/*if($allViewAbleLeadsForThisUser)
				{
					foreach($allViewAbleLeadsForThisUser as $lead)
					{
						
						
						$isMain = true;
						$this->dashboardHelping($isMain, $usersArr, $lead, $data, $tSUserAvgConCounter, $tSUserAvgWoConCounter);

						$subCount = 0;
						$subLeads = getSubLeads1($lead->email,$lead->id); 

						if($subLeads) 
						{
							$subCount = count($subLeads); 
							$data['totalConnectedLeadsCount'] += $subCount;		

							foreach($subLeads as $subLead){

								$isMain = false;
								$this->dashboardHelping($isMain, $usersArr, $subLead, $data, $tSUserAvgConCounter, $tSUserAvgWoConCounter);

							}
						}	
					}
					
				}*/

				//=====, it should be on id base may be?
				/*foreach($usersArr as $user) //$usersArr is basicaly just the ids array of users.
				{
					$uRes = $this->Model_user->get($user);
					if($uRes)
					{
						$uName = $uRes->full_name;
						$uColor = $uRes->pie_color;

						if(!in_array($uName,$data['usersUnderMangNameArr']))
						{
							$data['usersUnderMangNameArr'][] = $uName;
							$data['usersUnderMangColorArr'][] = $uColor;
							$data['usersUnderMangAcLCountArr'][] = 0;
						}
					}
					
				}*/
				//======


				//$branchIdOfLoggedInUser = getBranchIdOfUserByUserId($this->session->userdata['user']['id']);
				//$totalSatisfactionBranchAvg = $this->Model_survey_result->getAvgByBranchId($branchIdOfLoggedInUser);						
				//if($totalSatisfactionBranchAvg)
				//{
					//$data['totalSatisfactionBranchAvg'] = round($totalSatisfactionBranchAvg[0]->average,2);
				//}

				
				//$totalSatisfactionAllBranchsAvg = $this->Model_survey_result->getAvgAllBranchs();						
				//if($totalSatisfactionAllBranchsAvg)
				//{
					//$data['totalSatisfactionAllBranchsAvg'] = round($totalSatisfactionAllBranchsAvg[0]->average,2);
				//}


				//if($tSUserAvgWoConCounter)
				//$data['totalSatisfactionUserAvgWithOutConnected'] = $data['totalSatisfactionUserAvgWithOutConnected']/$tSUserAvgWoConCounter;

				//if($tSUserAvgConCounter)
				//$data['totalSatisfactionUserAvgConnected'] = $data['totalSatisfactionUserAvgConnected']/$tSUserAvgConCounter;



			//user satisfaction == //ignore the above calc, following is correct as it includes archived leads too.
				//$data['totalSatisfactionUserAvg'] = 0;
				//$userAvgScore = $this->Model_survey_result->getUserAvgScore($this->session->userdata['user']['id']);
				//if($userAvgScore)
					//$data['totalSatisfactionUserAvg'] = $userAvgScore->avgval;
				

		//============End For Graphs================


		
		$user_id = $this->session->userdata['user']['id'];
		$user_role = $this->session->userdata['user']['role_id'];
        $user_city = $this->session->userdata['user']['city_id'];		
        $data['vehicles_specific'] = $this->Model_vehicle_specific_name->getAll(); 
		//$data['leads_status'] = $this->Model_report->getAllStatus($user_role, $user_id,$check_for_fetching_leads);
		//$data['satisfaction'] = $this->Model_report->getSatisfaction($user_role, $user_id,'','',$check_for_fetching_leads);
		//if($check_for_fetching_leads==="assign")
			//$data['satisfaction1'] = $this->Model_report->getSatisfaction1($user_id,"Assigned");
		//else
			//$data['satisfaction1'] = $this->Model_report->getSatisfaction1($user_id,"Creator");
		//$data['satisfaction_overall1'] = $this->Model_report->getSatisfaction1(0); //user id -1 mean overall

		//$data['related_category'] = $this->Model_report->getLeadsByCategory($user_role, $user_id,'','',$check_for_fetching_leads);
		//$data['leads_by_region'] = $this->Model_report->getLeadsByRegion($user_role, $user_id,'','',$check_for_fetching_leads);

		//set variable for which the reports are showing.
		//$data['check_for_fetching_leads'] = $check_for_fetching_leads;

		//get news
		$data['newsArr'] = $this->Model_news->getMultipleRows(array(),false,'desc');

		//leads
		$pageNumber = 1;
		$dashboard = 1;
		$users = ""; //make it again empty
		$keyWord = "";
		$checkLoggedInUserInboxRole = checkLoggedInUserInboxRole();
		$applyFilter = false;
		$category_id=0;
		$status="";
		$applyLimit=1;
		$branchIdOfLoggedInManager = 0;
		$branchIdOfLoggedInSubMng = 0;

		//on the dashboard it won't show leads under the manager.
		//Ref to https://docs.google.com/spreadsheets/d/1L4XHwwEbT8uTCCYCrxZsqZ5R131AjkEOGbmE7lmOPzE/edit#gid=1213455002

		//dashboard only show those leads in which the user is either created by or assign to or inbox user.
		$data['leads'] = $this->Model_lead->getAllSortedByPaged($pageNumber, $dashboard, $applyLimit, $users, $keyWord, $checkLoggedInUserInboxRole, $applyFilter, $category_id, $status,$branchIdOfLoggedInManager,$branchIdOfLoggedInSubMng); 

		//set other imp variables for view
		$data['isSingleDetailLead'] = false;
		$data['cities'] = $this->Model_city->getAll();
		$data['logged_in_user_id'] = $this->session->userdata['user']['id'];
		$data['categories'] = $this->Model_category->getAll();
		//$data['get_late_leads'] = $this->Model_lead->getAllLateLeads($data['logged_in_user_id']);        
		$data['loadmore'] = false;
		$data['loadonlyleads'] = false;		
		$data['dashboard'] = true;


		$this->load->view('template',$data);
		
		
		}

	/*private function dashboardHelping($isMain, $usersArr, $lead, &$data, &$tSUserAvgConCounter, &$tSUserAvgWoConCounter)
	{

		if(!$isMain && !$data['managerView'] && ($lead->assign_to != $this->session->userdata['user']['id'] || $lead->created_by != $this->session->userdata['user']['id'] )) 
		return '';

		//==
		//users under manager	

		//if(count($usersArr)) //its mean there are users under manager or sub manager
		//{								
			//if($lead->status!=="Closed" && $lead->status!=="Finished" && $lead->status!=="Approved and Archived" && $lead->status!=="Duplicated")
			//{							
				//if($lead->assign_to && (int)$lead->assign_to!==(int)$this->session->userdata['user']['id'] && in_array($lead->assign_to,$usersArr)) 
				//{
					//$uRes = $this->Model_user->get($lead->assign_to);
					//$uName = $uRes->full_name;
					//$uColor = $uRes->pie_color;
					//$isUAlready = false;
					//$uIndex = array_search($uName,$data['usersUnderMangNameArr']);
					//if($uIndex!==false)
						//$isUAlready = true;

					//if($isUAlready)
					//{
						//do nothing, name already in the array just update the counter below.
					//}
					//else
					//{
						//$data['usersUnderMangNameArr'][] = $uName;
						//$data['usersUnderMangColorArr'][] = $uColor;						
					//}

					//if($isUAlready)
					//{
						//$data['usersUnderMangAcLCountArr'][$uIndex] = ((int)$data['usersUnderMangAcLCountArr'][$uIndex])+1;
					//}
					//else
						//$data['usersUnderMangAcLCountArr'][] = 1;
				//}
				
			//}					

		//}
		//== 


		//==
		$lmsg = array();
		$lmsg['lead_id'] = $lead->id;
		$lmsgRes = $this->Model_leads_messages->getMultipleRows($lmsg);
		if(is_array($lmsgRes))
		{
			foreach($lmsgRes as $leadMsg)
			{
				//vehicles
				if($leadMsg->message_id==="5" && $leadMsg->vehicle_specific_names_id!=="" && $lead->status!=="Closed" && $lead->status!=="Finished" && $lead->status!=="Approved and Archived" && $lead->status!=="Duplicated") //5; //test drive schedule
				{
					
					$vehicle_specific_names_id_arr = explode(",",$leadMsg->vehicle_specific_names_id);
					$ivc = 0;
					foreach($vehicle_specific_names_id_arr as $vsi)
					{
						$isVAlready = false;
						$vsRes = $this->Model_vehicle_specific_name->get($vsi);
						$gId = $vsRes->vehicle_id;
						$vgRes = $this->Model_vehicle->get($gId);
						$generalVehicleName = $vgRes->name;
						$bubbleGraphColor = $vgRes->bubble_graph_color;
						$generalVehicleShrNm = $vgRes->short_name;
						$gIndex = array_search($generalVehicleName,$data['genaralVehicleNameArr']);
						if($gIndex!==false)
							$isVAlready = true;

						if($isVAlready)
						{
							//do nothing, name already in the array just update the counter below.
						}
						else
						{
							$data['genaralVehicleNameArr'][] = $generalVehicleName;
							if($generalVehicleShrNm)
							{
								$data['genaralVehicleShrNmArr'][] = $generalVehicleShrNm;
								$data['genaralVehicleColorArr'][$generalVehicleShrNm] = $bubbleGraphColor; // for d.className for color picking in js
								$data['generalVehicleShrNmVsNorName'][$generalVehicleShrNm] = $generalVehicleName;
							}
							else
							{
								$data['genaralVehicleShrNmArr'][] = $generalVehicleName;
								$data['genaralVehicleColorArr'][$generalVehicleName] = $bubbleGraphColor; // for d.className for color picking in js
								$data['generalVehicleShrNmVsNorName'][$generalVehicleName] = $generalVehicleName;
							}
							
						}

						if($isVAlready)
						{
							$data['genaralVehicleCountArr'][$gIndex] = ((int)$data['genaralVehicleCountArr'][$gIndex])+1;
						}
						else
							$data['genaralVehicleCountArr'][] = 1;

						$ivc++;	
					}

				}
				//==
			}
		}
		//== 		
		
		if($lead->status!=="Closed" && $lead->status!=="Finished" && $lead->status!=="Approved and Archived" && $lead->status!=="Duplicated")
		{
			//$data['totalUserActiveLeads']++;
		}

		if($lead->status!=="Closed" && $lead->status!=="Approved and Archived" && $lead->status!=="Duplicated")
		{
			//$data['totalManagerActiveLeads']++;
		}


		if($lead->status!=="Closed" && $lead->status!=="Finished" && $lead->status!=="Approved and Archived" && $lead->status!=="Duplicated" && dueCounter($lead->due_date)==="Delayed")
		{
			//lead is Delayed. delay has more priority
		}
		else
		{
			$cac = array();
			$cac['lead_id'] = $lead->id;

			if($isMain)
				$cac['created_by !='] = "0";
			else
				$cac['created_by'] = $this->session->userdata['user']['id'];

			//$cac['message_id !='] = "19";
			$cacMsgs = $this->Model_leads_messages->getMultipleRows($cac);			


			if($data['managerView'])
			{
				if(is_array($cacMsgs) && count($cacMsgs)>0)
				{
					//if($isMain)
						//$data['totalActionTakenLeadsWithoutConnected']++;
					//else
						//$data['totalActionTakenLeadsConnected']++;

				}
				else
				{
					//if($isMain)
						//$data['totalNotActionTakenLeadsWithoutConnected']++;
					//else
						//$data['totalNotActionTakenLeadsConnected']++;
				}
			}
			else
			{
				//simple user

				if(is_array($cacMsgs) && count($cacMsgs)===1)
				{
					//if its a simple user and only one message is of assigned type and user have not taken any further action then its no action
					if($cacMsgs[0]->message_id==="19") 
					{
						//if($isMain)
							//$data['totalNotActionTakenLeadsWithoutConnected']++;
						//else
							//$data['totalNotActionTakenLeadsConnected']++;
					}
				}
				if(is_array($cacMsgs) && count($cacMsgs)>0)
				{
					//if($isMain)
						//$data['totalActionTakenLeadsWithoutConnected']++;
					//else
						//$data['totalActionTakenLeadsConnected']++;
				}							
				else
				{
					//if($isMain)
						//$data['totalNotActionTakenLeadsWithoutConnected']++;
					//else
						//$data['totalNotActionTakenLeadsConnected']++;
				}

			}


		}


		//if($lead->assign_to > 0)
		//{
			//if($isMain)			
				//$data['totalAssignedLeadsCountWithOutConnected']++;
			//else
				//$data['totalConnectedAssignedLeadsCount']++;

			//if($lead->status!=="Closed" && $lead->status!=="Finished" && $lead->status!=="Approved and Archived" && $lead->status!=="Duplicated" && dueCounter($lead->due_date)==="Delayed")
			//{
				//if($isMain)
					//$data['totalAssignedDelayedWithOutConnected']++;
				//else
					//$data['totalAssignedDelayedConnected']++;
			//}
			//else
			//{
				//if($isMain)
					//$data['totalAssignedNotDelayedWithOutConnected']++;
				//else
					//$data['totalAssignedNotDelayedConnected']++;
			//}
		//}
		//else
		//{
			//if($isMain)
				//$data['totalUnAssignedWithOutConnected']++;
			//else
			//{
				//if($lead->status!=="Approved and Archived" && $lead->status!=="Duplicated")
				//$data['totalUnAssignedConnected']++;			
			//}

			//if($lead->status!=="Closed" && $lead->status!=="Finished" && $lead->status!=="Approved and Archived" && $lead->status!=="Duplicated" && dueCounter($lead->due_date)==="Delayed")
			//{
				//if($isMain)
					//$data['totalUnAssignedDelayedWithOutConnected']++;
				//else
					//$data['totalUnAssignedDelayedConnected']++;
			//}
			//else
			//{
				//if($isMain)
					//$data['totalUnAssignedNotDelayedWithOutConnected']++;
				//else
					//$data['totalUnAssignedNotDelayedConnected']++;
			//}

		//}

	
		//===
		
		if($data['managerView'])
		{
			//logged in user is a manager or submanager. show Data: "branch Average" Vs "All branches Average"
			//both, Branch and All branches, it is out side the loop.

		}
		else
		{
			//Logged in user is a simple user so show "User Average" Vs "Branch Average" 
			//Branch Avg is outside the loop

			//if($lead->survey_response)
			//{
				// $totalSatisfactionUserAvg = $this->Model_survey_result->getAvgBySurvAndLeadId($lead->survey_id,$lead->id);						
				 //if($totalSatisfactionUserAvg)
				 //{
					//if($isMain)
					//{
					//$data['totalSatisfactionUserAvgWithOutConnected'] = $data['totalSatisfactionUserAvgWithOutConnected'] + $totalSatisfactionUserAvg[0]->average;
					//$tSUserAvgWoConCounter++;

					//}else{
						//$data['totalSatisfactionUserAvgConnected'] = $data['totalSatisfactionUserAvgConnected'] + $totalSatisfactionUserAvg[0]->average;
						//$tSUserAvgConCounter++;
					//}
					
				 //}							 
			//}

		}
		
		//===
	}*/
	

	//same copy in Lead contoller
	private function usersUnderMngRecursive() //gives comma sep users under manager.
	{
		$users = array(); //All the users under this manager.

		$loggedInUserManagingDeps = getLoggedInUserManagingDeps();		
		if(is_array($loggedInUserManagingDeps) && count($loggedInUserManagingDeps)>0)
		{
			foreach($loggedInUserManagingDeps as $dep)
			{
				$this->allChild = array();
				$this->allChild[] = $dep->id; //also include itself
				$this->getStrucChildren($dep->id); //it will update an object level array varible with the child dep ids				
				$allChildArr = $this->allChild;
				if($allChildArr)
				{
					foreach($allChildArr as $chDepId)
					{
						$usersUnderMngr = getUsersUnderMngr($chDepId);
						
						if($usersUnderMngr)
						foreach($usersUnderMngr as $depUser)
						{
							//if($users!="") $users .= ",";
							if(!in_array($depUser->user_id,$users))
							$users[] = $depUser->user_id;
						}
						//include managers too.
						$depRow = $this->Model_structure_departments->get($chDepId);
						$users[] = $depRow->manager_id;
					}
				}
			}		
		}

		//print_r($users); exit();
		$users = implode(",",$users);
		$users = str_replace(",0","",$users);

		return $users;
	}


	//same copy in Lead controller
	//current logic is that the sub manager can see all the leads as the manager. Even the further nested department leads.
	private function usersUnderSubmngRecursive() //gives comma sep users under manager.
	{
		$users = array(); //all other users in his departments.
		$this->allChild = array(); //reset object level array

		if( rights(33,'read') ) //lead controller e.g sub manager.
		{

			$wdu = array();
			$wdu['user_id'] = $this->session->userdata['user']['id'];
			$thisUserDeps = $this->Model_struc_dep_users->getMultipleRows($wdu);			

			if(is_array($thisUserDeps) && count($thisUserDeps)>0)
			{
				foreach($thisUserDeps as $dep)
				{
					$this->allChild = array();
					$this->allChild[] = $dep->struc_dep_id; //also include itself
					$this->getStrucChildren($dep->struc_dep_id); //it will update an object level array varible with the child dep ids				
					$allChildArr = $this->allChild;
					if($allChildArr)
					{
						foreach($allChildArr as $chDepId)
						{
							$usersUnderMngr = getUsersUnderMngr($chDepId);
							
							if($usersUnderMngr)
							foreach($usersUnderMngr as $depUser)
							{
								//if($users!="") $users .= ",";
								if(!in_array($depUser->user_id,$users) && is_numeric($depUser->user_id))
								$users[] = $depUser->user_id;
							}
							//include managers too.
							$depRow = $this->Model_structure_departments->get($chDepId);
							if(is_numeric($depRow->manager_id))
								$users[] = $depRow->manager_id;
						}
					}
				}		
			}

		}

	
		$users = implode(",",$users);
		$users = str_replace(",0"," ",$users);
		return $users;
	}

	//same copy in Lead Controller
	private function getStrucChildren($id) 
	{

		$where['parent_id'] = $id;
		$deps = $this->Model_structure_departments->getMultipleRows($where);

		$children = array();
		
		if($deps) {
			# It has children, let's get them.
			//while($row = mysql_fetch_array($r)) {
			foreach($deps as $dep) {
				# Add the child to the list of children, and get its subchildren
				$this->allChild[] = $dep->id;
				$children[$dep->id] = $this->getStrucChildren($dep->id);				
			}
		}

		return $children;
	}

}
