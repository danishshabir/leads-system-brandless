<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Manager_report extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	  public function __construct()

    {

		parent::__construct();
		

		checkAdminSession();

      	$this->load->model('Model_manager_report');

	    $this->load->model('Model_city');

		$this->load->model('Model_category');

		$this->load->model('Model_user');

		$this->load->model('Model_branch');

		$this->load->model('Model_struc_dep_users');

		$this->load->model('Model_common'); //use this in every controller because it is accessed in header.

			
		
        //$res = checkLevels(2);

		//checkAuth($res);

    }

	

	

	public function index(){

	
		$data = array();   

		$data['managers'] = $this->Model_common->getManagers();		
		
		//print_r($data['managers'] ); exit();

		

		$filterData = $this->input->get(); 		

		

		//====

		$is_manager = false;

		$depId = 0;

		for($i=0; $i<count($data['managers']); $i++)

		{

			if($data['managers'][$i]->id == $this->session->userdata['user']['id'])

			{

				$is_manager = true;

				$depId = $data['managers'][$i]->dep_id;

				break;

			}			

		}

		

		//if( rights(33,'read') ) //if logged in user is a sub manager then same graph as of manager.

		//{

			$is_manager = true;

			$depId = $this->Model_common->getUserDep($this->session->userdata['user']['id']);			

		//}

		
	
		

			if(isset($_GET["abha"])) $depId = 54;

			if(isset($_GET["madina"])) $depId = 80;

			

			//if($this->session->userdata['user']['id'] === "132" || $this->session->userdata['user']['id']==="133" || $this->session->userdata['user']['id']==="172" || $this->session->userdata['user']['id']==="135" || $this->session->userdata['user']['id']==="292")

			//{

				

			//}

			//elseif($is_manager)

			//{

				//if(!$depId)

				//{

					//echo "Sorry you don't have access to this page!";

					//exit();

					

				//}

				

				$filterData['manager_id_dep_id'] = $this->session->userdata['user']['id']."|".$depId;

				$_GET['manager_id_dep_id'] = $filterData['manager_id_dep_id'];

				

				

				

			//}else

			//{

			//echo "Sorry you don't have access to this page!";

			//exit();

			//}

		//===

		

		if(isset($filterData['manager_id_dep_id']) && $filterData['manager_id_dep_id']!=="0")

		{
			
			$manager_id_dep_id_arr = explode("|",$filterData['manager_id_dep_id']);

			$data['manager_id'] = $filterData['manager_id'] = $manager_id_dep_id_arr[0];

		
			$data['dep_id'] = $filterData['dep_id'] = $manager_id_dep_id_arr[1];			
	
			$filterData['depUsers'] = $this->Model_manager_report->getAllDepUsersCommaSep($filterData);
			  
			
			
			//$filterData['depUsers'];

						
              
			$data['timeToMakeCallActionEachUserAvg'] = $this->Model_manager_report->getTimeToMakeCallActionEachUserAvg($filterData); 

			$data['timeToFinishActionEachUserAvg'] = $this->Model_manager_report->getTimeToFinishActionEachUserAvg($filterData); 

			$data['numOfDisapprovedActionEachUser'] = $this->Model_manager_report->getNumOfDisapprovedActionEachUser($filterData); 

				
			
			$data['numOfSuccessfullCallActionEachUser'] = $this->Model_manager_report->getNumOfSuccessfullCallActionEachUser($filterData);

			// echo '<pre>' ;
			// print_r($data['numOfSuccessfullCallActionEachUser']);
			// die;

			//$data['numOfSuccessfullCallActionEachUser'] = ""; //this is creating issue in slow reports.

						

			$data['branchesAvgScore'] = $this->Model_manager_report->getBranchesAvgScore($filterData);			

			$data['totalAvgScore'] = $this->Model_manager_report->getTotalAvgScore($filterData); 

			$data['avgScoreYearly'] = $this->Model_manager_report->getAvgScoreYearly($filterData); 

			$data['avgScore'] = $this->Model_manager_report->getAvgScore($filterData); 

			$data['dGraphTotalLeadsYearly'] = $this->Model_manager_report->getDGraphTotalLeadsYearly($filterData);

			$data['dGraphTotalComLeadsYearly'] = $this->Model_manager_report->getDGraphTotalComLeadsYearly($filterData);

			$data['userTLeads'] = $this->Model_manager_report->getUserTLeads($filterData);

			$data['perCatAndSource'] = $this->Model_manager_report->getPerCatAndSource($filterData);

			// echo '<pre>' ;
			// print_r($data['perCatAndSource']);
			// die ;

			$data['perCatAndSourceCurrentMonth'] = $this->Model_manager_report->getPerCatAndSourceCurrentMonth($filterData);

			
			
			

			//====Start Conversions		

			$data['countDepLeads'] = $this->Model_manager_report->getCountDepLeads($filterData); 			

			$data['countDepScheduled'] = $this->Model_manager_report->getCountDepScheduled($filterData); 			

			$data['countDepCompleted'] = $this->Model_manager_report->getCountDepCompleted($filterData);			

			$data['countDepPurchased'] = $this->Model_manager_report->getCountDepPurchased($filterData);			

			$data['countDepLost'] = $this->Model_manager_report->getCountDepLost($filterData);

			

			$data['countDepLeads1'] = $this->Model_manager_report->getCountDepLeads1($filterData); 			

			$data['countDepScheduled1'] = $this->Model_manager_report->getCountDepScheduled1($filterData); 			

			$data['countDepCompleted1'] = $this->Model_manager_report->getCountDepCompleted1($filterData);			

			$data['countDepPurchased1'] = $this->Model_manager_report->getCountDepPurchased1($filterData);			

			$data['countDepLost1'] = $this->Model_manager_report->getCountDepLost1($filterData);

			

			$data['countDepLeads2'] = $this->Model_manager_report->getCountDepLeads2($filterData); 			

			$data['countDepScheduled2'] = $this->Model_manager_report->getCountDepScheduled2($filterData); 			

			$data['countDepCompleted2'] = $this->Model_manager_report->getCountDepCompleted2($filterData);			

			$data['countDepPurchased2'] = $this->Model_manager_report->getCountDepPurchased2($filterData);			

			$data['countDepLost2'] = $this->Model_manager_report->getCountDepLost2($filterData);

			

			$data['countDepLeads3'] = $this->Model_manager_report->getCountDepLeads3($filterData); 			

			$data['countDepScheduled3'] = $this->Model_manager_report->getCountDepScheduled3($filterData); 			

			$data['countDepCompleted3'] = $this->Model_manager_report->getCountDepCompleted3($filterData);			

			$data['countDepPurchased3'] = $this->Model_manager_report->getCountDepPurchased3($filterData);			

			$data['countDepLost3'] = $this->Model_manager_report->getCountDepLost3($filterData);

			

			$data['countDepLeads4'] = $this->Model_manager_report->getCountDepLeads4($filterData); 			

			$data['countDepScheduled4'] = $this->Model_manager_report->getCountDepScheduled4($filterData); 			

			$data['countDepCompleted4'] = $this->Model_manager_report->getCountDepCompleted4($filterData);			

			$data['countDepPurchased4'] = $this->Model_manager_report->getCountDepPurchased4($filterData);			

			$data['countDepLost4'] = $this->Model_manager_report->getCountDepLost4($filterData);

			

			$data['countDepLeads5'] = $this->Model_manager_report->getCountDepLeads5($filterData); 			

			$data['countDepScheduled5'] = $this->Model_manager_report->getCountDepScheduled5($filterData); 			

			$data['countDepCompleted5'] = $this->Model_manager_report->getCountDepCompleted5($filterData);			

			$data['countDepPurchased5'] = $this->Model_manager_report->getCountDepPurchased5($filterData);			

			$data['countDepLost5'] = $this->Model_manager_report->getCountDepLost5($filterData);

			

			$data['countDepLeads6'] = $this->Model_manager_report->getCountDepLeads6($filterData); 			

			$data['countDepScheduled6'] = $this->Model_manager_report->getCountDepScheduled6($filterData); 			

			$data['countDepCompleted6'] = $this->Model_manager_report->getCountDepCompleted6($filterData);			

			$data['countDepPurchased6'] = $this->Model_manager_report->getCountDepPurchased6($filterData);			

			$data['countDepLost6'] = $this->Model_manager_report->getCountDepLost6($filterData);

			

			$data['countDepLeads7'] = $this->Model_manager_report->getCountDepLeads7($filterData); 			

			$data['countDepScheduled7'] = $this->Model_manager_report->getCountDepScheduled7($filterData); 			

			$data['countDepCompleted7'] = $this->Model_manager_report->getCountDepCompleted7($filterData);			

			$data['countDepPurchased7'] = $this->Model_manager_report->getCountDepPurchased7($filterData);			

			$data['countDepLost7'] = $this->Model_manager_report->getCountDepLost7($filterData);

			

			$data['countDepLeads8'] = $this->Model_manager_report->getCountDepLeads8($filterData); 			

			$data['countDepScheduled8'] = $this->Model_manager_report->getCountDepScheduled8($filterData); 			

			$data['countDepCompleted8'] = $this->Model_manager_report->getCountDepCompleted8($filterData);			

			$data['countDepPurchased8'] = $this->Model_manager_report->getCountDepPurchased8($filterData);			

			$data['countDepLost8'] = $this->Model_manager_report->getCountDepLost8($filterData);

			

			$data['countDepLeads9'] = $this->Model_manager_report->getCountDepLeads9($filterData); 			

			$data['countDepScheduled9'] = $this->Model_manager_report->getCountDepScheduled9($filterData); 			

			$data['countDepCompleted9'] = $this->Model_manager_report->getCountDepCompleted9($filterData);			

			$data['countDepPurchased9'] = $this->Model_manager_report->getCountDepPurchased9($filterData);			

			$data['countDepLost9'] = $this->Model_manager_report->getCountDepLost9($filterData);

			

			$data['countDepLeads10'] = $this->Model_manager_report->getCountDepLeads10($filterData); 			

			$data['countDepScheduled10'] = $this->Model_manager_report->getCountDepScheduled10($filterData); 			

			$data['countDepCompleted10'] = $this->Model_manager_report->getCountDepCompleted10($filterData);			

			$data['countDepPurchased10'] = $this->Model_manager_report->getCountDepPurchased10($filterData);			

			$data['countDepLost10'] = $this->Model_manager_report->getCountDepLost10($filterData);

			$data['carsConversions3'] = $this->Model_manager_report->getCarsConversions3($filterData); 

			

			$data['carsConversions'] = $this->Model_manager_report->getCarsConversions($filterData);  

			

			

			//====End Conversions

			

		

		}

		

		$data['view'] = 'report/manager_report';

		$this->load->view('template',$data);		

		



		

	}

	

	public function d3_bars_hierarchical_cars($dep_id=0, $date_from="", $date_to="")

	{

		$filterData = array();

		$filterData['date_from'] = $date_from;

		$filterData['date_to'] = $date_to;

		$filterData['dep_id'] = $dep_id;			

		$filterData['depUsers'] = $this->Model_manager_report->getAllDepUsersCommaSep($filterData);

		

		$getCarTypes = $this->Model_manager_report->getCarTypes($filterData);

	   
		

		$carsEachBranch = $this->Model_manager_report->getCarsEachBranch($filterData);

		$gCarTypeEachBranch = $this->Model_manager_report->getGCarTypeEachBranch($filterData);		

		$cars = $getCarTypes[0];

		$branchesSC = $getCarTypes[1];



		$gCarType = $this->Model_manager_report->getGCarType($filterData);

		$cars = array_unique(array_merge($cars, $gCarType[0]));

		$cars = array_values($cars);



		$branches = $gCarType[1];



		foreach($branches as $gid=>$branch)

		{

			if(array_key_exists($gid,$branchesSC))

			{

				//general $branch 

				//SC $branchesSC[$gid];

				$tempGBsArr = explode(",",$branch);

				$tempBsArr = explode(",",$branchesSC[$gid]);



				foreach($tempBsArr as $tempB)

				{

					if(!in_array($tempB, $tempGBsArr))

					{

						$branches[$gid] = $branches[$gid].",".$tempB;

					}

				}



			}



		}

		

		$json_data = '

		{"name": "Cars","children": [';

		for($carCounter=0; $carCounter<count($cars); $carCounter++)

		{			

			$gId = $cars[$carCounter];

			

			if(isset($branches[$gId]))

			{

				

				$gCarName = getGeneralVehicleNameById($gId);

				if($gCarName==="N/A") continue;

				if($carCounter>0) $json_data .= ',';

				$json_data .= '{"name": "'.$gCarName.'", ';

				$json_data .= '"children": [';



				

					

				$branchesArr = explode(",",$branches[$gId]);

				$branchesArr = array_unique($branchesArr);

				$bCounter = 0;

				/*foreach($branchesArr as $bId)

				{

					if($bCounter>0) $json_data .= ',';

					//$json_data .= '{"name": "'.getBranchName($bId).'", ';// "size": "20"}



					//$json_data .= '"children": [';





					$gCars = array();

					$addComma = false;

					if(isset($gCarTypeEachBranch[1][$bId]))

					{

						$gCars = $gCarTypeEachBranch[1][$bId];

						$gCarsArr = explode(",",$gCars);                  //Requests

						$gCarsArr = array_count_values($gCarsArr);										

						if(isset($gCarsArr[$gId]))

						{						

							$json_data .= '{"name": "Requests", "size": '.$gCarsArr[$gId].'}';

							$addComma  = true;

						}



					}



					if(isset($carsEachBranch[1][$bId])) 

					{					

						$scheduledArr = explode(",",$carsEachBranch[1][$bId]);                     //Scheduled

						$scheduledArr = array_count_values($scheduledArr);										

						if(isset($scheduledArr[$gId]))

						{

							if($addComma) $json_data .= ',';

							$json_data .= '{"name": "Scheduled", "size": '.$scheduledArr[$gId].'}';		

							$addComma = true;

						}										

					}



					if(isset($carsEachBranch[2][$bId]))

					{						

						$completedArr = explode(",",$carsEachBranch[2][$bId]);                   //Completed

						$completedArr = array_count_values($completedArr);

							if(isset($completedArr[$gId]))

							{

								if($addComma) $json_data .= ',';

								$json_data .= '{"name": "Completed", "size": '.$completedArr[$gId].'}';

							}

					}

					



					//$json_data .= ']}';



					$bCounter++;

				}*/

				foreach($branchesArr as $bId)

				{

					if($bCounter>0) $json_data .= ',';

					//$json_data .= '{"name": "'.getBranchName($bId).'", ';// "size": "20"}



					//$json_data .= '"children": [';

					$completedCount = 0;

					if(isset($carsEachBranch[2][$bId]))

					{						

						$completedArr = explode(",",$carsEachBranch[2][$bId]);                   //Completed

						$completedArr = array_count_values($completedArr);

							if(isset($completedArr[$gId]))

							{

								$completedCount = $completedArr[$gId];

							}

					}



					$gCars = array();

					$addComma = false;

					if(isset($gCarTypeEachBranch[1][$bId]))

					{

						$gCars = $gCarTypeEachBranch[1][$bId];

						$gCarsArr = explode(",",$gCars);                  //Requests

						$gCarsArr = array_count_values($gCarsArr);										

						if(isset($gCarsArr[$gId]))

						{			

							$notCompleted = $gCarsArr[$gId]-$completedCount;					

							$json_data .= '{"name": "Not Completed", "size": '.$notCompleted.'}';

							$addComma  = true;

						}



					}



					if($completedCount)

					{												

						if($addComma) $json_data .= ',';

						$json_data .= '{"name": "Completed", "size": '.$completedCount.'}';						

					}

					



					//$json_data .= ']}';



					$bCounter++;

				}

				

				$json_data .= ']}';

			}

		}

		$json_data .= ']}';



		echo $json_data;

		exit();





	



	}

	

	public function d3_bars_hierarchical_cars2($dep_id=0, $date_from="", $date_to="")

	{

		$filterData = array();

		$filterData['date_from'] = $date_from;

		$filterData['date_to'] = $date_to;

		$filterData['dep_id'] = $dep_id;			

		$filterData['depUsers'] = $this->Model_manager_report->getAllDepUsersCommaSep($filterData);

		$usersRes = $this->Model_manager_report->getUsersRes($filterData['depUsers']);



		$numOfCarsEachUser = $this->Model_manager_report->getCarTypeEachUser($filterData);

		$gCarTypeEachUser = $this->Model_manager_report->getGCarTypeEachUser($filterData);

		



		/*$json_data = '

		{"name": "Cars","children": [';*/

		$json_data = '';



		//$allCities = $this->Model_manager_report->getCitiesWithBranches();

		//echo "<pre>";

		//print_r($allCities); exit();

		

		//for($cityCounter=0; $cityCounter<count($allCities); $cityCounter++)

		//{

			

			//if($cityCounter>0) $json_data .= ',';

			//$json_data .= '{"name": "'.$allCities[$cityCounter]->title.'", ';



			//$bWhere['city_id'] = $allCities[$cityCounter]->id;

			//$branchesRes = $this->Model_branch->getMultipleRows($bWhere);

			//if($branchesRes)

			//{

				//$json_data .= '"children": [';

				//for($branchCounter=0; $branchCounter<count($branchesRes); $branchCounter++)					

				//{

					//if($branchCounter>0) $json_data .= ',';

					//$json_data .= '{"name": "'.$branchesRes[$branchCounter]->title.'", ';



					//$uWhere['branch_id'] = $branchesRes[$branchCounter]->id;

					//$usersRes = $this->Model_user->getMultipleRows($uWhere);	

					

					$json_data .= '{"name": "Users","children": [';

					if($usersRes)

					{

						//$json_data .= '"children": [';

						for($userCounter=0; $userCounter<count($usersRes); $userCounter++)					

						{

							if($userCounter>0) $json_data .= ',';

							$json_data .= '{"name": "'.$usersRes[$userCounter]->full_name.'", '; 



							$uId = $usersRes[$userCounter]->id;

							if(isset($gCarTypeEachUser) && in_array($uId,$gCarTypeEachUser[0]))

							{

								$json_data .= '"children": [';



																									

								$gCarTypeEachUserArr = explode(",",$gCarTypeEachUser[1][$uId]); //Requested

								$gCarTypeEachUserArr = array_count_values($gCarTypeEachUserArr);



								$carsOfThisUserArr = array();

								if(isset($numOfCarsEachUser[1][$uId]))

								{

									$carsOfThisUserArr = explode(",",$numOfCarsEachUser[1][$uId]); //Scheduled

									$carsOfThisUserArr = array_count_values($carsOfThisUserArr);

								}



								$carsOfThisUserArr2 = array();

								if(isset($numOfCarsEachUser[2][$uId]))

								{

									$carsOfThisUserArr2 = explode(",",$numOfCarsEachUser[2][$uId]); //Completed (Including walkin)

									$carsOfThisUserArr2 = array_count_values($carsOfThisUserArr2);	

								}



								$gCarsCounter = 0;

								/*foreach($gCarTypeEachUserArr as $carName=>$count)

								{

									if(getGeneralVehicleNameById($carName)==="N/A") continue;



									if($gCarsCounter>0) $json_data .= ',';

									$json_data .= '{"name": "'.getGeneralVehicleNameById($carName).'", '; //	"size": '.$count.'}	

									$json_data .= '"children": [';

									$json_data .= '{"name": "Requests", "size": '.$count.'}';



									if(isset($carsOfThisUserArr[$carName]))

									{

										$json_data .= ',';

										$json_data .= '{"name": "Scheduled", "size": '.$carsOfThisUserArr[$carName].'}';

									}

									if(isset($carsOfThisUserArr2[$carName]))

									{

										$json_data .= ',';

										$json_data .= '{"name": "Completed", "size": '.$carsOfThisUserArr2[$carName].'}';

									}



									$json_data .= ']}';

									$gCarsCounter++;

								}*/

								

								foreach($gCarTypeEachUserArr as $carName=>$count)

								{

									if(getGeneralVehicleNameById($carName)==="N/A") continue;



									$completedCount = 0;

									if(isset($carsOfThisUserArr2[$carName]))

									{

										$completedCount = $carsOfThisUserArr2[$carName];

									}

									

									if($gCarsCounter>0) $json_data .= ',';

									$json_data .= '{"name": "'.getGeneralVehicleNameById($carName).'", '; //	"size": '.$count.'}	

									$json_data .= '"children": [';

									$notCompleted = $count-$completedCount;

									$json_data .= '{"name": "Not Completed", "size": '.$notCompleted.'}';

									

									if($completedCount)

									{

										$json_data .= ',';

										$json_data .= '{"name": "Completed", "size": '.$completedCount.'}';

									}



									$json_data .= ']}';

									$gCarsCounter++;

								}

								



								$json_data .= ']}';

							}

							else

							{

								$json_data .= '"size": 0}';

							}



						}

						//$json_data .= ']}';

					

					}

					$json_data .= ' ]}';

					

					/*}

					else

					{

						$json_data .= '"size": 0}';

					}*/



				//}

				//$json_data .= ']}';



			/*}

			else

			{

				$json_data .= '"size": 0}';

			}*/

		//}



		//$json_data .= ' ]}';

		//========================



		//echo "<pre>";

		echo $json_data;



		exit();

	}

	

	

	public function car_sales_consultant_hierarchical($dep_id=0, $date_from="", $date_to="")

	{

		$filterData = array();

		$filterData['date_from'] = $date_from;

		$filterData['date_to'] = $date_to;

		$filterData['dep_id'] = $dep_id;	

		

		$filterData['depUsers'] = $this->Model_manager_report->getAllDepUsersCommaSep($filterData);

		

		$car_sales_consultant_res = $this->Model_manager_report->get_car_sales_consultant_hierarchical($filterData);

		

		

		$json_data = '';

		

		$json_data .= '{"name": "graph","children": [';

		if($car_sales_consultant_res)

		{

			$prevVehId = 0;

			for($counter=0; $counter<count($car_sales_consultant_res); $counter++)					

			{

				

				if($counter>0) $prevVehId = $car_sales_consultant_res[$counter-1]->id;

				

				//close the previously opened children

				if($counter>0 && $prevVehId!=$car_sales_consultant_res[$counter]->id)

				{

					$json_data .= ']}';

				}

				

				if($prevVehId!=$car_sales_consultant_res[$counter]->id)

				{						

					if($counter>0) $json_data .= ',';

					$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->name.'", '; 

					$json_data .= '"children": [';

				}

				else

				{					

					$json_data .= ',';

				}

				

				$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->sc.'", "size": '.$car_sales_consultant_res[$counter]->requests_count.'}';				



				

			}

			

		}

		$json_data .= ' ]} ]}';



		//echo "<pre>";

		echo $json_data;



		exit();

	}

	

	public function car_sales_consultant_hierarchical2($dep_id=0, $date_from="", $date_to="")

	{

		$filterData = array();

		$filterData['date_from'] = $date_from;

		$filterData['date_to'] = $date_to;

		$filterData['dep_id'] = $dep_id;	

		

		$filterData['depUsers'] = $this->Model_manager_report->getAllDepUsersCommaSep($filterData);

		

		$car_sales_consultant_res = $this->Model_manager_report->get_car_sales_consultant_hierarchical2($filterData);

		

		

		$json_data = '';

		

		$json_data .= '{"name": "graph","children": [';

		if($car_sales_consultant_res)

		{

			$prevVehId = 0;

			for($counter=0; $counter<count($car_sales_consultant_res); $counter++)					

			{

				

				if($counter>0) $prevVehId = $car_sales_consultant_res[$counter-1]->id;

				

				//close the previously opened children

				if($counter>0 && $prevVehId!=$car_sales_consultant_res[$counter]->id)

				{

					$json_data .= ']}';

				}

				

				if($prevVehId!=$car_sales_consultant_res[$counter]->id)

				{						

					if($counter>0) $json_data .= ',';

					$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->name.'", '; 

					$json_data .= '"children": [';

				}

				else

				{					

					$json_data .= ',';

				}

				

				$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->sc.'", "size": '.$car_sales_consultant_res[$counter]->completed_count.'}';				



				

			}

			

		}

		$json_data .= ' ]} ]}';



		//echo "<pre>";

		echo $json_data;



		exit();

	}

	

	public function car_sales_consultant_hierarchical3($dep_id=0, $date_from="", $date_to="")

	{

		$filterData = array();

		$filterData['date_from'] = $date_from;

		$filterData['date_to'] = $date_to;

		$filterData['dep_id'] = $dep_id;	

		

		$filterData['depUsers'] = $this->Model_manager_report->getAllDepUsersCommaSep($filterData);

		

		$car_sales_consultant_res = $this->Model_manager_report->get_car_sales_consultant_hierarchical3($filterData);

		

		

		$json_data = '';

		

		$json_data .= '{"name": "graph","children": [';

		if($car_sales_consultant_res)

		{

			$prevUId = 0;

			$prevVId = 0;

			for($counter=0; $counter<count($car_sales_consultant_res); $counter++)					

			{

				

				if($counter>0) $prevUId = $car_sales_consultant_res[$counter-1]->uid;

				if($counter>0) $prevVId = $car_sales_consultant_res[$counter-1]->id;				

				

				//close the previously opened children

				if($counter>0 && $prevVId!=$car_sales_consultant_res[$counter]->id)

				{

					$json_data .= ']}';

				}

				

				//close the previously opened children

				if($counter>0 && $prevUId!=$car_sales_consultant_res[$counter]->uid)

				{

					$json_data .= ']}';

				}

			

				$commaA = false;

				if($prevUId!=$car_sales_consultant_res[$counter]->uid)

				{		

					

					if($counter>0) { $json_data .= ','; $commaA=true;}

					$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->sc.'", '; 

					$json_data .= '"children": [';

				}

				else

				{					

					$json_data .= ',';

					$commaA = true;

				}

				

				if($prevVId!=$car_sales_consultant_res[$counter]->id)

				{						

					if($counter>0 && !$commaA) $json_data .= ',';

					$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->name.'", '; 

					$json_data .= '"children": [';

				}

				else

				{		

					if(!$commaA)

					$json_data .= ',';

				}

				

				

				

				$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->src.'", "size": '.$car_sales_consultant_res[$counter]->requests_count.'}';				



				

			}

			

		}

		$json_data .= ' ]} ]} ]}';



		//echo "<pre>";

		echo $json_data;



		exit();

	}		

	

	public function car_sales_consultant_hierarchical4($dep_id=0, $date_from="", $date_to="")

	{

		$filterData = array();

		$filterData['date_from'] = $date_from;

		$filterData['date_to'] = $date_to;

		$filterData['dep_id'] = $dep_id;	

		

		$filterData['depUsers'] = $this->Model_manager_report->getAllDepUsersCommaSep($filterData);

		

		$car_sales_consultant_res = $this->Model_manager_report->get_car_sales_consultant_hierarchical4($filterData);

		

		

		$json_data = '';

		

		$json_data .= '{"name": "graph","children": [';

		if($car_sales_consultant_res)

		{

			$prevSrc = "";

			for($counter=0; $counter<count($car_sales_consultant_res); $counter++)					

			{

				

				if($counter>0) $prevSrc = $car_sales_consultant_res[$counter-1]->src;

				

				//close the previously opened children

				if($counter>0 && $prevSrc!=$car_sales_consultant_res[$counter]->src)

				{

					$json_data .= ']}';

				}

				

				if($prevSrc!=$car_sales_consultant_res[$counter]->src)

				{						

					if($counter>0) $json_data .= ',';

					$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->src.'", '; 

					$json_data .= '"children": [';

				}

				else

				{					

					$json_data .= ',';

				}

				

				$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->name.'", "size": '.$car_sales_consultant_res[$counter]->completed_count.'}';				



				

			}

			

		}

		$json_data .= ' ]} ]}';



		//echo "<pre>";

		echo $json_data;



		exit();

	}		

	

	public function car_sales_consultant_hierarchical5($dep_id=0, $date_from="", $date_to="")

	{

		$filterData = array();

		$filterData['date_from'] = $date_from;

		$filterData['date_to'] = $date_to;

		$filterData['dep_id'] = $dep_id;	

		

		$filterData['depUsers'] = $this->Model_manager_report->getAllDepUsersCommaSep($filterData);

		

		$car_sales_consultant_res = $this->Model_manager_report->get_car_sales_consultant_hierarchical5($filterData);

		

		

		$json_data = '';

		

		$json_data .= '{"name": "graph","children": [';

		if($car_sales_consultant_res)

		{

			$prevSrc = "";

			for($counter=0; $counter<count($car_sales_consultant_res); $counter++)					

			{

				

				if($counter>0) $prevSrc = $car_sales_consultant_res[$counter-1]->src;

				

				//close the previously opened children

				if($counter>0 && $prevSrc!=$car_sales_consultant_res[$counter]->src)

				{

					$json_data .= ']}';

				}

				

				if($prevSrc!=$car_sales_consultant_res[$counter]->src)

				{						

					if($counter>0) $json_data .= ',';

					$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->src.'", '; 

					$json_data .= '"children": [';

				}

				else

				{					

					$json_data .= ',';

				}

				

				$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->name.'", "size": '.$car_sales_consultant_res[$counter]->requests_count.'}';				



				

			}

			

		}

		$json_data .= ' ]} ]}';



		//echo "<pre>";

		echo $json_data;



		exit();

	}	

	

	public function car_sales_consultant_hierarchical6($dep_id=0, $date_from="", $date_to="")

	{

		$filterData = array();

		$filterData['date_from'] = $date_from;

		$filterData['date_to'] = $date_to;

		$filterData['dep_id'] = $dep_id;	

		

		$filterData['depUsers'] = $this->Model_manager_report->getAllDepUsersCommaSep($filterData);

		

		$car_sales_consultant_res = $this->Model_manager_report->get_car_sales_consultant_hierarchical6($filterData);

		

		

		$json_data = '';

		

		$json_data .= '{"name": "graph","children": [';

		if($car_sales_consultant_res)

		{

			$prevUid = "";

			for($counter=0; $counter<count($car_sales_consultant_res); $counter++)					

			{

				

				if($counter>0) $prevUid = $car_sales_consultant_res[$counter-1]->uid;

				

				//close the previously opened children

				if($counter>0 && $prevUid!=$car_sales_consultant_res[$counter]->uid)

				{

					$json_data .= ']}';

				}

				

				if($prevUid!=$car_sales_consultant_res[$counter]->uid)

				{						

					if($counter>0) $json_data .= ',';

					$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->full_name.'", '; 

					$json_data .= '"children": [';

				}

				else

				{					

					$json_data .= ',';

				}

				

				$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->name.'", "size": '.$car_sales_consultant_res[$counter]->purchased_count.'}';				



				

			}

			

		}

		$json_data .= ' ]} ]}';



		//echo "<pre>";

		echo $json_data;



		exit();

	}	

	

	public function car_sales_consultant_hierarchical7($dep_id=0, $date_from="", $date_to="")

	{

		$filterData = array();

		$filterData['date_from'] = $date_from;

		$filterData['date_to'] = $date_to;

		$filterData['dep_id'] = $dep_id;	

		

		$filterData['depUsers'] = $this->Model_manager_report->getAllDepUsersCommaSep($filterData);

		

		$car_sales_consultant_res = $this->Model_manager_report->get_car_sales_consultant_hierarchical7($filterData);

		

		

		$json_data = '';

		

		$json_data .= '{"name": "graph","children": [';

		if($car_sales_consultant_res)

		{

			$prevVid = "";

			for($counter=0; $counter<count($car_sales_consultant_res); $counter++)					

			{

				

				if($counter>0) $prevVid = $car_sales_consultant_res[$counter-1]->id;

				

				//close the previously opened children

				if($counter>0 && $prevVid!=$car_sales_consultant_res[$counter]->id)

				{

					$json_data .= ']}';

				}

				

				if($prevVid!=$car_sales_consultant_res[$counter]->id)

				{						

					if($counter>0) $json_data .= ',';

					$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->name.'", '; 

					$json_data .= '"children": [';

				}

				else

				{					

					$json_data .= ',';

				}

				

				$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->full_name.'", "size": '.$car_sales_consultant_res[$counter]->purchased_count.'}';				



				

			}

			

		}

		$json_data .= ' ]} ]}';



		//echo "<pre>";

		echo $json_data;



		exit();

	}	

	

	public function car_sales_consultant_hierarchical8($dep_id=0, $date_from="", $date_to="")

	{

		$filterData = array();

		$filterData['date_from'] = $date_from;

		$filterData['date_to'] = $date_to;

		$filterData['dep_id'] = $dep_id;	

		

		$filterData['depUsers'] = $this->Model_manager_report->getAllDepUsersCommaSep($filterData);

		

		$car_sales_consultant_res = $this->Model_manager_report->get_car_sales_consultant_hierarchical8($filterData);

		

		

		$json_data = '';

		

		$json_data .= '{"name": "graph","children": [';

		if($car_sales_consultant_res)

		{

			$prevSrc = "";

			$prevUId = 0;

			for($counter=0; $counter<count($car_sales_consultant_res); $counter++)					

			{

				

				if($counter>0) $prevSrc = $car_sales_consultant_res[$counter-1]->src;

				if($counter>0) $prevUId = $car_sales_consultant_res[$counter-1]->uid;				

				

				//close the previously opened children

				if($counter>0 && $prevUId!=$car_sales_consultant_res[$counter]->uid)

				{

					$json_data .= ']}';

				}

				

				//close the previously opened children

				if($counter>0 && $prevSrc!=$car_sales_consultant_res[$counter]->src)

				{

					$json_data .= ']}';

				}

			

				$commaA = false;

				if($prevSrc!=$car_sales_consultant_res[$counter]->src)

				{		

					

					if($counter>0) { $json_data .= ','; $commaA=true;}

					$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->src.'", '; 

					$json_data .= '"children": [';

				}

				else

				{					

					$json_data .= ',';

					$commaA = true;

				}

				

				if($prevUId!=$car_sales_consultant_res[$counter]->uid)

				{						

					if($counter>0 && !$commaA) $json_data .= ',';

					$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->sc.'", '; 

					$json_data .= '"children": [';

				}

				else

				{		

					if(!$commaA)

					$json_data .= ',';

				}

				

				

				

				$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->name.'", "size": '.$car_sales_consultant_res[$counter]->purchased_count.'}';				



				

			}

			

		}

		$json_data .= ' ]} ]} ]}';



		//echo "<pre>";

		echo $json_data;



		exit();

	}		

	

	public function car_sales_consultant_hierarchical9($dep_id=0, $date_from="", $date_to="")

	{

		$filterData = array();

		$filterData['date_from'] = $date_from;

		$filterData['date_to'] = $date_to;

		$filterData['dep_id'] = $dep_id;	

		

		$filterData['depUsers'] = $this->Model_manager_report->getAllDepUsersCommaSep($filterData);

		

		$car_sales_consultant_res = $this->Model_manager_report->get_car_sales_consultant_hierarchical9($filterData);

		

		

		$json_data = '';

		

		$json_data .= '{"name": "graph","children": [';

		if($car_sales_consultant_res)

		{

			$prevSrc = "";

			$prevVId = 0;

			for($counter=0; $counter<count($car_sales_consultant_res); $counter++)					

			{

				

				if($counter>0) $prevSrc = $car_sales_consultant_res[$counter-1]->src;

				if($counter>0) $prevVId = $car_sales_consultant_res[$counter-1]->id;				

				

				//close the previously opened children

				if($counter>0 && $prevVId!=$car_sales_consultant_res[$counter]->id)

				{

					$json_data .= ']}';

				}

				

				//close the previously opened children

				if($counter>0 && $prevSrc!=$car_sales_consultant_res[$counter]->src)

				{

					$json_data .= ']}';

				}

			

				$commaA = false;

				if($prevSrc!=$car_sales_consultant_res[$counter]->src)

				{		

					

					if($counter>0) { $json_data .= ','; $commaA=true;}

					$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->src.'", '; 

					$json_data .= '"children": [';

				}

				else

				{					

					$json_data .= ',';

					$commaA = true;

				}

				

				if($prevVId!=$car_sales_consultant_res[$counter]->id)

				{						

					if($counter>0 && !$commaA) $json_data .= ',';

					$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->name.'", '; 

					$json_data .= '"children": [';

				}

				else

				{		

					if(!$commaA)

					$json_data .= ',';

				}

				

				

				

				$json_data .= '{"name": "'.$car_sales_consultant_res[$counter]->sc.'", "size": '.$car_sales_consultant_res[$counter]->purchased_count.'}';				



				

			}

			

		}

		$json_data .= ' ]} ]} ]}';



		//echo "<pre>";

		echo $json_data;



		exit();

	}			

	

}



























