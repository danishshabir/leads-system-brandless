<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organization_structure extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	private $allChild = array();

	public function __construct()
    {
        parent::__construct();
		checkAdminSession();
      	$this->load->model('Model_category');
	    $this->load->model('Model_city');
        $this->load->model('Model_user');
        $this->load->model('Model_branch');
		$this->load->model('Model_structure');
        $this->load->model('Model_structure_departments');
		$this->load->model('Model_struc_dep_users');
		$this->load->model('Model_common'); //use this in every controller because it is accessed in header.
			
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	
	
	
	public function index($parent_id=1){
		
		$data = array();
		$data['view'] = 'org/organization_structure';
        $data['system_setting'] = 'active';
		$data['cities'] = $this->Model_city->getAll();
        $data['users'] = $this->Model_user->getAll();
		$data['branches'] = $this->Model_branch->getAll();
		$data['structures'] = $this->Model_structure->getAll();
		$where = array();
		$where['id'] = $parent_id;
		$data['first_single_dep'] = $this->Model_structure_departments->getMultipleRows($where);
		$where = array();
		$where['parent_id'] = $parent_id;
		//$where['id'] = 2;
		$data['top_level_deps'] = $this->Model_structure_departments->getMultipleRows($where);
		$data['loadonlystruc'] = false;
		if($parent_id>1) $data['loadonlystruc'] = true;
        $this->load->view('template',$data);		
		
	}

	//this function is called via ajax
	public function nestedPops($dep_id){						

		$subDepartments = getSubDepartments($dep_id); 
		if($subDepartments)
		{
		?>
			<div class="modal fade" id="bs-multi-more-nested<?php echo $dep_id ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
			  <div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body" id="load_more_struc<?php echo $dep_id; ?>">
						<?php 
						//Via ajax
						?>
					</div>
				</div>
			  </div>
			</div>
		<?php
		}
		
	}	
    
    public function orgAction()
    {
        if(isset($_POST['form_type']))
        {
            switch($_POST['form_type'])
            {
                /*case 'moveuser';
                $move_user = $this->moveUser();
                if($move_user)
                {
                    $data['success'] = 'User move successfully.';
					$data['error'] = 'false';
                    $data['reload'] = 1;
                    echo json_encode($data);
                    exit();
                }
                break;*/
				/*case 'add_structure':
				$this->saveStructure();
				break;*/
				case 'add_department':
				$this->addDepartment();
				break;				
                /*case 'addUserToStructure';
                $this->addUserToStructure();
                break;*/
				case 'removeUser':
				$this->removeUser();
				break;
				case 'edit_department_name':
				$this->editDepartmentName();
				break;	
				case 'select_ceo':
				$this->selectCeo();
				break;		
				case 'add_employee':
				$this->addEmployee();
				break;	
				case 'select_manager':
				$this->selectManager();
				break;	
				case 'delete_employee':
				$this->deleteEmployee();
				break;	
				case 'delete_department':
				$this->deleteDepartment();
				break;	
            }
        }    
        
    }

	/*function manage($p_cid=0,$space='') {

      

		  $data['parent_id'] = $p_cid;
		  $results = $this->Model_structure_departments->getMultipleRows($data);
		   $count=count($results); 
		  
         //$q="SELECT * FROM catagory WHERE parent_id='$p_cid'";  

        // $r=mysql_query($q) or die(mysql_error());  

       //  $count=mysql_num_rows($r);  

         if($data['parent_id'] ==0){  

               $space='';  

         }else{  

               $space .='&nbsp &nbsp &nbsp &nbsp &nbsp';  

         }  

        if($count > 1){  
      
      echo '<pre>';
	  

     //  while($results){ 

   foreach($results as $result){
       // $html .= "<option value=''".$row['parent_id']."'='>".$space.$row['eng_title']."</option>";  

          

          echo  '<tr class="odd">
					<td class="table_row">'.$space.$result->title.'</td>                                
				  </tr>' ;

       $this->manage($result->id,$space);  

       // $this->i = $this->i +1;

    }  

      

}  

}*/


	private function selectManager()
    {
       $data = array();
	   $update_by = array();
       
	   $data['updated_at'] =  date('Y-m-d H:i:s');
	   $data['updated_by'] = $this->session->userdata['user']['id'];
	   $data['manager_id'] = $this->input->post('manager_id');
	   
	   $update_by['id'] = $this->input->post('id');

	   $this->Model_structure_departments->update($data,$update_by);

       $success['success'] = 'Manager added.';
	   $success['error'] = 'false';
       $success['reload'] = 0;
       echo json_encode($success);
       exit();
    }

	private function selectCeo()
    {
       $data = array();
	   $update_by = array();
       
	   $data['updated_at'] =  date('Y-m-d H:i:s');
	   $data['updated_by'] = $this->session->userdata['user']['id'];
	   $data['manager_id'] = $this->input->post('manager_id');
	   
	   $update_by['id'] = $this->input->post('id');

	   $this->Model_structure_departments->update($data,$update_by);

       $success['success'] = 'Manager updated.';
	   $success['error'] = 'false';
       $success['reload'] = 0;
       echo json_encode($success);
       exit();
    }


	private function editDepartmentName()
    {
       $data = array();
	   $update_by = array();

       $data['title'] = $this->input->post('title');
	   
	   if($this->input->post('branch_id')!=null)
	   {
			$data['bid'] = $this->input->post('branch_id');
	   }
	   
	   $data['updated_at'] =  date('Y-m-d H:i:s');
	   $data['updated_by'] = $this->session->userdata['user']['id'];
	   
	   $update_by['id'] = $this->input->post('id');

	   $this->Model_structure_departments->update($data,$update_by);

       $success['success'] = 'Name updated.';
	   $success['error'] = 'false';
       $success['reload'] = 0;
       echo json_encode($success);
       exit();
    }

	/*private function saveStructure()
    {
       $data = array();
       $data['title'] = $this->input->post('title');
	   $data['created_at'] =  date('Y-m-d H:i:s');
	   $data['created_by'] = $this->session->userdata['user']['id'];
	   $this->Model_structure->save($data);
       $success['success'] = 'Structure save successfully.';
	   $success['error'] = 'false';
       $success['reload'] = 1;
       echo json_encode($success);
       exit();
    }*/

    private function addEmployee()
    {
       $data = array();

       $data['user_id'] = $this->input->post('user_id');
	   $data['created_at'] =  date('Y-m-d H:i:s');
	   $data['created_by'] = $this->session->userdata['user']['id'];	   	   
	   $data['struc_dep_id'] = $this->input->post('struc_dep_id');   

	   $this->Model_struc_dep_users->save($data); 

       $success['success'] = 'Employee added.';
	   $success['error'] = 'false';
       $success['reload'] = 0;
       echo json_encode($success);
       exit();
    }

    private function addDepartment()
    {
       $data = array();

       $data['title'] = $this->input->post('title');
	   $data['created_at'] =  date('Y-m-d H:i:s');
	   $data['created_by'] = $this->session->userdata['user']['id'];	   	   
	   $data['parent_id'] = $this->input->post('parent_id');
	   $data['structure_id'] = $this->input->post('structure_id');	   

	   $this->Model_structure_departments->save($data);

       $success['success'] = 'Department added.';
	   $success['error'] = 'false';
       $success['reload'] = 0;
       echo json_encode($success);
       exit();
    }
    
	
	private function deleteEmployee()
    {
       $data = array();
       $update_by = array();
	  
	   
       $update_by['id'] = $this->input->post('id');

	   $this->Model_struc_dep_users->delete($update_by);

       $success['success'] = 'Employee removed.';
	   $success['error'] = 'false';
       $success['reload'] = 0;
       echo json_encode($success);
       exit();
    }

	private function deleteDepartment()
    {
       $data = array();
       $where = array();
	  
	   
       /*$where['id'] = $this->input->post('id');
	   $this->Model_structure_departments->delete($where);

	   $where['parent_id'] = $this->input->post('id');
	   $this->Model_structure_departments->delete($where);

       $success['success'] = 'Department removed.';
	   $success['error'] = 'false';
       $success['reload'] = 1;
       echo json_encode($success);
       exit();*/


		$dep_id = $this->input->post('id');

		//This recursive function will set (an object level defined array) its a single dim array for all the child and grand child and so on.
		$this->getStrucChildren($dep_id); 

		$allChildArr = $this->allChild;
		if($allChildArr)
		{
			$this->Model_structure_departments->deleteIn("id", $allChildArr);	 
			//also delete the users in from these departments.
			$this->Model_struc_dep_users->deleteIn("struc_dep_id", $allChildArr);
		}
		

		// all children are deleted in above query now delete itself too.
		$where['id'] = $dep_id;
		$this->Model_structure_departments->delete($where);
		$where = array();
		$where['struc_dep_id'] = $dep_id;
		$this->Model_struc_dep_users->delete($where);

		$success['success'] = 'Department removed.';
		$success['error'] = 'false';
		$success['reload'] = 1;
		echo json_encode($success);
		exit();


    }

	private function getStrucChildren($id) {

		$where['parent_id'] = $id;
		$deps = $this->Model_structure_departments->getMultipleRows($where);

		$children = array();
		
		if($deps) {
			# It has children, let's get them.
			//while($row = mysql_fetch_array($r)) {
			foreach($deps as $dep) {
				# Add the child to the list of children, and get its subchildren
				$this->allChild[] = $dep->id;
				$children[$dep->id] = $this->getStrucChildren($dep->id);				
			}
		}

		return $children;
	}


	
}
