<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class User extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	  public function __construct()

    {

        parent::__construct();

		checkAdminSession();

       

	    $this->load->model('Model_user');

		$this->load->model('Model_role');

		$this->load->model('Model_branch');

		$this->load->model('Model_category');

		$this->load->model('Model_city');

		$this->load->model('Model_lead');

		$this->load->model('Model_source');	 

		$this->load->model('Model_common'); //use this in every controller because it is accessed in header.		

        //$res = checkLevels(2);

		//checkAuth($res);

    }

	

	

	

	

	public function index()

	{

		$data = array();

		$data['view'] = 'user/users';

		$data['system_setting'] = 'active';

		$data['users'] = $this->Model_user->getAllUsersWithRoles();

		

		$this->load->view('template',$data);

		

		

	}

	

	public function view($user_id)

	{

		$data = array();

		$data['view'] = 'user/view';

		$data['system_setting'] = 'active';

		$data['user'] = $this->Model_user->getAllUsersWithRoles($user_id);

        

		$data['user'] = $data['user'][0];

		$this->load->view('template',$data);

	}

	 

	public function userAction()

	{

		/*$rows = $this->leads->getAll(true);

 		print_r($rows);*/

		$data = array();

		

		$data['view'] = 'user/create_new_user';

		
		

		if(isset($_POST['form_type'])){

			switch($_POST['form_type']){

				case 'save':

				if($this->validation())

                {

                  $this->save($this->input->post());  

                }

				

				break;

				case 'suspend':

				$data['user_suspend'] = $this->suspend();

				if($data['user_suspend'] == true)

				{

					$data['success'] = 'User(s) suspended successfully.';

					$data['error'] = 'false';

					$data['is_suspend'] = 1;

					echo json_encode($data);

					exit;

				}

				break;

				case 'delete':

				$data['user_delete'] = $this->delete();

				

				

				break;

				case 'update':

                $this->validation('update');

				$this->update($this->input->post());

				

				break;



				case 'send_email':

					$where = array();

					$where['is_suspend'] = 0;

					$where['id'] = $this->input->post('id');

					$users = $this->Model_user->getMultipleRows($where);					

					$emailData = array();

					$emailData['first_name'] = $users[0]->full_name;

					$emailData['password'] = $users[0]->password;

					$emailData['category_title'] = "";



					sendEmail(7, $users[0]->email, $emailData);					



					$success['error'] = 'false';

					$success['success'] = 'Email sent successfully.';

					echo json_encode($success);

					exit;

				break;

				

			}

		}

		$data['roles'] = $this->Model_role->getAll();

		$data['branches'] = $this->Model_branch->getAll();

		//$data['departments'] = $this->Model_category->getAll(); // departments are actually categories containing depart names

		$data['sources'] = $this->Model_source->getAll();

		$data['cities'] = $this->Model_city->getAll();
		$data['departments']=$this->Model_city->getDepartment(); //get department from this model not create a seprate model for this 
		

        $this->load->view('template',$data);

	}

	

	public function getUsersForTagging()

	{

		$data = array();

		$results = array();

		$notUsers = array();



		$notUsers[] = $this->session->userdata['user']['id'];

		$lead_id = $this->input->post('lead_id');

		$text = $this->input->post('text');

		$leadRec = $this->Model_lead->get($lead_id);

		$notUsers[] = $leadRec->created_by;

		$notUsers[] = $leadRec->assign_to;

		

		$results = $this->Model_user->getUsersForTagging($notUsers, $text);

		$i=0;

		foreach($results as $result){

			$data[$i]['id'] = $result->id;

			$data[$i]['name'] =  "[".$result->full_name."]";

			$i++;

		}

		echo json_encode($data);

	    exit;

		

		

		

	}

	

	

	private function validation($action = '')

	{

		    $errors = array();

			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            if($action == '')

			{				

				$this->form_validation->set_rules('password', 'Password', 'required|matches[confirm_password]');

				$this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'required');

			}



			if($this->input->post('oldEmail') === $this->input->post('email'))

			{

				//do nothing

			}

			else

			{

				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');

				$this->form_validation->set_message('is_unique', 'Email already used for another user');

			}

			

			$this->form_validation->set_rules('mobile', 'Mobile', 'required');

            //$this->form_validation->set_rules('depart_id', 'Category', 'required');

			//$this->form_validation->set_rules('phone1', 'Phone', 'required');

			//$this->form_validation->set_rules('phone2', 'Phone', 'required');

			$this->form_validation->set_rules('full_name', 'Full Name', 'required');



			if($action!=="update")

			{

				$this->form_validation->set_rules('branch_id', 'Branch', 'required');

				$this->form_validation->set_rules('role_id', 'Role', 'required');

			}



			if ($this->form_validation->run() == FALSE)

			{

				$errors['error'] = validation_errors();

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

            {

				return true;

			}

			

	}

	

	private function save($post_data)

	{

		$data = array();

        $get_user = array();

        $get_user_team_lead = array();

        $update_user = array();

        $user = array();

        

		foreach($post_data as $key => $value){

			

			if($key != 'form_type' && $key != 'terms_conditions' && $key != 'confirm_password' && $key != 'phone1' && $key != 'phone2'){

			 $data[$key] = $value;	

			}

		}

		$data['created_at'] = date('Y-m-d H:i:s');

		$data['created_by'] = $this->session->userdata['user']['id']; 

		$data['phone'] = $post_data['phone1'].'-'.$post_data['phone2'];

		$data['department_id'] = $post_data['department_id'] ;

       //add notification

		addNotification("New user added ".$post_data['full_name']."");



	   // Logic change we can add multiple branch manager

       /* if($this->input->post('is_branch_manager') == 1)

        {

            

            $get_user['branch_id'] = $this->input->post('branch_id');

            $get_user['is_branch_manager'] = 1;

            $user = $this->Model_user->getWithMultipleFields($get_user); // check if there is any other branch manager

            

        } 

		

		if(!empty($user))

        {

            

            

            $update_user_by['id'] = $user->id;

            $update_user['is_branch_manager'] = 0;

            

            $this->Model_user->update($update_user, $update_user_by);

            $success['success'] = 'User has been created successfully and new branch manager is assign to this branch.';

        }

        */

		if($this->input->post('is_ceo') == 1)

        {

            

            

            $get_user['is_ceo'] = 1;

            $user_ceo = $this->Model_user->getWithMultipleFields($get_user); // check if there is any other Ceo

            

        }

		if($data['adviser_type'] === "")

        {

            $data['adviser_type'] = null;            

        }

		 if(!empty($user_ceo))

        {

            

            

            $update_user_ceo_by['id'] = $user_ceo->id;

            $update_user_ceo['is_ceo'] = 0;

            

            $this->Model_user->update($update_user_ceo, $update_user_ceo_by);

            //$success['success'] = 'User has been created successfully and new Ceo.';

        }

		

		

        if($this->input->post('is_team_lead') == 1)

        {

            $get_user_team_lead['is_team_lead'] = 1;

            $get_user_team_lead['branch_id'] = $this->input->post('branch_id');;

            //$get_user_team_lead['depart_id'] = $this->input->post('depart_id');

            $user_team_lead = $this->Model_user->getWithMultipleFields($get_user_team_lead); // check if there is any other team lead

            

        }

        

        

        

        

      

        if(!empty($user_team_lead))

        {

            $update_user_team_by['id'] = $user_team_lead->id;

            $update_user_team['is_team_lead'] = 0;

            $this->Model_user->update($update_user_team, $update_user_team_by);

            $success['success'] = 'User has been created successfully and new team lead is assign to this category.';

        }elseif(empty($user_team_lead))

        {

            $success['success'] = 'User has been created successfully.';

        }

        

        

        

        $insert_user_id = $this->Model_user->save($data);

	

		

		$success['error'] = 'false';

	    $success['reset'] = 1;

		echo json_encode($success);

		exit;

		

	//	return $insert_user_id;

		

	}

	

	private function suspend()

	{

		$data = array();

		$arr_update = array();

		$data['is_suspend'] = 1;

		$checkBoxesSelectedCommas = $this->input->post('id');

		$checkBoxesSelectedCommasArr = explode(",",$checkBoxesSelectedCommas);

		foreach($checkBoxesSelectedCommasArr as $checkBoxesSelected)

		{

			$arr_update['id'] = $checkBoxesSelected;

		    $this->Model_user->update($data,$arr_update);

		}

		

       //add notification

		addNotification("User suspended");



		return true;

	}

	

	private function delete()

	{

		$data = array();

		$checkBoxesSelectedCommas = $this->input->post('id');

		$checkBoxesSelectedCommasArr = explode(",",$checkBoxesSelectedCommas);

		$i = 0;

		$delete = 0;

		$assgin = 0;

		foreach($checkBoxesSelectedCommasArr as $checkBoxesSelected)

		{

			$arr_delete['assign_to'] = $checkBoxesSelected;

			

			

		    $user_assign_to_with_lead =  $this->Model_lead->getWithMultipleFields($arr_delete);

			if(empty($user_assign_to_with_lead))

			{

				$data['id'] = $checkBoxesSelected;



			   //add notification

				addNotification("User deleted");



				$this->Model_user->delete($data);

				$delete++;

			}else

			{

				$assgin++;

				

			}

			

			

			

		    $i++;

		}

		if($this->input->post('redirect'))

		{

			$data['redirect'] = 'user';

		}else

		{

			$data['redirect'] = '';

		}	

		if($delete > 0 && $assgin == 0)

		{

			$data['success'] = 'User(s) deleted successfully.';

			$data['error'] = 'false';

		}

		if($delete > 0 && $assgin > 0 )

		{

			$data['success'] = 'User(s) deleted successfully But you can not delete user that are assigned to some lead(s).';

			$data['error'] = 'false';

		}

		if($delete == 0)

		{

			$data['error'] = 'User(s) assgin to some lead you cannot delete.';

			$data['success'] = 'false';

		}

		

		echo json_encode($data);

		exit;

	}

	

	public function editUser($user_id)

	{

		$data = array();

		

		$data['view'] = 'user/edit_user';

		$data['roles'] = $this->Model_role->getAll();

		$data['cities'] = $this->Model_city->getAll();

		$data['branches'] = $this->Model_branch->getAll();

		//$data['departments'] = $this->Model_category->getAll();

		$data['departments']= $this->Model_city->getDepartment();

		$data['sources'] = $this->Model_source->getAll();

		$data['user'] = $this->Model_user->get($user_id);

		//debug($data['departments']);

		

		$this->load->view('template',$data);

	}

	

	private function update($post_data)

	{
		

		$data = array();

		$arr_update = array();

        //$get_user = array();

        //$get_user_team = array();

        //$check = false;

       // $check_team = false;

        //$check_ceo = false;

        

        

		foreach($post_data as $key => $value){

			

			if($key != 'form_type' && $key != 'phone1' && $key != 'phone2' && $key != 'user_id' && $key != 'oldEmail'){

			 $data[$key] = $value;	

			}

		}

		$data['updated_at'] = date('Y-m-d H:i:s');

		$this->session->userdata['user']['id']; 

		$data['phone'] = $post_data['phone1'].'-'.$post_data['phone2'];

		$data['pie_color'] = "#".$data['pie_color'];

		$data['department_id'] = $post_data['department_id'];

		//$get_user_to_check_manger = $this->Model_user->get($post_data['user_id']);

		

		/*if($get_user_to_check_manger->is_branch_manager == 1 && $post_data['is_branch_manager'] == 0)

		{

			

			$update_employee_user['parent_id'] = 0;

			$update_employee_user_by['parent_id'] = $post_data['user_id'];

			$this->Model_user->update($update_employee_user, $update_employee_user_by);

		}*/

		$arr_update['id'] = $post_data['user_id'];

        

		

        //add notification

		addNotification("User updated ".$post_data['full_name']."");

	   

        

       /* if($post_data['is_branch_manager'] == 1){

            $get_user['branch_id'] = $this->input->post('branch_id');

            $get_user['is_branch_manager'] = 1;

            $user = $this->Model_user->getWithMultipleFields($get_user); // check if there is any other branch manager 

            if(!empty($user)){

                if($post_data['user_id'] != $user->id)

                {

                    $check = true;

                }

            }

            

        }

        

        if($check)

        {

            $update_user_by['id'] = $user->id;

            $update_user['is_branch_manager'] = 0;

            $this->Model_user->update($update_user, $update_user_by);

            $success['success'] = 'User has been updated successfully and new branch manager is assign to this branch.';

        }

		*/

		/*if($post_data['is_ceo'] == 1){

           

            $get_user_ceo['is_ceo'] = 1;

            $user_ceo = $this->Model_user->getWithMultipleFields($get_user_ceo); // check if there is any other branch manager 

            if(!empty($user_ceo)){

                if($post_data['user_id'] != $user_ceo->id)

                {

                    $check_ceo = true;

                }

            }

            

        }*/

        

		 /*if($check_ceo)

        {

            $update_user_ceo_by['id'] = $user_ceo->id;

            $update_user_ceo['is_ceo'] = 0;

            $this->Model_user->update($update_user_ceo, $update_user_ceo_by);

            //$success['success'] = 'User has been updated successfully and new branch manager is assign to this branch.';

        }

		

        if($post_data['is_team_lead'] == 1){

            $get_user_team['branch_id'] = $this->input->post('branch_id');

            //$get_user_team['depart_id'] = $this->input->post('depart_id');

            $get_user_team['is_team_lead'] = 1;

            $user_team = $this->Model_user->getWithMultipleFields($get_user_team); // check if there is any other branch manager 

            if(!empty($user_team))

            {

                if($post_data['user_id'] != $user_team->id)

                {

                    $check_team = true;

                }

            }

        }*/

        

        

       

        /*if($check_team)

        {

            $update_user__team_by['id'] = $user_team->id;

            $update_user_team['is_team_lead'] = 0;

            $this->Model_user->update($update_user_team, $update_user__team_by);

            $success['success'] = 'User has been updated successfully and new team lead is assign to this category.';

        }elseif($check == false && $check_team == false)

        {

            $success['success'] = 'User has been updated successfully .';

        }*/

        

		if($data['adviser_type'] === "")

        {

            $data['adviser_type'] = null;            

        }

        

        $this->Model_user->update($data,$arr_update);

		$success['error'] = 'false';

		$success['success'] = 'User has been updated successfully.';

		echo json_encode($success);

		exit;

        

		

	}



	public function sendEmailAll()

	{



		$where = array();

		$where['is_suspend'] = 0;

		$where['id !='] = 1;

		$users = $this->Model_user->getMultipleRows($where);

		//echo $this->db->last_query(); exit();

		foreach($users as $user)

		{

			$emailData = array();

			$emailData['first_name'] = $user->full_name;

			$emailData['password'] = $user->password;

			$emailData['category_title'] = "";



			sendEmail(7, $user->email, $emailData);



		}



		$success['error'] = 'false';

		$success['success'] = 'Email sent successfully.';

		echo json_encode($success);

		exit;

		

	}	

	

}

