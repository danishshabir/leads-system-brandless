<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branch_management extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
    {
        parent::__construct();
		checkAdminSession();
      	$this->load->model('Model_branch');
	    $this->load->model('Model_city');
		$this->load->model('Model_lead');
		$this->load->model('Model_user');
		$this->load->model('Model_common'); //use this in every controller because it is accessed in header.
        //$res = checkLevels(2);
		//checkAuth($res);
    }
	
	
	
	
	public function index(){
		
		$data = array();
		$data['view'] = 'branch_mng/branch_management';
        $data['system_setting'] = 'active';
		$data['cities'] = $this->Model_city->getAll();
		$data['users'] = $this->Model_user->getAll();		
		$this->load->view('template',$data);		
		
	}
	
	
	public function save(){
		
		$data = array();
		$data = $this->input->post();
		$this->Model_branch->save($data);

		//add notification
		addNotification("New branch added ".$data['title']."");

		redirect($this->config->item('base_url') . 'branch_management');
		
	}
	
	public function update(){
		
		$data = array();
		$arr_update_by['id'] = $this->input->post('id');
		foreach($_POST as $key =>$value)
		{
			if($key != 'id')
			{
				$data[$key] = $value;
			}
		}
		$this->Model_branch->update($data,$arr_update_by);

		//add notification
		addNotification("Branch updated ".$data['title']."");

		redirect($this->config->item('base_url') . 'branch_management');
		
	}
	
	public function delete()
	{
		$data = array();
		$data['id'] = $this->input->post('id');
		$arr_delete['branch_id'] = $data['id'];
		$lead_register_with_branch =  $this->Model_lead->getWithMultipleFields($arr_delete);
		$user_register_with_branch =  $this->Model_user->getWithMultipleFields($arr_delete);
		if(empty($lead_register_with_branch) && empty($user_register_with_branch)){

			//add notification
			addNotification("Branch deleted");

			$this->Model_branch->delete($data);
			$data['success'] = 'Branch deleted successfully.';
			$data['error'] = 'false';
			echo json_encode($data);
			exit;
		}else
		{
			$data['success'] = 'false';
			$data['error'] = 'Some of lead or user are register with this branch you cannot delete this branch.';
	     	echo json_encode($data);
			exit();
			
		}
	}
	 
	
}
