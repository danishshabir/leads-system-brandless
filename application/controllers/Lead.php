<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Lead extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */



	 private $allChild = array();

	 private $not_a_lead_type = 0;



	  public function __construct()

    {

        parent::__construct();

		checkAdminSession();

      	$this->load->model('Model_lead');

		$this->load->model('Model_common'); //use this in every controller because it is accessed in header.

	    $this->load->model('Model_vehicle');

		$this->load->model('Model_city');

		$this->load->model('Model_branch');

		$this->load->model('Model_category');

		$this->load->model('Model_complaint_type');

		$this->load->model('Model_leads_messages');

		$this->load->model('Model_messages');

		$this->load->model('Model_survey');

		$this->load->model('Model_notification');

		$this->load->model('Model_user');

		$this->load->model('Model_lead_tags');

		$this->load->model('Model_vehicle_specific_name');

		$this->load->model('Model_struc_dep_users');		

		$this->load->model('Model_structure_departments');

		$this->load->model('Model_source');

		$this->load->model('Model_tag');

		$this->load->model('Model_event');



		if( rights(1,'read') )

		{

			//ok fine

		}

		else

		{

			echo "You don't have permissions to access this page.";

			exit();

		}

    }

	

	

	



	/*public function searchLeads(){



		$data = array();	

		

		$users = $this->usersUnderMngRecursive();



		$data['leads'] = $this->Model_lead->searchLeads($this->input->get('search_keyword'), $users);



		$data['view'] = 'lead/leads';

        $data['lead_class'] = 'active';

		$data['isSingleDetailLead'] = false;

		$data['cities'] = $this->Model_city->getAll();

		$data['logged_in_user_id'] = $this->session->userdata['user']['id'];

		$data['categories'] = $this->Model_category->getAll();

		$data['tags'] = $this->Model_tag->getAll();

		$data['dashboard'] = false;

		$data['searchedOrFiltered'] = true;

		$data['vehicles_specific'] = $this->Model_vehicle_specific_name->getAll(); 

		$this->load->view('template',$data);

		

		

		}*/





	public function leadAction()

	{

		

		$data = array();



		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{



				case 'save':



					//$this->checkLicense();



					$this->validation();

					$trackAndLead_id = $this->save($this->input->post());

					$trackAndLead_idArr = explode("||",$trackAndLead_id);

					$track_id = $trackAndLead_idArr[0];

					$lead_id = $trackAndLead_idArr[1];

					

					$data['success'] = 'Track ID: '.$track_id.' Lead has been created successfully.';

					$data['error'] = 'false';

					$data['lead_id'] = $lead_id;

					$data['reset'] = 1;

					echo json_encode($data);

					exit;

					

				break;



				case 'delete':

					

					$this->deleteLeads();

					$data['success'] = 'Leads deleted successfully.';

					$data['error'] = 'false';

					echo json_encode($data);

					exit;



				break;



				case 'update':



					//$this->checkLicense();



					$this->validation();

					$lead_id = $this->update($this->input->post());



					$data['success'] = 'Lead has been updated successfully.';

					$data['error'] = 'false';

					$data['lead_id'] = $lead_id;

					echo json_encode($data);

					exit;					



				break;			



				case 'action_added':



					$this->addLeadMessage();



				break;

			

				case 'admin_assign_lead_action':



					$this->adminAssignLeadAction();



				break;					



				case 'check_leads_same_city_branch':



					$this->checkLeadsSameCityBranch();



				break;				

				



				

			}

		}

		

	}

	

	//same copy in Dashboard contoller

	private function usersUnderMngRecursive() //gives comma sep users under manager.

	{

		$users = array(); //All the users under this manager.



		$loggedInUserManagingDeps = getLoggedInUserManagingDeps();		

		if(is_array($loggedInUserManagingDeps) && count($loggedInUserManagingDeps)>0)

		{

			foreach($loggedInUserManagingDeps as $dep)

			{

				$this->allChild = array();

				$this->allChild[] = $dep->id; //also include itself

				$this->getStrucChildren($dep->id); //it will update an object level array varible with the child dep ids				

				$allChildArr = $this->allChild;

				if($allChildArr)

				{

					foreach($allChildArr as $chDepId)

					{

						$usersUnderMngr = getUsersUnderMngr($chDepId);

						

						if($usersUnderMngr)

						foreach($usersUnderMngr as $depUser)

						{

							//if($users!="") $users .= ",";

							if(!in_array($depUser->user_id,$users))

							$users[] = $depUser->user_id;

						}

						

						//include managers too.

						$depRow = $this->Model_structure_departments->get($chDepId);

						$users[] = $depRow->manager_id;

					}

				}

			}		

		}



		//print_r($users); exit();

		$users = implode(",",$users);

		$users = str_replace(",0","",$users);



		return $users;

	}



	//same copy in Dashboard controller

	//current logic is that the sub manager can see all the leads as the manager. Even the further nested department leads.

	private function usersUnderSubmngRecursive() //gives comma sep users under manager.

	{

		$users = array(); //all other users in his departments.

		$this->allChild = array(); //reset object level array



		if( rights(33,'read') ) //lead controller e.g sub manager.

		{



			$wdu = array();

			$wdu['user_id'] = $this->session->userdata['user']['id'];

			$thisUserDeps = $this->Model_struc_dep_users->getMultipleRows($wdu);			



			if(is_array($thisUserDeps) && count($thisUserDeps)>0)

			{

				foreach($thisUserDeps as $dep)

				{

					$this->allChild = array();

					$this->allChild[] = $dep->struc_dep_id; //also include itself

					$this->getStrucChildren($dep->struc_dep_id); //it will update an object level array varible with the child dep ids				

					$allChildArr = $this->allChild;

					if($allChildArr)

					{

						foreach($allChildArr as $chDepId)

						{

							$usersUnderMngr = getUsersUnderMngr($chDepId);

							

							if($usersUnderMngr)

							foreach($usersUnderMngr as $depUser)

							{

								//if($users!="") $users .= ",";

								if(!in_array($depUser->user_id,$users))

								$users[] = $depUser->user_id;

							}

							

							//include managers too.

							$depRow = $this->Model_structure_departments->get($chDepId);

							$users[] = $depRow->manager_id;

						

						}

					}

				}		

			}



		}



		//print_r($users); exit();

		$users = implode(",",$users);

		$users = str_replace(",0","",$users);



		return $users;

	}



	

	//$filter=false, $category_id=0, $status="" these paramerts are for filtering.

	//this function is used when leads listing page is 1. open by default. 2. searching 3. filter

	public function index($applyFilter="", $category_id=0, $status="")

	{ 

		$data = array();

		$data['managerView'] = false;

		$data['view'] = 'lead/leads';

        $data['lead_class'] = 'active';

		if($applyFilter=="filter") $applyFilter=true; else $applyFilter=false;	

		$status = str_replace("%20"," ",$status);

		$sepcialData = array();

		$data['totalLeadsCountForPaging'] = 0;

		

		//$data['leads'] = $this->Model_lead->getAllSortedBy();

		$pageNumber = 1;

		if($this->input->get('page')>1)

			$pageNumber = $this->input->get('page');



		$users = ""; //comma sep

		$branchIdOfLoggedInManager = 0;

		$branchIdOfLoggedInSubMng = 0;

	

		$loggedInUserManagingDeps = getLoggedInUserManagingDeps();		

		if(is_array($loggedInUserManagingDeps) && count($loggedInUserManagingDeps)>0)

		{						

			//logged in user is a  manager from org structure for one or more departments.

			//get users under him so this manager can see the leads of his users too.

			$users = $this->usersUnderMngRecursive();



			//Need to improve in future as the new req is that a manager can be of two Branches.

			//For now there is only one case for "Reda Mira" so we have done hard coding in model for it.

			$branchIdOfLoggedInManager = getBranchIdOfUserByUserId($this->session->userdata['user']['id']);



			$data['managerView'] = true;

		}

		else

		{



			//its mean logged in user is not a manager. Now check if the logged in user is a lead controller?

			//if( rights(33,'read') ) //33 is lead controller

			//{

				//logged in user is a submanager(lead controller) now get all the users of his departments.

				$users = $this->usersUnderSubmngRecursive();

				$branchIdOfLoggedInSubMng = getBranchIdOfUserByUserId($this->session->userdata['user']['id']);



				$data['managerView'] = true;



			//}

		}



		$checkLoggedInUserInboxRole = checkLoggedInUserInboxRole();



		$keyWord = $this->input->get('search_keyword');

		

		$filterKeywordValue = $this->input->get('filter_keyword');	

		$filterKeywordValue = str_replace("%20"," ",$filterKeywordValue);

		$filterKeywordValue = str_replace("+"," ",$filterKeywordValue);

		if($filterKeywordValue!='')

		{

			//$data['leads'] = $leads_data;

			$status = $filterKeywordValue;

			$applyFilter = true;

		}

		

		$sepcialData['manual_source'] = $this->Model_source->getIds();

		

		$data['leads'] = $this->Model_lead->getAllSortedByPaged($pageNumber,0,1,$users,$keyWord,$checkLoggedInUserInboxRole,$applyFilter,$category_id,$status,$branchIdOfLoggedInManager,$branchIdOfLoggedInSubMng,$sepcialData);

		

		//===

		$data['leadIdsNoActionArr'] = array();

		

		if($data['leads'])

		{

			$leadIdsCommaSep = "";

			foreach($data['leads'] as $leadForIds)

			{

				if($leadIdsCommaSep!="") $leadIdsCommaSep .= ",";

				$leadIdsCommaSep .= $leadForIds->id;			

			}

			

			

			$leadIdsNoActionArr = $this->Model_lead->checkNoAction($leadIdsCommaSep);		

			if(count($leadIdsNoActionArr)>0)

			{			

				$data['leadIdsNoActionArr'] = explode(',', $leadIdsNoActionArr[0]['commaIds']);

			}	

		}			

		//===

		

		

		if(isset($this->session->userdata['viewLeadStatus'])){

			if($this->session->userdata['viewLeadStatus'] == 1){

				$viewLeadStatus = 1;

			}else{

				//$viewLeadStatus = 0; always use this function now.

				$viewLeadStatus = 1;

			}

		}else{

			//$viewLeadStatus = 0; always use this function now.

			$viewLeadStatus = 1;

		}

		if($viewLeadStatus == 1 && $this->input->get('sortType')!='ntf'){

		$sepcialData['countOnly'] = 1;

		$countOnlyRes = $this->Model_lead->getAllSortedByPaged($pageNumber,0,1,$users,$keyWord,$checkLoggedInUserInboxRole,$applyFilter,$category_id,$status,$branchIdOfLoggedInManager,$branchIdOfLoggedInSubMng,$sepcialData);

		 

		//echo "<pre>"; print_r($countOnlyRes); exit;

		 $data['totalLeadsCountForPaging'] = $countOnlyRes[0]->count;

		 

		}

		

		if($this->input->get('excelFilRC') !=null && (int)$this->input->get('excelFilRC')===1){

			$sepcialData['countOnly'] = 1;

			$countOnlyRes = $this->Model_lead->getAllSortedByPaged($pageNumber,0,1,$users,$keyWord,$checkLoggedInUserInboxRole,$applyFilter,$category_id,$status,$branchIdOfLoggedInManager,$branchIdOfLoggedInSubMng,$sepcialData);

			 

			//echo "<pre>"; print_r($countOnlyRes); exit;

			echo $countOnlyRes[0]->count;

			exit();		 

		}

		

	

		$data['isSingleDetailLead'] = false;

		$data['cities'] = $this->Model_city->getAll();

		$data['logged_in_user_id'] = $this->session->userdata['user']['id'];

		//$data['categories'] = $this->Model_category->getAll();

		$data['categories'] = $this->Model_category->getAllOrderBy(false,"title","asc");		

        $data['vehicles_specific'] = $this->Model_vehicle_specific_name->getAll(); 

		$data['tags'] = $this->Model_tag->getAll();



		$data['loadmore'] = true;

		

		//show load more if needed

		$data['loadMoreNeeded'] = false;



		if(is_array($data['leads']) && count($data['leads'])===10)

		{

			$data['loadMoreNeeded'] = true;

		}

		

		$data['loadonlyleads'] = false;		



		if($pageNumber>1 or $this->input->get('sortType')!='' or $filterKeywordValue!="")

		{

			$data['loadonlyleads'] = true;

		}		

		else

		{

			//======================





			//Without Connected Total Leads that this person have access to. (With (main top/left sidebar) Filter Applied for it)

				/*$totalLeadsWithOutConnected = $this->Model_lead->getAllSortedByPaged(0,0,0,$users,$keyWord,$checkLoggedInUserInboxRole,$applyFilter,$category_id,$status,$branchIdOfLoggedInManager,$branchIdOfLoggedInSubMng);

				$data['totalLeadsCountForPaging'] = 0;

				if($totalLeadsWithOutConnected)

				$data['totalLeadsCountForPaging'] = count($totalLeadsWithOutConnected);*/



			//Without Connected Total Leads that this person have access to.

				//$totalLeads = $this->Model_lead->getAllSortedByPaged(0,0,0,$users,"",$checkLoggedInUserInboxRole,false,0,"",$branchIdOfLoggedInManager,$branchIdOfLoggedInSubMng);	

				//$data['totalLeadsCountWithOutConnected'] = 0;

				//if($totalLeads)

				//$data['totalLeadsCountWithOutConnected'] = count($totalLeads);





			//Further calculations

				$data['totalConnectedLeadsCount'] = 0;

				$data['totalConnectedUnAssignedLeadsCount'] = 0; //new	

				$data['totalConnectedAssignedLeadsCount'] = 0;

				$data['totalUnAssignedLeadsCountWithOutConnected'] = 0; //new

				$data['totalAssignedLeadsCountWithOutConnected'] = 0;

				$data['finishedCounter'] = 0;

				$data['approvedCounter'] = 0;

				$data['closedCounter'] = 0;

				$data['duplicatedCounter'] = 0;

				$data['delayedCounter'] = 0;

				$data['upcommingCounter'] = 0;

				$data['finishedCounterWoc'] = 0;

				$data['approvedCounterWoc'] = 0;

				$data['closedCounterWoc'] = 0;

				$data['duplicatedCounterWoc'] = 0;

				$data['delayedCounterWoc'] = 0;

				$data['upcommingCounterWoc'] = 0;

				

				$data['assignToDDB'] = array();

				$data['assignToDDBCounter'] = array();

				$data['assignToDDBCounterWOC'] = array();



				$data['noAssignToDDBCounter'] = array(0);

				$data['noAssignToDDBCounterWOC'] = array(0);



				$data['sourceDDB'] = array();

				$data['sourceDDBCounter'] = array();

				$data['sourceDDBCounterWOC'] = array();



				$data['branchDDB'] = array();

				$data['branchDDBCounter'] = array();

				$data['branchDDBCounterWOC'] = array();



				$data['eventDDB'] = array();

				$data['eventDDBCounter'] = array();

				$data['eventDDBCounterWOC'] = array();



				$data['noBranchDDBCounter'] = array(0);

				$data['noBranchDDBCounterWOC'] = array(0);

				

				

				

				

				

				

				//new faster===

				$branch_id_for_filter = 0;

				$simple_user_id_for_filter = 0;

				if(!rights(35,'read'))

				{

					if((int)$branchIdOfLoggedInManager !== 0) $branch_id_for_filter = $branchIdOfLoggedInManager;

					elseif((int)$branchIdOfLoggedInSubMng !== 0) $branch_id_for_filter = $branchIdOfLoggedInSubMng;

					elseif(!rights(35,'read') && !$branch_id_for_filter) $simple_user_id_for_filter = $this->session->userdata['user']['id'];

				}

		

				//if(rights(35,'read')){

				if(rights(35,'read')){

					$data['branchDDB'] = $this->Model_lead->getBranchDDB();

					$data['noBranchCount'] = $this->Model_lead->getNoBranchCount();

					$data['eventDDB'] = $this->Model_lead->getEventDDB();					

					

				}elseif(($data['managerView'] && rights(40,'write')) || $data['logged_in_user_id']==136){

					$data['branchDDB'] = $this->Model_lead->getBranchDDB($users);

					$data['noBranchCount'] = $this->Model_lead->getNoBranchCount($users);

					//$data['eventDDB'] = $this->Model_lead->getEventDDB();					

				}

				

				$data['sourceDDB'] = $this->Model_lead->getSourceDDB($branch_id_for_filter);

				$data['formTypeDDB'] = $this->Model_lead->getFormTypeDDB($branch_id_for_filter);

				

				$data['assignToDDB'] = $this->Model_lead->getAssignToDDB($branch_id_for_filter,$users);

				$data['noAssignToCount'] = $this->Model_lead->getNoAssignToCount($branch_id_for_filter);

				

				

				/*$data['statusCounter'] = $this->Model_lead->getStatusCounter($branch_id_for_filter,$simple_user_id_for_filter);

				$data['delayedCounter'] = $this->Model_lead->getDelayedCounter($branch_id_for_filter,$simple_user_id_for_filter);

				$data['upComingCounter'] = $this->Model_lead->getUpComingCounter($branch_id_for_filter,$simple_user_id_for_filter);

				$data['surveyDCCounter'] = $this->Model_lead->getSurveyDCCounter();*/

								

				$sepcialData['countOnly'] = 1;

				$sepcialData['statusCounter'] = 1;

				$sepcialData['statusCounter2'] = 0;

				$sepcialData['delayedCounter'] = 0;

				$sepcialData['upComingCounter'] = 0;

				$data['statusCounter'] = $this->Model_lead->getAllSortedByPaged($pageNumber,0,1,$users,$keyWord,$checkLoggedInUserInboxRole,$applyFilter,$category_id,$status,$branchIdOfLoggedInManager,$branchIdOfLoggedInSubMng,$sepcialData);

				

				$sepcialData['countOnly'] = 1;

				$sepcialData['statusCounter'] = 0;

				$sepcialData['statusCounter2'] = 1;

				$sepcialData['delayedCounter'] = 0;

				$sepcialData['upComingCounter'] = 0;

				$data['statusCounter2'] = $this->Model_lead->getAllSortedByPaged($pageNumber,0,1,$users,$keyWord,$checkLoggedInUserInboxRole,$applyFilter,$category_id,$status,$branchIdOfLoggedInManager,$branchIdOfLoggedInSubMng,$sepcialData);

				

				$sepcialData['countOnly'] = 1;

				$sepcialData['statusCounter'] = 0;

				$sepcialData['statusCounter2'] = 0;

				$sepcialData['delayedCounter'] = 1;

				$sepcialData['upComingCounter'] = 0;

				$data['delayedCounter'] = $this->Model_lead->getAllSortedByPaged($pageNumber,0,1,$users,$keyWord,$checkLoggedInUserInboxRole,$applyFilter,$category_id,$status,$branchIdOfLoggedInManager,$branchIdOfLoggedInSubMng,$sepcialData);

				

				

				$sepcialData['countOnly'] = 1;

				$sepcialData['statusCounter'] = 0;

				$sepcialData['statusCounter2'] = 0;

				$sepcialData['delayedCounter'] = 0;

				$sepcialData['upComingCounter'] = 1;

				$data['upComingCounter'] = $this->Model_lead->getAllSortedByPaged($pageNumber,0,1,$users,$keyWord,$checkLoggedInUserInboxRole,$applyFilter,$category_id,$status,$branchIdOfLoggedInManager,$branchIdOfLoggedInSubMng,$sepcialData);

				

				$data['surveyDCCounter'] = $this->Model_lead->getSurveyDCCounter();

				

				

		//kashif

		if($viewLeadStatus == 0){

				$data['totalLeadsCountForPaging'] = $this->Model_lead->getTotalLeadsCountForPaging($branch_id_for_filter,$simple_user_id_for_filter);

		}

				//end faster====

				





				$leads_data =  array();

				$leadsDelayEmailArr = array();

				$leadsUpComingEmailArr = array();



				$usersArrAt = explode(",",$users);



				/*

				if($totalLeads)

				{

					foreach($totalLeads as $mainLead)

					{

						if($mainLead->branch_id > 0)

						{

							

							//if($data['managerView'] && (in_array($mainLead->assign_to,$usersArrAt) || rights(35,'read') ) ) //35,'read' view all leads

							if(rights(35,'read') ) //35,'read' view all leads

							{

								if(!in_array($mainLead->branch_id,$data['branchDDB']))

								{

									//$data['branchDDB'][] = $mainLead->branch_id;

									//$data['branchDDBCounter'][$mainLead->branch_id] = 1;

									//$data['branchDDBCounterWOC'][$mainLead->branch_id] = 1;									

								}

								else

								{

									//in array so increase the counter

									//$data['branchDDBCounter'][$mainLead->branch_id]++;

									//$data['branchDDBCounterWOC'][$mainLead->branch_id]++;

								}

							}



							if($data['managerView'] || rights(35,'read')) //35,'read' view all leads

							{ 

								if($mainLead->event_id)

								{

									if(!in_array($mainLead->event_id,$data['eventDDB']))

									{

										//$data['eventDDB'][] = $mainLead->event_id;

										//$data['eventDDBCounter'][$mainLead->event_id] = 1;

										//$data['eventDDBCounterWOC'][$mainLead->event_id] = 1;									

									}

									else

									{

										//in array so increase the counter

										//$data['eventDDBCounter'][$mainLead->event_id]++;

										//$data['eventDDBCounterWOC'][$mainLead->event_id]++;

									}

								}

							}



						}else {

							//$data['noBranchDDBCounter'][0]++; 

							//$data['noBranchDDBCounterWOC'][0]++;

						}



						if($mainLead->assign_to > 0)

						{

							//$data['totalAssignedLeadsCountWithOutConnected']++;								



							//if($data['managerView'] && in_array($mainLead->assign_to,$usersArrAt))

							if($data['managerView'] && (in_array($mainLead->assign_to,$usersArrAt) || rights(35,'read') ) ) //35,'read' view all leads

							{

								if(!in_array($mainLead->assign_to,$data['assignToDDB']))

								{

									//$data['assignToDDB'][] = $mainLead->assign_to;

									//$data['assignToDDBCounter'][$mainLead->assign_to] = 1;

									//$data['assignToDDBCounterWOC'][$mainLead->assign_to] = 1;



									//if($mainLead->assign_to==="163") {echo "here"; echo $data['assignToDDBCounterWOC'][$mainLead->assign_to]; exit();}

								}

								else

								{

									//in array so increase the counter

									//$data['assignToDDBCounter'][$mainLead->assign_to]++;

									//$data['assignToDDBCounterWOC'][$mainLead->assign_to]++;

								}

							}

						}else {

							//$data['noAssignToDDBCounter'][0]++; 

							//$data['noBranchDDBCounterWOC'][0]++; 

							//$data['totalUnAssignedLeadsCountWithOutConnected']++;

							}





						$source = "";

						if($mainLead->form_type!=="") 

							$source = $mainLead->form_type; 

						else 

						{

							if($mainLead->manual_source!=="0") 

								$source = 'manual_source'.$mainLead->manual_source;//getSourceTitle($mainLead->manual_source); 

							else 

								$source = 'created_by'.$mainLead->created_by;//getCreaterNameById($mainLead->created_by);

						}





						if($source) //it will always contain some value

						{



							//if($data['managerView'] && in_array($mainLead->created_by,$usersArrAt))

							//if($data['managerView'] && (in_array($source,$usersArrAt) || rights(35,'read') ) ) //35,'read' view all leads

							if($data['managerView'] || rights(35,'read') ) //35,'read' view all leads

							{

								if(!in_array($source,$data['sourceDDB']))

								{

									//$data['sourceDDB'][] = $source;

									//$data['sourceDDBCounter'][$source] = 1;

									//$data['sourceDDBCounterWOC'][$source] = 1;



									//if($mainLead->created_by==="163") {echo "here"; echo $data['sourceDDBCounterWOC'][$mainLead->created_by]; exit();}

								}

								else

								{

									//in array so increase the counter

									//$data['sourceDDBCounter'][$source]++;

									//$data['sourceDDBCounterWOC'][$source]++;

								}

							}

						}



						//$this->filterLeadsForDelAnUpc($mainLead, $leads_data, $leadsDelayEmailArr, $leadsUpComingEmailArr, $filterKeywordValue, $pageNumber);



						//$this->dueDateColCounterHelper($mainLead, $data, true, $usersArrAt);



						//=====

						$subCount = 0;

						$subLeads = getSubLeads1($mainLead->email,$mainLead->id); 

						if($subLeads) 

						{							 

							$subCount = count($subLeads);



							foreach($subLeads as $subLeadsC){



								if((int)$subLeadsC->branch_id > 0)

								{								

									//if($data['managerView'] && (in_array($subLeadsC->assign_to,$usersArrAt) || rights(35,'read') ) ) //35,'read' view all leads

									if(rights(35,'read') ) //35,'read' view all leads

									{

										if(!in_array($subLeadsC->branch_id,$data['branchDDB']))

										{

											//$data['branchDDB'][] = $subLeadsC->branch_id;

											//$data['branchDDBCounter'][$subLeadsC->branch_id] = 1;

											//$data['branchDDBCounterWOC'][$subLeadsC->branch_id] = 0; //just to create the index

										}

										else

										{

											//in array so increase the counter

											//$data['branchDDBCounter'][$subLeadsC->branch_id]++;

										}

									}

									

									if($data['managerView'] || rights(35,'read')) //35,'read' view all leads

									{ 

										if($subLeadsC->event_id)

										{

											if(!in_array($subLeadsC->event_id,$data['eventDDB']))

											{

												//$data['eventDDB'][] = $subLeadsC->event_id;

												//$data['eventDDBCounter'][$subLeadsC->event_id] = 1;

												//$data['eventDDBCounterWOC'][$subLeadsC->event_id] = 0; //just to create the index

											}

											else

											{

												//in array so increase the counter

												//$data['eventDDBCounter'][$subLeadsC->event_id]++;

											}

										}

									}



								}else {

									//$data['noBranchDDBCounter'][0]++;

								}



								if((int)$subLeadsC->assign_to > 0)

								{



									//$data['totalConnectedAssignedLeadsCount']++;									



									//if($data['managerView'] && in_array($subLeadsC->assign_to,$usersArrAt))

									if( ($data['managerView'] && (int)$subLeadsC->created_by === (int)$this->session->userdata['user']['id'] && $subLeadsC->status!='Approved and Archived' && $subLeadsC->status!='Duplicated') || rights(35,'read') ) //35,'read' view all leads

									{

										if(!in_array($subLeadsC->assign_to,$data['assignToDDB']))

										{

											//$data['assignToDDB'][] = $subLeadsC->assign_to;

											//$data['assignToDDBCounter'][$subLeadsC->assign_to] = 1;

											//$data['assignToDDBCounterWOC'][$subLeadsC->assign_to] = 0; //just to create the index

										}

										else

										{

											//in array so increase the counter

											//$data['assignToDDBCounter'][$subLeadsC->assign_to]++;

										}

										}



								}else {

									//if( ($data['managerView'] && $subLeadsC->status!='Approved and Archived' && $subLeadsC->status!='Duplicated') || rights(35,'read') ) //35,'read' view all leads

									if( $data['managerView'] || rights(35,'read') ) //35,'read' view all leads

									{

										if($subLeadsC->status!='Approved and Archived' && $subLeadsC->status!='Duplicated')

										{

											//$data['noAssignToDDBCounter'][0]++;

											//$data['totalConnectedUnAssignedLeadsCount']++;

										}

									}

								}





								$source = "";

								if($subLeadsC->form_type!=="") 

									$source = $subLeadsC->form_type; 

								else 

								{

									if($subLeadsC->manual_source!=="0") 

										$source = 'manual_source'.$subLeadsC->manual_source;//getSourceTitle($mainLead->manual_source); 

									else 

										$source = 'created_by'.$subLeadsC->created_by;//getCreaterNameById($mainLead->created_by);

								}



								if($source)

								{



									// in source sub lead check if the latest responsible person is the logged in manger.

									if( ($data['managerView'] && (int)$subLeadsC->created_by === (int)$this->session->userdata['user']['id'] && $subLeadsC->status!='Approved and Archived' && $subLeadsC->status!='Duplicated') || rights(35,'read') ) //35,'read' view all leads

									{

										if(!in_array($source,$data['sourceDDB']))

										{

											//$data['sourceDDB'][] = $source;

											//$data['sourceDDBCounter'][$source] = 1;

											//$data['sourceDDBCounterWOC'][$source] = 0; //just to create the index

										}

										else

										{

											//in array so increase the counter

											//$data['sourceDDBCounter'][$source]++;

										}

										}



								}

								

								//$this->filterLeadsForDelAnUpc($subLeadsC, $leads_data, $leadsDelayEmailArr, $leadsUpComingEmailArr, $filterKeywordValue, $pageNumber);



								//$this->dueDateColCounterHelper($subLeadsC, $data, false, $usersArrAt);

							}

						}					

						//$data['totalConnectedLeadsCount'] += $subCount;

						//======

			

					}

				}*/



				if($filterKeywordValue == 'Delayed' || $filterKeywordValue == 'Upcomming')

				{

					//$data['leads'] = $leads_data;

				}



				//=======================

			}



			$data['dashboard'] = false;

			$data['searchedOrFiltered'] = false;

			

			$this->load->view('template',$data);		

		

	}



	/*private function dueDateColCounterHelper($lead,&$data, $isMainLead,$usersArrAt)

	{



		if($isMainLead) 

		{

			//lets go further for calculations

		}

		elseif(rights(35,'read'))

		{

			//lets go further for calculations

		}

		else

		{

			//sub lead

			if($data['managerView'])

			{

				if( $lead->status!='Approved and Archived' && $lead->status!='Duplicated' && (int)$lead->created_by === (int)$this->session->userdata['user']['id']) 

				{

					//lets go further for calculations

				}

				else

				{

					return '';

				}



			}

			else

			{

				//simple employee

				if(((int)$lead->assign_to === (int)$this->session->userdata['user']['id'] || (int)$lead->created_by === (int)$this->session->userdata['user']['id'] ))

				{

					//lets go further for calculations



				}else

				{

					return '';

				}



			}

		}



		switch($lead->status)

		{

			case 'Finished':			

			$data['finishedCounter'] += 1;

			if($isMainLead)

			$data['finishedCounterWoc'] += 1;

			break;

			//case 'Approved and Archived':			

			//$data['approvedCounter'] += 1;

			//if($isMainLead)

			//$data['approvedCounterWoc'] += 1;

			//break;

			case 'Closed':			

			$data['closedCounter'] += 1;

			if($isMainLead)

			$data['closedCounterWoc'] += 1;

			break;

			case 'Duplicated':			

			$data['duplicatedCounter'] += 1;

			if($isMainLead)

			$data['duplicatedCounterWoc'] += 1;

			break;

			default:

			$getStatus = dueCounter($lead->due_date);

			if($getStatus == 'Delayed')

				{					

					$data['delayedCounter'] += 1;

					if($isMainLead)

					$data['delayedCounterWoc'] += 1;

				}else

				{					

					$data['upcommingCounter'] += 1;

					if($isMainLead)

					$data['upcommingCounterWoc'] += 1;

				}

			break;			

		}

	}*/



	/*private function filterLeadsForDelAnUpc($lead, &$leads_data, &$leadsDelayEmailArr, &$leadsUpComingEmailArr, $filterKeywordValue, $pageNumber)

	{



		//========

		$limit = 10; //10 per page

		if($pageNumber>1)

			$start = (((int)$pageNumber-1)*$limit);

		else

			$start = 0;



		$end = $limit*$pageNumber;

		//==========



		if($filterKeywordValue == 'Delayed' || $filterKeywordValue == 'Upcomming')

		{

			if($lead->status != "Closed" && $lead->status != "Finished" && $lead->status != "Approved and Archived" && $lead->status != "Duplicated")

			{



				if(in_array($lead->email,$leadsDelayEmailArr)) return '';

				if(in_array($lead->email,$leadsUpComingEmailArr)) return '';

				

				$getStatus = dueCounter($lead->due_date);

				if($filterKeywordValue == 'Delayed'){

					if($getStatus == 'Delayed')

					{

						if(count($leadsDelayEmailArr)>=$start && count($leadsDelayEmailArr)<$end)

							$leads_data[] = $lead;



						$leadsDelayEmailArr[] = $lead->email;	

					}

				}else if($filterKeywordValue == 'Upcomming'){

					if($getStatus != 'Delayed')

					{

						$leads_data[] = $lead;

						$leadsUpComingEmailArr[] = $lead->email;	

					}

				}



					



			}

		}

	}*/

		

	/*public function filter($category_id, $status){

				

		$where = array();

		$data = array();

		$applyFilter = true;

		

		$status = str_replace("%20"," ",$status);

		

		$this->index($applyFilter, $category_id, $status);



		$users = $this->usersUnderMngRecursive();



		$data['leads'] = $this->Model_lead->getFilteredLeads($where, $users);		



		$data['view'] = 'lead/leads';

        $data['lead_class'] = 'active';		

		$data['isSingleDetailLead'] = false;

		$data['cities'] = $this->Model_city->getAll();

		$data['logged_in_user_id'] = $this->session->userdata['user']['id'];

		$data['categories'] = $this->Model_category->getAll();

		$data['dashboard'] = false;

		$data['searchedOrFiltered'] = true;

		$data['vehicles_specific'] = $this->Model_vehicle_specific_name->getAll(); 

		$data['tags'] = $this->Model_tag->getAll();

		$this->load->view('template',$data);

		

		

	}	*/	

	

	/*public function getEmployeeByBranchAndCategoryAjax()

	{

		

		$employees = getEmployeeByCategory($this->input->post('category_id'),$this->input->post('branch_id')); 

		$options = "";

		if(is_array($employees))

		{

			foreach($employees as $employee){				

				$options .= '<option value="'.$employee->id.'">'.$employee->full_name.'</option>';

			}

			

			echo $options;

			exit;

		}

	}*/



	public function createNewLead($param = NULL, $isServiceLead = NULL)

	{



		$data = array();

	/*======== set the param value if not null to populate in create form=========*/	

		$param = urldecode($param);

		if($param == NULL || $param == "p"){ 

			$data['searchParam'] = "";

		}else{

			$data['searchParam'] = $param;

		}

	/*======== End param populate in create form=========*/		

		$data['isServiceLead'] = false;

		if($isServiceLead !== NULL){ 

			$data['isServiceLead'] = true;

		}		

	

		$data['vehicles'] = $this->Model_vehicle->getAllSorted(); 

		$data['vehicles_specific'] = $this->Model_vehicle_specific_name->getAll(); 

		$data['cities'] = $this->Model_city->getAll();

		$data['sources'] = $this->Model_source->getAll();		

		//$data['events'] = $this->Model_event->getAll();		

		//10 jan 2017

		$whereEvent = array();

		$whereEvent['is_active'] = 1;

		$data['events'] = $this->Model_event->getMultipleRows($whereEvent);

		

		$data['tags'] = $this->Model_tag->getAll();

		$data['view'] = 'lead/create_edit_lead'; //same view is used for insert or update both

		$data['categories'] = $this->Model_category->getAll();

		$data['complaint_types'] = $this->Model_complaint_type->getAll();

		$data['edit'] = false;

        $data['lead_class'] = 'active';

		$this->load->view('template',$data);



	}



	public function editLead($id, $isServiceLead = NULL)

	{

		



		$data = array();

		

		$data = $this->Model_lead->get($id,true);		

		

		$data['isServiceLead'] = false;

		if($isServiceLead !== NULL){ 

			$data['isServiceLead'] = true;

		}	

		

		$data['vehicles'] = $this->Model_vehicle->getAllSorted(); 

		$data['vehicles_specific'] = $this->Model_vehicle_specific_name->getAll(); 

		$data['cities'] = $this->Model_city->getAll();

		$data['sources'] = $this->Model_source->getAll();		

		//$data['events'] = $this->Model_event->getAll();

		//10 jan 2017

		$whereEvent = array();

		$whereEvent['is_active'] = 1;

		$data['events'] = $this->Model_event->getMultipleRows($whereEvent);

		

		$data['tags'] = $this->Model_tag->getAll();

		$data['view'] = 'lead/create_edit_lead'; //same view is used for insert or update both

		$data['categories'] = $this->Model_category->getAll();

		$data['complaint_types'] = $this->Model_complaint_type->getAll();

		$data['edit'] = true;

        $data['lead_class'] = 'active';

		$this->load->view('template',$data);



	}



	/*===========if lead exist then repopulate in create new lead form============*/

	public function createAgainLead($id){

		$data = array();

		$data = $this->Model_lead->get($id,true);		

		$data['vehicles'] = $this->Model_vehicle->getAllSorted(); 

		$data['vehicles_specific'] = $this->Model_vehicle_specific_name->getAll(); 

		$data['cities'] = $this->Model_city->getAll();

		$data['sources'] = $this->Model_source->getAll();	

		

		//$data['events'] = $this->Model_event->getAll();

		//10 jan 2017

		$whereEvent = array();

		$whereEvent['is_active'] = 1;

		$data['events'] = $this->Model_event->getMultipleRows($whereEvent);

		

		$data['tags'] = $this->Model_tag->getAll();

		$data['view'] = 'lead/create_edit_lead'; //same view is used for insert or update both

		$data['categories'] = $this->Model_category->getAll();

		$data['again'] = true;

		$data['edit'] = false;

        $data['lead_class'] = 'active';

		$this->load->view('template',$data);

	}

	

	/*=========== End Re-create lead function============*/

	private function checkLicense()

	{

		//Please note, test drive cannot be booked without a valid driving license

			$preferred_date = $this->input->post('preferred_date');

			//Check if Valid Driving License = Yes

			$ValidDrivingLicenseYes = 0;

			if($this->input->post('valid_driving_license')==='1')

			{

				$ValidDrivingLicenseYes = $this->input->post('valid_driving_license');

			}



			if($preferred_date!='')

			{

				if(!$ValidDrivingLicenseYes)

				{

					$data['success'] = 'false';

					$data['error'] = 'Test drive cannot be booked without a valid driving license.';

					$data['reset'] = 0;

					$data['reload'] = 0;

					echo json_encode($data);

					exit;



				}



			}

	}



	private function updateLateLeadNotifination()

	{

		$ids = $this->input->post('ids');

		foreach($ids as $id)

		{

			$dataLead['late_lead_notication_read'] = 1;

			$where['id'] = $id;

			$this->Model_lead->update($dataLead,$where);

		}

		return true;

	}

	

	private function adminAssignLeadAction()

	{

		$data = array();

		$ids = $this->input->post('lead_id'); //this lead_id can contains the commma seperated ids of leads

		$idsArr = explode(",",$ids);

		$assign_to = $this->input->post('assign_to');



		foreach($idsArr as $lead_id)

		{

			$this->assignLead($lead_id, $assign_to);

		}



		$data['reload'] = 0;

		$data['reset'] = 0;	

		$data['success'] = "Lead(s) Assigned";	

		$data['error'] = 'false';

		$data['employeeName'] = getEmployeeName($assign_to);

		echo json_encode($data);

		exit;		

	}



	//this function is called from a loop in case of multi assign.

	private function assignLead($lead_id, $assign_to)

	{

		$dataLead = array();

		$success_msg = "";

		$post_data = $this->input->post();

		

		//get the assignee user branch

		$assigneeCityId = getCityIdOfUserByUserId($assign_to);

		$assigneeBranchId = getBranchIdOfUserByUserId($assign_to);



		//creator of the lead will be changed as per the new department manager.	

		$newCreator = 0; //basically we are using the created_by column as a new responsible person. (The orignal created backup column is also there.)

		$newAssigneeManagingDeps = $this->Model_structure_departments->getUserManagingDeps($assign_to);

		if(is_array($newAssigneeManagingDeps) && count($newAssigneeManagingDeps)>0)

		{			

			$newCreator = $assign_to; //In case of lead is assigned to other department's manager. the creator and assignee both will be same.

		}

		else

		{

			$wdu = array();

			$wdu['user_id'] = $assign_to;

			$thisUserDeps = $this->Model_struc_dep_users->getMultipleRows($wdu);

			if($thisUserDeps)

			{

				$whereSd = array();

				$whereSd['id'] = $thisUserDeps[0]->struc_dep_id;

				$deps = $this->Model_structure_departments->getMultipleRows($whereSd); //Dep the user belongs to as an employee in structure.

				if($deps)

				{

					$newCreator = $deps[0]->manager_id;

				}

			}

		}



		//assign lead

		$dataLead['city_id']  = $assigneeCityId;

		$dataLead['branch_id']  = $assigneeBranchId;

		$dataLead['assign_to']  = $assign_to;

		$dataLead['assign_by']  = $this->session->userdata['user']['id'];

		$dataLead['assigned_at'] = date('Y-m-d H:i:s');	

		$dataLead['status'] = 'Assigned';

		if($newCreator)

		$dataLead['created_by'] = $newCreator; //Note: in case of latest responsible manager the id will be replaced with latest manager. the backup column is there :)

		$where['id'] = $lead_id;					

		$this->Model_lead->update($dataLead,$where);



		//set notification

		$this->setNotification('Lead Assigned', $lead_id);	

		

		//add a message that lead is assigned in the action log box

		$dataM['lead_id'] = $lead_id;

		$dataM['message_id'] = 19;		

		$dataM['comments'] = "Lead is assigned to ".getEmployeeName($assign_to);

		$dataM['created_at'] = date('Y-m-d H:i:s');

		$dataM['created_by'] = $this->session->userdata['user']['id'];

		$this->Model_leads_messages->save($dataM);

		//==========



		//if the user is assigned now, then delete old tags so now this user can get the full assignee rights. Note: the name in tags will remain there.

		$mtD = array();

		$mtD['user_id'] = $assign_to;

		$mtD['lead_id'] = $lead_id;



		$this->Model_lead_tags->delete($mtD);		

		



	}



	private function sendSurvey($lead_id)

	{



		$dataLead = array();

		$dataMessage = array();

		$success_msg = "";

		$post_data = $this->input->post();

		

		//======

		$length = 10;

		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

		$charactersLength = strlen($characters);

		$randomString = '';

		for ($i = 0; $i < $length; $i++) {

			$randomString .= $characters[rand(0, $charactersLength - 1)];

		}

		//======



		//Send survery via email.

		$emailData = array();

		$leadRec = $this->Model_lead->get($lead_id);

		$cat_row = $this->Model_category->get($leadRec->category_id,true);



		$emailData['trackid'] = $leadRec->trackid;		

		$emailData['first_name'] = $leadRec->first_name;

		$emailData['category_title'] = $cat_row['title_email'];

		$emailData['category_title_ar'] = $cat_row['title_email_ar'];

		$surveyData = array();

		$surveyData['category_id'] = $leadRec->category_id;

		$surveyData['is_active'] = 1;

		$surveyRec = $this->Model_survey->getWithMultipleFields($surveyData);

		$dataLead['survey_sent'] = 0;

		$dataLead['survey_id'] = 0;

		if($surveyRec)

		{

			//$emailData['survey_link'] = base_url().'lead_survey/surveyAction/'.$surveyRec->id.'/'.$post_data['lead_id'];

			$emailData['survey_link'] = '<a href="'.base_url().'lead_survey/surveyAction/'.$surveyRec->id.'/'.$post_data['lead_id'].'/'.$randomString.'">Take The Survey</a>';			

			$emailData['survey_link_ar'] = '<a href="'.base_url().'lead_survey/surveyAction/'.$surveyRec->id.'/'.$post_data['lead_id'].'/'.$randomString.'">قم بتعبئة الإستبيان</a>';			

			$emailData['created_date'] = $leadRec->created_at;

			sendEmail(3, $leadRec->email, $emailData); //2 is the email id from database. it concatinates the email body

			$dataLead['survey_sent'] = 1;

			$dataLead['survey_id'] = $surveyRec->id;	

			$dataLead['survey_link_rand'] = $randomString;

			$dataLead['survey_sent_datetime'] = date('Y-m-d H:i:s');			

			

		}

						

		$where['id'] = $lead_id;

		$this->Model_lead->update($dataLead,$where);

		

	}



	private function setNotification($comments, $lead_id, $messageRec=null)

	{

		$dataLead = array();

		$dataNotification = array();



		//get lead rec for which the notification is going to be added.

		$leadRec = $this->Model_lead->get($lead_id);



		if($leadRec)

		{		

			if($leadRec->assign_to === $this->session->userdata['user']['id'] && $leadRec->created_by === $this->session->userdata['user']['id'])

			{

				//actually this is a case when a simple employee assigns a lead to his manager then the responsible becomes the manager, and assignee is also a manager.

				//so both creator and assignee are same. 



				//do nothing



			}else

			{



				if($leadRec->created_by==="0") //inbox lead

				{	

					if(false) //not a case anymore

					{

						$dataNotification['inbox_notification_read'] = 0;

					}

					else

					{

						if($leadRec->assign_to === $this->session->userdata['user']['id'])

							$dataNotification['inbox_notification_read'] = 1;



						if(checkLoggedInUserInboxRole())

						{

							if($leadRec->assign_to)

							$dataNotification['assignee_notification_read'] = 1;

						}

					}			

				}

				else

				{

					if(false) //it is handled through other conditions

					{		

						$dataNotification['assignee_notification_read'] = 0;

						$dataNotification['creater_notification_read'] = 0;						

					}

					else

					{

						if(/*$messageRec->show_on_creator_dashboard && */$leadRec->assign_to === $this->session->userdata['user']['id'])

							$dataNotification['creater_notification_read'] = 1;

						

						if(/*$messageRec->show_on_assignee_dashboard && */$leadRec->created_by === $this->session->userdata['user']['id'])

						{

							if(strpos($comments,"New lead created")===false)						

							{

								if($leadRec->assign_to)

								$dataNotification['assignee_notification_read'] = 1;

							}

							

						}



						$whereTagged = array();

						$whereTagged['user_id'] = $this->session->userdata['user']['id'];

						$whereTagged['lead_id'] = $lead_id;

						$checkIfTaggedUserIsReplying = $this->Model_lead_tags->getWithMultipleFields($whereTagged);

						//print_r($checkIfTaggedUerIsReplying); exit();

						if($checkIfTaggedUserIsReplying)

						{

							$dataNotification['creater_notification_read'] = 1;

						}

					}		

				}

			}

		}

				

		



		$dataNotification['created_at'] = date('Y-m-d H:i:s');

		$dataNotification['created_by'] = $this->session->userdata['user']['id'];	

		$dataNotification['lead_id'] = $lead_id;

		$dataNotification['comments'] = $comments;		

		$this->Model_notification->save($dataNotification);

		

	}





	public function setNotificationRead()

	{

		$dataLead = array();

		$dataNotification = array();



		$leadRec = $this->Model_lead->get($this->input->post('lead_id'));

		$notificationRec = $this->Model_notification->get($this->input->post('id'));

		if($leadRec->created_by == $this->session->userdata['user']['id'])

		{

			$dataNotification['creater_notification_read'] = 0;

		}

		elseif($leadRec->assign_to === $this->session->userdata['user']['id'])

		{

			$dataNotification['assignee_notification_read'] = 0;

		}

		elseif($notificationRec->tagged_user_id === $this->session->userdata['user']['id'])

		{

			//tagged

			$dataNotification['tagged_user_notification_read'] = 0;

			$where['tagged_user_id'] = $this->session->userdata['user']['id'];



		}

		elseif($leadRec->created_by === "0")

		{

			$dataNotification['inbox_notification_read'] = 0;

		}

		

		

		



		$where['id'] = $this->input->post('id');

		$this->Model_notification->update($dataNotification,$where);

		



	}



	public function clearAllNotification(){

		$data = array();

		$success = $this->Model_lead->allNotificationsRead();

		if($success == 1){

			echo "1"; 

			exit;

		}

	}

	public function allNotificationRead()

	{

		$dataLead = array();

		$dataNotification = array();





		$leadRec = $this->Model_lead->get($this->input->post('lead_id'));

		$notificationRec = $this->Model_notification->get($this->input->post('id'));

		

		if($leadRec->created_by === $this->session->userdata['user']['id'])

		{

			$dataNotification['creater_notification_read'] = 0;

		}

		elseif($leadRec->assign_to === $this->session->userdata['user']['id'])

		{

			$dataNotification['assignee_notification_read'] = 0;

		}

		elseif($notificationRec && $notificationRec->tagged_user_id === $this->session->userdata['user']['id'])

		{

			//tagged

			$dataNotification['tagged_user_notification_read'] = 0;

			$where['tagged_user_id'] = $this->session->userdata['user']['id'];

		}

		elseif($leadRec->created_by === "0")

		{

			$dataNotification['inbox_notification_read'] = 0;

		}

		

		

				

		$where['lead_id'] = $this->input->post('lead_id');

		if(count($dataNotification))

		$this->Model_notification->update($dataNotification,$where);





	}

	

	private function addLeadMessage()

	{

		

		$data = array();

		$dataNotification = array();

		$dataTagged = array();





		//get Data Ready

		$this->not_a_lead_type = $this->input->post('not_a_lead_type');

		$post_data = $this->input->post();

		

		foreach($post_data as $key => $value)

		{

			if($key != 'form_type' && $key!='create_action_date' && $key!='create_action_time' && $key!='call_status' && $key!='send_survey' && $key!='tagged_ids' && $key!='tag_id' && $key!='update_scheduled_message_id' && $key!='not_a_lead_type' && $key!='purchased_vehicle_id' && $key != "changeTestDrive")

				$data[$key] = $value;

		}



		$create_action_date = $this->input->post('create_action_date');

		$create_action_time = $this->input->post('create_action_time');

		$data['schedule_date_time'] = $create_action_date.' '.$create_action_time;

		$data['schedule_date_time'] = trim($data['schedule_date_time']);

		

		if($data['message_id'] === "5"){

			if(!empty($data['vehicle_specific_names_id'])){

				

				$vehicle_specific_names_id_array = $data['vehicle_specific_names_id'];

				$data['vehicle_specific_names_id'] = implode(',', $data['vehicle_specific_names_id']);

				

			  }else{

				$data['vehicle_specific_names_id'] = '';

			  }	

		}

		//note tag_id is the drop down box tag e.g coming from db table tags. (In settings screen)

		//The @ tagging any other employee is by using "tagged_ids" that is other functionality.

		if($data['message_id']==="4" && $this->input->post('tag_id')!=null && $this->input->post('tag_id')!=="") 

		{	

			$dataT = array();

			$whereT = array();

			$dataT['tag_id'] = $this->input->post('tag_id');

			$dataT['purchased_vehicle_id'] = $this->input->post('purchased_vehicle_id');			

			if($dataT['tag_id']=="2") $dataT['purchased_at'] = date('Y-m-d H:i:s'); //starting from 3rd Oct 2018

			if($dataT['tag_id']=="2") $dataT['sold_by'] = $this->session->userdata['user']['id']; //starting from 3rd Oct 2018

			$whereT['id'] = $data['lead_id'];

			$this->Model_lead->update($dataT,$whereT);

			$data['comments'] = "<label class='tagged_comment_lbl'>".getTagTitle($dataT['tag_id']).".</label> ".$data['comments'];

		}

		

		

		if($this->input->post('update_scheduled_message_id')!=null && $this->input->post('update_scheduled_message_id')!=="0")

		{ 	

			$data['created_at'] = date('Y-m-d H:i:s');

			$whereMsgUpd['id'] = $this->input->post('update_scheduled_message_id');

			

		// if test drive is update from schedule to walk in then 

		 if(isset($post_data['test_drive_type'])){

			 if($post_data['test_drive_type'] == "Walk in" && $post_data['changeTestDrive'] == 1){

				

				// cancel record for schedule test drive if changed to walk in

				unset($data['schedule_date_time']);

				$data['test_drive_canceled'] = "1";

				$data['test_drive_type'] = "Scheduled";

				$this->Model_leads_messages->update($data,$whereMsgUpd);

		

				// inserting record as new for walk in test drive

				$data['test_drive_completed']= "1";

				$data['created_by']= $this->session->userdata['user']['id'];

				$data['test_drive_type'] = $post_data['test_drive_type'];

				$returnId = $this->Model_leads_messages->save($data);

			 }else{

				$this->Model_leads_messages->update($data,$whereMsgUpd);

				 	//echo $this->db->last_query(); exit();

			 }

		 }

		 

		 

		 //=====the call action is being updated

		 if($post_data['message_id']==="8" || $post_data['message_id']==="9" || $post_data['message_id']==="10")

		 {

			 /*if($post_data['message_id']==="9" || $post_data['message_id']==="10")

			 {

				 $this->Model_leads_messages->save($data);

			 }

			 else

			 {*/

				 unset($data['created_at']);

				 $this->Model_leads_messages->update($data,$whereMsgUpd);

			 //}

		 }

		 // send email to technical support 

		

		 

		 //=====

		 

		 

		 //print_r($post_data); exit;

		//======== end ===========



		}else

		{ 	 

			//add message 

			$data['created_at'] = date('Y-m-d H:i:s');

			$data['created_by'] = $this->session->userdata['user']['id'];

			

			//insert test drive data one by one

			if(isset($vehicle_specific_names_id_array)){

				foreach ($vehicle_specific_names_id_array as $vehSpecificNames){	

					$data['vehicle_specific_names_id'] = $vehSpecificNames;	

					$insert_id = $this->Model_leads_messages->save($data);

				}

				// again set vehicles as comma sepread string for next use.

				$data['vehicle_specific_names_id'] = implode(",",$vehicle_specific_names_id_array);

			}else{

				$insert_id = $this->Model_leads_messages->save($data);

			}

		}

	

		//get message record

		$messageRec = $this->Model_messages->get($data['message_id']);

		

		//set notification			

		$this->setNotification($messageRec->title.' Action Added', $data['lead_id'], $messageRec);



		//tagged users

		$tagged_ids = $this->input->post('tagged_ids');

		if( ($data['message_id']==="4" || $data['message_id']==="9" || $data['message_id']==="8" || $data['message_id']==="10" || $data['message_id']==="3" || $data['message_id']==="5") && $tagged_ids) //note this tagged_ids are user ids who are tagged in this comment by @

		{ 

			$tagged_ids = explode(",",$tagged_ids);

			foreach($tagged_ids as $tagged_id) //loop here for the tagged user ids 

			{

				//add notifications for each tagged user

				$dataNotification['tagged_user_notification_read'] = 1;

				$dataNotification['tagged_user_id'] = $tagged_id;

				$dataNotification['created_at'] = date('Y-m-d H:i:s');

				$dataNotification['created_by'] = $this->session->userdata['user']['id'];	

				$dataNotification['lead_id'] = $data['lead_id'];

				$dataNotification['comments'] = "You are tagged";

				$this->Model_notification->save($dataNotification);



				

				//First unactive the old tags for same user.

				$dataUpd['active'] = 0;

				$whereUpd['user_id'] = $tagged_id;

				$whereUpd['lead_id'] = $data['lead_id'];

				$this->Model_lead_tags->update($dataUpd,$whereUpd);

				//Now save record in forign table for tagged users for this lead.

				$dataTagged['lead_id'] = $data['lead_id'];

				$dataTagged['tagged_datetime'] = date('Y-m-d H:i:s');

				$dataTagged['tagged_by'] = $this->session->userdata['user']['id'];

				$dataTagged['user_id'] = $tagged_id;

				$dataTagged['active'] = 1;

				$this->Model_lead_tags->save($dataTagged);

			}

		}



		//send email if applicable

		$leadRes = $this->Model_lead->get($data['lead_id']);

		if($data['message_id'] === "2")

		{ 

			$emailData = array();			

			$cat_row = $this->Model_category->get($leadRes->category_id,true);



			$emailData['trackid'] = $leadRes->trackid;		

			$emailData['first_name'] = $leadRes->first_name;

			$emailData['category_title'] = $cat_row['title'];

			$emailData['body_concatinate'] = $data['comments'];

			$emailData['assignee_name'] = getEmployeeName($leadRes->assign_to);

			if($emailData['assignee_name']=="") $emailData['assignee_name'] = getEmployeeName($this->session->userdata['user']['id']);

			$emailData['assignee_name_ar'] = getEmployeeNameAr($leadRes->assign_to);

			if($emailData['assignee_name_ar']=="") $emailData['assignee_name_ar'] = getEmployeeNameAr($this->session->userdata['user']['id']);

			$emailData['showroom'] = getBranchName($leadRes->branch_id);

			$emailData['showroom_ar'] = getBranchNameAr($leadRes->branch_id);

			$emailData['assign_to_contact'] = getEmployeeTelNExt($leadRes->assign_to);

			if($emailData['assign_to_contact']=="") $emailData['assign_to_contact'] = getEmployeeTelNExt($this->session->userdata['user']['id']);

			if($leadRes->assign_to) $emailData['job_title'] = getEmployeeJobTitle($leadRes->assign_to);

			else $emailData['job_title'] = getEmployeeJobTitle($this->session->userdata['user']['id']);

			if($leadRes->assign_to) $emailData['job_title_ar'] = getEmployeeJobTitleAr($leadRes->assign_to);

			else $emailData['job_title_ar'] = getEmployeeJobTitleAr($this->session->userdata['user']['id']);



			sendEmail(2, $leadRes->email, $emailData); //2 is the email id from database. it concatinates the email body

		}

		elseif($data['message_id'] === "10") //Phone call answer = No

		{

			$emailData = array();			

			$cat_row = $this->Model_category->get($leadRes->category_id,true);



			$emailData['trackid'] = $leadRes->trackid;		

			$emailData['first_name'] = $leadRes->first_name;

			$emailData['category_title'] = $cat_row['title'];



			$emailData['call_time'] = date('Y-m-d H:i:s');

			$emailData['contact_number'] = $leadRes->mobile;

			$emailData['assign_to_contact'] = getEmployeeTelNExt($leadRes->assign_to);

			if($emailData['assign_to_contact']=="") $emailData['assign_to_contact'] = getEmployeeTelNExt($this->session->userdata['user']['id']);



			$emailData['assignee_name'] = getEmployeeName($leadRes->assign_to);

			if($emailData['assignee_name']=="") $emailData['assignee_name'] = getEmployeeName($this->session->userdata['user']['id']);



			$emailData['assignee_name_ar'] = getEmployeeNameAr($leadRes->assign_to);

			if($emailData['assignee_name_ar']=="") $emailData['assignee_name_ar'] = getEmployeeNameAr($this->session->userdata['user']['id']);

			

			sendEmail(4, $leadRes->email, $emailData); 

		}		

		elseif($data['message_id'] === "8") //Re-schedule call

		{

			$emailData = array();			

			$cat_row = $this->Model_category->get($leadRes->category_id,true);



			$emailData['trackid'] = $leadRes->trackid;		

			$emailData['first_name'] = $leadRes->first_name;

			$emailData['category_title'] = $cat_row['title'];

			$emailData['create_action_date'] = $create_action_date;

			$emailData['create_action_time'] = $create_action_time;

			$emailData['contact_number'] = $leadRes->mobile;

			$emailData['assign_to_contact'] = getEmployeeTelNExt($leadRes->assign_to);

			if($emailData['assign_to_contact']=="") $emailData['assign_to_contact'] = getEmployeeTelNExt($this->session->userdata['user']['id']);

			

			

			sendEmail(5, $leadRes->email, $emailData);

		}

		elseif($data['message_id'] === "5" && isset($data['test_drive_type']) && $data['test_drive_type']==="Scheduled" && $this->input->post('update_scheduled_message_id')==='0') //schedule test drive

		{

			$emailData = array();			

			$cat_row = $this->Model_category->get($leadRes->category_id,true);



			$emailData['trackid'] = $leadRes->trackid;		

			$emailData['first_name'] = $leadRes->first_name;

			$emailData['category_title'] = $cat_row['title'];

			$emailData['create_action_date'] = $create_action_date;

			$emailData['create_action_time'] = $create_action_time;

			$emailData['assignee_name'] = getEmployeeName($leadRes->assign_to);

			if($emailData['assignee_name']=="") $emailData['assignee_name'] = getEmployeeName($this->session->userdata['user']['id']);

			$emailData['assignee_name_ar'] = getEmployeeNameAr($leadRes->assign_to);

			if($emailData['assignee_name_ar']=="") $emailData['assignee_name_ar'] = getEmployeeNameAr($this->session->userdata['user']['id']);

			$emailData['showroom'] = getBranchName($leadRes->branch_id);

			$emailData['showroom_ar'] = getBranchNameAr($leadRes->branch_id);

			$emailData['assign_to_contact'] = getEmployeeTelNExt($leadRes->assign_to);

			if($emailData['assign_to_contact']=="") $emailData['assign_to_contact'] = getEmployeeTelNExt($this->session->userdata['user']['id']);

			$vehicles = "";

			if($data['vehicle_specific_names_id']!="")

			{

				 $vehicle_specific_names_id_arr = explode(",",$data['vehicle_specific_names_id']);

				foreach($vehicle_specific_names_id_arr as $vsi)

				{

					if($vehicles!="") $vehicles .= ", ";

					$vehicles .= getSpecificVehicleNameById($vsi);

				}

			}

			$emailData['vehicles'] = $vehicles;

			sendEmail(6, $leadRes->email, $emailData);

		}

		// send email to the technical support

		if($post_data['message_id']==="4" && strpos(strtolower($post_data['comments']),'#ts') !== false){

			

			$emailData = array();			

			$emailData['trackid'] = $leadRes->trackid;		

			$emailData['comments'] = $post_data['comments'];

			sendTsEmail($emailData); 

		}





		//change status of lead if applicable.

		if($data['message_id'] === "6")

			$this->changeStatusOfLead('No Response',$data['lead_id']);

		elseif($data['message_id'] === "7")		

			$this->changeStatusOfLead('Finished',$data['lead_id']);

		elseif($data['message_id'] === "11")

		{

			$this->changeStatusOfLead('Closed',$data['lead_id']);

			if($this->input->post('send_survey')==='1')

				$this->sendSurvey($data['lead_id']);

		}

		elseif($data['message_id'] === "14")

			$this->changeStatusOfLead('Disapproved',$data['lead_id']);

		elseif($data['message_id'] === "14")

			$this->changeStatusOfLead('Disapproved',$data['lead_id']);

		elseif($data['message_id'] === "13")

			$this->changeStatusOfLead('Approved and Archived',$data['lead_id']);



		if($data['message_id']==="9")  //Did you get an answer = yes

		{

			$dataDl = array();

			$whereDl = array();



			$dataDl['due_date'] = calculateLeadDeadline(date('Y-m-d H:i:s'), $leadRes->category_id, 'closeKPI');

			$whereDl['id'] = $data['lead_id'];



			$this->Model_lead->update($dataDl,$whereDl);



		}



		//save first call time only for the first call.

		if($data['message_id']==="8" || $data['message_id']==="9" || $data['message_id']==="10") 

		{

			$dataFc = array();

			$whereFc = array();



			$dataFc['first_call_time'] = date('Y-m-d H:i:s');			

			$whereFc['id'] = $data['lead_id'];

			$whereFc['first_call_time'] = '0000-00-00 00:00:00';



			$this->Model_lead->update($dataFc,$whereFc);



		}

       

		



		

		$data['success'] = $messageRec->title.' Action Added.';

		$data['error'] = 'false';

		$data['reset'] = 1;

		$data['reload'] = 0;					

		echo json_encode($data);

		exit;			

	}



	private function changeStatusOfLead($status,$lead_id)

	{

		$data = array();



		$data['status'] = $status;

		$where['id'] = $lead_id;

		if($status==="Finished")

		{

			$data['finished_time'] = date('Y-m-d H:i:s');

			$data['not_a_lead_type'] = $this->not_a_lead_type;

		}

		elseif($status==="No Response") //actually clicking on no response is also a way to say that I am done with this lead.

			$data['finished_time'] = date('Y-m-d H:i:s');

		elseif($status==="Closed")

		{

			$data['close_time'] = date('Y-m-d H:i:s');



			//close its duplicated leads too.

			$dataD = array();

			$whereD = array();

			$dataD['status'] = $data['status'];

			$dataD['close_time'] = date('Y-m-d H:i:s');

			$whereD['duplicate_of'] = $lead_id;

			$this->Model_lead->update($dataD,$whereD);



		}



		$insert_lead_id = $this->Model_lead->update($data,$where);

	}



	private function deleteLeads()

	{



		$data = array();

		$checkBoxesSelectedCommas = $this->input->post('id');

		$checkBoxesSelectedCommasArr = explode(",",$checkBoxesSelectedCommas);





		foreach($checkBoxesSelectedCommasArr as $checkBoxesSelected)

		{

			

			//get the email address

			$leadRec = $this->Model_lead->get($checkBoxesSelected);



			//2nd parameter is the limit and this function already have order by id desc

			$subLatestLeadResult = $this->Model_lead->getSubLeads($leadRec->email, $leadRec->mobile, $leadRec->id, 1); 

			

			if($subLatestLeadResult)

			{

				$idToMakeSubtoLatest['id'] = $subLatestLeadResult[0]->id;

				$subToLatestData['new_lead_with_same_email'] = 1;



				$this->Model_lead->update($subToLatestData, $idToMakeSubtoLatest);

			}



			

			$this->setNotification('Lead deleted '.$leadRec->trackid, -1); 



		}



		$this->Model_lead->deleteIn('id',$checkBoxesSelectedCommasArr);		



		return true;

	}



	

	private function checkLeadsSameCityBranch()

	{



		$data = array();

		$leadsIds = $this->input->post('lead_ids');

		$leadsIdsArr = explode(",",$leadsIds);

		$data['success'] = "1";	



		$leadsRecs = $this->Model_lead->getLeadsWithIdsArr($leadsIdsArr);



		$city_id = 0;

		$branch_id = 0;

		$counter = 0;

		foreach($leadsRecs as $lead)

		{

			//for the first lead set the variables values.

			if($counter===0)

			{

				$city_id = $lead->city_id;

				$branch_id = $lead->branch_id;

			}

			else //for further leads keep on checking if same

			{

				if($lead->city_id == $city_id && $lead->branch_id == $branch_id)

				{

					//do nothing				

				}

				else

				{

					$data['success'] = "0";	

					break;

				}

			}			



			$counter++;

		}





		$cityRec = $this->Model_city->get($city_id);

		$branchRec = $this->Model_branch->get($branch_id);



		$data['city_id'] = $city_id;

		$data['city_name'] = $cityRec->title;

		$data['branch_id'] = $branch_id;

		$data['branch_name'] = $branchRec->title;



		echo json_encode($data);

		exit;

	}

	



	public function singleLead($id){



	//single lead will use the same leads listing screen but with if else to show only single lead.

		

		$data = array();

		$data['managerView'] = false;

		$data['view'] = 'lead/leads';

        $data['lead_class'] = 'active';

		//$data['leads'] = $this->Model_lead->getAllSortedBy();



		$data['leads'] = $this->Model_lead->getSingleLead($id);



		$email = $data['leads'][0]->email;



		/*

		need to improve this logic. for now just no need. 

		$allRelatedLeads = getAllRelatedLeads();

		$okCanAccess = false;

		foreach($allRelatedLeads as $relatedLead)

		{

			if($relatedLead->email == $email)

			{

				$okCanAccess = true;

				break;

			}

		}

		if(!$okCanAccess)

		{

			$data['leads'] = array();

		}*/







		$loggedInUserManagingDeps = getLoggedInUserManagingDeps();		

		if(is_array($loggedInUserManagingDeps) && count($loggedInUserManagingDeps)>0)

		{			

			$data['managerView'] = true;

		}

		elseif( rights(33,'read') )

		{

			 //33 is lead controller

			$data['managerView'] = true;			

		}



		$data['isSingleDetailLead'] = true;

		$data['cities'] = $this->Model_city->getAll();

		$data['logged_in_user_id'] = $this->session->userdata['user']['id'];

		$data['dashboard'] = false;

		$data['searchedOrFiltered'] = false;

		$data['vehicles_specific'] = $this->Model_vehicle_specific_name->getAll(); 

		$this->load->view('template',$data);		

		

	}





	

	private function validation()

	{

		    $errors = array();

			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('email', 'Email', 'required');

			//$this->form_validation->set_rules('title', 'Title', 'required');

			//$this->form_validation->set_rules('mobile', 'Mobile', 'required');

			$this->form_validation->set_rules('first_name', 'First Name', 'required');

			//$this->form_validation->set_rules('surname', 'Surname', 'required');

			$this->form_validation->set_rules('category_id', 'Category', 'required');

			$this->form_validation->set_rules('city_id', 'City', 'required');

			$this->form_validation->set_rules('branch_id', 'Branch', 'required');			

			//$this->form_validation->set_rules('preferred_date', 'Preferred Date', 'required');

			//$this->form_validation->set_rules('preferred_time', 'Preferred Time', 'required');

			

			if ($this->form_validation->run() == FALSE)

			{

				$errors['error'] = validation_errors();

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{

				return true;

			}

			

	}





	private function save($post_data)

	{

		$data = array();

		$update_with_email =  array();

		

		$update_with_email['email'] = $post_data['email'];

		$data['new_lead_with_same_email'] = 0;

		

		$this->updateIfLeadHasPreviousQuery($data,$update_with_email); // if already exist lead with same email we should need to update all recodes to zero and one for new query

		

		$old_assign = 0;

		$searched_lead_id = 0;

		if(isset($post_data['old_assign']))

			$old_assign = $post_data['old_assign'];

		if(isset($post_data['searched_lead_id']))

			$searched_lead_id = $post_data['searched_lead_id'];

		foreach($post_data as $key => $value)

		{

			if($key != 'form_type' && $key != 'old_assign' && $key != 'searched_lead_id')

			{

			 $data[$key] = $value;	

			}

		}

		

		if(!empty($data['vehicle'])){

			$data['vehicle'] = implode(',', $data['vehicle']);

		  }else{

			$data['vehicle'] = '';

		  }

		  

		if(!empty($data['vehicle_specific_names_id'])){

			$data['vehicle_specific_names_id'] = implode(',', $data['vehicle_specific_names_id']);

		  }else{

			$data['vehicle_specific_names_id'] = '';

		  }

		//$data['vehicle'] = implode(',', $data['vehicle']);

		



		$data['created_at'] = date('Y-m-d H:i:s');

		$data['new_lead_with_same_email'] = 1;

		$data['created_by'] = $this->session->userdata['user']['id']; //this will be 0 for leads created by cronjob email parsing.

		$data['orignal_created_by'] = $this->session->userdata['user']['id']; //this is a backup column, because the created by column can contain the latest manager id so the original creator id will remain here for future use 

		$data['trackid'] = generatTrackId($post_data['category_id']);



		$data['due_date'] = calculateLeadDeadline($data['created_at'], $post_data['category_id']);

		

				

		if($data['assign_to']!="")

		{

			$data['assign_by']  = $this->session->userdata['user']['id'];

			$data['assigned_at'] = date('Y-m-d H:i:s');	

			$data['status'] = 'Assigned';

		}



		if(isset($data['is_short_frm_lead']) && (int)$data['is_short_frm_lead'] === 1)

		{

			$data['assign_by']  = $this->session->userdata['user']['id'];

			$data['assigned_at'] = date('Y-m-d H:i:s');	

			$data['status'] = 'Assigned';

			$data['assign_to'] = $this->session->userdata['user']['id'];



		}

		

		if(isset($data['is_preowned_frm_lead']) && (int)$data['is_preowned_frm_lead'] === 1)

		{

			$data['assign_by']  = $this->session->userdata['user']['id'];

			$data['assigned_at'] = date('Y-m-d H:i:s');	

			$data['status'] = 'Assigned';

			$data['assign_to'] = $this->session->userdata['user']['id'];



		}



		$insert_lead_id = $this->Model_lead->save($data);



		//send email to the previous assigned person to aknowledegment.

		if($old_assign && $searched_lead_id){

			//$assignedEmail = $this->Model_lead->getAssignedPerson($old_assign);

			$old_assign_obj_sc = $this->Model_user->get($old_assign);	

			$logged_in_user_obj_sc = $this->Model_user->get($this->session->userdata['user']['id']);

			$lead_obj_sc = $this->Model_lead->get($searched_lead_id);			

			$emailData['first_name'] = $old_assign_obj_sc->full_name;

			$emailData['creator_name'] = $logged_in_user_obj_sc->full_name;	

			$emailData['trackid'] = $lead_obj_sc->trackid;

			sendEmail(10,$old_assign_obj_sc->email,$emailData);

			

			//add bell notification

			$dataNotificationCrAg = array();

			$dataNotificationCrAg['assignee_notification_read'] = 1;

			$dataNotificationCrAg['created_at'] = date('Y-m-d H:i:s');

			$dataNotificationCrAg['created_by'] = 0; //its a system generated msg

			$dataNotificationCrAg['lead_id'] = $searched_lead_id;

			$dataNotificationCrAg['comments'] = "A new lead (".$data['trackid'].") is created of this same lead";		

			$this->Model_notification->save($dataNotificationCrAg);

			//====

			

		}

		//Send Email To Customer For New Lead Created

		$cat_row = $this->Model_category->get($post_data['category_id'],true);

		$data['category_title'] = $cat_row['title'];



		if($post_data['category_id']==='1')

		{

			//only for test drive send this email.

			$emailData = $data;

			$emailData['create_action_date'] = $post_data['preferred_date'];

			$emailData['create_action_time'] = $post_data['preferred_time'];						

			$emailData['showroom'] = getBranchName($post_data['branch_id']);

			$emailData['showroom_ar'] = getBranchNameAr($post_data['branch_id']);

			$vehicles = "";

			if(isset($post_data['vehicle_specific_names_id']) && is_array($post_data['vehicle_specific_names_id']) && count($post_data['vehicle_specific_names_id'])>0)

			{

				$vehicle_specific_names_id_arr = $post_data['vehicle_specific_names_id'];

				foreach($vehicle_specific_names_id_arr as $vsi)

				{

					if($vehicles!="") $vehicles .= ", ";

					$vehicles .= getSpecificVehicleNameById($vsi);

				}

			}

			$emailData['vehicles'] = $vehicles;



			sendEmail('1',$post_data['email'],$emailData); //db id 1 is for new lead email to customer.

		}

		

		$this->setNotification('New lead created '. $data['trackid'], $insert_lead_id); 



		if($data['assign_to']!="")

		{

			//set notification of assign a lead too

			$this->setNotification('Lead Assigned', $insert_lead_id);



			//add a message that lead is assigned in the action log box

			$dataM['lead_id'] = $insert_lead_id;

			$dataM['message_id'] = 19;		

			$dataM['comments'] = "Lead is assigned to ".getEmployeeName($data['assign_to']);

			$dataM['created_at'] = date('Y-m-d H:i:s');

			$dataM['created_by'] = $this->session->userdata['user']['id'];

			$this->Model_leads_messages->save($dataM);

			//==========

		}





		return $data['trackid']."||".$insert_lead_id;

		

	}



	

	private function update($post_data)

	{

		$data = array();



		//====

		$update_with_email =  array();		

		$update_with_email['email'] = $post_data['email'];		



		$lead_id = $post_data['id'];

		$leadDataB4Update = $this->Model_lead->get($lead_id);

		$leadEmailB4Update = $leadDataB4Update->email;

		$leadCatB4Update = $leadDataB4Update->category_id;

		$leadTrackIdB4Update = $leadDataB4Update->trackid;

		$leadCreatedAt = $leadDataB4Update->created_at;



		//check if email is changed

		if($post_data['email'] != $leadEmailB4Update)

		{

			// if there are already leads with new email address then update all those records to zero

			$data['new_lead_with_same_email'] = 0;

			$this->updateIfLeadHasPreviousQuery($data,$update_with_email);



			//$data['new_lead_with_same_email'] = 1; //this updated will become on top and others with same email will become as nested.



			//if this was a parent then latest of its children will become parent, as this updating lead's email is changed

			if($leadDataB4Update->new_lead_with_same_email==1)

			{

				$this->Model_lead->updateLatestLeadByEmail($leadEmailB4Update); //update new_lead_with_same_email=1 to the latest lead, because this lead is moving to other leads by email 

			}

		}



		//check if category is changed

		if($post_data['category_id'] != $leadCatB4Update)

		{

			//update trackid alpha values only

			$data['trackid'] = updateTrackId($post_data['category_id'],$leadTrackIdB4Update);



			//update due date also

			$data['due_date'] = calculateLeadDeadline($leadCreatedAt, $post_data['category_id']);

		}



				

		foreach($post_data as $key => $value)

		{

			if($key != 'form_type' && $key != 'old_assign' && $key != 'searched_lead_id')

			{

			 $data[$key] = $value;	

			}

		}

		

		$data['vehicle'] = "";

		if(isset($post_data['vehicle']))

		$data['vehicle'] = implode(',', $post_data['vehicle']);



		$data['vehicle_specific_names_id'] = "";

	    if(isset($post_data['vehicle_specific_names_id']))

		$data['vehicle_specific_names_id'] = implode(',', $post_data['vehicle_specific_names_id']);



		$data['updated_at'] = date('Y-m-d H:i:s');

		$data['updated_by'] = $this->session->userdata['user']['id']; //this will be 0 for leads created by "cronjob email parsing".

		$where['id'] = $post_data['id'];				

		/*if($leadDataB4Update->trackid == 'N/A') //not a case anymore

		{

			$data['trackid'] = generatTrackId($post_data['category_id']);

		}*/

		$insert_lead_id = $this->Model_lead->update($data,$where);



		$this->setNotification('Lead updated', $post_data['id']); 



		return $lead_id;

		

	}

	

	private function updateIfLeadHasPreviousQuery($data,$update_with_email)

	{

		return $this->Model_lead->update($data,$update_with_email);

	}

	

	

	public function branchesByCity()

	{



		branchesByCity(true, $this->input->post('city_id'),0); //from custom helper

		

	}



	/*public function usersByCityAndBranchAjax()

	{



		//$this->input->post('city_id')

		//$this->input->post('branch_id')



		$options = "";



		$dataUsers['city_id'] = $this->input->post('city_id');

		$dataUsers['branch_id'] = $this->input->post('branch_id');

		$users = $this->Model_user->getMultipleRows($dataUsers,false,'asc');		

		

		echo '<option value= "">Please Select</option>';



		if(is_array($users))

		{

			foreach($users as $user){

				

				$selected = "";

				//if($branch_id){ if($branch_id==$branch->id) $selected = 'selected=selected'; } 

				$options .= '<option '.$selected.' value="'.$user->id.'">'.$user->full_name.'</option>';

			}

		}



		echo $options;

		exit();

		

	}*/



	//its basically now from org strucure and NOT from city and branch

	public function usersByCityAndBranchAjax()

	{



		$options = "";

		$users = array();

	

		$getUserIdIfAssignToAll = $this->Model_user->getUserIdIfSection($this->session->userdata['user']['id'],34); // 34 is the "Assign to all" section

		$getUserIdIfLeadControl = $this->Model_user->getUserIdIfSection($this->session->userdata['user']['id'],33); //33 is the "Lead Control" section

		$loggedInUserManagingDeps = getLoggedInUserManagingDeps();		

		

		if($getUserIdIfAssignToAll)

		{

			//this logged in user has the role/section of "Assign to All";



			$users = $this->Model_user->getAllUsersExcMe($this->session->userdata['user']['id']);



		}

		elseif(is_array($loggedInUserManagingDeps) && count($loggedInUserManagingDeps)>0)

		{

			//logged in user is a manager of one or more deps



			$users = $this->usersUnderMngRecursive(); //comma sep list of users under this manager.

			$users = $this->Model_user->getUsersIn(explode(",",$users)); //array

		}

		else

		{

			//don't have the "assign all users" role and also not a manager 

			//it can be a simple user who can assign lead to its manager or to "lead control role" within same deps in which he belongs to. Can be multiple deps.



			$wdu = array();

			$wdu['user_id'] = $this->session->userdata['user']['id'];

			$thisUserDeps = $this->Model_struc_dep_users->getMultipleRows($wdu);

			if($thisUserDeps)

			{

				$userManagerIds = array();

				foreach($thisUserDeps as $tud)

				{

					$whereSd = array();

					$whereSd['id'] = $tud->struc_dep_id;

					$deps = $this->Model_structure_departments->getMultipleRows($whereSd); //all dep results, the user belongs to as an employee in structure.

					if($deps)

					foreach($deps as $ud)

					{

						$userManagerIds[] = $ud->manager_id;



						//also check if this dep has one or more "lead control" role user?

						$whereOu = array();

						$whereOu['struc_dep_id'] = $ud->id;

						$oUsers = $this->Model_struc_dep_users->getMultipleRows($whereOu);

						

						if($oUsers)

						{

							//for now this loop will contain itself user too. but no worries.

							foreach($oUsers as $oU)

							{

								$getUserIdIfLeadControl = $this->Model_user->getUserIdIfSection($oU->user_id,33); //33 is the "Lead Control" section

								if($getUserIdIfLeadControl)

								{

									$userManagerIds[] = $getUserIdIfLeadControl[0]['id'];

								}



							}

						}



					}

				}



				$users = $this->Model_user->getUsersIn($userManagerIds); //array //this will return the users who are "managers of this user" or "lead control" of this user's dep. Can be multiple.

			}







		}

				

		

		echo '<option value= "">Please Select</option>';



		if(is_array($users))

		{

			foreach($users as $user){

				

				$selected = "";

				//if($branch_id){ if($branch_id==$branch->id) $selected = 'selected=selected'; } 

				$options .= '<option '.$selected.' value="'.$user->id.'">'.$user->full_name.'</option>';

			}

		}



		echo $options;

		exit();

		

	}



	public function checkRemainTime()

	{

	

		$allRelatedLeads = getAllRelatedLeads();

		$leads30Min = "";

		$remindLeadActionId = "";

		$remindLeadId = "";

		$remindLeadTrackId = "";

		if($allRelatedLeads)

		foreach($allRelatedLeads as $relatedLead)

		{

			$actionIds = checkRemainTimeForAction($relatedLead->id); //this will get all the messages that are 30 min for this lead, comma sep

			if($actionIds)

			{

				$actionIdsArr = explode(",",$actionIds);

				foreach($actionIdsArr as $actionId)

				{

					if($remindLeadActionId !="") $remindLeadActionId.= ",";

					$remindLeadActionId .= $actionId;



					if($remindLeadId !="") $remindLeadId.= ",";

					$remindLeadId .= $relatedLead->id;



					if($remindLeadTrackId !="") $remindLeadTrackId.= ",";

					$remindLeadTrackId .= $relatedLead->trackid;



				}





			}

		}



		$remindLead['remindLeadActionId'] = $remindLeadActionId;

		$remindLead['remindLeadId'] = $remindLeadId;

		$remindLead['remindLeadTrackId'] = $remindLeadTrackId;

		echo json_encode($remindLead);

		exit;

	}



	public function generateReminderNotification()

	{

		$lead_id = $this->input->post('lead_id');

		$leadRec = $this->Model_lead->get($lead_id);

		$dataNotification = array();



		//set notification



		//reminder popup appears for both, creator and assignee so notification is also for both.

		if($leadRec->created_by === $this->session->userdata['user']['id'] && $leadRec->assign_to === $this->session->userdata['user']['id'] && false)

		{

			//never comes here check above false

			//do nothing.

		}

		else

		{

			//this is self notification

			if($leadRec->created_by === $this->session->userdata['user']['id'])

				$dataNotification['creater_notification_read'] = 1;

			if($leadRec->assign_to === $this->session->userdata['user']['id'])

				$dataNotification['assignee_notification_read'] = 1;

		}



		$comments = 'Upcoming scheduled action for lead '.$leadRec->trackid.'';

		$dataNotification['created_at'] = date('Y-m-d H:i:s');

		$dataNotification['created_by'] = $this->session->userdata['user']['id'];	

		$dataNotification['lead_id'] = $lead_id;

		$dataNotification['comments'] = $comments;		

		$this->Model_notification->save($dataNotification);

	}







	public function ajaxAction()

	{



		//====set variables===

		$logged_in_user_id = $this->session->userdata['user']['id'];

		$lead_id = $this->input->post('lead_id');

		$lead = $this->Model_lead->get($lead_id);

		//echo "<pre>"; print_r($lead); exit;

		$dashboard = $this->input->post('dashboard');

		

		//check if more then one car slct then display as count else just one.

		$slctCarCount = $this->input->post('slctCarCount');

		if($slctCarCount > 0){

			$howManyLatest = $slctCarCount;

		}else{

			$howManyLatest = 1;

		}

		//====================



		$messages = getLeadMessages($lead_id,$howManyLatest);



		if($messages)

		{		

			displayMessages($messages, $logged_in_user_id, $lead, $dashboard, $howManyLatest);

		}



		exit();

	}





	public function ajaxSelfData()

	{



		$leadid = $this->input->post('lead_id');

		$i = $this->input->post('counter');

		$logged_in_user_id = $this->input->post('logged_in_user_id');

		$dashboard = $this->input->post('dashboard');

		//if($dashboard) $dashboard = true; else $dashboard = false;

		$isSingleDetailLead = $this->input->post('is_single');

		//if($isSingleDetailLead) $isSingleDetailLead = true; else $isSingleDetailLead = false;

		$is_sublead = $this->input->post('is_sublead');

		//if($is_sublead) $is_sublead = true; else $is_sublead = false;



		//echo "(".$is_sublead.')'; exit();



		$lead = $this->Model_lead->get($leadid);

		$vehicles_specific = $this->Model_vehicle_specific_name->getAll(); 

		$tags = $this->Model_tag->getAll();



		expandedLead($lead,$i,$logged_in_user_id, $dashboard, $isSingleDetailLead,$vehicles_specific,$is_sublead,$tags);		

		exit();

	}



	public function leadNotificationsAjaxCheck()

	{



		$lead_ids_comma = "";

		$allRelatedLeads = getAllRelatedLeads();



		foreach($allRelatedLeads as $relatedLead)

		{

			if(checkLeadHasUnreadNotifications($relatedLead->id))

			{

				if($lead_ids_comma!="") $lead_ids_comma .= ",";

				$lead_ids_comma .= $relatedLead->id;



			}

		}



		echo $lead_ids_comma;

		exit;





	}



	public function getNotificationsForHeaderAjax()

	{

		$notificationLeads = $this->Model_lead->getLeadsWithNotifications("header");



		if($notificationLeads) foreach( $notificationLeads as $notificationLead )

		{

			?>

			<tr class="hnt<?php echo $notificationLead->lead_id ?>">

			<td><a href="javascript:void(0);" onclick="notificationRead('<?php echo $notificationLead->id;  ?>', '<?php echo $notificationLead->lead_id ?>');"><?php echo $notificationLead->trackid; ?></a></td>

			<td><a href="javascript:void(0);" onclick="notificationRead('<?php echo $notificationLead->id;  ?>', '<?php echo $notificationLead->lead_id ?>');">

				<?php echo $notificationLead->comments ?>

				<br>

				<?php echo date("d F Y H:i a", strtotime($notificationLead->created_at)); ?>

			</a>

			</td>

			</tr>

			<?php

		}



		exit();

	}



	//same copy in Dashboard Controller

	private function getStrucChildren($id) 

	{



		$where['parent_id'] = $id;

		$deps = $this->Model_structure_departments->getMultipleRows($where);



		$children = array();

		

		if($deps) {

			# It has children, let's get them.

			//while($row = mysql_fetch_array($r)) {

			foreach($deps as $dep) {

				# Add the child to the list of children, and get its subchildren

				$this->allChild[] = $dep->id;

				$children[$dep->id] = $this->getStrucChildren($dep->id);				

			}

		}



		return $children;

	}



	public function getScheduledMessage()

	{



		//get message record

		$messageRec = $this->Model_leads_messages->get($this->input->post('id'));

		$messageRec = (array) $messageRec;



		//=========

		$messageRec['schedule_date'] = "";

		$messageRec['create_action_date'] = "";

		$messageRec['schedule_time'] = "";

		$messageRec['create_action_time'] = "";



		$schedule_date_time = $messageRec['schedule_date_time'];

		$schedule_date_time_arr = explode(" ",$schedule_date_time);

		if(count($schedule_date_time_arr)===2)

		{

			$messageRec['schedule_date'] = date("j F Y",strtotime($schedule_date_time_arr[0]));

			$messageRec['create_action_date'] = $schedule_date_time_arr[0];

			$messageRec['schedule_time'] = date("h:i a",strtotime($schedule_date_time_arr[1]));

			$messageRec['create_action_time'] = $schedule_date_time_arr[1];

		}

		//==========



		$messageRec['vehicles'] = "";



		$vehicle_specific_names_id = $messageRec['vehicle_specific_names_id'];

		$vehicle_specific_names_id_arr = explode(",",$vehicle_specific_names_id);

		foreach($vehicle_specific_names_id_arr as $vsi)

		{			

			$messageRec['vehicles'] .= '<div id="'.$vsi.'" data-id="'.$vsi.'" class="addCarUHav specific_car">'.getSpecificVehicleNameById($vsi).'<a href="javascript:void(0);">X</a><input type="hidden" name="vehicle_specific_names_id[]" value="'.$vsi.'"></div>';

		}		



		echo json_encode($messageRec);

		exit();

	}



	public function getCallScheduledMessage()

	{



		//get message record

		$messageRec = $this->Model_leads_messages->get($this->input->post('id'));

		$messageRec = (array) $messageRec;



		//=========

		$messageRec['schedule_date'] = "";

		$messageRec['create_action_date'] = "";

		$messageRec['schedule_time'] = "";

		$messageRec['create_action_time'] = "";



		$schedule_date_time = $messageRec['schedule_date_time'];

		$schedule_date_time_arr = explode(" ",$schedule_date_time);

		if(count($schedule_date_time_arr)===2)

		{

			$messageRec['schedule_date'] = date("j F Y",strtotime($schedule_date_time_arr[0]));

			$messageRec['create_action_date'] = $schedule_date_time_arr[0];

			$messageRec['schedule_time'] = date("h:i a",strtotime($schedule_date_time_arr[1]));

			$messageRec['create_action_time'] = $schedule_date_time_arr[1];

		}

		//==========			



		echo json_encode($messageRec);

		exit();

	}



	public function checkIfCustomerInteracted(){

		echo $this->Model_lead->checkIfCustomerInteracted($this->input->post('lead_id'));

		exit();

	}

	

	public function viewLeadStatus(){

	

			$viewLeadStatus = $this->input->post('viewLeadStatus');

		

			//$this->load->library('session');

			$this->session->set_userdata('viewLeadStatus',$viewLeadStatus);

			echo json_encode($this->session->userdata['viewLeadStatus']);

			exit;

	

	}

	

}

?>