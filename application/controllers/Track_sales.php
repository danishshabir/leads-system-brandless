<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Track_sales extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 private $allChild = array();
	 private $not_a_lead_type = 0;

	  public function __construct()
    {
        parent::__construct();
		checkAdminSession();
      	//$this->load->model('Model_track_sales');
		$this->load->model('Model_common'); //use this in every controller because it is accessed in header.
		$this->load->model('Model_track_sales');
		
    }
	

	public function index($applyFilter="", $category_id=0, $status="")
	{ 
		$data = array();	
		
		$sold_year = (isset($_GET['sold_year']))? $_GET['sold_year']:0;
		$sold_month = (isset($_GET['sold_month']))? $_GET['sold_month']:0;
		$data['sales'] = $this->Model_track_sales->getSales($sold_year,$sold_month);
		
		$data['view'] = 'track_sales/track_sales';		
        //$data['track_sales_class'] = 'active'; //for left menu to make it active
			
		$this->load->view('template',$data);		
		
	}
}
?>