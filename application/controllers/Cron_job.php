<?php
defined('BASEPATH') OR exit('No direct script access allowed');
set_time_limit(0);



class Cron_job extends CI_Controller {

public $checkSub = array();
	 
	 public function __construct()
    {
        parent::__construct();
		$this->load->model('Model_lead');
		$this->load->model('Model_user');
        $this->load->model('Model_leads_messages');
		$this->load->model('Model_messages');
		$this->load->model('Model_category');
		$this->load->model('Model_notification');
		$this->load->model('Model_struc_dep_users');		
		$this->load->model('Model_structure_departments');		
		$this->load->model('Model_lead_tags');
		$this->load->model('Model_appointment');
		$this->load->model('Model_common'); //use this in every controller because it is accessed in header.		

		//fow now not using this array but individual checks are added below
		$this->checkSub[] = 'Book a Test Drive request from the Leads System website'; //http://mercedesbenzme.com/ksa/en/footer/contact-sales/book-a-test-drive
		$this->checkSub[] = 'Enquiry from the Leads System website'; //http://mercedesbenzme.com/ksa/en/footer/contact-sales/contact-us
		$this->checkSub[] = 'Call back request from the Leads System website'; //http://mercedesbenzme.com/ksa/en/footer/contact-sales/contact-us/request-a-call-back
	    $this->checkSub[] = 'Leads System Enquiry Received';//http://mercedesbenzksa.com/
		$this->checkSub[] = 'Leads System Test Drive Request Received';//http://mercedesbenzksa.com/experience/
    }
	
	public function index()
	{
		
		exit();
		
		require(APPPATH.'libraries/rfc822_addresses.php');
	    require(APPPATH.'libraries/mime_parser.php');
	    require(APPPATH.'libraries/email_parser.php');
	    require(APPPATH.'libraries/pop3.php');
		
		if ($this->config->item('pop3_job_wait'))
		{
	
	
			// A log file used to store start of POP3 fetching  		
			$job_file = FCPATH.$this->config->item('attach_dir') . '/__pop3-' . sha1(__FILE__) . '.txt';

			// If the job file already exists, wait for the previous job to complete unless expired
			if ( file_exists($job_file ) )
			{
				// Get time when the active POP3 fetching started
				$last = intval( file_get_contents($job_file) );

				// Give a running process at least 31 minutes to finish
				if ( $last + $this->config->item('pop3_job_wait') * 60 > time() )
				{
					$message = $this->config->item('debug_mode') ? "Another POP3 fetching task is still in progress." : '';
					die($message);
				}
				else
				{
					// Start the process (force)
					file_put_contents($job_file, time() );
				}

			}
			else
			{
				// No job in progress, log when this one started
				file_put_contents($job_file, time() );
			}
		}



		// If a pop3 wrapper is registered un register it, we need our custom wrapper
		if ( in_array('pop3', stream_get_wrappers() ) )
		{
			stream_wrapper_unregister('pop3');
		}

		// Register the pop3 stream handler class
		stream_wrapper_register('pop3', 'pop3_stream');

		// Setup required variables
		$pop3 = new pop3_class;
		$pop3->hostname	= $this->config->item('pop3_host_name');
		$pop3->port		= $this->config->item('pop3_host_port');
		$pop3->tls		= $this->config->item('pop3_tls');
		$pop3->debug	= 0;
		$pop3->join_continuation_header_lines = 1;

		// Connect to POP3
		if(($error=$pop3->Open())=="")
		{
			echo $this->config->item('debug_mode') ? "<pre>Connected to the POP3 server &quot;" . $pop3->hostname . "&quot;.</pre>\n" : '';

			// Authenticate
			if(($error=$pop3->Login($this->config->item('pop3_user'), $this->mb_htmlspecialchars_decode($this->config->item('pop3_password'))))=="")
			{
				echo $this->config->item('debug_mode') ? "<pre>User &quot;" . $this->config->item('pop3_user') . "&quot; logged in.</pre>\n" : '';

				// Get number of messages and total size
				if(($error=$pop3->Statistics($messages,$size))=="")
				{
					echo $this->config->item('debug_mode') ? "<pre>There are $messages messages in the mail box with a total of $size bytes.</pre>\n" : '';

					// If we have any messages, process them
					if($messages>0)
					{
						// Connect to the database
						//mb_dbConnect();

						for ($message = 1; $message <= $messages; $message++)
						{
							echo $this->config->item('debug_mode') ? "<pre>Parsing message $message of $messages.</pre>\n" : '';

							$pop3->GetConnectionName($connection_name);
							$message_file = 'pop3://'.$connection_name.'/'.$message;

							// Parse the incoming email
							$results = parser($message_file);							
							
							$set_priority = 0 ;// use if we need to set priority
							 
							// Convert email into a ticket (or new reply)
							if ( $id = $this->mb_email2ticket($results, 1, $set_priority) )
							{
								echo $this->config->item('debug_mode') ? "<pre>Ticket $id created/updated.</pre>\n" : '';

							}
							else
							{
								echo $this->config->item('debug_mode') ? "<pre>Ticket NOT inserted - may be duplicate, blocked or an error.</pre>\n" : '';
							}

							// Queue message to be deleted on connection close
							if ( ! $this->config->item('pop3_keep'))
							{
								$pop3->DeleteMessage($message);
							}

							echo $this->config->item('debug_mode') ? "<br /><br />\n\n" : '';

							
						}
					}

					// Disconnect from the server - this also deletes queued messages
					if($error == "" && ($error=$pop3->Close()) == "")
					{
						echo $this->config->item('debug_mode') ? "<pre>Disconnected from the POP3 server &quot;" . $pop3->hostname . "&quot;.</pre>\n" : '';
					}
				}
			}
		}

		// Any error messages?
		if($error != '')
		{
			echo "<h2>Error: " . $this->mb_htmlspecialchars($error) . "</h2>";
		}

		// Remove active POP3 fetching log file
		if ($this->config->item('pop3_job_wait'))
		{
			unlink($job_file);
		}

		return NULL;
		
	}
	
	
public function mb_email2ticket($result, $pop3 = 0, $set_priority = -1)
{	

	$tmpdir = $result['tempdir'];
	$emailBody = "";
	$isReply = false;

	$parsedName = "";
	$parsedEmail = "";
	$parsedPhone = "";
	$parsedWhenNextCar = "";
	$parsedVehicles = "";
	$parsedLocation = "";
	$parsedComments = "";
	$parsedModeOfContact = "";
	$parsedPrefTimeToCall = "";



		//echo $this->checkSub;
		if($result['subject']==="Book a Test Drive request from the Leads System website" || $result['subject']==="Enquiry from the Leads System website" || $result['subject']==="Call back request from the Leads System website" || $result['subject'] === "Leads System Enquiry Received" || $result['subject'] === "Leads System Test Drive Request Received")
		{

			$dom = new DOMDocument;
			//@$dom->loadHTML($result['message']);
			@$dom->loadHTML(mb_convert_encoding($result['message'], 'HTML-ENTITIES', 'UTF-8'));
			$tds = $dom->getElementsByTagName('td');
			$parsedI = 0;

			//check test drive lengthy form
			//echo $tds->item(31)->nodeValue;
			$testDriveFrefDateFrm = false;			
			if($tds->item(31)!=null && strpos($tds->item(31)->nodeValue, "Preferred date:") !== false)
				$testDriveFrefDateFrm = true;

			//echo $tds->item(13)->nodeValue;
			//exit();
			
			$callBackLessHeaderTdsFrm = false;
			if($tds->item(13)!=null && strpos($tds->item(13)->nodeValue, "Name:") !== false)
				$callBackLessHeaderTdsFrm = true;
				

			foreach ($tds as $td) {
			
			//echo $parsedI.'-'.$td->nodeValue.'<br>';			

					if($result['subject']==="Book a Test Drive request from the Leads System website")
					{
				
						//We noticed that from unknown location :) these two fileds also comes in inbox, 
						//Preferred date and Preferred time:
						if($testDriveFrefDateFrm)
						{							
							if($parsedI==16)
								$parsedName = trim($td->nodeValue);
							elseif($parsedI==18)
								$parsedEmail = trim($td->nodeValue);
							elseif($parsedI==22)
								$parsedPhone = trim($td->nodeValue);							
							elseif($parsedI==26)
								$parsedVehicles = trim($td->nodeValue);
							elseif($parsedI==30)
								$parsedLocation = trim($td->nodeValue); //Jeddah - Madinah Rd.
							elseif($parsedI==36)
								$parsedComments = trim($td->nodeValue);
						}
						else
						{
							if($parsedI==16)
								$parsedName = trim($td->nodeValue);
							elseif($parsedI==18)
								$parsedEmail = trim($td->nodeValue);
							elseif($parsedI==20)
								$parsedPhone = trim($td->nodeValue);
							elseif($parsedI==22)
								$parsedWhenNextCar = trim($td->nodeValue);
							elseif($parsedI==24)
								$parsedVehicles = trim($td->nodeValue);
							elseif($parsedI==28)
								$parsedLocation = trim($td->nodeValue); //Jeddah - Madinah Rd.
							elseif($parsedI==30)
								$parsedComments = trim($td->nodeValue);
						}
					}

					if($result['subject']==="Enquiry from the Leads System website")
					{
						if($parsedI==16)
							$parsedName = trim($td->nodeValue);
						elseif($parsedI==18)
							$parsedEmail = trim($td->nodeValue);
						elseif($parsedI==20)
							$parsedPhone = trim($td->nodeValue);
						elseif($parsedI==22)
							$parsedModeOfContact = trim($td->nodeValue);
						elseif($parsedI==24)
							$parsedComments = trim($td->nodeValue);						
					}

					if($result['subject']==="Call back request from the Leads System website")
					{
						//Note: We noticed that from a webform that is unknown location, :) we don't know which form is this, does not contain location field.
						
						
						if(strpos($result['message'], "Preferred showroom's location:") !== false)
						{							

							if($parsedI==16)
								$parsedName = trim($td->nodeValue);
							elseif($parsedI==18)
								$parsedEmail = trim($td->nodeValue);
							elseif($parsedI==20)
								$parsedPhone = trim($td->nodeValue);
							elseif($parsedI==22)
								$parsedPrefTimeToCall = trim($td->nodeValue);
							elseif($parsedI==26)
								$parsedVehicles = trim($td->nodeValue);
							elseif($parsedI==28)
								$parsedLocation = trim($td->nodeValue); //Jeddah - Madinah Rd.
							elseif($parsedI==30)
								$parsedComments = trim($td->nodeValue);
						}
						elseif($callBackLessHeaderTdsFrm)
						{

							if($parsedI==14)
								$parsedName = trim($td->nodeValue);
							elseif($parsedI==16)
								$parsedEmail = trim($td->nodeValue);
							elseif($parsedI==18)
								$parsedPhone = trim($td->nodeValue);
							elseif($parsedI==20)
								$parsedPrefTimeToCall = trim($td->nodeValue);
							elseif($parsedI==24)
								$parsedVehicles = trim($td->nodeValue);
							elseif($parsedI==26)
								$parsedLocation = trim($td->nodeValue); //Jeddah - Madinah Rd.
							elseif($parsedI==28)
								$parsedComments = trim($td->nodeValue);
						}
						else
						{

							if($parsedI==14)
								$parsedName = trim($td->nodeValue);
							elseif($parsedI==16)
								$parsedEmail = trim($td->nodeValue);
							elseif($parsedI==18)
								$parsedPhone = trim($td->nodeValue);
							elseif($parsedI==20)
								$parsedPrefTimeToCall = trim($td->nodeValue);
							elseif($parsedI==24)
								$parsedVehicles = trim($td->nodeValue);						
							elseif($parsedI==26)
								$parsedComments = trim($td->nodeValue);
						}

					}
					if($result['subject'] === "Leads System Enquiry Received")
					{
						if($parsedI==1)
								$parsedName = trim($td->nodeValue);
							elseif($parsedI==5)
								$parsedEmail = trim($td->nodeValue);
							elseif($parsedI==3)
								$parsedPhone = trim($td->nodeValue);
							elseif($parsedI==7)
								$parsedComments = trim($td->nodeValue);
					}
					if($result['subject'] === "Leads System Test Drive Request Received")
					{ 
							if($parsedI==1)
								$parsedName = trim($td->nodeValue);								
							elseif($parsedI==3)
								$surName = trim($td->nodeValue);
							elseif($parsedI==5)
								$parsedPhone = trim($td->nodeValue);
							elseif($parsedI==7)
								$parsedEmail = trim($td->nodeValue);
							elseif($parsedI==9)
								$parsedLocation = trim($td->nodeValue);
							elseif($parsedI==11)
								$parsedLocation .= " - ".trim($td->nodeValue); //concatinate like //Jeddah - Madinah Rd. but its in Arabic.
							elseif($parsedI==13)
								$parsedVehicles = trim($td->nodeValue);
									
					}	
					
					$parsedI++;
			}			
			
	//exit();

		}
		
		// don't create such leads in which name contains email address. 
		if(strpos($parsedName, '@')!==false)
		{
			return false;
		}

	$resultSave = array();

	// OK, everything seems OK. Now determine if this is a reply to a ticket or a new ticket
	if ( strpos($result['subject'], '[#')!==false )
	{		

		// We found a possible tracking ID

		$temp_trackid = $result['subject'];
		$sp_ = strpos($temp_trackid, '[#');
		$ep_ = strpos($temp_trackid, ']');
		$temp_trackid = substr($temp_trackid,($sp_+2),(($ep_-$sp_)-2));

		$result['trackid'] = $temp_trackid;

		// get record form database
		$trackToId = ltrim($result['trackid'], '0');
		$arr_fetch['id'] = $trackToId; // for fetching data we have to make array as  function getWithMultipleFields() gets array which matches the condiion
		$res = $this->Model_lead->getWithMultipleFields($arr_fetch);

		if (!empty($res))
		{
			// its a Reply to old Ticket.

			$ticket = (array)$res;

			$arr_update = array();
			$arr_update_by = array();

	        // Is this ticket was Archived? Push back to Finished Status so it can be displayed again.
			if($ticket['status']=='Approved and Archived')
			{
				$arr_update['status'] = "Finished";

				//add notification
				$dataLead = array();
				$dataNotification = array();
				$dataNotification['assignee_notification_read'] = 1;			
				$dataNotification['creater_notification_read'] = 1;

				$dataNotification['created_at'] = date('Y-m-d H:i:s');
				$dataNotification['created_by'] = 0;	
				$dataNotification['lead_id'] = $ticket['id'];
				$dataNotification['comments'] = "Customer replied to an archived lead via email. Lead status is now set to Finished from Archived";
				$this->Model_notification->save($dataNotification);
				//====
			}

			
			//update all previous leads to zero
			$arr_update_email['new_lead_with_same_email'] = 0;
			$arr_update_email_by['email'] = $ticket['email'];			
			$this->Model_lead->update($arr_update_email,$arr_update_email_by);

			//update this lead to latest so it will be displayed in main listing intead of subleads in expanded view.
			$arr_update['reply'] = $ticket['reply'] + 1;
			$arr_update['new_lead_with_same_email'] = 1; 
			$arr_update_by['id'] = $ticket['id'];
			$this->Model_lead->update($arr_update,$arr_update_by);

			//add a new message as a forign key record of the main lead.
			$data = array();
			$data['lead_id'] = $ticket['id'];
			// Strip quoted reply from email
			$data['comments'] = $this->mb_stripQuotedText($result['message']);
			
			//11/09/2016 $data['comments'] = strip_tags(substr($data['comments'],0, strpos($data['comments'], '<div class="gmail_extra">') ) );	// get only replied message.
			$data['comments'] = nl2br($data['comments'],false);			
			$data['comments'] = strip_tags($data['comments'],"<br>");	// get only replied message.
			$data['comments'] = preg_replace('#<br>(\s*<br>)+#', '<br>', $data['comments']);

			
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['created_by'] = 0;
			$data['message_id'] = 15;
			$this->Model_leads_messages->save($data);			
			
			//add notification
			$dataLead = array();
			$dataNotification = array();
			$dataNotification['assignee_notification_read'] = 1;			
			$dataNotification['creater_notification_read'] = 1;
			$dataNotification['created_at'] = date('Y-m-d H:i:s');
			$dataNotification['created_by'] = 0;	
			$dataNotification['lead_id'] = $ticket['id'];
			$dataNotification['comments'] = "Customer replied via email";			
			$this->Model_notification->save($dataNotification);
			//====


			$isReply = true;

			// Delete the temporary files
			deleteAll($tmpdir);
			
			return $ticket['trackid']; 


	    }
	    else
	    { // its a new Ticket.

			$isReply = false;
			
	    }
	}
	
	if(!$isReply)
	{	//Insert New Ticket		

			$resultSave['created_at'] = date('Y-m-d H:i:s');
			$resultSave['created_by'] = 0; //cronjob
			$resultSave['new_lead_with_same_email'] = 1;
			$resultSave['trackid'] = generatTrackId(0);
			$resultSave['due_date'] = date('Y-m-d', strtotime('+2 day'));
			$resultSave['status'] = 'Not Assigned';			
							

			if($result['subject']==="Book a Test Drive request from the Leads System website")
			{
				$this->parsedNameSeparate($parsedName, $resultSave); //$resultSave is passed by reference
				$resultSave['email'] = $parsedEmail;
				$this->parsedPhoneFix($parsedPhone, $resultSave); //$resultSave is passed by reference
				$resultSave['expected_to_buy'] = $parsedWhenNextCar;
				$this->parsedVehiclesIds($parsedVehicles, $resultSave, "new_names"); //$resultSave is passed by reference
				$this->parsedCityAndLocation($parsedLocation, $resultSave); //$resultSave is passed by reference
				$resultSave['comments'] = $parsedComments;
				$resultSave['category_id'] = 1;
				$resultSave['trackid'] = generatTrackId(1);
				$resultSave['due_date'] = calculateLeadDeadline($resultSave['created_at'], $resultSave['category_id']);
				$resultSave['form_type'] = "Test Drive";
				
			}
			elseif($result['subject']==="Enquiry from the Leads System website")
			{

				$this->parsedNameSeparate($parsedName, $resultSave); //$resultSave is passed by reference
				$resultSave['email'] = $parsedEmail;
				$this->parsedPhoneFix($parsedPhone, $resultSave); //$resultSave is passed by reference
				if($parsedModeOfContact==="FEmail")
				$resultSave['preferred_mode_of_contact'] = "Email";
				elseif($parsedModeOfContact==="PhoneOrMobile")
				$resultSave['preferred_mode_of_contact'] = "Phone/Mobile";
				$resultSave['comments'] = $parsedComments;
				$resultSave['form_type'] = "Contact us";

			}
			elseif($result['subject']==="Call back request from the Leads System website")
			{
				$this->parsedNameSeparate($parsedName, $resultSave); //$resultSave is passed by reference
				$resultSave['email'] = $parsedEmail;
				$this->parsedPhoneFix($parsedPhone, $resultSave); //$resultSave is passed by reference

				$resultSave['prefered_time_to_call'] = $parsedPrefTimeToCall;
				$this->parsedVehiclesIds($parsedVehicles, $resultSave, "old_names"); //$resultSave is passed by reference
				$this->parsedCityAndLocation($parsedLocation, $resultSave); //$resultSave is passed by reference
				$resultSave['comments'] = $parsedComments;
				$resultSave['form_type'] = "Call Back";

			}
			elseif($result['subject']==="Leads System Enquiry Received")
			{
				$this->parsedNameSeparate($parsedName, $resultSave); //$resultSave is passed by reference
				$resultSave['email'] = $parsedEmail;
				$this->parsedPhoneFix($parsedPhone, $resultSave); //$resultSave is passed by reference

				//$resultSave['prefered_time_to_call'] = $parsedPrefTimeToCall;
				//$this->parsedVehiclesIds($parsedVehicles, $resultSave); //$resultSave is passed by reference
				//$this->parsedCityAndLocation($parsedLocation, $resultSave); //$resultSave is passed by reference
				$resultSave['comments'] = $parsedComments;
				$resultSave['form_type'] = "Contact us";

			}
			elseif($result['subject']==="Leads System Test Drive Request Received")
			{
				$resultSave['first_name'] = $parsedName; //$resultSave is passed by reference
				$resultSave['surname'] = $surName;
				$resultSave['email'] = $parsedEmail;
				$this->parsedPhoneFix($parsedPhone, $resultSave); //$resultSave is passed by reference
				$this->parsedVehiclesIds($parsedVehicles, $resultSave, "ksacom_vh_names"); //$resultSave is passed by reference
				$this->parsedCityAndLocation(trim($parsedLocation), $resultSave); //$resultSave is passed by reference
				//$resultSave['city_id'] = getCityIdByName($parsedLocation);
				//$resultSave['comments'] = $parsedComments;
				$resultSave['form_type'] = "Test Drive";

				$resultSave['category_id'] = 1;
				$resultSave['trackid'] = generatTrackId(1);
				$resultSave['due_date'] = calculateLeadDeadline($resultSave['created_at'], $resultSave['category_id']);



			}
			else
			{
				return false;
			}

			//===========
				//print_r($resultSave); exit();
				//when ever a new lead comes from any of the above 5 types 
				//the following email will help to have an eye that everything is working fine.
				$debug_mail_body = "";
				foreach($resultSave as $rfmkey=>$rfmvalue)
				$debug_mail_body .= $rfmkey." = (".$rfmvalue.")\n";
				//mail("waqas@astutesol.com","mb cron debug data",$debug_mail_body);			
			//===========			

		//check if same person is submitting duplicated lead within last 10 min then set the status of old (-10 min) lead to duplicated
			$arr_check_dup = array();
			$arr_check_dup['first_name'] = $resultSave['first_name'];
			if(isset($resultSave['surname'])) $arr_check_dup['surname'] = $resultSave['surname'];
			else $arr_check_dup['surname'] = "";
			$arr_check_dup['email'] = $resultSave['email'];
			$arr_check_dup['mobile'] = $resultSave['mobile'];
			if(isset($resultSave['branch_id'])) //for contact us type of leads the branch id is not available.
			$arr_check_dup['branch_id'] = $resultSave['branch_id'];
			$arr_check_dup['form_type'] = $resultSave['form_type'];
			$arr_check_dup['status !='] = "Duplicated"; 
			$arr_check_dup['created_at >='] = date('Y-m-d H:i:s', strtotime('-10 minutes')); //get same record within last 10 min.
			$duplicated_leads = $this->Model_lead->getMultipleRows($arr_check_dup); // same email exist 
			
			//now save the new lead
			$result['lead_id'] = $this->Model_lead->save($resultSave);

			//check if there were duplicated leads found then update them.
			if($duplicated_leads)
			{
				foreach($duplicated_leads as $duplicated_lead)
				{
					$dataD = array();
					$updateById = array();

					$dataD['status'] = "Duplicated";
					$dataD['duplicate_of'] = $result['lead_id'];
					$updateById['id'] = $duplicated_lead->id;
					$this->Model_lead->update($dataD,$updateById); //set previous lead to duplicated

					//add a new message as a forign key record of the main lead.
					$dataD = array();
					$dataD['lead_id'] = $duplicated_lead->id;
					$dataD['comments'] = "Duplicated Lead";
					$dataD['created_at'] = date('Y-m-d H:i:s');
					$dataD['created_by'] = 0;
					$dataD['message_id'] = 4;
					$this->Model_leads_messages->save($dataD);	

				}

				//echo $this->db->last_query(); exit();
			}
			//exit();
			//================

			
			//all madinah showroom selected email to be automatically assigned to Faisal Hadad f.hadad
			
			/*if($resultSave['branch_id']==="1" && ($result['subject']==="Book a Test Drive request from the Leads System website" || $result['subject'] === "Leads System Test Drive Request Received"))
			{
				
				$dataLead = array();
				$success_msg = "";
				
				$assign_to = 152; //136:faisalhaddad 152:Abshar
				$lead_id = $result['lead_id'];
												

				$dataLead['assign_to']  = $assign_to;
				$dataLead['assign_by']  = 0;
				$dataLead['assigned_at'] = date('Y-m-d H:i:s');	
				$dataLead['status'] = 'Assigned';
				$where['id'] = $lead_id;
				$this->Model_lead->update($dataLead,$where);

				//set notification
				//$this->setNotification('Lead Assigned', $lead_id);	
				
				//add a message that lead is assigned in the action log box
				$dataM['lead_id'] = $lead_id;
				$dataM['message_id'] = 19;		
				$dataM['comments'] = "Lead is assigned to ".getEmployeeName($assign_to);
				$dataM['created_at'] = date('Y-m-d H:i:s');
				$dataM['created_by'] = 0;
				$this->Model_leads_messages->save($dataM);
				//==========

			}*/
			

			//add notification
			/* 16 jan 2017
			$dataLead = array();
			$dataNotification = array();
			$dataNotification['inbox_notification_read'] = 1;

			$dataNotification['created_at'] = date('Y-m-d H:i:s');
			$dataNotification['created_by'] = 0;	
			$dataNotification['lead_id'] = $result['lead_id'];
			$dataNotification['comments'] = "Lead received from ".$resultSave['form_type'];
			$this->Model_notification->save($dataNotification);*/
			//====			

			// Delete the temporary files
			deleteAll($tmpdir);
			
			return $result['lead_id'];
	}


} // END mb_email2ticket()


function parsedPhoneFix($parsedPhone, &$resultSave)
{

	$fixedPhone = ""; //correct format is (+966) 123456789

	//05 34612494 Sample mobile without country code but starting with 05
	//01 96020111 landline example without country code but starting with 01


	/*===========
	change 00966 to +966

	For mobile or landline: if any number starts with 0 and total length is 10 digits, remove the 0 and add +966

	if mobile or landline is starting from 966 then add the + sign

	if the mobile number is starting with 5 and it has 9 digits then add +966

	if the landline number is starting with 1 and it has 9 digits then add +966	

	else remain as it is.
	===============
	*/


	//==
	
	$cCode = "(+966) ";

	//remove () and spaces
	$parsedPhone = str_replace("(","",$parsedPhone);
	$parsedPhone = str_replace(")","",$parsedPhone);
	$parsedPhone = str_replace(" ","",$parsedPhone);

	if(substr( $parsedPhone, 0, 4 ) === "+966")
	{
		$fixedPhone = preg_replace('/\+966/', $cCode, $parsedPhone, 1);
	}
	elseif(substr( $parsedPhone, 0, 3 ) === "966")
	{
		$fixedPhone = preg_replace('/966/', $cCode, $parsedPhone, 1);
	}
	else
	{
		//change 00966 to +966
		if(substr( $parsedPhone, 0, 5 ) === "00966")
		{
			$fixedPhone = preg_replace('/00966/', $cCode, $parsedPhone, 1);

		} //For mobile or landline: if any number starts with 0 and total length is 10 digits, remove the 0 and add +966
		elseif(strlen($parsedPhone)===10 && substr( $parsedPhone, 0, 1 ) === "0")
		{
			$fixedPhone = preg_replace('/0/', $cCode, $parsedPhone, 1); 

		}//if the mobile number is starting with 5 and it has 9 digits then add +966
		elseif(strlen($parsedPhone)===9 && substr( $parsedPhone, 0, 1 ) === "5")
		{
			$fixedPhone = $cCode.''.$parsedPhone;

		}//if the mobile number is starting with 5 and it has 9 digits then add +966
		elseif(strlen($parsedPhone)===9 && substr( $parsedPhone, 0, 1 ) === "1")
		{
			$fixedPhone = $cCode.''.$parsedPhone;
		}
		else
		{
			$fixedPhone = $parsedPhone;
		}
	}

 	$resultSave['mobile'] = $fixedPhone;
}

function parsedCityAndLocation($parsedLocation, &$resultSave)
{

	$parsedLocationArr = explode(" - ",$parsedLocation);

	if(count($parsedLocationArr)==2)
	{
		//10 jan 2018 new if condition added
		if (!preg_match('/[^A-Za-z0-9]/', $parsedLocationArr[0])) // '/[^a-z\d]/i' should also work.
		{
			// string contains only english letters & digits
			$resultSave['city_id'] = getCityIdByName($parsedLocationArr[0]);
			$resultSave['branch_id'] = getBranchIdByNameCron($resultSave['city_id'], $parsedLocationArr[1]);
		}
		else
		{
			
			if($parsedLocationArr[1] == "طريق المدينة")
			{
				$resultSave['city_id'] = 2;
				$resultSave['branch_id'] = 1;
				
			}elseif($parsedLocationArr[1] == "الخبر")
			{
				$resultSave['city_id'] = 7;
				$resultSave['branch_id'] = 20;		
				
			}elseif($parsedLocationArr[1] == "طريق العروبة")
			{				
				$resultSave['city_id'] = 1;
				$resultSave['branch_id'] = 18;		
			}elseif($parsedLocationArr[1] == "طريق خريص")
			{				
				$resultSave['city_id'] = 1;
				$resultSave['branch_id'] = 17;		
			}elseif($parsedLocationArr[1] == "اوتو مول")
			{				
				$resultSave['city_id'] = 2;
				$resultSave['branch_id'] = 7;		
			}
		}
		
	}
}
function parsedCityAndLocation($parsedLocation, &$resultSave)
{
	$parsedLocationArr = explode(" - ",$parsedLocation);
   
	if(count($parsedLocationArr)==2)
	{
		$resultSave['city_id'] = getCityIdByNameAr($parsedLocationArr[0]);
		$resultSave['branch_id'] = getBranchIdByNameAr($resultSave['city_id'], $parsedLocationArr[1]);
	}
}
function parsedVehiclesIds($parsedVehicles, &$resultSave, $nameStyle="old_names")
{
	$parsedVehiclesArr = explode(", ",$parsedVehicles);
	$vIds = "";
	for($ni=0; $ni<count($parsedVehiclesArr); $ni++)
	{
		if($vIds!="") $vIds.= ",";
		
		if($nameStyle==="new_names"){			
			if($parsedVehiclesArr[$ni]=="C-Class 200") $parsedVehiclesArr[$ni] = "C-Class Sedan";
			if($parsedVehiclesArr[$ni]=="S- كلاس سيدان") $parsedVehiclesArr[$ni] = "S-Class Sedan";
			
			$vIds .= getVehicleIdByName($parsedVehiclesArr[$ni]);			
		}
		elseif($nameStyle==="old_names")
			$vIds .= getVehicleIdByOldName($parsedVehiclesArr[$ni]);
		elseif($nameStyle==="ksacom_vh_names")
			$vIds .= getVehicleIdByKsaComName($parsedVehiclesArr[$ni]);		
	}
	$resultSave['vehicle'] = $vIds;
}

function parsedNameSeparate($parsedName, &$resultSave)
{
	//echo $parsedName;exit();
	$parsedNameArr = explode(" ",$parsedName);
     // echo '<pre>';
	 // print_r($parsedNameArr);exit();
	$isTitleDone = false;
	if(trim($parsedNameArr[0])==="Mr." || trim($parsedNameArr[0])==="Mrs." || trim($parsedNameArr[0])==="Miss")
	{
		$resultSave['title'] = $parsedNameArr[0];
		$isTitleDone = true;
	}
	

	if($isTitleDone)
	{
		
		if(count($parsedNameArr)==2)
		{
			$resultSave['first_name'] = $parsedNameArr[1];
		}
		elseif(count($parsedNameArr)>2)
		{
			$resultSave['first_name'] = $parsedNameArr[1]; //firstname

			$surName = "";
			for($ni=2; $ni<count($parsedNameArr); $ni++)
			{
				if($surName!="") $surName.= " ";

				$surName .= $parsedNameArr[$ni];

			}

			$resultSave['surname'] = $surName; //surname
		}

	}
	else
	{	//title does not exist

		//Title is necessory in the form submissions so this code is no more needed

		//echo '<pre>';
		//print_r($parsedNameArr);
		if(count($parsedNameArr)==1)
		{
			 $resultSave['first_name'] = $parsedNameArr[0];
		}
		elseif(count($parsedNameArr)>1)
		{
			$resultSave['first_name'] = $parsedNameArr[0]; //firstname

			$surName = "";
			for($ni=1; $ni<count($parsedNameArr); $ni++)
			{
				if($surName!="") $surName.= " ";

				$surName .= $parsedNameArr[$ni];

			}

			$resultSave['surname'] = $surName; //surname
		}
		
	}
//	exit();

}


/*
function commentsTextFix($comments)
{

			$resultSave['comments'] = convert_html_to_text($result['message']);
			$pattern = "/[a-zA-Z]*[:\/\/]*[A-Za-z0-9\-_]+\.+[A-Za-z0-9\.\/%&=\?\-_]+/i";
			$replacement = "";
			$resultSave['comments'] = preg_replace($pattern, $replacement, $resultSave['comments']);
}*/


function mb_htmlspecialchars_decode($in)
{
	return str_replace( array('&amp;', '&lt;', '&gt;', '&quot;'), array('&', '<', '>', '"'), $in);
} // END mb_htmlspecialchars_decode()

function mb_clean_utf8($in)
{
	//reject overly long 2 byte sequences, as well as characters above U+10000 and replace with ?
	$in = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
	 '|[\x00-\x7F][\x80-\xBF]+'.
	 '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
	 '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
	 '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
	 '?', $in );

	//reject overly long 3 byte sequences and UTF-16 surrogates and replace with ?
	$in = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.
	 '|\xED[\xA0-\xBF][\x80-\xBF]/S','?', $in );

	return $in;     
} // END mb_clean_utf8()


function mb_htmlspecialchars($in)
{
	return htmlspecialchars($in, ENT_COMPAT | ENT_SUBSTITUTE | ENT_XHTML, 'UTF-8');
    #return htmlspecialchars($in, ENT_COMPAT | ENT_SUBSTITUTE | ENT_XHTML, 'ISO-8859-1');
} // END mb_htmlspecialchars()

function mb_input($in, $error=0, $redirect_to='', $force_slashes=0, $max_length=0)
{
	// Strip whitespace
    $in = trim($in);

	// Sanitize input
	$in = $this->mb_clean_utf8($in);
	$in = $this->mb_htmlspecialchars($in);
	$in = preg_replace('/&amp;(\#[0-9]+;)/','&$1',$in);
    $in = addslashes($in);


	// Check length
    if ($max_length)
    {
    	$in = substr($in, 0, $max_length);
    }

    // Return processed value
    return $in;

} // END mb_input()

function mb_isValidEmail($email)
{
	/* Check for header injection attempts */
    if ( preg_match("/\r|\n|%0a|%0d/i", $email) )
    {
    	return false;
    }

    /* Does it contain an @? */
	$atIndex = strrpos($email, "@");
	if ($atIndex === false)
	{
		return false;
	}

	/* Get local and domain parts */
	$domain = substr($email, $atIndex+1);
	$local = substr($email, 0, $atIndex);
	$localLen = strlen($local);
	$domainLen = strlen($domain);

	/* Check local part length */
	if ($localLen < 1 || $localLen > 64)
	{
    	return false;
    }

    /* Check domain part length */
	if ($domainLen < 1 || $domainLen > 254)
	{
		return false;
	}

    /* Local part mustn't start or end with a dot */
	if ($local[0] == '.' || $local[$localLen-1] == '.')
	{
		return false;
	}

    /* Local part mustn't have two consecutive dots*/
	if ( strpos($local, '..') !== false )
	{
		return false;
	}

    /* Check domain part characters */
	if ( ! preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain) )
	{
		return false;
	}

	/* Domain part mustn't have two consecutive dots */
	if ( strpos($domain, '..') !== false )
	{
		return false;
	}

	/* Character not valid in local part unless local part is quoted */
    if ( ! preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local) ) ) /* " */
	{
		if ( ! preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local) ) ) /* " */
		{
			return false;
		}
	}

	/* All tests passed, email seems to be OK */
	return true;

} // END mb_isValidEmail()


function mb_html_entity_decode($in)
{
	return html_entity_decode($in, ENT_COMPAT | ENT_XHTML, 'UTF-8');
    #return html_entity_decode($in, ENT_COMPAT | ENT_XHTML, 'ISO-8859-1');
} // END mb_html_entity_decode()

function mb_msgToPlain($msg, $specialchars=0, $strip=1)
{
	$msg = preg_replace('/\<a href="(mailto:)?([^"]*)"[^\<]*\<\/a\>/i', "$2", $msg);
	$msg = preg_replace('/<br \/>\s*/',"\n",$msg);
    $msg = trim($msg);

    if ($strip)
    {
    	$msg = stripslashes($msg);
    }

    if ($specialchars)
    {
    	$msg = $this->mb_html_entity_decode($msg);
    }

    return $msg;
} // END mb_msgToPlain()
/*
function mb_ticketToPlain($ticket, $specialchars=0, $strip=1)
{
	if ( is_array($ticket) )
	{
		foreach ($ticket as $key => $value)
		{
			$ticket[$key] = is_array($ticket[$key]) ? $this->mb_ticketToPlain($value, $specialchars, $strip) : $this->mb_msgToPlain($value, $specialchars, $strip);
		}

		return $ticket;
	}
	else
	{
		return $this->mb_msgToPlain($ticket, $specialchars, $strip);
	}
} // END mb_ticketToPlain()
*/

function mb_encodeUTF8($in, $encoding)
{
	$encoding = strtoupper($encoding);

	switch($encoding)
	{
		case 'UTF-8':
			return $in;
            break;
		case 'ISO-8859-1':
			return utf8_encode($in);
			break;
		default:
			return iconv($encoding, 'UTF-8', $in);
			break;
	}
} // END mb_encodeUTF8()


function mb_stripQuotedText($message)
{

	if ( ($found = strpos($message, $this->config->item('quoted_line')) ) !== false )
	{
		// "Reply above this line" tag found, strip quoted reply
		$message  = substr($message, 0, $found);
		$message .= "\n" . "(quoted reply removed)";
	}
	

	return $message;
} // END mb_stripQuotedText()



function mb_cleanExit($results)
{

	// Delete the temporary files
	deleteAll($results['tempdir']);

	// Return NULL
	return NULL;
} // END mb_cleanExit()	
	
	public function updateLeadStatusToArchive()
	{
		$results = $this->Model_lead->getLeadsToUpdateArchive();
		if($results)
		foreach($results as $result)
		{
			
			$where['id'] = $result->id;
			$dataLead['updated_at'] = date('Y-m-d H:i:s');
			$dataLead['updated_by'] = '0'; //It is set for cron job automatically set leads to archive 
			$dataLead['status'] = 'Approved and Archived';
			$this->Model_lead->update($dataLead,$where);
					
			$dataMessage['lead_id'] = $result->id;
			$dataMessage['created_at'] = date('Y-m-d H:i:s');
			$dataMessage['created_by'] = '0'; 
			$dataMessage['message_id'] = 17;
			$this->Model_leads_messages->save($dataMessage);


			//add notification==
			$dataNotification = array();
			$dataNotification['creater_notification_read'] = 1;

			$dataNotification['created_at'] = date('Y-m-d H:i:s');
			$dataNotification['created_by'] = 0;
			$dataNotification['lead_id'] = $result->id;
			$dataNotification['comments'] = 'Auto archived by system.';

			$this->Model_notification->save($dataNotification);
			//===


			
			
		}
	}	

		
	public function checkLateLeads()
	{
		//check late leads
		$lateLeads = $this->Model_lead->getAllLateLeads();
		if($lateLeads)
		{

			foreach($lateLeads as $lateLead)
			{
				//add message 
				$dataMessage['message_id'] = 16;
				$dataMessage['created_at'] = date('Y-m-d H:i:s');
				$dataMessage['created_by'] = 0;
				$dataMessage['lead_id'] = $lateLead->id;			
				$insert_id = $this->Model_leads_messages->save($dataMessage);

				//set lead's late_lead_generated column value to 1 in leads table
				$where['id'] = $lateLead->id;		
				$dataLead['late_lead_generated'] = 1;
				$this->Model_lead->update($dataLead,$where);
					
				//get message record
				$messageRec = $this->Model_messages->get($dataMessage['message_id']);
				
				//add notification			
				$dataNotification = array();		
				if($lateLead->created_by === "0")
					$dataNotification['inbox_notification_read'] = 1;
				else
				{
					$dataNotification['creater_notification_read'] = 1;
					if($lateLead->assign_to > 0)
						$dataNotification['assignee_notification_read'] = 1;
				}
				$dataNotification['created_at'] = date('Y-m-d H:i:s');
				$dataNotification['created_by'] = 0;	
				$dataNotification['lead_id'] = $lateLead->id;
				$dataNotification['comments'] = $messageRec->title;
				$this->Model_notification->save($dataNotification);
			}
		}
	}
	
	//12 Nov 2016 - cron job for sending notification for completed surveys
	public function sendSurveyCompletionNotification()
	{
	
		$Leads = $this->Model_lead->getCompletedSurveyLeadsForNotification();
		if($Leads)
		{

			foreach($Leads as $Lead)
			{

				$dataNotification = array();
				$dataNotification['assignee_notification_read'] = 1;	//1 means unread, 0 means read		
				$dataNotification['creater_notification_read'] = 1;		//1 means unread, 0 means read
				$dataNotification['created_at'] = date('Y-m-d H:i:s');
				$dataNotification['created_by'] = 0;	
				$dataNotification['lead_id'] = $Lead->id;
				$dataNotification['comments'] = "Survey completed for the lead";
				$this->Model_notification->save($dataNotification);

				$where['id'] = $Lead->id;		
				$dataLead['survey_notification'] = 1;
				$this->Model_lead->update($dataLead,$where);
			}
		}
	
	
	}

	//after two weeks time the 
	public function surveyDataCenter()
	{
		$where['is_data_center'] = "0";
		$where['survey_response'] = "0";
		$where['date(survey_sent_datetime) <= '] = date('Y-m-d', strtotime('-2 weeks'));
		$where['survey_sent_datetime != '] = "0000-00-00 00:00:00";	//for older records don't do anything.	
		$dataLead['is_data_center'] = 1;
		$this->Model_lead->update($dataLead,$where);			
	}

	public function appointment_reminder()
    {
        set_time_limit(0);

        $data = date('Y-m-d', strtotime('+1 days'));

        $result = $this->Model_appointment->getAppointmentForReminder($data);
        echo "<pre>";

        foreach ($result as $row) {
            $name = $row->name;
            $mobile = $row->mobile;
            $email = $row->email;
            $appointment_start_time = $row->appointment_start_time;
            $appointment_end_time = $row->appointment_end_time;
            $branchTitleAr = $row->branchTitleAr;
            $branchTitleEn = $row->branchTitleEn;

            send_sms(array("phone_no" => str_replace(array('(', ')', ' '), array('', '', ''), '00923219410733'), "sms_text" => "عزیزي  ".$name.", شكرًا لتواصلكم معنا، تم تأكید موعدكم مع فریق الصیانة ویسعدنا استقبالكم في  $branchTitleAr, ".date("F j, Y, g:i a", strtotime($data['appointment_start_time']))." Regards Leads System"));

            // English SMS
            send_sms(array("phone_no" => str_replace(array('(', ')', ' '), array('', '', ''), '+923219410733'), "sms_text" => "Dear ".$data['name'].", Thank you for confirming your booking with our After Sales team. We look forward to welcoming you at  $branchTitleEn".date("F j, Y, g:i a", strtotime($data['appointment_start_time']))." Regards Leads System"));

            $emailData['first_name'] = $name;
            $emailData['branch_title_ar'] = $branchTitleAr;
            $emailData['branch_title_en'] = $branchTitleEn;
            $emailData['appointment_start_time'] = $appointment_start_time;
            $emailData['appointment_end_time'] = $appointment_end_time;
            if($email){
                sendEmail(13, $email, $emailData);
            }
        }
    }

}
