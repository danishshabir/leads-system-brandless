<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lead_survey extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
    {
        parent::__construct();
	    
        $this->load->model('Model_survey');
	    $this->load->model('Model_survey_result');
		$this->load->model('Model_question');
		$this->load->model('Model_lead');
		$this->load->model('Model_category');
		$this->load->model('Model_common'); //use this in every controller because it is accessed in header.
	  
        //$res = checkLevels(2);
		//checkAuth($res);
    }
	
	
	
	
	public function index($survey_id,$lead_id){
		
        $this->surveyAction($survey_id,$lead_id);
		
		
		}
	public function surveyAction($survey_id='',$lead_id='', $random_str='')
	{
		$data = array();
		$data['publicSurvey'] = true;
		$data['view'] = 'survey/user_survey';
		
		if(isset($_POST['form_type'])){
			switch($_POST['form_type']){
				case 'submit_survey':
				
				$this->saveSurveyResult();
				break;
			}
		}
        
        $data['survey'] = $this->Model_survey->get($survey_id);
        if($data['survey'])
        {
          $data['questions'] = $this->Model_question->getMultipleRowsOrderby($survey_id); 
          
        }else
        {
            $data['view'] = 'survey/survey_not_exist';
        }

		$leadRec = $this->Model_lead->get($lead_id);
		$cat_row = $this->Model_category->get($leadRec->category_id,true);

		if(!isset($_POST['form_type']))
		{
			if($leadRec->survey_link_rand == $random_str && $leadRec->survey_id == $survey_id)
			{
				//ok its fine.
			}
			else
			{
				//some one trying to change the url
				echo "Sorry something went wrong! please check the URL or click the link from email.";
				exit();
			}
		}
		
        $data['survey_id'] = $survey_id;
        $data['lead_id'] = $lead_id;		
		$data['trackid'] = preg_replace( '/[^0-9]/', '', $leadRec->trackid); //track id only numbers
		$data['trackid_ar'] = arabicNumbers($data['trackid']);
		$data['lead_created_at'] = date("l j M Y",strtotime($leadRec->created_at)).' '.date("h:i a",strtotime($leadRec->created_at)).'';

		$data['lead_created_at_ar'] = arabicDateAndTime($data['lead_created_at'], $data['lead_created_at']);

		$data['lead_category'] = $cat_row['title_email']; //use the category name that is used in emails.
		$data['lead_category_ar'] = $cat_row['title_email_ar'];
		$data['button'] = true;
		$data['pagetitle'] = "Leads System Test Drive Survey | مرسيدس-بنز استبيان تجربة القيادة";


		if(isset($this->session->userdata['user']['id']))
		{
			//its mean a system user is filling the survey on behalf of customer.
		}
		else
		{
			//set the survey for this lead as viewed.	
			$dataLead['survey_viewed'] = 1;
			$where['id'] = $data['lead_id']; 
			$this->Model_lead->update($dataLead,$where);
		}


		//====
		$data['submitted'] = false;
        if($leadRec->survey_response)
        {            
            $data['submitted'] = true;		    
		}
		//====

		$this->load->view('template',$data);
	}

	public function userview($survey_id='')
	{
		$data = array();
		$data['publicSurvey'] = true;
		$data['view'] = 'survey/user_survey';
		
        
        $data['survey'] = $this->Model_survey->get($survey_id);
        if($data['survey'])
        {
          $data['questions'] = $this->Model_question->getMultipleRowsOrderby($survey_id); 
          
        }else
        {
            $data['view'] = 'survey/survey_not_exist';
        }
		
		$cat_row = $this->Model_category->get($data['survey']->category_id,true);
		
		
        $data['survey_id'] = $survey_id;
        $data['lead_id'] = "0";		
		$data['trackid'] = "0001"; //track id only numbers
		$data['trackid_ar'] = "٠٠٠١"; //track id only numbers
		$data['lead_created_at'] = "Thursday 11 Aug 2016 04:53 pm";
		$data['lead_created_at_ar'] = "الخميس، ١١ أغسطس، ٢٠١٦، ٠٤:٥٣ مساء";
		$data['lead_category'] = $cat_row['title_email']; //use the category name that is used in emails.
		$data['lead_category_ar'] = $cat_row['title_email_ar'];
		$data['button'] = false;
		$data['pagetitle'] = "Leads System Test Drive Survey | مرسيدس-بنز استبيان تجربة القيادة";
		$data['submitted'] = false;
		$this->load->view('template',$data);
	}
    
    
    
    private function saveSurveyResult()
    {
        $data = array();
        $dataLead = array();
        $data['survey_id'] = $this->input->post('survey_id');
        $data['lead_id'] = $this->input->post('lead_id');
        $reslut = $this->Model_survey_result->getMultipleRows($data);
        if(!empty($reslut))
        {
            
            $data['success'] = 'false';
		    $data['error'] = 'You have already submitted this survey successfully.';
            $data['reset'] = 1;
            echo json_encode($data);
	        exit;  
        }else
        {
            $questions = $this->input->post('question');
            
            $yesno_answers = $this->input->post('yesno');
			$satoknots_answers = $this->input->post('satoknots');
			$hal_answers = $this->input->post('hal');
            //$radio_answers = $this->input->post('radio');
            //$answertext_answers = $this->input->post('answertext');
            foreach($questions as $key =>$question)
            {
                
                
                switch($question)
                {
                    case 'High Average Low' :
                        $data['answer'] = $hal_answers[$key];
						if($hal_answers[$key]=='High')
							$data['answer_val'] = 5;
						elseif($hal_answers[$key]=='Average')
							$data['answer_val'] = 3;
						elseif($hal_answers[$key]=='Low')
							$data['answer_val'] = 1;
                        break;
                    case 'Yes or No' :
                        $data['answer'] = $yesno_answers[$key];
						if($yesno_answers[$key]=='Yes')
							$data['answer_val'] = 5;
						elseif($yesno_answers[$key]=='No')
							$data['answer_val'] = 0;
                        break;
                    case 'Satisfied Ok! Not Satisfied' :
                        $data['answer'] = $satoknots_answers[$key];
						if($satoknots_answers[$key]=='Satisfied')
							$data['answer_val'] = 5;
						elseif($satoknots_answers[$key]=='Ok!')
							$data['answer_val'] = 3;
						elseif($satoknots_answers[$key]=='Not Satisfied')
							$data['answer_val'] = 1;
                        break;
                }
                $data['question_id'] = $key;
                $data['created_at'] = date('Y-m-d H:i:s');
                $this->Model_survey_result->save($data);
                 
            }

			if(isset($this->session->userdata['user']['id']))
			{
				//its mean a system user is filling the survey on behalf of customer.
				$dataLead['survey_filled_by'] = $this->session->userdata['user']['id'];
			}
            $dataLead['survey_response'] = 1;
            $where['id'] = $data['lead_id']; 
            $this->Model_lead->update($dataLead,$where);

            $data['success'] = 'Survey Submitted';
		    $data['error'] = 'false';
            $data['reload'] = 0;
            echo json_encode($data);
	        exit;
            
        }
        

    }
	
		
	
}
