<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
    {
        parent::__construct();
       
	    $this->load->model('Model_user');
		$this->load->model('Model_common'); //use this in every controller because it is accessed in header.
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	
	
	
	public function index(){
	 
		$data = array();
	  if($this->session->userdata('user'))
	  {
	   redirect($this->config->item('base_url') . 'dashboard');
	   
	  }else
	  {
	   $this->load->view('login',$data);
	   
	  }
		
		
	}
    
	public function login(){ 
		$data = array();		
		$post_data = $this->input->post();
		$post_data['is_suspend'] = 0;
		$checkUser = $this->checkUser($post_data);
//		echo $this->db->last_query(); die();

		if($checkUser != true)
		{
			$data = array();
			$data['success'] = 'false';
			$data['error'] = 'Username or password incorrect or may be user account is inactive.';
			echo json_encode($data);
			exit();
		}else
		{
			$data = array();
			$data['success'] = 'Login Successfully';
			$data['error'] = 'false';
			$data['redirect'] = '';

				//========
				$managersFH = $this->Model_common->getManagers();					
				$is_manager = false;
				for($i=0; $i<count($managersFH); $i++)
				{
					if($managersFH[$i]->id == $this->session->userdata['user']['id'])
					{
						$is_manager = true;						
					}			
				}
				
				if( rights(33,'read') ) //if logged in user is a sub manager then same graph as of manager.
				{
					$is_manager = true;					
				}
				//==========
			//if($this->session->userdata['user']['adviser_type']!=null || rights(40,'write'))


			if($this->session->userdata['user']['adviser_type'] != null || rights(40,'write') && !$is_manager)
			{
				//we are now always redirecting to dashboard 
				//$data['redirect'] = 'appointment';	
			}
				
			echo json_encode($data);
			exit();
		}
		}
	
	private function checkUser($post_data)
	{
		//echo "hello";
		//exit();
		$user = $this->Model_user->getWithMultipleFields($post_data);

	    
		if(!empty($user)){
			$user = (array)$user;
			$this->session->set_userdata('user',$user);
			$this->updateUserLoginStatus();
			return true;
		}else{
			return false;
		}
		
	}
	
	private function updateUserLoginStatus()
	{
		$data = array();
		$arr_update = array();
		$data['last_login'] = date('Y-m-d H:i:s');
		$user = $this->session->userdata('user');
		$arr_update['id'] = $user['id'];
		$this->Model_user->update($data,$arr_update);
		return true;
		//redirect($this->config->item('base_url') . 'user'); 
		
	}
	
	public function logout()
	{
		$data = array();
		$arr_update = array();
		
		$user = $this->session->userdata('user');
		$arr_update['id'] = $user['id'];
		//$this->Model_user->update($data,$arr_update);
		$this->session->unset_userdata('user');
		
		$this->session->sess_destroy();
		
		redirect($this->config->item('base_url') . 'admin');
		
	}
    
     public function forgotPassword(){
		
		
		
		  $data = array();
		 $email = $this->input->post('email');
         $arr_email['email'] = $email ;
		 $data = $this->Model_user->getWithMultipleFields($arr_email);
		
		 if(!empty($data)){
			 $bodytext="
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />
<title>Untitled Document</title>
<style type=\"text/css\">
<!--
.gray_12{font-family:\"Lucida Sans Unicode\", \"Lucida Grande\", sans-serif; font-size:12px; color:#666666; font-weight:normal; }
.gray_12_bold{font-family:\"Lucida Sans Unicode\", \"Lucida Grande\", sans-serif; font-size:12px; color:#666666; font-weight:bold; }
.gray_15_bold{font-family:\"Lucida Sans Unicode\", \"Lucida Grande\", sans-serif; font-size:15px; color:#666666; font-weight:bold; }
.gray_10{font-family:\"Lucida Sans Unicode\", \"Lucida Grande\", sans-serif; font-size:10px; color:#666666; font-weight:normal; }
-->
</style>
</head>

<body>
<table width=\"707\" cellpadding=\"8\" border=\"2\">
   
   <tr>    
    <td width=\"230\"><span class=\"gray_12_bold\">Your Password :</span></td><td><span class=\"gray_12\">&nbsp;".$data->password."</span></td>
   </tr>

   
</table>
<p>&nbsp;</p>
</body>
</html>

";
    /*----------------------------------------------*/
            $this->email->set_newline("\r\n");
                $this->email->set_mailtype("html");
                $this->email->from('Merecedes');
                $this->email->to($data->email); /* Admin Email */
                $this->email->subject('User Password');
                $this->email->message($bodytext);
                   if ($this->email->send()) {
					   
		   			$data['success'] = 'Please check your email.';
                    $data['error'] = 'false';
                    $data['model_hide'] = 1;
		        	echo json_encode($data);
		        	exit();
					 
				   }
			 
			 }else{
				 
				    $data['error'] = 'This email is not exist.';
                    $data['success'] = 'false';
                    $data['model_hide'] = 1;
		        	echo json_encode($data);
		        	exit();
				 
				 }
		 
		
		}
	
}
