<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
    {
        parent::__construct();
		checkAdminSession();
      	$this->load->model('Model_report');
		$this->load->model('Model_common'); //use this in every controller because it is accessed in header.
	    $this->load->model('Model_city');
		$this->load->model('Model_category');
		$this->load->model('Model_user');
		$this->load->model('Model_branch');
		$this->load->model('Model_struc_dep_users');
        //$res = checkLevels(2);
		//checkAuth($res);
    }
	
	
	public function download_excel()
	{
		$this->load->library('Excel');	
		
		if($this->input->post('download_excel_walkin')!=null)
		{
			//==============
			$walkinSheet1 = $this->Model_report->walkinSheet1();			
			$this->excel->setActiveSheetIndex(0);			
			$this->excel->getActiveSheet()->setTitle('Branches');        	 		
			// Field names in the first row
			$fields = $walkinSheet1->list_fields();
			$col = 0;
			foreach ($fields as $field)
			{
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
				$col++;
			}
	 
			// Fetching the table data
			$row = 2;
			foreach($walkinSheet1->result() as $data)
			{
				$col = 0;
				foreach ($fields as $field)
				{
					$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
					$col++;
				}
				$row++;
			}
			//================
			
			//==============
			$walkinSheet1 = $this->Model_report->walkinSheet2($this->Model_branch->getAll());	
			$this->excel->createSheet(1);
			$this->excel->setActiveSheetIndex(1);			
			$this->excel->getActiveSheet()->setTitle('Vehicle Walkin');        	 		
			// Field names in the first row
			$fields = $walkinSheet1->list_fields();
			$col = 0;
			foreach ($fields as $field)
			{
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
				$col++;
			}
	 
			// Fetching the table data
			$row = 2;
			foreach($walkinSheet1->result() as $data)
			{
				$col = 0;
				foreach ($fields as $field)
				{
					$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
					$col++;
				}
				$row++;
			}
			//================
			
			//================
			$walkinSheet1 = $this->Model_report->walkinSheet3($this->Model_branch->getAll());	
			$this->excel->createSheet(2);
			$this->excel->setActiveSheetIndex(2);			
			$this->excel->getActiveSheet()->setTitle('Vehicle Inbound');        	 		
			// Field names in the first row
			$fields = $walkinSheet1->list_fields();
			$col = 0;
			foreach ($fields as $field)
			{
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
				$col++;
			}
	 
			// Fetching the table data
			$row = 2;
			foreach($walkinSheet1->result() as $data)
			{
				$col = 0;
				foreach ($fields as $field)
				{
					$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
					$col++;
				}
				$row++;
			}
			//================
			
			//================
			$walkinSheet1 = $this->Model_report->walkinSheet4();	
			$this->excel->createSheet(3);
			$this->excel->setActiveSheetIndex(3);			
			$this->excel->getActiveSheet()->setTitle('Sales Consultant');
			// Field names in the first row
			$fields = $walkinSheet1->list_fields();
			$col = 0;
			foreach ($fields as $field)
			{
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
				$col++;
			}
	 
			// Fetching the table data
			$row = 2;
			foreach($walkinSheet1->result() as $data)
			{
				$col = 0;
				foreach ($fields as $field)
				{
					$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
					$col++;
				}
				$row++;
			}
			//================
			
			//================
			$walkinSheet1 = $this->Model_report->walkinSheet5();	
			$this->excel->createSheet(4);
			$this->excel->setActiveSheetIndex(4);			
			$this->excel->getActiveSheet()->setTitle('Vehicles');
			// Field names in the first row
			$fields = $walkinSheet1->list_fields();
			$col = 0;
			foreach ($fields as $field)
			{
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
				$col++;
			}
	 
			// Fetching the table data
			$row = 2;
			foreach($walkinSheet1->result() as $data)
			{
				$col = 0;
				foreach ($fields as $field)
				{
					$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
					$col++;
				}
				$row++;
			}
			//================
	 
			$this->excel->setActiveSheetIndex(0);	
	 
			$filename='Walkin_Inbound_Report.xls'; //save our workbook as this file name
	 
			header('Content-Type: application/vnd.ms-excel'); //mime type
	 
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
	 
			header('Cache-Control: max-age=0'); //no cache
						
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
	 
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
	 
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
			
		}
		
		if($this->input->post('download_excel_mkt_report')!=null)
		{
			//==============
			$walkinSheet1 = $this->Model_report->mktSheet();			
			$this->excel->setActiveSheetIndex(0);			
			$this->excel->getActiveSheet()->setTitle('MKT Report');        	 		
			// Field names in the first row
			$fields = $walkinSheet1->list_fields();
			$col = 0;
			foreach ($fields as $field)
			{
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
				$col++;
			}
	 
			// Fetching the table data
			$row = 2;
			foreach($walkinSheet1->result() as $data)
			{
				$col = 0;
				foreach ($fields as $field)
				{
					$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
					$col++;
				}
				$row++;
			}			
	 
			$this->excel->setActiveSheetIndex(0);	
	 
			$filename='MKT_Report.xls'; //save our workbook as this file name
	 
			header('Content-Type: application/vnd.ms-excel'); //mime type
	 
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
	 
			header('Cache-Control: max-age=0'); //no cache
						
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
	 
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
	 
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
			
		}
		
		if($this->input->post('download_excel_leads_count_report')!=null)
		{
			//==============
			$walkinSheet1 = $this->Model_report->leadsCountSheet();			
			$this->excel->setActiveSheetIndex(0);			
			$this->excel->getActiveSheet()->setTitle('Leads Count');        	 		
			// Field names in the first row
			$fields = $walkinSheet1->list_fields();
			$col = 0;
			/*
			foreach ($fields as $field)
			{
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
				$col++;
			}
			*/

            foreach($walkinSheet1->result() as $key=>$data)
            {
                if($col==0) {   //printing Months header first
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, 1, $fields[0]);
                }
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $data->$fields[1]);
                $col++;
            }

            // Fetching the table data
			$row = 2;
            $last_month = "";
            $col = 0;
			foreach($walkinSheet1->result() as $data)
			{
			    if($col == 0)   //print Month and Year
			    {
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $data->$fields[0]);
                }

				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $data->$fields[count($fields)-1]);
				if($last_month != "" && $last_month != $data->$fields[0])
                {
                    $row++;
                    $col = 0;
                }
			}			
	 
			$this->excel->setActiveSheetIndex(0);	
	 
			$filename='Leads_Count_Report.xls'; //save our workbook as this file name
	 
			header('Content-Type: application/vnd.ms-excel'); //mime type
	 
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
	 
			header('Cache-Control: max-age=0'); //no cache
						
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
	 
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
	 
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
			
		}

		if($this->input->post('download_excel_events_count_report')!=null)
		{
			//==============
			$walkinSheet1 = $this->Model_report->eventsCountSheet();			
			$this->excel->setActiveSheetIndex(0);			
			$this->excel->getActiveSheet()->setTitle('MKT Activity Count');        	 		
			// Field names in the first row
			$fields = $walkinSheet1->list_fields();
			$col = 0;
			foreach ($fields as $field)
			{
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
				$col++;
			}
	 
			// Fetching the table data
			$row = 2;
			foreach($walkinSheet1->result() as $data)
			{
				$col = 0;
				foreach ($fields as $field)
				{
					$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
					$col++;
				}
				$row++;
			}			
	 
			$this->excel->setActiveSheetIndex(0);	
	 
			$filename='MKT_Activity_Report.xls'; //save our workbook as this file name
	 
			header('Content-Type: application/vnd.ms-excel'); //mime type
	 
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
	 
			header('Cache-Control: max-age=0'); //no cache
						
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
	 
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
	 
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
			
		}
		
		if($this->input->post('download_excel_sc_testdrive_report')!=null)
		{
			//==============
			$sheet1 = $this->Model_report->scTestDriveSheet();			
			$this->excel->setActiveSheetIndex(0);			
			$this->excel->getActiveSheet()->setTitle('SC Test Drives');        	 		
			// Field names in the first row
			$fields = $sheet1->list_fields();
			$col = 0;
			foreach ($fields as $field)
			{
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
				$col++;
			}
	 
			// Fetching the table data
			$row = 2;
			foreach($sheet1->result() as $data)
			{
				$col = 0;
				foreach ($fields as $field)
				{
					$this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
					$col++;
				}
				$row++;
			}			
	 
			$this->excel->setActiveSheetIndex(0);	
	 
			$filename='SC_Test_Drive_Report.xls'; //save our workbook as this file name
	 
			header('Content-Type: application/vnd.ms-excel'); //mime type
	 
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
	 
			header('Cache-Control: max-age=0'); //no cache
						
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
	 
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
	 
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
			
		}
		
		exit();
	}
	
	
	public function index(){
		
		$data = array();        
        //$user_id = $this->session->userdata['user']['id'];
		//$user_role = $this->session->userdata['user']['role_id'];
        //$user_city = $this->session->userdata['user']['city_id'];
        //$user_branch = $this->session->userdata['user']['branch_id'];


		//filterData contains all from query string + dep id is used to fetch all the users of the dep and included in filter here in controller to send to model.
		$filterData = $this->input->get(); 
		$filterData['depUsers'] = ""; //comma seperated all users of the department

		$data['depUsers'] = $this->Model_report->getDepUsers();
		$data['is_manager'] = false;
		
		//===
		$userDepId = $this->Model_common->getUserDep($this->session->userdata['user']['id']);
		$managers = $this->Model_common->getManagers();
		
		$depId = 0;
		for($i=0; $i<count($managers); $i++)
		{
			if($managers[$i]->id == $this->session->userdata['user']['id'])
			{
				$data['is_manager'] = true;
				$depId = $managers[$i]->dep_id;
				break;
			}			
		}
		if( rights(33,'read') ) //if logged in user is a sub manager then same graph as of manager.
		{
			$data['is_manager'] = true;
			$depId = $this->Model_common->getUserDep($this->session->userdata['user']['id']);
		}
		
		
		//if(isset($_GET["abha"])) $depId = 54;
		//if(isset($_GET["madina"])) $depId = 80;
		
		/*
		if($this->session->userdata['user']['id'] === "132" || $this->session->userdata['user']['id']==="133" || $this->session->userdata['user']['id']==="172" || $this->session->userdata['user']['id']==="135" || $this->session->userdata['user']['id']==="292")
		{
			$data['is_manager'] = false;			
		}
		elseif($data['is_manager'])
		{			
			$_GET['dep_id'] = $depId;
			$filterData['dep_id'] = $depId;
			
			//check if manager while viewing individual reports, then did not try to change the id from url?
			//should only have access to his own branch users access
			if(isset($_GET['user_id']))
			{
				$isValidU = false;
				$depUsersResArrC = $data['depUsers'][2][$depId];
				foreach($depUsersResArrC as $depUserC)
				{								
					if($_GET['user_id']==$depUserC['user_id'])
					{
						$isValidU = true;
						break;					
					}
				}
				if(!$isValidU)
				{
					echo "Sorry you don't have access to this page!";
					exit();
				}
			}
			//===
			
			
		}
		elseif($userDepId)
		{
			//sales consultant can't view the report via report url. but same page is accessed via route :)
			$controller_name = $this->uri->segment(1);
			if($controller_name=="report") {
				echo "Sorry you don't have access to this page!";
				exit();
			}
			
			$filterData['user_id'] = $this->session->userdata['user']['id'];
			$filterData['dep_id'] = $userDepId;
			$_GET['user_id'] = $this->session->userdata['user']['id'];
			$_GET['dep_id'] = $userDepId;			
		
		}
		else
		{
			echo "Sorry you don't have access to this page!";
			exit();
		}
		//===
*/
		//get dep all users comma seperated
		$depUserIdsCommaSep = "";
		if(isset($filterData['dep_id']) && $filterData['dep_id']!=="0")
		{
			$depUsersResArr = $data['depUsers'][2][$filterData['dep_id']];			
			foreach($depUsersResArr as $depUser) {if($depUserIdsCommaSep!="") $depUserIdsCommaSep.=","; $depUserIdsCommaSep.=$depUser['user_id'];}

			$filterData['depUsers'] = $depUserIdsCommaSep;
			$data['filter_dep_report'] = 1;

		}
		//===

		//individual user
		if(isset($filterData['user_id']) && $filterData['user_id']!=="0" && $filterData['depUsers']!="")
		{
			$data['filter_individual_report'] = 1;
				
			//$data['userRedar'] = $this->Model_report->getUserRedar($user_id);
			$data['userRedar'] = $this->Model_report->getUserRedar($filterData);

			//$data['depRedar'] = $this->Model_report->getDepRedar($depUserIdsCommaSep);
			$data['depRedar'] = $this->Model_report->getDepRedar($filterData);
			
			$data['usersNumOfClosedLeads'] = $this->Model_report->getUsersNumOfClosedLeads($filterData);
			
			$data['usersNumOfFinishedButNotClosedLeads'] = $this->Model_report->getUsersNumOfFinishedButNotClosedLeads($filterData);
			
			$data['numOfDisapprovedActionEachUser'] = $this->Model_report->getNumOfDisapprovedActionEachUser($filterData);
			
			$data['numOfSuccessfullCallActionEachUser'] = $this->Model_report->getNumOfSuccessfullCallActionEachUser($filterData);		
			
			$data['numOfNoAnsActionEachUser'] = $this->Model_report->getNumOfNoAnsActionEachUser($filterData);
			
			$data['numOfScheduledCallActionEachUser'] = $this->Model_report->getNumOfScheduledCallActionEachUser($filterData);
			
			$data['userGraphAvgScore'] = $this->Model_report->getUserGraphAvgScore($filterData);
			
			$data['userGraphDepsAvgScore'] = $this->Model_report->getUserGraphDepsAvgScore($filterData);
			
			$data['uGraphTotalAvgScoreYearly'] = $this->Model_report->getUGraphTotalAvgScoreYearly($filterData);
			
			$data['userGraphAvgScoreAsUidKey'] = $this->Model_report->getUserGraphAvgScoreAsUidKey($filterData);
			
			$data['uGraphTotalLeadsYearly'] = $this->Model_report->getUGraphTotalLeadsYearly($filterData);
			
			$data['uGraphTotalComLeadsYearly'] = $this->Model_report->getUGraphTotalComLeadsYearly($filterData);
			
			$data['uGraphLeadSource'] = $this->Model_report->getUGraphLeadSource($filterData);
			
			$data['uGraphLeadSource2'] = $this->Model_report->getUGraphLeadSource2($filterData);
			
			$data['hourTimeOfSuccessfullCallAction'] = $this->Model_report->getHourTimeOfSuccessfullCallAction($filterData); 
			
			$data['carsConversions'] = $this->Model_report->getCarsConversions($filterData); 
			
			$data['carsConversions2'] = $this->Model_report->getCarsConversions2($filterData); 
			
			$data['carsConversions3'] = $this->Model_report->getCarsConversions3($filterData); 
			
			$data['countUserLeads'] = $this->Model_report->getCountUserLeads($filterData); 
			
			$data['countUserScheduled'] = $this->Model_report->getCountUserScheduled($filterData); 
			
			$data['countUserCompleted'] = $this->Model_report->getCountUserCompleted($filterData);
			
			$data['countUserPurchased'] = $this->Model_report->getCountUserPurchased($filterData);
			
			$data['countUserLost'] = $this->Model_report->getCountUserLost($filterData);
			
			$data['uGraphGradeAvgScoreYearly'] = $this->Model_report->getUGraphGradeAvgScoreYearly($filterData);
					
			
		}
		else
		{

			$data['timeToMakeCallActionEachUserAvg'] = $this->Model_report->getTimeToMakeCallActionEachUserAvg($filterData);

			$data['timeToFinishActionEachUserAvg'] = $this->Model_report->getTimeToFinishActionEachUserAvg($filterData);

			$data['timeToCloseActionEachUserAvg'] = $this->Model_report->getTimeToCloseActionEachUserAvg($filterData);

			$data['numOfDisapprovedActionEachUser'] = $this->Model_report->getNumOfDisapprovedActionEachUser($filterData);

			
			
			$data['usersFromLeads'] = $this->Model_report->getUsersFromLeads($filterData);
			$data['usersNumOfClosedLeads'] = $this->Model_report->getUsersNumOfClosedLeads($filterData);
			$data['usersNumOfFinishedButNotClosedLeads'] = $this->Model_report->getUsersNumOfFinishedButNotClosedLeads($filterData);

			

			$data['numOfSuccessfullCallActionEachUser'] = $this->Model_report->getNumOfSuccessfullCallActionEachUser($filterData);		

			$data['numOfNoAnsActionEachUser'] = $this->Model_report->getNumOfNoAnsActionEachUser($filterData);
			
			//$data['AllActionCountEachUser'] = $this->Model_report->getAllActionCountEachUser($filterData);
			
			$data['numOfClosedWithNoCallsActionEachUser'] = $this->Model_report->getNumOfClosedWithNoCallsActionEachUser($filterData);

			$data['numOfClosedWithNoCallsEmailsActionEachUser'] = $this->Model_report->getNumOfClosedWithNoCallsEmailsActionEachUser($filterData);		

			$data['numOfClosedWithNoAnsweredEachUser'] = $this->Model_report->getNumOfClosedWithNoAnsweredEachUser($filterData);

			$data['getNumOfClosedLeadsWithNoCallAndNoAnswerAndEmail'] = $this->Model_report->getNumOfClosedLeadsWithNoCallAndNoAnswerAndEmail($filterData);

			$data['numOfCarsEachUser'] = $this->Model_report->getNumOfCarsEachUser($filterData);

			$data['numOfCarsEachBranch'] = $this->Model_report->getNumOfCarsEachBranch($filterData);

			$data['numOfAllCarsEachBranch'] = $this->Model_report->getNumOfAllCarsEachBranch($filterData);		

			$data['allCarsKSA'] = $this->Model_report->getAllCarsKSA($filterData);

			$data['numOfCarsEachCity'] = $this->Model_report->getNumOfCarsEachCity($filterData);

			$data['numOfAllCarsEachCity'] = $this->Model_report->getNumOfAllCarsEachCity($filterData);

			$data['numOfSuccessDrivesEachBranch'] = $this->Model_report->getNumOfSuccessDrivesEachBranch($filterData);

			$data['successDrives'] = $this->Model_report->getSuccessDrives($filterData);		

			$data['allCarsKSAPerc'] = $this->Model_report->getAllCarsKSAPerc($filterData);

			$data['allCarsPerSource'] = $this->Model_report->getAllCarsPerSource($filterData);

			$data['allPurchCarsKSAPerc'] = $this->Model_report->getAllPurchCarsKSAPerc($filterData);

			$data['depUsersSurveyScores'] = $this->Model_report->getDepUsersSurveyScores($filterData);

		//====		
			$data['userAvgScore'] = $this->Model_report->getUserAvgScore($filterData);			
			$data['userAvgScore1'] = $this->Model_report->getUserAvgScore1($filterData);
			$data['userAvgScore2'] = $this->Model_report->getUserAvgScore2($filterData);
			$data['userAvgScoreAsUidKey'] = $this->Model_report->getUserAvgScoreAsUidKey($filterData);			
			$data['depsAvgScore'] = $this->Model_report->getDepsAvgScore($filterData);

			$userIds = array();
			if($data['userAvgScore1']) foreach($data['userAvgScore1'] as $avg){$userIds[] = $avg->userid;}
			
			$userDepAvg = array();
			if(count($userIds)>0)
			{
				//=====
					foreach($userIds as $uId)
					{
						$userDepsAvg[$uId] = 0;

						$depsOtherUsers = array();
						$wdu = array();
						$wdu['user_id'] = $uId;
						$thisUserDepsRes = $this->Model_struc_dep_users->getMultipleRows($wdu);
						if($thisUserDepsRes)
						{
							foreach($thisUserDepsRes as $uDepRow)
							{
								$depsOtherUsers = array(); 
								$wdu = array();
								$wdu['struc_dep_id'] = $uDepRow->struc_dep_id;
								$thisDepUsersRes = $this->Model_struc_dep_users->getMultipleRows($wdu);
								if($thisDepUsersRes)
								{
									foreach($thisDepUsersRes as $thisDepUserRow)
									{
										$depsOtherUsers[] = $thisDepUserRow->user_id;
									}
								}
								$depsOtherUsers1 = implode(',',$depsOtherUsers);
								$userDepsAvgScore = $this->Model_report->getUserDepsAvgScore($depsOtherUsers1,$filterData); 
								if($userDepsAvgScore)
									$userDepAvg[$uId] = $userDepsAvgScore->avgval;
							}

						}
					}

					$data['userDepsAvgScore'] = $userDepAvg;
				//====
					
					$data['totalAvgScoreYearly'] = $this->Model_report->getTotalAvgScoreYearly($userIds,$filterData); 

				//====
			}

		//===

			$data['totalAvgScore'] = $this->Model_report->getTotalAvgScore($filterData);

			$data['avgTimeOfNoAnsActionEachUser'] = $this->Model_report->getAvgTimeOfNoAnsActionEachUser($filterData);		

			$data['avgTimeOfSuccessfullCallActionEachUser'] = $this->Model_report->getAvgTimeOfSuccessfullCallActionEachUser($filterData);		

			$data['userTLPM'] = $this->Model_report->getUserTLPM($filterData);

			$data['userTLPM1'] = $this->Model_report->getUserTLPM1($filterData);
			$userIds = array();
			if($data['userTLPM1']) foreach($data['userTLPM1'] as $avg){$userIds[] = $avg->userid;}
			$data['totalLeadsYearly'] = $this->Model_report->getTotalLeadsYearly($userIds,$filterData);

			$data['branchTLPM'] = $this->Model_report->getBranchTLPM($filterData);
			$data['branchTLPM1'] = $this->Model_report->getBranchTLPM1($filterData);
			$branchIds = array();
			if($data['branchTLPM1']) foreach($data['branchTLPM1'] as $avg){$branchIds[] = $avg->branchid;}
			$data['totalBranchLeadsYearly'] = $this->Model_report->getTotalBranchLeadsYearly($branchIds,$filterData);
			$data['orgStrucDepLeadsYearly'] = $this->Model_report->getOrgStrucDepLeadsYearly($filterData);
			$data['orgStrucDepLeads'] = $this->Model_report->getOrgStrucDepLeads($filterData);

			$data['numberOfLeadsPerCategories'] = $this->Model_report->getNumberOfLeadsPerCategories($filterData);
			$data['numberOfCompletedTestDrivesPerCategories'] = $this->Model_report->getNumberOfCompletedTestDrivesPerCategories($filterData);
			$data['numberOfFinishedPerCategories'] = $this->Model_report->getNumberOfFinishedPerCategories($filterData);
			$data['numberOfLostPerCategories'] = $this->Model_report->getNumberOfLostPerCategories($filterData);
			
			$data['numberOfPurchPerCategories'] = $this->Model_report->getNumberOfPurchPerCategories($filterData);
			$data['numberOfPurchPerWebsiteSource'] = $this->Model_report->getNumberOfPurchPerWebsiteSource($filterData);

			$data['numberOfLostPerWebsiteSource'] = $this->Model_report->getNumberOfLostPerWebsiteSource($filterData);
			$data['numberOfFinishedPerWebsiteSource'] = $this->Model_report->getNumberOfFinishedPerWebsiteSource($filterData);
			//$data['numberOfCompletedTestDrivesPerWebsiteSource'] = $this->Model_report->getNumberOfCompletedTestDrivesPerWebsiteSource($filterData);
			//$data['numberOfLeadsPerManualSource'] = $this->Model_report->getNumberOfLeadsPerManualSource($filterData);
			$data['numberOfLeadsPerWebsiteSource'] = $this->Model_report->getNumberOfLeadsPerWebsiteSource($filterData);

			$data['numberOfViewedSurvey'] = $this->Model_report->getNumberOfViewedSurvey($filterData);		
			$data['numberOfSentSurvey'] = $this->Model_report->getNumberOfSentSurvey($filterData);		
			$data['numberOfCompletedSurvey'] = $this->Model_report->getNumberOfCompletedSurvey($filterData);		
			$data['numberOfTestDriveLeads'] = $this->Model_report->getNumberOfTestDriveLeads($filterData);		
			$data['numberOfCompletedTestDrivesLeads'] = $this->Model_report->getNumberOfCompletedTestDrivesLeads($filterData);	
			$data['numberOfLeads'] = $this->Model_report->getNumberOfLeads($filterData);			
			$data['numberOfPurchasedCarsLeads'] = $this->Model_report->getNumberOfPurchasedCarsLeads($filterData);			
			$data['timeFromLeadCreationToPurchase'] = $this->Model_report->getTimeFromLeadCreationToPurchase($filterData);			
			$data['timeFromLeadCreationToVisit'] = $this->Model_report->getTimeFromLeadCreationToVisit($filterData);
			$data['timeFromLeadCreationToLost'] = $this->Model_report->getTimeFromLeadCreationToLost($filterData);
			$data['branchesAvgScore'] = $this->Model_report->getBranchesAvgScore($filterData);
			$data['userLostTLPM'] = $this->Model_report->getUserLostTLPM($filterData);
			$data['avgScorePerCategories'] = $this->Model_report->getAvgScorePerCategories($filterData);
			$data['avgScorePerWebsiteSource'] = $this->Model_report->getAvgScorePerWebsiteSource($filterData);
			$data['userClosedTLPM'] = $this->Model_report->getUserClosedTLPM($filterData);
			$data['userFinishedTLPM'] = $this->Model_report->getUserFinishedTLPM($filterData);
			$data['branchClosedTLPM'] = $this->Model_report->getBranchClosedTLPM($filterData);
			$data['orgStrucBranchClosedTLPM'] = $this->Model_report->getOrgStrucBranchClosedTLPM($filterData);
			$data['branchFinishedTLPM'] = $this->Model_report->getBranchFinishedTLPM($filterData);
			$data['orgStrucBranchFinishedTLPM'] = $this->Model_report->getOrgStrucBranchFinishedTLPM($filterData);
			$data['noBranchFinishedTLPM'] = $this->Model_report->getNoBranchFinishedTLPM($filterData);
			$data['noBranchClosedTLPM'] = $this->Model_report->getNoBranchClosedTLPM($filterData);
			$data['hourTimeOfSuccessfullCallAction'] = $this->Model_report->getHourTimeOfSuccessfullCallAction($filterData); 
			$data['compTestDrivesBranches'] = $this->Model_report->getCompTestDrivesBranches($filterData); 
			$data['purchTestDrivesBranches'] = $this->Model_report->getPurchTestDrivesBranches($filterData); 
			$data['purchasedSalesCons'] = $this->Model_report->getPurchasedSalesCons($filterData);

		
		}

		
		$data['categories'] = $this->Model_category->getAll();
		//$data['users'] = $this->Model_user->getAllUsersWithRoles();
		$data['cities'] = $this->Model_city->getAll();
		$data['allBranches'] = $this->Model_branch->getAll();

		$data['view'] = 'report/report';
		$this->load->view('template',$data);		
	
	}

	public function d3_bars_hierarchical_cars($date_from="", $date_to="")
	{
		$filterData = array();
		$filterData['date_from'] = $date_from;
		$filterData['date_to'] = $date_to;
		
		/*echo $json_data = '
		{
		 "name": "Cars",
		 "children": [
		  
		  		  
		  {
		   "name": "analytics",
		   "children": [
			{
			 "name": "cluster",
			 "children": [
			  {"name": "AgglomerativeCluster", "size": 3938},
			  {"name": "CommunityStructure", "size": 3812},
			  {"name": "HierarchicalCluster", "size": 6714},
			  {"name": "MergeEdge", "size": 743},
			  {
			   "name": "analytics_cus",
			   "children": [				
			   {"name": "AgglomerativeCluster", "size": 3938},
			   {"name": "CommunityStructure", "size": 3812},
			   {"name": "HierarchicalCluster", "size": 6714},
			   {"name": "MergeEdge", "size": 743}
			   ]
			  }

			 ]
			},
			{
			 "name": "graph",
			 "children": [
			  {"name": "BetweennessCentrality", "size": 3534},
			  {"name": "LinkDistance", "size": 5731},
			  {"name": "MaxFlowMinCut", "size": 7840},
			  {"name": "ShortestPaths", "size": 5914},
			  {"name": "SpanningTree", "size": 3416}
			 ]
			},
			{
			 "name": "optimization",
			 "children": [
			  {"name": "AspectRatioBanker", "size": 7074}
			 ]
			}
		   ]
		  }


		 ]
		}';*/

		//========================

		//$filterData = $this->input->get();

		$numOfCarsEachUser = $this->Model_report->getCarTypeEachUser($filterData);
		$gCarTypeEachUser = $this->Model_report->getGCarTypeEachUser($filterData);
		

		$json_data = '
		{"name": "Cars","children": [';

		$allCities = $this->Model_report->getCitiesWithBranches();
		//echo "<pre>";
		//print_r($allCities); exit();
		
		for($cityCounter=0; $cityCounter<count($allCities); $cityCounter++)
		{
			
			if($cityCounter>0) $json_data .= ',';
			$json_data .= '{"name": "'.$allCities[$cityCounter]->title.'", ';

			//$bWhere['city_id'] = $allCities[$cityCounter]->id;
			//$branchesRes = $this->Model_branch->getMultipleRows($bWhere);
			$branchesRes = array();
			if((int)$allCities[$cityCounter]->id===2)
			{
				$branchesRes[0]['id'] = 52;
				$branchesRes[0]['title'] = "Madina Road Showroom";				
				
				$branchesRes[1]['id'] = 53;
				$branchesRes[1]['title'] = "Automall Showroom";				
			}
			elseif((int)$allCities[$cityCounter]->id===1)
			{
				$branchesRes[0]['id'] = 56;
				$branchesRes[0]['title'] = "Uroubah Showroom";				
				
				$branchesRes[1]['id'] = 55;
				$branchesRes[1]['title'] = "Khurais Showroom";				
			}
			elseif((int)$allCities[$cityCounter]->id===7)
			{
				$branchesRes[0]['id'] = 57;
				$branchesRes[0]['title'] = "Dammam Showroom";						
			}
			elseif((int)$allCities[$cityCounter]->id===17)
			{
				$branchesRes[0]['id'] = 54;
				$branchesRes[0]['title'] = "Abha Showroom";						
			}
			else
			{
				continue;
			}
			
			if(count($branchesRes))
			{
				$json_data .= '"children": [';
				for($branchCounter=0; $branchCounter<count($branchesRes); $branchCounter++)					
				{
					if($branchCounter>0) $json_data .= ',';
					
					//$json_data .= '{"name": "'.$branchesRes[$branchCounter]->title.'", ';

					//$uWhere['branch_id'] = $branchesRes[$branchCounter]->id;
					//$usersRes = $this->Model_user->getMultipleRows($uWhere);
					
					$json_data .= '{"name": "'.$branchesRes[$branchCounter]['title'].'", ';
					$usersRes = $this->Model_report->getDepUsersByDepId($branchesRes[$branchCounter]['id']);
	
					if($usersRes)
					{
						$json_data .= '"children": [';
						for($userCounter=0; $userCounter<count($usersRes); $userCounter++)					
						{
							if($userCounter>0) $json_data .= ',';
							$json_data .= '{"name": "'.$usersRes[$userCounter]->full_name.'", '; 

							$uId = $usersRes[$userCounter]->id;
							if(isset($gCarTypeEachUser) && in_array($uId,$gCarTypeEachUser[0]))
							{
								$json_data .= '"children": [';

																									
								$gCarTypeEachUserArr = explode(",",$gCarTypeEachUser[1][$uId]); //Requested
								$gCarTypeEachUserArr = array_count_values($gCarTypeEachUserArr);

								$carsOfThisUserArr = array();
								if(isset($numOfCarsEachUser[1][$uId]))
								{
									$carsOfThisUserArr = explode(",",$numOfCarsEachUser[1][$uId]); //Scheduled
									$carsOfThisUserArr = array_count_values($carsOfThisUserArr);
								}

								$carsOfThisUserArr2 = array();
								if(isset($numOfCarsEachUser[2][$uId]))
								{
									$carsOfThisUserArr2 = explode(",",$numOfCarsEachUser[2][$uId]); //Completed (Including walkin)
									$carsOfThisUserArr2 = array_count_values($carsOfThisUserArr2);	
								}

								$gCarsCounter = 0;
								/*foreach($gCarTypeEachUserArr as $carName=>$count)
								{
									if(getGeneralVehicleNameById($carName)==="N/A") continue;

									if($gCarsCounter>0) $json_data .= ',';
									$json_data .= '{"name": "'.getGeneralVehicleNameById($carName).'", '; //	"size": '.$count.'}	
									$json_data .= '"children": [';
									$json_data .= '{"name": "Requests", "size": '.$count.'}';

									if(isset($carsOfThisUserArr[$carName]))
									{
										$json_data .= ',';
										$json_data .= '{"name": "Scheduled", "size": '.$carsOfThisUserArr[$carName].'}';
									}
									if(isset($carsOfThisUserArr2[$carName]))
									{
										$json_data .= ',';
										$json_data .= '{"name": "Completed", "size": '.$carsOfThisUserArr2[$carName].'}';
									}

									$json_data .= ']}';
									$gCarsCounter++;
								}*/
								
								foreach($gCarTypeEachUserArr as $carName=>$count)
								{
									if(getGeneralVehicleNameById($carName)==="N/A") continue;
									
									$completedCount = 0;
									if(isset($carsOfThisUserArr2[$carName])) $completedCount = $carsOfThisUserArr2[$carName];

									if($gCarsCounter>0) $json_data .= ',';
									$json_data .= '{"name": "'.getGeneralVehicleNameById($carName).'", '; //	"size": '.$count.'}	
									$json_data .= '"children": [';
									$notCompleted = $count-$completedCount;
									$json_data .= '{"name": "Not Completed", "size": '.$notCompleted.'}';
									
									if($completedCount)
									{
										$json_data .= ',';
										$json_data .= '{"name": "Completed", "size": '.$completedCount.'}';
									}

									$json_data .= ']}';
									$gCarsCounter++;
								}
								

									//$carsCounter = $gCarsCounter;
									/*$carsCounter = 0;
									foreach($carsOfThisUserArr as $carName=>$count)
									{
										if($carsCounter>0) $json_data .= ',';		
										
										$json_data .= '{"name": "'.getGeneralVehicleNameById($carName).'", '; //
										$json_data .= '"children": [';
										$json_data .= '{"name": "Scheduled", "size": '.$count.'}';
										if(isset($carsOfThisUserArr2[$carName]))
										{
											$json_data .= ',';
											$json_data .= '{"name": "Completed", "size": '.$carsOfThisUserArr2[$carName].'}';
										}
										$json_data .= ']}';

										
										$carsCounter++;
									}*/
									
								

								//Completed
								/*if(isset($numOfCarsEachUser[2][$uId]))
								{
									$carsOfThisUserArr2 = explode(",",$numOfCarsEachUser[2][$uId]); //specific
									$carsOfThisUserArr2 = array_count_values($carsOfThisUserArr2);		
									$carsCounter2 = 0;
									foreach($carsOfThisUserArr2 as $carName=>$count)
									{
										if($carName)
										{
											if($count>1 && false)
											{
												$cars .= getSpecificCarName($carName)."(".$count."), ";
											}
											else
											{
												if(!array_key_exists($carName, $carsOfThisUserArr ))
												{
													if($carsCounter2>0) $json_data .= ',';
													$json_data .= '{"name": "'.getSpecificCarName($carName).'", "size": 1}';
													$carsCounter2++;
												}												
											}
										}										
									}									
								}*/

								$json_data .= ']}';
							}
							else
							{
								$json_data .= '"size": 0}';
							}
							














						}
						$json_data .= ']}';
					}
					else
					{
						$json_data .= '"size": 0}';
					}

				}
				$json_data .= ']}';

			}
			else
			{
				$json_data .= '"size": 0}';
			}
		}

		$json_data .= ' ]}';
		//========================

		//echo "<pre>";
		echo $json_data;

		/*echo $json_data = '
		{
		 "name": "Cars",
		 "children": [
		  
		  		  
		  {
		   "name": "analytics",
		   "children": [
			{
			 "name": "cluster",
			 "children": [
			  {"name": "AgglomerativeCluster", "size": 3938},
			  {"name": "CommunityStructure", "size": 3812},
			  {"name": "HierarchicalCluster", "size": 6714},
			  {"name": "MergeEdge", "size": 743},
			  {
			   "name": "analytics_cus",
			   "children": [				
			   {"name": "AgglomerativeCluster", "size": 3938},
			   {"name": "CommunityStructure", "size": 3812},
			   {"name": "HierarchicalCluster", "size": 6714},
			   {"name": "MergeEdge", "size": 743}
			   ]
			  }

			 ]
			},
			{
			 "name": "graph",
			 "children": [
			  {"name": "BetweennessCentrality", "size": 3534},
			  {"name": "LinkDistance", "size": 5731},
			  {"name": "MaxFlowMinCut", "size": 7840},
			  {"name": "ShortestPaths", "size": 5914},
			  {"name": "SpanningTree", "size": 3416}
			 ]
			},
			{
			 "name": "optimization",
			 "children": [
			  {"name": "AspectRatioBanker", "size": 7074}
			 ]
			}
		   ]
		  }


		 ]
		}';*/

		exit();
	}

	public function d3_bars_hierarchical_cars2($date_from="", $date_to="")
	{
		//$filterData = $this->input->get();
		$filterData = array();
		$filterData['date_from'] = $date_from;
		$filterData['date_to'] = $date_to;
		
		$getCarTypes = $this->Model_report->getCarTypes($filterData);
		$carsEachBranch = $this->Model_report->getCarsEachBranch($filterData);
		$gCarTypeEachBranch = $this->Model_report->getGCarTypeEachBranch($filterData);		
		$cars = $getCarTypes[0];
		$branchesSC = $getCarTypes[1];

		$gCarType = $this->Model_report->getGCarType($filterData);
		$cars = array_unique(array_merge($cars, $gCarType[0]));
		$cars = array_values($cars);

		$branches = $gCarType[1];

		foreach($branches as $gid=>$branch)
		{
			if(array_key_exists($gid,$branchesSC))
			{
				//general $branch 
				//SC $branchesSC[$gid];
				$tempGBsArr = explode(",",$branch);
				$tempBsArr = explode(",",$branchesSC[$gid]);

				foreach($tempBsArr as $tempB)
				{
					if(!in_array($tempB, $tempGBsArr))
					{
						$branches[$gid] = $branches[$gid].",".$tempB;
					}
				}

			}

		}


		$json_data = '
		{"name": "Cars","children": [';
		for($carCounter=0; $carCounter<count($cars); $carCounter++)
		{			
			$gId = $cars[$carCounter];
			$gCarName = getGeneralVehicleNameById($gId);
			if($gCarName==="N/A") continue;
			if($carCounter>0) $json_data .= ',';
			$json_data .= '{"name": "'.$gCarName.'", ';
			$json_data .= '"children": [';

			$branchesArr = explode(",",$branches[$gId]);
			$branchesArr = array_unique($branchesArr);
			$bCounter = 0;
			
			/*foreach($branchesArr as $bId)
			{
				if($bCounter>0) $json_data .= ',';
				$json_data .= '{"name": "'.getBranchName($bId).'", ';// "size": "20"}

				$json_data .= '"children": [';


				$gCars = array();
				$addComma = false;
				if(isset($gCarTypeEachBranch[1][$bId]))
				{
					$gCars = $gCarTypeEachBranch[1][$bId];
					$gCarsArr = explode(",",$gCars);                  //Requests
					$gCarsArr = array_count_values($gCarsArr);										
					if(isset($gCarsArr[$gId]))
					{						
						$json_data .= '{"name": "Requests", "size": '.$gCarsArr[$gId].'}';
						$addComma  = true;
					}

				}

				if(isset($carsEachBranch[1][$bId]))
				{					
					$scheduledArr = explode(",",$carsEachBranch[1][$bId]);                     //Scheduled
					$scheduledArr = array_count_values($scheduledArr);										
					if(isset($scheduledArr[$gId]))
					{
						if($addComma) $json_data .= ',';
						$json_data .= '{"name": "Scheduled", "size": '.$scheduledArr[$gId].'}';		
						$addComma = true;
					}										
				}

				if(isset($carsEachBranch[2][$bId]))
				{						
					$completedArr = explode(",",$carsEachBranch[2][$bId]);                   //Completed (Completed+walkin)
					$completedArr = array_count_values($completedArr);
						if(isset($completedArr[$gId]))
						{
							if($addComma) $json_data .= ',';
							$json_data .= '{"name": "Completed", "size": '.$completedArr[$gId].'}';
						}
				}
				

				$json_data .= ']}';

				$bCounter++;
			}*/
			
			foreach($branchesArr as $bId)
			{
				$completedCount = 0;
				if(isset($carsEachBranch[2][$bId]))
				{						
					$completedArr = explode(",",$carsEachBranch[2][$bId]);                   //Completed (Completed+walkin)
					$completedArr = array_count_values($completedArr);
						if(isset($completedArr[$gId]))
						{
							$completedCount = $completedArr[$gId];
						}
				}
				
				if($bCounter>0) $json_data .= ',';
				$json_data .= '{"name": "'.getBranchName($bId).'", ';// "size": "20"}

				$json_data .= '"children": [';


				$gCars = array();
				$addComma = false;
				if(isset($gCarTypeEachBranch[1][$bId]))
				{
					$gCars = $gCarTypeEachBranch[1][$bId];
					$gCarsArr = explode(",",$gCars);                  
					$gCarsArr = array_count_values($gCarsArr);										
					if(isset($gCarsArr[$gId]))
					{						
						$notCompleted = $gCarsArr[$gId]-$completedCount;    //Requests not completed
						$json_data .= '{"name": "Not Completed", "size": '.$notCompleted.'}';
						$addComma  = true;
					}

				}

				if($completedCount)
				{			
					if($addComma) $json_data .= ',';
					$json_data .= '{"name": "Completed", "size": '.$completedCount.'}';
				}
				

				$json_data .= ']}';

				$bCounter++;
			}
			$json_data .= ']}';
		}
		$json_data .= ']}';

		echo $json_data;
		exit();


		/*echo $json_data = '
		{
		 "name": "Cars",
		 "children": [
		  
		  		  
		  {
		   "name": "analytics",
		   "children": [
			{
			 "name": "cluster",
			 "children": [
			  {"name": "AgglomerativeCluster", "size": 3938},
			  {"name": "CommunityStructure", "size": 3812},
			  {"name": "HierarchicalCluster", "size": 6714},
			  {"name": "MergeEdge", "size": 743},
			  {
			   "name": "analytics_cus",
			   "children": [				
			   {"name": "AgglomerativeCluster", "size": 3938},
			   {"name": "CommunityStructure", "size": 3812},
			   {"name": "HierarchicalCluster", "size": 6714},
			   {"name": "MergeEdge", "size": 743}
			   ]
			  }

			 ]
			},
			{
			 "name": "graph",
			 "children": [
			  {"name": "BetweennessCentrality", "size": 3534},
			  {"name": "LinkDistance", "size": 5731},
			  {"name": "MaxFlowMinCut", "size": 7840},
			  {"name": "ShortestPaths", "size": 5914},
			  {"name": "SpanningTree", "size": 3416}
			 ]
			},
			{
			 "name": "optimization",
			 "children": [
			  {"name": "AspectRatioBanker", "size": 7074}
			 ]
			}
		   ]
		  }


		 ]
		}';*/


		





	}
	
	public function d3_bars_hierarchical_cars3($user_id=0, $date_from="", $date_to="")
	{
		$filterData = array();
		$filterData['date_from'] = $date_from;
		$filterData['date_to'] = $date_to;
		$filterData['user_id'] = $user_id;
		
		$getCarTypes = $this->Model_report->getCarTypes($filterData);
		$carsEachBranch = $this->Model_report->getCarsEachBranch($filterData);
		$gCarTypeEachBranch = $this->Model_report->getGCarTypeEachBranch($filterData);		
		$cars = $getCarTypes[0];
		$branchesSC = $getCarTypes[1];

		$gCarType = $this->Model_report->getGCarType($filterData);
		$cars = array_unique(array_merge($cars, $gCarType[0]));
		$cars = array_values($cars);

		$branches = $gCarType[1];

		foreach($branches as $gid=>$branch)
		{
			if(array_key_exists($gid,$branchesSC))
			{
				//general $branch 
				//SC $branchesSC[$gid];
				$tempGBsArr = explode(",",$branch);
				$tempBsArr = explode(",",$branchesSC[$gid]);

				foreach($tempBsArr as $tempB)
				{
					if(!in_array($tempB, $tempGBsArr))
					{
						$branches[$gid] = $branches[$gid].",".$tempB;
					}
				}

			}

		}


		/*$json_data = '
		{"name": "Cars","children": [';
		for($carCounter=0; $carCounter<count($cars); $carCounter++)
		{			
			$gId = $cars[$carCounter];
			$gCarName = getGeneralVehicleNameById($gId);
			if($gCarName==="N/A") continue;
			if($carCounter>0) $json_data .= ',';
			$json_data .= '{"name": "'.$gCarName.'", ';
			$json_data .= '"children": [';

			$branchesArr = explode(",",$branches[$gId]);
			$branchesArr = array_unique($branchesArr);
			$bCounter = 0;
			foreach($branchesArr as $bId)
			{
				if($bCounter>0) $json_data .= ',';
				$json_data .= '{"name": "'.getBranchName($bId).'", ';// "size": "20"}

				$json_data .= '"children": [';


				$gCars = array();
				$addComma = false;
				if(isset($gCarTypeEachBranch[1][$bId]))
				{
					$gCars = $gCarTypeEachBranch[1][$bId];
					$gCarsArr = explode(",",$gCars);                  //Requests
					$gCarsArr = array_count_values($gCarsArr);										
					if(isset($gCarsArr[$gId]))
					{						
						$json_data .= '{"name": "Requests", "size": '.$gCarsArr[$gId].'}';
						$addComma  = true;
					}

				}

				if(isset($carsEachBranch[1][$bId]))
				{					
					$scheduledArr = explode(",",$carsEachBranch[1][$bId]);                     //Scheduled
					$scheduledArr = array_count_values($scheduledArr);										
					if(isset($scheduledArr[$gId]))
					{
						if($addComma) $json_data .= ',';
						$json_data .= '{"name": "Scheduled", "size": '.$scheduledArr[$gId].'}';		
						$addComma = true;
					}										
				}

				if(isset($carsEachBranch[2][$bId]))
				{						
					$completedArr = explode(",",$carsEachBranch[2][$bId]);                   //Completed
					$completedArr = array_count_values($completedArr);
						if(isset($completedArr[$gId]))
						{
							if($addComma) $json_data .= ',';
							$json_data .= '{"name": "Completed", "size": '.$completedArr[$gId].'}';
						}
				}
				

				$json_data .= ']}';

				$bCounter++;
			}
			$json_data .= ']}';
		}
		$json_data .= ']}';*/
		
		$json_data = '
		{"name": "Cars","children": [';
		for($carCounter=0; $carCounter<count($cars); $carCounter++)
		{			
			$gId = $cars[$carCounter];
			
			if(isset($branches[$gId]))
			{
				
				$gCarName = getGeneralVehicleNameById($gId);
				if($gCarName==="N/A") continue;
				if($carCounter>0) $json_data .= ',';
				$json_data .= '{"name": "'.$gCarName.'", ';
				$json_data .= '"children": [';

				
					
				$branchesArr = explode(",",$branches[$gId]);
				$branchesArr = array_unique($branchesArr);
				$bCounter = 0;
				/*foreach($branchesArr as $bId)
				{
					if($bCounter>0) $json_data .= ',';
					//$json_data .= '{"name": "'.getBranchName($bId).'", ';// "size": "20"}

					//$json_data .= '"children": [';


					$gCars = array();
					$addComma = false;
					if(isset($gCarTypeEachBranch[1][$bId]))
					{
						$gCars = $gCarTypeEachBranch[1][$bId];
						$gCarsArr = explode(",",$gCars);                  //Requests
						$gCarsArr = array_count_values($gCarsArr);										
						if(isset($gCarsArr[$gId]))
						{						
							$json_data .= '{"name": "Requests", "size": '.$gCarsArr[$gId].'}';
							$addComma  = true;
						}

					}

					if(isset($carsEachBranch[1][$bId])) 
					{					
						$scheduledArr = explode(",",$carsEachBranch[1][$bId]);                     //Scheduled
						$scheduledArr = array_count_values($scheduledArr);										
						if(isset($scheduledArr[$gId]))
						{
							if($addComma) $json_data .= ',';
							$json_data .= '{"name": "Scheduled", "size": '.$scheduledArr[$gId].'}';		
							$addComma = true;
						}										
					}

					if(isset($carsEachBranch[2][$bId]))
					{						
						$completedArr = explode(",",$carsEachBranch[2][$bId]);                   //Completed
						$completedArr = array_count_values($completedArr);
							if(isset($completedArr[$gId]))
							{
								if($addComma) $json_data .= ',';
								$json_data .= '{"name": "Completed", "size": '.$completedArr[$gId].'}';
							}
					}
					

					//$json_data .= ']}';

					$bCounter++;
				}*/
				foreach($branchesArr as $bId)
				{
					if($bCounter>0) $json_data .= ',';
					//$json_data .= '{"name": "'.getBranchName($bId).'", ';// "size": "20"}

					//$json_data .= '"children": [';

					$completedCount = 0;
					if(isset($carsEachBranch[2][$bId]))
					{						
						$completedArr = explode(",",$carsEachBranch[2][$bId]);                   //Completed
						$completedArr = array_count_values($completedArr);
							if(isset($completedArr[$gId]))
							{
								$completedCount = $completedArr[$gId];
							}
					}

					$gCars = array();
					$addComma = false;
					if(isset($gCarTypeEachBranch[1][$bId]))
					{
						$gCars = $gCarTypeEachBranch[1][$bId];
						$gCarsArr = explode(",",$gCars);                  //Requests
						$gCarsArr = array_count_values($gCarsArr);										
						if(isset($gCarsArr[$gId]))
						{			
							$notCompleted = $gCarsArr[$gId]-$completedCount;  //Requests not completed					
							$json_data .= '{"name": "Not Completed", "size": '.$notCompleted.'}';
							$addComma  = true;
						}

					}

					if($completedCount)
					{										
						if($addComma) $json_data .= ',';
						$json_data .= '{"name": "Completed", "size": '.$completedCount.'}';
					}
					

					//$json_data .= ']}';

					$bCounter++;
				}
				
				$json_data .= ']}';
			}
		}
		$json_data .= ']}';

		echo $json_data;
		exit();


	

	}
	
}
