<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organization_structurebk extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
    {
        parent::__construct();
		checkAdminSession();
      	$this->load->model('Model_category');
	    $this->load->model('Model_city');
        $this->load->model('Model_user');
        $this->load->model('Model_branch');
        //$res = checkLevels(2);
		//checkAuth($res);
    }
	
	
	
	
	public function index($page = 1){
		
		$data = array();
		$arr_ceo = array();
        $arr_get_branches = array();
		$arr_branch_manage_get = array();
		$data['view'] = 'org/organization_structure';
        $data['system_setting'] = 'active';
		$data['cities'] = $this->Model_city->getAll();
        $data['users'] = $this->Model_user->getAll();
		$arr_ceo['is_ceo'] = 1;
		$data['ceo'] = $this->Model_user->getWithMultipleFields($arr_ceo);
		
		if($data['ceo'])
		{
			// do nothing
		}else
		{
			$data['ceo_not_exist'] = 'No Ceo Exist';
		}	
		$arr_branch_manage_get['is_branch_manager'] = 1;
		$data['managers']  = $this->Model_user->getMultipleRows($arr_branch_manage_get);
		
        $data['count'] = count($this->Model_category->getAll());
        $data['count'] = ($data['count']%4 > 0 ? (($data['count']/4) + 1 ) : ($data['count']/4));
      //  $data['categories'] = $this->Model_category->getCategoryByLimit($page);
        $data['page_number'] = $page;
       /* if(($data['cities']))
        {
           
            $data['city_id'] = ($city_id == '' ?  $data['cities'][0]->id : $city_id );
            
            $data['branches'] = getBranches($data['city_id']);
            
        }
		*/
        $this->load->view('template',$data);		
		
	}
    
    public function orgAction()
    {
        if(isset($_POST['form_type']))
        {
            switch($_POST['form_type'])
            {
                case 'moveuser';
                $move_user = $this->moveUser();
                if($move_user)
                {
                    $data['success'] = 'User move successfully.';
					$data['error'] = 'false';
                    $data['reload'] = 1;
                    echo json_encode($data);
                    exit();
                }
                break;
                case 'addUserToStructure';
                $this->addUserToStructure();
                break;
				case 'removeUser':
				$this->removeUser();
				break;
            }
        }    
        
    }
	
	private function removeUser()
    {
       $data = array();
       $update_by = array();
	  
	   $data['parent_id'] = 0;
       $update_by['id'] = $this->input->post('id');
	   $this->Model_user->update($data,$update_by);
       $success['success'] = 'User remove successfully.';
	   $success['error'] = 'false';
       $success['reload'] = 1;
       echo json_encode($success);
       exit();
    }
    
    private function moveUser()
    {
       $data = array();
       $update_by = array();
	  /* if($this->input->post('is_branch_manager') == 1)
	   {
		   $arr_previous_manager_to_employee_by['id'] = $this->input->post('previous_branch_manger'); 
           $arr_previous['is_branch_manager'] = 0; 
	       $this->Model_user->update($arr_previous,$arr_previous_manager_to_employee_by);
		   
		   
	   }*/
	   $data['parent_id'] = $this->input->post('parent_id');
       
	   $data['is_branch_manager'] = $this->input->post('is_branch_manager'); 
	  // $data['branch_id'] = $this->input->post('branch_id'); 
	   $update_by['id'] = $this->input->post('user_id');
	   
       $this->Model_user->update($data,$update_by);
       return true;
    }
	
    
    private function addUserToStructure()
    {
        $data = array();
		$arr_update = array();
        $get_user = array();
        $get_user_team = array();
        $check = false;
        $check_team = false;
        
       // $branch_id = $this->input->post('branch_id');
      // $category_id = $this->input->post('category_id');
        $user_id = $this->input->post('user_id');
		
      /*  if($category_id == '')
        {
            $data['depart_id'] = $this->input->post('depart_id');
        }else
        { */
           // $data['depart_id'] = $category_id;
            
       // }
       // $data['is_team_lead'] = $this->input->post('is_team_lead');
		$data['parent_id'] = $this->input->post('parent_id');
        $data['is_branch_manager'] = $this->input->post('is_branch_manager');
		//$data['branch_id'] = $branch_id;
       // $data['city_id'] = $this->input->post('city_id');
        $data['updated_at'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->session->userdata['user']['id']; 
		$arr_update['id'] = $this->input->post('user_id');
        
		
        
	   
        /*
        if($this->input->post('is_branch_manager') == 1){
            $get_user['branch_id'] = $this->input->post('branch_id');
            $get_user['is_branch_manager'] = 1;
            $user = $this->Model_user->getWithMultipleFields($get_user); // check if there is any other branch manager 
            if(!empty($user))
            {
                if($this->input->post('user_id') != $user->id)
                {
                    $check = true;
                }
            }
            
        }
        
        
       
        if($check)
        {
            $update_user_by['id'] = $user->id;
            $update_user['is_branch_manager'] = 0;
            $this->Model_user->update($update_user, $update_user_by);
            $success['success'] = 'User has been updated successfully and new branch manager is assign to this branch.';
        }
        
        if($this->input->post('is_team_lead') == 1){
            $get_user_team['branch_id'] = $this->input->post('branch_id');
            $get_user_team['depart_id'] = $this->input->post('depart_id');
            $get_user_team['is_team_lead'] = 1;
            $user_team = $this->Model_user->getWithMultipleFields($get_user_team); // check if there is any other branch manager 
            if(!empty($user_team))
            {
                if($this->input->post('user_id') != $user_team->id)
                {
                    $check_team = true;
                }
            }
            
        }
        
        
       
        if($check_team)
        {
            $update_user_team_by['id'] = $user_team->id;
            $update_user_team['is_team_lead'] = 0;
            $this->Model_user->update($update_user_team, $update_user_team_by);
            $success['success'] = 'User has been updated successfully and new team lead is assign to this category.';
        }
        
        if($check && $check_team)
        {
            $success['success'] = 'User has been updated successfully and new branch manage and team lead is assign.';
        }elseif($check == false && $check_team == false)
        {
            $success['success'] = 'User has been updated successfully .';
        }
        */
		$success['success'] = 'User has been updated successfully .';
       // echo '<pre>';
		//print_r($data);exit();
        $this->Model_user->update($data,$arr_update);
        $arr_cat_update['auto_assignee_id'] = 0;
        $arr_cat_update_by['auto_assignee_id'] = $user_id;
        $this->Model_category->update($arr_cat_update,$arr_cat_update_by);
		$success['error'] = 'false';
        $success['reload'] = 1;
		echo json_encode($success);
		exit;
    } 
	
}
