<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_bk extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
    {
        parent::__construct();
		checkAdminSession();
      	$this->load->model('Model_report');
	    $this->load->model('Model_city');
		$this->load->model('Model_category');
		$this->load->model('Model_user');
        //$res = checkLevels(2);
		//checkAuth($res);
    }
	
	
	
	
	public function index($type = '',$category_id = ''){ //$type is to show graph like weekly monthly and yearly
		
		$data = array();
        $arr_empty = array();// just for not getting recordes according to datewise so passing empty array
        $user_id = $this->session->userdata['user']['id'];
		$user_role = $this->session->userdata['user']['role_id'];
        $user_city = $this->session->userdata['user']['city_id'];
        $user_branch = $this->session->userdata['user']['branch_id'];
		if(isset($_GET['category_id']))
		{
			$type="category";
		}
		$data['view'] = 'report/report';
        $data['report'] = 'active';
		$data['categories'] = $this->Model_category->getAll();
		
        if(rights(1,'write'))
        {
            $user_role = 1; // it is set due to condition in the query just just 1 to get all data 
            
        }else
        {
            $user_role = 1; // its actual value is -1 we are just showing same data to for all users having rights to see reports so we are ignoring this condition we will implement it later.
        }
        if(isset($_GET['user_id']))
		{
			$user_role = -1;
			$user_id = $_GET['user_id'];
		}
        
        if($_POST)
        {
			
            $arr_date['date_to'] = $this->input->post('date_to_1');
            $arr_date['date_from'] = $this->input->post('date_from_1');
            
            $arr_date1['date_to'] = $this->input->post('date_to_2');
            $arr_date1['date_from'] = $this->input->post('date_from_2');
            $data['lead_compare_date'] = date("j M y",strtotime($arr_date['date_to'])).' - '. date("j M y",strtotime($arr_date['date_from']));
    	    $data['leads_compare_with_date'] = date("j M y",strtotime($arr_date1['date_to'])).' - '. date("j M y",strtotime($arr_date1['date_from']));
        	$data['leads_compare'] = $this->Model_report->getAllLeads($arr_date,$user_role,$user_id);
            $data['leads_compare_with'] = $this->Model_report->getAllLeads($arr_date1,$user_role,$user_id);
    	    $data['compare'] = 1;
        }else if($type != '')
		    {
				switch($type)
				{
					case 'Week':
					$day = date('w');
					$arr_date['date_to'] = date('Y-m-d', strtotime('-'.$day.' days'));
					$arr_date['date_from'] = date('Y-m-d', strtotime('+'.(6-$day).' days'));
					$data['leads'] = $this->Model_report->getAllLeads($arr_date,$user_role,$user_id);
					$data['type'] = $type; 
					break;
					case 'Month':
					$arr_date['date_to'] = date('Y-m-01');
					$arr_date['date_from'] = date('Y-m-t');
					$data['leads'] = $this->Model_report->getAllLeads($arr_date,$user_role,$user_id);
					$data['type'] = $type;
					break;
					case 'Year':
					$data['leads'] = $this->Model_report->getAllLeadsOfYear($arr_empty,$user_role,$user_id);
					$data['type'] = $type;
					break;
					case 'category':
					//echo 'test';
					//exit();
					$category_id = $_GET['category_id'];
					$data['category_id'] = $category_id;
					$data['leads'] = $this->Model_report->getAllLeads($arr_empty,$user_role,$user_id,'',$category_id); 
					break;
					
				}
			
			
		}else
		{
			$data['leads'] = $this->Model_report->getAllLeads($arr_empty,$user_role,$user_id,'',$category_id); 
        }
        
        
			
			$data['users'] = $this->Model_user->getAllUsersWithRoles();
			$data['cities'] = $this->Model_city->getAll();
			// $data['pie_leads'] = $this->Model_report->getLeadsByCategories($user_role, $user_id,'', $category_id);
			$data['comp_leads'] = $this->Model_report->getLeadsByComplaint($user_role, $user_id,'', $category_id);
	
			$data['satisfaction'] = $this->Model_report->getSatisfaction($user_role, $user_id,'', $category_id);
			
		   
			
			$data['rate_category'] = $this->Model_report->getLeadsByRateCategories($user_role, $user_id,'', $category_id);
			$data['related_category'] = $this->Model_report->getLeadsByCategory($user_role, $user_id,'', $category_id);
			$data['vehicle_type'] = $this->Model_report->getVehicleType($user_role, $user_id, '', $category_id);
			
			$data['leads_by_city'] = $this->Model_report->getLeadsByCity($user_role, $user_id, '', $category_id);
			$data['leads_by_user'] = $this->Model_report->getLeadsByUser($user_role, $user_id,'', $category_id);
			$data['leads_totals'] = $this->Model_report->totalCountLeads($user_role, $user_id,'', $category_id);
			$data['total_lead_in_the_system'] = $this->Model_report->totalCountLeads('1','-1','','',false)[0]->lead_count;
			
			
			//$data['related_category_dir'] = $this->Model_report->getLeadsByCategoryDir($user_role, $user_id,'', $category_id);
			$data['leads_by_region'] = $this->Model_report->getLeadsByRegion($user_role, $user_city,'', $category_id);
			$data['leads_by_branch'] = $this->Model_report->getLeadsByBranch($user_role, $user_branch,'', $category_id);
			$this->load->view('template',$data);		
		
	}
	
    
    public function reportByBranches(){
		   if($_POST){
			   
			$data = array();
			$arr_empty = array();// just for not getting recordes according to datewise so passing empty array
			$user_id = $this->session->userdata['user']['id'];
			$user_role = $this->session->userdata['user']['role_id'];
			$user_city = $this->session->userdata['user']['city_id'];
			$user_branch = $this->session->userdata['user']['branch_id'];
			$branch_ids = $this->input->post('branch_id');
			$data['view'] = 'report/report';
			$data['report'] = 'active';
			$data['categories'] = $this->Model_category->getAll();
		
        if(rights(1,'write'))
        {
            $user_role = 1; // it is set due to condition in the query just just 1 to get all data 
            
        }else
        {
            $user_role = 1; // its actual value is -1 we are just showing same data to for all users having rights to see reports so we are ignoring this condition we will implement it later.
        }
        
        
       
            $data['leads'] = $this->Model_report->getAllLeads($arr_empty,$user_role,$user_id,$branch_ids); 
       		$data['cities'] = $this->Model_city->getAll();
			// $data['pie_leads'] = $this->Model_report->getLeadsByCategories($user_role, $user_id,$branch_ids);
			$data['comp_leads'] = $this->Model_report->getLeadsByComplaint($user_role, $user_id,$branch_ids);
	
			$data['satisfaction'] = $this->Model_report->getSatisfaction($user_role, $user_id,$branch_ids);
			
		   
			
			$data['rate_category'] = $this->Model_report->getLeadsByRateCategories($user_role, $user_id,$branch_ids);
			$data['related_category'] = $this->Model_report->getLeadsByCategory($user_role, $user_id,$branch_ids);
			$data['vehicle_type'] = $this->Model_report->getVehicleType($user_role, $user_id,$branch_ids);
			
			$data['leads_by_city'] = $this->Model_report->getLeadsByCity($user_role, $user_id,$branch_ids);
			$data['leads_by_user'] = $this->Model_report->getLeadsByUser($user_role, $user_id,$branch_ids);
			$data['leads_totals'] = $this->Model_report->totalCountLeads($user_role, $user_id,$branch_ids);
			$data['total_lead_in_the_system'] = $this->Model_report->totalCountLeads('1','-1','','',false)[0]->lead_count;
			
			//$data['related_category_dir'] = $this->Model_report->getLeadsByCategoryDir($user_role, $user_id,$branch_ids);
			$data['leads_by_region'] = $this->Model_report->getLeadsByRegion($user_role, $user_city,$branch_ids);
			$data['leads_by_branch'] = $this->Model_report->getLeadsByBranch($user_role, $user_branch,$branch_ids);
			$this->load->view('template',$data);		
		
	 }else
		{
			redirect($this->config->item('base_url') . 'report');
		}
	}
	
}
