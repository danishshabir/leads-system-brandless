<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
    {
        parent::__construct();
		checkAdminSession();
      	//$this->load->model('Model_lead');
		$this->load->model('Model_common'); //use this in every controller because it is accessed in header.
	    //$this->load->model('Model_vehicle');
        //$res = checkLevels(2);
		//checkAuth($res);
    }
	
	
	
	
	public function index(){
		
		$data = array();
		$data['view'] = 'logs/log_management';
		//$data['leads'] = $this->Model_lead->getAllSortedBy();
		$this->load->view('template',$data);		
		
	}
	 
	
}
