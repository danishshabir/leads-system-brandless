<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class System_settings extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	  public function __construct()

    {

        parent::__construct();

		checkAdminSession();

      	$this->load->model('Model_email');

		$this->load->model('Model_common'); //use this in every controller because it is accessed in header.

		$this->load->model('Model_category');

		$this->load->model('Model_lead');

		$this->load->model('Model_user');

	    $this->load->model('Model_vehicle');

		$this->load->model('Model_vehicle_specific_name');

		$this->load->model('Model_news');		

		$this->load->model('Model_source');		

		$this->load->model('Model_tag');

		$this->load->model('Model_event');

		$this->load->model('Model_registered_app_devices');

		$this->load->model('Model_branch');

		$this->load->model('Model_city');

		$this->load->model('Model_appointment');

		$this->load->model('Model_appointment_vehicle_type');

		$this->load->model('Model_appointment_vehicle_model');

        //$res = checkLevels(2);

		//checkAuth($res);



		// if( rights(14,'read') )

		// {

		// 	//ok fine

		// }

		// else

		// {

		// 	echo "You don't have permissions to access this page.";

		// 	exit();

		// }

    }

	

	

	

	

	public function index(){

		

		$data = array();

		$data['view'] = 'system/system_settings';

        $data['system_setting'] = 'active';

		$data['emails'] = $this->Model_email->getAll();

		//$data['categories'] = $this->Model_category->getAll();

		$data['categories'] = $this->Model_category->getAllOrderBy(false,"title","asc");

		//$data['vehicles'] = $this->Model_vehicle->getAll();

		$data['vehicles'] = $this->Model_vehicle->getAllOrderBy(false,"name","asc");		



		$data['sources'] = $this->Model_source->getAll();

		$data['events'] = $this->Model_event->getAll();

		$data['registered_app_devices'] = $this->Model_registered_app_devices->getAll();		

		$data['tags'] = $this->Model_tag->getAll();		

		$data['newsArr'] = $this->Model_news->getMultipleRows(array(),false,'desc');

		

		$cities = $this->Model_city->getCities(true);

		for($cct=0; $cct<count($cities); $cct++)

		{

			$cities[$cct]['branches'] = $this->Model_branch->getBranchesByCity($cities[$cct]['id']);

		}

		//$data['branches'] = $this->Model_branch->getAll(true);	

		$data['cities'] = $cities;

		//echo "<pre>";

		//print_r($cities); exit();

		

		//$data['users'] = $this->Model_user->getAll();

		$this->load->view('template',$data);		

		

	}



   public function saveCategory()

	{



		$data = array();

		$data = $this->input->post();

		$this->validation();

		

        $data['created_at'] = date('Y-m-d H:i:s');

		$data['created_by'] = $this->session->userdata['user']['id'];

		



		//add notification

		addNotification("New category created ".$data['title']." (".$data['lead_code'].")"."");



		$category_id = $this->Model_category->save($data);

		if($category_id > 0)

				{

					$data['success'] = 'Category has been created successfully.';

					$data['error'] = 'false';

					$data['reload'] = 1;

					echo json_encode($data);

					exit;

				}else

				{

					$errors['error'] = 'Category not save there is some error.';

					$errors['success'] = 'false';

					echo json_encode($errors);

					exit;

				}



	}



   public function saveNews()

	{



		$data = array();

		$data = $this->input->post();

		

        $data['created_at'] = date('Y-m-d H:i:s');

		$data['created_by'] = $this->session->userdata['user']['id'];

		



		//add notification

		addNotification("News added");



		$this->Model_news->save($data);

		$data['success'] = 'News has been added successfully.';

		$data['error'] = 'false';

		$data['reload'] = 1;

		echo json_encode($data);

		exit;



	}	





	private function validation($color_check = '', $email_subject = '')

	{

		    $errors = array();

			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('title', 'Title', 'required');

            if($color_check == '')

            {

                $this->form_validation->set_rules('color', 'Category Color', 'required|is_unique[categories.color]');

            }

            

			$this->form_validation->set_rules('time_limit', 'Time Limit', 'required');

			/*if($email_subject == '')

			{

				

				$this->form_validation->set_rules('email_subject', 'Email Subject Tags', 'required|is_unique[categories.email_subject]');

			}

			$this->form_validation->set_rules('department_tag', 'Department Type Tags', 'required');

			$this->form_validation->set_rules('lead_type_tag', 'Category Lead Type Tag', 'required');

			$this->form_validation->set_rules('lead_type_name', 'Category Lead Type Tag Name', 'required');*/

			$this->form_validation->set_rules('lead_code', 'Lead Code', 'required');

			

			

			if ($this->form_validation->run() == FALSE)

			{

				$errors['error'] = validation_errors();

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{

				return true;

			}

			

	}

	

	

   public function updateCategory()

	{



		$data = array();

		$data_arr = $this->input->post();

        $color_check = 1;

		$email_subjec_check = 1;

        if($this->input->post('old_color') != $this->input->post('color'))

        {

            $color_check = '';

        }

        if($this->input->post('old_email_subject') != $this->input->post('email_subject'))

        {

            $email_subjec_check = '';

        }

        $this->validation($color_check,$email_subjec_check);

		

        foreach($data_arr as $key =>$value)

        {

            if($key != 'old_color' && $key != 'old_email_subject')

            $data[$key] = $value;

        }

        $arr_update['id'] = $this->input->post("id");



				

		

		$data['updated_at'] = date('Y-m-d H:i:s');

		$data['updated_by'] = $this->session->userdata['user']['id'];

		



		//add notification

		addNotification("Category updated ".$data['title']." (".$data['lead_code'].")"."");



		$this->Model_category->update($data,$arr_update);

		

		$data['success'] = 'Category has been updated successfully.';

		$data['error'] = 'false';

		$data['reset'] = 0;

		echo json_encode($data);

		exit;





	}





	public function updateEmail()

	{



		$data = array();

		$data = $this->input->post();

		$arr_update['id'] = $this->input->post("id");

		$file_name = "";



				

		$data['created_at'] = date('Y-m-d H:i:s');

		$data['updated_at'] = date('Y-m-d H:i:s');

		$data['updated_by'] = $this->session->userdata['user']['id'];



		if($data['id']==="7")

		{		

			 $config['upload_path'] = './uploads/intro_email_attach/';

			 $config['allowed_types'] = '*';

			 $config['overwrite'] = TRUE;

			 $this->load->library('upload', $config);



			 if (!$this->upload->do_upload('user_manual_attachment'))

			 {

				$error = array('error' => $this->upload->display_errors());

				//print_r($error);

			 }

			 else

			 {

				$upload_data = array('upload_data' => $this->upload->data());

				//print_r($upload_data);			

				$file_name = $upload_data['upload_data']['file_name'];

				$data['attachment_name'] = $file_name;				

			 }		 

		}

		

		//add notification

		if($data['id']==="1")

		addNotification("Email body updated for new ticket");

		elseif($data['id']==="2")

		addNotification("Email body updated for employee sends to customer");

		elseif($data['id']==="3")

		addNotification("Email body updated for survey sends to customer");

		elseif($data['id']==="4")

		addNotification("Email body updated for did not get an answer on phone call");

		elseif($data['id']==="5")

		addNotification("Email body updated for call back");

		elseif($data['id']==="6")

		addNotification("Email body updated for test drive schedule");

		elseif($data['id']==="7")

		addNotification("Email body updated for employees introduction email");



		$this->Model_email->update($data,$arr_update);	

		

		if($data['id']==="7")

		{

			redirect('/system_settings');

			exit();

		}

		else

		{		

			$dataN['success'] = 'Email has been updated successfully.';

			$dataN['error'] = 'false';

			$dataN['reset'] = 0;

			echo json_encode($dataN);

			exit;

		}





	}

  

   public function deleteCategory()

   {

	   

		 $data = array();

		 $id = $this->input->post('id');

		 $data['id'] = $id;  

		 $arr_delete['category_id'] = $id;

		 $category_register_with_lead =  $this->Model_lead->getWithMultipleFields($arr_delete);

		 //$arr_delete_user['depart_id'] = $id; //it is category id

		 //$category_register_with_user =  $this->Model_user->getWithMultipleFields($arr_delete_user);

		 if(empty($category_register_with_lead)){



			//add notification

			addNotification("Category deleted");



			 $this->Model_category->delete($data);

			 $data['success'] = 'Category deleted successfully.';

			 $data['error'] = 'false';

			 echo json_encode($data);

			 exit;

		 }else

		 {

			 $data['success'] = 'false';

			 $data['error'] = 'This category is assign to some lead. You can not delete this category.';

	     	 echo json_encode($data);

			 exit();

		 }

		 

	 

	   

   }





   public function deleteNews()

   {

	   

		 $data = array();

		 $delNews['id'] = $this->input->post('id');

		 $this->Model_news->delete($delNews);



		//add notification

		addNotification("News deleted");



		 $data['success'] = 'News deleted successfully.';

		 $data['error'] = 'false';

		 echo json_encode($data);

		 exit;

	   

   }



	public function removeSpecVehicle()

	{

		 $data = array();

		 $data['id'] = $this->input->post('id');

		 $this->Model_vehicle_specific_name->delete($data);

		 exit();

	}

   

    public function saveVehicle()

	{



		$data = array();

		$data_specific = array();

		$data_arr = $this->input->post();

		foreach($data_arr as $key => $value){

			if($key != 'specific_name'){

				$data[$key] = $value;

			}

			

		}

		$data['created_at'] = date('Y-m-d H:i:s');

		$data['created_by'] = $this->session->userdata['user']['id'];

		

		$vehicle_id = $this->Model_vehicle->save($data);



		//add notification

		addNotification("New vehicle added ".$data['name']."");



		$specific_cars = explode(',',$this->input->post('specific_name'));

		foreach($specific_cars as $value){

			if($value != ''){

				$data_specific['name'] = trim($value);

				$data_specific['vehicle_id'] = $vehicle_id;

				$data_specific['created_at'] = date('Y-m-d H:i:s');

				$data_specific['created_by'] = $this->session->userdata['user']['id'];

				$specific_vehicle_id = $this->Model_vehicle_specific_name->save($data_specific);

			}

			

		}

		

		if($vehicle_id > 0)

				{

					$data['success'] = 'Vehicle has been added successfully.';

					$data['error'] = 'false';

					$data['reload'] = 1;

					echo json_encode($data);

					exit;

				}else

				{

					$errors['error'] = 'Vehicle not save there is some error.';

					$errors['success'] = 'false';

					echo json_encode($errors);

					exit;

				}



	}

    public function updateVehicles()

	{



		$data = array();

		$vfn = "";

		$data_specific = array();

		$specific_cars = $this->input->post('specific_name');

		if($specific_cars)

		foreach($specific_cars as $key => $value){

			

			if($value != ''){

				$arr_update['id'] = $key;

				$data_specific['name'] = trim($value);

				//$data_specific['vehicle_id'] = $vehicle_id;

				$data_specific['updated_at'] = date('Y-m-d H:i:s');

				$data_specific['updated_by'] = $this->session->userdata['user']['id'];

				$specific_vehicle_id = $this->Model_vehicle_specific_name->update($data_specific,$arr_update);

			}

			/*else

			{

			    $data['id'] = $key;

				$this->Model_vehicle_specific_name->delete($data);	

			}*/

			

		}

		$specific_cars_new = explode(',',$this->input->post('specific_name_new'));

		$vehicle_id = $this->input->post('vehicle_id');

		if(!empty($specific_cars_new))

		{

			foreach($specific_cars_new as $value){

				if($value != ''){

					$data['name'] = trim($value);

					$data['vehicle_id'] = $vehicle_id;

					$data['created_at'] = date('Y-m-d H:i:s');

					$data['created_by'] = $this->session->userdata['user']['id'];

					$specific_vehicle_id = $this->Model_vehicle_specific_name->save($data);

				}

				

			}

		}

		

		

		//add notification

		addNotification("Vehicle updated");



		$data['success'] = 'Vehicle has been added successfully.';

		$data['error'] = 'false';

		$data['reload'] = 0;

		echo json_encode($data);

		exit;

				

	}

   public function deleteVehicle()

   {

	   

		 $data = array();

		 $data['id'] = $this->input->post('id');

		 $this->Model_vehicle->delete($data);



		 //also delete its specific cars

		 $this->Model_vehicle_specific_name->deleteByGeneralId($data['id']);



		//add notification

		addNotification("Vehicle deleted");



		 $data['success'] = 'Vehicle deleted successfully.';

		 $data['error'] = 'false';

		 echo json_encode($data);

		 exit;

		

		 

	 

	   

   }



   

	public function saveSource()

	{



		$data = array();

		$data = $this->input->post();

		

        //$data['created_at'] = date('Y-m-d H:i:s');

		//$data['created_by'] = $this->session->userdata['user']['id'];

		



		//add notification

		addNotification("Source entry added in settings screen");



		$this->Model_source->save($data);

		$data['success'] = 'Source has been added successfully.';

		$data['error'] = 'false';

		$data['reload'] = 1;

		echo json_encode($data);

		exit;



	}



	public function saveEvent()

	{



		$data = array();

		$data = $this->input->post();

		// print_r($data); exit;

		

        //$data['created_at'] = date('Y-m-d H:i:s');

		//$data['created_by'] = $this->session->userdata['user']['id'];

		



		//add notification

		addNotification("Event entry added in settings screen");



		$this->Model_event->save($data);

		$data['success'] = 'Event has been added successfully.';

		$data['error'] = 'false';

		$data['reload'] = 1;

		echo json_encode($data);

		exit;



	}



	public function saveMobileApp()

	{



		$data = array();

		$data = $this->input->post();

		

        //$data['created_at'] = date('Y-m-d H:i:s');

		//$data['created_by'] = $this->session->userdata['user']['id'];

		



	

		$checkCodeDup = $this->Model_registered_app_devices->getWithMultipleFields(array('code'=>$data['code']));



		if($checkCodeDup)

		{			

			$data['error'] = "Sorry this code is arleady used in other app.";

			//$errors['success'] = 'false';

			$data['reload'] = 0;

			echo json_encode($data);

			exit;

		}

		else

		{

			//add notification

			addNotification("Mobile app device entry added in settings screen");

			

						

			$data['branches_id'] = implode(",",$this->input->post('branches_id'));

			$data['vehicles_id'] = implode(",",$this->input->post('vehicles_id'));

			$data['categories_id'] = implode(",",$this->input->post('categories_id'));

		

	

			$this->Model_registered_app_devices->save($data);

			$data['success'] = 'Mobile app device has been added successfully.';

			$data['error'] = 'false';

			$data['reload'] = 1;

			echo json_encode($data);

			exit;

		}



	}



	public function updateSource()

	{



		$data = array();

		$data = $this->input->post();

		$updateBy['id'] = $this->input->post("id");



		$checkCodeDup = $this->Model_registered_app_devices->getWithMultipleFields(array('code'=>$data['code']));

		if($checkCodeDup)

		{			

			$data['error'] = "Sorry this code is arleady used in other app.";

			//$errors['success'] = 'false';

			$data['reload'] = 0;

			echo json_encode($data);

			exit;

		}

		else

		{

			$this->Model_source->update($data,$updateBy);

			addNotification("Source entry updated in settings screen");

			

			$data['success'] = 'Source has been updated successfully.';

			$data['error'] = 'false';

			$data['reset'] = 0;

			echo json_encode($data);

			exit;

		}





	}

//here kashif 

	public function updateMobileApp()

	{



		$data = array();

		$data = $this->input->post();

		$updateBy['id'] = $this->input->post("id");

		

		$data['branches_id'] = implode(",",$this->input->post('branches_id'));

		$data['vehicles_id'] = implode(",",$this->input->post('vehicles_id'));

		$data['categories_id'] = implode(",",$this->input->post('categories_id'));



			

		$this->Model_registered_app_devices->update($data,$updateBy);

		addNotification("Mobile app device entry updated in settings screen");

		

		$this->sendFCMPushApple($updateBy['id']); //send push notification for new settings



		$data = array(); //reset the data array

		$data['success'] = 'Mobile app device settings has been updated successfully. Please check notification in iPad.';

		$data['error'] = 'false';

		$data['reset'] = 0;

		echo json_encode($data);

		exit;





	}



	private function sendFCMPushApple($id)

	{



		$appleDevice = $this->Model_registered_app_devices->get($id); //0 for apple

			

		//comma separated ids

		$categoriesResArr = $this->Model_category->getMultipleCategories($appleDevice->categories_id);

		

		$vehiclesResArr = $this->Model_category->getMultipleVehicles($appleDevice->vehicles_id);

		

		$branchesResArr = $this->Model_category->getMultipleBranches($appleDevice->branches_id);

		

		//print_r($categoriesResArr); exit;

		

		$url = 'https://fcm.googleapis.com/fcm/send';

		

		if($appleDevice->code_connected && $appleDevice->token!="")

		{

			$fields = array 

			(

			'registration_ids' => array (

				$appleDevice->token

			),

			"content_available" => true,

			"priority" => "high",		

			"notification" => array

				(				

					"title" => "Leads System Inquiry (New Settings Received)",

					"body" => "Click here to apply new settings",				

					"sound" => "default"

				),

			"data" => array

				(

					"source_id" => $appleDevice->source_id,

					"event_id" => $appleDevice->event_id

					//"categories" => $categoriesResArr,

					//"vehicles" => $vehiclesResArr,

					//"branches" => $branchesResArr

					

				)

			);

			

			

			$fields = json_encode ( $fields , JSON_UNESCAPED_UNICODE);

			

			//print_r($fields); exit;



			$headers = array (

				'Authorization: key=' . "AIzaSyAmA7k1qI4ticnxycrjoR2u8H1IXwcIRk4", //this key is for Firebase google messaging fcm app using app2@astutesol.com

				'Content-Type: application/json'

			);



			$ch = curl_init ();

			curl_setopt ( $ch, CURLOPT_URL, $url );

			curl_setopt ( $ch, CURLOPT_POST, true );

			curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );

			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );

			curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );



			$result = curl_exec ( $ch );

			//echo $result.'<br>';

			curl_close ( $ch );	

		}

		



	}



	public function updateEvent()

	{



		$data = array();

		$data = $this->input->post();

		

		if(!isset($data['is_active'])) $data['is_active'] = 0;			

		

		$updateBy['id'] = $this->input->post("id");



		$this->Model_event->update($data,$updateBy);

		addNotification("Event entry updated in settings screen");

		

		$data['success'] = 'Event has been updated successfully.';

		$data['error'] = 'false';

		$data['reset'] = 0;

		echo json_encode($data);

		exit;





	}



   public function deleteSource()

   {

	   

		$data = array();

		$id = $this->input->post('id');

		$data['id'] = $id;  



		$this->Model_source->delete($data);

		//add notification

		addNotification("Source entry deleted from settings screen");



		$data['success'] = 'Source deleted successfully.';

		$data['error'] = 'false';

		echo json_encode($data);

		exit;

   }



   public function deleteMobileApp()

   {

	   

		$data = array();

		$id = $this->input->post('id');

		$data['id'] = $id;  



		$this->Model_registered_app_devices->delete($data);

		//add notification

		addNotification("Mobile app device entry deleted from settings screen");



		$data['success'] = 'Mobile app deleted successfully.';

		$data['error'] = 'false';

		echo json_encode($data);

		exit;

   }



   public function deleteEvent()

   {

	   

		$data = array();

		$id = $this->input->post('id');

		$data['id'] = $id;  



		$this->Model_event->delete($data);

		//add notification

		addNotification("Event entry deleted from settings screen");



		$data['success'] = 'Event deleted successfully.';

		$data['error'] = 'false';

		echo json_encode($data);

		exit;

   }





   	public function saveTag()

	{



		$data = array();

		$data = $this->input->post();

		

        //$data['created_at'] = date('Y-m-d H:i:s');

		//$data['created_by'] = $this->session->userdata['user']['id'];

		



		//add notification

		addNotification("Tag entry added in settings screen");



		$this->Model_tag->save($data);

		$data['success'] = 'Tag has been added successfully.';

		$data['error'] = 'false';

		$data['reload'] = 1;

		echo json_encode($data);

		exit;



	}



	public function updateTag()

	{



		$data = array();

		$data = $this->input->post();

		$updateBy['id'] = $this->input->post("id");



		$this->Model_tag->update($data,$updateBy);

		addNotification("Tag entry updated in settings screen");

		

		$data['success'] = 'Tag has been updated successfully.';

		$data['error'] = 'false';

		$data['reset'] = 0;

		echo json_encode($data);

		exit;





	}



   public function deleteTag()

   {

	   

		$data = array();

		$id = $this->input->post('id');

		$data['id'] = $id;  



		$this->Model_tag->delete($data);

		//add notification

		addNotification("Tag entry deleted from settings screen");



		$data['success'] = 'Tag deleted successfully.';

		$data['error'] = 'false';

		echo json_encode($data);

		exit;

   }



   public function list_appointment_vehicle_type()

   {

	   

		$appointment_vehicles = $this->Model_appointment_vehicle_type->getAll(true);

				

		$rows = $appointment_vehicles;



		//Return result to jTable

		$jTableResult = array();

		$jTableResult['Result'] = "OK";

		$jTableResult['Records'] = $rows;

		print json_encode($jTableResult);

		

		

   }

   

   public function insert_appointment_vehicle_type()

   {

	   $data = $this->input->post();

	   

	   $id = $this->Model_appointment_vehicle_type->save($data);

				

	   $rows = $this->Model_appointment_vehicle_type->get($id,true);



		//Return result to jTable

		$jTableResult = array();

		$jTableResult['Result'] = "OK";

		$jTableResult['Record'] = $rows;

		print json_encode($jTableResult);

		

		

   }

   

   public function update_appointment_vehicle_type()

   {

	   

	    $data = $this->input->post();

		$this->Model_appointment_vehicle_type->update($data,array('id'=>$this->input->post("id")));		



		//Return result to jTable

		$jTableResult = array();

		$jTableResult['Result'] = "OK";

		print json_encode($jTableResult);

		

		

   }

   

   public function delete_appointment_vehicle_type()

   {

	   

		$data = $this->input->post();

		$this->Model_appointment_vehicle_type->delete(array('id'=>$this->input->post("id")));



		//Return result to jTable

		$jTableResult = array();

		$jTableResult['Result'] = "OK";

		print json_encode($jTableResult);

		

		

   } 

   

   public function list_appointment_vehicle_model()

   {

	   

	   $data = $this->input->get();

		$appointment_vehicles = $this->Model_appointment_vehicle_model->getMultipleRows(array("appnt_veh_type_id"=>$data['appnt_veh_type_id']));

				

		$rows = $appointment_vehicles;



		//Return result to jTable

		$jTableResult = array();

		$jTableResult['Result'] = "OK";

		$jTableResult['Records'] = $rows;

		print json_encode($jTableResult);

		

		

   }

   

   public function insert_appointment_vehicle_model()

   {

	   $data = $this->input->post();

	   

	   $id = $this->Model_appointment_vehicle_model->save($data);

				

	   $rows = $this->Model_appointment_vehicle_model->get($id,true);



		//Return result to jTable

		$jTableResult = array();

		$jTableResult['Result'] = "OK";

		$jTableResult['Record'] = $rows;

		print json_encode($jTableResult);

		

		

   }

   

   public function update_appointment_vehicle_model()

   {

	   

	    $data = $this->input->post();

		$this->Model_appointment_vehicle_model->update($data,array('id'=>$this->input->post("id")));		



		//Return result to jTable

		$jTableResult = array();

		$jTableResult['Result'] = "OK";

		print json_encode($jTableResult);

		

		

   }

   

   public function delete_appointment_vehicle_model()

   {

	   

		$data = $this->input->post();

		$this->Model_appointment_vehicle_model->delete(array('id'=>$this->input->post("id")));



		//Return result to jTable

		$jTableResult = array();

		$jTableResult['Result'] = "OK";

		print json_encode($jTableResult);

		

		

   }

	

}

