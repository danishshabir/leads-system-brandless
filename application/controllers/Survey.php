<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Survey extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
    {
        parent::__construct();
		checkAdminSession();
        $this->load->model('Model_survey');
	    $this->load->model('Model_category');
		$this->load->model('Model_question');
        $this->load->model('Model_survey_result');
        $this->load->model('Model_lead');
		$this->load->model('Model_common'); //use this in every controller because it is accessed in header.
        	  
        //$res = checkLevels(2);
		//checkAuth($res);
    }
	
	
	
	
	public function index(){
		$data = array();
		$data['view'] = 'survey/manage';
        $data['system_setting'] = 'active';
		$data['surveys'] = $this->Model_survey->getAll();
		//$data['categories'] = $this->Model_category->getAll();
		$this->load->view('template',$data);
		
		
		}
	public function surveyAction()
	{
		$data = array();
		$data['view'] = 'survey/create_survey';
		
		if(isset($_POST['form_type'])){
			switch($_POST['form_type']){
				case 'save':
				$this->validation();
				$data['inser_survey_id'] = $this->save();
				if($data['inser_survey_id'] > 0)
				{
					$data['success'] = 'Survey has been created successfully.';
					$data['error'] = 'false';
					$data['reset'] = 1;
					echo json_encode($data);
					exit;
				}
				break;
				case 'delete':
				$this->deleteQuestion();
				
				
				break;
				case 'update':
				$data['user_update'] = $this->update();
				$this->validation();
				if($data['user_update'] == true)
				{
					$data['success'] = 'Survey has been updated successfully.';
					$data['error'] = 'false';
                    $data['reload'] = 1;
					echo json_encode($data);
					exit;
				}
				break;
				
			}
		}
		$data['surveys'] = $this->Model_survey->getAll();
		$data['categories'] = $this->Model_category->getAll();
		$this->load->view('template',$data);
	}
	
	private function save()
	{
		$data = array();
		$data['title'] = $this->input->post('title');
		$data['category_id']  = $this->input->post('category_id');
        $data['is_active']  = $this->input->post('is_active');
		if($data['is_active'] == 1)
        {
		    $this->disActiveOtherSurveyAttachToThisCategory($data['category_id']);
		}
		$data['description'] = $this->input->post('description');
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->session->userdata['user']['id'];
		
		$survey_id = $this->Model_survey->save($data);
		$data = array(); // array again define to store new data passing to question save function
		$questions = $this->input->post('question');
		$questions_ar = $this->input->post('question_ar');
        $answers = $this->input->post('answer');
		for($i=0; $i<count($questions); $i++)
		{
			if($questions[$i] != '')
			{
				$data['survey_id'] = $survey_id;
                $data['question'] = $questions[$i];
				$data['question_ar'] = $questions_ar[$i];
				$data['answer'] = $answers[$i];
                /*switch($data['answer'])
                {
                    case 'Rating' :
                    $data['order_by'] = 1;
                    break;
                    case 'Yes or No' :
                    $data['order_by'] = 2;
                    break;
                    case 'Textarea' :
                    $data['order_by'] = 3;
                    break;

                }*/
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['created_by'] = $this->session->userdata['user']['id'];
				$question_id = $this->Model_question->save($data);				
			}

		}

	    //add notification
		addNotification("New survey added ".$this->input->post('title')."");

		return $survey_id;
		
	}
	private function update()
    {
        $data = array();
        $arr_update = array();
		$data['title'] = $this->input->post('title');
		$data['category_id']  = $this->input->post('category_id');
        $data['is_active']  = $this->input->post('is_active');
		if($data['is_active'] == 1)
        {
		    $this->disActiveOtherSurveyAttachToThisCategory($data['category_id']);
		}
        $data['description'] = $this->input->post('description');
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->session->userdata['user']['id'];
		$arr_update['id'] = $this->input->post('survey_id'); 
		$this->Model_survey->update($data,$arr_update);
		$data = array(); // array again define to store new data passing to question save function
		$questions = $this->input->post('question');
		$questions_ar = $this->input->post('question_ar');
        $answers = $this->input->post('answer');
	
		if(!empty($questions))
		{
			foreach($questions as $key =>$question)
			{
				$data = array();			
				$data['question'] = $question;
				$data['answer'] = $answers[$key];
				$data['updated_at'] = date('Y-m-d H:i:s');
				$data['updated_by'] = $this->session->userdata['user']['id'];
                $arr_update['id'] = $key;
				$this->Model_question->update($data,$arr_update);			

			}
		}

		if(!empty($questions_ar))
		{
			foreach($questions_ar as $key =>$question_ar)
			{
				$data = array();
				$data['question_ar'] = $question_ar;
                $arr_update['id'] = $key;
				$this->Model_question->update($data,$arr_update);
			}
		}
		
        
        $addquestions = $this->input->post('questionadd');
		$addquestions_ar = $this->input->post('questionadd_ar');
        $addanswers = $this->input->post('answeradd');
        if(!empty($addquestions))
        {
            //$i=0;
            //foreach($addquestions as $question)
			for($i=0; $i<count($addquestions); $i++)
    		{
    		     
    			if($addquestions[$i] != '')
    			{
    				$data['survey_id'] = $this->input->post('survey_id');
                    $data['question'] = $addquestions[$i];
					$data['question_ar'] = $addquestions_ar[$i];
    				$data['answer'] = $addanswers[$i];
                    /*switch($data['answer'])
                    {
                        case 'Rating' :
                        $data['order_by'] = 1;
                        break;
                        case 'Yes or No' :
                        $data['order_by'] = 2;
                        break;
                        case 'Textarea' :
                        $data['order_by'] = 3;
                        break;
    
                    }*/
    				$data['created_at'] = date('Y-m-d H:i:s');
    				$data['created_by'] = $this->session->userdata['user']['id'];
    				$question_id = $this->Model_question->save($data);
    				//$i++;
    			}
    
    		}
            //$i++;
            
        }
        

		//add notification
		addNotification("Survey updated ".$this->input->post('title')."");
        
		return true;
    }
    
    private function disActiveOtherSurveyAttachToThisCategory($category_id)
    {
        $data['is_active'] = 0 ;
        $arr_update['is_active'] = 1;
        $arr_update['category_id'] = $category_id; 
        $this->Model_survey->update($data,$arr_update);
        return true;
    }
    
	private function validation()
	{
		    $errors = array();
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            
			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('category_id', 'Category', 'required');
			//$this->form_validation->set_rules('description', 'Description', 'required');
			if ($this->form_validation->run() == FALSE)
			{
				$errors['error'] = validation_errors();
				$errors['success'] = 'false';
				echo json_encode($errors);
				exit;
			}else{
				return true;
			}
			
	}
	
	
	public function view($survey_id){
		$data = array();
		$arr_get = array();
		$data['view'] = 'survey/view';
		$data['survey'] = $this->Model_survey->get($survey_id);
		$arr_get['survey_id'] = $survey_id;
        $data['categories'] = $this->Model_category->getAll();
		$data['questions'] = $this->Model_question->getMultipleRowsOrderby($survey_id);
		//$data['sections'] = $this->Model_section->getAll();
        $data['survey_id'] = $survey_id;
		$this->load->view('template',$data);
		
		
		}
	public function edit($survey_id){
		$data = array();
		$arr_get = array();
		$data['view'] = 'survey/edit';
		$data['survey'] = $this->Model_survey->get($survey_id);
		$arr_get['survey_id'] = $survey_id;
        $data['categories'] = $this->Model_category->getAll();
		$data['questions'] = $this->Model_question->getMultipleRows($arr_get);
		//$data['sections'] = $this->Model_section->getAll();
		$this->load->view('template',$data);
		
		
		}
	public function response($survey_id,$cat_id){
		$data = array();
		$data['view'] = 'survey/response';
        $data['survey'] = $this->Model_survey->get($survey_id);
		$data['results'] = $this->Model_survey_result->getSurveyResult($survey_id); 
        
        $avg = $this->Model_survey_result->getTotalAvg($survey_id);
        $data['total_avg'] = $avg[0]->average;
        if(!empty($avg))
        {
           $data['total_avg'] = round($data['total_avg'], 1);  
        }
        $other_cat_response_avg = $this->Model_survey_result->getTotalAvgCat($survey_id,$cat_id);
       $data['total_avg_cat'] = $other_cat_response_avg[0]->average;
        if(!empty($other_cat_response_avg))
        {
           $data['total_avg_cat'] = round($data['total_avg_cat'], 1);  
        }
		$this->load->view('template',$data);
		
		
	}

    public function leadSurveyResponse($survey_id,$lead_id)
    {
		$data = array();
		$data['view'] = 'survey/lead_response';
		$data['questions'] = $this->Model_survey_result->getleadSurveyResponse($survey_id,$lead_id); 
        $data['lead'] = $this->Model_lead->get($lead_id);
        $avg = $this->Model_survey_result->getTotalAvg($survey_id);
        $data['total_avg'] = $avg[0]->average; // this total avg to this only of survey
        if(!empty($avg))
        {
           $data['total_avg'] = round($data['total_avg'], 1);  
        }
        $avg_by_lead = $this->Model_survey_result->getTotalAvgLead($survey_id,$lead_id);
        $data['total_avg_by_lead'] = $avg_by_lead[0]->average; // this total avg to this only of survey
        if(!empty($avg_by_lead))
        {
           $data['total_avg_by_lead'] = round($data['total_avg_by_lead'], 1);  
        }
        $this->load->view('template',$data);
		
		
	}
    private function deleteQuestion()
   {
	   
		 $data = array();
		 $data['id'] = $this->input->post('id');
		 $this->Model_question->delete($data);
		 $data['success'] = 'Question deleted successfully.';
		 $data['error'] = 'false';
		 echo json_encode($data);
		 exit;
    }
    
     public function deleteSurvey()
   {
	   
		 $data = array();
		 $data['id'] = $this->input->post('id');
         $arr_data['survey_id'] = $data['id'];
         $check = $this->Model_survey_result->getWithMultipleFields($arr_data);
         if($check)
         {
             
             $data['error'] = 'You cannot delete this survey.Please use archive option.';
    		 $data['success'] = 'false';
    		 echo json_encode($data);
		     exit;
         }else
         {
             $this->Model_survey->delete($data);
             $arr_question['survey_id'] = $data['id'];
             $this->Model_question->delete($data);
    		 $data['success'] = 'Survey and its questions deleted successfully.';
    		 $data['error'] = 'false';
    		 echo json_encode($data);
		     exit;
         }
		
    		//add notification
		addNotification("Survey deleted");     
 
    }
    
         public function archiveSurvey()
   {
	   
		 $data = array();
		 $data['id'] = $this->input->post('id');
         $arr_data['survey_id'] = $data['id'];
         
         $update_data['is_archive'] = 1;
         $update_data['is_active'] = 0;

		//add notification
		addNotification("Survey archived");

         $this->Model_survey->update($update_data,$data);
         $data['success'] = 'Survey archived.';
    	 $data['error'] = 'false';
   		 echo json_encode($data);
         exit;
         
         
 
    }
    	
		
	
}
