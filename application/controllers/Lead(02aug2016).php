<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lead extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 private $allChild = array();

	  public function __construct()
    {
        parent::__construct();
		checkAdminSession();
      	$this->load->model('Model_lead');
	    $this->load->model('Model_vehicle');
		$this->load->model('Model_city');
		$this->load->model('Model_branch');
		$this->load->model('Model_category');
		$this->load->model('Model_leads_messages');
		$this->load->model('Model_messages');
		$this->load->model('Model_survey');
		$this->load->model('Model_notification');
		$this->load->model('Model_user');
		$this->load->model('Model_lead_tags');
		$this->load->model('Model_vehicle_specific_name');
		$this->load->model('Model_struc_dep_users');		
		$this->load->model('Model_structure_departments');
		$this->load->model('Model_source');
		$this->load->model('Model_tag');

		if( rights(1,'read') )
		{
			//ok fine
		}
		else
		{
			echo "You don't have permissions to access this page.";
			exit();
		}
    }
	
	
	

	/*public function searchLeads(){

		$data = array();	
		
		$users = $this->usersUnderMngRecursive();

		$data['leads'] = $this->Model_lead->searchLeads($this->input->get('search_keyword'), $users);

		$data['view'] = 'lead/leads';
        $data['lead_class'] = 'active';
		$data['isSingleDetailLead'] = false;
		$data['cities'] = $this->Model_city->getAll();
		$data['logged_in_user_id'] = $this->session->userdata['user']['id'];
		$data['categories'] = $this->Model_category->getAll();
		$data['tags'] = $this->Model_tag->getAll();
		$data['dashboard'] = false;
		$data['searchedOrFiltered'] = true;
		$data['vehicles_specific'] = $this->Model_vehicle_specific_name->getAll(); 
		$this->load->view('template',$data);
		
		
		}*/


	public function leadAction()
	{
		$data = array();

		if(isset($_POST['form_type']))
		{

			switch($_POST['form_type'])
			{

				case 'save':

					//$this->checkLicense();

					$this->validation();
					$data['trackid'] = $this->save($this->input->post());
					if($data['trackid'])
					{
						$data['success'] = 'Track ID: '.$data['trackid'].' Lead has been created successfully.';
						$data['error'] = 'false';
						$data['reset'] = 0;
						echo json_encode($data);
						exit;
					}
				break;

				case 'delete':
					
					$this->deleteLeads();
					$data['success'] = 'Leads deleted successfully.';
					$data['error'] = 'false';
					echo json_encode($data);
					exit;

				break;

				case 'update':

					//$this->checkLicense();

					$this->validation();
					$this->update($this->input->post());

					$data['success'] = 'Lead has been updated successfully.';
					$data['error'] = 'false';
					echo json_encode($data);
					exit;					

				break;			

				case 'action_added':

					$this->addLeadMessage();

				break;
			
				case 'admin_assign_lead_action':

					$this->adminAssignLeadAction();

				break;					

				case 'check_leads_same_city_branch':

					$this->checkLeadsSameCityBranch();

				break;				
				

				
			}
		}
		
	}
	
	private function usersUnderMngRecursive() //gives comma sep users under manager.
	{
		$users = array(); //All the users under this manager.

		$loggedInUserManagingDeps = getLoggedInUserManagingDeps();		
		if(is_array($loggedInUserManagingDeps) && count($loggedInUserManagingDeps)>0)
		{
			foreach($loggedInUserManagingDeps as $dep)
			{
				$this->allChild = array();
				$this->allChild[] = $dep->id; //also include itself
				$this->getStrucChildren($dep->id); //it will update an object level array varible with the child dep ids				
				$allChildArr = $this->allChild;
				if($allChildArr)
				{
					foreach($allChildArr as $chDepId)
					{
						$usersUnderMngr = getUsersUnderMngr($chDepId);
						
						if($usersUnderMngr)
						foreach($usersUnderMngr as $depUser)
						{
							//if($users!="") $users .= ",";
							if(!in_array($depUser->user_id,$users))
							$users[] = $depUser->user_id;
						}
					}
				}
			}		
		}

		//print_r($users); exit();
		$users = implode(",",$users);

		return $users;
	}

	//current logic is that the sub manager can see all the leads as the manager. Even the further nested department leads.
	private function usersUnderSubmngRecursive() //gives comma sep users under manager.
	{
		$users = array(); //all other users in his departments.
		$this->allChild = array(); //reset object level array

		if( rights(33,'read') ) //lead controller e.g sub manager.
		{

			$wdu = array();
			$wdu['user_id'] = $this->session->userdata['user']['id'];
			$thisUserDeps = $this->Model_struc_dep_users->getMultipleRows($wdu);			

			if(is_array($thisUserDeps) && count($thisUserDeps)>0)
			{
				foreach($thisUserDeps as $dep)
				{
					$this->allChild = array();
					$this->allChild[] = $dep->struc_dep_id; //also include itself
					$this->getStrucChildren($dep->struc_dep_id); //it will update an object level array varible with the child dep ids				
					$allChildArr = $this->allChild;
					if($allChildArr)
					{
						foreach($allChildArr as $chDepId)
						{
							$usersUnderMngr = getUsersUnderMngr($chDepId);
							
							if($usersUnderMngr)
							foreach($usersUnderMngr as $depUser)
							{
								//if($users!="") $users .= ",";
								if(!in_array($depUser->user_id,$users))
								$users[] = $depUser->user_id;
							}
						}
					}
				}		
			}

		}

		//print_r($users); exit();
		$users = implode(",",$users);

		return $users;
	}

	
	//$filter=false, $category_id=0, $status="" these paramerts are for filtering.
	//this function is used when leads listing page is 1. open by default. 2. searching 3. filter
	public function index($applyFilter="", $category_id=0, $status=""){ 

		$data = array();
		$data['view'] = 'lead/leads';
        $data['lead_class'] = 'active';
		if($applyFilter=="filter") $applyFilter=true; else $applyFilter=false;	
		$status = str_replace("%20"," ",$status);
		
		//$data['leads'] = $this->Model_lead->getAllSortedBy();
		$pageNumber = 1;
		if($this->input->get('page')>1)
			$pageNumber = $this->input->get('page');


		$users = $this->usersUnderMngRecursive();

		if(!$users)
		{
			//if no users are returned then its mean logged in user is not a manager. Now check if the logged in user is a lead controller?
			if( rights(33,'read') ) //33 is lead controller
			{
				//logged in user is a submanager now get all the users of his departments.
				$users = $this->usersUnderSubmngRecursive();

			}
		}

		$checkLoggedInUserInboxRole = checkLoggedInUserInboxRole();

		$keyWord = $this->input->get('search_keyword');

		$data['leads'] = $this->Model_lead->getAllSortedByPaged($pageNumber,0,1,$users,$keyWord,$checkLoggedInUserInboxRole,$applyFilter,$category_id,$status);

		$data['isSingleDetailLead'] = false;
		$data['cities'] = $this->Model_city->getAll();
		$data['logged_in_user_id'] = $this->session->userdata['user']['id'];
		//$data['categories'] = $this->Model_category->getAll();
		$data['categories'] = $this->Model_category->getAllOrderBy(false,"title","asc");		
        $data['vehicles_specific'] = $this->Model_vehicle_specific_name->getAll(); 
		$data['tags'] = $this->Model_tag->getAll();

		$data['loadmore'] = true;
		
		//show load more if needed
		$data['loadMoreNeeded'] = false;
		//if(count($this->Model_lead->getAllSortedByPaged($pageNumber,0,0,$users,$keyWord,$checkLoggedInUserInboxRole,$applyFilter,$category_id,$status)) > 10)		
		if(is_array($data['leads']) && count($data['leads'])===10)
		{
			$data['loadMoreNeeded'] = true;
		}

		$data['totalLeads'] = count($this->Model_lead->getAllSortedByPaged($pageNumber,0,0,$users,$keyWord,$checkLoggedInUserInboxRole,false,0,""));
		$data['assignedLeads'] = count($this->Model_lead->getAllSortedByPaged($pageNumber,0,0,$users,$keyWord,$checkLoggedInUserInboxRole,true,"All","Assigned Plus"));
			
		
		$data['loadonlyleads'] = false;		

		if($pageNumber>1 or $this->input->get('sortType')!='')
		{
			$data['loadonlyleads'] = true;
		}

		$data['dashboard'] = false;
		$data['searchedOrFiltered'] = false;


		$this->load->view('template',$data);		
		
		}
		
	/*public function filter($category_id, $status){
				
		$where = array();
		$data = array();
		$applyFilter = true;
		
		$status = str_replace("%20"," ",$status);
		
		$this->index($applyFilter, $category_id, $status);

		$users = $this->usersUnderMngRecursive();

		$data['leads'] = $this->Model_lead->getFilteredLeads($where, $users);		

		$data['view'] = 'lead/leads';
        $data['lead_class'] = 'active';		
		$data['isSingleDetailLead'] = false;
		$data['cities'] = $this->Model_city->getAll();
		$data['logged_in_user_id'] = $this->session->userdata['user']['id'];
		$data['categories'] = $this->Model_category->getAll();
		$data['dashboard'] = false;
		$data['searchedOrFiltered'] = true;
		$data['vehicles_specific'] = $this->Model_vehicle_specific_name->getAll(); 
		$data['tags'] = $this->Model_tag->getAll();
		$this->load->view('template',$data);
		
		
	}	*/	
	
	/*public function getEmployeeByBranchAndCategoryAjax()
	{
		
		$employees = getEmployeeByCategory($this->input->post('category_id'),$this->input->post('branch_id')); 
		$options = "";
		if(is_array($employees))
		{
			foreach($employees as $employee){				
				$options .= '<option value="'.$employee->id.'">'.$employee->full_name.'</option>';
			}
			
			echo $options;
			exit;
		}
	}*/

	public function createNewLead()
	{

		$data = array();
		$data['vehicles'] = $this->Model_vehicle->getAllSorted(); 
		$data['vehicles_specific'] = $this->Model_vehicle_specific_name->getAll(); 
		$data['cities'] = $this->Model_city->getAll();
		$data['sources'] = $this->Model_source->getAll();		
		$data['tags'] = $this->Model_tag->getAll();
		$data['view'] = 'lead/create_edit_lead'; //same view is used for insert or update both
		$data['categories'] = $this->Model_category->getAll();
		$data['edit'] = false;
        $data['lead_class'] = 'active';
		$this->load->view('template',$data);

	}

	public function editLead($id)
	{

		$data = array();
		$data = $this->Model_lead->get($id,true);		
		$data['vehicles'] = $this->Model_vehicle->getAllSorted(); 
		$data['vehicles_specific'] = $this->Model_vehicle_specific_name->getAll(); 
		$data['cities'] = $this->Model_city->getAll();
		$data['sources'] = $this->Model_source->getAll();		
		$data['tags'] = $this->Model_tag->getAll();
		$data['view'] = 'lead/create_edit_lead'; //same view is used for insert or update both
		$data['categories'] = $this->Model_category->getAll();
		$data['edit'] = true;
        $data['lead_class'] = 'active';
		$this->load->view('template',$data);

	}


	private function checkLicense()
	{
		//Please note, test drive cannot be booked without a valid driving license
			$preferred_date = $this->input->post('preferred_date');
			//Check if Valid Driving License = Yes
			$ValidDrivingLicenseYes = 0;
			if($this->input->post('valid_driving_license')==='1')
			{
				$ValidDrivingLicenseYes = $this->input->post('valid_driving_license');
			}

			if($preferred_date!='')
			{
				if(!$ValidDrivingLicenseYes)
				{
					$data['success'] = 'false';
					$data['error'] = 'Test drive cannot be booked without a valid driving license.';
					$data['reset'] = 0;
					$data['reload'] = 0;
					echo json_encode($data);
					exit;

				}

			}
	}

	private function updateLateLeadNotifination()
	{
		$ids = $this->input->post('ids');
		foreach($ids as $id)
		{
			$dataLead['late_lead_notication_read'] = 1;
			$where['id'] = $id;
			$this->Model_lead->update($dataLead,$where);
		}
		return true;
	}
	
	private function adminAssignLeadAction()
	{
		$data = array();
		$ids = $this->input->post('lead_id'); //this lead_id can contains the commma seperated ids of leads
		$idsArr = explode(",",$ids);
		$assign_to = $this->input->post('assign_to');

		foreach($idsArr as $lead_id)
		{
			$this->assignLead($lead_id, $assign_to);
		}

		$data['reload'] = 0;
		$data['reset'] = 0;	
		$data['success'] = "Lead(s) Assigned";	
		$data['error'] = 'false';
		$data['employeeName'] = getEmployeeName($assign_to);
		echo json_encode($data);
		exit;		
	}

	//this function is called from a loop in case of multi assign.
	private function assignLead($lead_id, $assign_to)
	{
		$dataLead = array();
		$success_msg = "";
		$post_data = $this->input->post();
		
		//get the assignee user branch
		$assigneeCityId = getCityIdOfUserByUserId($assign_to);
		$assigneeBranchId = getBranchIdOfUserByUserId($assign_to);

		//creator of the lead will be changed as per the new department manager.	
		$newCreator = 0; //basically we are using the created_by column as a new responsible person. (The orignal created backup column is also there.)
		$newAssigneeManagingDeps = $this->Model_structure_departments->getUserManagingDeps($assign_to);
		if(is_array($newAssigneeManagingDeps) && count($newAssigneeManagingDeps)>0)
		{			
			$newCreator = $assign_to; //In case of lead is assigned to other department's manager. the creator and assignee both will be same.
		}
		else
		{
			$wdu = array();
			$wdu['user_id'] = $assign_to;
			$thisUserDeps = $this->Model_struc_dep_users->getMultipleRows($wdu);
			if($thisUserDeps)
			{
				$whereSd = array();
				$whereSd['id'] = $thisUserDeps[0]->struc_dep_id;
				$deps = $this->Model_structure_departments->getMultipleRows($whereSd); //Dep the user belongs to as an employee in structure.
				if($deps)
				{
					$newCreator = $deps[0]->manager_id;
				}
			}
		}

		//assign lead
		$dataLead['city_id']  = $assigneeCityId;
		$dataLead['branch_id']  = $assigneeBranchId;
		$dataLead['assign_to']  = $assign_to;
		$dataLead['assign_by']  = $this->session->userdata['user']['id'];
		$dataLead['assigned_at'] = date('Y-m-d H:i:s');	
		$dataLead['status'] = 'Assigned';
		if($newCreator)
		$dataLead['created_by'] = $newCreator; //Note: in case of latest responsible manager the id will be replaced with latest manager. the backup column is there :)
		$where['id'] = $lead_id;					
		$this->Model_lead->update($dataLead,$where);

		//set notification
		$this->setNotification('Lead Assigned', $lead_id);	
		
		//add a message that lead is assigned in the action log box
		$dataM['lead_id'] = $lead_id;
		$dataM['message_id'] = 19;		
		$dataM['comments'] = "Lead is assigned to ".getEmployeeName($assign_to);
		$dataM['created_at'] = date('Y-m-d H:i:s');
		$dataM['created_by'] = $this->session->userdata['user']['id'];
		$this->Model_leads_messages->save($dataM);
		//==========

		//if the user is assigned now, then delete old tags so now this user can get the full assignee rights. Note: the name in tags will remain there.
		$mtD = array();
		$mtD['user_id'] = $assign_to;
		$mtD['lead_id'] = $lead_id;

		$this->Model_lead_tags->delete($mtD);		
		

	}

	private function sendSurvey($lead_id)
	{

		$dataLead = array();
		$dataMessage = array();
		$success_msg = "";
		$post_data = $this->input->post();
		
		//======
		$length = 10;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		//======

		//Send survery via email.
		$emailData = array();
		$leadRec = $this->Model_lead->get($lead_id);
		$cat_row = $this->Model_category->get($leadRec->category_id,true);

		$emailData['trackid'] = $leadRec->trackid;		
		$emailData['first_name'] = $leadRec->first_name;
		$emailData['category_title'] = $cat_row['title_email'];
		$emailData['category_title_ar'] = $cat_row['title_email_ar'];
		$surveyData = array();
		$surveyData['category_id'] = $leadRec->category_id;
		$surveyData['is_active'] = 1;
		$surveyRec = $this->Model_survey->getWithMultipleFields($surveyData);
		$dataLead['survey_sent'] = 0;
		$dataLead['survey_id'] = 0;
		if($surveyRec)
		{
			//$emailData['survey_link'] = base_url().'lead_survey/surveyAction/'.$surveyRec->id.'/'.$post_data['lead_id'];
			$emailData['survey_link'] = '<a href="'.base_url().'lead_survey/surveyAction/'.$surveyRec->id.'/'.$post_data['lead_id'].'/'.$randomString.'">Take The Survey</a>';			
			$emailData['survey_link_ar'] = '<a href="'.base_url().'lead_survey/surveyAction/'.$surveyRec->id.'/'.$post_data['lead_id'].'/'.$randomString.'">قم بتعبئة الإستبيان</a>';			
			$emailData['created_date'] = $leadRec->created_at;
			sendEmail(3, $leadRec->email, $emailData); //2 is the email id from database. it concatinates the email body
			$dataLead['survey_sent'] = 1;
			$dataLead['survey_id'] = $surveyRec->id;	
			$dataLead['survey_link_rand'] = $randomString;
			
		}
						
		$where['id'] = $lead_id;
		$this->Model_lead->update($dataLead,$where);
		
	}

	private function setNotification($comments, $lead_id, $messageRec=null)
	{
		$dataLead = array();
		$dataNotification = array();

		//get lead rec for which the notification is going to be added.
		$leadRec = $this->Model_lead->get($lead_id);

		if($leadRec)
		{		
			if($leadRec->assign_to === $this->session->userdata['user']['id'] && $leadRec->created_by === $this->session->userdata['user']['id'])
			{
				//actually this is a case when a simple employee assigns a lead to his manager then the responsible becomes the manager, and assignee is also a manager.
				//so both creator and assignee are same. 

				//do nothing

			}else
			{

				if($leadRec->created_by==="0") //inbox lead
				{	
					if(false) //not a case anymore
					{
						$dataNotification['inbox_notification_read'] = 0;
					}
					else
					{
						if($leadRec->assign_to === $this->session->userdata['user']['id'])
							$dataNotification['inbox_notification_read'] = 1;

						if(checkLoggedInUserInboxRole())
						{
							if($leadRec->assign_to)
							$dataNotification['assignee_notification_read'] = 1;
						}
					}			
				}
				else
				{
					if(false) //it is handled through other conditions
					{		
						$dataNotification['assignee_notification_read'] = 0;
						$dataNotification['creater_notification_read'] = 0;						
					}
					else
					{
						if(/*$messageRec->show_on_creator_dashboard && */$leadRec->assign_to === $this->session->userdata['user']['id'])
							$dataNotification['creater_notification_read'] = 1;
						
						if(/*$messageRec->show_on_assignee_dashboard && */$leadRec->created_by === $this->session->userdata['user']['id'])
						{
							if(strpos($comments,"New lead created")===false)						
							{
								if($leadRec->assign_to)
								$dataNotification['assignee_notification_read'] = 1;
							}
							
						}

						$whereTagged = array();
						$whereTagged['user_id'] = $this->session->userdata['user']['id'];
						$whereTagged['lead_id'] = $lead_id;
						$checkIfTaggedUserIsReplying = $this->Model_lead_tags->getWithMultipleFields($whereTagged);
						//print_r($checkIfTaggedUerIsReplying); exit();
						if($checkIfTaggedUserIsReplying)
						{
							$dataNotification['creater_notification_read'] = 1;
						}
					}		
				}
			}
		}
				
		

		$dataNotification['created_at'] = date('Y-m-d H:i:s');
		$dataNotification['created_by'] = $this->session->userdata['user']['id'];	
		$dataNotification['lead_id'] = $lead_id;
		$dataNotification['comments'] = $comments;		
		$this->Model_notification->save($dataNotification);
		
	}


	public function setNotificationRead()
	{
		$dataLead = array();
		$dataNotification = array();

		$leadRec = $this->Model_lead->get($this->input->post('lead_id'));
		if($leadRec->created_by == $this->session->userdata['user']['id'])
		{
			$dataNotification['creater_notification_read'] = 0;
		}
		elseif($leadRec->assign_to === $this->session->userdata['user']['id'])
		{
			$dataNotification['assignee_notification_read'] = 0;
		}
		elseif($leadRec->created_by === "0")
		{
			$dataNotification['inbox_notification_read'] = 0;
		}
		else
		{
			//tagged
			$dataNotification['tagged_user_notification_read'] = 0;
			$where['tagged_user_id'] = $this->session->userdata['user']['id'];

		}
		
		

		$where['id'] = $this->input->post('id');
		$this->Model_notification->update($dataNotification,$where);

	}

	public function allNotificationRead()
	{
		$dataLead = array();
		$dataNotification = array();


		$leadRec = $this->Model_lead->get($this->input->post('lead_id'));

		if($leadRec->created_by === $this->session->userdata['user']['id'])
		{
			$dataNotification['creater_notification_read'] = 0;
		}
		elseif($leadRec->assign_to === $this->session->userdata['user']['id'])
		{
			$dataNotification['assignee_notification_read'] = 0;
		}
		elseif($leadRec->created_by === "0")
		{
			$dataNotification['inbox_notification_read'] = 0;
		}
		else
		{
			//tagged
			$dataNotification['tagged_user_notification_read'] = 0;
			$where['tagged_user_id'] = $this->session->userdata['user']['id'];
		}
		
				
		$where['lead_id'] = $this->input->post('lead_id');
		if(count($dataNotification))
		$this->Model_notification->update($dataNotification,$where);


	}
	
	private function addLeadMessage()
	{

		$data = array();
		$dataNotification = array();
		$dataTagged = array();


		//get Data Ready
		$post_data = $this->input->post();
		foreach($post_data as $key => $value)
		{
			if($key != 'form_type' && $key!='create_action_date' && $key!='create_action_time' && $key!='call_status' && $key!='send_survey' && $key!='tagged_ids' && $key!='tag_id' && $key!='update_scheduled_message_id')
				$data[$key] = $value;
		}

		$create_action_date = $this->input->post('create_action_date');
		$create_action_time = $this->input->post('create_action_time');
		$data['schedule_date_time'] = $create_action_date.' '.$create_action_time;
		$data['schedule_date_time'] = trim($data['schedule_date_time']);

		if($data['message_id'] === "5"){
			if(!empty($data['vehicle_specific_names_id'])){
				$data['vehicle_specific_names_id'] = implode(',', $data['vehicle_specific_names_id']);
			  }else{
				$data['vehicle_specific_names_id'] = '';
			  }	
		}

		//note tag_id is the drop down box tag e.g coming from db table tags. (In settings screen)
		//The @ tagging any other employee is by using "tagged_ids" that is other functionality.
		if($data['message_id']==="4" && $this->input->post('tag_id')!=null && $this->input->post('tag_id')!=="") 
		{
			$dataT = array();
			$whereT = array();
			$dataT['tag_id'] = $this->input->post('tag_id');
			$whereT['id'] = $data['lead_id'];
			$this->Model_lead->update($dataT,$whereT);
			$data['comments'] = "<label class='tagged_comment_lbl'>".getTagTitle($dataT['tag_id']).".</label> ".$data['comments'];
		}
		
		
		if($this->input->post('update_scheduled_message_id')!=null && $this->input->post('update_scheduled_message_id')!=="0")
		{
			$whereMsgUpd['id'] = $this->input->post('update_scheduled_message_id');
			$this->Model_leads_messages->update($data,$whereMsgUpd);
			//echo $this->db->last_query(); exit();

		}else
		{
			//add message 
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['created_by'] = $this->session->userdata['user']['id'];
			$insert_id = $this->Model_leads_messages->save($data);
		}
	
		//get message record
		$messageRec = $this->Model_messages->get($data['message_id']);
		
		//set notification			
		$this->setNotification($messageRec->title.' Action Added', $data['lead_id'], $messageRec);

		//tagged users
		$tagged_ids = $this->input->post('tagged_ids');
		if($data['message_id']==="4" && $tagged_ids) //note this tagged_ids are user ids who are tagged in this comment by @
		{
			$tagged_ids = explode(",",$tagged_ids);
			foreach($tagged_ids as $tagged_id) //loop here for the tagged user ids 
			{
				//add notifications for each tagged user
				$dataNotification['tagged_user_notification_read'] = 1;
				$dataNotification['tagged_user_id'] = $tagged_id;
				$dataNotification['created_at'] = date('Y-m-d H:i:s');
				$dataNotification['created_by'] = $this->session->userdata['user']['id'];	
				$dataNotification['lead_id'] = $data['lead_id'];
				$dataNotification['comments'] = "You are tagged";
				$this->Model_notification->save($dataNotification);

				
				//First unactive the old tags for same user.
				$dataUpd['active'] = 0;
				$whereUpd['user_id'] = $tagged_id;
				$whereUpd['lead_id'] = $data['lead_id'];
				$this->Model_lead_tags->update($dataUpd,$whereUpd);
				//Now save record in forign table for tagged users for this lead.
				$dataTagged['lead_id'] = $data['lead_id'];
				$dataTagged['tagged_datetime'] = date('Y-m-d H:i:s');
				$dataTagged['tagged_by'] = $this->session->userdata['user']['id'];
				$dataTagged['user_id'] = $tagged_id;
				$dataTagged['active'] = 1;
				$this->Model_lead_tags->save($dataTagged);
			}
		}

		//send email if applicable
		$leadRes = $this->Model_lead->get($data['lead_id']);
		if($data['message_id'] === "2")
		{
			$emailData = array();			
			$cat_row = $this->Model_category->get($leadRes->category_id,true);

			$emailData['trackid'] = $leadRes->trackid;		
			$emailData['first_name'] = $leadRes->first_name;
			$emailData['category_title'] = $cat_row['title'];
			$emailData['body_concatinate'] = $data['comments'];
			$emailData['assignee_name'] = getEmployeeName($leadRes->assign_to);
			if($emailData['assignee_name']=="") $emailData['assignee_name'] = getEmployeeName($this->session->userdata['user']['id']);
			$emailData['assignee_name_ar'] = getEmployeeNameAr($leadRes->assign_to);
			if($emailData['assignee_name_ar']=="") $emailData['assignee_name_ar'] = getEmployeeNameAr($this->session->userdata['user']['id']);
			$emailData['showroom'] = getBranchName($leadRes->branch_id);
			$emailData['showroom_ar'] = getBranchNameAr($leadRes->branch_id);
			$emailData['assign_to_contact'] = getEmployeeTelNExt($leadRes->assign_to);
			if($emailData['assign_to_contact']=="") $emailData['assign_to_contact'] = getEmployeeTelNExt($this->session->userdata['user']['id']);
			if($leadRes->assign_to) $emailData['job_title'] = getEmployeeJobTitle($leadRes->assign_to);
			else $emailData['job_title'] = getEmployeeJobTitle($this->session->userdata['user']['id']);
			if($leadRes->assign_to) $emailData['job_title_ar'] = getEmployeeJobTitleAr($leadRes->assign_to);
			else $emailData['job_title_ar'] = getEmployeeJobTitleAr($this->session->userdata['user']['id']);

			sendEmail(2, $leadRes->email, $emailData); //2 is the email id from database. it concatinates the email body
		}
		elseif($data['message_id'] === "10") //Phone call answer = No
		{
			$emailData = array();			
			$cat_row = $this->Model_category->get($leadRes->category_id,true);

			$emailData['trackid'] = $leadRes->trackid;		
			$emailData['first_name'] = $leadRes->first_name;
			$emailData['category_title'] = $cat_row['title'];

			$emailData['call_time'] = date('Y-m-d H:i:s');
			$emailData['contact_number'] = $leadRes->mobile;
			$emailData['assign_to_contact'] = getEmployeeTelNExt($leadRes->assign_to);
			if($emailData['assign_to_contact']=="") $emailData['assign_to_contact'] = getEmployeeTelNExt($this->session->userdata['user']['id']);

			sendEmail(4, $leadRes->email, $emailData);
		}		
		elseif($data['message_id'] === "8") //Re-schedule call
		{
			$emailData = array();			
			$cat_row = $this->Model_category->get($leadRes->category_id,true);

			$emailData['trackid'] = $leadRes->trackid;		
			$emailData['first_name'] = $leadRes->first_name;
			$emailData['category_title'] = $cat_row['title'];
			$emailData['create_action_date'] = $create_action_date;
			$emailData['create_action_time'] = $create_action_time;
			$emailData['contact_number'] = $leadRes->mobile;
			$emailData['assign_to_contact'] = getEmployeeTelNExt($leadRes->assign_to);
			if($emailData['assign_to_contact']=="") $emailData['assign_to_contact'] = getEmployeeTelNExt($this->session->userdata['user']['id']);
			
			
			sendEmail(5, $leadRes->email, $emailData);
		}
		elseif($data['message_id'] === "5" && isset($data['test_drive_type']) && $data['test_drive_type']==="Scheduled" && $this->input->post('update_scheduled_message_id')==='0') //schedule test drive
		{
			$emailData = array();			
			$cat_row = $this->Model_category->get($leadRes->category_id,true);

			$emailData['trackid'] = $leadRes->trackid;		
			$emailData['first_name'] = $leadRes->first_name;
			$emailData['category_title'] = $cat_row['title'];
			$emailData['create_action_date'] = $create_action_date;
			$emailData['create_action_time'] = $create_action_time;
			$emailData['assignee_name'] = getEmployeeName($leadRes->assign_to);
			if($emailData['assignee_name']=="") $emailData['assignee_name'] = getEmployeeName($this->session->userdata['user']['id']);
			$emailData['assignee_name_ar'] = getEmployeeNameAr($leadRes->assign_to);
			if($emailData['assignee_name_ar']=="") $emailData['assignee_name_ar'] = getEmployeeNameAr($this->session->userdata['user']['id']);
			$emailData['showroom'] = getBranchName($leadRes->branch_id);
			$emailData['showroom_ar'] = getBranchNameAr($leadRes->branch_id);
			$emailData['assign_to_contact'] = getEmployeeTelNExt($leadRes->assign_to);
			if($emailData['assign_to_contact']=="") $emailData['assign_to_contact'] = getEmployeeTelNExt($this->session->userdata['user']['id']);
			$vehicles = "";
			if($data['vehicle_specific_names_id']!="")
			{
				$vehicle_specific_names_id_arr = explode(",",$data['vehicle_specific_names_id']);
				foreach($vehicle_specific_names_id_arr as $vsi)
				{
					if($vehicles!="") $vehicles .= ", ";
					$vehicles .= getSpecificVehicleNameById($vsi);
				}
			}
			$emailData['vehicles'] = $vehicles;

			
			sendEmail(6, $leadRes->email, $emailData);
		}


		//change status of lead if applicable.
		if($data['message_id'] === "6")
			$this->changeStatusOfLead('No Response',$data['lead_id']);
		elseif($data['message_id'] === "7")		
			$this->changeStatusOfLead('Finished',$data['lead_id']);
		elseif($data['message_id'] === "11")
		{
			$this->changeStatusOfLead('Closed',$data['lead_id']);
			if($this->input->post('send_survey')==='1')
				$this->sendSurvey($data['lead_id']);
		}
		elseif($data['message_id'] === "14")
			$this->changeStatusOfLead('Disapproved',$data['lead_id']);
		elseif($data['message_id'] === "14")
			$this->changeStatusOfLead('Disapproved',$data['lead_id']);
		elseif($data['message_id'] === "13")
			$this->changeStatusOfLead('Approved and Archived',$data['lead_id']);

		if($data['message_id']==="9")  //Did you get an answer = yes
		{
			$dataDl = array();
			$whereDl = array();

			$dataDl['due_date'] = calculateLeadDeadline(date('Y-m-d H:i:s'), $leadRes->category_id, 'closeKPI');
			$whereDl['id'] = $data['lead_id'];

			$this->Model_lead->update($dataDl,$whereDl);

		}

		//save first call time only for the first call.
		if($data['message_id']==="9")  //Did you get an answer = yes
		{
			$dataFc = array();
			$whereFc = array();

			$dataFc['first_call_time'] = date('Y-m-d H:i:s');			
			$whereFc['id'] = $data['lead_id'];
			$whereFc['first_call_time'] = '0000-00-00 00:00:00';

			$this->Model_lead->update($dataFc,$whereFc);

		}
       
		

		
		$data['success'] = $messageRec->title.' Action Added.';
		$data['error'] = 'false';
		$data['reset'] = 1;
		$data['reload'] = 0;					
		echo json_encode($data);
		exit;			
	}

	private function changeStatusOfLead($status,$lead_id)
	{
		$data = array();

		$data['status'] = $status;
		$where['id'] = $lead_id;
		if($status==="Finished")
			$data['finished_time'] = date('Y-m-d H:i:s');
		elseif($status==="Closed")
			$data['close_time'] = date('Y-m-d H:i:s');

		$insert_lead_id = $this->Model_lead->update($data,$where);
	}

	private function deleteLeads()
	{

		$data = array();
		$checkBoxesSelectedCommas = $this->input->post('id');
		$checkBoxesSelectedCommasArr = explode(",",$checkBoxesSelectedCommas);


		foreach($checkBoxesSelectedCommasArr as $checkBoxesSelected)
		{
			
			//get the email address
			$leadRec = $this->Model_lead->get($checkBoxesSelected);

			//2nd parameter is the limit and this function already have order by id desc
			$subLatestLeadResult = $this->Model_lead->getSubLeads($leadRec->email, 1); 
			
			if($subLatestLeadResult)
			{
				$idToMakeSubtoLatest['id'] = $subLatestLeadResult[0]->id;
				$subToLatestData['new_lead_with_same_email'] = 1;

				$this->Model_lead->update($subToLatestData, $idToMakeSubtoLatest);
			}

			
			$this->setNotification('Lead deleted '.$leadRec->trackid, -1); 

		}

		$this->Model_lead->deleteIn('id',$checkBoxesSelectedCommasArr);		

		return true;
	}

	
	private function checkLeadsSameCityBranch()
	{

		$data = array();
		$leadsIds = $this->input->post('lead_ids');
		$leadsIdsArr = explode(",",$leadsIds);
		$data['success'] = "1";	

		$leadsRecs = $this->Model_lead->getLeadsWithIdsArr($leadsIdsArr);

		$city_id = 0;
		$branch_id = 0;
		$counter = 0;
		foreach($leadsRecs as $lead)
		{
			//for the first lead set the variables values.
			if($counter===0)
			{
				$city_id = $lead->city_id;
				$branch_id = $lead->branch_id;
			}
			else //for further leads keep on checking if same
			{
				if($lead->city_id == $city_id && $lead->branch_id == $branch_id)
				{
					//do nothing				
				}
				else
				{
					$data['success'] = "0";	
					break;
				}
			}			

			$counter++;
		}


		$cityRec = $this->Model_city->get($city_id);
		$branchRec = $this->Model_branch->get($branch_id);

		$data['city_id'] = $city_id;
		$data['city_name'] = $cityRec->title;
		$data['branch_id'] = $branch_id;
		$data['branch_name'] = $branchRec->title;

		echo json_encode($data);
		exit;
	}
	

	public function singleLead($id){

	//single lead will use the same leads listing screen but with if else to show only single lead.
		
		$data = array();
		$data['view'] = 'lead/leads';
        $data['lead_class'] = 'active';
		//$data['leads'] = $this->Model_lead->getAllSortedBy();

		$data['leads'] = $this->Model_lead->getSingleLead($id);

		$email = $data['leads'][0]->email;

		$allRelatedLeads = getAllRelatedLeads();
		$okCanAccess = false;
		foreach($allRelatedLeads as $relatedLead)
		{
			if($relatedLead->email == $email)
			{
				$okCanAccess = true;
				break;
			}
		}
		if(!$okCanAccess)
		{
			$data['leads'] = array();
		}

		$data['isSingleDetailLead'] = true;
		$data['cities'] = $this->Model_city->getAll();
		$data['logged_in_user_id'] = $this->session->userdata['user']['id'];
		$data['dashboard'] = false;
		$data['searchedOrFiltered'] = false;
		$data['vehicles_specific'] = $this->Model_vehicle_specific_name->getAll(); 
		$this->load->view('template',$data);		
		
	}


	
	private function validation()
	{
		    $errors = array();
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            $this->form_validation->set_rules('email', 'Email', 'required');
			//$this->form_validation->set_rules('title', 'Title', 'required');
			//$this->form_validation->set_rules('mobile', 'Mobile', 'required');
			$this->form_validation->set_rules('first_name', 'First Name', 'required');
			//$this->form_validation->set_rules('surname', 'Surname', 'required');
			$this->form_validation->set_rules('category_id', 'Category', 'required');
			$this->form_validation->set_rules('city_id', 'City', 'required');
			$this->form_validation->set_rules('branch_id', 'Branch', 'required');			
			//$this->form_validation->set_rules('preferred_date', 'Preferred Date', 'required');
			//$this->form_validation->set_rules('preferred_time', 'Preferred Time', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$errors['error'] = validation_errors();
				$errors['success'] = 'false';
				echo json_encode($errors);
				exit;
			}else
			{
				return true;
			}
			
	}


	private function save($post_data)
	{
		$data = array();
		$update_with_email =  array();
		
		$update_with_email['email'] = $post_data['email'];
		$data['new_lead_with_same_email'] = 0;
		
		$this->updateIfLeadHasPreviousQuery($data,$update_with_email); // if already exist lead with same email we should need to update all recodes to zero and one for new query
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type')
			{
			 $data[$key] = $value;	
			}
		}
		
		if(!empty($data['vehicle'])){
			$data['vehicle'] = implode(',', $data['vehicle']);
		  }else{
			$data['vehicle'] = '';
		  }
		  
		if(!empty($data['vehicle_specific_names_id'])){
			$data['vehicle_specific_names_id'] = implode(',', $data['vehicle_specific_names_id']);
		  }else{
			$data['vehicle_specific_names_id'] = '';
		  }
		//$data['vehicle'] = implode(',', $data['vehicle']);
		

		$data['created_at'] = date('Y-m-d H:i:s');
		$data['new_lead_with_same_email'] = 1;
		$data['created_by'] = $this->session->userdata['user']['id']; //this will be 0 for leads created by cronjob email parsing.
		$data['orignal_created_by'] = $this->session->userdata['user']['id']; //this is a backup column, because the created by column can contain the latest manager id so the original creator id will remain here for future use 
		$data['trackid'] = generatTrackId($post_data['category_id']);

		$data['due_date'] = calculateLeadDeadline($data['created_at'], $post_data['category_id']);
		
				
		if($data['assign_to']!="")
		{
			$data['assign_by']  = $this->session->userdata['user']['id'];
			$data['assigned_at'] = date('Y-m-d H:i:s');	
			$data['status'] = 'Assigned';
		}

		$insert_lead_id = $this->Model_lead->save($data);

		//Send Email To Customer For New Lead Created
		$cat_row = $this->Model_category->get($post_data['category_id'],true);
		$data['category_title'] = $cat_row['title'];

		if($post_data['category_id']==='1')
		{
			//only for test drive send this email.
			$emailData = $data;
			$emailData['create_action_date'] = $post_data['preferred_date'];
			$emailData['create_action_time'] = $post_data['preferred_time'];						
			$emailData['showroom'] = getBranchName($post_data['branch_id']);
			$emailData['showroom_ar'] = getBranchNameAr($post_data['branch_id']);
			$vehicles = "";
			if(isset($post_data['vehicle_specific_names_id']) && is_array($post_data['vehicle_specific_names_id']) && count($post_data['vehicle_specific_names_id'])>0)
			{
				$vehicle_specific_names_id_arr = $post_data['vehicle_specific_names_id'];
				foreach($vehicle_specific_names_id_arr as $vsi)
				{
					if($vehicles!="") $vehicles .= ", ";
					$vehicles .= getSpecificVehicleNameById($vsi);
				}
			}
			$emailData['vehicles'] = $vehicles;

			sendEmail('1',$post_data['email'],$emailData); //db id 1 is for new lead email to customer.
		}
		
		$this->setNotification('New lead created '. $data['trackid'], $insert_lead_id); 

		if($data['assign_to']!="")
		{
			//set notification of assign a lead too
			$this->setNotification('Lead Assigned', $insert_lead_id);

			//add a message that lead is assigned in the action log box
			$dataM['lead_id'] = $insert_lead_id;
			$dataM['message_id'] = 19;		
			$dataM['comments'] = "Lead is assigned to ".getEmployeeName($data['assign_to']);
			$dataM['created_at'] = date('Y-m-d H:i:s');
			$dataM['created_by'] = $this->session->userdata['user']['id'];
			$this->Model_leads_messages->save($dataM);
			//==========
		}


		return $data['trackid'];
		
	}

	
	private function update($post_data)
	{
		$data = array();

		//====
		$update_with_email =  array();		
		$update_with_email['email'] = $post_data['email'];		

		$lead_id = $post_data['id'];
		$leadDataB4Update = $this->Model_lead->get($lead_id);
		$leadEmailB4Update = $leadDataB4Update->email;
		$leadCatB4Update = $leadDataB4Update->category_id;
		$leadTrackIdB4Update = $leadDataB4Update->trackid;
		$leadCreatedAt = $leadDataB4Update->created_at;

		//check if email is changed
		if($post_data['email'] != $leadEmailB4Update)
		{
			// if there are already leads with new email address then update all those records to zero
			$data['new_lead_with_same_email'] = 0;
			$this->updateIfLeadHasPreviousQuery($data,$update_with_email);

			//$data['new_lead_with_same_email'] = 1; //this updated will become on top and others with same email will become as nested.

			//if this was a parent then latest of its children will become parent, as this updating lead's email is changed
			if($leadDataB4Update->new_lead_with_same_email==1)
			{
				$this->Model_lead->updateLatestLeadByEmail($leadEmailB4Update); //update new_lead_with_same_email=1 to the latest lead, because this lead is moving to other leads by email 
			}
		}

		//check if category is changed
		if($post_data['category_id'] != $leadCatB4Update)
		{
			//update trackid alpha values only
			$data['trackid'] = updateTrackId($post_data['category_id'],$leadTrackIdB4Update);

			//update due date also
			$data['due_date'] = calculateLeadDeadline($leadCreatedAt, $post_data['category_id']);
		}

				
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type')
			{
			 $data[$key] = $value;	
			}
		}
		
		$data['vehicle'] = "";
		if(isset($post_data['vehicle']))
		$data['vehicle'] = implode(',', $post_data['vehicle']);

		$data['vehicle_specific_names_id'] = "";
	    if(isset($post_data['vehicle_specific_names_id']))
		$data['vehicle_specific_names_id'] = implode(',', $post_data['vehicle_specific_names_id']);

		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->session->userdata['user']['id']; //this will be 0 for leads created by "cronjob email parsing".
		$where['id'] = $post_data['id'];				
		/*if($leadDataB4Update->trackid == 'N/A') //not a case anymore
		{
			$data['trackid'] = generatTrackId($post_data['category_id']);
		}*/
		$insert_lead_id = $this->Model_lead->update($data,$where);

		$this->setNotification('Lead updated', $post_data['id']); 

		return $insert_lead_id;
		
	}
	
	private function updateIfLeadHasPreviousQuery($data,$update_with_email)
	{
		return $this->Model_lead->update($data,$update_with_email);
	}
	
	
	public function branchesByCity()
	{

		branchesByCity(true, $this->input->post('city_id'),0); //from custom helper
		
	}

	/*public function usersByCityAndBranchAjax()
	{

		//$this->input->post('city_id')
		//$this->input->post('branch_id')

		$options = "";

		$dataUsers['city_id'] = $this->input->post('city_id');
		$dataUsers['branch_id'] = $this->input->post('branch_id');
		$users = $this->Model_user->getMultipleRows($dataUsers,false,'asc');		
		
		echo '<option value= "">Please Select</option>';

		if(is_array($users))
		{
			foreach($users as $user){
				
				$selected = "";
				//if($branch_id){ if($branch_id==$branch->id) $selected = 'selected=selected'; } 
				$options .= '<option '.$selected.' value="'.$user->id.'">'.$user->full_name.'</option>';
			}
		}

		echo $options;
		exit();
		
	}*/

	//its basically now from org strucure and NOT from city and branch
	public function usersByCityAndBranchAjax()
	{

		$options = "";
		$users = array();
	
		$getUserIdIfAssignToAll = $this->Model_user->getUserIdIfSection($this->session->userdata['user']['id'],34); // 34 is the "Assign to all" section
		$getUserIdIfLeadControl = $this->Model_user->getUserIdIfSection($this->session->userdata['user']['id'],33); //33 is the "Lead Control" section
		$loggedInUserManagingDeps = getLoggedInUserManagingDeps();		
		
		if($getUserIdIfAssignToAll)
		{
			//this logged in user has the role/section of "Assign to All";

			$users = $this->Model_user->getAllUsersExcMe($this->session->userdata['user']['id']);

		}
		elseif(is_array($loggedInUserManagingDeps) && count($loggedInUserManagingDeps)>0)
		{
			//logged in user is a manager of one or more deps

			$users = $this->usersUnderMngRecursive(); //comma sep list of users under this manager.
			$users = $this->Model_user->getUsersIn(explode(",",$users)); //array
		}
		else
		{
			//don't have the "assign all users" role and also not a manager 
			//it can be a simple user who can assign lead to its manager or to "lead control role" within same deps in which he belongs to. Can be multiple deps.

			$wdu = array();
			$wdu['user_id'] = $this->session->userdata['user']['id'];
			$thisUserDeps = $this->Model_struc_dep_users->getMultipleRows($wdu);
			if($thisUserDeps)
			{
				$userManagerIds = array();
				foreach($thisUserDeps as $tud)
				{
					$whereSd = array();
					$whereSd['id'] = $tud->struc_dep_id;
					$deps = $this->Model_structure_departments->getMultipleRows($whereSd); //all dep results, the user belongs to as an employee in structure.
					if($deps)
					foreach($deps as $ud)
					{
						$userManagerIds[] = $ud->manager_id;

						//also check if this dep has one or more "lead control" role user?
						$whereOu = array();
						$whereOu['struc_dep_id'] = $ud->id;
						$oUsers = $this->Model_struc_dep_users->getMultipleRows($whereOu);
						
						if($oUsers)
						{
							//for now this loop will contain itself user too. but no worries.
							foreach($oUsers as $oU)
							{
								$getUserIdIfLeadControl = $this->Model_user->getUserIdIfSection($oU->user_id,33); //33 is the "Lead Control" section
								if($getUserIdIfLeadControl)
								{
									$userManagerIds[] = $getUserIdIfLeadControl[0]['id'];
								}

							}
						}

					}
				}

				$users = $this->Model_user->getUsersIn($userManagerIds); //array //this will return the users who are "managers of this user" or "lead control" of this user's dep. Can be multiple.
			}



		}
				
		
		echo '<option value= "">Please Select</option>';

		if(is_array($users))
		{
			foreach($users as $user){
				
				$selected = "";
				//if($branch_id){ if($branch_id==$branch->id) $selected = 'selected=selected'; } 
				$options .= '<option '.$selected.' value="'.$user->id.'">'.$user->full_name.'</option>';
			}
		}

		echo $options;
		exit();
		
	}

	public function checkRemainTime()
	{
	
		$allRelatedLeads = getAllRelatedLeads();
		$leads30Min = "";
		$remindLeadActionId = "";
		$remindLeadId = "";
		$remindLeadTrackId = "";
		foreach($allRelatedLeads as $relatedLead)
		{
			$actionIds = checkRemainTimeForAction($relatedLead->id); //this will get all the messages that are 30 min for this lead, comma sep
			if($actionIds)
			{
				$actionIdsArr = explode(",",$actionIds);
				foreach($actionIdsArr as $actionId)
				{
					if($remindLeadActionId !="") $remindLeadActionId.= ",";
					$remindLeadActionId .= $actionId;

					if($remindLeadId !="") $remindLeadId.= ",";
					$remindLeadId .= $relatedLead->id;

					if($remindLeadTrackId !="") $remindLeadTrackId.= ",";
					$remindLeadTrackId .= $relatedLead->trackid;

				}


			}
		}

		$remindLead['remindLeadActionId'] = $remindLeadActionId;
		$remindLead['remindLeadId'] = $remindLeadId;
		$remindLead['remindLeadTrackId'] = $remindLeadTrackId;
		echo json_encode($remindLead);
		exit;
	}

	public function generateReminderNotification()
	{
		$lead_id = $this->input->post('lead_id');
		$leadRec = $this->Model_lead->get($lead_id);
		$dataNotification = array();

		//set notification

		//reminder popup appears for both, creator and assignee so notification is also for both.
		if($leadRec->created_by === $this->session->userdata['user']['id'] && $leadRec->assign_to === $this->session->userdata['user']['id'])
		{
			//do nothing.
		}
		else
		{
			//this is self notification
			if($leadRec->created_by === $this->session->userdata['user']['id'])
				$dataNotification['creater_notification_read'] = 1;
			if($leadRec->assign_to === $this->session->userdata['user']['id'])
				$dataNotification['assignee_notification_read'] = 1;
		}

		$comments = 'Upcoming scheduled action for lead '.$leadRec->trackid.'';
		$dataNotification['created_at'] = date('Y-m-d H:i:s');
		$dataNotification['created_by'] = $this->session->userdata['user']['id'];	
		$dataNotification['lead_id'] = $lead_id;
		$dataNotification['comments'] = $comments;		
		$this->Model_notification->save($dataNotification);
	}



	public function ajaxAction()
	{

		//====set variables===
		$logged_in_user_id = $this->session->userdata['user']['id'];
		$lead_id = $this->input->post('lead_id');
		$lead = $this->Model_lead->get($lead_id);
		$dashboard = $this->input->post('dashboard');
		$howManyLatest = 1;
		//====================

		$messages = getLeadMessages($lead_id,$howManyLatest);

		if($messages)
		{		
			displayMessages($messages, $logged_in_user_id, $lead, $dashboard, $howManyLatest);
		}

		exit();
	}


	public function ajaxSelfData()
	{

		$leadid = $this->input->post('lead_id');
		$i = $this->input->post('counter');
		$logged_in_user_id = $this->input->post('logged_in_user_id');
		$dashboard = $this->input->post('dashboard');
		//if($dashboard) $dashboard = true; else $dashboard = false;
		$isSingleDetailLead = $this->input->post('is_single');
		//if($isSingleDetailLead) $isSingleDetailLead = true; else $isSingleDetailLead = false;
		$is_sublead = $this->input->post('is_sublead');
		//if($is_sublead) $is_sublead = true; else $is_sublead = false;

		//echo "(".$is_sublead.')'; exit();

		$lead = $this->Model_lead->get($leadid);
		$vehicles_specific = $this->Model_vehicle_specific_name->getAll(); 
		$tags = $this->Model_tag->getAll();

		expandedLead($lead,$i,$logged_in_user_id, $dashboard, $isSingleDetailLead,$vehicles_specific,$is_sublead,$tags);		
		exit();
	}

	public function leadNotificationsAjaxCheck()
	{

		$lead_ids_comma = "";
		$allRelatedLeads = getAllRelatedLeads();

		foreach($allRelatedLeads as $relatedLead)
		{
			if(checkLeadHasUnreadNotifications($relatedLead->id))
			{
				if($lead_ids_comma!="") $lead_ids_comma .= ",";
				$lead_ids_comma .= $relatedLead->id;

			}
		}

		echo $lead_ids_comma;
		exit;


	}

	public function getNotificationsForHeaderAjax()
	{
		$notificationLeads = $this->Model_lead->getLeadsWithNotifications("header");

		if($notificationLeads) foreach( $notificationLeads as $notificationLead )
		{
			?>
			<tr class="hnt<?php echo $notificationLead->lead_id ?>">
			<td><a href="javascript:void(0);" onclick="notificationRead('<?php echo $notificationLead->id;  ?>', '<?php echo $notificationLead->lead_id ?>');"><?php echo $notificationLead->trackid; ?></a></td>
			<td><a href="javascript:void(0);" onclick="notificationRead('<?php echo $notificationLead->id;  ?>', '<?php echo $notificationLead->lead_id ?>');">
				<?php echo $notificationLead->comments ?>
				<br>
				<?php echo date("d F Y H:i a", strtotime($notificationLead->created_at)); ?>
			</a>
			</td>
			</tr>
			<?php
		}

		exit();
	}

	
	private function getStrucChildren($id) 
	{

		$where['parent_id'] = $id;
		$deps = $this->Model_structure_departments->getMultipleRows($where);

		$children = array();
		
		if($deps) {
			# It has children, let's get them.
			//while($row = mysql_fetch_array($r)) {
			foreach($deps as $dep) {
				# Add the child to the list of children, and get its subchildren
				$this->allChild[] = $dep->id;
				$children[$dep->id] = $this->getStrucChildren($dep->id);				
			}
		}

		return $children;
	}

	public function getScheduledMessage()
	{

		//get message record
		$messageRec = $this->Model_leads_messages->get($this->input->post('id'));
		$messageRec = (array) $messageRec;

		//=========
		$messageRec['schedule_date'] = "";
		$messageRec['create_action_date'] = "";
		$messageRec['schedule_time'] = "";
		$messageRec['create_action_time'] = "";

		$schedule_date_time = $messageRec['schedule_date_time'];
		$schedule_date_time_arr = explode(" ",$schedule_date_time);
		if(count($schedule_date_time_arr)===2)
		{
			$messageRec['schedule_date'] = date("j F Y",strtotime($schedule_date_time_arr[0]));
			$messageRec['create_action_date'] = $schedule_date_time_arr[0];
			$messageRec['schedule_time'] = date("h:i a",strtotime($schedule_date_time_arr[1]));
			$messageRec['create_action_time'] = $schedule_date_time_arr[1];
		}
		//==========

		$messageRec['vehicles'] = "";

		$vehicle_specific_names_id = $messageRec['vehicle_specific_names_id'];
		$vehicle_specific_names_id_arr = explode(",",$vehicle_specific_names_id);
		foreach($vehicle_specific_names_id_arr as $vsi)
		{			
			$messageRec['vehicles'] .= '<div id="'.$vsi.'" data-id="'.$vsi.'" class="addCarUHav specific_car">'.getSpecificVehicleNameById($vsi).'<a href="javascript:void(0);">X</a><input type="hidden" name="vehicle_specific_names_id[]" value="'.$vsi.'"></div>';
		}		

		echo json_encode($messageRec);
		exit();
	}

	public function getCallScheduledMessage()
	{

		//get message record
		$messageRec = $this->Model_leads_messages->get($this->input->post('id'));
		$messageRec = (array) $messageRec;

		//=========
		$messageRec['schedule_date'] = "";
		$messageRec['create_action_date'] = "";
		$messageRec['schedule_time'] = "";
		$messageRec['create_action_time'] = "";

		$schedule_date_time = $messageRec['schedule_date_time'];
		$schedule_date_time_arr = explode(" ",$schedule_date_time);
		if(count($schedule_date_time_arr)===2)
		{
			$messageRec['schedule_date'] = date("j F Y",strtotime($schedule_date_time_arr[0]));
			$messageRec['create_action_date'] = $schedule_date_time_arr[0];
			$messageRec['schedule_time'] = date("h:i a",strtotime($schedule_date_time_arr[1]));
			$messageRec['create_action_time'] = $schedule_date_time_arr[1];
		}
		//==========			

		echo json_encode($messageRec);
		exit();
	}
	
}
?>