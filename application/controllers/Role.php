<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct()
    {
        parent::__construct();
		checkAdminSession();
        $this->load->model('Model_role');
	    $this->load->model('Model_section');
		$this->load->model('Model_right');
		$this->load->model('Model_user');
		$this->load->model('Model_common'); //use this in every controller because it is accessed in header.
	  
        //$res = checkLevels(2);
		//checkAuth($res);
    }
	
	
	
	
	public function index()
	{
		$data = array();
		$data['view'] = 'role/roles';
        $data['system_setting'] = 'active';
		$data['roles'] = $this->Model_role->getAll();
		$data['sections'] = $this->Model_section->getAllSections();
		$this->load->view('template',$data);
		
		
	}
	public function saveRole()
	{
		$data = array();
		$data = $this->input->post();
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->session->userdata['user']['id'];
		$this->Model_role->save($data);

       //add notification
		addNotification("New role added ".$data['title']."");

		redirect($this->config->item('base_url') . 'role');
		
		
	}	
	
	public function save()
	{
		$data = array();
		$arr_delete = array();
		$arr_role_title_update = array();
		
		$role_id = $this->input->post('role_id');
		$sections = $this->input->post('section');
		$read = $this->input->post('read');
		$write = $this->input->post('write');
		$delete = $this->input->post('delete');
		$update = $this->input->post('update');
		$email = $this->input->post('email');
		$print = $this->input->post('print');
		
		$arr_role_title_update['title'] = $this->input->post('title'); 
		$arr_role_title_update_by['id'] = $role_id;
		$this->Model_role->update($arr_role_title_update,$arr_role_title_update_by);
		
		$arr_delete['role_id'] = $role_id;
		$this->Model_right->delete($arr_delete);
		$i = 0;
		foreach($sections as $section)
		{
			$read = $this->input->post('read'.$section.'');
			$write = $this->input->post('write'.$section.'');
			$delete = $this->input->post('delete'.$section.'');
			$update = $this->input->post('update'.$section.'');
			//$email = $this->input->post('email'.$section.'');
			//$print = $this->input->post('print'.$section.'');
			
			$data['section_id'] = $section;
			$data['role_id'] = $role_id;
			$data['read'] = $read ? $read : 0;
			$data['write'] = $write ? $write : 0;
			$data['delete'] = $delete ? $delete : 0;
			$data['update'] = $update ? $update : 0;
			//$data['email'] = $email ? $email : 0;
			//$data['print'] = $print ? $print : 0;
			
			$this->Model_right->save($data);
			$i++;
		}

      //redirect($this->config->item('base_url') . 'role');

	    //add notification
		addNotification("Role updated ".$arr_role_title_update['title']."");
	  
	  $success['error'] = 'false';
		$success['success'] = 'Role has been updated successfully .';
		echo json_encode($success);
		exit;
		
		
		}
		
	 public function delete()
	 {
		 $data = array();
		 $id = $this->input->post('id');
		 $data['id'] = $id;  
		 $arr_delete['role_id'] = $id;
		 $user_register_with_role =  $this->Model_user->getWithMultipleFields($arr_delete);
		 if(empty($user_register_with_role)){
			 $this->Model_role->delete($data);
			 $this->Model_right->delete($arr_delete);
			 $data['success'] = 'Role deleted successfully.';
			 $data['error'] = 'false';
			 echo json_encode($data);
			 exit;
		 }else
		 {
			 $data['success'] = 'false';
			 $data['error'] = 'This role is assign to some user. You can not delete this role.';
	     	 echo json_encode($data);
			 exit();
		 }
		 
	 }
	
}
