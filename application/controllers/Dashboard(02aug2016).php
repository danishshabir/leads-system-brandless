<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
		checkAdminSession();
       $this->load->model('Model_report');
	   $this->load->model('Model_city');
	   $this->load->model('Model_category');
	   $this->load->model('Model_lead');
	   $this->load->model('Model_news');
	   $this->load->model('Model_leads_messages');
	   $this->load->model('Model_messages');
	   $this->load->model('Model_notification');
	    $this->load->model('Model_vehicle_specific_name');
	   
    }
	
	
	
	
	public function index($check_for_fetching_leads = 'assign'){ // by default it gets reports of leads that assign to me

		$data = array();
		$data['view'] = 'dashboard/dashboard';
		
		$user_id = $this->session->userdata['user']['id'];
		$user_role = $this->session->userdata['user']['role_id'];
        $user_city = $this->session->userdata['user']['city_id'];		
        $data['vehicles_specific'] = $this->Model_vehicle_specific_name->getAll(); 
		$data['leads_status'] = $this->Model_report->getAllStatus($user_role, $user_id,$check_for_fetching_leads);
		$data['satisfaction'] = $this->Model_report->getSatisfaction($user_role, $user_id,'','',$check_for_fetching_leads);
		if($check_for_fetching_leads==="assign")
			$data['satisfaction1'] = $this->Model_report->getSatisfaction1($user_id,"Assigned");
		else
			$data['satisfaction1'] = $this->Model_report->getSatisfaction1($user_id,"Creator");
		$data['satisfaction_overall1'] = $this->Model_report->getSatisfaction1(0); //user id -1 mean overall

		$data['related_category'] = $this->Model_report->getLeadsByCategory($user_role, $user_id,'','',$check_for_fetching_leads);
		$data['leads_by_region'] = $this->Model_report->getLeadsByRegion($user_role, $user_id,'','',$check_for_fetching_leads);

		//set variable for which the reports are showing.
		$data['check_for_fetching_leads'] = $check_for_fetching_leads;

		//get news
		$data['newsArr'] = $this->Model_news->getMultipleRows(array(),false,'desc');

		//leads
		$pageNumber = 1;
		$dashboard = 1;
		$users = array();
		$keyWord = "";
		$checkLoggedInUserInboxRole = checkLoggedInUserInboxRole();
		$applyFilter = false;
		$category_id=0;
		$status="";

		//on the dashboard it won't show leads under the manager.
		//Ref to https://docs.google.com/spreadsheets/d/1L4XHwwEbT8uTCCYCrxZsqZ5R131AjkEOGbmE7lmOPzE/edit#gid=1213455002

		$data['leads'] = $this->Model_lead->getAllSortedByPaged($pageNumber, $dashboard, 0, $users, $keyWord, $checkLoggedInUserInboxRole, $applyFilter, $category_id, $status); 

		//set other imp variables for view
		$data['isSingleDetailLead'] = false;
		$data['cities'] = $this->Model_city->getAll();
		$data['logged_in_user_id'] = $this->session->userdata['user']['id'];
		$data['categories'] = $this->Model_category->getAll();
		//$data['get_late_leads'] = $this->Model_lead->getAllLateLeads($data['logged_in_user_id']);        
		$data['loadmore'] = false;
		$data['loadonlyleads'] = false;		
		$data['dashboard'] = true;


		$this->load->view('template',$data);
		
		
		}
	
	
}
