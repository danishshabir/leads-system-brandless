tinymce.init({ 
 mode : "exact",
elements:"desc",
plugins: [
    'advlist',
    'autolink',
    'lists',
    'link',
    'image',
    'charmap',
    'print',
    'preview',
    'anchor',
    'searchreplace',
    'visualblocks',
    'code',
    'fullscreen',
    'insertdatetime',
    'media',
    'table',
    'contextmenu',
    'paste',
    'imagetools'
  ],
  
 });
//Menu nav bar function 
(function() {
  $(function() {
    var collapseMyMenu, expandMyMenu, hideMenuTexts, showMenuTexts;
    expandMyMenu = function() {
      return $("nav.sidebar").removeClass("sidebar-menu-collapsed").addClass("sidebar-menu-expanded");
    };
    collapseMyMenu = function() {
      return $("nav.sidebar").removeClass("sidebar-menu-expanded").addClass("sidebar-menu-collapsed");
    };
    showMenuTexts = function() {
      return $("nav.sidebar ul div.expanded-element").show();
    };
    hideMenuTexts = function() {
      return $("nav.sidebar ul div.expanded-element").hide();
    };
    return $("#justify-icon").click(function(e) {
      if ($(this).parent("nav.sidebar").hasClass("sidebar-menu-collapsed")) {
        expandMyMenu();
        showMenuTexts();
        $(this).css({
          color: "#000"
        });
      } else if ($(this).parent("nav.sidebar").hasClass("sidebar-menu-expanded")) {
        collapseMyMenu();
        hideMenuTexts();
        $(this).css({
          color: "#FFF"
        });
      }
	  $('.menuLinksBtn .sprite.sprite-homeIcon, .menuLinksBtn .sprite.sprite-homeIcon_hvr').toggleClass('sprite-homeIcon sprite-homeIcon_hvr');
	  $('.menuLinksBtn .sprite.sprite-menagment, .menuLinksBtn .sprite.sprite-menagment_hvr').toggleClass('sprite-menagment sprite-menagment_hvr');
	  $('.menuLinksBtn .sprite.sprite-reports, .menuLinksBtn .sprite.sprite-reports_hvr').toggleClass('sprite-reports sprite-reports_hvr');
      return false;
    });
  });

}).call(this);
/*$(function() {
	$( "#date_to_1,#date_to_2,#date_to_3,#date_to_4").datepicker({
	  changeMonth: true,
	  changeYear: true,
	  dateFormat: 'dd/mm/yy',
	  minDate: 0 
	});
	
});*/

function putCookieForReminded(leadActionId,leadId)
{

	var retBackRemindedCookieArr = [];
	if($.cookie('remindedCookie') != null)
	var retBackRemindedCookieArr = JSON.parse($.cookie('remindedCookie'));

	if(retBackRemindedCookieArr.length > 0 && $.inArray( leadActionId, retBackRemindedCookieArr) > -1 )
	{
		//already in cookie
	}
	else
	{
		reminded = [];
		if(retBackRemindedCookieArr.length > 0) //check and include old array values in same cookie.
		reminded = retBackRemindedCookieArr;
		reminded.push(leadActionId);
		
		$.cookie("remindedCookie", JSON.stringify(reminded), { path: '/', expires: 1 }); 
	}

	return true;

}

function checkReminders()
{
	
			$('#timeMessage').html("Please wait.. <img src='"+base_url+"assets/images/black-loader.gif' alt='loading' id='loadingbranch' height='16' width='16' />");
			$('#leadRemain').click();

			//var reminded = [];
			  //your jQuery ajax code
			$.ajax({
					type: "POST",
					url: base_url+'Lead/checkRemainTime',
					data: {},
					dataType:"json",
					cache: false,
					success: function(result){					
						if(result.remindLeadId!='')
						{
							remindLeadActionId = result.remindLeadActionId; //1,2,3,4 ... //this is for if one lead has multiple actions of time30
							remindLeadId = result.remindLeadId; //1,1,2,3, ... //the lead id will be repated if a lead has multiple actions of time30
							remindLeadTrackId = result.remindLeadTrackId; //A,A,B,C ...//the lead trackid will be repated if a lead has multiple actions of time30

							var remindLeadActionIdArr = remindLeadActionId.split(',');
							var remindLeadIdArr = remindLeadId.split(',');
							var remindLeadTrackIdArr = remindLeadTrackId.split(',');

							var generateMessage = "";
							for (i = 0; i < remindLeadIdArr.length; i++) {

								leadActionId = remindLeadActionIdArr[i];
								leadId = remindLeadIdArr[i];
								leadTrackId = remindLeadTrackIdArr[i];

								//alert(leadTrackId);
								//===== same code as in the putCookieForReminded func======
								var retBackRemindedCookieArr = [];
								if($.cookie('remindedCookie') != null)
								var retBackRemindedCookieArr = JSON.parse($.cookie('remindedCookie'));

								if(retBackRemindedCookieArr.length > 0 && $.inArray( leadActionId, retBackRemindedCookieArr) > -1 )
								{
									//already in cookie so its mean user have clicked on it to open it.

									$('#notibell'+leadId+'').html('<i class="fa fa-clock-o" aria-hidden="true"></i>');
									$('#notibell_db'+leadId+'').html('<i class="fa fa-clock-o" aria-hidden="true"></i>');
								}
								else
								{
									if(generateMessage!="") generateMessage+= ", ";
									//generateMessage += "<a style='text-decoration: underline;' onClick=\"return putCookieForReminded('"+leadActionId+"','"+leadId+"')\" target='_blank' href='"+base_url+"lead/singleLead/"+leadId+"'>"+leadTrackId+"</a>";									
									
									generateMessage += "<a style='text-decoration: underline;' target='_blank' href='"+base_url+"lead/singleLead/"+leadId+"'>"+leadTrackId+"</a>";									
									
									$('#notibell'+leadId+'').html('<i class="fa fa-clock-o" aria-hidden="true"></i>');
									$('#notibell_db'+leadId+'').html('<i class="fa fa-clock-o" aria-hidden="true"></i>');

									//so never check for cookie putCookieForReminded(leadActionId,leadId); //previously it was on click event from the popup. now it is auto so reminder is only once.
									//generateReminderNotification(leadId);


								}
								//============================================================								
							}

							if(generateMessage!="")
							{
								generateMessage = "Upcoming scheduled action(s) for lead(s) "+ generateMessage;

								$('#timeMessage').html(generateMessage);
								//$('#leadRemain').click();
							}
							
							
						}
						else
						{
							//no upcoming schedule
							$('#timeMessage').html("No Upcoming Schedules..");
						}

						//call it here because if clock icon is shown first then there is a condition in this function to check it.
						//speedfix checkNotifications(); //basically it should only be called on the leads management listing screen.

					}
				});
}

function generateReminderNotification(leadId)
{
	$.ajax({
		type: "POST",
		url: base_url+'/Lead/generateReminderNotification',
		data: {'lead_id':leadId},
		//dataType:"json",
		cache: false,
		success: function(result)
		{			
			//speedfix getNotificationsHeader(); //now when page will be refreshed then notification will show up.
		}
	});
}

function checkNotifications()
{
			$.ajax({
					type: "POST",
					url: base_url+'/Lead/leadNotificationsAjaxCheck',
					data: {},
					//dataType:"json",
					cache: false,
					success: function(result)
					{
						if(result!='')
						{
							//lead_ids_arr = [];
							//if (result.indexOf(",") >= 0)
								//lead_ids_arr = result.split(",");
							//else
								//lead_ids_arr[0] = result;

							lead_ids_arr = result.split(",");

							for(lna=0; lna<=lead_ids_arr.length; lna++)
							{
								if(document.getElementById('notibell'+lead_ids_arr[lna]+'') !== null)
								{
									
									if($.trim($('#notibell'+lead_ids_arr[lna]+'').html())=='<i class="fa fa-clock-o" aria-hidden="true"></i>')
									{
										// let the clock icon be there.
									}
									else
									{
										$('#notibell'+lead_ids_arr[lna]+'').html('<i class="fa fa-bell"></i>');
									}

									if($('#Schedule_Test'+lead_ids_arr[lna]+'').find("#update_scheduled_message_id").val()!='0' && $('#Call'+lead_ids_arr[lna]+'').find("#update_scheduled_message_id").val()!='0')
									expLeadAjax(lead_ids_arr[lna]);
								}
							}
						}
					
					}
				});
}

function getNotificationsHeader()
{
			$.ajax({
					type: "POST",
					url: base_url+'/Lead/getNotificationsForHeaderAjax',
					data: {},
					//dataType:"json",
					cache: false,
					success: function(result)
					{
						if(result!='')
						{							
							$('#headerNotiTable').html(result);		
							//$('#headerNotiEst').show();
							$('#flashNotifi').show();
							$('#noNotifi').hide();
							$('#nomnhp').hide(); //no more notifications text in header right side
							$('#viewallnha').show(); //view all notifications link in header right side
						}
					
					}
				});
}

function loadMoreStrucPop(dep_id)
{
	$.ajax({
		type: "POST",
		url: base_url+'organization_structure/index/'+dep_id,
		data: {},				
		cache: false,				
		success: function(result){
			
			$('#load_more_struc'+dep_id+'').html(result);
			$('#clickLoadMore'+dep_id+'').click();
		
		}
	});	
}

function loadMoreStrucAjax(dep_id){

		//if "bs-multi-more-nested+dep_id" already appended in "#nestedPops" then first remove it to avoid duplication
		$("#nestedPops #bs-multi-more-nested"+dep_id+"").remove();
	
		$.ajax({
			type: "POST",
			url: base_url+'organization_structure/nestedPops/'+dep_id,
			data: {},				
			cache: false,				
			success: function(result){
				
				$('#nestedPops').append(result);
				loadMoreStrucPop(dep_id); // now call next ajax
			
			}
		});			
	
	
}

var count_check_number = 0;
var count_check_number2 = 0; //for sub leads in list management screen.

function bindDateTimePicker()
{
	if($(".datepicker").length)
	{
		$(".datepicker").datepicker({
			dateFormat: 'dd MM yy',
			altField: ".date_alternate",
			altFormat: "yy-mm-dd",
			showOn: "both",
			buttonImage: base_url+"assets/images/calendar_icon.png",
			buttonImageOnly: true,
			buttonText: "Select date",
			minDate: 0
		});
	}

	if($(".datepicker1").length)
	{
		$(".datepicker1").datepicker({
			dateFormat: 'dd MM yy',
			altField: ".date_alternate1",
			altFormat: "yy-mm-dd",
			showOn: "both",
			buttonImage: base_url+"assets/images/calendar_icon.png",
			buttonImageOnly: true,
			buttonText: "Select date",
			//minDate: 0
		});
	}

	if($(".datepicker2").length)
	{
		$(".datepicker2").datepicker({
			dateFormat: 'dd MM yy',
			altField: ".date_alternate2",
			altFormat: "yy-mm-dd",
			showOn: "both",
			buttonImage: base_url+"assets/images/calendar_icon.png",
			buttonImageOnly: true,
			buttonText: "Select date",
			//minDate: 0
		});
	}

	if($('.timepicker').length)
	{
		$('.timepicker').timepicker({
			timeFormat: 'hh:mm tt',
			altField: ".time_alternate",
			altTimeFormat: "HH:mm",
			showOn: "both",
			buttonImage: base_url+"assets/images/clock_icon.png",
			buttonImageOnly: true,
			buttonText: "Select time",
			stepMinute: 5,
		});
	}
	
	if($('.timepicker1').length)
	{
		$('.timepicker1').timepicker({
			timeFormat: 'hh:mm tt',
			altField: ".time_alternate1",
			altTimeFormat: "HH:mm",
			showOn: "both",
			buttonImage: base_url+"assets/images/clock_icon.png",
			buttonImageOnly: true,
			buttonText: "Select time",
			stepMinute: 5,
		});
	}
	
	if($('.timepicker2').length)
	{
		$('.timepicker2').timepicker({
			timeFormat: 'hh:mm tt',
			altField: ".time_alternate2",
			altTimeFormat: "HH:mm",
			showOn: "both",
			buttonImage: base_url+"assets/images/clock_icon.png",
			buttonImageOnly: true,
			buttonText: "Select time",
			stepMinute: 5,
		});
	}

	if($('.timepicker3').length)
	{
		$('.timepicker3').timepicker({
			timeFormat: 'hh:mm tt',
			altField: ".time_alternate3",
			altTimeFormat: "HH:mm",
			showOn: "both",
			buttonImage: base_url+"assets/images/clock_icon.png",
			buttonImageOnly: true,
			buttonText: "Select time",
			stepMinute: 5,
		});
	}

	if($('.timepicker4').length)
	{
		$('.timepicker4').timepicker({
			timeFormat: 'hh:mm tt',
			altField: ".time_alternate4",
			altTimeFormat: "HH:mm",
			showOn: "both",
			buttonImage: base_url+"assets/images/clock_icon.png",
			buttonImageOnly: true,
			buttonText: "Select time",
			stepMinute: 5,
		});
	}	
	if($('.timepicker5').length)
	{
		$('.timepicker5').timepicker({
			timeFormat: 'hh:mm tt',
			altField: ".time_alternate5",
			altTimeFormat: "HH:mm",
			showOn: "both",
			buttonImage: base_url+"assets/images/clock_icon.png",
			buttonImageOnly: true,
			buttonText: "Select time",
			stepMinute: 5,
		});
	}
	if($('.timepicker6').length)
	{
		$('.timepicker6').timepicker({
			timeFormat: 'hh:mm tt',
			altField: ".time_alternate6",
			altTimeFormat: "HH:mm",
			showOn: "both",
			buttonImage: base_url+"assets/images/clock_icon.png",
			buttonImageOnly: true,
			buttonText: "Select time",
			stepMinute: 5,
		});
	}
}

$( document ).ready(function() {	

	bindDateTimePicker();

	$("#cityDDB").on("change",function(){
		
		$("#branchDDB").prop('disabled', true);
		
		$.ajax({
				type: "POST",
				url: base_url+'lead/branchesByCity',
				data: {'city_id' : $(this).val()},
				//dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
					
					$("#branchDDB").html(result);
					$("#branchDDB").prop('disabled', false);
							
				}
		});
	
	});
	/*$("#branchDDB").on("change",function(){
		
		$("#employeeDDB").prop('disabled', true); 
		
		$.ajax({
				type: "POST",
				url: base_url+'lead/getEmployeeByBranchAndCategoryAjax',
				data: {'branch_id' : $(this).val(),  'category_id' : $("#assign_category_id").val()},
				//dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
					
					$("#employeeDDB").html(result);
					$("#employeeDDB").prop('disabled', false);
							
				}
		});
	
	});*/
	

  // for editor
  //  $("#txtEditor").Editor();
  
//		Dashboard Leads Assigned Detailing options
   $( ".dashboardLeadsAssignedTable table tbody td .showDtl, .userAccountsSection table tbody tr td input" ).click(function() {
		//$( ".dashboardLeadsAssignedTable table tbody tr").removeClass('active');
		$(this).parent().parent().toggleClass('active');
		/*$( ".dashboardLeadsAssignedTable .dashLeadsAssignedDTL").slideUp();
		$(this).next('.dashLeadsAssignedDTL').show();*/
	});
	
// Add class active on Leads page
   $( "#ajaxLoadMoreTBody .leadCheckbox" ).click(function() {
	   $(this).parent().parent().toggleClass('active');
   });

	// Lead page menu sub dropdown function
	function lead_categories() {
		var dropDownHeight = $('.dropdown-menu.leadCategories li:hover ul.innerDDown').height();
		var dropDownHeight = dropDownHeight + 85;
		$('.dropdown-menu.leadCategories ul.parrentlist').css('min-height', dropDownHeight + 'px');
	}
	$('.dropdown-menu.leadCategories ul li').hover(function() {
		lead_categories();
	}), function() {
    	$('.dropdown-menu.leadCategories ul.parrentlist').css('min-height', 'inherit');
	}

	//		Test drive servey active function
	$( ".QAirListing ol li" ).click(function() {
		$(this).nextAll().removeClass('active');
		$(this).addClass('active');
		$(this).prevAll().addClass('active');
	});
	
	$( ".colorSelectionCode ul li" ).click(function() {
		$(this).nextAll().removeClass('active');
		$(this).addClass('active');
		$(this).prevAll().removeClass('active');
	});
	
	//		survey_inner_edit.php
	$( ".deleteRow" ).click(function() {
		$(this).parent().css('display','none');
	});
	$('.EditQAirListing input').click(function(){
		$(this).parent().toggleClass('active');
	});
	//		survey_inner_edit.php
	/*$( ".EditQAirListing li .AddRow" ).click(function() {
		$(this).parent().prev().ne.prepend('ji');
	});*/
	
	
	$('.closeThis').click(function(){
		$(this).parent().parent().parent().removeClass('in');
	});
	
	//	System Setting js Function
		$('.eidtField').click(function(){
			$(this).hasClass('collapsed')
			$(this).toggleClass('active');
			$(this).parent().parent().parent().toggleClass('active');
		});
	
	$(".cancel_button").on("click",function(){	
		window.location.reload();
	});	


	function ajaxAction($form,buttonText)
	{
			
			lead_id = $form.attr('data-ajaxactionleadid');
			dashboard = $form.attr('data-dashboard');

			var attraal = $form.attr('data-ajaxactionleadid');
			
			//check if more then one car slct then display as count else just one.
			var isActiveTestDrive  = $("#myTab").find("li:eq(3)").hasClass("active");
			if(isActiveTestDrive){
				var countCars = $("#project-icon"+lead_id).find("div").length;
			}else{
				var countCars = "0";
			}
			
			$.ajax({
				type: "POST",
				url: base_url+'lead/ajaxAction',
				data: {'lead_id' : lead_id, 'dashboard' : dashboard,'slctCarCount':countCars},
				//dataType:"json",
				cache: false,
				success: function(result){					
					//console.log(result)					

					if( parseInt($form.find('#update_scheduled_message_id').val()) > 0 )
					{
						//do something if you want to do for the update case..
					}
					else
					{
					if($('#ActionBox'+lead_id+'').find('div.row').length > 0)
					{
						$(result).insertAfter('#ActionBox'+lead_id+' div.row:last');

						if(dashboard==1 && $('#ActionBox'+lead_id+'').find('div.row').length > 2)
						{
							$('#ActionBox'+lead_id+' div.row:first').remove();
						}
					}
					else
					{	
						if(dashboard==1)
						{							
							$(result).insertBefore('#ActionBox'+lead_id+' div.dashLeadsAssignedDTL');
						}
						else
						{
							$(result).insertBefore('#ActionBox'+lead_id+' a.AddPlusBtn');						
						}
					}
					}


					if(typeof attraal !== typeof undefined && attraal != '')
					{
						$form.find(':submit,:button').prop('disabled', false);
						$form.find(':submit,:button').attr('value', buttonText);

						//leads listing screen. when action is added then hide the action adding box
						if(typeof $('#AddPlusAreaCreateAnAction'+lead_id+'') !== typeof undefined) 
						$('#AddPlusAreaCreateAnAction'+lead_id+'').hide();
					}
					
					
					if(
								(typeof $form.find("#update_scheduled_message_id").val() !== typeof undefined && $form.find("#update_scheduled_message_id").val()!=="0")
								||
								(typeof $form.find("[name='message_id']").val() !== typeof undefined && $form.find("[name='message_id']").val()==="11")
								||
								(typeof $form.find("[name='message_id']").val() !== typeof undefined && $form.find("[name='message_id']").val()==="5" && $("#project-icon"+attraal+" div").length > 1)
							  )
							{
								//the scheduled test drive is being updated so rexpand the lead to show the updated data.
								expLeadAjax(lead_id); //attraal is leadid								
							}
					
					
					
					
				}
			});

	}


	//$(".mb_form").submit(function(e){
	$(document).on('submit','.mb_form',function(e){

		if($(this).attr('id') == 'tinymce_form')
		{
		 tinyMCE.triggerSave();
		}
		if($(this).attr('id') == 'late_lead_form')
		{
		   $('#nModeEdFtr').modal('hide');
		}
		e.preventDefault();
		$form = $(this);


		var msgType = $form.find("#type");
		if(typeof msgType !== typeof undefined && msgType.val() == 2)
		{
			//before sending email, confirm
			if(confirm("Are you sure you want to send email?"))
			{
				//do-nothing and let it go further in this function.
				//check 3 words count
				var emailMsgWordLength = ($form.find("[name='comments']").val().split(" ")).length
				if(emailMsgWordLength<=2)
				{
					alert("The email message must be at least 3 words.");
					return false;
				}
			}
			else
			{
				return false; //stop it from here for further going in this function
			}
		}

		var attraal = $form.attr('data-ajaxactionleadid');
		if(typeof attraal !== typeof undefined && attraal != '')
		{
			$form.find(':submit,:button').prop('disabled', true);
			var buttonText = $form.find(':submit,:button').attr('value');
			$form.find(':submit,:button').attr('value', 'Please Wait..');
		}

		$.ajax({
				type: "POST",
				url: $form.attr('action'),
				data: $form.serialize(),
				dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
				
					if(result.error != 'false'){
						$("#mySmallModalLabel").html('Error');
						$("#message").html(result.error);
                         if(result.model_hide)    
                        $('.leadCreateSussesslogin').modal('hide');  
						document.getElementById("show_success_messge").click();
						
					}else{
						count_check_number = 0;
						$("#mySmallModalLabel").html('Success');
						$("#message").html(result.success);
						if(result.reset)
						{
							$form[0].reset();

							//after reset, make these radio buttons selected again
							if(typeof attraal !== typeof undefined && attraal != '')
							{
								if($('#ayrba'+attraal+'').length)
								{
									$('#ayrba'+attraal+'').click();
								}
								if($('#typeUnscheduled'+attraal+'').length)
								{
									$('#typeUnscheduled'+attraal+'').click();
								}								
							}
						}
						if(result.reload)
							window.location.reload();
                         if(result.model_hide)    
                        $('.leadCreateSussesslogin').modal('hide');                               						
						
						
						if(typeof attraal !== typeof undefined && attraal != '')
						{								
							
							ajaxAction($form,buttonText);
							
						}
						else
						{
							if(result.success != "Survey Submitted")
							{
								document.getElementById("show_success_messge").click();
							}
						}

						//when assigning then close the previous popup
						if(typeof $form.attr('data-assignleadfrm') !== typeof undefined && $form.attr('data-assignleadfrm') == '1')
						{
							$form.find(":button.close").click();

							//update the assignes names in listing
							lead_ids_assigned = $('#assign_lead_id').val(); //comma separated							
							var lead_ids_assigned_arr = lead_ids_assigned.split(',');
							for (i = 0; i < lead_ids_assigned_arr.length; i++) {

								$('#assigneeName'+lead_ids_assigned_arr[i]+'').html(result.employeeName);

							}
							
							//deselect all the checkboxes
							$('.leadCheckbox').each(function() { //loop through each checkbox
								if(this.checked)
								{
									//this.checked = false; //deselect all checkboxes                    
									this.click(); 
								}
							}); 
						}

						//when assigning then close the previous popup
						if(typeof $form.attr('data-leadfrm') !== typeof undefined && $form.attr('data-leadfrm') == '1')
						{ 
							//$("#project-icon").html("");
							//$("#project-id").val("");
							//10 Jan 2017
							var resetCarDiv = $('[type=submit]').val();
							if(resetCarDiv != "Update"){
								$("#project-icon").html("");
								$("#project-id").val("");
							}

							/*=== redirect to back if lead created again=====*/
							var createAgain  = $('#createAgain').val();
							if(createAgain == "createAgain"){
								window.location.href = base_url+"lead";
							}
							
						}
						var createAppointment = $form.find("#createAppointment");
						if(typeof createAppointment !== typeof undefined && createAppointment.val() == 1)
						{
							//alert(result.lead_id);
							window.location.href = base_url+"appointment?lead_id="+result.lead_id;
						}						
					}
				
				}
		});
	});
	
	$("#mb_form_login").submit(function(e){

		e.preventDefault();
		$form = $(this);
		$.ajax({
				type: "POST",
				url: $form.attr('action'),
				data: $form.serialize(),
				dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
				 
					if(result.error != 'false'){
						$("#mySmallModalLabel").html('Error');
						$("#message").html(result.error);
						document.getElementById("show_success_messge").click();
						
					}else{	
						if(result.redirect == 'appointment')
							window.location.href = base_url+"appointment";
						else						
							window.location = base_url+'dashboard';
					}
				
				}
		});
	});	
	
	$("#user_action_form").submit(function(e){
		e.preventDefault();
		$form = $(this);
		$.ajax({
				type: "POST",
				url: $form.attr('action'),
				data: $form.serialize(),
				dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
				
					if(result.error == 'false'){
						var row_id = '#user-'+$("#user_id_action_form").val();
						if(result.is_suspend != 1){
							$(row_id).remove();
						}
						$form[0].reset();
						$("#mySmallModalLabel").html('Success');
						$("#message").html(result.success);
						document.getElementById("show_success_messge").click();
						if(result.redirect == 'user')
						{
							window.location.href = base_url+"user";
						}	
					}
				
				}
		});
	});	
	 
/*	$('#surveyForm')
	.on('click', '.addButton', function() {
		var $template = $('#optionTemplate'),
			$clone    = $template
							.clone()
							.removeClass('hide')
							.removeAttr('id')
							.insertBefore($template),
			$option   = $clone.find('[name="option[]"]');

		// Add new field
	}).on('click', '.removeButton', function() {
		var $row    = $(this).parents('.form-group'),
			$option = $row.find('[name="option[]"]');

		// Remove element containing the option
		$row.remove();
	})*/
	
	$('#CreateSurvey')
	.on('click', '.addButton', function() {
		var $template = $('#CreateSurveyTemplate'),
			$clone    = $template
							.clone()
							.removeClass('hide')
							.removeAttr('id')
							.insertBefore($template),
			$option   = $clone.find('[name="option[]"]');

		// Add new field
	}).on('click', '.removeButton', function() {
		var $row    = $(this).parents('.form-group'),
			$option = $row.find('[name="option[]"]');

		// Remove element containing the option
		$row.remove();
	})
   /* $('#addMoreSurQ').on('click', function() {
        
        alert("check3");
        
       
        
        $( "#newQAppend" ).append( '<div class="form-group" class="CreateSurveyTemplate"><div class="col-sm-1"><button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button></div><div class="col-sm-11"><textarea cols="" rows="0" placeholder="Description..." name="question[]"></textarea><select name="answer[]"><option value="Rating">Rating 1-5</option><option value="Yes or No">Yes or No</option><option value="Textarea">Textarea</option></select></div>' );
        
     }); */
    				
				$( ".checkbox_click_count" ).on( "click", function() {
				    
                    
                    
                   if($(this).is(':checked')){					  
						count_check_number++;                        
                   }else{
                        count_check_number--;                        
                   }
                    
				  if(count_check_number > 3)
				  {
					 alert('You can only select up to 3 vehicles.');
					 $(this).attr('checked', false);
					 count_check_number--;
				  }

				});		
	
 //    if($("#mobile-number").length){
		jQuery(function($){
		   //$("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
		   if($('#mobile-number').length > 0) {
			$("#mobile-number").mask("(+999) 999999999"); //+966 504622444
		   }
		   //$("#tin").mask("99-9999999");
		   //$("#ssn").mask("999-99-9999");
		});
	// }



		$('#ChkAll').click(function(e) {  //on click 			
			if(this.checked) { // check select status
				$('.leadCheckbox').each(function() { //loop through each checkbox
					this.checked = true;  //select all checkboxes with class "checkbox1"               
				});
			}else{
				$('.leadCheckbox').each(function() { //loop through each checkbox
					this.checked = false; //deselect all checkboxes with class "checkbox1"                       
				});         
			}
		});

		
		//alert message when 30 min left for call or test drive scheduled.		
		if(typeof is_lead_mng_screen !== typeof undefined && is_lead_mng_screen) //speedfix
		{
			setInterval(function() {
				
				//checkReminders();			
				//speedfix getNotificationsHeader();

				//speedfix increased the time to 10 min instead of every 1 min
			}, 1000 * 60 * 10); // where X is your every X minutes
		}

		//by default call this function when page reloads too.
		//speedfix checkReminders();

});




function userAction(id,action){
	
	$("#user_id_action_form").val(id);
	$("#user_action_form_type").val(action);
	$('#user_action_form').prop('action', base_url+'user/userAction');
	$("#user_action_form").submit();
	
}


function deleteCheckBoxesRec(checkBoxClass,actionUrl,reloadUrl)
{
	var checkBoxesSelectedCommas = "";

	$('input:checkbox.'+checkBoxClass+'').each(function () {
		if(this.checked)
		{
			if(checkBoxesSelectedCommas!="") checkBoxesSelectedCommas += ",";
			checkBoxesSelectedCommas +=  $(this).val();
		}
	});

	if(checkBoxesSelectedCommas!="")
	{
		deleteRecord(checkBoxesSelectedCommas,actionUrl,reloadUrl);

	}else
	{
		$("#mySmallModalLabel").html('Error');
		$("#message").html('Please select one ore more leads.');
		document.getElementById("show_success_messge").click();
	}
	

}


function deleteRecord(id,actionUrl,reloadUrl)
{

	//id can contain comma separated ids too.
	
	if(confirm("Are you sure you want to delete?"))
	{
		$.ajax({
				type: "POST",
				url: base_url+''+actionUrl,
				data: {'id' : id, 'form_type' : 'delete'},
				dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
				
			if(result.error != 'false'){
				$("#mySmallModalLabel").html('Error');
				$("#message").html(result.error);
				document.getElementById("show_success_messge").click();
				
			}else{
				$('#'+id).remove();
				$("#mySmallModalLabel").html('Success');
				$("#message").html(result.success);
				document.getElementById("show_success_messge").click();
				if(reloadUrl!="") document.location.href = reloadUrl;
				}
				
			}
		});
	}
	
}

function deleteRecordSystemScreen(id,actionUrl,reloadUrl,row_id)
{

	//id can contain comma separated ids too.
	
	if(confirm("Are you sure you want to delete?"))
	{
		$.ajax({
				type: "POST",
				url: base_url+''+actionUrl,
				data: {'id' : id, 'form_type' : 'delete'},
				dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
				
			if(result.error != 'false'){
				$("#mySmallModalLabel").html('Error');
				$("#message").html(result.error);
				document.getElementById("show_success_messge").click();
				
			}else{
				$('#'+row_id).remove();
				$("#mySmallModalLabel").html('Success');
				$("#message").html(result.success);
				document.getElementById("show_success_messge").click();
				if(reloadUrl!="") document.location.href = reloadUrl;
				}
				
			}
		});
	}
	
}

function suspendCheckBoxesRec(checkBoxClass,actionUrl,reloadUrl)
{
	var checkBoxesSelectedCommas = "";

	$('input:checkbox.'+checkBoxClass+'').each(function () {
		if(this.checked)
		{
			if(checkBoxesSelectedCommas!="") checkBoxesSelectedCommas += ",";
			checkBoxesSelectedCommas +=  $(this).val();
		}
	});

	suspendRecord(checkBoxesSelectedCommas,actionUrl,reloadUrl)

}

function suspendRecord(id,actionUrl,reloadUrl)
{

	//id can contain comma separated ids too.
	
	if(confirm("Are you sure you want to suspend ?"))
	{
		$.ajax({
				type: "POST",
				url: base_url+''+actionUrl,
				data: {'id' : id, 'form_type' : 'suspend'},
				dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
				
			if(result.error != 'false'){
				$("#mySmallModalLabel").html('Error');
				$("#message").html(result.error);
				document.getElementById("show_success_messge").click();
				
			}else{
				//$('#'+id).remove();
				$("#mySmallModalLabel").html('Success');
				$("#message").html(result.success);
				document.getElementById("show_success_messge").click();
				if(reloadUrl!="") document.location.href = reloadUrl;
				}
				
			}
		});
	}
	
}

function selectMultipleCheckBoxes(source, checkbox_name) {
     checkboxes = document.getElementsByName(checkbox_name);
     for(var i=0, n=checkboxes.length;i<n;i++) {
       checkboxes[i].checked = source.checked;
    }
 }
 
function getStucture(city_id)
{
     window.location= base_url+'Organization_structure/index/'+city_id;
            
} 


var page = 2;
var sortType = '';
var filterKeyword = '';
var AscDesc = '';
//var filterConnectedCount = 0;


function sortLeads(sortBy, ref)
{	

	$('#loading'+sortBy).show();

	page = 2;

	sortType = sortBy;

	AscDesc = ref.attr('data-AscDesc');

	var search_keyword = "";

	if(getParameterByName('search_keyword')!=null && getParameterByName('search_keyword')!="")
	{
		search_keyword = getParameterByName('search_keyword');
	}

	$.ajax({
			type: "GET",
			url: window.location.pathname, //for the filter it will get the filter path too to maintain the state
			data: {'page':1, 'sortType':sortBy, 'AscDesc': ref.attr('data-AscDesc'), 'search_keyword': search_keyword},
			cache: false,
			success: function(result){
			
				$("#ajaxLoadMoreTBody").html(result);
				$('#loading'+sortBy).hide();

				$('#loadedLeadsCount').text("10");
			
			}
	});

	
	//save state in the same <th>
	if(ref.attr('data-AscDesc')=='Asc')
	{
		ref.attr('data-AscDesc','Desc');
		$('.sortTh span').html('<i class="fa fa-sort" aria-hidden="true"></i>');
		ref.find('span').html('<i class="fa fa-sort-asc" aria-hidden="true"></i>');		
	}
	else
	{
		ref.attr('data-AscDesc','Asc');
		$('.sortTh span').html('<i class="fa fa-sort" aria-hidden="true"></i>');
		ref.find('span').html('<i class="fa fa-sort-desc" aria-hidden="true"></i>');		
	}
	//==

}

function sortLeadsExcel(sortBy, oderAscOrDesc)
{	

	if(sortBy=="ntf")
	{
		$("#ajaxLoadMoreTBody").html("<tr><td colspan='10'><div style='text-align:center;'><h3>Please wait..</h3><img src='"+base_url+"assets/images/black-loader.gif' alt='loading' id='loadingbranch' height='16' width='16' /></div></td></tr>");
	}
	
	AscDesc = oderAscOrDesc; //AscDesc is a global

	page = 2;

	sortType = sortBy;

	var search_keyword = "";

	if(getParameterByName('search_keyword')!=null && getParameterByName('search_keyword')!="")
	{
		search_keyword = getParameterByName('search_keyword');
	}

	$.ajax({
			type: "GET",
			url: window.location.pathname, //for the filter it will get the filter path too to maintain the state
			data: {'page':1, 'sortType':sortBy, 'AscDesc': AscDesc, 'search_keyword': search_keyword, 'filter_keyword': filterKeyword},
			cache: false,
			success: function(result){
			
				$("#ajaxLoadMoreTBody").html(result);
				
				var rowsCount = (result.match(/<tr class="First">/g) || []).length;
				if($.trim(result)=="" || rowsCount<10 || sortBy=="ntf")
					$('.loadMore').hide();					
				else
					$('.loadMore').show();


				$('#loadedLeadsCount').text(rowsCount);
			
				$('#totalLeadsCount').hide();
				$('#ofWord').hide();

			
				$('#btn_'+sortBy+'').click();
			}
	});

	
	//save state in the same <th>
	if(AscDesc=='Asc')
	{
		//ref.attr('data-AscDesc','Desc');
		$('.sortIconGen').html('<i class="fa fa-sort" aria-hidden="true"></i>');
		$('.sortIcon_'+sortBy+'').html('<i class="fa fa-sort-asc" aria-hidden="true"></i>');		
	}
	else
	{
		//ref.attr('data-AscDesc','Asc');
		$('.sortIconGen').html('<i class="fa fa-sort" aria-hidden="true"></i>');
		$('.sortIcon_'+sortBy+'').html('<i class="fa fa-sort-desc" aria-hidden="true"></i>');		
	}
	//==

}


/*
function filterLeads(filterBy, ref, filterByValue)
{	

	$('#connMsg').hide();

	if(filterByValue=='All') document.location.href=window.location.pathname;
	
	var filterByValueArr = filterByValue.split('|');
	filterKeyword = filterByValueArr[0];
	filterTotalCount = filterByValueArr[1];
	filterConnectedCount = filterByValueArr[2]; //filterConnectedCount variable is global


	var url = base_url+'lead/index/';

	$('#loading'+filterBy).show();
	
	//if(filterBy == 'duedate')
	//{
	//	url = url+'filter/All/'+filterKeyword; //filterKeyword is global variable
	//}
	
	
	
	page = 2;

	//sortType = filterBy;

//AscDesc = ref.attr('data-AscDesc');

	var search_keyword = "";

	if(getParameterByName('search_keyword')!=null && getParameterByName('search_keyword')!="")
	{
		search_keyword = getParameterByName('search_keyword');
	}

	$.ajax({
			type: "GET",
			url: url, //for the filter it will get the filter path too to maintain the state
			data: {'page':1,'search_keyword': search_keyword, 'filter_keyword': filterKeyword},
			cache: false,
			success: function(result){
			
				$("#ajaxLoadMoreTBody").html(result);
				$('#loading'+filterBy).hide();

				//$('#loadedLeadsCount').text("10");				
				var rowsCount = (result.match(/<tr class="First">/g) || []).length;
				if($.trim(result)=="" || rowsCount<10)
				{
					$('.loadMore').hide();					
				}
				$('#loadedLeadsCount').text(rowsCount);

				//$('#totalLeadsCount').text(filterTotalCount + ' ('+filterConnectedCount+' are Connected) ');				
				$('#totalLeadsCount').hide();
				$('#ofWord').hide();

				if(filterTotalCount>10) $('.loadMore').show();
			
			}
	});

	

}*/

//function filterLeadsExcel(filterBy, ref, filterByValue)
function filterLeadsExcel(chk_class)
{	

	$('.sortIconGen').html('<i class="fa fa-sort" aria-hidden="true"></i>');

	var filterByValue = "";
	//var filterTotalCount = 0;
	//filterConnectedCount = 0; //reset it //filterConnectedCount variable is global
	filterKeyword = ""; //reset it //filterKeyword variable is global
	
	
		var branchFil = "";
		var eventFil = "";
		var sourceFil = "";
		var assigntoFil = "";
		var duedateFil = "";
		$('input:checkbox.filterChks').each(function () {
			if(this.checked)
			{
				var filterByValueArr = $(this).val().split('|');

				if($(this).hasClass('branch'))
				{
					if(branchFil!="") branchFil += '-'+filterByValueArr[0].split('-')[1];
					else branchFil = filterByValueArr[0];
				}
				if($(this).hasClass('event'))
				{
					if(eventFil!="") eventFil += '-'+filterByValueArr[0].split('-')[1];
					else eventFil = filterByValueArr[0];
				}
				if($(this).hasClass('source'))
				{
					if(sourceFil!="") sourceFil += '::'+filterByValueArr[0].split('::')[1];
					else sourceFil = filterByValueArr[0];
				}
				if($(this).hasClass('assignto'))
				{
					if(assigntoFil!="") assigntoFil += '-'+filterByValueArr[0].split('-')[1];
					else assigntoFil = filterByValueArr[0];
				}
				if($(this).hasClass('duedate'))
				{
					if(duedateFil!="") duedateFil += '-'+filterByValueArr[0];
					else duedateFil = filterByValueArr[0];
					
				}									
				
				//filterTotalCount += parseInt(filterByValueArr[1]);
				//filterConnectedCount += parseInt(filterByValueArr[2]); //filterConnectedCount variable is global

			}
		});

		filterKeyword = branchFil+'||'+sourceFil+'||'+assigntoFil+'||'+duedateFil+'||'+eventFil; //filterKeyword variable is global
	

	$('#connMsg').hide();	

	var url = base_url+'lead/index/';

	$('#loading'+chk_class).show();
	
	
	page = 2;


	var search_keyword = "";

	if(getParameterByName('search_keyword')!=null && getParameterByName('search_keyword')!="")
	{
		search_keyword = getParameterByName('search_keyword');
	}

	$.ajax({
			type: "GET",
			url: url, //for the filter it will get the filter path too to maintain the state
			data: {'page':1,'search_keyword': search_keyword, 'filter_keyword': filterKeyword},
			cache: false,
			success: function(result){
			
				$("#ajaxLoadMoreTBody").html(result);
				$('#loading'+chk_class).hide();
			
				var rowsCount = (result.match(/<tr class="First">/g) || []).length;
				if($.trim(result)=="" || rowsCount<10)
					$('.loadMore').hide();					
				else
					$('.loadMore').show();


				$('#loadedLeadsCount').text(rowsCount);
			
				//$('#totalLeadsCount').hide();
				//$('#ofWord').hide();
				
				$.ajax({
						type: "GET",
						url: url, //for the filter it will get the filter path too to maintain the state
						data: {'excelFilRC':1,'page':1,'search_keyword': search_keyword, 'filter_keyword': filterKeyword},
						cache: false,
						success: function(result){
						
							$('#totalLeadsCount').html(result);
						
						}
				});
				
				
				
				
				
				

				$('#btn_'+chk_class+'').click();
			
			}
	});

	

}

function loadMoreLeads(base_url)
{

	$('#mbCom-loader').show();

	var search_keyword = "";

	if(getParameterByName('search_keyword')!=null && getParameterByName('search_keyword')!="")
	{
		search_keyword = getParameterByName('search_keyword');
	}
	$.ajax({
			type: "GET",
			url: window.location.pathname, //for the filter it will get the filter path too to maintain the state
			data: {'page' : page, 'sortType': sortType, 'AscDesc':AscDesc , 'search_keyword': search_keyword, 'filter_keyword': filterKeyword},			
			cache: false,
			success: function(result){
			
				//console.log(result);
				var rowsCount = (result.match(/<tr class="First">/g) || []).length;

				$('#ajaxLoadMoreTBody').append(result);
				$('#mbCom-loader').hide();
				if($.trim(result)=="" || rowsCount<10)
				{
					$('.loadMore').hide();
					//if(filterKeyword!="" && filterConnectedCount>0)
					//$('#connMsg').show();
				}
				$('#loadedLeadsCount').text(parseInt($('#loadedLeadsCount').text())+rowsCount);
			}
	});

	page++;

}


function notificationRead(id, lead_id)
{

	$.ajax({
			type: "POST",
			url: base_url+'lead/setNotificationRead',
			data: {'id' : id, 'lead_id' : lead_id},			
			cache: false,
			success: function(result){
			
				document.location.href = base_url+'lead/singleLead/'+lead_id;

			}
	});
	
}
// clear all notifications
function clearAllNotification()
{ 	if(confirm("Are you sure you want to clear all?"))
	{
		$.ajax({
				type: "POST",
				url: base_url+'lead/clearAllNotification',		
				cache: false,
				success: function(success){
					if(success == 1){
						document.location.reload();
					}else{
						alert("Unkwon error, please try again!")
					}
				}
		});
	}
}
function allNotificationRead(lead_id)
{

	$.ajax({
			type: "POST",
			url: base_url+'lead/allNotificationRead',
			data: {'lead_id' : lead_id},			
			cache: false,
			success: function(result){			
				//do nothing
			}
	});	

	$('#notibell'+lead_id+'').html(''); //hide bell icon while expanding detail
	$('.hnt'+lead_id+'').remove();	//remove tr from top header 
	if($('#headerNotiTable tr').length === 0) {	//check if no more tr then hide the red estric
		$('#flashNotifi').hide();
		$('#noNotifi').show();
		$('#nomnhp').show(); //no more notifications text in header right side
		$('#viewallnha').hide(); //view all notifications link in header right side
	}
	
}


function removeUserFromStr(userid)
{

	$.ajax({
			type: "POST",
			url: base_url+'organization_structure/orgAction',
			data: {'id' : userid, 'form_type': 'removeUser'},			
			cache: false,
			success: function(result){
			$("#mySmallModalLabel").html('Success');
			$("#message").html('User remove successfully');
			document.getElementById("show_success_messge").click();
		    document.location.reload();

			}
	});	
	
}

/*function beforeAssign(leadid,category_id)
{
	$('#cityDDB :nth-child(1)').prop('selected', true); 	
	$('#branchDDB').html('<option value= "">Please Select</option>'); 
	$('#employeeDDB').html('<option value= "">Please Select</option>'); 
	$('#assign_lead_id').val(leadid); 
	$('#assign_category_id').val(category_id);
}*/

/**** Ed 05-Apr-2016	****/
		//		Text and input feild border color
jQuery(document).ready(function($) {
	$("input[type='text'],input[type='email'], textarea, select").focus(function(){
		$("input[type='text'],input[type='email'], textarea, select").removeClass("blueBorder");
		$(this).addClass("blueBorder");	
	}).blur(function(){
		$("input[type='text'],input[type='email'], textarea, select").removeClass("blueBorder");
		$(this).addClass("blueBorder");
	})
});
		//	report.php Select Branches function
jQuery(document).ready(function($) {
	$("#showBranches").click(function(){
		$('#BranchesList').toggle();
	})
});
jQuery(document).ready(function($) {
	$(".reportPageN .reportPageNTOP ul#BranchesList > li > input[type='checkbox']").change(function() {
		if(this.checked) {
			$(this).siblings('ul').children().children('input').prop('checked', $(this).prop("checked"));			
		}else {	
			$(this).siblings('ul').children().children('input').prop('checked', $(this).prop("checked"));
		}			
	});
		$(".reportPageN .reportPageNTOP ul#BranchesList > li:nth-child(2) > input[type='checkbox']").change(function() {	
		if(this.checked) {
			$(this).parent().siblings('li').find("input[type='checkbox']").prop('checked', true);		
		}else {	
			$(this).parent().siblings('li').find("input[type='checkbox']").prop('checked', false);		
		}		
	});
});


function beforeAssignPopup(lead_id,city_name,branch_name,city_id,branch_id)
{

	$("#employeeDDB").prop('disabled', true);

	$('#assign_lead_id').val(lead_id); 
	$('#lead_city_for_assign').text(city_name); 
	$('#lead_branch_for_assign').text(branch_name); 
	$('#assign_lead_city').val(city_id); 
	$('#assign_lead_branch').val(branch_id);

	

	$.ajax({
		type: "POST",
		url: base_url+'lead/usersByCityAndBranchAjax',
		data: {'city_id' : $('#assign_lead_city').val(), 'branch_id' : $('#assign_lead_branch').val()},
		//dataType:"json",
		cache: false,
		//async:false,
		success: function(result){
			
			$("#employeeDDB").html(result);
			$("#employeeDDB").prop('disabled', false);
					
		}
	});
}

function leadAssignDDBWhileCreatingLeadAjax()
{

	$("#employeeDDB").prop('disabled', true);

	city_id = $('#cityDDB').val();	
	branch_id = $('#branchDDB').val();

	$('#lead_city_for_assign').text($("#cityDDB option:selected").text()); 
	$('#lead_branch_for_assign').text($("#branchDDB option:selected").text()); 	

	$.ajax({
		type: "POST",
		url: base_url+'lead/usersByCityAndBranchAjax',
		data: {'city_id' : city_id, 'branch_id' : branch_id},
		//dataType:"json",
		cache: false,
		//async:false,
		success: function(result){
			
			$("#employeeDDB").html(result);
			$("#employeeDDB").prop('disabled', false);
					
		}
	});
}


/*function checkIfAllSelLeadsFromSameCityAndBranch(lead_ids)
{
		$.ajax({
			type: "POST",
			url: base_url+'lead/leadAction',
			data: {'lead_ids' : lead_ids, 'form_type' : 'check_leads_same_city_branch'},
			dataType:"json",
			cache: false,
			success: function(result){				
				if(result.success=='1')
				{					
					beforeAssignPopup(lead_ids,result.city_name,result.branch_name,result.city_id,result.branch_id);
					$('#multiAssignPop').click();
				}
				else
				{					
					$("#mySmallModalLabel").html('Error');
					$("#message").html('Selected Leads are not from same City and Branch.');
					document.getElementById("show_success_messge").click();	
				}
			}
	});
}*/

function checkIfAllSelLeadsFromSameCityAndBranch(lead_ids)
{							
	beforeAssignPopup(lead_ids,"N/A","N/A",0,0);
	$('#multiAssignPop').click();
}

function assignMultiLeads()
{

	var lead_ids = "";

	$('input:checkbox.leadCheckbox').each(function () {
		if(this.checked)
		{
			if(lead_ids!="") lead_ids += ",";
			lead_ids +=  $(this).val();
		}
	});

	//alert(checkBoxesSelectedCommas);

	//check if all the selected leads are from same city and branch and further process will be done in this function.
	if(lead_ids!="")
	{
		checkIfAllSelLeadsFromSameCityAndBranch(lead_ids);
	}else
	{
		$("#mySmallModalLabel").html('Error');
		$("#message").html('Please select one ore more leads.');
		document.getElementById("show_success_messge").click();
	}

}


jQuery(document).ready(function($) {
	$('.category_no').click(function() {
		$('.category_no').removeClass('active');
		$(this).addClass('active');
		var category_no = $( ".category_no" ).index( this );
		category_no++; 
		$("#CategoriesGrapf li").hide();
		$( "#CategoriesGrapf li:nth-child( "+category_no+")").fadeIn("slow");
		drawVehChart();	
		drawVehChartCity();	
		drawVehChartTotalLead();	
		drawVehChartUser();	
	});
});
//		Browse Leads navigation system
$(document).mouseup(function (e)
{
    var container = $(".leadCategoriesFilter");
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
       // container.hide();
	   $('.dropdown.leadCategoriesFilter .dropdown-menu.leadCategories').hide();
    }
});
//		
$(document).mouseup(function (e)
{
    var container = $(".org_strSec .bigBoxOS");
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
       // container.hide();
	   $('.org_strSec .bigBoxOS.menuBtnOn').removeClass('menuBtnOn');
    }
});
$(document).mouseup(function (e)
{
    var container = $(".org_strSec .smallBoxOS");
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
       // container.hide();
	   $('.org_strSec .smallBoxOS.menuBtnOn').removeClass('menuBtnOn');
    }
});
$(document).mouseup(function (e)
{
    var container = $(".dashboardSecMain");
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    	{ $('.dashboardSecMain .dashBordSec .dropdown-menu').hide(); }
});
$(document).mouseup(function (e)
{
    var container = $(".leadsListingSec .reportSec");
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    	{ $('.leadsListingSec .reportSec.dropdown .dropdown-menu').hide(); }
});
jQuery(document).ready(function($) {
	$('.dropdown.leadCategoriesFilter #leadCategories').click(function(e) {
        $('.dropdown.leadCategoriesFilter .dropdown-menu.leadCategories').slideToggle();
    });	// only show sub menu
	
	$('.addEmpSec button#dLabel, .dashboardSecMain .dashBordSec button#dLabel, .leadsListingSec .reportSec button#dLabel').click(function(e){
		//$('.org_strSec .bigBoxOS, .org_strSec .smallBoxOS').removeClass('menuBtnOn');
		//$(this).parent().parent().toggleClass('menuBtnOn');
		//$('.org_strSec .bigBoxOS:not(.menuBtnOn) .addEmpSec .dropdown-menu, .org_strSec .smallBoxOS:not(.menuBtnOn) .addEmpSec .dropdown-menu').hide();
		//.org_strSec .bigBoxOS.menuBtnOn .addEmpSec .dropdown-menu, .org_strSec .smallBoxOS.menuBtnOn .addEmpSec .dropdown-menu,
		$(' .dashboardSecMain .dashBordSec .dropdown-menu, .leadsListingSec .reportSec.dropdown .dropdown-menu').slideToggle();
		//$('.newStandDropDown').removeClass('active');
	});
	
	var menuFuncLLb = 1025;
	var window_width = $( window  ).width();
	if (window_width < menuFuncLLb) {
	//	alert('less Then');
		$( ".dropdown-menu.leadCategories > ul > li" ).click(function() {
			$( ".dropdown-menu.leadCategories > ul > li" ).removeClass('active');
			$(this).addClass('active');
			var browseLeadsNo = $( ".dropdown-menu.leadCategories > ul > li" ).index( this );
			browseLeadsNo++;
			$(".dropdown-menu.leadCategories > ul > li ul.innerDDown").hide();
			$( ".dropdown-menu.leadCategories > ul > li:nth-child( "+browseLeadsNo+") ul.innerDDown").show();
		});
	}
});
//	Slider function Animation
$(window).resize(function () {
	//	slider_Function();	
	var menuFuncLLb = 1025;
	var window_width = $( window  ).width();
	if (window_width < menuFuncLLb) {
	//	alert('less Then');
		$( ".dropdown-menu.leadCategories > ul > li" ).click(function() {
			$( ".dropdown-menu.leadCategories > ul > li" ).removeClass('active');
			$(this).addClass('active');
			var browseLeadsNo = $( ".dropdown-menu.leadCategories > ul > li" ).index( this );
			browseLeadsNo++;
			$(".dropdown-menu.leadCategories > ul > li ul.innerDDown").hide();
			$( ".dropdown-menu.leadCategories > ul > li:nth-child( "+browseLeadsNo+") ul.innerDDown").show();
		});
	}
});
/*****	29-Apr-2016		*****/
$(document).mouseup(function (e)
{
    var container = $(".newStandDropDown .dropdown-menu");
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    	{ $('.newStandDropDown .dropdown-menu').hide(); }
});

//		Browse Leads navigation system
$(document).mouseup(function (e)
{
    var container = $("#rep_dep_user_selec");
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
       // container.hide();
	   $('#rep_dep_user_selec .dropdown-menu').hide();
    }
});

function strucManClick(ref)
{
			if(ref.parent().hasClass('active')) {
			$('.org_strSec .bigBoxOS .newStandDropDown .dropdown-menu, .org_strSec .smallBoxOS .newStandDropDown .dropdown-menu').slideUp();
			$('.newStandDropDown').removeClass('active');
		} else {
			$('.org_strSec .addEmpSec .dropdown-menu').slideUp();
			$('.newStandDropDown').removeClass('active');
			ref.parent().addClass('active');
			$('.org_strSec .bigBoxOS, .org_strSec .smallBoxOS').removeClass('menuBtnOn');
			ref.parent().parent().parent().addClass('menuBtnOn');
			$('.org_strSec .bigBoxOS .newStandDropDown .dropdown-menu, .org_strSec .smallBoxOS .newStandDropDown .dropdown-menu').hide();
			$('.org_strSec .bigBoxOS.menuBtnOn .newStandDropDown.active .dropdown-menu, .org_strSec .smallBoxOS.menuBtnOn .newStandDropDown.active .dropdown-menu').slideDown();
		};
}


jQuery(document).ready(function($) {
	$('.newStandDropDown #dLabel').click(function(e) {


		/*if($(this).parent().hasClass('active')) {
			$('.org_strSec .bigBoxOS .newStandDropDown .dropdown-menu, .org_strSec .smallBoxOS .newStandDropDown .dropdown-menu').slideUp();
			$('.newStandDropDown').removeClass('active');
		} else {
			$('.org_strSec .addEmpSec .dropdown-menu').slideUp();
			$('.newStandDropDown').removeClass('active');
			$(this).parent().addClass('active');
			$('.org_strSec .bigBoxOS, .org_strSec .smallBoxOS').removeClass('menuBtnOn');
			$(this).parent().parent().parent().addClass('menuBtnOn');
			$('.org_strSec .bigBoxOS .newStandDropDown .dropdown-menu, .org_strSec .smallBoxOS .newStandDropDown .dropdown-menu').hide();
			$('.org_strSec .bigBoxOS.menuBtnOn .newStandDropDown.active .dropdown-menu, .org_strSec .smallBoxOS.menuBtnOn .newStandDropDown.active .dropdown-menu').slideDown();
		};

		*/
    });


	if($('textarea.mention,input.mention').length > 0) {
		bindMention();
	}

});

function bindMention()
{
	$(function () {

	  $('textarea.mention,input.mention').mentionsInput({
		  
	  onDataRequest:function (mode, query, callback) {
	  $context = $(this); 
	   var data;
		   $.ajax({
				type: "POST",
				url: base_url+'user/getUsersForTagging',
				data: {'lead_id' : $context.attr("data-leadid"), 'text' : query},
				dataType:"json",
				cache: true,
				async: true,
				success: function(result){				
					data=result;//alert(data[0].id);
					callback.call(this, data);
				}
		});
		
		
		
		/*var data = [
		  { id:1, name:'Kenneth Auchenberg', 'avatar':'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif', 'type':'contact' },
		  { id:2, name:'Jon Froda', 'avatar':'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif', 'type':'contact' },
		  { id:3, name:'Anders Pollas', 'avatar':'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif', 'type':'contact' },
		  { id:4, name:'Kasper Hulthin', 'avatar':'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif', 'type':'contact' },
		  { id:5, name:'Andreas Haugstrup', 'avatar':'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif', 'type':'contact' },
		  { id:6, name:'Pete Lacey', 'avatar':'http://cdn0.4dots.com/i/customavatars/avatar7112_1.gif', 'type':'contact' }
		];*/

		//data = _.filter(data, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });

		
	  }
	});

	 /*$('.get-mentions').click(function() {
		 
		$('textarea.mention').mentionsInput('getMentions', function(data) {
		  alert(JSON.stringify(data));
		});
	  }) ;*/

	});
}



function getTaggedFromLeadsScr(div_id)
{
	$('#'+div_id+' .mention').mentionsInput('getMentions', function(data) {
     var tagged_ids = '' ;
	 for(var i=0;i<data.length;i++){
        var obj = data[i];
        for(var key in obj){
			if(key == 'id'){
				if(tagged_ids != ''){
					tagged_ids += ',';
				}
				tagged_ids += obj[key];
			}
        }
    }
	$('#'+div_id+' .tagged_ids').val(tagged_ids);
	$('#'+div_id+' .mb_form').submit();
  //alert(tagged_ids);
    });
}

//10 Nov 2016 - creating function for copying on tagged ids and not submitting the form
function getTaggedFromLeads(div_id)
{
	$('#'+div_id+' .mention').mentionsInput('getMentions', function(data) {
     var tagged_ids = '' ;
	 for(var i=0;i<data.length;i++){
        var obj = data[i];
        for(var key in obj){
			if(key == 'id'){
				if(tagged_ids != ''){
					tagged_ids += ',';
				}
				tagged_ids += obj[key];
			}
        }
    }
	$('#'+div_id+' .tagged_ids').val(tagged_ids);
	//	$('#'+div_id+' .mb_form').submit();
	//alert(tagged_ids);
    });
}


//		16-May-2016

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function loadTestDriveAction(msgid,leadid)
{
	$('#AddPlusAreaCreateAnAction'+leadid+'').show();
	$('#AddPlusAreaCreateAnAction'+leadid+'').find("[data-target='#Schedule_Test" + leadid + "']").click();
	$('#AddPlusAreaCreateAnAction'+leadid+'').find("[id='typeScheduled" + leadid + "']").click();	

	$.ajax({
		type: "POST",
		url: base_url+'/Lead/getScheduledMessage',
		data: {'id' : msgid},
		dataType:"json",
		success: function(result){				
			
			$('#Schedule_Test'+leadid+'').find("textarea[name='comments']").text(result.comments);
			$('#Schedule_Test'+leadid+'').find(".datepicker").val(result.schedule_date);
			$('#Schedule_Test'+leadid+'').find("#date_alternate").val(result.create_action_date);
			$('#Schedule_Test'+leadid+'').find(".timepicker").val(result.schedule_time);
			$('#Schedule_Test'+leadid+'').find(".time_alternate").val(result.create_action_time);
			$('#Schedule_Test'+leadid+'').find("#update_scheduled_message_id").val(msgid);			
			$('#Schedule_Test'+leadid+'').find("div[id='project-icon"+leadid+"']").html(result.vehicles);
			$('#Schedule_Test'+leadid+'').find("#project-id"+leadid+"").val("0"); //it can be any value just to fill
			$('#Schedule_Test'+leadid+'').find(':submit').attr('value', 'Complete');
			$('#Schedule_Test'+leadid+'').find('#test_drive_completed').val('1');
			$('#Schedule_Test'+leadid+'').find('#scheduled_canceled_btn').show();

			//$('#msglog'+msgid+'').remove();
		}
	});
}

function changeButtonToUpdate(leadid)
{
	if($('#Schedule_Test'+leadid+'').find("#update_scheduled_message_id").val()!=="0")							
	{
		//
		$('#Schedule_Test'+leadid+'').find(':submit').attr('value', 'Update');
		$('#Schedule_Test'+leadid+'').find('#test_drive_completed').val('0');

	}
}

function walkinTestDrive(leadid)
{
	//$('.updSchedMsg').val('0'); otherwise ajax call is still on the way so it creates problem if we set it to 0 here.
	// the expand will reload so by default it will be 0

	//$('.updSchedComm').text(''); 
	//$('#schSubm'+leadid+'').attr('value', 'Create'); 
	
	$('#Schedule_Test'+leadid+'').find('#scheduled_canceled_btn').hide();
	$('#Schedule_Test'+leadid+'').find('#test_drive_completed').val('0');
	$('#Schedule_Test'+leadid+'').find('#test_drive_canceled').val('0');
	$('#tdrive'+leadid+'').hide(); 
	//$('.datepicker,.timepicker,.date_alternate,.time_alternate,#project-id'+leadid+'').val('');
	$('.datepicker,.timepicker,.date_alternate,.time_alternate').val('');
	if($('#schSubm'+leadid+'').attr('value')=='Complete')
	{
		$('#schSubm'+leadid+'').attr('value', 'Update'); 
	}
	
//code to set "changeTestDrive" input value on Walk In radio btn checked	
	var radioBtnUnscd = $("#typeUnscheduled"+leadid).is(":checked");
	var carExist = $("#project-icon"+leadid).find(".specific_car").text();
	var chekIfUpdate = $("#schSubm"+leadid).val();

	if(carExist != "" && radioBtnUnscd == true && chekIfUpdate == "Update"){
		$("#changeTestDrive_"+leadid).attr("value",1);
	}

}



function testDriveCanceled(leadid)
{
	if(confirm("Are you sure you want to cancel?"))
	{
		$('#Schedule_Test'+leadid+'').find('#test_drive_canceled').val('1');
		$('#Schedule_Test'+leadid+'').find('#test_drive_completed').val('0');
		$('#Schedule_Test'+leadid+'').find('form').submit();
	}
	
	return false;
}

function loadCallBackAction(msgid,leadid)
{
	$('#AddPlusAreaCreateAnAction'+leadid+'').show();
	$('#AddPlusAreaCreateAnAction'+leadid+'').find("[data-target='#Call" + leadid + "']").click();
	$('#AddPlusAreaCreateAnAction'+leadid+'').find("[id='acblrba" + leadid + "']").click();	

	$.ajax({
		type: "POST",
		url: base_url+'/Lead/getCallScheduledMessage',
		data: {'id' : msgid},
		dataType:"json",
		success: function(result){				
			
			$('#Call'+leadid+'').find("textarea[name='comments']").text(result.comments);
			$('#Call'+leadid+'').find(".datepicker").val(result.schedule_date);
			$('#Call'+leadid+'').find("#date_alternate").val(result.create_action_date);
			$('#Call'+leadid+'').find(".timepicker").val(result.schedule_time);
			$('#Call'+leadid+'').find(".time_alternate").val(result.create_action_time);
			$('#Call'+leadid+'').find("#update_scheduled_message_id").val(msgid);						
			$('#Call'+leadid+'').find(':submit').attr('value', 'Update');
		}
	});
}

function clearAllNotifications()
{
	
}


// sarfraz code

function displaySelectBox(id)
{
	
	var display = $("#"+id).css( "display" );
	    if(display == 'none')
		{
			$("#"+id).show();
		}else
		{
			$("#"+id).hide();
		}
		
}
// end sarfraz code

function finishBtnProc(leadId)
{
	$('#finbtn'+leadId+'').prop('value', 'Wait..');
	$('#finbtn'+leadId+'').attr('onclick', "doneFinish('"+leadId+"')");
	$.ajax({
		type: "POST",
		url: base_url+'/Lead/checkIfCustomerInteracted',
		data: {'lead_id':leadId},
		//dataType:"json",
		cache: false,
		success: function(result)
		{			

			if(result=="1") //result true/false 1/0
			{
				 //customer has Interacted
				 doneFinish(leadId);
				
			}
			else
			{		
				//$('#nalt'+leadId+'').val('1');
				//$('#finbtn'+leadId+'').prop('value', 'Finish (Not a Lead)');
				$('#finbtn'+leadId+'').prop('value', 'Finish');
				$('#hnallns'+leadId+'').fadeIn();				
				
			}
		}
	});
 }

 function doneFinish(leadId)
 {
	 
	if($("#hnallns"+leadId+"").is(":visible"))
	{
		if($("#nalt"+leadId+"").val()==="0")
		{			
			return false;
		}
	}
	
	$('#finishBtnFrm'+leadId+'').submit();
	$('#finbtn'+leadId+'').hide();
	$('#hnallns'+leadId+'').hide();
	$('#statusval'+leadId+',#duedateval'+leadId+'').html('Finished');
	if($('#nalt'+leadId+'').val()!='0')
	{
		$('#send_survey'+leadId+'').val('0');
		$('#onlyCloseTxt'+leadId+'').text('Close');
	}
 }


	function turnOffLeads(viewStatus){
		
		$.ajax({
		type: "POST",
		url: base_url+'/Lead/viewLeadStatus',
		data: {'viewLeadStatus':viewStatus},
		dataType:"json",
		cache: false,
		success: function(result)
		{	//alert(result)
			if(result !=null){
				window.location.reload();
			}else{
				alert("Error: pleae try again!")
			}
		}
	});
	}